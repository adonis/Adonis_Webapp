<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\Treatment;

class ProtocolFixtures extends Fixture implements DependentFixtureInterface
{

    public function getDependencies(): array
    {
        return [
            AuthenticationFixtures::class,
            FactorFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        /** @var Factor $factor1 */
        $factor1 = $this->getReference("factor1");
        /** @var User $admin */
        $admin = $this->getReference('admin');
        $protocol = (new Protocol())
            ->setName("Protocol")
            ->setSite($this->getReference("site1"))
            ->setAim('')
            ->addFactors($factor1)
            ->setTreatments(new ArrayCollection([
                (new Treatment())
                    ->setName('Treatment')
                    ->setShortName('')
                    ->setRepetitions(1)
                    ->setModalities(new ArrayCollection([$factor1->getModalities()->get(0)]))
            ]));
        $protocol->setOwner($admin);
        $this->setReference("protocol1", $protocol);
        $manager->persist($protocol);

        $manager->flush();
    }
}
