<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Modality;

class FactorFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            AuthenticationFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $factor = (new Factor())
            ->setName("Factor")
            ->setSite($this->getReference("site1"))
            ->setModalities(new ArrayCollection([
                (new Modality())->setValue("string"),
            ]));
        $this->setReference("factor1", $factor);
        $manager->persist($factor);

        $manager->flush();
    }


}
