<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;

class ProjectFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            AuthenticationFixtures::class,
            ExperimentFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        /** @var User $admin */
        $admin = $this->getReference('admin');
        /** @var Site $site */
        $site = $this->getReference("site1");
        /** @var Platform $platform */
        $platform = $this->getReference('platform1');

        $factor = (new Factor())
            ->setName("Factor")
            ->setModalities(new ArrayCollection([
                (new Modality())->setValue("string"),
            ]));
        $protocol = (new Protocol())
            ->setName("Protocol")
            ->setAim('')
            ->setFactors(new ArrayCollection([$factor]))
            ->setOwner($admin)
            ->setTreatments(new ArrayCollection([
                (new Treatment())
                    ->setName('Treatment')
                    ->setShortName('')
                    ->setRepetitions(1)
                    ->setModalities(new ArrayCollection([$factor->getModalities()[0]]))
            ]));
        $experiment4 = (new Experiment())
            ->setName('3')
            ->setSite($site)
            ->setOwner($admin)
            ->setProtocol($protocol)
            ->setBlocks(new ArrayCollection([
                (new Block())
                    ->setNumber('3')
                    ->setUnitPlots(new ArrayCollection([
                        (new UnitPlot())
                            ->setNumber('3')
                            ->setTreatment($protocol->getTreatments()[0])
                            ->setIndividuals(new ArrayCollection([
                                (new Individual())
                                    ->setNumber('1')
                                    ->setX(3)
                                    ->setY(1),
                                (new Individual())
                                    ->setNumber('2')
                                    ->setX(3)
                                    ->setY(2)
                            ]))
                    ]))
            ]))
            ->setIndividualUP(true);

        $platform->addExperiment($experiment4);

        $this->setReference("experiment4", $experiment4);
        $manager->persist($experiment4);

        $manager->flush();
    }

}
