<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;

class SimpleVariableFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            AuthenticationFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $shortName = "short";
        $repetitions = 1;
        $pathLevel = PathLevelEnum::INDIVIDUAL;
        $mandatory = false;
        $name = "Alpha";
        $type = VariableTypeEnum::ALPHANUMERIC;
        $variable = (new SimpleVariable($name, $shortName, $repetitions, $pathLevel, $mandatory, $type))
            ->setSite($this->getReference("site1"));
        $this->setReference("variableAlpha1", $variable);
        $manager->persist($variable);

        $variable = (new SimpleVariable($name, $shortName, $repetitions, $pathLevel, $mandatory, $type))
            ->setSite($this->getReference("site1"));
        $this->setReference("variableAlpha2", $variable);
        $manager->persist($variable);

        $name = "Int";
        $type = VariableTypeEnum::INTEGER;
        $variable = (new SimpleVariable($name, $shortName, $repetitions, $pathLevel, $mandatory, $type))
            ->setSite($this->getReference("site1"));
        $this->setReference("variableInt", $variable);
        $manager->persist($variable);

        $manager->flush();
    }

}
