<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Shared\Authentication\Entity\RelUserSite;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AuthenticationFixtures extends Fixture
{
    private UserPasswordHasherInterface $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void
    {
        $admin = new User();
        $admin->setName('Jhon')
            ->setSurname('Doe')
            ->setRoles(['ROLE_ADMIN', 'ROLE_USER'])
            ->setActive(true)
            ->setEmail('dummy@test.com')
            ->setUsername('admin')
            ->setPassword($this->encoder->hashPassword($admin, 'admin'));
        $this->setReference("admin", $admin);
        $manager->persist($admin);

        $user1 = new User();
        $user1->setName('Jhon')
            ->setSurname('Dare')
            ->setRoles(['ROLE_USER'])
            ->setActive(true)
            ->setEmail('dummy@test.com')
            ->setUsername('user1')
            ->setPassword($this->encoder->hashPassword($user1, 'user1'));
        $this->setReference("user1", $user1);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setName('Jhon')
            ->setSurname('De La-Rivérà')
            ->setRoles(['ROLE_USER'])
            ->setActive(true)
            ->setEmail('dummy@test.com')
            ->setUsername('user2')
            ->setPassword($this->encoder->hashPassword($user2, 'user2'));
        $this->setReference("user2", $user2);
        $manager->persist($user2);

        $platformManager = new User();
        $platformManager->setName('Jhonnt')
            ->setSurname('Cash')
            ->setRoles(['ROLE_USER'])
            ->setActive(true)
            ->setEmail('dummy@test.com')
            ->setUsername('platformManager')
            ->setPassword($this->encoder->hashPassword($platformManager, 'platformManager'));
        $this->setReference("platformManager", $platformManager);
        $manager->persist($platformManager);

        $experimenter = new User();
        $experimenter->setName('Exp')
            ->setSurname('Erimenter')
            ->setRoles(['ROLE_USER'])
            ->setActive(true)
            ->setEmail('dummy@test.com')
            ->setUsername('experimenter')
            ->setPassword($this->encoder->hashPassword($experimenter, 'experimenter'));
        $this->setReference("experimenter", $experimenter);
        $manager->persist($experimenter);

        $site1 = new Site();
        $site1->setLabel('MySite1');
        $this->setReference("site1", $site1);
        $manager->persist($site1);

        $site2 = new Site();
        $site2->setLabel('MySite2');
        $this->setReference("site2", $site2);
        $manager->persist($site2);

        // ------------------- SITE 1 -------------------
        $rel = new RelUserSite();
        $rel->setSite($site1)
            ->setUser($admin)
            ->setRole(RelUserSite::SITE_ADMIN);
        $manager->persist($rel);

        $rel = new RelUserSite();
        $rel->setSite($site1)
            ->setUser($user1)
            ->setRole(RelUserSite::PLATFORM_MANAGER);
        $manager->persist($rel);

        $rel = new RelUserSite();
        $rel->setSite($site1)
            ->setUser($platformManager)
            ->setRole(RelUserSite::PLATFORM_MANAGER);
        $manager->persist($rel);

        $rel = new RelUserSite();
        $rel->setSite($site1)
            ->setUser($experimenter)
            ->setRole(RelUserSite::EXPERIMENTER);
        $manager->persist($rel);

        // ------------------- SITE 2 -------------------
        $rel = new RelUserSite();
        $rel->setSite($site2)
            ->setUser($admin)
            ->setRole(RelUserSite::SITE_ADMIN);
        $manager->persist($rel);

        $rel = new RelUserSite();
        $rel->setSite($site2)
            ->setUser($user2)
            ->setRole(RelUserSite::PLATFORM_MANAGER);
        $manager->persist($rel);

        $rel = new RelUserSite();
        $rel->setSite($site2)
            ->setUser($platformManager)
            ->setRole(RelUserSite::PLATFORM_MANAGER);
        $manager->persist($rel);

        $rel = new RelUserSite();
        $rel->setSite($site2)
            ->setUser($experimenter)
            ->setRole(RelUserSite::EXPERIMENTER);
        $manager->persist($rel);

        $manager->flush();
    }
}
