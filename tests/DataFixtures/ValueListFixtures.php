<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Webapp\Core\Entity\ValueList;
use Webapp\Core\Entity\ValueListItem;

class ValueListFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            AuthenticationFixtures::class,
            SimpleVariableFixtures::class
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $valueList = (new ValueList())
            ->setName("ValueList")
            ->setValues(new ArrayCollection([
                new ValueListItem("ListItem")
            ]))
            ->setSite($this->getReference("site1"));
        $this->setReference("valueList1", $valueList);
        $manager->persist($valueList);

        $valueList = (new ValueList())
            ->setName("ValueList")
            ->setValues(new ArrayCollection([
                (new ValueListItem("ListItem"))
            ]))
            ->setVariable($this->getReference("variableAlpha2"));
        $this->setReference("valueList2", $valueList);
        $manager->persist($valueList);

        $manager->flush();
    }
    
}
