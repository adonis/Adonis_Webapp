<?php

namespace Tests\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;
use Webapp\Core\Enumeration\ExperimentStateEnum;

class ExperimentFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            AuthenticationFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        /** @var User $admin */
        $admin = $this->getReference('admin');
        /** @var Site $site */
        $site = $this->getReference("site1");

        $factor1 = (new Factor())
            ->setName("Factor")
            ->setModalities(new ArrayCollection([
                (new Modality())->setValue("string"),
            ]));
        $protocol1 = (new Protocol())
            ->setName("Protocol")
            ->setAim('')
            ->setFactors(new ArrayCollection([$factor1]))
            ->setOwner($admin)
            ->setTreatments(new ArrayCollection([
                (new Treatment())
                    ->setName('Treatment')
                    ->setShortName('')
                    ->setRepetitions(1)
                    ->setModalities(new ArrayCollection([$factor1->getModalities()[0]]))
            ]));
        $experiment1 = (new Experiment())
            ->setName('1')
            ->setSite($site)
            ->setOwner($admin)
            ->setProtocol($protocol1)
            ->setBlocks(new ArrayCollection([
                (new Block())
                    ->setNumber('1')
                    ->setUnitPlots(new ArrayCollection([
                        (new UnitPlot())
                            ->setNumber('1')
                            ->setTreatment($protocol1->getTreatments()[0])
                            ->setIndividuals(new ArrayCollection([
                                (new Individual())
                                    ->setNumber('1')
                                    ->setX(1)
                                    ->setY(1),
                                (new Individual())
                                    ->setNumber('2')
                                    ->setX(1)
                                    ->setY(2)
                            ]))
                    ]))
            ]))
            ->setIndividualUP(true);

        $factor2 = (new Factor())
            ->setName("Factor")
            ->setModalities(new ArrayCollection([
                (new Modality())->setValue("string"),
            ]));
        $protocol2 = (new Protocol())
            ->setName("Protocol")
            ->setAim('')
            ->setFactors(new ArrayCollection([$factor2]))
            ->setOwner($admin)
            ->setTreatments(new ArrayCollection([
                (new Treatment())
                    ->setName('Treatment')
                    ->setShortName('')
                    ->setRepetitions(1)
                    ->setModalities(new ArrayCollection([$factor2->getModalities()[0]]))
            ]));
        $experiment2 = (new Experiment())
            ->setName('2')
            ->setSite($site)
            ->setOwner($admin)
            ->setProtocol($protocol2)
            ->setBlocks(new ArrayCollection([
                (new Block())
                    ->setNumber('1')
                    ->setUnitPlots(new ArrayCollection([
                        (new UnitPlot())
                            ->setNumber('1')
                            ->setTreatment($protocol2->getTreatments()[0])
                            ->setIndividuals(new ArrayCollection([
                                (new Individual())
                                    ->setNumber('1')
                                    ->setX(1)
                                    ->setY(1),
                                (new Individual())
                                    ->setNumber('2')
                                    ->setX(1)
                                    ->setY(2)
                            ]))
                    ]))
            ]))
            ->setState(ExperimentStateEnum::LOCKED)
            ->setIndividualUP(true);

        $factor3 = (new Factor())
            ->setName("Factor")
            ->setModalities(new ArrayCollection([
                (new Modality())->setValue("string"),
            ]));
        $protocol3 = (new Protocol())
            ->setName("Protocol")
            ->setAim('')
            ->setFactors(new ArrayCollection([$factor3]))
            ->setOwner($admin)
            ->setTreatments(new ArrayCollection([
                (new Treatment())
                    ->setName('Treatment')
                    ->setShortName('')
                    ->setRepetitions(1)
                    ->setModalities(new ArrayCollection([$factor3->getModalities()[0]]))
            ]));
        $experiment3 = (new Experiment())
            ->setName('3')
            ->setSite($site)
            ->setOwner($admin)
            ->setProtocol($protocol3)
            ->setBlocks(new ArrayCollection([
                (new Block())
                    ->setNumber('1')
                    ->setSurfacicUnitPlots(new ArrayCollection([
                        (new SurfacicUnitPlot())
                            ->setNumber('1')
                            ->setTreatment($protocol3->getTreatments()[0])
                            ->setX(1)
                            ->setY(1),
                        (new SurfacicUnitPlot())
                            ->setNumber('2')
                            ->setTreatment($protocol3->getTreatments()[0])
                            ->setX(1)
                            ->setY(2)
                    ]))
            ]))
            ->setIndividualUP(false);

        $platform1 = (new Platform())
            ->setName('1')
            ->setSite($site)
            ->setOwner($admin)
            ->setExperiments(new ArrayCollection([$experiment2]))
            ->setPlaceName('')
            ->setSiteName('');

        $platform2 = (new Platform())
            ->setName('2')
            ->setSite($site)
            ->setOwner($admin)
            ->setPlaceName('')
            ->setSiteName('');

        $this->setReference("treatment2", $protocol2->getTreatments()[0]);
        $this->setReference("experiment1", $experiment1);
        $this->setReference("experiment2", $experiment2);
        $this->setReference("experiment3", $experiment3);
        $this->setReference("platform1", $platform1);
        $this->setReference("platform2", $platform2);
        $manager->persist($experiment1);
        $manager->persist($experiment2);
        $manager->persist($experiment3);
        $manager->persist($platform1);
        $manager->persist($platform2);

        $manager->flush();
    }
    
}
