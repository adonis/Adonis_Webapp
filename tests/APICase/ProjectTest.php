<?php

namespace Tests\APICase;


use Tests\DataFixtures\ExperimentFixtures;
use Tests\DataFixtures\ProjectFixtures;

class ProjectTest extends AbstractAPITest
{
    protected function neededFixtures(): array
    {
        return [
            new ExperimentFixtures(),
            new ProjectFixtures()
        ];
    }

    public function testCreateProject()
    {
        $siteIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("site1"));
        $experiment1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $experiment2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment2"));
        $experiment4Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment4"));
        $platform1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("platform1"));
        $this->testEach([
            'method' => 'POST',
            'url' => '/api/projects',
            'role' => 'admin'
        ], [
            [
                'body' => [
                    'name' => 'string',
                    'site' => $siteIri,
                    'platform' => $platform1Iri,
                    'experiments' => [$experiment2Iri],
                ]
            ],
            [
                'body' => [
                    'name' => 'string',
                    'site' => $siteIri,
                    'platform' => $platform1Iri,
                    'experiments' => [$experiment4Iri],
                ], 'errorCode' => 422
            ],
            [
                'body' => [
                    'name' => 'string',
                    'site' => $siteIri,
                    'platform' => $platform1Iri,
                    'experiments' => [$experiment1Iri],
                ], 'errorCode' => 422
            ],
        ], true);

    }
}
