<?php

namespace Tests\APICase;

use Shared\RightManagement\EventSubscriber\HasOwnerEntityCreateEventSubscriber;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\DataFixtures\FactorFixtures;
use Tests\DataFixtures\ProtocolFixtures;

class ProtocolTest extends AbstractAPITest
{
    protected function neededFixtures(): array
    {
        return [
            new FactorFixtures(),
            new ProtocolFixtures()
        ];
    }

    protected function beforeFixtureLoad(ContainerInterface $container): void
    {
        $someService = $this->getMockBuilder(HasOwnerEntityCreateEventSubscriber::class)->disableOriginalConstructor()->getMock();
        $container->set(HasOwnerEntityCreateEventSubscriber::class, $someService);
    }

    public function testGetCollection()
    {
        $this->testEach([
            'method' => 'GET',
            'url' => '/api/protocols',
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'experimenter', 'errorCode' => 403],
        ]);
    }

    public function testGetItem()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("protocol1"));
        $this->testEach([
            'method' => 'GET',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager', 'errorCode' => 404],
            ['role' => 'user1', 'errorCode' => 404],
            ['role' => 'experimenter', 'errorCode' => 404],
            ['role' => 'user2', 'errorCode' => 404],
        ]);
    }

    public function testGetFactorCollection()
    {
        $this->testEach([
            'method' => 'GET',
            'url' => '/api/factors',
        ], [
            ['role' => 'admin', 'content' => ['hydra:totalItems' => 1]],
            ['role' => 'user1', 'content' => ['hydra:totalItems' => 0]],
            ['role' => 'user2', 'content' => ['hydra:totalItems' => 0]],
        ]);
    }

    public function testGetFactor()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("factor1"));
        $this->testEach([
            'method' => 'GET',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager', 'errorCode' => 404],
            ['role' => 'user1', 'errorCode' => 404],
            ['role' => 'experimenter', 'errorCode' => 404],
            ['role' => 'user2', 'errorCode' => 404],
        ]);
    }

    public function testPost()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("site1"));
        $this->testEach([
            'method' => 'POST',
            'url' => '/api/protocols',
            'body' => [
                'name' => 'string',
                "aim" => "aim",
                "factors" => [
                    [
                        "name" => "Facteur1",
                        "modalities" => [
                            ["uniqId" => "096eeecb-35b8-4ec4-a6f2-44f8d2c47704", "value" => "Modalite1", "shortName" => "1"]
                        ],
                        "order" => 0]
                ],
                "comment" => "",
                "treatments" => [
                    ["name" => "Modalite1", "shortName" => "1", "repetitions" => 1, "modalities" => ["096eeecb-35b8-4ec4-a6f2-44f8d2c47704"]]
                ],
                'site' => $iri,
                'protocolAttachments' => [],
            ]
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'errorCode' => 403],
            ['role' => 'admin', 'body' => ['dummy' => 'dummy'], 'errorCode' => 422],
        ], true);
    }

    public function testPostProtocolSameProtocolName()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("site1"));
        $this->testEach([
            'method' => 'POST',
            'url' => '/api/protocols',
            'body' => [
                'name' => 'Protocol',
                "aim" => "aim",
                "factors" => [
                    [
                        "name" => "Facteur1",
                        "modalities" => [
                            ["uniqId" => "096eeecb-35b8-4ec4-a6f2-44f8d2c47704", "value" => "Modalite1", "shortName" => "1"]
                        ],
                        "order" => 0]
                ],
                "comment" => "",
                "treatments" => [
                    ["name" => "Modalite1", "shortName" => "1", "repetitions" => 1, "modalities" => ["096eeecb-35b8-4ec4-a6f2-44f8d2c47704"]]
                ],
                'site' => $iri,
            ]
        ], [
            ['role' => 'admin', 'errorCode' => 422],
        ]);
    }

    public function testDelete()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("protocol1"));
        $this->testEach([
            'method' => 'DELETE',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager', 'errorCode' => 404],
            ['role' => 'user1', 'errorCode' => 404],
            ['role' => 'experimenter', 'errorCode' => 404],
            ['role' => 'user2', 'errorCode' => 404],
        ], true);
    }

    public function testPatch()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("protocol1"));
        $siteIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("site1"));
        $this->testEach([
            'method' => 'PUT',
            'url' => $iri,
            'body' => [
                'name' => 'Hello',
                "aim" => "aim",
                "factors" => [
                    [
                        "name" => "Facteur1",
                        "modalities" => [
                            ["uniqId" => "096eeecb-35b8-4ec4-a6f2-44f8d2c47704", "value" => "Modalite1", "shortName" => "1"]
                        ],
                        "order" => 0]
                ],
                "comment" => "",
                "treatments" => [
                    ["name" => "Modalite1", "shortName" => "1", "repetitions" => 1, "modalities" => ["096eeecb-35b8-4ec4-a6f2-44f8d2c47704"]]
                ],
                'site' => $siteIri,
            ]
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager', 'errorCode' => 404],
            ['role' => 'user1', 'errorCode' => 404],
            ['role' => 'experimenter', 'errorCode' => 404],
            ['role' => 'user2', 'errorCode' => 404],
        ], true);
    }
}
