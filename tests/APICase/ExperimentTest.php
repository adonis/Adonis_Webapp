<?php

namespace Tests\APICase;


use Tests\DataFixtures\ExperimentFixtures;
use Webapp\Core\Enumeration\ExperimentStateEnum;
use Webapp\Core\Enumeration\PathLevelEnum;

class ExperimentTest extends AbstractAPITest
{
    protected function neededFixtures(): array
    {
        return [
            new ExperimentFixtures()
        ];
    }

    public function testGetCollection()
    {
        $this->testEach([
            'method' => 'GET',
            'url' => '/api/experiments',
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager', 'content' => ['hydra:totalItems' => 0]],
            ['role' => 'user1', 'content' => ['hydra:totalItems' => 0]],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'content' => ['hydra:totalItems' => 0]],
        ]);
    }

    public function testGetItem()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $this->testEach([
            'method' => 'GET',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager', 'errorCode' => 404],
            ['role' => 'user1', 'errorCode' => 404],
            ['role' => 'experimenter', 'errorCode' => 404],
            ['role' => 'user2', 'errorCode' => 404],
        ]);
    }

    public function testChangeStatus()
    {
        $experiment1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $experiment2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment2"));
        $this->testEach([
            'method' => 'PATCH',
            'role' => 'admin'
        ], [
            ['url' => $experiment1Iri, 'body' => ['state' => ExperimentStateEnum::LOCKED], 'errorCode' => 422],
            ['url' => $experiment2Iri, 'body' => ['state' => ExperimentStateEnum::CREATED]],
            ['url' => $experiment2Iri, 'body' => ['state' => ExperimentStateEnum::NON_UNLOCKABLE], 'errorCode' => 422],
        ]);
    }

    public function testDirectLinkExperimentToPlatform()
    {
        $experimentIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $platform1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("platform1"));
        $platform2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("platform2"));
        $this->testEach([
            'method' => 'PATCH',
            'role' => 'admin',
        ], [
            ['url' => $experimentIri, 'body' => ['platform' => $platform1Iri], 'errorCode' => 422],
            ['url' => $experimentIri, 'body' => ['platform' => $platform2Iri]],
        ], true);
    }

    public function testLinkExperimentToPlatform()
    {
        $experiment1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $experiment2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment2"));
        $experiment3Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment3"));
        $platform1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("platform1"));
        $this->testEach([
            'method' => 'PATCH',
            'role' => 'admin',
            'url' => '/api/place_experiments'
        ], [
            ['body' => [
                'experiment' => $experiment1Iri,
                'platform' => $platform1Iri,
                'dx' => 0,
                'dy' => 0,
            ], 'errorCode' => 400],
            ['body' => [
                'experiment' => $experiment1Iri,
                'platform' => $platform1Iri,
                'dx' => -1,
                'dy' => 0,
            ], 'errorCode' => 400],
            ['body' => [
                'experiment' => $experiment1Iri,
                'platform' => $platform1Iri,
                'dx' => 1,
                'dy' => 0,
            ]],
            ['body' => [
                'experiment' => $experiment2Iri,
                'platform' => $platform1Iri,
                'dx' => 1,
                'dy' => 0,
            ], 'errorCode' => 400],
            ['body' => [
                'experiment' => $experiment3Iri,
                'platform' => $platform1Iri,
                'dx' => 1,
                'dy' => 0,
            ], 'errorCode' => 400],
        ], true);
    }

    public function testEditExperimentsObjects()
    {
        $experiment1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $experiment2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment2"));
        $platform1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("platform1"));
        $this->testEach([
            'method' => 'PATCH',
            'role' => 'admin',
            'url' => '/api/edits'
        ], [
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment1Iri]],
                'dx' => 1,
                'dy' => 0,
            ]],
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment1Iri]],
                'color' => 1,
            ]],
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment1Iri]],
                'dx' => 1,
                'dy' => 0,
            ], 'role' => 'platformManager', 'errorCode' => 400],
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment2Iri]],
                'dx' => 1,
                'dy' => 0,
            ], 'errorCode' => 400],
            ['body' => [
                'objectIris' => [PathLevelEnum::PLATFORM => [$platform1Iri]],
                'dx' => 1,
                'dy' => 0,
            ], 'errorCode' => 400],
        ], true);
    }

    public function testDeleteExperimentsObjects()
    {
        $experiment1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $experiment2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment2"));
        $platform1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("platform1"));
        $this->testEach([
            'method' => 'PATCH',
            'role' => 'admin',
            'url' => '/api/deletes'
        ], [
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment1Iri]],
            ]],
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment1Iri]],
            ], 'role' => 'platformManager', 'errorCode' => 400],
            ['body' => [
                'objectIris' => [PathLevelEnum::EXPERIMENT => [$experiment2Iri]],
            ], 'errorCode' => 400],
            ['body' => [
                'objectIris' => [PathLevelEnum::PLATFORM => [$platform1Iri]],
            ], 'errorCode' => 400],
        ], true);
    }

    public function testCreateExperimentsObjects()
    {
        $experiment1Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment1"));
        $treatmentIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("treatment2"));
        $experiment2Iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("experiment2"));
        $this->testEach([
            'method' => 'PATCH',
            'role' => 'admin',
            'url' => '/api/creates'
        ], [
            ['body' => [
                'parent' => $experiment1Iri,
                'useSubBlock' => true,
                'x1' => 2,
                'x2' => 3,
                'y1' => 2,
                'y2' => 3,
                'treatment' => $treatmentIri,
                'pathLevel' => PathLevelEnum::BLOCK
            ]],
            ['body' => [
                'parent' => $experiment1Iri,
                'useSubBlock' => true,
                'x1' => 1,
                'x2' => 3,
                'y1' => 1,
                'y2' => 3,
                'treatment' => $treatmentIri,
                'pathLevel' => PathLevelEnum::BLOCK
            ], 'errorCode' => 400],
            ['body' => [
                'parent' => $experiment2Iri,
                'useSubBlock' => true,
                'x1' => 2,
                'x2' => 3,
                'y1' => 2,
                'y2' => 3,
                'treatment' => $treatmentIri,
                'pathLevel' => PathLevelEnum::BLOCK
            ], 'errorCode' => 400],
        ], true);
    }
}
