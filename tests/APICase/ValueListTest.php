<?php

namespace Tests\APICase;


use Tests\DataFixtures\SimpleVariableFixtures;
use Tests\DataFixtures\ValueListFixtures;

class ValueListTest extends AbstractAPITest
{
    protected function neededFixtures(): array
    {
        return [
            new SimpleVariableFixtures(),
            new ValueListFixtures()
        ];
    }

    public function testGetCollection()
    {
        $this->testEach([
            'method' => 'GET',
            'url' => '/api/value_lists',
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1', 'content' => ['hydra:totalItems' => 2]],
            ['role' => 'experimenter', 'errorCode' => 403],
        ]);
    }

    public function testGetItem()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("valueList1"));
        $this->testEach([
            'method' => 'GET',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
            ['role' => 'experimenter', 'errorCode' => 403],
        ]);
    }

    public function testLinkVariable()
    {
        $valueListIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("valueList1"));
        $variableIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("variableAlpha1"));
        $this->testEach([
            'method' => 'PATCH',
            'url' => $valueListIri,
            'body' => [
                'variable' => $variableIri,
            ]
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
        ], true);
    }

    public function testLinkVariableUnauthorized()
    {
        $valueListIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("valueList1"));
        $variableIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("variableInt"));
        $this->testEach([
            'method' => 'PATCH',
            'url' => $valueListIri,
            'body' => [
                'variable' => $variableIri,
            ]
        ], [
            ['role' => 'admin', 'errorCode' => 403],
        ], true);
    }

    public function testLinkVariableExistingOtherValueList()
    {
        $valueListIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("valueList1"));
        $variableIri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("variableAlpha2"));
        $this->testEach([
            'method' => 'PATCH',
            'url' => $valueListIri,
            'body' => [
                'variable' => $variableIri,
            ]
        ], [
            ['role' => 'admin', 'errorCode' => 403],
        ], true);
    }


}
