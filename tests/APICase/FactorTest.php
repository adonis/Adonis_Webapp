<?php

namespace Tests\APICase;


use Tests\DataFixtures\FactorFixtures;

class FactorTest extends AbstractAPITest
{
    protected function neededFixtures(): array
    {
        return [new FactorFixtures()];
    }

    public function testGetCollection()
    {
        $this->testEach([
            'method' => 'GET',
            'url' => '/api/factors',
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1', 'content' => ['hydra:totalItems' => 1]],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'content' => ['hydra:totalItems' => 0]],
        ]);
    }

    public function testGetItem()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("factor1"));
        $this->testEach([
            'method' => 'GET',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'errorCode' => 404],
        ]);
    }

    public function testPostFactor()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("site1"));
        $this->testEach([
            'method' => 'POST',
            'url' => '/api/factors',
            'body' => [
                'name' => 'string',
                'modalities' => [
                    [
                        'value' => 'string',
                        'shortName' => 'string',
                        'identifier' => 'string',
                    ],
                ],
                'site' => $iri,
            ]
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'errorCode' => 403],
            ['role' => 'admin', 'body' => ['dummy' => 'dummy'], 'errorCode' => 422],
        ], true);
    }

    public function testPostFactorSameFactorName()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("site1"));
        $this->testEach([
            'method' => 'POST',
            'url' => '/api/factors',
            'body' => [
                'name' => 'Factor',
                'modalities' => [
                    [
                        'value' => 'string',
                        'shortName' => 'string',
                        'identifier' => 'string',
                    ],
                ],
                'site' => $iri,
            ]
        ], [
            ['role' => 'admin', 'errorCode' => 422],
        ]);
    }

    public function testDeleteFactor()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("factor1"));
        $this->testEach([
            'method' => 'DELETE',
            'url' => $iri,
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'errorCode' => 404],
        ], true);
    }

    public function testPatchFactor()
    {
        $iri = $this->iriConverter->getIriFromItem($this->referenceRepository->getReference("factor1"));
        $this->testEach([
            'method' => 'PATCH',
            'url' => $iri,
            'body' => [
                'name' => 'strong',
                'modalities' => [
                    [
                        'value' => 'string',
                        'shortName' => 'string',
                        'identifier' => 'string',
                    ],
                ]
            ]
        ], [
            ['role' => 'admin'],
            ['role' => 'platformManager'],
            ['role' => 'user1'],
            ['role' => 'experimenter', 'errorCode' => 403],
            ['role' => 'user2', 'errorCode' => 404],
        ], true);
    }

}
