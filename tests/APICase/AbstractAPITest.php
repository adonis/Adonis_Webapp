<?php


namespace Tests\APICase;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\Loader\SymfonyFixturesLoader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use Shared\RightManagement\SQLFilter\AdvancedRightFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Tests\DataFixtures\AuthenticationFixtures;

abstract class AbstractAPITest extends ApiTestCase
{
    private ?EntityManagerInterface $em = null;

    protected ReferenceRepository $referenceRepository;
    protected IriConverterInterface $iriConverter;

    protected function getEntityManager(): EntityManagerInterface
    {
        if ($this->em === null) {
            $this->em = self::getContainer()->get(EntityManagerInterface::class);
        }

        return $this->em;
    }

    public function setUp(): void
    {
        self::bootKernel();

        $em = $this->getEntityManager();
        $this->iriConverter = self::getContainer()->get(IriConverterInterface::class);
        $encoder = self::getContainer()->get(UserPasswordHasherInterface::class);

        $metadatas = $this->getEntityManager()->getMetadataFactory()->getAllMetadata();
        $schemaTool = new SchemaTool($this->getEntityManager());
        $schemaTool->dropDatabase();
        $schemaTool->updateSchema($metadatas);

        $this->beforeFixtureLoad(self::getContainer());

        $loader = new SymfonyFixturesLoader(self::getContainer());
        $authFixtures = new AuthenticationFixtures($encoder);
        $loader->addFixture($authFixtures);
        foreach ($this->neededFixtures() as $fixture) {
            $loader->addFixture($fixture);
        }

        $purger = new ORMPurger($em);
        $executor = new ORMExecutor($em, $purger);

        $em->getFilters()->disable(AdvancedRightFilter::NAME);
        $executor->execute($loader->getFixtures());
        $em->getFilters()->enable(AdvancedRightFilter::NAME);

        $this->referenceRepository = $executor->getReferenceRepository();

        parent::setUp();
    }

    public function tearDown(): void
    {
        parent::tearDown();
        $schemaTool = new SchemaTool($this->getEntityManager());
        $schemaTool->dropDatabase();
    }

    protected function createClientWithCredentials($token = null, $extraHeaders = []): Client
    {
        $token = $token ?: $this->getToken();

        return static::createClient([], ['headers' => array_merge(['authorization' => 'Bearer ' . $token, 'content-type' => 'application/json'], $extraHeaders)]);
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken($body = []): string
    {
        $response = static::createClient()->request('POST', '/authentication/login', [
            'json' => $body ?: [
                'username' => 'admin',
                'password' => 'admin',
            ],
            'headers' => ['content-type' => 'application/json']
        ]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }

    /**
     * @return Fixture[]
     */
    abstract protected function neededFixtures(): array;

    protected function beforeFixtureLoad(ContainerInterface $container): void
    {

    }

    /**
     * @param array $commonParams
     * @param array<array> $testParams
     */
    protected function testEach(array $commonParams, array $testParams, $needReset = false)
    {
        foreach ($testParams as $key => $param) {
            $testParam = array_merge($commonParams, $param);
            $userToken = $this->getToken([
                'username' => $testParam['role'],
                'password' => $testParam['role'],
            ]);
            $extraHeaders = $testParam['method'] === 'PATCH' ? [
                'content-type' => 'application/merge-patch+json'
            ] : [];
            $requestOptions = isset($testParam['body']) ? ['json' => $testParam['body']] : [];
            static::createClientWithCredentials($userToken, $extraHeaders)
                ->request($testParam['method'], $testParam['url'], $requestOptions);

            if (isset($testParam['errorCode'])) {
                $this->assertResponseStatusCodeSame($testParam['errorCode']);
            } else {
                if (isset($testParam['content'])) {
                    $this->assertJsonContains($testParam['content']);
                }
                $this->assertResponseIsSuccessful();
            }

            if ($needReset && count($testParams) - 1 !== $key) {
                $this->tearDown();
                $this->setUp();
            }
        }
    }
}
