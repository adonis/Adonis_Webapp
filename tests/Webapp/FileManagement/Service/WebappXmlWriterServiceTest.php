<?php

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service;

use Shared\Authentication\Entity\Site;
use Webapp\Core\Entity\Algorithm;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\OezNature;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\Project;
use Webapp\Core\Entity\ProjectData;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\Session;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Entity\StateCode;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;
use Webapp\FileManagement\Service\WebappXmlWriterService;

class WebappXmlWriterServiceTest extends AbstractXmlServiceTest
{
    public function testGeneratePlatformWithData(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlWriterService::class);

        $xml = $service->generatePlatformWithData($this->createPlatform(true));

        $this->assertXmlStringEqualsXmlFile(__DIR__.'/resources/webapp-xml-writer/platform-with-data.xml', $xml);
    }

    public function testGeneratePlatform(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlWriterService::class);

        $xml = $service->generatePlatform($this->createPlatform(false));

        $this->assertXmlStringEqualsXmlFile(__DIR__.'/resources/webapp-xml-writer/platform.xml', $xml);
    }

    public function testGenerateExperiment(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlWriterService::class);

        $xml = $service->generateExperiment($this->createExperiment());

        $this->assertXmlStringEqualsXmlFile(__DIR__.'/resources/webapp-xml-writer/experiment.xml', $xml);
    }

    private function createExperiment(): Experiment
    {
        $site = $this->createSite();

        $modality1 = new Modality();
        $this->setEntityId($modality1, 1);
        $modality1->setValue('L26_12');
        $modality2 = new Modality();
        $this->setEntityId($modality2, 2);
        $modality2->setValue('L26_11');
        $modality3 = new Modality();
        $this->setEntityId($modality3, 3);
        $modality3->setValue('L26_10');

        $factor1 = new Factor();
        $this->setEntityId($factor1, 1);
        $factor1
            ->setName('Facteur 1')
            ->addModality($modality1)
            ->addModality($modality2)
            ->addModality($modality3);

        $factor2 = new Factor();
        $this->setEntityId($factor2, 2);
        $factor2->setName('Facteur 2');
        $factor3 = new Factor();
        $this->setEntityId($factor3, 3);
        $factor3->setName('Facteur 3');

        $treatment1 = new Treatment();
        $this->setEntityId($treatment1, 1);
        $treatment1
            ->setName('L26_12')
            ->setShortName('26_12')
            ->setRepetitions(1)
            ->addModalities($modality1);

        $treatment2 = new Treatment();
        $this->setEntityId($treatment2, 2);
        $treatment2
            ->setName('L26_11')
            ->setShortName('26_11')
            ->setRepetitions(1)
            ->addModalities($modality2);

        $treatment3 = new Treatment();
        $this->setEntityId($treatment3, 3);
        $treatment3
            ->setName('L26_10')
            ->setShortName('26_10')
            ->setRepetitions(1)
            ->addModalities($modality3);

        $individual1 = new Individual();
        $this->setEntityId($individual1, 1);
        $individual1
            ->setNumber('1')
            ->setX(22)
            ->setY(1)
            ->setAppeared(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual2 = new Individual();
        $this->setEntityId($individual2, 2);
        $individual2
            ->setNumber('2')
            ->setX(22)
            ->setY(2)
            ->setAppeared(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual3 = new Individual();
        $this->setEntityId($individual3, 3);
        $individual3
            ->setNumber('3')
            ->setX(30)
            ->setY(34)
            ->setAppeared(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual4 = new Individual();
        $this->setEntityId($individual4, 4);
        $individual4
            ->setNumber('4')
            ->setX(30)
            ->setY(35)
            ->setAppeared(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual5 = new Individual();
        $this->setEntityId($individual5, 5);
        $individual5
            ->setNumber('5')
            ->setX(30)
            ->setY(36)
            ->setAppeared(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual6 = new Individual();
        $this->setEntityId($individual6, 6);
        $individual6
            ->setNumber('6')
            ->setX(30)
            ->setY(37)
            ->setAppeared(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $unitPlot1 = new UnitPlot();
        $this->setEntityId($unitPlot1, 1);
        $unitPlot1
            ->setNumber('1')
            ->setTreatment($treatment1)
            ->addIndividual($individual1);

        $unitPlot2 = new UnitPlot();
        $this->setEntityId($unitPlot2, 2);
        $unitPlot2
            ->setNumber('2')
            ->setTreatment($treatment2)
            ->addIndividual($individual2);

        $unitPlot3 = new UnitPlot();
        $this->setEntityId($unitPlot3, 3);
        $unitPlot3
            ->setNumber('3')
            ->setTreatment($treatment3)
            ->addIndividual($individual3)
            ->addIndividual($individual4)
            ->addIndividual($individual5)
            ->addIndividual($individual6);

        $bloc = new Block();
        $this->setEntityId($bloc, 1);
        $bloc
            ->setNumber('1')
            ->addUnitPlots($unitPlot1)
            ->addUnitPlots($unitPlot2)
            ->addUnitPlots($unitPlot3);

        $algorithm = new Algorithm();
        $this->setEntityId($algorithm, 1);
        $algorithm
            ->setName('Sans Tirage');

        $protocol = new Protocol();
        $this->setEntityId($protocol, 1);
        $protocol
            ->setName('Mon protocole')
            ->setCreated(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-05-24T08:23:46.000+00:00'))
            ->setOwner($this->getUser())
            ->setAim('')
            ->setAlgorithm($algorithm)
            ->addFactors($factor1)
            ->addFactors($factor2)
            ->addFactors($factor3)
            ->addTreatments($treatment1)
            ->addTreatments($treatment2)
            ->addTreatments($treatment3);

        $experiment = new Experiment();
        $this->setEntityId($experiment, 1);
        $experiment
            ->setSite($site)
            ->setName('Mon dispositif')
            ->setCreated(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-05-24T08:23:46.000+00:00'))
            ->setValidated(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-12-22T10:30:56.000+00:00'))
            ->setIndividualUP(true)
            ->setProtocol($protocol)
            ->addBlocks($bloc);

        return $experiment;
    }

    private function createPlatform(bool $withData): Platform
    {
        $platform = new Platform();
        $this->setEntityId($platform, 1);
        $platform
            ->setSite($this->createSite())
            ->setCreated(new \DateTime(self::CURRENT_DATE))
            ->addExperiment($this->createExperiment());

        if ($withData) {
            $platform->addProject($this->createProject($platform));
        }

        return $platform;
    }

    protected function createSite(): Site
    {
        $oezNature1 = new OezNature();
        $this->setEntityId($oezNature1, 1);
        $oezNature1
            ->setNature('Ligne vide')
            ->setColor(321072);

        $oezNature2 = new OezNature();
        $this->setEntityId($oezNature2, 2);
        $oezNature2
            ->setNature('Bordure')
            ->setColor(321072);

        $oezNature3 = new OezNature();
        $this->setEntityId($oezNature3, 3);
        $oezNature3
            ->setNature('B')
            ->setColor(321072);

        $site = new Site();
        $this->setEntityId($site, 1);
        $site
            ->addOezNature($oezNature1)
            ->addOezNature($oezNature2)
            ->addOezNature($oezNature3);

        return $site;
    }

    private function createProject(Platform $platform): Project
    {
        $variable = new SimpleVariable();
        $this->setEntityId($variable, 1);
        $variable
            ->setName('observat')
            ->setShortName('obs')
            ->setRepetitions(1)
//            ->setActive(true)
//            ->setAskTimestamp(true)
            ->setCreated(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2021-10-14T10:27:17.000+00:00'))
            ->setLastModified(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:50:12.000+00:00'))
            ->setOrder(2)
            ->setMandatory(true)
            ->setPathLevel(PathLevelEnum::INDIVIDUAL)
            ->setType(VariableTypeEnum::ALPHANUMERIC);

        $measure1 = new Measure();
        $this->setEntityId($measure1, 1);
        $measure1
            ->setTimestamp(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:52.000+00:00'))
            ->setValue('ras');

        $measure2 = new Measure();
        $this->setEntityId($measure2, 2);
        $measure2
            ->setTimestamp(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:52.000+00:00'))
            ->setValue('Ras');

        $measure3 = new Measure();
        $this->setEntityId($measure3, 3);
        $measure3
            ->setTimestamp(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:52.000+00:00'))
            ->setValue('Ras');

        $measure4 = new Measure();
        $this->setEntityId($measure4, 4);
        $measure4
            ->setTimestamp(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:52.000+00:00'))
            ->setValue('Fente verticale,  écorce rugueuse,  gros lenticelles');

        $block = $platform->getExperiments()->get(0)->getBlocks()->get(0);
        $formField1 = new FieldMeasure();
        $this->setEntityId($formField1, 1);
        $formField1
            ->setVariable($variable)
            ->setTarget($block->getUnitPlots()->get(0)->getIndividuals()->first())
            ->addMeasure($measure1);

        $formField2 = new FieldMeasure();
        $this->setEntityId($formField2, 2);
        $formField2
            ->setVariable($variable)
            ->setTarget($block->getUnitPlots()->get(1)->getIndividuals()->first())
            ->addMeasure($measure2);

        $formField3 = new FieldMeasure();
        $this->setEntityId($formField3, 3);
        $formField3
            ->setVariable($variable)
            ->setTarget($block->getUnitPlots()->get(2)->getIndividuals()->get(0))
            ->addMeasure($measure3);

        $formField4 = new FieldMeasure();
        $this->setEntityId($formField4, 4);
        $formField4
            ->setVariable($variable)
            ->setTarget($block->getUnitPlots()->get(2)->getIndividuals()->get(1))
            ->addMeasure($measure4);

        $session = new Session();
        $this->setEntityId($session, 1);
        $session
            ->setStartedAt(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-05T09:41:20.000+00:00'))
            ->setEndedAt(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-05T09:41:20.000+00:00'))
            ->setUser($this->getUser())
            ->addFieldMeasure($formField1)
            ->addFieldMeasure($formField2)
            ->addFieldMeasure($formField3)
            ->addFieldMeasure($formField4);

        $dataEntry = new ProjectData();
        $this->setEntityId($dataEntry, 1);
        $dataEntry
//            ->setStartedAt(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-05T09:41:20.000+00:00'))
            ->addSession($session)
            ->addVariable($variable);

        $stateCode1 = new StateCode();
        $this->setEntityId($stateCode1, 1);
        $stateCode1
            ->setCode(-6)
            ->setTitle('Donnée Manquante')
            ->setMeaning('Code Etat permanent : Donnée Manquante')
            ->setColor(0);

        $stateCode2 = new StateCode();
        $this->setEntityId($stateCode2, 2);
        $stateCode2
            ->setCode(-5)
            ->setTitle('Non notable')
            ->setMeaning('observation n\'est pas possible car interférence avec un autre événement')
            ->setColor(0);

        $stateCode3 = new StateCode();
        $this->setEntityId($stateCode3, 3);
        $stateCode3
            ->setCode(-8)
            ->setTitle('Absent')
            ->setMeaning('individu absent')
            ->setColor(0);

        $stateCode4 = new StateCode();
        $this->setEntityId($stateCode4, 4);
        $stateCode4
            ->setCode(-9)
            ->setTitle('Mort')
            ->setMeaning('Code Etat permanent : Individu mort')
            ->setColor(0)
            ->setSpreading(PathLevelEnum::INDIVIDUAL);

        $project = new Project();
        $this->setEntityId($project, 1);
        $project
            ->setCreated(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:38:56.000+00:00'))
            ->setName('Mon projet de saisie')
            ->setOwner($this->getUser())
//            ->addDesktopUser($user)
            ->setPlatform($platform)
            ->addExperiment($platform->getExperiments()->first())
            ->addSimpleVariable($variable)
            ->addStateCode($stateCode1)
            ->addStateCode($stateCode2)
            ->addStateCode($stateCode3)
            ->addStateCode($stateCode4)
            ->addProjectData($dataEntry);

        return $project;
    }
}
