<?php

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service;

use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\Device;
use Mobile\Device\Entity\Factor;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\Modality;
use Mobile\Device\Entity\Protocol;
use Mobile\Device\Entity\Treatment;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Measure\Entity\DataEntry;
use Mobile\Measure\Entity\FormField;
use Mobile\Measure\Entity\GeneratedField;
use Mobile\Measure\Entity\Measure;
use Mobile\Measure\Entity\Session;
use Mobile\Measure\Entity\Variable\GeneratorVariable;
use Mobile\Measure\Entity\Variable\StateCode;
use Mobile\Measure\Entity\Variable\UniqueVariable;
use Mobile\Measure\Entity\Variable\ValueHint;
use Mobile\Measure\Entity\Variable\ValueHintList;
use Mobile\Project\Entity\DataEntryProject;
use Mobile\Project\Entity\DesktopUser;
use Mobile\Project\Entity\Platform;
use Mobile\Project\Entity\Workpath;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;
use Webapp\FileManagement\Service\MobileXmlWriterService;

class MobileXmlWriterServiceTest extends AbstractXmlServiceTest
{
    public function testGenerateDataEntry(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlWriterService::class);

        $dataEntryProject = $this->createDataEntryProject();
        $dataEntryProject->setDataEntry($this->createDataEntry($dataEntryProject));

        $xml = $service->generateDataEntry($dataEntryProject);

        $this->assertXmlStringEqualsXmlFile(__DIR__.'/resources/mobile-xml-writer/data-entry.xml', $xml->getXml());
    }

    /**
     * Redmine 5253 : Retour mobile des projets de saisie avec liste de valeurs.
     */
    public function testGenerateDataEntryWithValuesList(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlWriterService::class);

        $dataEntryProject = $this->createDataEntryWithValuesListProject();
        $dataEntryProject->setDataEntry($this->createDataEntryWithValuesList($dataEntryProject));

        $xml = $service->generateDataEntry($dataEntryProject);

        $this->assertXmlStringEqualsXmlFile(__DIR__.'/resources/mobile-xml-writer/data-entry-with-values-list.xml', $xml->getXml());
    }

    public function testGenerateProject(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlWriterService::class);

        $xml = $service->generateDataEntryProject($this->createDataEntryProject());

        $this->assertXmlStringEqualsXmlFile(__DIR__.'/resources/mobile-xml-writer/project.xml', $xml->getXml());
    }

    private function createDataEntry(DataEntryProject $project): DataEntry
    {
        $stateCode4 = $project->getStateCodes()->get(3);
        \assert($stateCode4 instanceof StateCode);
        $device = $project->getPlatform()->getDevices()->first();
        \assert($device instanceof Device);
        $block = $device->getBlocks()->first();
        \assert($block instanceof Block);
        $unitParcel0 = $block->getUnitParcels()->get(0);
        $unitParcel1 = $block->getUnitParcels()->get(1);
        $unitParcel2 = $block->getUnitParcels()->get(2);
        \assert($unitParcel0 instanceof UnitParcel && $unitParcel1 instanceof UnitParcel && $unitParcel2 instanceof UnitParcel);
        $individual00 = $unitParcel0->getIndividuals()->get(0);
        $individual10 = $unitParcel1->getIndividuals()->get(0);
        $individual20 = $unitParcel2->getIndividuals()->get(0);
        $individual21 = $unitParcel2->getIndividuals()->get(1);
        $individual22 = $unitParcel2->getIndividuals()->get(2);
        \assert($individual00 instanceof Individual && $individual10 instanceof Individual && $individual20 instanceof Individual && $individual21 instanceof Individual && $individual22 instanceof Individual);
        $variable1 = $project->getUniqueVariables()->first();
        \assert($variable1 instanceof UniqueVariable);
        $variable2 = $project->getGeneratorVariables()->first();
        \assert($variable2 instanceof GeneratorVariable);
        $variable2gen1 = $variable2->getUniqueVariables()->first();
        \assert($variable2gen1 instanceof UniqueVariable);
        $date = \DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:52.000+00:00');

        $measure1 = Measure::buildValue($date, 'ras');
        $this->setEntityId($measure1);

        $measure2 = Measure::buildStateCode($date, $stateCode4);
        $this->setEntityId($measure2);

        $measure3 = Measure::buildValue($date, 'Ras');
        $this->setEntityId($measure3);

        $measure4 = Measure::buildValue($date, 'Fente verticale,  écorce rugueuse,  gros lenticelles');
        $this->setEntityId($measure4);

        $formField1 = FormField::build($variable1, $individual00);
        $this->setEntityId($formField1);
        $formField1->addMeasure($measure1);

        $formField2 = FormField::build($variable1, $individual10);
        $this->setEntityId($formField2);
        $formField2->addMeasure($measure2);

        $formField3 = FormField::build($variable1, $individual20);
        $this->setEntityId($formField3);
        $formField3->addMeasure($measure3);

        $formField4 = FormField::build($variable1, $individual21);
        $this->setEntityId($formField4);
        $formField4->addMeasure($measure4);

        $measure5 = Measure::buildValue($date, 'ras');
        $this->setEntityId($measure5);

        $measure6 = Measure::buildValue($date, 'Ras');
        $this->setEntityId($measure6);

        $measure7 = Measure::buildRepetition($date, 'Ras', 1);
        $this->setEntityId($measure7);

        $measure7b = Measure::buildRepetition($date, 'Ras', 2);
        $this->setEntityId($measure7b);

        $measure8 = Measure::buildRepetition($date, 'Fente verticale,  écorce rugueuse,  gros lenticelles', 1);
        $this->setEntityId($measure8);

        $measure9 = Measure::buildRepetition($date, 'Ras', 2);
        $this->setEntityId($measure9);

        $formField5 = FormField::build($variable2gen1, $individual00);
        $this->setEntityId($formField5);
        $formField5->addMeasure($measure5);

        $formField6 = FormField::build($variable2gen1, $individual10);
        $this->setEntityId($formField6);
        $formField6->addMeasure($measure6);

        $formField7 = FormField::build($variable2gen1, $individual20);
        $this->setEntityId($formField7);
        $formField7
            ->addMeasure($measure7)
            ->addMeasure($measure7b);

        $genField1 = GeneratedField::build();
        $genField1
            ->addFormField($formField7);

        $measurex1 = Measure::buildValue($date, 1);
        $this->setEntityId($measurex1);

        $formFieldx1 = FormField::build($variable2, $individual20);
        $formFieldx1
            ->addMeasure($measurex1)
            ->addGeneratedField($genField1);

        $formField8 = FormField::build($variable2gen1, $individual21);
        $this->setEntityId($formField8);
        $formField8
            ->addMeasure($measure8)
            ->addMeasure($measure9);

        $genField2 = GeneratedField::build();
        $genField2
            ->addFormField($formField8);

        $measurex2 = Measure::buildValue($date, 1);
        $this->setEntityId($measurex2);

        $formFieldx2 = FormField::build($variable2, $individual21);
        $formFieldx2
            ->addMeasure($measurex2)
            ->addGeneratedField($genField2);

        $session = Session::build($date, $date);
        $this->setEntityId($session);
        $session
            ->addFormField($formField1)
            ->addFormField($formField2)
            ->addFormField($formField3)
            ->addFormField($formField4)
            ->addFormField($formField5)
            ->addFormField($formField6)
            ->addFormField($formFieldx1)
            ->addFormField($formFieldx2);

        $dataEntry = DataEntry::build($date);
        $this->setEntityId($dataEntry);
        $dataEntry->addSession($session);

        return $dataEntry;
    }

    private function createDataEntryWithValuesList(DataEntryProject $project): DataEntry
    {
        $stateCode2 = $project->getStateCodes()->get(1);
        \assert($stateCode2 instanceof StateCode);
        $device = $project->getPlatform()->getDevices()->first();
        \assert($device instanceof Device);
        $block = $device->getBlocks()->first();
        \assert($block instanceof Block);
        $unitParcel0 = $block->getUnitParcels()->get(0);
        \assert($unitParcel0 instanceof UnitParcel);
        $individual0 = $unitParcel0->getIndividuals()->get(0);
        $individual1 = $unitParcel0->getIndividuals()->get(1);
        $individual2 = $unitParcel0->getIndividuals()->get(2);
        $individual3 = $unitParcel0->getIndividuals()->get(3);
        \assert($individual0 instanceof Individual && $individual1 instanceof Individual && $individual2 instanceof Individual && $individual3 instanceof Individual);
        $variable1 = $project->getUniqueVariables()->first();
        \assert($variable1 instanceof UniqueVariable);
        $date = \DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:52.000+00:00');

        $measure1 = Measure::buildValue($date, 'v1');
        $this->setEntityId($measure1);

        $measure2 = Measure::buildStateCode($date, $stateCode2);
        $this->setEntityId($measure2);

        $measure3 = Measure::buildValue($date, 'v2');
        $this->setEntityId($measure3);

        $measure4 = Measure::buildValue($date, 'v1');
        $this->setEntityId($measure4);

        $formField1 = FormField::build($variable1, $individual0)
            ->addMeasure($measure1);
        $this->setEntityId($formField1);

        $formField2 = FormField::build($variable1, $individual1)
            ->addMeasure($measure2);
        $this->setEntityId($formField2);

        $formField3 = FormField::build($variable1, $individual2)
            ->addMeasure($measure3);
        $this->setEntityId($formField3);

        $formField4 = FormField::build($variable1, $individual3)
            ->addMeasure($measure4);
        $this->setEntityId($formField4);

        $session = Session::build($date, $date)
            ->addFormField($formField1)
            ->addFormField($formField2)
            ->addFormField($formField3)
            ->addFormField($formField4);
        $this->setEntityId($session);

        $dataEntry = DataEntry::build($date)
            ->addSession($session);
        $this->setEntityId($dataEntry);

        return $dataEntry;
    }

    private function createDataEntryProject(): DataEntryProject
    {
        $user = new DesktopUser();
        $this->setEntityId($user);
        $user
            ->setEmail('dupont.robert@localhost')
            ->setFirstname('Robert')
            ->setName('Dupont')
            ->setLogin('rdupont');

        $stateCode1 = new StateCode();
        $this->setEntityId($stateCode1);
        $stateCode1
            ->setCode(-6)
            ->setLabel('Donnée Manquante')
            ->setDescription('Code Etat permanent : Donnée Manquante')
            ->setColor('RGB{256,255,224}');

        $stateCode2 = new StateCode();
        $this->setEntityId($stateCode2);
        $stateCode2
            ->setCode(-5)
            ->setLabel('Non notable')
            ->setDescription('observation n\'est pas possible car interférence avec un autre événement')
            ->setColor('RGB{0,0,1}');

        $stateCode3 = new StateCode();
        $this->setEntityId($stateCode3);
        $stateCode3
            ->setCode(-8)
            ->setLabel('Absent')
            ->setDescription('individu absent')
            ->setColor('RGB{0,0,1}');

        $stateCode4 = new StateCode();
        $this->setEntityId($stateCode4);
        $stateCode4
            ->setCode(-9)
            ->setLabel('Mort')
            ->setDescription('Code Etat permanent : Individu mort')
            ->setColor('RGB{255,0,0}')
            ->setPropagation(PathLevelEnum::INDIVIDUAL);

        $modality1 = new Modality('L26_12');
        $this->setEntityId($modality1);
        $modality2 = new Modality('L26_11');
        $this->setEntityId($modality2);
        $modality3 = new Modality('L26_10');
        $this->setEntityId($modality3);

        $factor1 = new Factor('Facteur 1');
        $this->setEntityId($factor1);
        $factor1
            ->addModality($modality1)
            ->addModality($modality2)
            ->addModality($modality3);

        $factor2 = new Factor('Facteur 2');
        $this->setEntityId($factor2);

        $factor3 = new Factor('Facteur 3');
        $this->setEntityId($factor3);

        $treatment1 = new Treatment();
        $this->setEntityId($treatment1);
        $treatment1
            ->setName('L26_12')
            ->setShortName('26_12')
            ->setRepetitions(1)
            ->addModality($modality1);

        $treatment2 = new Treatment();
        $this->setEntityId($treatment2);
        $treatment2
            ->setName('L26_11')
            ->setShortName('26_11')
            ->setRepetitions(1)
            ->addModality($modality2);

        $treatment3 = new Treatment();
        $this->setEntityId($treatment3);
        $treatment3
            ->setName('L26_10')
            ->setShortName('26_10')
            ->setRepetitions(1)
            ->addModality($modality3);

        $individual1 = new Individual('1');
        $this->setEntityId($individual1);
        $individual1
            ->setX(22)
            ->setY(1)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual2 = new Individual('2');
        $this->setEntityId($individual2);
        $individual2
            ->setX(22)
            ->setY(2)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'))
            ->setStateCode($stateCode4);

        $individual3 = new Individual('3');
        $this->setEntityId($individual3);
        $individual3
            ->setX(30)
            ->setY(34)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual4 = new Individual('4');
        $this->setEntityId($individual4);
        $individual4
            ->setX(30)
            ->setY(35)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual5 = new Individual('5');
        $this->setEntityId($individual5);
        $individual5
            ->setX(30)
            ->setY(36)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual6 = new Individual('6');
        $this->setEntityId($individual6);
        $individual6
            ->setX(30)
            ->setY(37)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $unitParcel1 = new UnitParcel('1');
        $this->setEntityId($unitParcel1);
        $unitParcel1
            ->setTreatment($treatment1)
            ->addIndividual($individual1);

        $unitParcel2 = new UnitParcel('2');
        $this->setEntityId($unitParcel2);
        $unitParcel2
            ->setTreatment($treatment2)
            ->addIndividual($individual2);

        $unitParcel3 = new UnitParcel('3');
        $this->setEntityId($unitParcel3);
        $unitParcel3
            ->setTreatment($treatment3)
            ->addIndividual($individual3)
            ->addIndividual($individual4)
            ->addIndividual($individual5)
            ->addIndividual($individual6);

        $bloc = new Block('1');
        $this->setEntityId($bloc);
        $bloc
            ->addUnitParcel($unitParcel1)
            ->addUnitParcel($unitParcel2)
            ->addUnitParcel($unitParcel3);

        $protocol = new Protocol();
        $this->setEntityId($protocol);
        $protocol
            ->setName('Mon protocole')
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-05-24T08:23:46.000+00:00'))
            ->setCreator($user)
            ->setAim('')
            ->setAlgorithm('Sans Tirage')
            ->addFactor($factor1)
            ->addFactor($factor2)
            ->addFactor($factor3)
            ->addTreatment($treatment1)
            ->addTreatment($treatment2)
            ->addTreatment($treatment3);

        $device = new Device();
        $this->setEntityId($device);
        $device
            ->setName('Mon dispositif')
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-05-24T08:23:46.000+00:00'))
            ->setValidationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-12-22T10:30:56.000+00:00'))
            ->setIndividualPU(true)
            ->setProtocol($protocol)
            ->addBlock($bloc);

        $platform = new Platform();
        $this->setEntityId($platform);
        $platform
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-04-15T10:36:31.000+00:00'))
            ->setName('Ma plateforme')
            ->setSite('Bloc L')
            ->setPlace('Toulenne')
            ->addDevice($device);

        $variable1 = new UniqueVariable();
        $this->setEntityId($variable1);
        $variable1
            ->setName('observat')
            ->setShortName('obs')
            ->setRepetitions(1)
            ->setActive(true)
            ->setAskTimestamp(true)
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2021-10-14T10:27:17.000+00:00'))
            ->setModificationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:50:12.000+00:00'))
            ->setOrder(2)
            ->setMandatory(true)
            ->setPathLevel(PathLevelEnum::INDIVIDUAL)
            ->setType(VariableTypeEnum::ALPHANUMERIC);

        $variable2 = GeneratorVariable::build('genv');
        $this->setEntityId($variable2);
        $variable2
            ->setName('generator')
            ->setShortName('gen')
            ->setRepetitions(1)
            ->setActive(true)
            ->setAskTimestamp(true)
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2021-10-14T10:27:17.000+00:00'))
            ->setModificationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:50:12.000+00:00'))
            ->setOrder(2)
            ->setMandatory(true)
            ->setPathLevel(PathLevelEnum::INDIVIDUAL)
            ->setType(VariableTypeEnum::ALPHANUMERIC);

        $variable2gen1 = new UniqueVariable();
        $this->setEntityId($variable2gen1);
        $variable2gen1
            ->setName('generator_v1')
            ->setShortName('genv1')
            ->setRepetitions(2)
            ->setActive(true)
            ->setAskTimestamp(true)
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2021-10-14T10:27:17.000+00:00'))
            ->setModificationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:50:12.000+00:00'))
            ->setOrder(2)
            ->setMandatory(true)
            ->setPathLevel(PathLevelEnum::INDIVIDUAL)
            ->setType(VariableTypeEnum::ALPHANUMERIC);
        $variable2->addUniqueVariable($variable2gen1);

        $variable2gen2 = new UniqueVariable();
        $this->setEntityId($variable2gen2);
        $variable2gen2
            ->setName('generator_v2')
            ->setShortName('genv2')
            ->setRepetitions(1)
            ->setActive(true)
            ->setAskTimestamp(true)
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2021-10-14T10:27:17.000+00:00'))
            ->setModificationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:50:12.000+00:00'))
            ->setOrder(2)
            ->setMandatory(true)
            ->setPathLevel(PathLevelEnum::INDIVIDUAL)
            ->setType(VariableTypeEnum::ALPHANUMERIC);
        $variable2->addUniqueVariable($variable2gen2);

        $workpath = new Workpath();
        $this->setEntityId($workpath);
        $workpath
            ->setUsername($user->getLogin())
            ->setPath($individual1->getUri());

        $dataEntryProject = DataEntryProject::build(
            'Mon projet de saisie',
            \DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:38:56.000+00:00')
        );
        $this->setEntityId($dataEntryProject);
        $dataEntryProject
            ->setCreator($user)
            ->addDesktopUser($user)
            ->setPlatform($platform)
            ->addVariable($variable1)
            ->addVariable($variable2)
            ->addStateCode($stateCode1)
            ->addStateCode($stateCode2)
            ->addStateCode($stateCode3)
            ->addStateCode($stateCode4)
            ->addWorkpath($workpath);

        return $dataEntryProject;
    }

    private function createDataEntryWithValuesListProject(): DataEntryProject
    {
        $user = DesktopUser::build('Dupont', 'Robert', 'dupont.robert@localhost', 'rdupont');
        $this->setEntityId($user);

        $stateCode1 = StateCode::build(-6, 'Donnée Manquante');
        $this->setEntityId($stateCode1);

        $stateCode2 = StateCode::build(-9, 'Mort')
            ->setPropagation(PathLevelEnum::INDIVIDUAL);
        $this->setEntityId($stateCode2);

        $modality1 = Modality::build('L26_12');
        $this->setEntityId($modality1);

        $factor1 = Factor::build('Facteur 1', [$modality1]);
        $this->setEntityId($factor1);

        $treatment1 = Treatment::build('L26_12', '26_12', [$modality1])
            ->setRepetitions(1);
        $this->setEntityId($treatment1);

        $individual1 = new Individual('1');
        $this->setEntityId($individual1);
        $individual1
            ->setX(22)
            ->setY(1)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual2 = new Individual('2');
        $this->setEntityId($individual2);
        $individual2
            ->setX(22)
            ->setY(2)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'))
            ->setStateCode($stateCode2);

        $individual3 = new Individual('3');
        $this->setEntityId($individual3);
        $individual3
            ->setX(30)
            ->setY(34)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $individual4 = new Individual('4');
        $this->setEntityId($individual4);
        $individual4
            ->setX(30)
            ->setY(35)
            ->setApparitionDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-01-06T04:53:49.000+00:00'));

        $unitParcel1 = new UnitParcel('1');
        $this->setEntityId($unitParcel1);
        $unitParcel1
            ->setTreatment($treatment1)
            ->addIndividual($individual1)
            ->addIndividual($individual2)
            ->addIndividual($individual3)
            ->addIndividual($individual4);

        $block = new Block('1');
        $this->setEntityId($block);
        $block
            ->addUnitParcel($unitParcel1);

        $protocol = new Protocol();
        $this->setEntityId($protocol);
        $protocol
            ->setName('Mon protocole')
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-05-24T08:23:46.000+00:00'))
            ->setCreator($user)
            ->setAim('')
            ->setAlgorithm('Sans Tirage')
            ->addFactor($factor1)
            ->addTreatment($treatment1);

        $device = new Device();
        $this->setEntityId($device);
        $device
            ->setName('Mon dispositif')
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-05-24T08:23:46.000+00:00'))
            ->setValidationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2023-12-22T10:30:56.000+00:00'))
            ->setIndividualPU(true)
            ->setProtocol($protocol)
            ->addBlock($block);

        $platform = new Platform();
        $this->setEntityId($platform);
        $platform
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-04-15T10:36:31.000+00:00'))
            ->setName('Ma plateforme')
            ->setSite('Bloc L')
            ->setPlace('Toulenne')
            ->addDevice($device);

        $valueHint1 = ValueHint::build('v1');
        $this->setEntityId($valueHint1);
        $valueHint2 = ValueHint::build('v2');
        $this->setEntityId($valueHint2);
        $valueHintList = ValueHintList::build('test', [$valueHint1, $valueHint2]);
        $this->setEntityId($valueHintList);

        $variable1 = new UniqueVariable();
        $this->setEntityId($variable1);
        $variable1
            ->setName('observat')
            ->setShortName('obs')
            ->setRepetitions(1)
            ->setActive(true)
            ->setAskTimestamp(true)
            ->setCreationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2021-10-14T10:27:17.000+00:00'))
            ->setModificationDate(\DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:50:12.000+00:00'))
            ->setOrder(2)
            ->setMandatory(true)
            ->setPathLevel(PathLevelEnum::INDIVIDUAL)
            ->setType(VariableTypeEnum::ALPHANUMERIC)
            ->setValueHintList($valueHintList);

        $workpath = Workpath::build($individual1->getUri(), $user->getLogin());
        $this->setEntityId($workpath);

        $dataEntryProject = DataEntryProject::build(
            'Mon projet de saisie',
            \DateTime::createFromFormat(\DateTimeInterface::RFC3339_EXTENDED, '2022-06-01T08:38:56.000+00:00')
        );
        $this->setEntityId($dataEntryProject);
        $dataEntryProject
            ->setCreator($user)
            ->addDesktopUser($user)
            ->setPlatform($platform)
            ->addVariable($variable1)
            ->addStateCode($stateCode1)
            ->addStateCode($stateCode2)
            ->addWorkpath($workpath);

        return $dataEntryProject;
    }
}
