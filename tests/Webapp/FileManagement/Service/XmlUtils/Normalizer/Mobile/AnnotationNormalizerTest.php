<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Device\Entity\Block;
use Mobile\Measure\Entity\Annotation;
use Symfony\Component\Serializer\SerializerInterface;
use Tests\Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractNormalizerTest;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile\AnnotationNormalizer;
use Webapp\FileManagement\Service\XmlUtils\ReaderHelper;

class AnnotationNormalizerTest extends AbstractNormalizerTest
{
    private const XML_CONTENT = <<<XML
        <?xml version="1.0" encoding="UTF-8"?>
        <root nom="Test" date="2024-12-19T10:23:35.000+00:00" nature="texte" donnee="Texte" objetMetier="/4/@dispositifs.0/@blocs.0">
        <categorie>c1</categorie>
        <categorie>c2</categorie>
        <motscles>m1</motscles>
        <motscles>m2</motscles>
        </root>
        XML;

    public function testSupportsDenormalization(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $annotationNormalizer = $container->get(AnnotationNormalizer::class);
        $supports = $annotationNormalizer->supportsDenormalization(self::XML_CONTENT, Annotation::class, self::XML, $this->getMobileReaderContext());

        $this->assertTrue($supports);
    }

    public function testDenormalize(): void
    {
        self::bootKernel();
        $container = static::getContainer();

        $mobileReaderContext = $this->getMobileReaderContext();
        /** @var ReaderHelper $readerHelper */
        $target = new Block();
        $readerHelper = $mobileReaderContext[AbstractXmlNormalizer::READER_HELPER];
        $readerHelper->add('/4/@dispositifs.0/@blocs.0', $target);

        $serializer = $container->get(SerializerInterface::class);
        $model = $serializer->deserialize(self::XML_CONTENT, Annotation::class, self::XML, $mobileReaderContext);

        $this->assertInstanceOf(Annotation::class, $model);
        $this->assertSame('Test', $model->getName());
        $this->assertSame(Annotation::ANNOT_TYPE_TEXT, $model->getType());
        $this->assertSame('Texte', $model->getValue());
        $this->assertSame($target, $model->getProjectObject());
        $this->assertSame('2024-12-19T10:23:35.000+00:00', $model->getTimestamp()->format(\DateTime::RFC3339_EXTENDED));
        $this->assertSame(['c1', 'c2'], $model->getCategories());
        $this->assertSame(['m1', 'm2'], $model->getKeywords());
    }
}
