<?php

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service\XmlUtils\Normalizer;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile\CommonDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile\StateCodeContext;

abstract class AbstractNormalizerTest extends KernelTestCase
{
    protected const XML = 'xml';

    protected function getMobileWriterContext(): array
    {
        return [
        ];
    }

    protected function getMobileReaderContext(): array
    {
        $context = [
            CommonDtoNormalizer::STATE_CODES => new StateCodeContext(),
            XmlEncoder::AS_COLLECTION => true,
            XmlEncoder::ENCODING => 'UTF-8',
            DateTimeNormalizer::FORMAT_KEY => \DateTimeInterface::RFC3339_EXTENDED,
            AbstractXmlNormalizer::IMPORT_XML => true,
        ];
        AbstractXmlNormalizer::initDenormalizeContext($context);

        return $context;
    }
}
