<?php

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service;

use Webapp\FileManagement\Dto\Mobile\ProjectDto;
use Webapp\FileManagement\Dto\Mobile\ReturnFileDto;
use Webapp\FileManagement\Service\MobileXmlReaderService;

class MobileXmlReaderServiceTest extends AbstractXmlServiceTest
{
    public function testReadProjectFile(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlReaderService::class);

        $projectDto = $service->readProjectFile(file_get_contents(__DIR__.'/resources/mobile-xml-reader/project.xml'));

        $this->assertInstanceOf(ProjectDto::class, $projectDto);

        $this->assertNotNull($projectDto->dataEntryProject);
        $this->assertNull($projectDto->dataEntryProject->getDataEntry());
    }

    public function testReadReturnFile(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlReaderService::class);

        $returnFileDto = $service->readReturnFile(
            file_get_contents(__DIR__.'/resources/mobile-xml-reader/return-file.xml'),
            'Ma platforme',
            'request1'
        );

        $this->assertInstanceOf(ReturnFileDto::class, $returnFileDto);

        $this->assertNotNull($returnFileDto->dataEntryProject);
        $this->assertNotNull($returnFileDto->dataEntryProject->getDataEntry());
    }

    public function testReadReturnFileWithMetadonnees(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlReaderService::class);

        $returnFileDto = $service->readReturnFile(
            file_get_contents(__DIR__.'/resources/mobile-xml-reader/return-file-with-metadonnees.xml'),
            'Ma platforme',
            'request1'
        );

        $this->assertInstanceOf(ReturnFileDto::class, $returnFileDto);

        $this->assertNotNull($returnFileDto->dataEntryProject);
        $this->assertCount(1, $returnFileDto->dataEntryProject->getPlatform()->getDevices()->get(0)->getBlocks()->get(0)->getAnnotations());
    }

    public function testReadReturnFileWithIndividualStateCode(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlReaderService::class);

        $returnFileDto = $service->readReturnFile(
            file_get_contents(__DIR__.'/resources/mobile-xml-reader/return-file-with-individual-state-code.xml'),
            'Ma platforme',
            'request1'
        );

        $this->assertInstanceOf(ReturnFileDto::class, $returnFileDto);

        $this->assertNotNull($returnFileDto->dataEntryProject);
        $platform = $returnFileDto->dataEntryProject->getPlatform();
        $this->assertCount(1, $platform->getDevices());
        $device = $platform->getDevices()->get(0);
        $this->assertCount(1, $device->getBlocks());
        $block = $device->getBlocks()->get(0);
        $this->assertCount(1, $block->getUnitParcels());
        $unitParcel = $block->getUnitParcels()->get(0);
        $this->assertNotNull($unitParcel->getStateCode());
        $this->assertNotNull($unitParcel->getStateCode()->getProject());
        $this->assertCount(1, $unitParcel->getIndividuals());
        $individual = $unitParcel->getIndividuals()->get(0);
        $this->assertNotNull($individual->getStateCode());
        $this->assertNotNull($individual->getStateCode()->getProject());
    }

    public function testReadWritedReturnFile(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(MobileXmlReaderService::class);

        $returnFileDto = $service->readReturnFile(
            file_get_contents(__DIR__.'/resources/mobile-xml-writer/data-entry.xml'),
            'Ma plateforme',
            'request1'
        );

        $this->assertInstanceOf(ReturnFileDto::class, $returnFileDto);

        $this->assertNotNull($returnFileDto->dataEntryProject);
        $dataEntry = $returnFileDto->dataEntryProject->getDataEntry();
        $this->assertNotNull($dataEntry);
        $this->assertCount(1, $dataEntry->getSessions());
        $session = $dataEntry->getSessions()->get(0);
        $this->assertNotNull($session);
        $this->assertCount(8, $session->getFormFields());
        $formField = $session->getFormFields()->get(1);
        $this->assertNotNull($formField);
        $this->assertCount(1, $formField->getMeasures());
        $measure = $formField->getMeasures()->get(0);
        $this->assertNotNull($measure);
        $stateCode = $measure->getState();
        $this->assertNotNull($stateCode);
        $this->assertNotNull($stateCode->getProject());
    }
}
