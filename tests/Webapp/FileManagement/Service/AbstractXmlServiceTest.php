<?php

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service;

use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Security;

abstract class AbstractXmlServiceTest extends KernelTestCase
{
    private static int $id = 0;

    protected const CURRENT_DATE = '2024/01/01 00:00:00';

    private ?User $user = null;

    protected function getUser(): User
    {
        if (null === $this->user) {
            $this->user = new User();
            $this->setEntityId($this->user);
            $this->user
                ->setName('Robert')
                ->setSurname('Dupont')
                ->setEmail('dupont.robert@localhost')
                ->setUsername('rdupont');
        }

        return $this->user;
    }

    protected function mockServices(): void
    {
        $security = $this->createMock(Security::class);
        $security
            ->expects(self::any())
            ->method('getUser')
            ->willReturn($this->getUser());
        static::getContainer()->set(Security::class, $security);
    }

    /**
     * Initialiser un id doctrine sur une entité.
     *
     * @return int l'id généré
     */
    protected function setEntityId(IdentifiedEntity $entity): int
    {
        $id = self::$id++;
        $reflectionClass = new \ReflectionClass($entity);
        $reflectionProperty = $reflectionClass->getProperty('id');
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($entity, $id);
        $reflectionProperty->setAccessible(false);

        return $id;
    }
}
