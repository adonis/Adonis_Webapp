<?php

/*
 * @author TRYDEA - 2024
 */

namespace Tests\Webapp\FileManagement\Service;

use Shared\Authentication\Entity\Site;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;
use Webapp\FileManagement\Dto\Webapp\ExperimentDto;
use Webapp\FileManagement\Dto\Webapp\PlatformDto;
use Webapp\FileManagement\Dto\Webapp\VariableCollectionDto;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\WebappXmlReaderService;

class WebappXmlReaderServiceTest extends AbstractXmlServiceTest
{
    public function testReadPlatform(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $platformDto = $service->readPlatform(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/platform.xml'),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(PlatformDto::class, $platformDto);

        $this->assertNotNull($platformDto->platform);

        $experiments = $platformDto->platform->getExperiments();
        $this->assertCount(1, $experiments);

        /** @var Experiment $experiment */
        $experiment = $experiments->first();
        $this->assertCount(3, $experiment->getProtocol()->getFactors());
        $this->assertCount(3, $experiment->getProtocol()->getFactors()->first()->getModalities());
        $this->assertCount(3, $experiment->getProtocol()->getTreatments());
        $this->assertCount(1, $experiment->getProtocol()->getTreatments()->first()->getModalities());
        $this->assertCount(1, $experiment->getBlocks());
        $this->assertCount(3, $experiment->getBlocks()->first()->getUnitPlots());
        $this->assertCount(4, $experiment->getBlocks()->first()->getUnitPlots()->last()->getIndividuals());
    }

    public function testReadExperiment(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $experimentDto = $service->readExperiment(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/experiment.xml'),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(ExperimentDto::class, $experimentDto);

        $this->assertNotNull($experimentDto->experiment);
    }

    public function testReadVariableCollection1(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $variableCollectionDto = $service->readVariableCollection(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/variable-collection-1.xml'),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(VariableCollectionDto::class, $variableCollectionDto);

        $this->assertCount(1, $variableCollectionDto->materiels);
        $this->assertCount(2, $variableCollectionDto->variables);

        $variable1 = $variableCollectionDto->variables[0];
        $this->assertInstanceOf(SimpleVariable::class, $variable1);
        $this->assertSame('Commentaire_Ind', $variable1->getName());
        $this->assertSame('com', $variable1->getShortName());
        $this->assertSame(1, $variable1->getRepetitions());
        $this->assertSame('2024-06-18T11:25:38.000+00:00', $variable1->getCreated()->format(\DateTimeInterface::RFC3339_EXTENDED));
        $this->assertFalse($variable1->isMandatory());
        $this->assertSame(PathLevelEnum::INDIVIDUAL, $variable1->getPathLevel());
        $this->assertSame(VariableTypeEnum::ALPHANUMERIC, $variable1->getType());
        $this->assertFalse($variable1->getDefaultTrueValue());

        $variable2 = $variableCollectionDto->variables[1];
        $this->assertInstanceOf(SemiAutomaticVariable::class, $variable2);
        $this->assertNotNull($variable2->getDevice());
    }

    public function testReadVariableCollection2(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $variableCollectionDto = $service->readVariableCollection(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/variable-collection-2.xml'),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(VariableCollectionDto::class, $variableCollectionDto);

        $this->assertNotEmpty($variableCollectionDto->variables);
    }

    private function createSite(): Site
    {
        $site = new Site();
        $this->setEntityId($site, 1);

        return $site;
    }

    /**
     * @dataProvider providePlatformXmlExemples
     */
    public function testReadPlatformXmlExemple(string $xmlFile): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $platformDto = $service->readPlatform(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/'.$xmlFile),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(PlatformDto::class, $platformDto);
        $this->assertNotNull($platformDto->platform);

        $this->assertCount(2, $platformDto->platform->getProjects());
        $project = $platformDto->platform->getProjects()->get(0);
        foreach ($project->getVariables() as $variable) {
            foreach ($variable->getTests() as $test) {
                $this->assertNotNull($test->getType());
            }
        }

        $this->assertCount(3, $project->getProjectDatas());
        $projectData1 = $project->getProjectDatas()->get(0);
        $this->assertSame($project->getName(), $projectData1->getName());

        $this->assertCount(1, $projectData1->getSessions());
        $session1 = $projectData1->getSessions()->get(0);

        $this->assertCount(390, $session1->getFieldMeasures());
    }

    public function providePlatformXmlExemples(): array
    {
        return [
            ['test-ornans-melezes-13011.xml'],
        ];
    }

    /**
     * @dataProvider provideCollectionVariablesExemples
     */
    public function testReadVariableCollectionExemples(string $file, string $workingDir): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $variableCollectionDto = $service->readVariableCollection(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/'.$file),
            $this->createSite(),
            $this->getUser(),
            __DIR__.'/resources/webapp-xml-reader/'.$workingDir
        );

        $this->assertInstanceOf(VariableCollectionDto::class, $variableCollectionDto);

        $this->assertNotEmpty($variableCollectionDto->variables);
    }

    public function provideCollectionVariablesExemples(): array
    {
        return [
            ['test-variable-balance-pm4800.xml', 'empty-workdir'],
            ['test-variable-debourrement.xml', 'empty-workdir'],
            ['test-variable-stade-debourrement/stade-debourrement.xml', 'test-variable-stade-debourrement'],
        ];
    }

    /**
     * @dataProvider provideCollectionVariablesExceptionsExemples
     */
    public function testReadVariableCollectionExceptionsExemples(string $file, string $workingDir): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $this->expectException(ParsingException::class);

        $service->readVariableCollection(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/'.$file),
            $this->createSite(),
            $this->getUser(),
            __DIR__.'/resources/webapp-xml-reader/'.$workingDir
        );
    }

    public function provideCollectionVariablesExceptionsExemples(): array
    {
        return [
            ['test-variables.xml', 'empty-workdir'],
        ];
    }

    public function testReadPlatformNatureZhe(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $platformDto = $service->readPlatform(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/platform_with_zhe.xml'),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(PlatformDto::class, $platformDto);

        $this->assertNotNull($platformDto->platform);

        $experiments = $platformDto->platform->getExperiments();
        $this->assertCount(1, $experiments);

        /* @var Experiment $experiment */
        $experiment = $experiments->first();
        $this->assertCount(2, $experiment->getOutExperimentationZones());
    }

    /**
     *  Redmine #5253.
     */
    public function testReadExperimentListeValeurs(): void
    {
        $this->mockServices();

        $service = static::getContainer()->get(WebappXmlReaderService::class);

        $platformDto = $service->readPlatform(
            file_get_contents(__DIR__.'/resources/webapp-xml-reader/test-retour-avec-liste-valeurs.xml'),
            $this->createSite(),
            $this->getUser()
        );

        $this->assertInstanceOf(PlatformDto::class, $platformDto);

        $this->assertNotNull($platformDto->platform);

        $project = $platformDto->platform->getProjects()->get(0);
        $this->assertNotNull($project);
        $variable = $project->getVariables()[0] ?? null;
        self::assertInstanceOf(SimpleVariable::class, $variable);
        self::assertNotNull($variable->getValueList());

        $expectedArray = ['mildiou', 'alternariose', 'anthracnose', 'noctuelle', 'doryphore', 'botrytis', 'chlorose', 'oïdium', 'nécrose apicale', 'pourriture grise'];
        $values = $variable->getValueList()->getValues();
        self::assertSame(\count($expectedArray), $values->count());
        foreach ($expectedArray as $i => $expected) {
            self::assertSame($expected, $values->get($i)->getName());
        }
    }
}
