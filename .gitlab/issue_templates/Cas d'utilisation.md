# Description du cas d'utilisation

En tant que _Role-Utilisateur_, j'aimerais pouvoir _TODO_

# Validation du besoin

| Fait | Point concerné | Justification |
|:----:|:---------------|:--------------|
| :stop_sign:  | Désirable         | _Besoin client ou technique_ |
| :stop_sign:  | Décomposée        | _Affinage réalisé et liens vers les issues remplis_ |
| :stop_sign:  | Dérisquée         | _Quote vers la spécification bien définie si changement par rapport à l'existant_ |

