#!/bin/sh
set -e

echo "---- WAITING FOR DATABASE TO BE READY ----"
ATTEMPTS_LEFT_TO_REACH_DATABASE=60
until [ $ATTEMPTS_LEFT_TO_REACH_DATABASE -eq 0 ] || DATABASE_ERROR=$(php bin/console doctrine:query:sql -q "SELECT 1" 2>&1); do
  if [ $? -eq 255 ]; then
    # If the Doctrine command exits with 255, an unrecoverable error occurred
    ATTEMPTS_LEFT_TO_REACH_DATABASE=0
    break
  fi
  sleep 1
  ATTEMPTS_LEFT_TO_REACH_DATABASE=$((ATTEMPTS_LEFT_TO_REACH_DATABASE - 1))
  echo "---- STILL WAITING FOR DATABASE TO BE READY... OR MAYBE THE DATABASE IS NOT REACHABLE $ATTEMPTS_LEFT_TO_REACH_DATABASE ATTEMPTS LEFT ----"
done

if [ $ATTEMPTS_LEFT_TO_REACH_DATABASE -eq 0 ]; then
  echo "---- THE DATABASE IS NOT UP OR NOT REACHABLE ----"
  echo "$DATABASE_ERROR"
  exit 1
else
  echo "---- THE DATABASE IS NOW READY AND REACHABLE ----"
fi

echo "---- EXECUTING MIGRATIONS ----"
php bin/console --no-interaction doctrine:migrations:migrate

echo "*************** END OF ENTRYPOINT ***************"

exec docker-php-entrypoint "$@"
