#!/bin/sh
set -e

if [ "$1" = 'npm' ] || [ "$2" = 'run' ] || [ "$3" = 'dev-server' ]; then
  npm install
fi

exec docker-entrypoint.sh "$@"