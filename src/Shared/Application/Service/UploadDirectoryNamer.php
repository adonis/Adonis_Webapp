<?php

namespace Shared\Application\Service;

use DateTime;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Exception\NameGenerationException;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

/**
 * Class FileDateDirectoryNamer
 *
 * @template-implements DirectoryNamerInterface<\Shared\Application\Entity\File>
 */
class UploadDirectoryNamer implements DirectoryNamerInterface
{

    /**
     * @param \Shared\Application\Entity\File $object
     * @param PropertyMapping $mapping
     *
     * @return string
     * @throws NameGenerationException
     */
    public function directoryName($object, PropertyMapping $mapping): string
    {
        if ($object instanceof \Shared\Application\Entity\File) {
            return 'upload/' . $object->getDate()->format('Y/m/d/');
        } elseif ($object instanceof File) {
            $date = new DateTime();
            return 'upload/' . $date->format('Y/m/d/');
        }
        throw new NameGenerationException("Impossible de trouver le chemin de stockage");
    }

}
