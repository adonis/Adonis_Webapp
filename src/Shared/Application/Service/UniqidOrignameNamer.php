<?php

namespace Shared\Application\Service;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

/**
 * @template-implements NamerInterface<\Shared\Application\Entity\File>
 */
class UniqidOrignameNamer implements NamerInterface
{

    public function name($object, PropertyMapping $mapping): string
    {
        if ($object instanceof File) {
            $filename = $object->getFilename();
        } else {
            $filename = $mapping->getFile($object)->getClientOriginalName();
        }
        setlocale(LC_CTYPE, 'fr_FR.UTF-8');
        $filename = iconv('UTF-8', 'ASCII//TRANSLIT', $filename);
        $filename = preg_replace('/\s/', '_', $filename);
        return uniqid() . '_' . $filename;
    }

}
