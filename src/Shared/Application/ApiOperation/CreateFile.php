<?php

namespace Shared\Application\ApiOperation;

use DateTime;
use Shared\Application\Entity\File;
use Shared\Authentication\Entity\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;

class CreateFile
{
    public function __invoke(Request $request, Security $security): File
    {
        /** @var $user User */
        $user = $security->getUser();
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        return (new File())
            ->setFile($uploadedFile)
            ->setFileName($uploadedFile->getFilename())
            ->setSize($uploadedFile->getSize())
            ->setOriginalFileName($uploadedFile->getClientOriginalName())
            ->setDate(new DateTime())
            ->setOwner($user);
    }
}
