<?php

namespace Shared\Application\Entity;

use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     },
 *     itemOperations={
 *         "get"={}
 *     }
 * )
 */
class Env
{
    /**
     * @var mixed
     */
    private $id = null;

    /**
     * @var mixed
     */
    private $ldapOn = null;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getLdapOn()
    {
        return $this->ldapOn;
    }

    /**
     * @param mixed $ldapOn
     */
    public function setLdapOn($ldapOn): void
    {
        $this->ldapOn = $ldapOn;
    }

}
