<?php

namespace Shared\Application\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Application\ApiOperation\CreateFile;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Shared\RightManagement\Traits\HasOwnerEntity;
use Symfony\Component\HttpFoundation\File\File as SymfonyHttpFile;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateFile::class,
 *             "deserialize"=false,
 *             "access_control"="is_granted('ROLE_USER')",
 *             "validation_groups"={"Default"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     },
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *     },
 *     itemOperations={
 *         "get"={ "is_granted('ROLE_USER') && security"="object.getUser() == user" },
 *     }
 * )
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="file", schema="shared")
 *
 * @Gedmo\SoftDeleteable()
 *
 * @Vich\Uploadable()
 */
class File extends IdentifiedEntity
{
    use HasOwnerEntity;
    use SoftDeleteableEntity;

    /**
     * @Assert\NotNull()
     *
     * @Assert\File(maxSize="10M")
     *
     * @Vich\UploadableField(mapping="attachments")
     */
    protected ?SymfonyHttpFile $file = null;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"attachment_read"})
     */
    private ?string $fileName = null;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"attachment_read"})
     */
    private ?string $originalFileName = null;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"attachment_read"})
     */
    private ?int $size = null;

    /**
     * @ORM\Column(type="date")
     *
     * @Groups({"attachment_read"})
     */
    private ?\DateTime $date = null;

    /**
     * @psalm-mutation-free
     */
    public function getFile(): ?SymfonyHttpFile
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function setFile(?SymfonyHttpFile $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    /**
     * @return $this
     */
    public function setFileName(?string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOriginalFileName(): ?string
    {
        return $this->originalFileName;
    }

    /**
     * @return $this
     */
    public function setOriginalFileName(?string $originalFileName): self
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @return $this
     */
    public function setSize(?int $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @return $this
     */
    public function setDate(?\DateTime $date): File
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return $this
     */
    public function setOwner(?User $owner): File
    {
        $this->owner = $owner;

        return $this;
    }
}
