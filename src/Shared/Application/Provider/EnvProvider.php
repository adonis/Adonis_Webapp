<?php

namespace Shared\Application\Provider;

use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Shared\Application\Entity\Env;
use Shared\RightManagement\Entity\AbstractAdvancedRight;

/**
 * Class AdvancedRightProvider
 * @package Shared\RightManagement\Provider
 */
class EnvProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private bool $ldapOn;
    public function __construct($ldapOn)
    {
        $this->ldapOn = $ldapOn;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $res = new Env();
        $res->setId(0);
        $res->setLdapOn($this->ldapOn);
        return [$res];
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return ($resourceClass === Env::class) && $operationName === 'get';
    }
}
