<?php

namespace Shared\Application\Controller;

use Shared\Application\Entity\File;
use Shared\Application\Voter\FileVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;

/**
 * Class FileController.
 */
class FileController extends AbstractController
{
    /**
     * @Route("/api/download/files/{file}", name="downloadFile")
     * @param File $file
     * @param DownloadHandler $downloadHandler
     * @return Response
     */
    public function downloadFile(File $file, DownloadHandler $downloadHandler): Response
    {
        $this->denyAccessUnlessGranted(FileVoter::DOWNLOAD, $file);
        $response = $downloadHandler->downloadObject($file, 'file');
        $response->headers->set('Content-Type', '*');
        return $response;
    }

}
