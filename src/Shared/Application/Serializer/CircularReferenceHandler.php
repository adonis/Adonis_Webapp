<?php

namespace Shared\Application\Serializer;

/**
 * Class CircularReferenceHandler.
 */
class CircularReferenceHandler
{
    public function __invoke($object)
    {
        return $object->getId();
    }
}
