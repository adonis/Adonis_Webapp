<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Enumeration\Annotation;

use Doctrine\Common\Annotations\Annotation\Required;
use Doctrine\ORM\Mapping\Annotation;

/**
 * Class EnumType
 * @package Shared\Enumeration\Annotation
 * @Annotation
 * @Target("PROPERTY")
 */
class EnumType implements Annotation
{
    /**
     * @Required()
     */
    public string $class;

    public bool $nullable = false;
}
