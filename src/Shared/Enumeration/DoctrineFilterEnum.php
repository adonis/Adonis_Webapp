<?php

namespace Shared\Enumeration;

/**
 * Enumeration for Doctrine Filters
 */
final class DoctrineFilterEnum extends BasicEnum
{
    public const SOFT_DELETEABLE = 'soft_deleteable';
}
