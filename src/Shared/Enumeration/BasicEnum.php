<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\Enumeration;

use ReflectionClass;
use ReflectionException;

/**
 * Class BasicEnum
 * @package Shared\Enumeration
 */
abstract class BasicEnum
{
    private static array $constCacheArray = [];

    /**
     * @throws ReflectionException
     */
    private static function getConstants()
    {
        $calledClass = get_called_class();
        if (!array_key_exists($calledClass, self::$constCacheArray)) {
            $reflect = new ReflectionClass($calledClass);
            self::$constCacheArray[$calledClass] = $reflect->getConstants();
        }
        return self::$constCacheArray[$calledClass];
    }

    /**
     * @throws ReflectionException
     */
    public static function isValidValue($value, $nullable = false, $strict = true): bool
    {
        $values = array_values(self::getConstants());
        return $nullable && null == $value || in_array($value, $values, $strict);
    }
}
