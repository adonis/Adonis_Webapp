<?php

/*
 * @author TRYDEA - 2024
 */

namespace Shared\Exception;

class NotInitializedPropertyException extends LogicException
{
    /**
     * @param class-string $class
     */
    public function __construct(string $class, string $property)
    {
        $message = \sprintf('Property "%s" is not initialized in class "%s".', $property, $class);

        parent::__construct($message);
    }
}
