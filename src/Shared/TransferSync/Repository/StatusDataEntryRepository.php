<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\TransferSync\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Mobile\Project\Entity\DataEntryProject;
use Shared\TransferSync\Entity\StatusDataEntry;
use Webapp\Core\Entity\Project;

/**
 * Class StatusDataEntryRepository.
 *
 * @method StatusDataEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method StatusDataEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method StatusDataEntry[] findAll()
 * @method StatusDataEntry[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusDataEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StatusDataEntry::class);
    }

    public function findOneByResponseProject(?DataEntryProject $responseProject)
    {
        $qb = $this->createQueryBuilder('sde');
        $qb->where(
            $qb->expr()->eq('sde.response', ':response_project')
        );
        $qb->setParameter('response_project', $responseProject);
        $qb->setMaxResults(1);
        try {
            $result = $qb->getQuery()->getSingleResult();
        } catch (Exception $exception) {
            $result = null;
        }
        return $result;
    }

    public function findBenchmarkByWebappProject(Project $project)
    {
        $qb = $this->createQueryBuilder('sde');

        $qb->join('sde.request', 'req')
            ->where($qb->expr()->andX(
                $qb->expr()->eq('req.benchmark', ':benchmark'),
                $qb->expr()->eq('sde.webappProject', ':webappProject')
            )
            );
        $qb->setParameter('benchmark', true);
        $qb->setParameter('webappProject', $project);
        return $qb->getQuery()->getResult();
    }
}
