<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\TransferSync\Service;

use Mobile\Project\Entity\DataEntryProject;
use Webapp\Core\Entity\Project as WebappProject;

/**
 * Interface WebappToMobileBridgeInterface
 * Service to allow transfer of Webapp Project into Mobile Project
 * @package Shared\TransferSync\Service
 */
interface WebappToMobileBridgeInterface
{
    /**
     * @param bool $isBenchmark
     * @param WebappProject $webappProject
     * @param array $users
     * @param array $webappIriToMobileItemMap
     * @return DataEntryProject
     */
    public function buildProjectFromWebappToMobile(bool $isBenchmark, WebappProject $webappProject, array $users, array &$webappIriToMobileItemMap): DataEntryProject;
}
