<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\TransferSync\Service;

use ApiPlatform\Core\Api\IriConverterInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\Device;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\Factor;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\Modality;
use Mobile\Device\Entity\Protocol;
use Mobile\Device\Entity\SubBlock;
use Mobile\Device\Entity\Treatment;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Measure\Entity\Test\CombinationTest;
use Mobile\Measure\Entity\Test\GrowthTest;
use Mobile\Measure\Entity\Test\PreconditionedCalculation;
use Mobile\Measure\Entity\Test\RangeTest;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\Driver;
use Mobile\Measure\Entity\Variable\GeneratorVariable;
use Mobile\Measure\Entity\Variable\Mark;
use Mobile\Measure\Entity\Variable\Material;
use Mobile\Measure\Entity\Variable\PreviousValue;
use Mobile\Measure\Entity\Variable\RequiredAnnotation;
use Mobile\Measure\Entity\Variable\Scale;
use Mobile\Measure\Entity\Variable\StateCode;
use Mobile\Measure\Entity\Variable\UniqueVariable;
use Mobile\Measure\Entity\Variable\ValueHint;
use Mobile\Measure\Entity\Variable\ValueHintList;
use Mobile\Project\Entity\DataEntryProject;
use Mobile\Project\Entity\DesktopUser;
use Mobile\Project\Entity\GraphicalStructure;
use Mobile\Project\Entity\Platform;
use Mobile\Project\Entity\Workpath;
use Shared\Authentication\Entity\User;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\Block as WebappBlock;
use Webapp\Core\Entity\Device as WebappDevice;
use Webapp\Core\Entity\Experiment as WebappExperiment;
use Webapp\Core\Entity\Factor as WebappFactor;
use Webapp\Core\Entity\GeneratorVariable as WebappGeneratorVariable;
use Webapp\Core\Entity\Individual as WebappIndividual;
use Webapp\Core\Entity\Modality as WebappModality;
use Webapp\Core\Entity\Note;
use Webapp\Core\Entity\PathUserWorkflow;
use Webapp\Core\Entity\Platform as WebappPlatform;
use Webapp\Core\Entity\Project as WebappProject;
use Webapp\Core\Entity\Protocol as WebappProtocol;
use Webapp\Core\Entity\RequiredAnnotation as WebappRequiredAnnotation;
use Webapp\Core\Entity\SemiAutomaticVariable as WebappSemiAutomaticVariable;
use Webapp\Core\Entity\SimpleVariable as WebappSimpleVariable;
use Webapp\Core\Entity\StateCode as WebappStateCode;
use Webapp\Core\Entity\SubBlock as WebappSubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot as WebappSurfaceUnitPlot;
use Webapp\Core\Entity\Test;
use Webapp\Core\Entity\Treatment as WebappTreatment;
use Webapp\Core\Entity\ValueList;
use Webapp\Core\Entity\ValueListItem;
use Webapp\Core\Entity\VariableScaleItem;
use Webapp\Core\Enumeration\GraphicalOriginEnum;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\SpreadingEnum;
use Webapp\Core\Enumeration\StopBitEnum;
use Webapp\Core\Enumeration\TestTypeEnum;
use Webapp\Core\Enumeration\VariableFormatEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;

/**
 * Class WebappToMobileBridge
 * Service to allow transfer of Webapp Project into Mobile Project
 * @package Mobile\Transfer\Service
 */
class WebappToMobileBridge implements WebappToMobileBridgeInterface
{
    private IriConverterInterface $iriConverter;
    private EntityManagerInterface $entityManager;
    private Security $security;

    public function __construct(
        IriConverterInterface  $iriConverter,
        EntityManagerInterface $entityManager,
        Security               $security
    )
    {
        $this->iriConverter = $iriConverter;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function buildProjectFromWebappToMobile(bool $isBenchmark, WebappProject $webappProject, array $users, array &$webappIriToMobileItemMap): DataEntryProject
    {
        $mobileProject = (new DataEntryProject())
            ->setCreationDate($webappProject->getCreated())
            ->setImprovised(false)
            ->setName($webappProject->getName())
            ->setBenchmark($isBenchmark);

        $stateCodeMap = [];
        foreach ($webappProject->getStateCodes() as $webappStateCode) {
            $mobileStateCode = $this->handleStateCode($webappStateCode, $stateCodeMap);
            $mobileProject->addStateCode($mobileStateCode);
        }

        foreach ($users as $user) {
            $mobileDesktopUser = $this->handleProjectDesktopUser($user, $webappIriToMobileItemMap);
            $mobileProject->addDesktopUser($mobileDesktopUser);
        }

        $mobilePlatform = $this->handlePlatform($webappProject->getPlatform(), $webappProject->getExperiments()->toArray(), $webappIriToMobileItemMap, $stateCodeMap);
        $mobileProject->setPlatform($mobilePlatform);

        $variableMap = [];
        $order = 0;
        foreach ($webappProject->getSimpleVariables() as $simpleVariable) {
            $mobileSimpleVariable = $this->handleSimpleVariable($simpleVariable, $variableMap, $order++, $stateCodeMap);
            $mobileProject->addUniqueVariable($mobileSimpleVariable);
        }
        foreach ($webappProject->getGeneratorVariables() as $generatorVariable) {
            $mobileGeneratorVariable = $this->handleGeneratorVariable($generatorVariable, $variableMap, $order++, $stateCodeMap, $webappIriToMobileItemMap);
            $mobileProject->addGeneratorVariable($mobileGeneratorVariable);
        }
        foreach ($webappProject->getDevices() as $device) {
            $material = $this->handleDevice($device, $variableMap, $stateCodeMap);
            $mobileProject->addMaterial($material);
            foreach ($material->getUniqueVariables() as $variable) {
                $mobileProject->addVariable($variable);
            }
        }

        // Second loop is needed because tests can point to other variables
        foreach ($webappProject->getSimpleVariables() as $simpleVariable) {
            foreach ($simpleVariable->getTests() as $test) {
                $this->handleTest($variableMap[$this->iriConverter->getIriFromItem($simpleVariable)], $test, $variableMap);
            }
        }
        foreach ($webappProject->getGeneratorVariables() as $generatorVariable) {
            foreach ($generatorVariable->getTests() as $test) {
                $this->handleTest($variableMap[$this->iriConverter->getIriFromItem($generatorVariable)], $test, $variableMap);
            }
        }

        // TODO: $naturesZHE dans mobile...

        foreach ($webappProject->getRequiredAnnotations() as $annotation) {
            $mobileAnnotation = $this->handleRequiredAnnotation($annotation);
            $mobileProject->addRequiredAnnotation($mobileAnnotation);
        }

        $graphicalStructure = $this->handleGraphicalStructure($webappProject->getPlatform());
        $mobileProject->setGraphicalStructure($graphicalStructure);

        if($isBenchmark){
            $mobileProject->setCreator($webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($this->security->getUser())]);
        } else {
            $mobileProject->setCreator($webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappProject->getOwner())]);
        }

        // Needed to give URI to the objects
        $this->entityManager->persist($mobileProject);
        if ($webappProject->getPathBase() !== null) {
            foreach ($webappProject->getPathBase()->getUserPaths() as $userPath) {
                $mobileUserPath = $this->handleUserPath($userPath, $webappIriToMobileItemMap);
                $mobileProject->addWorkpath($mobileUserPath);
            }
        }
        return $mobileProject;
    }

    private function handleProjectDesktopUser(User $webappUser, array &$webappIriToMobileItemMap): DesktopUser
    {
        $mobileDesktopUser = (new DesktopUser())
            ->setName($webappUser->getSurname())
            ->setFirstname($webappUser->getName())
            ->setLogin($webappUser->getUsername())
            ->setEmail($webappUser->getEmail())
            ->setPassword("NO");

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappUser)] = $mobileDesktopUser;
        return $mobileDesktopUser;
    }

    private function handlePlatform(WebappPlatform $webappPlatform, array $webappExperiments, array &$webappIriToMobileItemMap, array $stateCodeMap): Platform
    {
        $mobilePlatform = (new Platform())
            ->setName($webappPlatform->getName())
            ->setCreationDate($webappPlatform->getCreated())
            ->setSite($webappPlatform->getSiteName())
            ->setPlace($webappPlatform->getPlaceName());

        foreach ($webappExperiments as $webappExperiment) {
            $mobileExperiment = $this->handleExperiment($webappExperiment, $webappIriToMobileItemMap, $stateCodeMap);
            $mobilePlatform->addDevice($mobileExperiment);
        }

        return $mobilePlatform;
    }

    private function handleExperiment(WebappExperiment $webappExperiment, array &$webappIriToMobileItemMap, array $stateCodeMap): Device
    {
        $mobileExperiment = (new Device())
            ->setName($webappExperiment->getName())
            ->setCreationDate($webappExperiment->getCreated())
            ->setIndividualPU($webappExperiment->isIndividualUP())
            ->setAnomaly(null) // TODO: Utiliser la donnée de webapp
            ->setValidationDate($webappExperiment->getValidated()); // TODO: Utiliser la donnée de webapp

        $mobileProtocol = $this->handleProtocol($webappExperiment->getProtocol(), $webappIriToMobileItemMap);
        $mobileExperiment->setProtocol($mobileProtocol);

        foreach ($webappExperiment->getBlocks() as $webappBlock) {
            $mobileBlock = $this->handleBlock($webappBlock, $webappIriToMobileItemMap, $stateCodeMap);
            $mobileExperiment->addBlock($mobileBlock);
        }

        foreach ($webappExperiment->getNotes() as $note) {
            $mobileNote = $this->handleNote($note);
            $mobileExperiment->addNote($mobileNote);
        }

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappExperiment)] = $mobileExperiment;
        return $mobileExperiment;
    }

    private function handleProtocol(WebappProtocol $webappProtocol, array &$webappIriToMobileItemMap): Protocol
    {
        $mobileProtocol = (new Protocol())
            ->setCreationDate($webappProtocol->getCreated())
            ->setName($webappProtocol->getName())
            ->setAim($webappProtocol->getAim())
            ->setCreator($webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappProtocol->getOwner())] ??
                $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($this->security->getUser())])
            ->setAlgorithm("sans_tirage");
        foreach ($webappProtocol->getFactors() as $webappFactor) {
            $mobileFactor = $this->handleProtocolFactor($webappFactor, $webappIriToMobileItemMap);
            $mobileProtocol->addFactor($mobileFactor);
        }

        foreach ($webappProtocol->getTreatments() as $webappTreatment) {
            $mobileTreatment = $this->handleTreatment($webappTreatment, $webappIriToMobileItemMap);
            $mobileProtocol->addTreatment($mobileTreatment);
        }
        return $mobileProtocol;
    }

    private function handleProtocolFactor(WebappFactor $webappFactor, array &$webappIriToMobileItemMap): Factor
    {
        $mobileFactor = (new Factor())
            ->setName($webappFactor->getName());

        foreach ($webappFactor->getModalities() as $webappModality) {
            $mobileModality = $this->handleModality($webappModality, $webappIriToMobileItemMap);
            $mobileFactor->addModality($mobileModality);
        }
        return $mobileFactor;
    }

    private function handleModality(WebappModality $webappModality, array &$webappIriToMobileItemMap): Modality
    {
        $mobileModality = (new Modality())
            ->setValue($webappModality->getValue());

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappModality)] = $mobileModality;
        return $mobileModality;
    }

    private function handleTreatment(WebappTreatment $webappTreatment, array &$webappIriToMobileItemMap): Treatment
    {
        $mobileTreatment = (new Treatment())
            ->setName($webappTreatment->getName())
            ->setShortName($webappTreatment->getShortName())
            ->setRepetitions($webappTreatment->getRepetitions());

        foreach ($webappTreatment->getModalities() as $webappModality) {
            $mobileTreatment->addModality($webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappModality)]);
        }

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappTreatment)] = $mobileTreatment;
        return $mobileTreatment;
    }

    private function handleBlock(WebappBlock $webappBlock, array &$webappIriToMobileItemMap, array $stateCodeMap): Block
    {
        $mobileBlock = (new Block())
            ->setName($webappBlock->getNumber());

        foreach ($webappBlock->getSubBlocks() as $webappSubBlock) {
            $mobileSubBlock = $this->handleSubBlock($webappSubBlock, $webappIriToMobileItemMap, $stateCodeMap);
            $mobileBlock->addSubBlock($mobileSubBlock);
        }

        foreach ($webappBlock->getUnitPlots() as $webappUnitPlot) {
            $mobileUnitPlot = $this->handleUnitPlot($webappUnitPlot, $webappIriToMobileItemMap, $stateCodeMap);
            $mobileBlock->addUnitParcel($mobileUnitPlot);
        }

        foreach ($webappBlock->getSurfacicUnitPlots() as $webappUnitPlot) {
            $mobileUnitPlot = $this->handleUnitPlot($webappUnitPlot, $webappIriToMobileItemMap, $stateCodeMap);
            $mobileBlock->addUnitParcel($mobileUnitPlot);
        }

        foreach ($webappBlock->getNotes() as $note) {
            $mobileNote = $this->handleNote($note);
            $mobileBlock->addNote($mobileNote);
        }

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappBlock)] = $mobileBlock;
        return $mobileBlock;
    }

    private function handleSubBlock(WebappSubBlock $webappSubBlock, array &$webappIriToMobileItemMap, array $stateCodeMap): SubBlock
    {
        $mobileSubBlock = (new SubBlock())
            ->setName($webappSubBlock->getNumber());

        foreach ($webappSubBlock->getUnitPlots() as $webappUnitPlot) {
            $mobileUnitPlot = $this->handleUnitPlot($webappUnitPlot, $webappIriToMobileItemMap, $stateCodeMap);
            $mobileSubBlock->addUnitParcel($mobileUnitPlot);
        }

        foreach ($webappSubBlock->getSurfacicUnitPlots() as $webappUnitPlot) {
            $mobileUnitPlot = $this->handleUnitPlot($webappUnitPlot, $webappIriToMobileItemMap, $stateCodeMap);
            $mobileSubBlock->addUnitParcel($mobileUnitPlot);
        }

        foreach ($webappSubBlock->getNotes() as $note) {
            $mobileNote = $this->handleNote($note);
            $mobileSubBlock->addNote($mobileNote);
        }

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappSubBlock)] = $mobileSubBlock;
        return $mobileSubBlock;
    }

    private function handleUnitPlot($webappUnitPlot, array &$webappIriToMobileItemMap, array $stateCodeMap): UnitParcel
    {
        $mobileUnitPlot = (new UnitParcel())
            ->setName($webappUnitPlot->getNumber())
            ->setX($webappUnitPlot instanceof WebappSurfaceUnitPlot ? $webappUnitPlot->getX() : 0)
            ->setY($webappUnitPlot instanceof WebappSurfaceUnitPlot ? $webappUnitPlot->getY() : 0)
            ->setIdent($webappUnitPlot instanceof WebappSurfaceUnitPlot ? $webappUnitPlot->getIdentifier() : null)
            ->setApparitionDate($webappUnitPlot instanceof WebappSurfaceUnitPlot ? $webappUnitPlot->getAppeared() : new DateTime())
            ->setDemiseDate($webappUnitPlot instanceof WebappSurfaceUnitPlot ? $webappUnitPlot->getDisappeared() : null)
            ->setTreatment($webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappUnitPlot->getTreatment())]);

        if ($webappUnitPlot instanceof WebappSurfaceUnitPlot && in_array(StateCode::DEAD_STATE_CODE, array_keys($stateCodeMap)) && $webappUnitPlot->isDead()) {
            $mobileUnitPlot->setStateCode($stateCodeMap[StateCode::DEAD_STATE_CODE]);
        }

        foreach ($webappUnitPlot instanceof WebappSurfaceUnitPlot ? [] : $webappUnitPlot->getIndividuals() as $webappIndividuals) {
            $mobileIndividual = $this->handleIndividual($webappIndividuals, $stateCodeMap, $webappIriToMobileItemMap);
            $mobileUnitPlot->addIndividual($mobileIndividual);
        }

        foreach ($webappUnitPlot->getNotes() as $note) {
            $mobileNote = $this->handleNote($note);
            $mobileUnitPlot->addNote($mobileNote);
        }

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappUnitPlot)] = $mobileUnitPlot;
        return $mobileUnitPlot;
    }

    private function handleIndividual(WebappIndividual $webappIndividual, array $stateCodeMap, array &$webappIriToMobileItemMap): Individual
    {
        $mobileIndividual = (new Individual())
            ->setName($webappIndividual->getNumber())
            ->setX($webappIndividual->getX())
            ->setY($webappIndividual->getY())
            ->setApparitionDate($webappIndividual->getAppeared())
            ->setDemiseDate($webappIndividual->getDisappeared())
            ->setIdent($webappIndividual->getIdentifier());

        if (in_array(StateCode::DEAD_STATE_CODE, array_keys($stateCodeMap)) && $webappIndividual->isDead()) {
            $mobileIndividual->setStateCode($stateCodeMap[StateCode::DEAD_STATE_CODE]);
        }

        foreach ($webappIndividual->getNotes() as $note) {
            $mobileNote = $this->handleNote($note);
            $mobileIndividual->addNote($mobileNote);
        }

        $webappIriToMobileItemMap[$this->iriConverter->getIriFromItem($webappIndividual)] = $mobileIndividual;
        return $mobileIndividual;
    }

    private function handleDevice(WebappDevice $device, &$variableMap, $stateCodeMap): Material
    {
        $material = new Material();
        $driver = new Driver();
        $driver->setType($device->getCommunicationProtocol())
            ->setFrameLength($device->getFrameLength())
            ->setFrameStart($device->getFrameStart())
            ->setFrameEnd($device->getFrameEnd())
            ->setTimeout(1000)
            ->setCsvSeparator($device->getFrameCsv());
        if ($driver->getType() === Driver::RS232) {
            $driver->setBaudrate($device->getBaudrate())
                ->setDatabitsFormat($device->getBitFormat())
                ->setFlowControl($device->getFlowControl())
                ->setParity($device->getParity())
                ->setPush($device->getRemoteControl());
            switch ($device->getStopBit()) {
                case StopBitEnum::ONE:
                    $driver->setStopbit(1);
                    break;
                case StopBitEnum::ONE_HALF:
                    $driver->setStopbit(1.5);
                    break;
                case StopBitEnum::TWO:
                    $driver->setStopbit(2);
                    break;
            }
        }
        $material->setType($device->getType())
            ->setName($device->getName())
            ->setDriver($driver)
            ->setManufacturer($device->getManufacturer())
            ->setPhysicalDevice($device->getAlias());

        foreach ($device->getManagedVariables() as $managedVariable) {
            $variable = $this->handleSemiAutomaticVariable($managedVariable, $variableMap, $stateCodeMap);
            $material->addVariable($variable);
        }
        return $material;
    }

    private function handleSemiAutomaticVariable(WebappSemiAutomaticVariable $managedVariable, &$variableMap, $stateCodeMap): UniqueVariable
    {
        $variable = new UniqueVariable();
        $variableMap[$this->iriConverter->getIriFromItem($managedVariable)] = $variable;
        $this->manageVariableData($variable, $managedVariable, $stateCodeMap, $variableMap);
        $variable->setFrameStartPosition($managedVariable->getStart())
            ->setFrameEndPosition($managedVariable->getEnd())
            ->setType($managedVariable->getType())
            ->setOrder($managedVariable->getOrder() ?? -1); // TODO vérifier que l'ordre est tout le temps défini
        return $variable;
    }

    private function handleSimpleVariable(WebappSimpleVariable $webappSimpleVariable, array &$variableMap, $order, $stateCodeMap): UniqueVariable
    {
        $mobileSimpleVariable = new UniqueVariable();
        $variableMap[$this->iriConverter->getIriFromItem($webappSimpleVariable)] = $mobileSimpleVariable;
        $this->manageVariableData($mobileSimpleVariable, $webappSimpleVariable, $stateCodeMap, $variableMap);
        $mobileSimpleVariable->setType($webappSimpleVariable->getType());

        $mobileSimpleVariable->setOrder($webappSimpleVariable->getOrder() ?? $order);

        /** @var ValueList $webappValueList */
        if (($webappValueList = $webappSimpleVariable->getValueList()) !== null) {
            $valueHintList = (new ValueHintList())->setName($webappValueList->getName());
            /** @var ValueListItem $value */
            foreach ($webappValueList->getValues() as $value) {
                $valueHintList->addValueHint((new ValueHint())->setValue($value->getName()));
            }
            $mobileSimpleVariable->setValueHintList($valueHintList);
        }

        if (($webappScale = $webappSimpleVariable->getScale()) !== null) {
            $scale = (new Scale())
                ->setName($webappScale->getName())
                ->setExclusive(!$webappScale->isOpen())
                ->setMinValue($webappScale->getMinValue())
                ->setMaxValue($webappScale->getMaxValue());
            /** @var VariableScaleItem $value */
            foreach ($webappScale->getValues() as $value) {
                $scale->addMark((new Mark())
                    ->setValue($value->getValue())
                    ->setText($value->getText())
                    ->setImage($value->getPic() === "" ? null : $value->getPic()));
            }
            $mobileSimpleVariable->setScale($scale);
        }

        if ($webappSimpleVariable->getType() === VariableTypeEnum::ALPHANUMERIC || $webappSimpleVariable->getType() === VariableTypeEnum::REAL) {
            $mobileSimpleVariable->setFormat(strval($webappSimpleVariable->getFormatLength()));
        } else if ($webappSimpleVariable->getFormat() === VariableFormatEnum::QUANTIEM) {
            $mobileSimpleVariable->setFormat($webappSimpleVariable->getFormat());
        }
        return $mobileSimpleVariable;
    }

    private function handleGeneratorVariable(WebappGeneratorVariable $webappGeneratorVariable, array &$variableMap, $order, $stateCodeMap, $webappIriToMobileItemMap): GeneratorVariable
    {
        $mobileGeneratorVariable = new GeneratorVariable();
        $variableMap[$this->iriConverter->getIriFromItem($webappGeneratorVariable)] = $mobileGeneratorVariable;
        $this->manageVariableData($mobileGeneratorVariable, $webappGeneratorVariable, $stateCodeMap, $variableMap);
        $mobileGeneratorVariable->setOrder($webappGeneratorVariable->getOrder() ?? $order)
            ->setGeneratedPrefix($webappGeneratorVariable->getGeneratedPrefix())
            ->setNumeralIncrement($webappGeneratorVariable->isNumeralIncrement());
        $mobileGeneratorVariable->setType(VariableTypeEnum::INTEGER);
        // TODO: Gérer les données spécifiques à Génératrice...

        $localOrder = 0;
        foreach ($webappGeneratorVariable->getGeneratedSimpleVariables() as $simpleVariable) {
            $uniqueVariable = $this->handleSimpleVariable($simpleVariable, $variableMap, $localOrder++, $stateCodeMap);
            $mobileGeneratorVariable->addUniqueVariable($uniqueVariable);
        }
        foreach ($webappGeneratorVariable->getGeneratedGeneratorVariables() as $generatorVariable) {
            $generatorVariable = $this->handleGeneratorVariable($generatorVariable, $variableMap, $localOrder++, $stateCodeMap, $webappIriToMobileItemMap);
            $mobileGeneratorVariable->addGeneratorVariable($generatorVariable);
        }

        return $mobileGeneratorVariable;
    }

    /**
     * @param UniqueVariable|GeneratorVariable $mobileVariable
     * @param WebappSimpleVariable|WebappGeneratorVariable|WebappSemiAutomaticVariable $webappVariable
     */
    private function manageVariableData($mobileVariable, $webappVariable, $stateCodeMap, &$variableMap): void
    {
        $mobileVariable
            ->setUnit($webappVariable->getUnit())
            ->setCreationDate($webappVariable->getCreated())
            ->setModificationDate($webappVariable->getLastModified())
            ->setName($webappVariable->getName())
            ->setShortName($webappVariable->getShortName())
            ->setRepetitions($webappVariable->getRepetitions())
            ->setComment($webappVariable->getComment())
            ->setAskTimestamp(true) // TODO: useless and never false and never queried on both Bureau, Terrain and Mobile
            ->setDefaultValue(
                !$webappVariable instanceof WebappGeneratorVariable &&
                VariableTypeEnum::BOOLEAN === $webappVariable->getType() &&
                $webappVariable->getDefaultTrueValue() ? 'true' : null)
            ->setMandatory($webappVariable->isMandatory())
            ->setActive(true);

        foreach ($webappVariable->getConnectedVariables() as $variableConnection) {
            $originVariable = $variableConnection->getDataEntryVariable();
            $dummyMap = [];
            if ($originVariable instanceof WebappGeneratorVariable) {
                $mobileOriginVariable = $this->handleGeneratorVariable($originVariable, $dummyMap, 0, $dummyMap, $dummyMap);
            } else {
                $mobileOriginVariable = $this->handleSimpleVariable($originVariable, $dummyMap, 0, $dummyMap);
            }
            $variableMap[$this->iriConverter->getIriFromItem($originVariable)] = $mobileOriginVariable;
            foreach ($variableConnection->getDataEntryVariable()->getProjectData()->getSessions() as $session) {
                foreach ($session->getFieldMeasures() as $fieldMeasure) {
                    if ($fieldMeasure->getVariable() === $originVariable) {

                        $previousValue = (new PreviousValue())
                            ->setDate($fieldMeasure->getMeasures()->get(0)->getTimestamp())
                            ->setObjectUri($this->iriConverter->getIriFromItem($fieldMeasure->getTarget())) // No other choice, uri does not exist atm, have to be corrected
                            ->setValue($fieldMeasure->getMeasures()->get(0)->getValue())
                            ->setOriginVariable($mobileOriginVariable);

                        // Manage the state code of previous value.
                        if (!is_null(($state = $fieldMeasure->getMeasures()->get(0)->getState())) && isset($stateCodeMap[$state->getCode()])) {
                            $previousValue->setState($stateCodeMap[$state->getCode()]);
                        }

                        $mobileVariable->addPreviousValue($previousValue);
                    }
                }

            }
        }

        $mobileVariable->setPathLevel($this->transformPathLevelEnum($webappVariable->getPathLevel()));
    }

    private function handleGraphicalStructure(WebappPlatform $platform): GraphicalStructure
    {
        $graphicalStructure = new GraphicalStructure();
        switch ($platform->getOrigin()) {
            case GraphicalOriginEnum::BOTTOM_LEFT:
                $graphicalStructure->setOrigin('basGauche');
                break;
            case GraphicalOriginEnum::TOP_LEFT:
                $graphicalStructure->setOrigin('hautGauche');
                break;
            case GraphicalOriginEnum::TOP_RIGHT:
                $graphicalStructure->setOrigin('hautDroite');
                break;
            case GraphicalOriginEnum::BOTTOM_RIGHT:
                $graphicalStructure->setOrigin(null);
                break;
        }
        return ($graphicalStructure)
            ->setPlatformColor("RGB{255,255,255}") // TODO: Utiliser la donnée de webapp
            ->setDeviceColor("RGB{153,255,153}") // TODO: Utiliser la donnée de webapp
            ->setBlockColor("RGB{51,153,204}") // TODO: Utiliser la donnée de webapp
            ->setSubBlockColor("RGB{51,204,255}") // TODO: Utiliser la donnée de webapp
            ->setIndividualUnitParcelColor("RGB{51,204,0}") // TODO: Utiliser la donnée de webapp
            ->setSurfaceUnitParcelColor("RGB{51,204,0}") // TODO: Utiliser la donnée de webapp
            ->setAbnormalUnitParcelColor("RGB{51,204,0}") // TODO: Utiliser la donnée de webapp
            ->setIndividualColor("RGB{255,255,0}") // TODO: Utiliser la donnée de webapp
            ->setAbnormalIndividualColor("RGB{255,255,0}") // TODO: Utiliser la donnée de webapp
            ->setEmptyColor("RGB{255,255,153}") // TODO: Utiliser la donnée de webapp
            ->setTooltipActive(false) // TODO: Utiliser la donnée de webapp
            ->setDeviceLabel("dispoNom") // TODO: Utiliser la donnée de webapp
            ->setBlockLabel("blocNum") // TODO: Utiliser la donnée de webapp
            ->setSubBlockLabel("sousblocNum") // TODO: Utiliser la donnée de webapp
            ->setIndividualUnitParcelLabel("puTraitCourt") // TODO: Utiliser la donnée de webapp
            ->setSurfaceUnitParcelLabel("puNum") // TODO: Utiliser la donnée de webapp
            ->setIndividualLabel("indNum") // TODO: Utiliser la donnée de webapp
            ->setBaseWidth($platform->getXMesh())
            ->setBaseHeight($platform->getYMesh());
    }

    private function handleStateCode(WebappStateCode $webappStateCode, array &$stateCodeMap): StateCode
    {
        $mobileStateCode = (new StateCode())
            ->setCode($webappStateCode->getCode())
            ->setLabel($webappStateCode->getTitle())
            ->setDescription($webappStateCode->getMeaning())
            ->setColor($webappStateCode->getColor() === null ? null : $this->transformColor($webappStateCode->getColor()));

        switch ($webappStateCode->getSpreading()) {
            case SpreadingEnum::INDIVIDUAL:
                $mobileStateCode->setPropagation(PathLevelEnum::INDIVIDUAL);
                break;
            case SpreadingEnum::UNIT_PLOT:
                $mobileStateCode->setPropagation(PathLevelEnum::UNIT_PLOT);
                break;
            default:
                $mobileStateCode->setPropagation(null);
        }

        switch ($webappStateCode->getTitle()) {
            case "Mort":
                $mobileStateCode->setType(StateCode::DEAD_STATE_CODE);
                $stateCodeMap[StateCode::DEAD_STATE_CODE] = $mobileStateCode;
                break;
            case "Donnée Manquante":
                $mobileStateCode->setType(StateCode::MISSING_STATE_CODE);
                $stateCodeMap[StateCode::MISSING_STATE_CODE] = $mobileStateCode;
                break;
            default:
                $mobileStateCode->setType(StateCode::NON_PERMANENT_STATE_CODE);
        }
        $stateCodeMap[$mobileStateCode->getCode()] = $mobileStateCode;
        return $mobileStateCode;
    }

    /**
     * @param Variable $variable
     * @param Test $test
     * @param array $variableMap
     */
    private function handleTest(Variable $variable, Test $test, array $variableMap)
    {
        switch ($test->getType()) {
            case TestTypeEnum::INTERVAL:
                $variable->addRangeTest(
                    (new RangeTest())->setMandatoryMaxValue($test->getAuthIntervalMax())
                        ->setMandatoryMinValue($test->getAuthIntervalMin())
                        ->setOptionalMaxValue($test->getProbIntervalMax())
                        ->setOptionalMinValue($test->getProbIntervalMin())
                        ->setActive(true)
                );
                break;
            case TestTypeEnum::COMBINATION:
                $variable->addCombinationTest(
                    (new CombinationTest())->setFirstOperand($variableMap[$this->iriConverter->getIriFromItem($test->getCombinedVariable1())])
                        ->setHighLimit($test->getMaxLimit())
                        ->setLowLimit($test->getMinLimit())
                        ->setOperator($test->getCombinationOperation())
                        ->setSecondOperand($variableMap[$this->iriConverter->getIriFromItem($test->getCombinedVariable2())])
                        ->setActive(true)
                );
                break;
            case TestTypeEnum::GROWTH:
                $variable->addGrowthTest(
                    (new GrowthTest())
                        ->setComparisonVariable($variableMap[$this->iriConverter->getIriFromItem($test->getComparedVariable())])
                        ->setMinDiff($test->getMinGrowth())
                        ->setMaxDiff($test->getMaxGrowth())
                        ->setActive(true)
                );
                break;
            case TestTypeEnum::PRECONDITIONED:
                $variable->addPreconditionedCalculation(
                    (new PreconditionedCalculation())
                        ->setComparisonVariable($variableMap[$this->iriConverter->getIriFromItem($test->getComparedVariable1())])
                        ->setAffectationValue($test->getAssignedValue())
                        ->setComparedValue($test->getComparedValue())
                        ->setComparedVariable($test->getComparedVariable2() !== null ? $variableMap[$this->iriConverter->getIriFromItem($test->getComparedVariable2())] : null)
                        ->setCondition($test->getComparisonOperation())
                        ->setActive(true)
                );
                break;
        }
    }

    private function handleNote(Note $webappNote): EntryNote
    {
        return (new EntryNote())
            ->setCreator($webappNote->getCreator())
            ->setCreationDate($webappNote->getCreationDate())
            ->setDeleted($webappNote->isDeleted())
            ->setText($webappNote->getText());
    }

    private function handleUserPath(PathUserWorkflow $webappUserPath, array $webappIriToMobileItemMap): Workpath
    {
        return (new Workpath())
            ->setPath($this->iriToUri($webappUserPath->getWorkflow(), $webappIriToMobileItemMap))
            ->setStartEnd(!$webappUserPath->getPathBase()->isAskWhenEntering())
            ->setUsername($webappUserPath->getUsername());
    }

    private function handleRequiredAnnotation(WebappRequiredAnnotation $annotation): RequiredAnnotation
    {
        return (new RequiredAnnotation())
            ->setPathLevel($this->transformPathLevelEnum($annotation->getLevel()))
            ->setType($annotation->getType())
            ->setActive(true)
            ->setAskWhenEntering($annotation->isAskWhenEntering())
            ->setCategory([])
            ->setComment($annotation->getComment())
            ->setKeywords([])
            ->setBuisnessObjects("");
    }

    private function transformPathLevelEnum(string $webappPathLevel): string
    {
        if ($webappPathLevel === PathLevelEnum::SURFACIC_UNIT_PLOT) {
            $webappPathLevel = PathLevelEnum::UNIT_PLOT;
        }

        return $webappPathLevel;
    }

    /**
     * @param array $iriTab
     * @param array $webappIriToMobileItemMap
     * @return string
     */
    private function iriToUri(array $iriTab, array $webappIriToMobileItemMap): string
    {
        return implode('', array_map(function ($iri) use ($webappIriToMobileItemMap) {
            return $webappIriToMobileItemMap[$iri]->getUri();
        }, $iriTab));
    }

    private function transformColor(int $webappColor): string
    {
        $rColor = intdiv($webappColor, 256 * 256);
        $gColor = intdiv($webappColor - $rColor * 256 * 256, 256);
        $bColor = $webappColor - $rColor * 256 * 256 - $gColor * 256;
        return sprintf('RGB{%d,%d,%d}', $rColor, $gColor, $bColor);
    }
}
