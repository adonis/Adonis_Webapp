<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\TransferSync\Service;

use Mobile\Measure\Entity\Session;
use Mobile\Project\Entity\DataEntryProject;
use Mobile\Project\Entity\Platform;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Shared\TransferSync\Entity\IndividualStructureChange;
use Webapp\Core\Entity\Platform as WebappPlatform;
use Webapp\Core\Entity\Project;
use Webapp\Core\Entity\Project as WebappProject;
use Webapp\Core\Entity\ProjectData;

/**
 * Interface MobileToWebappBridgeInterface
 * @package Shared\TransferSync\Service
 */
interface MobileToWebappBridgeInterface
{
    /**
     * @param WebappProject $project
     * @param Session[] $mobileSessions
     * @param IndividualStructureChange[] $changes
     * @return ProjectData
     */
    public function buildDataEntryFromMobileToWebapp(WebappProject $project, array $mobileSessions, array $changes, array $objectsAnnotations, array $mobileNotes, User $user): ProjectData;

    /**
     * @param Platform $platform
     * @return WebappPlatform
     */
    public function buildPlatformFromMobileToWebapp(Platform $platform, User $user, Site $site): WebappPlatform;

    /**
     * @param DataEntryProject $project
     * @param WebappPlatform $platform
     * @return WebappProject
     */
    public function buildProjectFromMobileToWebapp(DataEntryProject $project, WebappPlatform $platform, User $user): Project;
}
