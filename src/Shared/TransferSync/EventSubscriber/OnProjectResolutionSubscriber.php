<?php

/*
 * @author TRYDEA - 2024
 */

namespace Shared\TransferSync\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Shared\FileManagement\Entity\UserLinkedJob;
use Shared\TransferSync\Entity\StatusDataEntry;
use Webapp\FileManagement\Entity\ResponseFile;
use Webapp\FileManagement\Worker\ResponseDataEntryProjectWorker;

/**
 * for Doctrine < 2.4: use Doctrine\ORM\Event\LifecycleEventArgs;.
 */
class OnProjectResolutionSubscriber implements EventSubscriber
{
    private ResponseDataEntryProjectWorker $responseDataEntryProjectWorker;

    public function __construct(
        ResponseDataEntryProjectWorker $responseDataEntryProjectWorker
    ) {
        $this->responseDataEntryProjectWorker = $responseDataEntryProjectWorker;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postUpdate,
            Events::postPersist,
        ];
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->index($args);
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        // only apply to the dataEntry creation
        if ($entity instanceof StatusDataEntry) {
            $statusDataEntry = $entity;
            if (StatusDataEntry::STATUS_WRITE_PENDING === $statusDataEntry->getStatus()) {
                $user = $statusDataEntry->getUser();
                $responseFile = (new ResponseFile())
                    ->setName($statusDataEntry->getResponse()->getName())
                    ->setProject($statusDataEntry->getResponse())
                    ->setUser($user)
                    ->setStatus(UserLinkedJob::STATUS_PENDING)
                    ->setUniqDirectoryName(null === $statusDataEntry->getRequest() || null === $statusDataEntry->getRequest()->getOriginFile()
                        ? uniqid() : $statusDataEntry->getRequest()->getOriginFile()->getUniqDirectoryName())
                    ->setUploadDate(new \DateTime());
                $args->getObjectManager()->persist($responseFile);
                $args->getObjectManager()->flush();

                $this->responseDataEntryProjectWorker->later()->createFile($responseFile->getId(), $statusDataEntry->getId());
            }
        }
    }
}
