<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\TransferSync\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Shared\TransferSync\Entity\StatusDataEntry;
use UnexpectedValueException;
use Webapp\FileManagement\Entity\RequestFile;

/**
 * Class OnFileStateChangeSubscriber.
 */
class OnFileStateChangeSubscriber implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            Events::postUpdate,
        ];
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof RequestFile) {
            $requestFile = $entity;
            $entityManager = $args->getObjectManager();
            $requestProject = $requestFile->getProject();
            $repo = $entityManager->getRepository(StatusDataEntry::class);
            if (!$requestProject) {
                return; // Request project might be null in case of error while attempting to parse file.
            }
            $statusDataEntry = $repo->findOneBy(['request' => $requestProject]);
            if (is_null($statusDataEntry)) {
                throw new UnexpectedValueException(''); // Must never happen.
            }
            $statusDataEntry->setSyncable(!$requestFile->isDisabled());
            $entityManager->flush();
        }
    }
}
