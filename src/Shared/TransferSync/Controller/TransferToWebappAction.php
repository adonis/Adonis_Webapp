<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\TransferSync\Controller;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Repository\EntryNoteRepository;
use Mobile\Measure\Entity\Session;
use Shared\TransferSync\Entity\IndividualStructureChange;
use Shared\TransferSync\Entity\StatusDataEntry;
use Shared\TransferSync\Service\MobileToWebappBridgeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

/**
 * Custom Operation to perform transformation of a Webapp Data Entry Project into a Mobile compatible Project.
 *
 * Class TransferToWebappAction
 * @package Shared\TransferSync\Controller
 */
class TransferToWebappAction
{
    private EntityManagerInterface $entityManager;

    private IriConverterInterface $converter;

    private MobileToWebappBridgeInterface $webappBridge;

    private Security $security;

    public function __construct(
        EntityManagerInterface $entityManager,
        IriConverterInterface $converter,
        MobileToWebappBridgeInterface $webappBridge,
        Security $security
    )
    {
        $this->converter = $converter;
        $this->entityManager = $entityManager;
        $this->webappBridge = $webappBridge;
        $this->security = $security;
    }

    /**
     * @param Request $request
     */
    public function __invoke(Request $request)
    {
        set_time_limit(300);
        $sessionMobileRepo = $this->entityManager->getRepository(Session::class);
        /** @var EntryNoteRepository $mobileNoteRepo */
        $mobileNoteRepo = $this->entityManager->getRepository(EntryNote::class);

        /** @var $statusDataEntry StatusDataEntry */
        $statusDataEntry = $request->attributes->get('data');
        $requestContent = json_decode($request->getContent());

        if($statusDataEntry->getResponse()->isImprovised()){
            $platform = $statusDataEntry->getWebappProject()->getPlatform();

            $project = $this->webappBridge->buildProjectFromMobileToWebapp($statusDataEntry->getResponse(), $platform, $this->security->getUser());
            $this->entityManager->persist($project);
        }else {
            $project = $statusDataEntry->getWebappProject();
        }
        $sessions = array_map(function (string $id) use ($sessionMobileRepo) {
            return $sessionMobileRepo->find($id);
        }, $requestContent->sessions);

        /** @var IndividualStructureChange $change */
        foreach ($statusDataEntry->getChanges() as $change){
            if(in_array($this->converter->getIriFromItem($change), $requestContent->approvedChanges)){
                $change->setApproved(true);
            }
        }

        $objectsAnnotations = $statusDataEntry->getResponse()->getAnnotations();

        $mobileNotes = $mobileNoteRepo->findPlatformNotes($statusDataEntry->getResponse()->getPlatform());

        $projectData = $this->webappBridge->buildDataEntryFromMobileToWebapp($project, $sessions, $statusDataEntry->getChanges()->toArray(), $objectsAnnotations, $mobileNotes, $statusDataEntry->getUser());

        $requestMobileProject = $statusDataEntry->getRequest();
        if (1 === $this->entityManager->getRepository(StatusDataEntry::class)->count(['request' => $requestMobileProject])) {
            $this->entityManager->remove($requestMobileProject);
            $statusDataEntry->getWebappProject()->setTransferDate(null);
        }

        $this->entityManager->remove($statusDataEntry);
        if ($projectData->getSessions()->count() !== 0){
            return $projectData;
        }
    }
}
