<?php

namespace Shared\TransferSync\Controller;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Shared\TransferSync\Entity\StatusDataEntry;
use Webapp\Core\Entity\Project;
use Webapp\Core\Enumeration\ExperimentStateEnum;

/**
 * Custom Operation to cancel a mobile export
 *
 * Class TransferToMobileAction
 * @package Shared\TransferSync\Controller
 */
class CancelTransferToMobileAction
{
    private EntityManagerInterface $entityManager;

    private IriConverterInterface $converter;

    public function __construct(
        EntityManagerInterface $entityManager,
        IriConverterInterface $converter
    )
    {
        $this->converter = $converter;
        $this->entityManager = $entityManager;
    }

    /**
     * @param $request
     * @return StatusDataEntry
     */
    public function __invoke(StatusDataEntry $relUserProject): StatusDataEntry
    {
        $project = $relUserProject->getWebappProject();
        if(count($project->getStatusDataEntries()) === 1){
            $project->setTransferDate(null);
            foreach ($project->getExperiments() as $experiment){
                if(array_reduce($experiment->getProjects()->toArray(), function ($acc, Project $item) use ($project) {
                    return $acc && ($item === $project || count($item->getStatusDataEntries()) === 0);
                }, true)) {
                    $experiment->setState(ExperimentStateEnum::LOCKED);
                }

            }

        }
        return $relUserProject;
    }
}
