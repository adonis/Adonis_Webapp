<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\TransferSync\Controller;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Authentication\Entity\User;
use Shared\RightManagement\Entity\AbstractAdvancedRight;
use Shared\RightManagement\Entity\AdvancedUserRight;
use Shared\TransferSync\Entity\StatusDataEntry;
use Shared\TransferSync\Service\WebappToMobileBridgeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\Project;
use Webapp\Core\Enumeration\ExperimentStateEnum;

/**
 * Custom Operation to perform transformation of a Webapp Data Entry Project into a Mobile compatible Project.
 *
 * Class TransferToMobileAction
 * @package Shared\TransferSync\Controller
 */
class TransferToMobileAction
{
    private EntityManagerInterface $entityManager;

    private IriConverterInterface $converter;

    private WebappToMobileBridgeInterface $mobileBridge;

    private Security  $security;

    public function __construct(
        EntityManagerInterface        $entityManager,
        IriConverterInterface         $converter,
        WebappToMobileBridgeInterface $mobileBridge,
        Security                      $security
    )
    {
        $this->converter = $converter;
        $this->entityManager = $entityManager;
        $this->mobileBridge = $mobileBridge;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @return Project
     */
    public function __invoke(Request $request): Project
    {
        /** @var $project Project */
        $project = $request->attributes->get('data');
        $content = json_decode($request->getContent());
        $userIris = $content->users;
        $isBenchmark = 0 === sizeof($userIris);

        $siteUsers = array_map(function ($userRole) {
            return $userRole->getUser();
        }, $project->getPlatform()->getSite()->getUserRoles()->toArray());

        if ($isBenchmark) {

            $exportToUsers = [$this->security->getUser()];
            $existingBenchmarkProjects = $this->entityManager->getRepository(StatusDataEntry::class)->findBenchmarkByWebappProject($project);
            foreach ($existingBenchmarkProjects as $benchmarkProject){
                $this->entityManager->remove($benchmarkProject);
            }
        } else {

            $project->setTransferDate(date_create());
            foreach ($project->getExperiments() as $experiment) {
                $experiment->setState(ExperimentStateEnum::NON_UNLOCKABLE);
            }

            $exportToUsers = array_filter($siteUsers, function (User $user) use ($userIris) {
                return in_array($this->converter->getIriFromItem($user), $userIris);
            });
        }

        $webappIriToMobileItemMap = [];
        $mobileProject = $this->mobileBridge->buildProjectFromWebappToMobile($isBenchmark, $project, $siteUsers, $webappIriToMobileItemMap);

        foreach ($mobileProject->getUniqueVariables() as $variable) {
            foreach ($variable->getPreviousValues() as $previousValue) {
                $previousValue->setObjectUri($webappIriToMobileItemMap[$previousValue->getObjectUri()]->getUri());
                $mobileProject->addConnectedVariable($previousValue->getOriginVariable());
            }
        }

        foreach ($mobileProject->getGeneratorVariables() as $variable) {
            foreach ($variable->getPreviousValues() as $previousValue) {
                $previousValue->setObjectUri($webappIriToMobileItemMap[$previousValue->getObjectUri()]->getUri());
                $mobileProject->addConnectedVariable($previousValue->getOriginVariable());
            }
        }

        // No user attached on benchmark action.
        $rightRepository = $this->entityManager->getRepository(AdvancedUserRight::class);
        foreach ($exportToUsers as $user) {

            if ($user !== $project->getOwner() && 0 === count($rightRepository->findBy([
                    'classIdentifier' => 'webapp_project',
                    'objectId' => $project->getId(),
                    'userId' => $user->getId()
                ]))) {
                $this->entityManager->persist((new AdvancedUserRight())
                    ->setObjectId($project->getId())
                    ->setClassIdentifier('webapp_project')
                    ->setUserId($user->getId())
                    ->setRight(AbstractAdvancedRight::ADVANCED_RIGHT_VIEW)
                );
            }

            if ($user !== $project->getOwner() && 0 === count($rightRepository->findBy([
                    'classIdentifier' => 'webapp_platform',
                    'objectId' => $project->getPlatform()->getId(),
                    'userId' => $user->getId()
                ]))) {
                $this->entityManager->persist((new AdvancedUserRight())
                    ->setObjectId($project->getPlatform()->getId())
                    ->setClassIdentifier('webapp_platform')
                    ->setUserId($user->getId())
                    ->setRight(AbstractAdvancedRight::ADVANCED_RIGHT_VIEW)
                );
            }

            $status = (new StatusDataEntry($user, StatusDataEntry::STATUS_NEW))
                ->setRequest($mobileProject)
                ->setSyncable(true)
                ->setWebappProject($project);

            $this->entityManager->persist($status);
        }

        $this->entityManager->flush();
        return $project;
    }
}
