<?php

declare(strict_types=1);

namespace Shared\TransferSync\Controller;

use Shared\TransferSync\Entity\StatusDataEntry;
use Shared\TransferSync\Voter\StatusDataEntryVoter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class PermissionController extends AbstractController
{

    /**
     * Check if user is allowed to send return data for a status data entry.
     *
     * @Route("/api/status_data_entries/{id}/return-data-permission", name="statusDataEntriesReturnDataPermission")
     */
    public function index(Security $security, StatusDataEntry $statusDataEntry): Response
    {
        return new JsonResponse($security->isGranted(StatusDataEntryVoter::STATUS_DATA_ENTRY_GET_PATCH, $statusDataEntry));
    }
}
