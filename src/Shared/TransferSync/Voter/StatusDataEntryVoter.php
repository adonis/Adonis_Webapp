<?php

namespace Shared\TransferSync\Voter;

use Shared\TransferSync\Entity\StatusDataEntry;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Webapp\Core\Entity\PathUserWorkflow;

/**
 * Voter to check rights on StatusDataEntry.
 */
final class StatusDataEntryVoter extends Voter
{
    public const STATUS_DATA_ENTRY_GET_PATCH = 'STATUS_DATA_ENTRY_GET_PATCH';

    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $subject instanceof StatusDataEntry && $attribute === self::STATUS_DATA_ENTRY_GET_PATCH;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        assert($subject instanceof StatusDataEntry);

        $user = $token->getUser();
        if (null === $user) {
            return false;
        }

        $vote = false;
        if ($attribute == self::STATUS_DATA_ENTRY_GET_PATCH) {
            $vote = $this->canPatch($subject, $user);
        }

        return $vote;
    }

    /**
     * Check if user can PATCH.
     */
    private function canPatch(StatusDataEntry $statusDataEntry, UserInterface $user): bool
    {
        $project = $statusDataEntry->getWebappProject();
        $platform = $project !== null ? $project->getPlatform() : null;
        $site = $platform !== null ? $platform->getSite() : null;
        if ($project === null || $platform === null || $site === null) {
            throw new \LogicException('StatusDataEntry not link to a site.');
        }

        if (!$this->security->isGranted('ROLE_PLATFORM_MANAGER') && !$this->security->isGranted('ROLE_SITE_ADMIN')) {
            // Only site admin and platform manager are allowed to patch
            return false;
        }

        $pathBase = $project->getPathBase();

        return
            // Site admin and platform manager are allowed
            $this->security->isGranted('ROLE_SITE_ADMIN', $site)
            || $this->security->isGranted('ROLE_PLATFORM_MANAGER', $site)
            // Project owner is allowed
            || $project->getOwner() === $user
            // User with user path on project is allowed
            || ($pathBase !== null && $pathBase->getUserPaths()->exists(
                    fn($key, PathUserWorkflow $userPath) => $userPath->getUser() === $user)
            );
    }
}
