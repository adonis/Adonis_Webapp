<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Shared\TransferSync\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\UnitParcel;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Declaration of changes between one request project business object and its state in response project.
 *
 * @ApiResource(
 *     collectionOperations={},
 *     itemOperations={
 *         "get"={
 *             "security"="is_granted('STRUCTURE_CHANGE_GET_PATCH', object)",
 *         },
 *         "patch"={
 *             "security"="is_granted('STRUCTURE_CHANGE_GET_PATCH', object)",
 *             "denormalization_context"={"groups"={"individual_structure_change_patch"}},
 *         }
 *     }
 * )
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="rel_individual_change", schema="shared")
 */
class IndividualStructureChange extends IdentifiedEntity
{
    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Individual")
     */
    private ?Individual $requestIndividual = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Individual")
     */
    private ?Individual $responseIndividual = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\UnitParcel")
     */
    private ?UnitParcel $requestUnitParcel = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\UnitParcel")
     */
    private ?UnitParcel $responseUnitParcel = null;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\TransferSync\Entity\StatusDataEntry", inversedBy="changes")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private StatusDataEntry $statusDataEntry;

    /**
     * @Groups({"individual_structure_change_patch"})
     *
     * @ORM\Column(type="boolean", options={"default"=false})
     */
    private bool $approved = false;

    /**
     * @psalm-mutation-free
     */
    public function getRequestIndividual(): ?Individual
    {
        return $this->requestIndividual;
    }

    /**
     * @return $this
     */
    public function setRequestIndividual(?Individual $requestIndividual): self
    {
        $this->requestIndividual = $requestIndividual;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResponseIndividual(): ?Individual
    {
        return $this->responseIndividual;
    }

    /**
     * @return $this
     */
    public function setResponseIndividual(?Individual $responseIndividual): self
    {
        $this->responseIndividual = $responseIndividual;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRequestUnitParcel(): ?UnitParcel
    {
        return $this->requestUnitParcel;
    }

    /**
     * @return $this
     */
    public function setRequestUnitParcel(?UnitParcel $requestUnitParcel): self
    {
        $this->requestUnitParcel = $requestUnitParcel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResponseUnitParcel(): ?UnitParcel
    {
        return $this->responseUnitParcel;
    }

    /**
     * @return $this
     */
    public function setResponseUnitParcel(?UnitParcel $responseUnitParcel): self
    {
        $this->responseUnitParcel = $responseUnitParcel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStatusDataEntry(): StatusDataEntry
    {
        return $this->statusDataEntry;
    }

    /**
     * @return $this
     */
    public function setStatusDataEntry(StatusDataEntry $statusDataEntry): self
    {
        $this->statusDataEntry = $statusDataEntry;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }

    /**
     * @return $this
     */
    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }
}
