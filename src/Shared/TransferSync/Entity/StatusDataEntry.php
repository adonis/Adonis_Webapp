<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\TransferSync\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Mobile\Project\Entity\DataEntryProject;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Shared\TransferSync\Controller\CancelTransferToMobileAction;
use Shared\TransferSync\Controller\TransferToWebappAction;
use Shared\TransferSync\Dto\SessionAndChangeSelectionInput;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Entity\Project;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     },
 *     itemOperations={
 *         "get"={"normalization_context"={"groups"={"status_project_webapp_return_data", "id_read"}},},
 *         "patch"={
 *             "security"="object.getUser() == user",
 *             "denormalization_context"={"groups"={"status_project_patch"}},
 *         },
 *         "delete"={
 *             "security"="object.getUser() == user || (object.getWebappProject() !== null && object.getWebappProject().getOwner() == user)",
 *             "controller"=CancelTransferToMobileAction::class,
 *         },
 *         "patch_return_data"={
 *             "security"="is_granted('STATUS_DATA_ENTRY_GET_PATCH', object)",
 *             "method"="PATCH",
 *             "path"="/status_data_entries/{id}/return-data",
 *             "controller"=TransferToWebappAction::class,
 *             "input"=SessionAndChangeSelectionInput::class,
 *             "validate"=false,
 *         },
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={
 *     "webappProject": "exact",
 *     "webappProject.platform.site": "exact",
 *     "user": "exact",
 *     "syncable": "exact",
 *     "response.improvised": "exact"
 * })
 * @ApiFilter(GroupFilter::class, arguments={
 *     "whitelist"={
 *         "status_project_mobile_view",
 *         "status_project_webapp_view",
 *         "status_project_webapp_return_data",
 *         "id_read",
 *     }
 * })
 * @ApiFilter(BooleanFilter::class, properties={
 *     "request.benchmark"
 * })
 * @ApiFilter(ExistsFilter::class, properties={
 *     "response"
 * })
 *
 * @Gedmo\SoftDeleteable()
 *
 * @ORM\Table(name="rel_user_project_status", schema="shared")
 *
 * @ORM\Entity(repositoryClass="Shared\TransferSync\Repository\StatusDataEntryRepository")
 */
class StatusDataEntry extends IdentifiedEntity
{
    use SoftDeleteableEntity;

    public const STATUS_SYNCHRONIZED = 'synchronized';
    public const STATUS_NEW = 'new';
    public const STATUS_WRITE_PENDING = 'writing';
    public const STATUS_DONE = 'done';

    /**
     * @Groups({"status_project_webapp_view", "status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User", inversedBy="projects")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected User $user;

    /**
     * @Groups({"status_project_webapp_view", "status_project_patch", "status_project_mobile_view"})
     *
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    protected string $status;

    /**
     * @Groups({"status_project_webapp_view", "status_project_patch"})
     *
     * @ORM\Column(name="syncable", type="boolean", nullable=false, options={"default"=false})
     */
    protected bool $syncable = false;

    /**
     * @Groups({"status_project_webapp_view", "status_project_mobile_view"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", cascade={"persist"})
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?DataEntryProject $request = null;

    /**
     * @ApiSubresource()
     *
     * @Groups({"status_project_webapp_return_data", "user_linked_file_job_read"})
     *
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", cascade={"persist", "remove", "detach"})
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?DataEntryProject $response = null;

    /**
     * @Groups({"status_project_webapp_view", "status_project_webapp_return_data", "status_project_mobile_view"})
     *
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Project", inversedBy="statusDataEntries")
     */
    private ?Project $webappProject = null;

    /**
     * @var Collection<int, IndividualStructureChange>
     *
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\OneToMany(targetEntity="Shared\TransferSync\Entity\IndividualStructureChange",mappedBy="statusDataEntry", cascade={"persist", "remove", "detach"}, orphanRemoval=true)
     */
    private Collection $changes;

    /**
     * StatusDataEntry constructor.
     */
    public function __construct(User $user, string $status)
    {
        $this->user = $user;
        $this->status = $status;
        $this->changes = new ArrayCollection();
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function isSyncable(): bool
    {
        return $this->syncable;
    }

    public function setSyncable(bool $syncable): self
    {
        $this->syncable = $syncable;

        return $this;
    }

    public function getRequest(): ?DataEntryProject
    {
        return $this->request;
    }

    public function setRequest(?DataEntryProject $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getResponse(): ?DataEntryProject
    {
        return $this->response;
    }

    public function setResponse(?DataEntryProject $response): self
    {
        $this->response = $response;

        return $this;
    }

    public function getWebappProject(): ?Project
    {
        return $this->webappProject;
    }

    public function setWebappProject(?Project $webappProject): self
    {
        $this->webappProject = $webappProject;

        return $this;
    }

    /**
     * @return Collection<int,  IndividualStructureChange>
     */
    public function getChanges(): Collection
    {
        return $this->changes;
    }

    public function addChange(IndividualStructureChange $change): self
    {
        if (!$this->changes->contains($change)) {
            $this->changes->add($change);
            $change->setStatusDataEntry($this);
        }

        return $this;
    }

    public function removeChange(IndividualStructureChange $change): self
    {
        if ($this->changes->contains($change)) {
            $this->changes->removeElement($change);
        }

        return $this;
    }

    /**
     * @Groups({"status_project_webapp_view"})
     */
    public function getMobileProjectName(): ?string
    {
        return isset($this->response) ? $this->response->getName() : null;
    }
}
