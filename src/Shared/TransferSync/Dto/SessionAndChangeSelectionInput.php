<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\TransferSync\Dto;

/**
 * Class SessionAndChangeSelectionInput
 * @package Shared\TransferSync\Dto
 */
class SessionAndChangeSelectionInput
{
    /**
     * @var string[]
     */
    private array $sessions;

    /**
     * @var string[]
     */
    private array $approvedChanges;

    /**
     * @return string[]
     */
    public function getSessions(): array
    {
        return $this->sessions;
    }

    /**
     * @param string[] $sessions
     * @return SessionAndChangeSelectionInput
     */
    public function setSessions(array $sessions): SessionAndChangeSelectionInput
    {
        $this->sessions = $sessions;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getApprovedChanges(): array
    {
        return $this->approvedChanges;
    }

    /**
     * @param string[] $approvedChanges
     * @return SessionAndChangeSelectionInput
     */
    public function setApprovedChanges(array $approvedChanges): SessionAndChangeSelectionInput
    {
        $this->approvedChanges = $approvedChanges;
        return $this;
    }
}
