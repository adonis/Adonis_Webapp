<?php

/*
 * @author TRYDEA - 2024
 */

namespace Shared\Authentication\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\MappedSuperclass()
 */
abstract class IdentifiedEntity
{
    /**
     * @Groups({"id_read"})
     *
     * @ORM\Id()
     *
     * @ORM\GeneratedValue()
     *
     * @ORM\Column(name="id", type="integer")
     */
    protected ?int $id = null;

    /**
     * @psalm-mutation-free
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUri(): string
    {
        $className = (new \ReflectionClass(static::class))->getShortName();

        return \sprintf('/%s.%d', $className, $this->id);
    }
}
