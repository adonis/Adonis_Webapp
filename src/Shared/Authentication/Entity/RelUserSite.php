<?php

declare(strict_types=1);

namespace Shared\Authentication\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={"security_post_denormalize"="is_granted('REL_USER_SITE_POST', object)"},
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('REL_USER_SITE_GET', object)"},
 *         "put"={"security"="is_granted('REL_USER_SITE_EDIT', object)"},
 *         "patch"={"security"="is_granted('REL_USER_SITE_EDIT', object)"},
 *         "delete"={"security"="is_granted('REL_USER_SITE_DELETE', object)"},
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"site": "exact", "user": "exact"})
 *
 * @Gedmo\SoftDeleteable()
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="rel_user_site", schema="shared")
 */
class RelUserSite
{
    use SoftDeleteableEntity;
    public const EXPERIMENTER = 'expert';
    public const PLATFORM_MANAGER = 'manager';
    public const SITE_ADMIN = 'admin';

    /**
     * The user involved in the relationship.
     *
     * @Groups({"site_post"})
     *
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User", inversedBy="siteRoles")
     *
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private ?User $user = null;

    /**
     * The site involved in the relationship.
     *
     * @ORM\Id()
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="userRoles")
     *
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id", nullable=false)
     */
    private ?Site $site = null;

    /**
     * The role of the user in that site.
     *
     * @Groups({"site_post"})
     *
     * @ORM\Column(type="string")
     */
    private string $role = '';

    /**
     * @psalm-mutation-free
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @return $this
     */
    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }
}
