<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller.
 */
class LoginController extends AbstractController
{
    /**
     * @Route("/authentication/login", name="authentication_token")
     */
    public function isReachableAction()
    {
        return new JsonResponse();
    }
}
