<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Dto;

/**
 * Class OperatorInput.
 */
class UserPatchInput
{
    /**
     * @var ?string
     */
    private $username;

    /**
     * @var ?string
     */
    private $name;

    /**
     * @var ?string
     */
    private $surname;

    /**
     * @var ?string
     */
    private $email;

    /**
     * @var ?boolean
     */
    private $active;

    /**
     * @var ?string
     */
    private $avatar;

    /**
     * @var ?string
     */
    private $currentPassword;

    /**
     * @var ?string
     */
    private $password;

    /**
     * @var ?bool
     */
    private $renewPassword;

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     * @return UserPatchInput
     */
    public function setUsername(?string $username): UserPatchInput
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return UserPatchInput
     */
    public function setName(?string $name): UserPatchInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string|null $surname
     * @return UserPatchInput
     */
    public function setSurname(?string $surname): UserPatchInput
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UserPatchInput
     */
    public function setEmail(?string $email): UserPatchInput
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     * @return UserPatchInput
     */
    public function setActive(?bool $active): UserPatchInput
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     * @return UserPatchInput
     */
    public function setAvatar(?string $avatar): UserPatchInput
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrentPassword(): ?string
    {
        return $this->currentPassword;
    }

    /**
     * @param string|null $currentPassword
     * @return UserPatchInput
     */
    public function setCurrentPassword(?string $currentPassword): UserPatchInput
    {
        $this->currentPassword = $currentPassword;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return UserPatchInput
     */
    public function setPassword(?string $password): UserPatchInput
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRenewPassword(): ?bool
    {
        return $this->renewPassword;
    }

    /**
     * @param bool|null $renewPassword
     * @return UserPatchInput
     */
    public function setRenewPassword(?bool $renewPassword): UserPatchInput
    {
        $this->renewPassword = $renewPassword;
        return $this;
    }

}
