<?php

declare(strict_types=1);

namespace Shared\Authentication\ApiTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use Aws\CommandPool;
use Aws\S3\S3Client;
use Exception;
use Shared\Authentication\Dto\UserPatchInput;
use Shared\Authentication\Entity\User;
use Shared\Authentication\Service\EmailNotifier;
use Shared\Authentication\Service\PasswordGenerator;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class UserPatchInputDataTransformer.
 */
class UserPatchInputDataTransformer implements DataTransformerInterface
{

    /**
     * @var bool
     */
    private bool $isLdapOn;

    /**
     * @var bool
     */
    private bool $isS3On;

    /**
     * @var string
     */
    private string $projectFileUploadDir;

    /**
     * @var Security
     */
    private Security $security;

    /**
     * @var EmailNotifier
     */
    private $mailer;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordEncoder;

    private PasswordGenerator $passwordGenerator;

    private S3Client $s3Client;

    private string $s3BucketName;

    private string $projectFileSubDir;


    /**
     * UserPostInputDataTransformer constructor.
     * @param bool $isLdapOn
     */
    public function __construct(
        bool                        $isLdapOn,
        bool                        $isS3On,
        string                      $projectFileUploadDir,
        string                      $s3BucketName,
        string                      $projectFileSubDir,
        S3Client                    $s3Client,
        Security                    $security,
        EmailNotifier               $mailer,
        UserPasswordHasherInterface $encoder,
        PasswordGenerator           $passwordGenerator
    )
    {
        $this->isLdapOn = $isLdapOn;
        $this->isS3On = $isS3On;
        $this->projectFileUploadDir = $projectFileUploadDir;
        $this->security = $security;
        $this->mailer = $mailer;
        $this->passwordEncoder = $encoder;
        $this->passwordGenerator = $passwordGenerator;
        $this->s3Client = $s3Client;
        $this->s3BucketName = $s3BucketName;
        $this->projectFileSubDir = $projectFileSubDir;
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return User::class === $to && null !== ($context['input']['class'] ?? null) && $context['input']['class'] === UserPatchInput::class;
    }

    /**
     * @param UserPatchInput $object
     * @param string $to
     * @param array $context
     * @return User
     * @throws Exception
     * @see DataTransformerInterface
     */
    public function transform($object, string $to, array $context = []): User
    {
        if ($this->isLdapOn) {
            throw new Exception('');
        }
        /** @var User $existingUser */
        $existingUser = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        $needMail = false;
        $generatedPassword = null;
        if ($object->getEmail() !== null && $object->getEmail() !== $existingUser->getEmail()) {
            $existingUser->setEmail($object->getEmail());
            $needMail = true;
        }
        if ($object->getName() !== null && $object->getName() !== $existingUser->getName()) {
            $existingUser->setName($object->getName());
        }
        if ($object->getSurname() !== null && $object->getSurname() !== $existingUser->getSurname()) {
            $existingUser->setSurname($object->getSurname());
        }
        if ($object->getActive() !== null && $existingUser !== $this->security->getUser()) {
            // Avoid an admin to deactivate himself
            $existingUser->setActive($object->getActive());
        }
        if ($object->getUsername() !== null && $object->getUsername() !== $existingUser->getUsername()) {
            if ($this->isS3On) {
                $keys = array_map(
                    fn($doc) => $doc["Key"],
                    $this->s3Client->listObjects([
                        'Bucket' => $this->s3BucketName,
                        'Prefix' => $this->projectFileSubDir . '/' . $existingUser->getUsername()
                    ])->toArray()["Contents"]
                );
                $copyBatch = array_map(
                    fn($key) => $this->s3Client->getCommand('CopyObject', [
                        'Bucket' => $this->s3BucketName,
                        'Key' => implode(
                            $this->projectFileSubDir . '/' . $object->getUsername(),
                            explode($this->projectFileSubDir . '/' . $existingUser->getUsername(), $key, 2)),
                        'CopySource' => "{$this->s3BucketName}/{$key}",
                    ]),
                    $keys
                );

                $removeBatch = array_map(
                    fn($key) => $this->s3Client->getCommand('DeleteObject', [
                        'Bucket' => $this->s3BucketName,
                        'Key' => $key,
                    ]),
                    $keys
                );

                CommandPool::batch($this->s3Client, $copyBatch);
                CommandPool::batch($this->s3Client, $removeBatch);
            } else {
                $fileSystem = new Filesystem();
                $originUserUploadDir = $this->projectFileUploadDir . '/' . $existingUser->getUsername();
                $newUserUploadDir = $this->projectFileUploadDir . '/' . $object->getUsername();
                if ($fileSystem->exists($originUserUploadDir) && !$fileSystem->exists($newUserUploadDir)) {
                    $fileSystem->rename($originUserUploadDir, $newUserUploadDir);
                }
            }

            $existingUser->setUsername($object->getUsername());
            $needMail = true;
        }
        if ($object->getAvatar() !== null && $object->getAvatar() !== $existingUser->getAvatar()) {
            $object->setAvatar($object->getAvatar() === '' ? null : $object->getAvatar());
            $existingUser->setAvatar($object->getAvatar());
        }
        if ($object->getPassword() !== null) {
            if ($object->getCurrentPassword() !== null) {
                if ($this->passwordEncoder->isPasswordValid($existingUser, $object->getCurrentPassword())) {
                    $existingUser->setPassword($this->passwordEncoder->hashPassword($existingUser, $object->getPassword()));
                } else {
                    throw new AccessDeniedHttpException('Current password is incorrect');
                }
            } else {
                throw new UnprocessableEntityHttpException('Current password is needed');
            }

        }
        if ($object->getRenewPassword()) {
            $generatedPassword = $this->passwordGenerator->generate();
            $existingUser->setPassword($this->passwordEncoder->hashPassword($existingUser, $generatedPassword));
            $needMail = true;
        }

        if ($needMail) {
            $this->mailer->notifyUserPassword($existingUser, $generatedPassword);
        }
        return $existingUser;
    }
}
