<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Shared\Authentication\Entity\User;
use Shared\Authentication\Repository\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Class ModifyUserPasswordCommand.
 */
class ModifyUserPasswordCommand extends Command
{
    const ARG_USERNAME = 'user';

    const ARG_PASSWORD = 'password';

    /**
     * @var UserRepository
     */
    private $userRepo;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var bool
     */
    private $isLdapOn;

    /**
     * ModifyUserPasswordCommand constructor.
     *
     * @param ManagerRegistry $managerRegistry
     * @param ParameterBagInterface $params
     * @param UserPasswordHasherInterface $encoder
     */
    public function __construct(ManagerRegistry $managerRegistry,
                                ParameterBagInterface $params,
                                UserPasswordHasherInterface $encoder)
    {
        parent::__construct('adonis:user:password');
        $this->userRepo = $managerRegistry->getRepository(User::class);
        $this->entityManager = $managerRegistry->getManager();
        $this->passwordEncoder = $encoder;
        $this->isLdapOn = $params->get('ldap.auth');
    }

    /**
     * @see Command
     */
    protected function configure()
    {
        $this->setDescription('Set user password');
        $this->addArgument(
            static::ARG_USERNAME,
            InputArgument::REQUIRED,
            'The username of the user');
        $this->addArgument(
            static::ARG_PASSWORD,
            InputArgument::REQUIRED,
            'The new password of the new user');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->isLdapOn) {
            $output->writeln('Disabled as LDAP mode is active');
            return 3;
        }

        $username = $input->getArgument(static::ARG_USERNAME);
        $password = $input->getArgument(static::ARG_PASSWORD);

        /* @var $user User */
        $user = $this->userRepo->findOneBy(['username' => $username]);
        if (empty($user)) {
            $output->writeln(sprintf('No user founded with given username: %s', $username));
            return 2;
        }

        $user->setPassword($this->passwordEncoder->hashPassword($user, $password));
        $this->entityManager->flush();
        $output->writeln('Password changed');
        return 0;
    }
}
