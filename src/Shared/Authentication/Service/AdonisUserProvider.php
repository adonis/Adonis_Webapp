<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Service;

use Doctrine\ORM\EntityManagerInterface;
use Shared\Authentication\Entity\User;
use Shared\Authentication\Repository\UserRepository;
use Symfony\Component\Ldap\Security\LdapUser;
use Symfony\Component\Ldap\Security\LdapUserProvider;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use function get_class;

/**
 * Class AdonisUserProvider.
 */
class AdonisUserProvider implements UserProviderInterface
{
    /**
     * @var LdapUserProvider
     */
    private $ldapUserProvider;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $mailField;

    /**
     * AdonisUserProvider constructor.
     *
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $em
     * @param LdapUserProvider $ldapUserProvider
     * @param string $mailField
     */
    public function __construct(
        UserRepository         $userRepository,
        EntityManagerInterface $em,
        LdapUserProvider       $ldapUserProvider,
        string                 $mailField
    )
    {
        $this->userRepository = $userRepository;
        $this->ldapUserProvider = $ldapUserProvider;
        $this->em = $em;
        $this->mailField = $mailField;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        $ldapUser = new LdapUser($user->getEntry(), $user->getUsername(), $user->getPassword(), $user->getRoles());

        return $this->convertUser($ldapUser);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class): bool
    {
        return User::class === $class;
    }

    protected function convertUser(LdapUser $ldapUser)
    {
        $dbUser = $this->userRepository->findOneBy(['username' => $ldapUser->getUsername()]);
        if (null == $dbUser) {
            $dbUser = new User();
            $dbUser
                ->setUsername($ldapUser->getUserIdentifier())
                ->setEmail($ldapUser->getExtraFields()[$this->mailField]);
            $this->em->persist($dbUser);
        }
        $dbUser->setEntry($ldapUser->getEntry());

        $this->em->flush();

        return $dbUser;
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        /* @var LdapUser $user */
        $user = $this->ldapUserProvider->loadUserByIdentifier($identifier);

        return $this->convertUser($user);
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        throw new UserNotFoundException();
    }
}
