<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Service;

use Exception;

/**
 * Class PasswordGenerator.
 * see :  compermisos/strong-passwords.php
 */
class PasswordGenerator
{
    const MIN_LENGTH = 8;

    /**
     * @var string[]
     */
    private $sets = [
        'abcdefghijklmnopqrstuvwxyz',
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        '1234567890',
        '!@#$%&*?',
    ];

    /**
     * Check if given password respects the password policy of this generator.
     *
     * @param string $password
     * @return bool
     */
    public function check(string $password): bool
    {
        return preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d\W]).*$/', $password) && static::MIN_LENGTH <= strlen($password);
    }

    /**
     * Generate a password with at least one capital letter, one minus letter, one number and one special char.
     *
     * @param int $length
     * @return string
     */
    public function generate($length = 10): string
    {
        // Ensure the password is minimum MIN_LENGTH long.
        $length = static::MIN_LENGTH <= $length ? $length : static::MIN_LENGTH;

        $all = '';
        $password = '';

        // Select at least one of each type.
        foreach ($this->sets as $set) {
            $password .= $set[$this->tweak_array_rand(str_split($set))];
            $all .= $set;
        }

        // Complete randomly.
        $all = str_split($all);
        for ($i = 0; $i < $length - count($this->sets); $i++) {
            $password .= $all[$this->tweak_array_rand($all)];
        }

        $password = str_shuffle($password);

        return $password;
    }

    // Take a array and get random index, same function of array_rand, only difference is
    // intent use secure random algorithm on fail use mersene twistter, and on fail use default array_rand
    private function tweak_array_rand($array): int
    {
        if (function_exists('random_int')) {
            try {
                return random_int(0, count($array) - 1);
            } catch (Exception $e) {
                return array_rand($array);
            }
        } elseif (function_exists('mt_rand')) {
            return mt_rand(0, count($array) - 1);
        } else {
            return array_rand($array);
        }
    }
}
