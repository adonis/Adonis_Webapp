<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Ldap\Exception\ConnectionException;
use Symfony\Component\Ldap\LdapInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\AuthenticatorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

/**
 * Class AdonisLdapAuthenticator.
 */
class AdonisLdapAuthenticator extends AbstractGuardAuthenticator implements AuthenticatorInterface
{
    private $userProvider;
    private $ldap;

    /**
     * @var AuthenticationSuccessHandlerInterface
     */
    private $successHandler;

    /**
     * @var AuthenticationFailureHandlerInterface
     */
    private $failureHandler;

    private $dnString;
    private $searchDn;
    private $searchPassword;

    /**
     * AdonisLdapAuthenticator constructor.
     * @param UserProviderInterface $userProvider
     * @param LdapInterface $ldap
     * @param AuthenticationSuccessHandlerInterface $successHandler
     * @param AuthenticationFailureHandlerInterface $failureHandler
     * @param string $dnString
     * @param string $searchDn
     * @param string $searchPassword
     */
    public function __construct(
        UserProviderInterface $userProvider,
        LdapInterface $ldap,
        AuthenticationSuccessHandlerInterface $successHandler,
        AuthenticationFailureHandlerInterface $failureHandler,
        string $dnString = '{username}',
        string $searchDn = '',
        string $searchPassword = ''
    ) {
        $this->userProvider = $userProvider;
        $this->ldap = $ldap;
        $this->successHandler = $successHandler;
        $this->failureHandler = $failureHandler;
        $this->dnString = $dnString;
        $this->searchDn = $searchDn;
        $this->searchPassword = $searchPassword;
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new Response('Auth header required', 401);
    }

    public function supports(Request $request): bool
    {
        return $request->isMethod('POST');
    }

    public function getCredentials(Request $request): array
    {
        $content = json_decode($request->getContent());
        return [
            'username' => $content->username,
            'password' => $content->password,
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider): ?UserInterface
    {
        return $userProvider->loadUserByIdentifier($credentials['username']);
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        $username = $credentials['username'];
        $password = $credentials['password'];

        if ('' === (string) $password) {
            throw new BadCredentialsException('The presented password must not be empty.');
        }

        try {
            $username = $this->ldap->escape($username, '', LdapInterface::ESCAPE_DN);
            $dn = str_replace('{username}', $username, $this->dnString);
            $this->ldap->bind($dn, $password);
        } catch (ConnectionException $e) {
            throw new BadCredentialsException('The presented password is invalid.');
        }
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return $this->failureHandler->onAuthenticationFailure($request, $exception);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): ?Response
    {
        return $this->successHandler->onAuthenticationSuccess($request, $token);
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }
}
