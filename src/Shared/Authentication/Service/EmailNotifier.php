<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Shared\Authentication\Service;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Shared\Authentication\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * Class EmailNotifier.
 */
class EmailNotifier implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @var string
     */
    private $mailSender;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * EmailNotifier constructor.
     *
     * @param MailerInterface $mailer
     * @param string $mailSender
     * @param TranslatorInterface $translator
     */
    public function __construct(MailerInterface $mailer, string $mailSender, TranslatorInterface $translator)
    {
        $this->mailer = $mailer;
        $this->mailSender = $mailSender;
        $this->translator = $translator;
    }

    /**
     * Send an e-mail to user to inform about the new password.
     *
     * @param User $user
     * @param string|null $password
     */
    public function notifyUserPassword(User $user, string $password = null): void
    {
        try {
            $title = $this->translator->trans('mail.password.title');

            $message = (new TemplatedEmail())
                ->from($this->mailSender)
                ->to($user->getEmail())
                ->subject($title)
                ->htmlTemplate('emails/user-password.html.twig')
                ->context([
                    'user' => $user,
                    'password' => $password
                ]);

            $this->mailer->send($message);
        } catch (Throwable $exception) {
            $this->logger->critical('Error occured while sending email to user', [$exception]);
        }
    }
}
