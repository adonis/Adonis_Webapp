<?php


namespace Shared\Authentication\Voter;

use ApiPlatform\Core\Api\IriConverterInterface;
use Shared\Authentication\Entity\RelUserSite;
use Shared\Authentication\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class UserVoter extends Voter
{
    public const USER_DELETE = 'USER_DELETE';
    public const USER_POST = 'USER_POST';
    public const USER_EDIT = 'USER_EDIT';
    public const USER_GET = 'USER_GET';
    public const USER_GET_COLLECTION = 'USER_GET_COLLECTION';

    private Security $security;

    private IriConverterInterface $iriConverter;

    public function __construct(Security $security, IriConverterInterface $iriConverter)
    {
        $this->security = $security;
        $this->iriConverter = $iriConverter;
    }

    protected function supports($attribute, $subject): bool
    {
        if($attribute === self::USER_GET_COLLECTION && $subject instanceof Request){
            return true;
        }else{
            $supportsAttribute = in_array($attribute, [self::USER_DELETE, self::USER_EDIT, self::USER_GET, self::USER_POST]);
            $supportsSubject = $subject instanceof User;
            return $supportsAttribute && $supportsSubject;
        }

    }

    /**
     * @param string $attribute
     * @param User | Request $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$token->isAuthenticated()) {
            return false;
        }
        switch ($attribute) {

            case self::USER_GET_COLLECTION:
                if (
                    $this->security->isGranted('ROLE_ADMIN') ||
                    $this->security->isGranted(SiteRoleVoter::ROLE_SITE_ADMIN) ||
                    $this->security->isGranted(SiteRoleVoter::ROLE_EXPERIMENTER, $this->iriConverter->getItemFromIri($subject->get('siteRoles_site')))
                ) {
                    return true;
                }
                break;
            case self::USER_POST:
                if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted(SiteRoleVoter::ROLE_SITE_ADMIN)) {
                    return true;
                }
                break;
            case self::USER_EDIT:
            case self::USER_GET:
                if ($this->security->isGranted(self::USER_POST, $subject) ||
                    $this->security->getUser() === $subject ||
                    count(array_intersect(
                        array_map(fn(RelUserSite $e) => $e->getSite()->getId(), $subject->getSiteRoles()->toArray()),
                        array_map(fn(RelUserSite $e) => $e->getSite()->getId(), $this->security->getUser()->getSiteRoles()->toArray())
                    )) > 0
                ){
                    return true;
                }
                break;
            case self::USER_DELETE:
                if ($this->security->isGranted('ROLE_ADMIN') && $subject->getSiteRoles()->count() === 0) {
                    return true;
                }
                break;
        }

        return false;
    }
}
