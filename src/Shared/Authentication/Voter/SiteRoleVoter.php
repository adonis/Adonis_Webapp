<?php


namespace Shared\Authentication\Voter;

use Shared\Authentication\Entity\RelUserSite;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class SiteRoleVoter extends Voter
{

    public const ROLE_SITE_ADMIN = 'ROLE_SITE_ADMIN';
    public const ROLE_EXPERIMENTER = 'ROLE_EXPERIMENTER';
    public const ROLE_PLATFORM_MANAGER = 'ROLE_PLATFORM_MANAGER';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = in_array($attribute, [self::ROLE_SITE_ADMIN, self::ROLE_EXPERIMENTER, self::ROLE_PLATFORM_MANAGER, ]);
        $supportsSubject = is_null($subject) || $subject instanceof Site;
        return $supportsAttribute && $supportsSubject;
    }

    /**
     * @param string $attribute
     * @param Site $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var User $user */
        $user = $this->security->getUser();
        switch ($attribute) {
            case self::ROLE_SITE_ADMIN:
                if ($this->isSiteAdmin($user, $subject)) {
                    return true;
                }
                break;
            case self::ROLE_PLATFORM_MANAGER:
                if ($this->isSitePlatformManager($user, $subject)) {
                    return true;
                }
                break;
            case self::ROLE_EXPERIMENTER:
                if ($this->isSiteExperimenter($user, $subject)) {
                    return true;
                }
                break;
        }
        return false;
    }

    private function isSiteAdmin(User $user, ?Site $site)
    {
        return $user->getSiteRoles()->exists(function (int $i, RelUserSite $rel) use ($site) {
            return $rel->getRole() === RelUserSite::SITE_ADMIN && (is_null($site) || $rel->getSite() === $site);
        });
    }

    private function isSitePlatformManager(User $user, ?Site $site)
    {
        return $user->getSiteRoles()->exists(function (int $i, RelUserSite $rel) use ($site) {
            return ($rel->getRole() === RelUserSite::SITE_ADMIN || $rel->getRole() === RelUserSite::PLATFORM_MANAGER) &&
                (is_null($site) || $rel->getSite() === $site);
        });
    }

    private function isSiteExperimenter(User $user, ?Site $site)
    {
        return $user->getSiteRoles()->exists(function (int $i, RelUserSite $rel) use ($site) {
            return ($rel->getRole() === RelUserSite::EXPERIMENTER || $rel->getRole() === RelUserSite::SITE_ADMIN || $rel->getRole() === RelUserSite::PLATFORM_MANAGER) &&
                (is_null($site) || $rel->getSite() === $site);
        });
    }
}