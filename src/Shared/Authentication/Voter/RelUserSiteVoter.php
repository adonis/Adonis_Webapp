<?php


namespace Shared\Authentication\Voter;

use Shared\Authentication\Entity\RelUserSite;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class RelUserSiteVoter extends Voter
{
    public const REL_USER_SITE_EDIT = 'REL_USER_SITE_EDIT';
    public const REL_USER_SITE_GET = 'REL_USER_SITE_GET';
    public const REL_USER_SITE_POST = 'REL_USER_SITE_POST';
    public const REL_USER_SITE_DELETE = 'REL_USER_SITE_DELETE';

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsAttribute = in_array($attribute, [self::REL_USER_SITE_EDIT, self::REL_USER_SITE_GET, self::REL_USER_SITE_POST, self::REL_USER_SITE_DELETE]);
        $supportsSubject = $subject instanceof RelUserSite;
        return $supportsAttribute && $supportsSubject;
    }

    /**
     * @param string $attribute
     * @param RelUserSite $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$token->isAuthenticated()) {
            return false;
        }

        switch ($attribute) {
            case self::REL_USER_SITE_GET:
                if ($this->security->getUser() === $subject->getUser() || $this->security->isGranted('ROLE_ADMIN')) {
                    return true;
                }
                break;
            case self::REL_USER_SITE_DELETE:
                if ($this->security->isGranted(self::REL_USER_SITE_EDIT, $subject) && $subject->getUser() !== $this->security->getUser()) {
                    return true;
                }
                break;
            case self::REL_USER_SITE_EDIT:
            case self::REL_USER_SITE_POST:
                if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_SITE_ADMIN', $subject->getSite())) {
                    return true;
                }
                break;
        }

        return false;
    }
}
