<?php


namespace Shared\Authentication\Extension;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Symfony\Component\Security\Core\Security;

class SiteGetExtension implements QueryCollectionExtensionInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        /** @var User $user */
        $user = $this->security->getUser();
        if (Site::class !== $resourceClass || $this->security->isGranted('ROLE_ADMIN') || null === $user) {
            return;
        }
        $rootAlias = $queryBuilder->getRootAliases()[0];
        $queryBuilder
            ->innerJoin(sprintf('%s.userRoles', $rootAlias), 'rel_user_site')
            ->andWhere('rel_user_site.user = :current_user')
            ->setParameter('current_user', $user->getId());
    }
}