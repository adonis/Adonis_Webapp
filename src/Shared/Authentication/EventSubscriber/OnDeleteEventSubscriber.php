<?php


namespace Shared\Authentication\EventSubscriber;


use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use ApiPlatform\Core\Exception\ItemNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OnDeleteEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    private IriConverterInterface $iriConverter;

    public function __construct(EntityManagerInterface $em, IriConverterInterface $iriConverter)
    {
        $this->em = $em;
        $this->iriConverter = $iriConverter;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['disableSoftDeleteFilter', EventPriorities::PRE_READ],
        ];
    }

    public function disableSoftDeleteFilter(RequestEvent $event): void
    {
        $method = $event->getRequest()->getMethod();
        if (Request::METHOD_DELETE !== $method ) {
            return;
        }
        try {
            $this->iriConverter->getItemFromIri($event->getRequest()->getRequestUri());
        }catch (ItemNotFoundException $e){
            if($this->em->getFilters()->isEnabled('soft_deleteable')){
                $this->em->getFilters()->disable('soft_deleteable');
            }
        } catch (InvalidArgumentException $e){
            // Do nothing, triggered on custom collection DELETE
        }
    }
}
