<?php


namespace Shared\Authentication\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use Shared\Authentication\Entity\User;
use Shared\Authentication\Service\EmailNotifier;
use Shared\Authentication\Service\PasswordGenerator;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserUpdateEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var PasswordGenerator
     */
    private $passwordGenerator;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordEncoder;

    /**
     * @var EmailNotifier
     */
    private $mailer;

    public function __construct(
        PasswordGenerator $generator,
        UserPasswordHasherInterface $encoder,
        EmailNotifier $mailer
    )
    {
        $this->passwordGenerator = $generator;
        $this->passwordEncoder = $encoder;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['updateUserPassword', EventPriorities::PRE_WRITE],
        ];
    }

    public function updateUserPassword(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$user instanceof User || Request::METHOD_POST !== $method) {
            return;
        }

        $generatedPassword = $this->passwordGenerator->generate();
        $user->setPassword($this->passwordEncoder->hashPassword($user, $generatedPassword));
        $this->mailer->notifyUserPassword($user, $generatedPassword);

    }
}
