<?php


namespace Shared\Authentication\CustomFilters;


use ApiPlatform\Core\Serializer\Filter\FilterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class DeletedFilter implements FilterInterface
{

    private string $parameterName;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, string $parameterName = "deleted")
    {
        $this->parameterName = $parameterName;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        return [
            "$this->parameterName" => [
                'property' => null,
                'type' => 'bool',
                'is_collection' => false,
                'required' => false,
            ],
        ];
    }

    public function apply(Request $request, bool $normalization, array $attributes, array &$context)
    {
        if (array_key_exists($this->parameterName, $commonAttribute = $request->attributes->get('_api_filters', []))) {
            $deleted = $commonAttribute[$this->parameterName] === "true";
        } else {
            $deleted = ($request->query->all()[$this->parameterName] ?? null) === "true";
        }

        if (!$deleted) {
            return;
        }
        if($this->em->getFilters()->isEnabled('soft_deleteable')){
            $this->em->getFilters()->disable('soft_deleteable');
        }
    }
}