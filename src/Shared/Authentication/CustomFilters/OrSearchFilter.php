<?php


namespace Shared\Authentication\CustomFilters;


use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryBuilderHelper;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\ORM\QueryBuilder;

class OrSearchFilter extends SearchFilter
{
    private const PROPERTY_NAME_PREFIX = 'search_';

    /**
     * {@inheritdoc}
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if (0 !== strpos($property, self::PROPERTY_NAME_PREFIX)) {
            return;
        }

        if (false === isset($this->properties[$property])) {
            return;
        }

        $values = $this->normalizeValues((array) $value, $property);
        if (null === $values) {
            return;
        }

        $orExpressions = [];

        foreach ($values as $index => $value) {
            foreach ($this->properties[$property] as $propertyName => $strategy) {
                $strategy = $strategy ?? self::STRATEGY_EXACT;
                $alias = $queryBuilder->getRootAliases()[0];
                $field = $propertyName;

                $associations = [];
                if ($this->isPropertyNested($propertyName, $resourceClass)) {
                    [$alias, $field, $associations] = $this->addJoinsForNestedProperty($propertyName, $alias, $queryBuilder, $queryNameGenerator, $resourceClass);
                }

                $caseSensitive = true;
                $trimmed = false;
                $unaccent = false;
                $metadata = $this->getNestedMetadata($resourceClass, $associations);

                if ($metadata->hasField($field)) {
                    if ('id' === $field) {
                        $value = $this->getIdFromValue($value);
                    }

                    if (!$this->hasValidValues((array)$value, $this->getDoctrineFieldType($propertyName, $resourceClass))) {
                        $this->logger->notice('Invalid filter ignored', [
                            'exception' => new InvalidArgumentException(sprintf('Values for field "%s" are not valid according to the doctrine type.', $field)),
                        ]);
                        continue;
                    }

                    // prefixing the strategy with u makes it unaccent
                    if (0 === strpos($strategy, 'u')) {
                        $strategy = substr($strategy, 1);
                        $unaccent = true;
                    }

                    // prefixing the strategy with t makes it trimmed
                    if (0 === strpos($strategy, 't')) {
                        $strategy = substr($strategy, 1);
                        $trimmed = true;
                    }

                    // prefixing the strategy with i makes it case insensitive
                    if (0 === strpos($strategy, 'i')) {
                        $strategy = substr($strategy, 1);
                        $caseSensitive = false;
                    }

                    $orExpressions[] = $this->addWhereByStrategy($strategy, $queryBuilder, $queryNameGenerator, $alias, $field, $value, $caseSensitive, $trimmed, $unaccent);
                }
                // metadata doesn't have the field, nor an association on the field
                if (!$metadata->hasAssociation($field)) {
                    continue;
                }

                $values = array_map([$this, 'getIdFromValue'], $values);
                $associationFieldIdentifier = 'id';
                $doctrineTypeField = $this->getDoctrineFieldType($property, $resourceClass);

                if (null !== $this->identifiersExtractor) {
                    $associationResourceClass = $metadata->getAssociationTargetClass($field);
                    $associationFieldIdentifier = $this->identifiersExtractor->getIdentifiersFromResourceClass($associationResourceClass)[0];
                    $doctrineTypeField = $this->getDoctrineFieldType($associationFieldIdentifier, $associationResourceClass);
                }

                if (!$this->hasValidValues($values, $doctrineTypeField)) {
                    $this->logger->notice('Invalid filter ignored', [
                        'exception' => new InvalidArgumentException(sprintf('Values for field "%s" are not valid according to the doctrine type.', $field)),
                    ]);

                    continue;
                }

                $associationAlias = $alias;
                $associationField = $field;
                if ($metadata->isCollectionValuedAssociation($associationField) || $metadata->isAssociationInverseSide($field)) {
                    $associationAlias = QueryBuilderHelper::addJoinOnce($queryBuilder, $queryNameGenerator, $alias, $associationField);
                    $associationField = $associationFieldIdentifier;
                }

                $orExpressions[] = $this->addWhereByStrategy($strategy, $queryBuilder, $queryNameGenerator, $associationAlias, $associationField, $values, $caseSensitive);
            }
        }

        $queryBuilder->andWhere($queryBuilder->expr()->orX(...$orExpressions));
    }

    /**
     * {@inheritDoc}
     */
    protected function addWhereByStrategy(string $strategy, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, $value, bool $caseSensitive, bool $trimmed = false, bool $unaccent = false)
    {
        $wrapCase = $this->createWrapCase($caseSensitive, $trimmed, $unaccent);
        $valueParameter = $queryNameGenerator->generateParameterName($field);
        $exprBuilder = $queryBuilder->expr();

        $queryBuilder->setParameter($valueParameter, $value);

        switch ($strategy) {
            case null:
            case self::STRATEGY_EXACT:
                return $exprBuilder->eq($wrapCase("$alias.$field"), $wrapCase(":$valueParameter"));
            case self::STRATEGY_PARTIAL:
                return $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat("'%'", $wrapCase(":$valueParameter"), "'%'"));
            case self::STRATEGY_START:
                return $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat($wrapCase(":$valueParameter"), "'%'"));
            case self::STRATEGY_END:
                return $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat("'%'", $wrapCase(":$valueParameter")));
            case self::STRATEGY_WORD_START:
                return $exprBuilder->orX(
                    $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat($wrapCase(":$valueParameter"), "'%'")),
                    $exprBuilder->like($wrapCase("$alias.$field"), $exprBuilder->concat("'%'", $wrapCase(":$valueParameter")))
                );
            default:
                throw new InvalidArgumentException(sprintf('strategy %s does not exist.', $strategy));
        }
    }

    protected function createWrapCase(bool $caseSensitive, bool $trimmed = false, bool $unaccent = false): \Closure
    {
        return static function (string $expr) use ($unaccent, $trimmed, $caseSensitive): string {
            $res = $expr;
            if($trimmed){
                $res = sprintf('REPLACE(%s, \' \', \'\')', $res);
            }

            if (!$caseSensitive) {
                $res = sprintf('LOWER(%s)', $res);
            }

            if ($unaccent) {
                $res = sprintf('UNACCENT(%s)', $res);
            }

            return $res;
        };
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(string $resourceClass): array
    {
        $descriptions = [];

        foreach ($this->properties as $filterName => $properties) {
            $propertyNames = [];

            foreach ($properties as $property => $strategy) {
                if (!$this->isPropertyMapped($property, $resourceClass, true)) {
                    continue;
                }

                $propertyNames[] = $this->normalizePropertyName($property);
            }

            $filterParameterName = $filterName . '[]';
            $descriptions[$filterParameterName] = [
                'property' => $filterName,
                'type' => 'string',
                'required' => false,
                'is_collection' => true,
                'openapi' => [
                    'description' => 'Search involves the fields: ' . implode(', ', $propertyNames),
                ],
            ];
        }

        return $descriptions;
    }
}
