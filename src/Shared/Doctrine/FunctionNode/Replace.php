<?php


namespace Shared\Doctrine\FunctionNode;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class Replace extends FunctionNode
{
    private $string1;
    private $string2;
    private $string3;

    public function getSql(SqlWalker $sqlWalker): string
    {
        return 'REPLACE(' . $this->string1->dispatch($sqlWalker) . ',' . $this->string2->dispatch($sqlWalker) . ',' .$this->string3->dispatch($sqlWalker) . ')';
    }

    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->string1 = $parser->StringPrimary();

        $parser->match(Lexer::T_COMMA);

        $this->string2 = $parser->StringPrimary();

        $parser->match(Lexer::T_COMMA);

        $this->string3 = $parser->StringPrimary();

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }


}
