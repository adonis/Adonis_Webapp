<?php

/*
 * @author TRYDEA - 2024
 */

namespace Shared\FileManagement\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Shared\Exception\NotInitializedPropertyException;

/**
 * @ORM\MappedSuperclass()
 */
abstract class UserLinkedJob extends IdentifiedEntity
{
    public const STATUS_ERROR = 'error';
    public const STATUS_RUNNING = 'running';
    public const STATUS_SUCCESS = 'success';
    public const STATUS_PENDING = 'pending';

    /**
     * @ORM\Column(type="string")
     */
    protected string $status = '';

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected ?User $user = null;

    /**
     * @psalm-mutation-free
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUser(): User
    {
        if (null === $this->user) {
            throw new NotInitializedPropertyException(self::class, 'user');
        }

        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
