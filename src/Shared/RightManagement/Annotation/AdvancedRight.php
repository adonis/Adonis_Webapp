<?php


namespace Shared\RightManagement\Annotation;


use Doctrine\Common\Annotations\Annotation;

/**
 * Class AdvancedRight
 * @package Shared\RightManagement\Annotation
 * @Annotation
 * @Target("CLASS")
 */
class AdvancedRight
{
    /**
     * @var string Unique identifier used in the database to identify the class. Any modification of this value must be done with a db update
     */
    public string $classIdentifier;

    /**
     *
     * @var string field name of the owner field
     */
    public string $ownerField;

    /**
     * @var array define witch field is a parent field
     */
    public array $parentFields = [];

    public ?string $siteAttribute = null;
}
