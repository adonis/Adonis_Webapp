<?php


namespace Shared\RightManagement\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Shared\Authentication\Entity\Site;
use Shared\RightManagement\SQLFilter\AdvancedRightFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class DeleteSiteAction
{
    private EntityManagerInterface $entityManager;
    private Security $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function __invoke(Request $request, Site $data)
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $this->entityManager->getFilters()->disable(AdvancedRightFilter::NAME);
            $this->entityManager->getFilters()->disable('graphically_deletable');
        }
        return $data;
    }
}
