<?php


namespace Shared\RightManagement\Controller;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Paginator;
use Doctrine\ORM\EntityManagerInterface;

class DeleteRightAction
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke($data)
    {
        if ($data instanceof Paginator) {
            $batchSize = 200;
            $i = 1;
            foreach ($data->getIterator() as $entity) {
                $this->entityManager->remove($entity);
                if (($i % $batchSize) === 0) {
                    $this->entityManager->flush(); // Executes all deletions.
                    $this->entityManager->clear(); // Detaches all objects from Doctrine!
                }
            }
            $this->entityManager->flush();
        }
        return $data;
    }
}
