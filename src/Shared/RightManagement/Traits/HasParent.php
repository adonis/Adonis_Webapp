<?php


namespace Shared\RightManagement\Traits;


use Doctrine\ORM\Mapping as ORM;

trait HasParent
{
    /**
     * @var bool Disable the filter if the entity have a parent
     *
     * @ORM\Column(type="boolean")
     */
    private bool $hasParent = false;

    /**
     * @return bool
     */
    public function isHasParent(): bool
    {
        return $this->hasParent;
    }

    /**
     * @param bool $hasParent
     */
    public function setHasParent(bool $hasParent)
    {
        $this->hasParent = $hasParent;
    }

}
