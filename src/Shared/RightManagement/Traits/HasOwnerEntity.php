<?php

namespace Shared\RightManagement\Traits;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\User;

trait HasOwnerEntity
{
    /**
     * @var ?User the owner of the entity
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     */
    private ?User $owner = null;

    /**
     * @psalm-mutation-free
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @return $this
     */
    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
