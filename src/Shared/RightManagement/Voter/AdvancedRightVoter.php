<?php


namespace Shared\RightManagement\Voter;

use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use Shared\RightManagement\Annotation\AdvancedRight;
use Shared\RightManagement\Entity\AbstractAdvancedRight;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class AdvancedRightVoter extends Voter
{
    public const ADVANCED_RIGHT_POST = 'ADVANCED_RIGHT_POST';
    public const ADVANCED_RIGHT_DELETE_ALL = 'ADVANCED_RIGHT_DELETE_ALL';

    private Security $security;

    private Reader $annotationReader;

    private EntityManagerInterface $entityManager;

    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(Security $security, Reader $annotationReader, EntityManagerInterface $entityManager, PropertyAccessorInterface $propertyAccessor)
    {
        $this->security = $security;
        $this->annotationReader = $annotationReader;
        $this->entityManager = $entityManager;
        $this->propertyAccessor = $propertyAccessor;
    }

    protected function supports($attribute, $subject): bool
    {
        $supportsDeleteAll = $attribute === self::ADVANCED_RIGHT_DELETE_ALL &&  $subject instanceof Request;
        $supportsPost = $subject instanceof AbstractAdvancedRight && $attribute === self::ADVANCED_RIGHT_POST;
        return $supportsDeleteAll || $supportsPost;
    }

    /**
     * @param string $attribute
     * @param AbstractAdvancedRight | Request $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        if (!$token->isAuthenticated()) {
            return false;
        }
        switch ($attribute) {

            case self::ADVANCED_RIGHT_POST:
                if ($this->isUserOwnerOfSubject($subject->getObjectId(), $subject->getClassIdentifier())) {
                    return true;
                }
                break;

            case self::ADVANCED_RIGHT_DELETE_ALL:
                if ($this->isUserOwnerOfSubject($subject->get('objectId'), $subject->get('classIdentifier'))) {
                    return true;
                }
                break;

        }

        return false;
    }

    private function isUserOwnerOfSubject($objectId, $classIdentifier){
        foreach ($this->entityManager->getMetadataFactory()->getAllMetadata() as $metadata){
            /** @var AdvancedRight $annotation */
            $annotation = $this->annotationReader->getClassAnnotation($metadata->getReflectionClass(), 'Shared\\RightManagement\\Annotation\\AdvancedRight');
            if($annotation !== null && isset($annotation->classIdentifier) && $annotation->classIdentifier === $classIdentifier){
                $entity = $this->entityManager->getRepository($metadata->getName())->find($objectId);
                return $entity !== null && $this->propertyAccessor->getValue($entity, $annotation->ownerField) === $this->security->getUser();
            }
        }
        return false;
    }
}
