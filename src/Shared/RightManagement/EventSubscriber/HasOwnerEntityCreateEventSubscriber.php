<?php


namespace Shared\RightManagement\EventSubscriber;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use ReflectionClass;
use Shared\RightManagement\Annotation\AdvancedRight;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Security\Core\Security;

class HasOwnerEntityCreateEventSubscriber implements EventSubscriber
{

    private Security $security;
    private Reader $annotationReader;
    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(
        Security $security,
        Reader $annotationReader,
        PropertyAccessorInterface $propertyAccessor
    )
    {
        $this->security = $security;
        $this->annotationReader = $annotationReader;
        $this->propertyAccessor = $propertyAccessor;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $object = $event->getObject();
        /** @var AdvancedRight $annotation */
        $annotation = $this->annotationReader->getClassAnnotation(new ReflectionClass($object), 'Shared\\RightManagement\\Annotation\\AdvancedRight');
        if(!$annotation){
            return;
        }
        if(isset($annotation->ownerField) && null === $this->propertyAccessor->getValue($object, $annotation->ownerField)){
            $this->propertyAccessor->setValue($object, $annotation->ownerField, $this->security->getUser());
        }
    }

}
