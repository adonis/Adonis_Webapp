<?php


namespace Shared\RightManagement\EventSubscriber;


use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\QuoteStrategy;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;

class FilterConfigurator implements EventSubscriber
{
    protected $em;
    protected $security;
    protected $annotationReader;
    protected $quoteStrategy;
    protected $requestStack;
    protected $iriConverter;

    public function __construct(EntityManager $em, Security $security, Reader $annotationReader, QuoteStrategy $quoteStrategy, RequestStack $requestStack, IriConverterInterface $iriConverter)
    {
        $this->em = $em;
        $this->security = $security;
        $this->annotationReader = $annotationReader;
        $this->quoteStrategy = $quoteStrategy;
        $this->requestStack = $requestStack;
        $this->iriConverter = $iriConverter;
    }

    public function loadClassMetadata($event)
    {
        // Leave empty, this listener is only here to inject SQL filter dependencies
    }

    /**
     * @return EntityManager
     */
    public function getEm(): EntityManager
    {
        return $this->em;
    }

    /**
     * @return Security
     */
    public function getSecurity(): Security
    {
        return $this->security;
    }

    /**
     * @return QuoteStrategy
     */
    public function getQuoteStrategy(): QuoteStrategy
    {
        return $this->quoteStrategy;
    }

    /**
     * @return Reader
     */
    public function getAnnotationReader(): Reader
    {
        return $this->annotationReader;
    }

    /**
     * @return RequestStack
     */
    public function getRequestStack(): RequestStack
    {
        return $this->requestStack;
    }

    /**
     * @return IriConverterInterface
     */
    public function getIriConverter(): IriConverterInterface
    {
        return $this->iriConverter;
    }

    public function getSubscribedEvents(): array
    {
        return [
            'loadClassMetadata',
        ];
    }
}
