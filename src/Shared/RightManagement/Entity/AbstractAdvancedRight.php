<?php

namespace Shared\RightManagement\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 */
class AbstractAdvancedRight
{
    public const ADVANCED_RIGHT_EDIT = 'edit';
    public const ADVANCED_RIGHT_VIEW = 'view';

    /**
     * @ORM\Column(name="class_identifier", type="string")
     *
     * @ORM\Id
     */
    private string $classIdentifier = '';

    /**
     * @ORM\Column(name="object_id", type="integer")
     *
     * @ORM\Id
     */
    private int $objectId = 0;

    /**
     * @ORM\Column(name="ad_right", type="string")
     */
    private string $right = '';

    /**
     * @psalm-mutation-free
     */
    public function getClassIdentifier(): string
    {
        return $this->classIdentifier;
    }

    /**
     * @return $this
     */
    public function setClassIdentifier(string $classIdentifier): self
    {
        $this->classIdentifier = $classIdentifier;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return $this
     */
    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRight(): string
    {
        return $this->right;
    }

    /**
     * @return $this
     */
    public function setRight(string $right): self
    {
        $this->right = $right;

        return $this;
    }
}
