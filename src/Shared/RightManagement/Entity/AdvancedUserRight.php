<?php

namespace Shared\RightManagement\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\RightManagement\Controller\DeleteRightAction;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ADVANCED_RIGHT_POST', object)",
 *          },
 *         "delete"={
 *           "security"="is_granted('ADVANCED_RIGHT_DELETE_ALL', request)",
 *           "method" = "DELETE",
 *           "controller" = DeleteRightAction::class,
 *           "openapi_context" = {
 *              "parameters" = {
 *                   {
 *                      "name": "classIdentifier",
 *                      "type": "integer",
 *                      "in": "query",
 *                      "required": false,
 *                      "description": "",
 *                      "example": ""
 *                   },
 *                   {
 *                      "name": "objectId",
 *                      "type": "integer",
 *                      "in": "query",
 *                      "required": false,
 *                      "description": "",
 *                      "example": ""
 *                   },
 *              }
 *           }
 *         },
 *     },
 *     itemOperations={
 *         "get"={},
 *         "delete"={"security"="is_granted('ADVANCED_RIGHT_POST', object)"},
 *     },
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={
 *     "classIdentifier": "exact",
 *     "objectId": "exact"
 * })
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="advanced_user_right", schema="shared")
 */
class AdvancedUserRight extends AbstractAdvancedRight
{
    /**
     * @ORM\Column(name="user_id", type="integer")
     *
     * @ORM\Id
     */
    private int $userId = 0;

    /**
     * @psalm-mutation-free
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return $this
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }
}
