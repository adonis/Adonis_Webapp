<?php

namespace Shared\RightManagement\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\RightManagement\Controller\DeleteRightAction;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ADVANCED_RIGHT_POST', object)",
 *          },
 *         "delete"={
 *           "security"="is_granted('ADVANCED_RIGHT_DELETE_ALL', request)",
 *           "method" = "DELETE",
 *           "controller" = DeleteRightAction::class,
 *           "openapi_context" = {
 *              "parameters" = {
 *                   {
 *                      "name": "classIdentifier",
 *                      "type": "integer",
 *                      "in": "query",
 *                      "required": false,
 *                      "description": "",
 *                      "example": ""
 *                   },
 *                   {
 *                      "name": "objectId",
 *                      "type": "integer",
 *                      "in": "query",
 *                      "required": false,
 *                      "description": "",
 *                      "example": ""
 *                   },
 *              }
 *           }
 *         },
 *     },
 *     itemOperations={
 *         "get"={},
 *         "delete"={"security"="is_granted('ADVANCED_RIGHT_POST', object)"},
 *     },
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={
 *     "classIdentifier": "exact",
 *     "objectId": "exact"
 * })
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="advanced_group_right", schema="shared")
 */
class AdvancedGroupRight extends AbstractAdvancedRight
{
    /**
     * @ORM\Column(name="group_id", type="integer")
     *
     * @ORM\Id
     */
    private int $groupId = 0;

    /**
     * @psalm-mutation-free
     */
    public function getGroupId(): int
    {
        return $this->groupId;
    }

    /**
     * @return $this
     */
    public function setGroupId(int $groupId): self
    {
        $this->groupId = $groupId;

        return $this;
    }
}
