<?php

namespace Shared\Utils;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ArrayCollectionUtils
{
    /**
     * @template T
     * @template TSourceKey of array-key
     * @template TTargetKey of array-key
     *
     * @param Collection<TSourceKey, T> $source
     * @param iterable<TTargetKey, T>   $target
     * @param Closure(T):void           $itemCallback
     */
    public static function update(Collection $source, iterable $target, ?\Closure $itemCallback = null): void
    {
        $target = new ArrayCollection(is_array($target) ? $target : iterator_to_array($target));

        $valuesToRemove = $source->filter(/** @param T $value */ fn ($value): bool => !$target->contains($value));
        foreach ($valuesToRemove as $value) {
            $source->removeElement($value);
        }

        $valuesToAdd = $target->filter(/** @param T $value */ fn ($value): bool => !$source->contains($value));
        foreach ($valuesToAdd as $value) {
            $source->add($value);
            if (null !== $itemCallback) {
                $itemCallback($value);
            }
        }
    }
}
