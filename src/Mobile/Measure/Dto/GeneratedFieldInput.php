<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto;

/**
 * Class GeneratedFieldInput.
 */
class GeneratedFieldInput
{
    /**
     * @var int
     */
    private $index;

    /**
     * @var string
     */
    private $prefix;

    /**
     * @var bool
     */
    private $numeralIncrement;

    /**
     * @var FormFieldInput[]
     */
    private $formFields;

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @param int $index
     * @return GeneratedFieldInput
     */
    public function setIndex(int $index): GeneratedFieldInput
    {
        $this->index = $index;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return GeneratedFieldInput
     */
    public function setPrefix(string $prefix): GeneratedFieldInput
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNumeralIncrement(): bool
    {
        return $this->numeralIncrement;
    }

    /**
     * @param bool $numeralIncrement
     * @return GeneratedFieldInput
     */
    public function setNumeralIncrement(bool $numeralIncrement): GeneratedFieldInput
    {
        $this->numeralIncrement = $numeralIncrement;
        return $this;
    }

    /**
     * @return FormFieldInput[]
     */
    public function getFormFields(): array
    {
        return $this->formFields;
    }

    /**
     * @param FormFieldInput[] $formFields
     * @return GeneratedFieldInput
     */
    public function setFormFields(array $formFields): GeneratedFieldInput
    {
        $this->formFields = $formFields;
        return $this;
    }
}
