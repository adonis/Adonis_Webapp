<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto;

/**
 * Class FormFieldInput.
 */
class FormFieldInput
{
    /**
     * @var string
     */
    private $targetUri;

    /**
     * @var string
     */
    private $variableUri;

    /**
     * @var MeasureInput[]
     */
    private $measures;

    /**
     * @var GeneratedFieldInput[]|null
     */
    private $generatedFields;

    /**
     * @return string
     */
    public function getTargetUri(): string
    {
        return $this->targetUri;
    }

    /**
     * @param string $targetUri
     * @return FormFieldInput
     */
    public function setTargetUri(string $targetUri): FormFieldInput
    {
        $this->targetUri = $targetUri;
        return $this;
    }

    /**
     * @return string
     */
    public function getVariableUri(): string
    {
        return $this->variableUri;
    }

    /**
     * @param string $variableUri
     * @return FormFieldInput
     */
    public function setVariableUri(string $variableUri): FormFieldInput
    {
        $this->variableUri = $variableUri;
        return $this;
    }

    /**
     * @return MeasureInput[]
     */
    public function getMeasures(): array
    {
        return $this->measures;
    }

    /**
     * @param MeasureInput[] $measures
     * @return FormFieldInput
     */
    public function setMeasures(array $measures): FormFieldInput
    {
        $this->measures = $measures;
        return $this;
    }

    /**
     * @return GeneratedFieldInput[]|null
     */
    public function getGeneratedFields(): ?array
    {
        return $this->generatedFields;
    }

    /**
     * @param GeneratedFieldInput[]|null $generatedFields
     * @return FormFieldInput
     */
    public function setGeneratedFields(?array $generatedFields): FormFieldInput
    {
        $this->generatedFields = $generatedFields;
        return $this;
    }
}
