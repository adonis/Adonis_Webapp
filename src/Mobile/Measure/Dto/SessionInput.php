<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto;

use DateTime;

/**
 * Class SessionInput.
 */
class SessionInput
{
    /**
     * @var DateTime
     */
    private $startedAt;

    /**
     * @var DateTime
     */
    private $endedAt;

    /**
     * @var FormFieldInput[]
     */
    private $formFields;

    /**
     * @return DateTime
     */
    public function getStartedAt(): DateTime
    {
        return $this->startedAt;
    }

    /**
     * @param DateTime $startedAt
     */
    public function setStartedAt(DateTime $startedAt): void
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return DateTime
     */
    public function getEndedAt(): DateTime
    {
        return $this->endedAt;
    }

    /**
     * @param DateTime $endedAt
     */
    public function setEndedAt(DateTime $endedAt): void
    {
        $this->endedAt = $endedAt;
    }

    /**
     * @return FormFieldInput[]
     */
    public function getFormFields(): array
    {
        return $this->formFields;
    }

    /**
     * @param FormFieldInput[] $formFields
     */
    public function setFormFields(array $formFields): void
    {
        $this->formFields = $formFields;
    }
}
