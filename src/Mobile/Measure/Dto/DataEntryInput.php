<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto;

use DateTime;

/**
 * Class DataEntrInput.
 */
class DataEntryInput
{
    const STATUS_NEW = 'new';
    const STATUS_WIP = 'wip';
    const STATUS_DONE = 'done';

    /**
     * @var string
     */
    private $status;

    /**
     * @var DateTime
     */
    private $startedAt;

    /**
     * @var DateTime|null
     */
    private $endedAt;

    /**
     * @var string|null
     */
    private $currentPosition;

    /**
     * @var string[]|null
     */
    private $workpath;

    /**
     * @var SessionInput[]
     */
    private $sessions;

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return DataEntryInput
     */
    public function setStatus(string $status): DataEntryInput
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getStartedAt(): DateTime
    {
        return $this->startedAt;
    }

    /**
     * @param DateTime $startedAt
     * @return DataEntryInput
     */
    public function setStartedAt(DateTime $startedAt): DataEntryInput
    {
        $this->startedAt = $startedAt;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getEndedAt(): ?DateTime
    {
        return $this->endedAt;
    }

    /**
     * @param DateTime|null $endedAt
     * @return DataEntryInput
     */
    public function setEndedAt(?DateTime $endedAt): DataEntryInput
    {
        $this->endedAt = $endedAt;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCurrentPosition(): ?string
    {
        return $this->currentPosition;
    }

    /**
     * @param string|null $currentPosition
     * @return DataEntryInput
     */
    public function setCurrentPosition(?string $currentPosition): DataEntryInput
    {
        $this->currentPosition = $currentPosition;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getWorkpath(): ?array
    {
        return $this->workpath;
    }

    /**
     * @param string[]|null $workpath
     * @return DataEntryInput
     */
    public function setWorkpath(?array $workpath): DataEntryInput
    {
        $this->workpath = $workpath;
        return $this;
    }

    /**
     * @return SessionInput[]
     */
    public function getSessions(): array
    {
        return $this->sessions;
    }

    /**
     * @param SessionInput[] $sessions
     * @return DataEntryInput
     */
    public function setSessions(array $sessions): DataEntryInput
    {
        $this->sessions = $sessions;
        return $this;
    }
}
