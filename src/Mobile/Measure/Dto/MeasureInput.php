<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto;

use DateTime;
use Mobile\Measure\Dto\Variable\StateCodeInput;
use Mobile\Shared\Dto\HasAnnotationInput;

/**
 * Class MeasureInput.
 */
class MeasureInput extends HasAnnotationInput
{
    /**
     * @var string|int|null
     */
    private $value;

    /**
     * Define the level in a single form element depending on generator variable levels.
     * @var int
     */
    private $index;

    /**
     * Define the repetition number of the variable.
     * @var int
     */
    private $repetition;

    /**
     * @var DateTime
     */
    private $timestamp;

    /**
     * @var StateCodeInput|null
     */
    private $state;

    /**
     * @return mixed|null
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed|null $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex(int $index): void
    {
        $this->index = $index;
    }

    /**
     * @return int
     */
    public function getRepetition(): int
    {
        return $this->repetition;
    }

    /**
     * @param int $repetition
     */
    public function setRepetition(int $repetition): void
    {
        $this->repetition = $repetition;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp(): DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param DateTime $timestamp
     */
    public function setTimestamp(DateTime $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return StateCodeInput|null
     */
    public function getState(): ?StateCodeInput
    {
        return $this->state;
    }

    /**
     * @param StateCodeInput|null $state
     * @return MeasureInput
     */
    public function setState(?StateCodeInput $state): MeasureInput
    {
        $this->state = $state;
        return $this;
    }
}
