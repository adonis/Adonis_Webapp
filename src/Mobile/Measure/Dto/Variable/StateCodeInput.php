<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

use Mobile\Measure\Entity\Variable\StateCode;

/**
 * Class StateCodeInput.
 */
class StateCodeInput
{
    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $label;

    /**
     * @var string
     */
    private $type = StateCode::NON_PERMANENT_STATE_CODE;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $propagation;

    /**
     * @var string|null
     */
    private $color;

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return StateCodeInput
     */
    public function setCode(int $code): StateCodeInput
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return StateCodeInput
     */
    public function setLabel(string $label): StateCodeInput
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return StateCodeInput
     */
    public function setType(string $type): StateCodeInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return StateCodeInput
     */
    public function setDescription(?string $description): StateCodeInput
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPropagation(): ?string
    {
        return $this->propagation;
    }

    /**
     * @param string|null $propagation
     * @return StateCodeInput
     */
    public function setPropagation(?string $propagation): StateCodeInput
    {
        $this->propagation = $propagation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     * @return StateCodeInput
     */
    public function setColor(?string $color): StateCodeInput
    {
        $this->color = $color;
        return $this;
    }
}
