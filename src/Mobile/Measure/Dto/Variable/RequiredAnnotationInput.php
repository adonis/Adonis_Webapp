<?php


namespace Mobile\Measure\Dto\Variable;


class RequiredAnnotationInput
{
    /**
     * @var string
     */
    private $pathLevel;

    /**
     * @var int
     */
    private $type;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var bool
     */
    private $askWhenEntering;

    /**
     * @var string
     */
    private $buisnessObjects;

    /**
     * @var string[]
     */
    private $category;

    /**
     * @var string[]
     */
    private $keywords;

    /**
     * @return string
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @param string $pathLevel
     * @return RequiredAnnotationInput
     */
    public function setPathLevel(string $pathLevel): RequiredAnnotationInput
    {
        $this->pathLevel = $pathLevel;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return RequiredAnnotationInput
     */
    public function setType(int $type): RequiredAnnotationInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return RequiredAnnotationInput
     */
    public function setComment(string $comment): RequiredAnnotationInput
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return RequiredAnnotationInput
     */
    public function setActive(bool $active): RequiredAnnotationInput
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAskWhenEntering(): bool
    {
        return $this->askWhenEntering;
    }

    /**
     * @param bool $askWhenEntering
     * @return RequiredAnnotationInput
     */
    public function setAskWhenEntering(bool $askWhenEntering): RequiredAnnotationInput
    {
        $this->askWhenEntering = $askWhenEntering;
        return $this;
    }

    /**
     * @return string
     */
    public function getBuisnessObjects(): string
    {
        return $this->buisnessObjects;
    }

    /**
     * @param string $buisnessObjects
     * @return RequiredAnnotationInput
     */
    public function setBuisnessObjects(string $buisnessObjects): RequiredAnnotationInput
    {
        $this->buisnessObjects = $buisnessObjects;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    /**
     * @param string[] $category
     * @return RequiredAnnotationInput
     */
    public function setCategory(array $category): RequiredAnnotationInput
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param string[] $keywords
     * @return RequiredAnnotationInput
     */
    public function setKeywords(array $keywords): RequiredAnnotationInput
    {
        $this->keywords = $keywords;
        return $this;
    }
}
