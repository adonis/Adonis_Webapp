<?php


namespace Mobile\Measure\Dto\Variable;


class MaterialInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $physicalDevice;

    /**
     * @var string|null
     */
    private $manufacturer;

    /**
     * @var string|null
     */
    private $type;

    /**
     * @var DriverInput
     */
    private $driver;

    /**
     * @var string
     */
    private $uid;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MaterialInput
     */
    public function setName(string $name): MaterialInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhysicalDevice(): string
    {
        return $this->physicalDevice;
    }

    /**
     * @param string $physicalDevice
     * @return MaterialInput
     */
    public function setPhysicalDevice(string $physicalDevice): MaterialInput
    {
        $this->physicalDevice = $physicalDevice;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

    /**
     * @param string|null $manufacturer
     * @return MaterialInput
     */
    public function setManufacturer(?string $manufacturer): MaterialInput
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return MaterialInput
     */
    public function setType(?string $type): MaterialInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return DriverInput
     */
    public function getDriver(): DriverInput
    {
        return $this->driver;
    }

    /**
     * @param DriverInput $driver
     * @return MaterialInput
     */
    public function setDriver(DriverInput $driver): MaterialInput
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return MaterialInput
     */
    public function setUid(string $uid): MaterialInput
    {
        $this->uid = $uid;
        return $this;
    }

}
