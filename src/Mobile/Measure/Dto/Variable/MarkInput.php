<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

/**
 * Class MarkInput.
 */
class MarkInput
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var int
     */
    private $value;

    /**
     * @var string|null
     */
    private $imageName;

    /**
     * @var string|null
     */
    private $imageData;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return MarkInput
     */
    public function setText(string $text): MarkInput
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return MarkInput
     */
    public function setValue(int $value): MarkInput
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * @param string|null $imageName
     * @return MarkInput
     */
    public function setImageName(?string $imageName): MarkInput
    {
        $this->imageName = $imageName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageData(): ?string
    {
        return $this->imageData;
    }

    /**
     * @param string|null $imageData
     * @return MarkInput
     */
    public function setImageData(?string $imageData): MarkInput
    {
        $this->imageData = $imageData;
        return $this;
    }
}
