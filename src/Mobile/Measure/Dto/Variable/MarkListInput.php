<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

/**
 * Class MarkListInput.
 */
class MarkListInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $minValue;

    /**
     * @var int
     */
    private $maxValue;

    /**
     * @var MarkInput[]
     */
    private $marks;

    /**
     * @var bool
     */
    private $exclusive;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MarkListInput
     */
    public function setName(string $name): MarkListInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinValue(): int
    {
        return $this->minValue;
    }

    /**
     * @param int $minValue
     * @return MarkListInput
     */
    public function setMinValue(int $minValue): MarkListInput
    {
        $this->minValue = $minValue;
        return $this;
    }

    /**
     * @return int
     */
    public function getMaxValue(): int
    {
        return $this->maxValue;
    }

    /**
     * @param int $maxValue
     * @return MarkListInput
     */
    public function setMaxValue(int $maxValue): MarkListInput
    {
        $this->maxValue = $maxValue;
        return $this;
    }

    /**
     * @return MarkInput[]
     */
    public function getMarks(): array
    {
        return $this->marks;
    }

    /**
     * @param MarkInput[] $marks
     * @return MarkListInput
     */
    public function setMarks(array $marks): MarkListInput
    {
        $this->marks = $marks;
        return $this;
    }

    /**
     * @return bool
     */
    public function isExclusive(): bool
    {
        return $this->exclusive;
    }

    /**
     * @param bool $exclusive
     * @return MarkListInput
     */
    public function setExclusive(bool $exclusive): MarkListInput
    {
        $this->exclusive = $exclusive;
        return $this;
    }
}
