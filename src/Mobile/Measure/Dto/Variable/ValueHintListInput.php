<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

/**
 * Class ValueHintListInput.
 */
class ValueHintListInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var ValueHintInput[]
     */
    private $valueHints;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ValueHintListInput
     */
    public function setName(string $name): ValueHintListInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ValueHintInput[]
     */
    public function getValueHints(): array
    {
        return $this->valueHints;
    }

    /**
     * @param ValueHintInput[] $valueHints
     * @return ValueHintListInput
     */
    public function setValueHints(array $valueHints): ValueHintListInput
    {
        $this->valueHints = $valueHints;
        return $this;
    }
}
