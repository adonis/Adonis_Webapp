<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

/**
 * Class ValueHintInput.
 */
class ValueHintInput
{
    /**
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ValueHintInput
     */
    public function setValue(string $value): ValueHintInput
    {
        $this->value = $value;
        return $this;
    }
}
