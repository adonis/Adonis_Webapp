<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

use DateTime;
use Mobile\Measure\Dto\Test\CombinationTestInput;
use Mobile\Measure\Dto\Test\GrowthTestInput;
use Mobile\Measure\Dto\Test\PreconditionedCalculationInput;
use Mobile\Measure\Dto\Test\RangeTestInput;

/**
 * Class VariableInput.
 */
abstract class VariableInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $shortName;

    /**
     * @var int
     */
    private $repetitions;

    /**
     * @var string|null
     */
    private $unit;

    /**
     * @var bool
     */
    private $askTimestamp;

    /**
     * @var string
     */
    private $pathLevel;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var int
     */
    private $order;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var string|null
     */
    private $format;

    /**
     * @var string|bool|null
     */
    private $defaultValue;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $mandatory;

    /**
     * @var GrowthTestInput[]
     */
    private $growthTests;

    /**
     * @var RangeTestInput[]
     */
    private $rangeTests;

    /**
     * @var CombinationTestInput[]
     */
    private $combinationTests;

    /**
     * @var PreconditionedCalculationInput[]
     */
    private $preconditionedCalculations;

    /**
     * @var MarkListInput|null
     */
    private $scale;

    /**
     * @var PreviousValueInput[]
     */
    private $previousValues;

    /**
     * @var DateTime
     */
    private $creationDate;

    /**
     * @var DateTime
     */
    private $modificationDate;

    /**
     * @var string
     */
    private $uri;

    /**
     * @var int|null
     */
    private $frameStartPosition;

    /**
     * @var int|null
     */
    private $frameEndPosition;

    /**
     * @var string|null
     */
    private $materialUid;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return VariableInput
     */
    public function setName(string $name): VariableInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return VariableInput
     */
    public function setShortName(string $shortName): VariableInput
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return int
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @param int $repetitions
     * @return VariableInput
     */
    public function setRepetitions(int $repetitions): VariableInput
    {
        $this->repetitions = $repetitions;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string|null $unit
     * @return VariableInput
     */
    public function setUnit(?string $unit): VariableInput
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAskTimestamp(): bool
    {
        return $this->askTimestamp;
    }

    /**
     * @param bool $askTimestamp
     * @return VariableInput
     */
    public function setAskTimestamp(bool $askTimestamp): VariableInput
    {
        $this->askTimestamp = $askTimestamp;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @param string $pathLevel
     * @return VariableInput
     */
    public function setPathLevel(string $pathLevel): VariableInput
    {
        $this->pathLevel = $pathLevel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return VariableInput
     */
    public function setComment(?string $comment): VariableInput
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return VariableInput
     */
    public function setOrder(int $order): VariableInput
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return VariableInput
     */
    public function setActive(bool $active): VariableInput
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     * @return VariableInput
     */
    public function setFormat(?string $format): VariableInput
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return string|bool|null
     */
    public function getDefaultValue()
    {
        $default = null;
        if (!is_null($this->defaultValue) && is_bool($this->defaultValue)) {
            $default = $this->defaultValue ? 'true' : 'false';
        } else {
            $default = $this->defaultValue;
        }
        return $default;
    }

    /**
     * @param string|bool|null $defaultValue
     * @return VariableInput
     */
    public function setDefaultValue($defaultValue): VariableInput
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return VariableInput
     */
    public function setType(string $type): VariableInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMandatory(): bool
    {
        return $this->mandatory;
    }

    /**
     * @param bool $mandatory
     * @return VariableInput
     */
    public function setMandatory(bool $mandatory): VariableInput
    {
        $this->mandatory = $mandatory;
        return $this;
    }

    /**
     * @return GrowthTestInput[]
     */
    public function getGrowthTests(): array
    {
        return $this->growthTests;
    }

    /**
     * @param GrowthTestInput[] $growthTests
     * @return VariableInput
     */
    public function setGrowthTests(array $growthTests): VariableInput
    {
        $this->growthTests = $growthTests;
        return $this;
    }

    /**
     * @return RangeTestInput[]
     */
    public function getRangeTests(): array
    {
        return $this->rangeTests;
    }

    /**
     * @param RangeTestInput[] $rangeTests
     * @return VariableInput
     */
    public function setRangeTests(array $rangeTests): VariableInput
    {
        $this->rangeTests = $rangeTests;
        return $this;
    }

    /**
     * @return CombinationTestInput[]
     */
    public function getCombinationTests(): array
    {
        return $this->combinationTests;
    }

    /**
     * @param CombinationTestInput[] $combinationTests
     * @return VariableInput
     */
    public function setCombinationTests(array $combinationTests): VariableInput
    {
        $this->combinationTests = $combinationTests;
        return $this;
    }

    /**
     * @return PreconditionedCalculationInput[]
     */
    public function getPreconditionedCalculations(): array
    {
        return $this->preconditionedCalculations;
    }

    /**
     * @param PreconditionedCalculationInput[] $preconditionedCalculations
     * @return VariableInput
     */
    public function setPreconditionedCalculations(array $preconditionedCalculations): VariableInput
    {
        $this->preconditionedCalculations = $preconditionedCalculations;
        return $this;
    }

    /**
     * @return MarkListInput|null
     */
    public function getScale(): ?MarkListInput
    {
        return $this->scale;
    }

    /**
     * @param MarkListInput|null $scale
     * @return VariableInput
     */
    public function setScale(?MarkListInput $scale): VariableInput
    {
        $this->scale = $scale;
        return $this;
    }

    /**
     * @return PreviousValueInput[]
     */
    public function getPreviousValues(): array
    {
        return $this->previousValues;
    }

    /**
     * @param PreviousValueInput[] $previousValues
     * @return VariableInput
     */
    public function setPreviousValues(array $previousValues): VariableInput
    {
        $this->previousValues = $previousValues;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     * @return VariableInput
     */
    public function setCreationDate(DateTime $creationDate): VariableInput
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getModificationDate(): DateTime
    {
        return $this->modificationDate;
    }

    /**
     * @param DateTime $modificationDate
     * @return VariableInput
     */
    public function setModificationDate(DateTime $modificationDate): VariableInput
    {
        $this->modificationDate = $modificationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return VariableInput
     */
    public function setUri(string $uri): VariableInput
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFrameStartPosition(): ?int
    {
        return $this->frameStartPosition;
    }

    /**
     * @param int|null $frameStartPosition
     * @return VariableInput
     */
    public function setFrameStartPosition(?int $frameStartPosition): VariableInput
    {
        $this->frameStartPosition = $frameStartPosition;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFrameEndPosition(): ?int
    {
        return $this->frameEndPosition;
    }

    /**
     * @param int|null $frameEndPosition
     * @return VariableInput
     */
    public function setFrameEndPosition(?int $frameEndPosition): VariableInput
    {
        $this->frameEndPosition = $frameEndPosition;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMaterialUid(): ?string
    {
        return $this->materialUid;
    }

    /**
     * @param string|null $materialUid
     * @return VariableInput
     */
    public function setMaterialUid(?string $materialUid): VariableInput
    {
        $this->materialUid = $materialUid;
        return $this;
    }

}
