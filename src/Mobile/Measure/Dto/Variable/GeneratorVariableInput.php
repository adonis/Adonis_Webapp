<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

/**
 * Class GeneratorVariableInput.
 */
class GeneratorVariableInput extends VariableInput
{
    /**
     * @var string
     */
    private $generatedPrefix;

    /**
     * @var UniqueVariableInput[]
     */
    private $uniqueVariables;

    /**
     * @var GeneratorVariableInput[]
     */
    private $generatorVariables;

    /**
     * @var bool
     */
    private $numeralIncrement;

    /**
     * @return string
     */
    public function getGeneratedPrefix(): string
    {
        return $this->generatedPrefix;
    }

    /**
     * @param string $generatedPrefix
     * @return GeneratorVariableInput
     */
    public function setGeneratedPrefix(string $generatedPrefix): GeneratorVariableInput
    {
        $this->generatedPrefix = $generatedPrefix;
        return $this;
    }

    /**
     * @return UniqueVariableInput[]
     */
    public function getUniqueVariables(): array
    {
        return $this->uniqueVariables;
    }

    /**
     * @param UniqueVariableInput[] $uniqueVariables
     * @return GeneratorVariableInput
     */
    public function setUniqueVariables(array $uniqueVariables): GeneratorVariableInput
    {
        $this->uniqueVariables = $uniqueVariables;
        return $this;
    }

    /**
     * @return GeneratorVariableInput[]
     */
    public function getGeneratorVariables(): array
    {
        return $this->generatorVariables;
    }

    /**
     * @param GeneratorVariableInput[] $generatorVariables
     * @return GeneratorVariableInput
     */
    public function setGeneratorVariables(array $generatorVariables): GeneratorVariableInput
    {
        $this->generatorVariables = $generatorVariables;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNumeralIncrement(): bool
    {
        return $this->numeralIncrement;
    }

    /**
     * @param bool $numeralIncrement
     * @return GeneratorVariableInput
     */
    public function setNumeralIncrement(bool $numeralIncrement): GeneratorVariableInput
    {
        $this->numeralIncrement = $numeralIncrement;
        return $this;
    }
}
