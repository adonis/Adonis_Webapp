<?php

namespace Mobile\Measure\Dto\Variable;

/**
 * Class DriverInput.
 */
class DriverInput
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var int
     */
    private $timeout;

    /**
     * @var int
     */
    private $frameLength;

    /**
     * @var string|null
     */
    private $frameStart;

    /**
     * @var string|null
     */
    private $frameEnd;

    /**
     * @var string|null
     */
    private $csvSeparator;

    /**
     * @var integer|null
     */
    private $baudrate;

    /**
     * @var float|null
     */
    private $stopbit;

    /**
     * @var string|null
     */
    private $parity;

    /**
     * @var string|null
     */
    private $flowControl;

    /**
     * @var bool|null
     */
    private $push;

    /**
     * @var string|null
     */
    private $request;

    /**
     * @var int|null
     */
    private $databitsFormat;

    /**
     * @var string|null
     */
    private $port;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return DriverInput
     */
    public function setType(string $type): DriverInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     * @return DriverInput
     */
    public function setTimeout(int $timeout): DriverInput
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return int
     */
    public function getFrameLength(): int
    {
        return $this->frameLength;
    }

    /**
     * @param int $frameLength
     * @return DriverInput
     */
    public function setFrameLength(int $frameLength): DriverInput
    {
        $this->frameLength = $frameLength;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFrameStart(): ?string
    {
        return $this->frameStart;
    }

    /**
     * @param string|null $frameStart
     * @return DriverInput
     */
    public function setFrameStart(?string $frameStart): DriverInput
    {
        $this->frameStart = $frameStart;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFrameEnd(): ?string
    {
        return $this->frameEnd;
    }

    /**
     * @param string|null $frameEnd
     * @return DriverInput
     */
    public function setFrameEnd(?string $frameEnd): DriverInput
    {
        $this->frameEnd = $frameEnd;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCsvSeparator(): ?string
    {
        return $this->csvSeparator;
    }

    /**
     * @param string|null $csvSeparator
     * @return DriverInput
     */
    public function setCsvSeparator(?string $csvSeparator): DriverInput
    {
        $this->csvSeparator = $csvSeparator;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBaudrate(): ?int
    {
        return $this->baudrate;
    }

    /**
     * @param int|null $baudrate
     * @return DriverInput
     */
    public function setBaudrate(?int $baudrate): DriverInput
    {
        $this->baudrate = $baudrate;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getStopbit(): ?float
    {
        return $this->stopbit;
    }

    /**
     * @param float|null $stopbit
     * @return DriverInput
     */
    public function setStopbit(?float $stopbit): DriverInput
    {
        $this->stopbit = $stopbit;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getParity(): ?string
    {
        return $this->parity;
    }

    /**
     * @param string|null $parity
     * @return DriverInput
     */
    public function setParity(?string $parity): DriverInput
    {
        $this->parity = $parity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFlowControl(): ?string
    {
        return $this->flowControl;
    }

    /**
     * @param string|null $flowControl
     * @return DriverInput
     */
    public function setFlowControl(?string $flowControl): DriverInput
    {
        $this->flowControl = $flowControl;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPush(): ?bool
    {
        return $this->push;
    }

    /**
     * @param bool|null $push
     * @return DriverInput
     */
    public function setPush(?bool $push): DriverInput
    {
        $this->push = $push;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRequest(): ?string
    {
        return $this->request;
    }

    /**
     * @param string|null $request
     * @return DriverInput
     */
    public function setRequest(?string $request): DriverInput
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDatabitsFormat(): ?int
    {
        return $this->databitsFormat;
    }

    /**
     * @param int|null $databitsFormat
     * @return DriverInput
     */
    public function setDatabitsFormat(?int $databitsFormat): DriverInput
    {
        $this->databitsFormat = $databitsFormat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPort(): ?string
    {
        return $this->port;
    }

    /**
     * @param string|null $port
     * @return DriverInput
     */
    public function setPort(?string $port): DriverInput
    {
        $this->port = $port;
        return $this;
    }

}
