<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

/**
 * Class UniqueVariableInput.
 */
class UniqueVariableInput extends VariableInput
{
    /**
     * @var ValueHintListInput|null
     */
    private $valueHintList;

    /**
     * @return ValueHintListInput|null
     */
    public function getValueHintList(): ?ValueHintListInput
    {
        return $this->valueHintList;
    }

    /**
     * @param ValueHintListInput|null $valueHintList
     * @return UniqueVariableInput
     */
    public function setValueHintList(?ValueHintListInput $valueHintList): UniqueVariableInput
    {
        $this->valueHintList = $valueHintList;
        return $this;
    }
}
