<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Variable;

use DateTime;

/**
 * Class PreviousValueInput.
 */
class PreviousValueInput
{
    /**
     * @var string|null
     */
    private $value;

    /**
     * @var string
     */
    private $objectUri;

    /**
     * @var DateTime|null
     */
    private $date;

    /**
     * @var int|null
     */
    private $stateCode;

    /**
     * @var string
     */
    private $originVariableUid;

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return PreviousValueInput
     */
    public function setValue(?string $value): PreviousValueInput
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getObjectUri(): string
    {
        return $this->objectUri;
    }

    /**
     * @param string $objectUri
     * @return PreviousValueInput
     */
    public function setObjectUri(string $objectUri): PreviousValueInput
    {
        $this->objectUri = $objectUri;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime|null $date
     * @return PreviousValueInput
     */
    public function setDate(?DateTime $date): PreviousValueInput
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStateCode(): ?int
    {
        return $this->stateCode;
    }

    /**
     * @param int|null $stateCode
     * @return PreviousValueInput
     */
    public function setStateCode(?int $stateCode): PreviousValueInput
    {
        $this->stateCode = $stateCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginVariableUid(): string
    {
        return $this->originVariableUid;
    }

    /**
     * @param string $originVariableUid
     * @return PreviousValueInput
     */
    public function setOriginVariableUid(string $originVariableUid): PreviousValueInput
    {
        $this->originVariableUid = $originVariableUid;
        return $this;
    }
}
