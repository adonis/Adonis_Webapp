<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Test;

/**
 * Class RangeTestInput.
 */
class RangeTestInput extends TestInput
{
    /**
     * @var float
     */
    private $mandatoryMinValue;

    /**
     * @var float
     */
    private $mandatoryMaxValue;

    /**
     * @var float
     */
    private $optionalMinValue;

    /**
     * @var float
     */
    private $optionalMaxValue;

    /**
     * @return float
     */
    public function getMandatoryMinValue(): float
    {
        return $this->mandatoryMinValue;
    }

    /**
     * @param float $mandatoryMinValue
     * @return RangeTestInput
     */
    public function setMandatoryMinValue(float $mandatoryMinValue): RangeTestInput
    {
        $this->mandatoryMinValue = $mandatoryMinValue;
        return $this;
    }

    /**
     * @return float
     */
    public function getMandatoryMaxValue(): float
    {
        return $this->mandatoryMaxValue;
    }

    /**
     * @param float $mandatoryMaxValue
     * @return RangeTestInput
     */
    public function setMandatoryMaxValue(float $mandatoryMaxValue): RangeTestInput
    {
        $this->mandatoryMaxValue = $mandatoryMaxValue;
        return $this;
    }

    /**
     * @return float
     */
    public function getOptionalMinValue(): float
    {
        return $this->optionalMinValue;
    }

    /**
     * @param float $optionalMinValue
     * @return RangeTestInput
     */
    public function setOptionalMinValue(float $optionalMinValue): RangeTestInput
    {
        $this->optionalMinValue = $optionalMinValue;
        return $this;
    }

    /**
     * @return float
     */
    public function getOptionalMaxValue(): float
    {
        return $this->optionalMaxValue;
    }

    /**
     * @param float $optionalMaxValue
     * @return RangeTestInput
     */
    public function setOptionalMaxValue(float $optionalMaxValue): RangeTestInput
    {
        $this->optionalMaxValue = $optionalMaxValue;
        return $this;
    }
}
