<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Test;

/**
 * Class PreconditionedCalculationInput.
 */
class PreconditionedCalculationInput extends TestInput
{
    /**
     * @var string
     */
    private $comparisonVariableUid;

    /**
     * @var string
     */
    private $condition;

    /**
     * @var float|null
     */
    private $comparedValue;

    /**
     * @var string|null
     */
    private $comparedVariableUid;

    /**
     * @var string
     */
    private $affectationValue;

    /**
     * @return string
     */
    public function getComparisonVariableUid(): string
    {
        return $this->comparisonVariableUid;
    }

    /**
     * @param string $comparisonVariableUid
     * @return PreconditionedCalculationInput
     */
    public function setComparisonVariableUid(string $comparisonVariableUid): PreconditionedCalculationInput
    {
        $this->comparisonVariableUid = $comparisonVariableUid;
        return $this;
    }

    /**
     * @return string
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * @param string $condition
     * @return PreconditionedCalculationInput
     */
    public function setCondition(string $condition): PreconditionedCalculationInput
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getComparedValue(): ?float
    {
        return $this->comparedValue;
    }

    /**
     * @param float|null $comparedValue
     * @return PreconditionedCalculationInput
     */
    public function setComparedValue(?float $comparedValue): PreconditionedCalculationInput
    {
        $this->comparedValue = $comparedValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComparedVariableUid(): ?string
    {
        return $this->comparedVariableUid;
    }

    /**
     * @param string|null $comparedVariableUid
     * @return PreconditionedCalculationInput
     */
    public function setComparedVariableUid(?string $comparedVariableUid): PreconditionedCalculationInput
    {
        $this->comparedVariableUid = $comparedVariableUid;
        return $this;
    }

    /**
     * @return string
     */
    public function getAffectationValue(): string
    {
        return $this->affectationValue;
    }

    /**
     * @param string $affectationValue
     * @return PreconditionedCalculationInput
     */
    public function setAffectationValue(string $affectationValue): PreconditionedCalculationInput
    {
        $this->affectationValue = $affectationValue;
        return $this;
    }
}
