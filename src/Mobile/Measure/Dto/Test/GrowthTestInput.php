<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Test;

/**
 * Class GrowthTestInput.
 */
class GrowthTestInput extends TestInput
{
    /**
     * @var string
     */
    private $comparisonVariableUid;

    /**
     * @var float
     */
    private $minDiff;

    /**
     * @var float
     */
    private $maxDiff;

    /**
     * @return string
     */
    public function getComparisonVariableUid(): string
    {
        return $this->comparisonVariableUid;
    }

    /**
     * @param string $comparisonVariableUid
     * @return GrowthTestInput
     */
    public function setComparisonVariableUid(string $comparisonVariableUid): GrowthTestInput
    {
        $this->comparisonVariableUid = $comparisonVariableUid;
        return $this;
    }

    /**
     * @return float
     */
    public function getMinDiff(): float
    {
        return $this->minDiff;
    }

    /**
     * @param float $minDiff
     * @return GrowthTestInput
     */
    public function setMinDiff(float $minDiff): GrowthTestInput
    {
        $this->minDiff = $minDiff;
        return $this;
    }

    /**
     * @return float
     */
    public function getMaxDiff(): float
    {
        return $this->maxDiff;
    }

    /**
     * @param float $maxDiff
     * @return GrowthTestInput
     */
    public function setMaxDiff(float $maxDiff): GrowthTestInput
    {
        $this->maxDiff = $maxDiff;
        return $this;
    }
}
