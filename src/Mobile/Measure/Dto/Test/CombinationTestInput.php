<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto\Test;

/**
 * Class CombinationTestInput.
 */
class CombinationTestInput extends TestInput
{
    /**
     * @var string
     */
    private $firstOperandUid;

    /**
     * @var string
     */
    private $secondOperandUid;

    /**
     * @var string
     */
    private $operator;

    /**
     * @var float
     */
    private $highLimit;

    /**
     * @var float
     */
    private $lowLimit;

    /**
     * @return string
     */
    public function getFirstOperandUid(): string
    {
        return $this->firstOperandUid;
    }

    /**
     * @param string $firstOperandUid
     * @return CombinationTestInput
     */
    public function setFirstOperandUid(string $firstOperandUid): CombinationTestInput
    {
        $this->firstOperandUid = $firstOperandUid;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecondOperandUid(): string
    {
        return $this->secondOperandUid;
    }

    /**
     * @param string $secondOperandUid
     * @return CombinationTestInput
     */
    public function setSecondOperandUid(string $secondOperandUid): CombinationTestInput
    {
        $this->secondOperandUid = $secondOperandUid;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     * @return CombinationTestInput
     */
    public function setOperator(string $operator): CombinationTestInput
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return float
     */
    public function getHighLimit(): float
    {
        return $this->highLimit;
    }

    /**
     * @param float $highLimit
     * @return CombinationTestInput
     */
    public function setHighLimit(float $highLimit): CombinationTestInput
    {
        $this->highLimit = $highLimit;
        return $this;
    }

    /**
     * @return float
     */
    public function getLowLimit(): float
    {
        return $this->lowLimit;
    }

    /**
     * @param float $lowLimit
     * @return CombinationTestInput
     */
    public function setLowLimit(float $lowLimit): CombinationTestInput
    {
        $this->lowLimit = $lowLimit;
        return $this;
    }
}
