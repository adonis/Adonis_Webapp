<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Dto;

use DateTime;

class AnnotationInput
{
    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int
     */
    private $type;

    /**
     * @var string|null
     */
    private $value;

    /**
     * @var string|null
     */
    private $image;

    /**
     * @var DateTime
     */
    private $timestamp;

    /**
     * @var string[]
     */
    private $categories;

    /**
     * @var string[]
     */
    private $keywords;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return AnnotationInput
     */
    public function setName(?string $name): AnnotationInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return AnnotationInput
     */
    public function setType(int $type): AnnotationInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     * @return AnnotationInput
     */
    public function setValue(?string $value): AnnotationInput
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     * @return AnnotationInput
     */
    public function setImage(?string $image): AnnotationInput
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTimestamp(): DateTime
    {
        return $this->timestamp;
    }

    /**
     * @param DateTime $timestamp
     * @return AnnotationInput
     */
    public function setTimestamp(DateTime $timestamp): AnnotationInput
    {
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param string[] $categories
     * @return AnnotationInput
     */
    public function setCategories(array $categories): AnnotationInput
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param string[] $keywords
     * @return AnnotationInput
     */
    public function setKeywords(array $keywords): AnnotationInput
    {
        $this->keywords = $keywords;
        return $this;
    }

}
