<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Measure\Entity\Variable;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\ValueHintRepository")
 *
 * @ORM\Table(name="value_hint", schema="adonis")
 */
class ValueHint extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $value = '';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\ValueHintList", inversedBy="valueHints")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private ValueHintList $list;

    /**
     * @param non-empty-string $value
     */
    public static function build(string $value): self
    {
        $entity = new self();
        $entity->setValue($value);

        return $entity;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getList(): ValueHintList
    {
        return $this->list;
    }

    /**
     * @return $this
     */
    public function setList(ValueHintList $list): self
    {
        $this->list = $list;

        return $this;
    }
}
