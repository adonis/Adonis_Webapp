<?php

namespace Mobile\Measure\Entity\Variable\Base;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Test\Base\Test;
use Mobile\Measure\Entity\Test\CombinationTest;
use Mobile\Measure\Entity\Test\GrowthTest;
use Mobile\Measure\Entity\Test\PreconditionedCalculation;
use Mobile\Measure\Entity\Test\RangeTest;
use Mobile\Measure\Entity\Variable\GeneratorVariable;
use Mobile\Measure\Entity\Variable\Material;
use Mobile\Measure\Entity\Variable\PreviousValue;
use Mobile\Measure\Entity\Variable\Scale;
use Mobile\Project\Entity\DataEntryProject;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableFormatEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="variable", schema="adonis")
 *
 * @ORM\InheritanceType("JOINED")
 *
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 *
 * @ORM\DiscriminatorMap({
 *     "GeneratorVariable" = "Mobile\Measure\Entity\Variable\GeneratorVariable",
 *     "UniqueVariable" = "Mobile\Measure\Entity\Variable\UniqueVariable"
 * })
 *
 * @psalm-import-type PathLevelEnumId from PathLevelEnum
 * @psalm-import-type VariableFormatEnumId from VariableFormatEnum
 * @psalm-import-type VariableTypeEnumId from VariableTypeEnum
 */
abstract class Variable extends IdentifiedEntity
{
    protected ?DataEntryProject $project = null;

    protected ?DataEntryProject $connectedDataEntryProject = null;

    protected ?GeneratorVariable $generatorVariable = null;

    protected ?Material $material = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $shortName = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $repetitions = 1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $unit = null;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $askTimestamp = false;

    /**
     * @psalm-var PathLevelEnumId|''
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private string $pathLevel = '';

    /**
     * @ORM\Column(type="text", nullable=true, options={"default": null})
     */
    private ?string $comment = null;

    /**
     * @ORM\Column(type="integer", name="print_order", nullable=false)
     */
    private int $order = 0;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $active = false;

    /**
     * @psalm-var VariableFormatEnumId|numeric-string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $format = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $defaultValue = null;

    /**
     * @psalm-var VariableTypeEnumId|''
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private string $type = '';

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $mandatory = false;

    /**
     * @var Collection<int, GrowthTest>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Test\GrowthTest", mappedBy="variable", cascade={"persist", "remove", "detach"})
     */
    private Collection $growthTests;

    /**
     * @var Collection<int, RangeTest>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Test\RangeTest", mappedBy="variable", cascade={"persist", "remove", "detach"})
     */
    private Collection $rangeTests;

    /**
     * @var Collection<int, CombinationTest>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Test\CombinationTest", mappedBy="variable", cascade={"persist", "remove", "detach"})
     */
    private Collection $combinationTests;

    /**
     * @var Collection<int, PreconditionedCalculation>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Test\PreconditionedCalculation", mappedBy="variable", cascade={"persist", "remove", "detach"})
     */
    private Collection $preconditionedCalculations;

    /**
     * @var Collection<int, PreviousValue>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\PreviousValue", mappedBy="variable", cascade={"persist", "remove", "detach"})
     */
    private Collection $previousValues;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\Variable\Scale", mappedBy="variable", cascade={"persist", "remove", "detach"}, orphanRemoval=true)
     */
    private ?Scale $scale = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $creationDate;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $modificationDate;

    /**
     * @ORM\Column(type="integer", nullable=true, name="start_position")
     */
    private ?int $frameStartPosition = null;

    /**
     * @ORM\Column(type="integer", nullable=true, name="end_position")
     */
    private ?int $frameEndPosition = null;

    public function __construct()
    {
        $this->growthTests = new ArrayCollection();
        $this->rangeTests = new ArrayCollection();
        $this->combinationTests = new ArrayCollection();
        $this->preconditionedCalculations = new ArrayCollection();
        $this->previousValues = new ArrayCollection();
        $this->creationDate = new \DateTime();
        $this->modificationDate = new \DateTime();
    }

    public function merge(Variable $variable): void
    {
        $this->shortName = $variable->getShortName();
        $this->repetitions = $variable->getRepetitions();
        $this->unit = $variable->getUnit();
        $this->askTimestamp = $variable->isAskTimestamp();
        $this->pathLevel = $variable->getPathLevel();
        $this->comment = $variable->getComment();
        $this->order = $variable->getOrder();
        $this->active = $variable->isActive();
        $this->format = $variable->getFormat();
        $this->type = $variable->getType();
        $this->mandatory = $variable->isMandatory();
        $this->setScale($variable->getScale());
        $variable->setScale(null);
    }

    /**
     * @psalm-mutation-free
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return $this
     */
    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @return $this
     */
    public function setRepetitions(int $repetitions): self
    {
        $this->repetitions = $repetitions;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @return $this
     */
    public function setUnit(?string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isAskTimestamp(): bool
    {
        return $this->askTimestamp;
    }

    /**
     * @return $this
     */
    public function setAskTimestamp(bool $askTimestamp): self
    {
        $this->askTimestamp = $askTimestamp;

        return $this;
    }

    /**
     * @psalm-return PathLevelEnumId|''
     *
     * @psalm-mutation-free
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @psalm-param PathLevelEnumId|'' $pathLevel
     *
     * @return $this
     */
    public function setPathLevel(string $pathLevel): self
    {
        $this->pathLevel = $pathLevel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @return $this
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return $this
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @psalm-return VariableFormatEnumId|numeric-string|null
     *
     * @psalm-mutation-free
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @psalm-param VariableFormatEnumId|numeric-string|null $format
     *
     * @return $this
     */
    public function setFormat(?string $format): self
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @psalm-return VariableTypeEnumId|''
     *
     * @psalm-mutation-free
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @psalm-param VariableTypeEnumId|'' $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isMandatory(): bool
    {
        return $this->mandatory;
    }

    /**
     * @return $this
     */
    public function setMandatory(bool $mandatory): self
    {
        $this->mandatory = $mandatory;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getScale(): ?Scale
    {
        return $this->scale;
    }

    /**
     * @return $this
     */
    public function setScale(?Scale $scale): self
    {
        if (null !== $this->scale) {
            $this->scale->setVariable(null);
        }
        $this->scale = $scale;
        if (null !== $scale) {
            $scale->setVariable($this);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @return $this
     */
    public function setDefaultValue(?string $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): ?DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(?DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getGeneratorVariable(): ?GeneratorVariable
    {
        return $this->generatorVariable;
    }

    /**
     * @return $this
     */
    public function setGeneratorVariable(?GeneratorVariable $generatorVariable): self
    {
        $this->generatorVariable = $generatorVariable;

        return $this;
    }

    /**
     * @return Collection<int,  GrowthTest>
     *
     * @psalm-mutation-free
     */
    public function getGrowthTests(): Collection
    {
        return $this->growthTests;
    }

    /**
     * @param iterable<int, GrowthTest> $growthTests
     *
     * @return $this
     */
    public function setGrowthTests(iterable $growthTests): self
    {
        ArrayCollectionUtils::update($this->growthTests, $growthTests, function (GrowthTest $growthTest) {
            $growthTest->setVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addGrowthTest(GrowthTest $test): self
    {
        if (!$this->growthTests->contains($test)) {
            $this->growthTests->add($test);
            $test->setVariable($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGrowthTest(GrowthTest $test): self
    {
        if ($this->growthTests->contains($test)) {
            $this->growthTests->removeElement($test);
        }

        return $this;
    }

    /**
     * @return Collection<int,  RangeTest>
     *
     * @psalm-mutation-free
     */
    public function getRangeTests(): Collection
    {
        return $this->rangeTests;
    }

    /**
     * @param iterable<int, RangeTest> $rangeTests
     *
     * @return $this
     */
    public function setRangeTests(iterable $rangeTests): self
    {
        ArrayCollectionUtils::update($this->rangeTests, $rangeTests, function (RangeTest $rangeTest) {
            $rangeTest->setVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addRangeTest(RangeTest $test): self
    {
        if (!$this->rangeTests->contains($test)) {
            $this->rangeTests->add($test);
            $test->setVariable($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeRangeTest(RangeTest $test): self
    {
        if ($this->rangeTests->contains($test)) {
            $this->rangeTests->removeElement($test);
        }

        return $this;
    }

    /**
     * @return Collection<int,  CombinationTest>
     *
     * @psalm-mutation-free
     */
    public function getCombinationTests(): Collection
    {
        return $this->combinationTests;
    }

    /**
     * @param iterable<int, CombinationTest> $combinationTests
     *
     * @return $this
     */
    public function setCombinationTests(iterable $combinationTests): self
    {
        ArrayCollectionUtils::update($this->combinationTests, $combinationTests, function (CombinationTest $combinationTest) {
            $combinationTest->setVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addCombinationTest(CombinationTest $test): self
    {
        if (!$this->combinationTests->contains($test)) {
            $this->combinationTests->add($test);
            $test->setVariable($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeCombinationTest(CombinationTest $test): self
    {
        if ($this->combinationTests->contains($test)) {
            $this->combinationTests->removeElement($test);
        }

        return $this;
    }

    /**
     * @return Collection<int, PreconditionedCalculation>
     *
     * @psalm-mutation-free
     */
    public function getPreconditionedCalculations(): Collection
    {
        return $this->preconditionedCalculations;
    }

    /**
     * @param iterable<int, PreconditionedCalculation> $preconditionedCalculations
     *
     * @return $this
     */
    public function setPreconditionedCalculations(iterable $preconditionedCalculations): self
    {
        ArrayCollectionUtils::update($this->preconditionedCalculations, $preconditionedCalculations, function (PreconditionedCalculation $preconditionedCalculation) {
            $preconditionedCalculation->setVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addPreconditionedCalculation(PreconditionedCalculation $test): self
    {
        if (!$this->preconditionedCalculations->contains($test)) {
            $this->preconditionedCalculations->add($test);
            $test->setVariable($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removePreconditionedCalculation(PreconditionedCalculation $test): self
    {
        if ($this->preconditionedCalculations->contains($test)) {
            $this->preconditionedCalculations->removeElement($test);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addTest(Test $test): self
    {
        if ($test instanceof GrowthTest) {
            $this->addGrowthTest($test);
        } elseif ($test instanceof RangeTest) {
            $this->addRangeTest($test);
        } elseif ($test instanceof CombinationTest) {
            $this->addCombinationTest($test);
        } elseif ($test instanceof PreconditionedCalculation) {
            $this->addPreconditionedCalculation($test);
        }

        return $this;
    }

    /**
     * @return Collection<int,  PreviousValue>
     *
     * @psalm-mutation-free
     */
    public function getPreviousValues(): Collection
    {
        return $this->previousValues;
    }

    /**
     * @param iterable<int,  PreviousValue> $previousValues
     *
     * @return $this
     */
    public function setPreviousValues(iterable $previousValues): self
    {
        ArrayCollectionUtils::update($this->previousValues, $previousValues, function (PreviousValue $previousValue) {
            $previousValue->setVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addPreviousValue(PreviousValue $previousValue): self
    {
        if (!$this->previousValues->contains($previousValue)) {
            $previousValue->setVariable($this);
            $this->previousValues->add($previousValue);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removePreviousValue(PreviousValue $previousValue): self
    {
        if ($this->previousValues->contains($previousValue)) {
            $this->previousValues->removeElement($previousValue);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getConnectedDataEntryProject(): ?DataEntryProject
    {
        return $this->connectedDataEntryProject;
    }

    /**
     * @return $this
     */
    public function setConnectedDataEntryProject(?DataEntryProject $connectedDataEntryProject): self
    {
        $this->connectedDataEntryProject = $connectedDataEntryProject;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }

    /**
     * @return $this
     */
    public function setModificationDate(\DateTime $modificationDate): self
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    /**
     * @return $this
     */
    public function setMaterial(?Material $material): self
    {
        $this->material = $material;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFrameStartPosition(): ?int
    {
        return $this->frameStartPosition;
    }

    /**
     * @return $this
     */
    public function setFrameStartPosition(?int $frameStartPosition): self
    {
        $this->frameStartPosition = $frameStartPosition;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFrameEndPosition(): ?int
    {
        return $this->frameEndPosition;
    }

    /**
     * @return $this
     */
    public function setFrameEndPosition(?int $frameEndPosition): self
    {
        $this->frameEndPosition = $frameEndPosition;

        return $this;
    }
}
