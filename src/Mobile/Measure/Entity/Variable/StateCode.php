<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Measure\Entity\Variable;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\DataEntryProject;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\StateCodeRepository")
 *
 * @ORM\Table(name="state_code", schema="adonis")
 *
 * @psalm-type StateCodeType = self::NON_PERMANENT_STATE_CODE|self::DEAD_STATE_CODE|self::MISSING_STATE_CODE
 *
 * @psalm-import-type PathLevelEnumId from PathLevelEnum
 */
class StateCode extends IdentifiedEntity
{
    public const NON_PERMANENT_STATE_CODE = 'non_permanent';
    public const DEAD_STATE_CODE = 'dead';
    public const MISSING_STATE_CODE = 'missing';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="stateCodes")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private DataEntryProject $project;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $code = 0;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private string $label = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $description = null;

    /**
     * @psalm-var PathLevelEnumId|''|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $propagation = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $color = null;

    /**
     * Field added in order to handle if a state code is permanent or not. If permanent, it define the type.
     *
     * @var StateCodeType|''
     *
     * @ORM\Column(type="string", nullable=false, options={"default"="non_permanent"})
     */
    private string $type = '';

    /**
     * @param non-empty-string $label
     */
    public static function build(int $code, string $label, string $description = '', string $color = ''): self
    {
        $entity = new self();
        $entity->setCode($code);
        $entity->setLabel($label);
        $entity->setDescription($description);
        $entity->setColor($color);

        return $entity;
    }

    /**
     * Utiliser {@see self::build}.
     *
     * @psalm-internal Mobile\Measure\Entity\Variable
     */
    public function __construct()
    {
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return $this
     */
    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @return $this
     */
    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @psalm-return PathLevelEnumId|''|null
     *
     * @psalm-mutation-free
     */
    public function getPropagation(): ?string
    {
        return $this->propagation;
    }

    /**
     * @psalm-param PathLevelEnumId|''|null $propagation
     */
    public function setPropagation(?string $propagation): self
    {
        $this->propagation = $propagation;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return StateCodeType|''
     *
     * @psalm-mutation-free
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param StateCodeType|'' $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
