<?php

namespace Mobile\Measure\Entity\Variable;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="driver", schema="adonis")
 */
class Driver extends IdentifiedEntity
{
    public const RS232 = 'rs232';
    public const BLUETOOTH = 'bluetooth';
    public const USB = 'usb';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $type = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $timeout = 0;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $frameLength = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $frameStart = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $frameEnd = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $csvSeparator = null;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\Variable\Material", mappedBy="driver")
     */
    private Material $material;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $baudrate = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $stopbit = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $parity = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $flowControl = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $push = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $request = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $databitsFormat = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $port = null;

    /**
     * @psalm-mutation-free
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @return $this
     */
    public function setTimeout(int $timeout): self
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFrameLength(): int
    {
        return $this->frameLength;
    }

    /**
     * @return $this
     */
    public function setFrameLength(int $frameLength): self
    {
        $this->frameLength = $frameLength;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFrameStart(): ?string
    {
        return $this->frameStart;
    }

    /**
     * @return $this
     */
    public function setFrameStart(?string $frameStart): self
    {
        $this->frameStart = $frameStart;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFrameEnd(): ?string
    {
        return $this->frameEnd;
    }

    /**
     * @return $this
     */
    public function setFrameEnd(?string $frameEnd): self
    {
        $this->frameEnd = $frameEnd;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCsvSeparator(): ?string
    {
        return $this->csvSeparator;
    }

    /**
     * @return $this
     */
    public function setCsvSeparator(?string $csvSeparator): self
    {
        $this->csvSeparator = $csvSeparator;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaterial(): Material
    {
        return $this->material;
    }

    /**
     * @return $this
     */
    public function setMaterial(Material $material): self
    {
        $this->material = $material;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBaudrate(): ?int
    {
        return $this->baudrate;
    }

    /**
     * @return $this
     */
    public function setBaudrate(?int $baudrate): self
    {
        $this->baudrate = $baudrate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStopbit(): ?float
    {
        return $this->stopbit;
    }

    /**
     * @return $this
     */
    public function setStopbit(?float $stopbit): self
    {
        $this->stopbit = $stopbit;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getParity(): ?string
    {
        return $this->parity;
    }

    /**
     * @return $this
     */
    public function setParity(?string $parity): self
    {
        $this->parity = $parity;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFlowControl(): ?string
    {
        return $this->flowControl;
    }

    /**
     * @return $this
     */
    public function setFlowControl(?string $flowControl): self
    {
        $this->flowControl = $flowControl;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPush(): ?bool
    {
        return $this->push;
    }

    /**
     * @return $this
     */
    public function setPush(?bool $push): self
    {
        $this->push = $push;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRequest(): ?string
    {
        return $this->request;
    }

    /**
     * @return $this
     */
    public function setRequest(?string $request): self
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDatabitsFormat(): ?int
    {
        return $this->databitsFormat;
    }

    /**
     * @return $this
     */
    public function setDatabitsFormat(?int $databitsFormat): self
    {
        $this->databitsFormat = $databitsFormat;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPort(): ?string
    {
        return $this->port;
    }

    /**
     * @return $this
     */
    public function setPort(?string $port): self
    {
        $this->port = $port;

        return $this;
    }
}
