<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Measure\Entity\Variable;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Project\Entity\DataEntryProject;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\UniqueVariableRepository")
 *
 * @ORM\Table(name="variable_unique", schema="adonis")
 */
class UniqueVariable extends Variable
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="uniqueVariables")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected ?DataEntryProject $project = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="connectedUniqueVariables")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected ?DataEntryProject $connectedDataEntryProject = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\GeneratorVariable", inversedBy="uniqueVariables")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected ?GeneratorVariable $generatorVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Material", inversedBy="uniqueVariables", cascade={"persist", "remove", "detach"})
     */
    protected ?Material $material = null;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\Variable\ValueHintList", mappedBy="variable", cascade={"persist","remove"}, orphanRemoval=true)
     */
    private ?ValueHintList $valueHintList = null;

    public function merge(Variable $variable): void
    {
        parent::merge($variable);
        if ($variable instanceof self) {
            $this->setValueHintList($variable->getValueHintList());
            $variable->setValueHintList(null);
        }
    }

    /**
     * @psalm-mutation-free
     */
    public function getValueHintList(): ?ValueHintList
    {
        return $this->valueHintList;
    }

    /**
     * @return $this
     */
    public function setValueHintList(?ValueHintList $valueHintList): self
    {
        if (null !== $this->valueHintList) {
            $this->valueHintList->setVariable(null);
        }
        $this->valueHintList = $valueHintList;
        if (null !== $valueHintList) {
            $valueHintList->setVariable($this);
        }

        return $this;
    }
}
