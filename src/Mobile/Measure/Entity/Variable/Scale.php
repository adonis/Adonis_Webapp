<?php

namespace Mobile\Measure\Entity\Variable;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\ScaleRepository")
 *
 * @ORM\Table(name="scale_data", schema="adonis")
 */
class Scale extends IdentifiedEntity
{
    /**
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable", inversedBy="scale")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Variable $variable = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $minValue = 0;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $maxValue = 0;

    /**
     * @var Collection<int, Mark>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\Mark", mappedBy="scale", cascade={"persist", "remove", "detach"})
     */
    private Collection $marks;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $exclusive;

    public function __construct()
    {
        $this->marks = new ArrayCollection();
        $this->exclusive = false;
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): ?Variable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(?Variable $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMinValue(): int
    {
        return $this->minValue;
    }

    /**
     * @return $this
     */
    public function setMinValue(int $minValue): self
    {
        $this->minValue = $minValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaxValue(): int
    {
        return $this->maxValue;
    }

    /**
     * @return $this
     */
    public function setMaxValue(int $maxValue): self
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    /**
     * @return Collection<int,  Mark>
     *
     * @psalm-mutation-free
     */
    public function getMarks(): Collection
    {
        return $this->marks;
    }

    /**
     * @param iterable<int,  Mark> $marks
     *
     * @return $this
     */
    public function setMarks(iterable $marks): self
    {
        ArrayCollectionUtils::update($this->marks, $marks, function (Mark $mark) {
            $mark->setScale($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addMark(Mark $mark): self
    {
        if (!$this->marks->contains($mark)) {
            $mark->setScale($this);
            $this->marks->add($mark);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeMark(Mark $mark): self
    {
        if ($this->marks->contains($mark)) {
            $this->marks->removeElement($mark);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isExclusive(): bool
    {
        return $this->exclusive;
    }

    /**
     * @return $this
     */
    public function setExclusive(bool $exclusive): self
    {
        $this->exclusive = $exclusive;

        return $this;
    }
}
