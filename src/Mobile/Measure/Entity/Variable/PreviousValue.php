<?php

namespace Mobile\Measure\Entity\Variable;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\PreviousValueRepository")
 *
 * @ORM\Table(name="previous_value", schema="adonis")
 */
class PreviousValue extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable", inversedBy="previousValues")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Variable $variable;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private Variable $originVariable;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $value = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $objectUri;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $date = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\StateCode")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?StateCode $state = null;

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): Variable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(Variable $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOriginVariable(): Variable
    {
        return $this->originVariable;
    }

    /**
     * @return $this
     */
    public function setOriginVariable(Variable $originVariable): self
    {
        $this->originVariable = $originVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getObjectUri(): string
    {
        return $this->objectUri;
    }

    /**
     * @return $this
     */
    public function setObjectUri(string $objectUri): self
    {
        $this->objectUri = $objectUri;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @return $this
     */
    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getState(): ?StateCode
    {
        return $this->state;
    }

    /**
     * @return $this
     */
    public function setState(?StateCode $state): self
    {
        $this->state = $state;

        return $this;
    }
}
