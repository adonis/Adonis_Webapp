<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Measure\Entity\Variable;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\ValueHintListRepository")
 *
 * @ORM\Table(name="value_hint_list", schema="adonis")
 */
class ValueHintList extends IdentifiedEntity
{
    /**
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\Variable\UniqueVariable", inversedBy="valueHintList")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?UniqueVariable $variable = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @var Collection<int, ValueHint>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\ValueHint", mappedBy="list", cascade={"persist", "remove", "detach"})
     */
    private Collection $valueHints;

    /**
     * @param non-empty-string $name
     * @param ValueHint[]      $valueHints
     */
    public static function build(string $name, array $valueHints): self
    {
        $entity = new self();
        $entity->setName($name);
        $entity->setValueHints(new ArrayCollection($valueHints));

        return $entity;
    }

    public function __construct()
    {
        $this->valueHints = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): ?UniqueVariable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(?UniqueVariable $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int,  ValueHint>
     *
     * @psalm-mutation-free
     */
    public function getValueHints(): Collection
    {
        return $this->valueHints;
    }

    /**
     * @param Collection<int,  ValueHint> $valueHints
     *
     * @return $this
     */
    public function setValueHints(Collection $valueHints): self
    {
        ArrayCollectionUtils::update($this->valueHints, $valueHints, function (ValueHint $valueHint) {
            $valueHint->setList($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addValueHint(ValueHint $valueHint): self
    {
        if (!$this->valueHints->contains($valueHint)) {
            $valueHint->setList($this);
            $this->valueHints->add($valueHint);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeValueHint(ValueHint $valueHint): self
    {
        if ($this->valueHints->contains($valueHint)) {
            $this->valueHints->removeElement($valueHint);
        }

        return $this;
    }
}
