<?php

namespace Mobile\Measure\Entity\Variable;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\DataEntryProject;
use Shared\Authentication\Entity\IdentifiedEntity;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="required_annotation", schema="adonis")
 *
 * @psalm-import-type PathLevelEnumId from PathLevelEnum
 */
class RequiredAnnotation extends IdentifiedEntity
{
    /**
     * @var PathLevelEnumId|''
     *
     * @ORM\Column(type="string")
     */
    private string $pathLevel = '';

    /**
     * @ORM\Column(type="integer")
     */
    private int $type = 0;

    /**
     * @ORM\Column(type="text")
     */
    private string $comment = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $askWhenEntering = false;

    /**
     * @ORM\Column(type="text")
     */
    private string $buisnessObjects = '';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="requiredAnnotations")
     */
    private DataEntryProject $dataEntryProject;

    /**
     * @var string[]
     *
     * @ORM\Column(type="array", nullable=false)
     */
    private array $category = [];

    /**
     * @var string[]
     *
     * @ORM\Column(type="array", nullable=false)
     */
    private array $keywords = [];

    /**
     * @return PathLevelEnumId|''
     *
     * @psalm-mutation-free
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @param PathLevelEnumId|'' $pathLevel
     *
     * @return $this
     */
    public function setPathLevel(string $pathLevel): self
    {
        $this->pathLevel = $pathLevel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return $this
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isAskWhenEntering(): bool
    {
        return $this->askWhenEntering;
    }

    /**
     * @return $this
     */
    public function setAskWhenEntering(bool $askWhenEntering): self
    {
        $this->askWhenEntering = $askWhenEntering;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBuisnessObjects(): string
    {
        return $this->buisnessObjects;
    }

    /**
     * @return $this
     */
    public function setBuisnessObjects(string $buisnessObjects): self
    {
        $this->buisnessObjects = $buisnessObjects;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDataEntryProject(): DataEntryProject
    {
        return $this->dataEntryProject;
    }

    /**
     * @return $this
     */
    public function setDataEntryProject(DataEntryProject $dataEntryProject): self
    {
        $this->dataEntryProject = $dataEntryProject;

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getCategory(): array
    {
        return $this->category;
    }

    /**
     * @param string[] $category
     *
     * @return $this
     */
    public function setCategory(array $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return $this
     */
    public function addCategory(string $category): self
    {
        if (!in_array($category, $this->category)) {
            $this->category[] = $category;
        }

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param string[] $keywords
     *
     * @return $this
     */
    public function setKeywords(array $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return $this
     */
    public function addKeyword(string $keyword): self
    {
        if (!in_array($keyword, $this->keywords)) {
            $this->keywords[] = $keyword;
        }

        return $this;
    }
}
