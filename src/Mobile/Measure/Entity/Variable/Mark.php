<?php

namespace Mobile\Measure\Entity\Variable;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\MarkRepository")
 *
 * @ORM\Table(name="mark", schema="adonis")
 */
class Mark extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $text = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $value = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Scale", inversedBy="marks")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Scale $scale;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $imageName = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $image = null;

    public function getImageData(): ?string
    {
        if (isset($this->imageName)) {
            return 'data:image/jpeg;base64, '.base64_encode(file_get_contents($this->imageName));
        }
        if (isset($this->image)) {
            return $this->image;
        }

        return null;
    }

    /**
     * @psalm-mutation-free
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return $this
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getScale(): Scale
    {
        return $this->scale;
    }

    /**
     * @return $this
     */
    public function setScale(Scale $scale): self
    {
        $this->scale = $scale;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    /**
     * @return $this
     */
    public function setImageName(?string $imageName): self
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return $this
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
