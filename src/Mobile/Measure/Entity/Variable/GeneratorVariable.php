<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Measure\Entity\Variable;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Project\Entity\DataEntryProject;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity(repositoryClass="Mobile\Measure\Repository\Variable\GeneratorVariableRepository")
 *
 * @ORM\Table(name="variable_generator", schema="adonis")
 */
class GeneratorVariable extends Variable
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="generatorVariables")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected ?DataEntryProject $project = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="connectedGeneratorVariables")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected ?DataEntryProject $connectedDataEntryProject = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\GeneratorVariable", inversedBy="generatorVariables")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected ?GeneratorVariable $generatorVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Material", inversedBy="generatorVariables")
     */
    protected ?Material $material = null;

    /**
     * @var Collection<int, UniqueVariable>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\UniqueVariable", mappedBy="generatorVariable", cascade={"persist", "remove", "detach"})
     */
    private Collection $uniqueVariables;

    /**
     * @var Collection<int, GeneratorVariable>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\GeneratorVariable", mappedBy="generatorVariable", cascade={"persist", "remove", "detach"})
     */
    private Collection $generatorVariables;

    /**
     * @ORM\Column(type="string")
     */
    private string $generatedPrefix;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $numeralIncrement = false;

    /**
     * @param non-empty-string $generatedPrefix
     */
    public static function build(string $generatedPrefix): self
    {
        return new self($generatedPrefix);
    }

    /**
     * @internal
     */
    public function __construct(string $generatedPrefix = '')
    {
        parent::__construct();
        $this->generatedPrefix = $generatedPrefix;
        $this->uniqueVariables = new ArrayCollection();
        $this->generatorVariables = new ArrayCollection();
    }

    /**
     * @return Collection<int,  UniqueVariable>
     *
     * @psalm-mutation-free
     */
    public function getUniqueVariables(): Collection
    {
        return $this->uniqueVariables;
    }

    /**
     * @param iterable<array-key, UniqueVariable> $uniqueVariables
     *
     * @return $this
     */
    public function setUniqueVariables(iterable $uniqueVariables): self
    {
        ArrayCollectionUtils::update($this->uniqueVariables, $uniqueVariables, function (UniqueVariable $uniqueVariable) {
            $uniqueVariable->setGeneratorVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addUniqueVariable(UniqueVariable $variable): self
    {
        if (!$this->uniqueVariables->contains($variable)) {
            $variable->setGeneratorVariable($this);
            $this->uniqueVariables->add($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeUniqueVariable(UniqueVariable $variable): self
    {
        if ($this->uniqueVariables->contains($variable)) {
            $this->uniqueVariables->removeElement($variable);
        }

        return $this;
    }

    /**
     * @return Collection<int,  GeneratorVariable>
     *
     * @psalm-mutation-free
     */
    public function getGeneratorVariables(): Collection
    {
        return $this->generatorVariables;
    }

    /**
     * @param iterable<array-key, GeneratorVariable> $generatorVariables
     *
     * @return $this
     */
    public function setGeneratorVariables(iterable $generatorVariables): self
    {
        ArrayCollectionUtils::update($this->generatorVariables, $generatorVariables, function (GeneratorVariable $generatorVariable) {
            $generatorVariable->setGeneratorVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addGeneratorVariable(self $variable): self
    {
        if (!$this->generatorVariables->contains($variable)) {
            $variable->setGeneratorVariable($this);
            $this->generatorVariables->add($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGeneratorVariable(self $variable): self
    {
        if ($this->generatorVariables->contains($variable)) {
            $this->generatorVariables->removeElement($variable);
        }

        return $this;
    }

    /**
     * @param Variable[] $generatedVariables
     *
     * @return $this
     */
    public function setGeneratedVariables(array $generatedVariables): self
    {
        $this->setUniqueVariables(array_filter($generatedVariables, fn (Variable $variable) => $variable instanceof UniqueVariable));
        $this->setGeneratorVariables(array_filter($generatedVariables, fn (Variable $variable) => $variable instanceof GeneratorVariable));

        return $this;
    }

    /**
     * @return $this
     */
    public function addGeneratedVariable(Variable $variable): self
    {
        if ($variable instanceof UniqueVariable) {
            $this->addUniqueVariable($variable);
        } elseif ($variable instanceof self) {
            $this->addGeneratorVariable($variable);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getGeneratedPrefix(): string
    {
        return $this->generatedPrefix;
    }

    /**
     * @return $this
     */
    public function setGeneratedPrefix(string $generatedPrefix): self
    {
        $this->generatedPrefix = $generatedPrefix;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isNumeralIncrement(): bool
    {
        return $this->numeralIncrement;
    }

    /**
     * @return $this
     */
    public function setNumeralIncrement(bool $numeralIncrement): self
    {
        $this->numeralIncrement = $numeralIncrement;

        return $this;
    }
}
