<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="field_generation", schema="adonis")
 */
class GeneratedField extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\FormField", inversedBy="generatedFields")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private FormField $formField;

    /**
     * @ORM\Column(type="integer")
     */
    private int $index = 0;

    /**
     * @ORM\Column(type="string")
     */
    private string $prefix = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $numeralIncrement = false;

    /**
     * @var Collection<int, FormField>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\FormField", mappedBy="generatedField", cascade={"persist", "remove", "detach"})
     */
    private Collection $formFields;

    public static function build(): self
    {
        return new self();
    }

    /**
     * @internal 
     * @see self::build()
     */
    public function __construct()
    {
        $this->formFields = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getFormField(): FormField
    {
        return $this->formField;
    }

    /**
     * @return $this
     */
    public function setFormField(FormField $formField): self
    {
        $this->formField = $formField;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @return $this
     */
    public function setIndex(int $index): GeneratedField
    {
        $this->index = $index;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @return $this
     */
    public function setPrefix(string $prefix): self
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isNumeralIncrement(): bool
    {
        return $this->numeralIncrement;
    }

    /**
     * @return $this
     */
    public function setNumeralIncrement(bool $numeralIncrement): self
    {
        $this->numeralIncrement = $numeralIncrement;

        return $this;
    }

    /**
     * @return Collection<int,  FormField>
     *
     * @psalm-mutation-free
     */
    public function getFormFields(): Collection
    {
        return $this->formFields;
    }

    /**
     * @param iterable<int, FormField> $formFields
     *
     * @return $this
     */
    public function setFormFields(iterable $formFields): self
    {
        ArrayCollectionUtils::update($this->formFields, $formFields, function (FormField $formField) {
            $formField->setGeneratedField($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addFormField(FormField $formField): self
    {
        if (!$this->formFields->contains($formField)) {
            $this->formFields->add($formField);
            $formField->setGeneratedField($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeFormField(FormField $formField): self
    {
        if ($this->formFields->contains($formField)) {
            $this->formFields->removeElement($formField);
            $formField->setGeneratedField(null);
        }

        return $this;
    }
}
