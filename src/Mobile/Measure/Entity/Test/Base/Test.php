<?php

namespace Mobile\Measure\Entity\Test\Base;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="test", schema="adonis")
 *
 * @ORM\InheritanceType("JOINED")
 *
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 *
 * @ORM\DiscriminatorMap({
 *     "RangeTest": "Mobile\Measure\Entity\Test\RangeTest",
 *     "GrowthTest": "Mobile\Measure\Entity\Test\GrowthTest",
 *     "PreconditionedCalculation": "Mobile\Measure\Entity\Test\PreconditionedCalculation",
 *     "CombinationTest": "Mobile\Measure\Entity\Test\CombinationTest",
 * })
 */
abstract class Test extends IdentifiedEntity
{
    protected Variable $variable;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $active = false;

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): Variable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(Variable $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    abstract public function getTypeTest(): string;

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return $this
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
