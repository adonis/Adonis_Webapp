<?php

namespace Mobile\Measure\Entity\Test;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Test\Base\Test;
use Mobile\Measure\Entity\Variable\Base\Variable;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="test_range", schema="adonis")
 */
class RangeTest extends Test
{
    public const RANGE_TEST = 'RangeTest';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable", inversedBy="rangeTests")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected Variable $variable;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $mandatoryMinValue = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $mandatoryMaxValue = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $optionalMinValue = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $optionalMaxValue = 0;

    /**
     * @psalm-mutation-free
     */
    public function getTypeTest(): string
    {
        return self::RANGE_TEST;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMandatoryMinValue(): float
    {
        return $this->mandatoryMinValue;
    }

    /**
     * @return $this
     */
    public function setMandatoryMinValue(float $mandatoryMinValue): self
    {
        $this->mandatoryMinValue = $mandatoryMinValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMandatoryMaxValue(): float
    {
        return $this->mandatoryMaxValue;
    }

    /**
     * @return $this
     */
    public function setMandatoryMaxValue(float $mandatoryMaxValue): self
    {
        $this->mandatoryMaxValue = $mandatoryMaxValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOptionalMinValue(): float
    {
        return $this->optionalMinValue;
    }

    /**
     * @return $this
     */
    public function setOptionalMinValue(float $optionalMinValue): self
    {
        $this->optionalMinValue = $optionalMinValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOptionalMaxValue(): float
    {
        return $this->optionalMaxValue;
    }

    /**
     * @return $this
     */
    public function setOptionalMaxValue(float $optionalMaxValue): self
    {
        $this->optionalMaxValue = $optionalMaxValue;

        return $this;
    }
}
