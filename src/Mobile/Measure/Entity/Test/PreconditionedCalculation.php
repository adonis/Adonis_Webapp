<?php

namespace Mobile\Measure\Entity\Test;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Test\Base\Test;
use Mobile\Measure\Entity\Variable\Base\Variable;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="test_preconditioned", schema="adonis")
 */
class PreconditionedCalculation extends Test
{
    public const PRECONDITIONED_TEST = 'PreconditionedCalculation';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable", inversedBy="preconditionedCalculations")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected Variable $variable;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Variable $comparisonVariable;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $condition = '';

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private ?float $comparedValue = null;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Variable $comparedVariable = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private string $affectationValue = '';

    /**
     * @psalm-mutation-free
     */
    public function getTypeTest(): string
    {
        return self::PRECONDITIONED_TEST;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComparisonVariable(): Variable
    {
        return $this->comparisonVariable;
    }

    /**
     * @return $this
     */
    public function setComparisonVariable(Variable $comparisonVariable): self
    {
        $this->comparisonVariable = $comparisonVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCondition(): string
    {
        return $this->condition;
    }

    /**
     * @return $this
     */
    public function setCondition(string $condition): self
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComparedValue(): ?float
    {
        return $this->comparedValue;
    }

    /**
     * @return $this
     */
    public function setComparedValue(?float $comparedValue): self
    {
        $this->comparedValue = $comparedValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComparedVariable(): ?Variable
    {
        return $this->comparedVariable;
    }

    /**
     * @return $this
     */
    public function setComparedVariable(?Variable $comparedVariable): self
    {
        $this->comparedVariable = $comparedVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAffectationValue(): string
    {
        return $this->affectationValue;
    }

    /**
     * @return $this
     */
    public function setAffectationValue(string $affectationValue): self
    {
        $this->affectationValue = $affectationValue;

        return $this;
    }
}
