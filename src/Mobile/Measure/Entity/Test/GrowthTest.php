<?php

namespace Mobile\Measure\Entity\Test;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Test\Base\Test;
use Mobile\Measure\Entity\Variable\Base\Variable;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="test_growth", schema="adonis")
 */
class GrowthTest extends Test
{
    public const GROWTH_TEST = 'GrowthTest';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable", inversedBy="growthTests")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected Variable $variable;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Variable $comparisonVariable;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $minDiff = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $maxDiff = 0;

    /**
     * @psalm-mutation-free
     */
    public function getTypeTest(): string
    {
        return self::GROWTH_TEST;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComparisonVariable(): Variable
    {
        return $this->comparisonVariable;
    }

    /**
     * @return $this
     */
    public function setComparisonVariable(Variable $comparisonVariable): self
    {
        $this->comparisonVariable = $comparisonVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMinDiff(): float
    {
        return $this->minDiff;
    }

    /**
     * @return $this
     */
    public function setMinDiff(float $minDiff): self
    {
        $this->minDiff = $minDiff;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaxDiff(): float
    {
        return $this->maxDiff;
    }

    /**
     * @return $this
     */
    public function setMaxDiff(float $maxDiff): self
    {
        $this->maxDiff = $maxDiff;

        return $this;
    }
}
