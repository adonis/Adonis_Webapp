<?php

namespace Mobile\Measure\Entity\Test;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Test\Base\Test;
use Mobile\Measure\Entity\Variable\Base\Variable;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="test_combination", schema="adonis")
 */
class CombinationTest extends Test
{
    public const COMBINATION_TEST = 'CombinationTest';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable", inversedBy="combinationTests")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    protected Variable $variable;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Variable $firstOperand;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Variable $secondOperand;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $operator = '';

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $highLimit = 0;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $lowLimit = 0;

    /**
     * @psalm-mutation-free
     */
    public function getTypeTest(): string
    {
        return self::COMBINATION_TEST;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFirstOperand(): Variable
    {
        return $this->firstOperand;
    }

    /**
     * @return $this
     */
    public function setFirstOperand(Variable $firstOperand): self
    {
        $this->firstOperand = $firstOperand;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSecondOperand(): Variable
    {
        return $this->secondOperand;
    }

    /**
     * @return $this
     */
    public function setSecondOperand(Variable $secondOperand): self
    {
        $this->secondOperand = $secondOperand;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return $this
     */
    public function setOperator(string $operator): self
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getHighLimit(): float
    {
        return $this->highLimit;
    }

    /**
     * @return $this
     */
    public function setHighLimit(float $highLimit): self
    {
        $this->highLimit = $highLimit;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLowLimit(): float
    {
        return $this->lowLimit;
    }

    /**
     * @return $this
     */
    public function setLowLimit(float $lowLimit): self
    {
        $this->lowLimit = $lowLimit;

        return $this;
    }
}
