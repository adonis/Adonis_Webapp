<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\ProjectObject;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="annotation", schema="adonis")
 *
 * @psalm-type AnnotationType = self::ANNOT_TYPE_*
 */
class Annotation extends IdentifiedEntity
{
    public const ANNOT_TYPE_TEXT = 0;
    public const ANNOT_TYPE_IMAG = 1;
    public const ANNOT_TYPE_SONG = 2;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\ProjectObject", inversedBy="annotations")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private ProjectObject $projectObject;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $name = null;

    /**
     * @var AnnotationType
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $type = self::ANNOT_TYPE_TEXT;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $value = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $image = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $timestamp;

    /**
     * @var string[]
     *
     * @ORM\Column(type="array", nullable=false)
     */
    private array $categories = [];

    /**
     * @var string[]
     *
     * @ORM\Column(type="array", nullable=false)
     */
    private array $keywords = [];

    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @psalm-mutation-free
     */
    public function getProjectObject(): ProjectObject
    {
        return $this->projectObject;
    }

    /**
     * @return $this
     */
    public function setProjectObject(ProjectObject $projectObject): self
    {
        $this->projectObject = $projectObject;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return AnnotationType
     *
     * @psalm-mutation-free
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param AnnotationType $type
     *
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return $this
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    /**
     * @return $this
     */
    public function setTimestamp(\DateTime $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param string[] $categories
     *
     * @return $this
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return $this
     */
    public function addCategory(string $category): self
    {
        if (!in_array($category, $this->categories)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param string[] $keywords
     *
     * @return $this
     */
    public function setKeywords(array $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * @return $this
     */
    public function addKeyword(string $keyword): self
    {
        if (!in_array($keyword, $this->keywords)) {
            $this->keywords[] = $keyword;
        }

        return $this;
    }
}
