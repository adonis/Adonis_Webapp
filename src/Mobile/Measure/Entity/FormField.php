<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Device\Entity\BusinessObject;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="field_measure", schema="adonis")
 */
class FormField extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Session", inversedBy="formFields")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Session $session = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\GeneratedField", inversedBy="formFields")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?GeneratedField $generatedField = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\Base\Variable")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Variable $variable;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\BusinessObject")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private BusinessObject $target;

    /**
     * @var Collection<int, Measure>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Measure", mappedBy="formField", cascade={"persist", "remove", "detach"})
     */
    private Collection $measures;

    /**
     * @var Collection<int, GeneratedField>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\GeneratedField", mappedBy="formField", cascade={"persist", "remove", "detach"})
     */
    private Collection $generatedFields;

    public static function build(Variable $variable, BusinessObject $target): self
    {
        return new FormField($variable, $target);
    }

    /**
     * @internal
     * @see self::build()
     */
    public function __construct(Variable $variable = null, BusinessObject $target = null)
    {
        if (null !== $variable) {
            $this->variable = $variable;
        }
        if (null !== $target) {
            $this->target = $target;
        }
        $this->measures = new ArrayCollection();
        $this->generatedFields = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getSession(): ?Session
    {
        return $this->session;
    }

    /**
     * @return $this
     */
    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getGeneratedField(): ?GeneratedField
    {
        return $this->generatedField;
    }

    /**
     * @return $this
     */
    public function setGeneratedField(?GeneratedField $generatedField): FormField
    {
        $this->generatedField = $generatedField;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): Variable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(Variable $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTarget(): BusinessObject
    {
        return $this->target;
    }

    /**
     * @return $this
     */
    public function setTarget(BusinessObject $target): self
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return Collection<int,  Measure>
     *
     * @psalm-mutation-free
     */
    public function getMeasures(): Collection
    {
        return $this->measures;
    }

    /**
     * @param iterable<int, Measure> $measures
     *
     * @return $this
     */
    public function setMeasures(iterable $measures): self
    {
        ArrayCollectionUtils::update($this->measures, $measures, function (Measure $measure) {
            $measure->setFormField($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addMeasure(Measure $measure): self
    {
        if (!$this->measures->contains($measure)) {
            $this->measures->add($measure);
            $measure->setFormField($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeMeasure(Measure $measure): self
    {
        if ($this->measures->contains($measure)) {
            $this->measures->removeElement($measure);
        }

        return $this;
    }

    /**
     * @return Collection<int,  GeneratedField>
     *
     * @psalm-mutation-free
     */
    public function getGeneratedFields(): Collection
    {
        return $this->generatedFields;
    }

    /**
     * @param iterable<int,  GeneratedField> $generatedFields
     *
     * @return $this
     */
    public function setGeneratedFields(iterable $generatedFields): self
    {
        ArrayCollectionUtils::update($this->generatedFields, $generatedFields, function (GeneratedField $generatedField) {
            $generatedField->setFormField($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addGeneratedField(GeneratedField $generatedField): self
    {
        if (!$this->generatedFields->contains($generatedField)) {
            $this->generatedFields->add($generatedField);
            $generatedField->setFormField($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGeneratedField(GeneratedField $generatedField): self
    {
        if ($this->generatedFields->contains($generatedField)) {
            $this->generatedFields->removeElement($generatedField);
        }

        return $this;
    }
}
