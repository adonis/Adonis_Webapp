<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\ValueHintList;

/**
 * Class ValueHintListRepositoryRepository.
 *
 * @method ValueHintList|null find($id, $lockMode = null, $lockVersion = null)
 * @method ValueHintList|null findOneBy(array $criteria, array $orderBy = null)
 * @method ValueHintList[] findAll()
 * @method ValueHintList[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValueHintListRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ValueHintList::class);
    }
}
