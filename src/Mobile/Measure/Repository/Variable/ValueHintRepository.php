<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\ValueHint;

/**
 * Class ValueHintRepositoryRepository.
 *
 * @method ValueHint|null find($id, $lockMode = null, $lockVersion = null)
 * @method ValueHint|null findOneBy(array $criteria, array $orderBy = null)
 * @method ValueHint[] findAll()
 * @method ValueHint[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValueHintRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ValueHint::class);
    }
}
