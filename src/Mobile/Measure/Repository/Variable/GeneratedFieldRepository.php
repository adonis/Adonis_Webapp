<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\GeneratedField;

/**
 * Class GeneratorVariableRepository.
 *
 * @method GeneratedField|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeneratedField|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeneratedField[] findAll()
 * @method GeneratedField[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneratedFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeneratedField::class);
    }

}
