<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\RequiredAnnotation;

/**
 * Class RequiredAnnotationRepository.
 *
 * @method RequiredAnnotation|null find($id, $lockMode = null, $lockVersion = null)
 * @method RequiredAnnotation|null findOneBy(array $criteria, array $orderBy = null)
 * @method RequiredAnnotation[] findAll()
 * @method RequiredAnnotation[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RequiredAnnotationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequiredAnnotation::class);
    }
}
