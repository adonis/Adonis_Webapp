<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\PreviousValue;

/**
 * Class PreviousValueRepositoryRepository.
 *
 * @method PreviousValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method PreviousValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method PreviousValue[] findAll()
 * @method PreviousValue[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PreviousValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PreviousValue::class);
    }
}
