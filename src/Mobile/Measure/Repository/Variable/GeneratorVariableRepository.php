<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\GeneratorVariable;

/**
 * Class GeneratorVariableRepository.
 *
 * @method GeneratorVariable|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeneratorVariable|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeneratorVariable[] findAll()
 * @method GeneratorVariable[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneratorVariableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeneratorVariable::class);
    }

}
