<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\UniqueVariable;

/**
 * Class UniqueVariableRepository.
 *
 * @method UniqueVariable|null find($id, $lockMode = null, $lockVersion = null)
 * @method UniqueVariable|null findOneBy(array $criteria, array $orderBy = null)
 * @method UniqueVariable[] findAll()
 * @method UniqueVariable[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UniqueVariableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UniqueVariable::class);
    }
}
