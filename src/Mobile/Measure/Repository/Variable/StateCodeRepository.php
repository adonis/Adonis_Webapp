<?php

namespace Mobile\Measure\Repository\Variable;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\Variable\StateCode;

/**
 * Class StateCodeRepository.
 *
 * @method StateCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method StateCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method StateCode[] findAll()
 * @method StateCode[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StateCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StateCode::class);
    }
}
