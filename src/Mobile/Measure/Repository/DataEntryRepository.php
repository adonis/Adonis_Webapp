<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Measure\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Measure\Entity\DataEntry;

/**
 * Class DataEntryRepository
 *
 * @method DataEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataEntry[] findAll()
 * @method DataEntry[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataEntry::class);
    }
}
