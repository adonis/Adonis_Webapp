<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Shared\ApiTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Exception\InvalidArgumentException;
use Doctrine\Common\Collections\Collection;
use Mobile\Device\Dto\BlockInput;
use Mobile\Device\Dto\DeviceInput;
use Mobile\Device\Dto\FactorInput;
use Mobile\Device\Dto\IndividualInput;
use Mobile\Device\Dto\ModalityInput;
use Mobile\Device\Dto\NoteInput;
use Mobile\Device\Dto\OutExperimentationZoneInput;
use Mobile\Device\Dto\ProtocolInput;
use Mobile\Device\Dto\SubBlockInput;
use Mobile\Device\Dto\TreatmentInput;
use Mobile\Device\Dto\UnitParcelInput;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\Device;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\Factor;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\Modality;
use Mobile\Device\Entity\OutExperimentationZone;
use Mobile\Device\Entity\Protocol;
use Mobile\Device\Entity\SubBlock;
use Mobile\Device\Entity\Treatment;
use Mobile\Device\Entity\UnitParcel;

class DeviceOutputDataTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $object
     */
    public function transform($object, string $to, array $context = []): DeviceInput
    {
        if (!$object instanceof Device) {
            throw new InvalidArgumentException('$object must be an instance of '.Device::class);
        }

        return $this->transform2device($object);
    }

    /**
     * @param mixed $data
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof Device && (DeviceInput::class === $to);
    }

    private function transform2device(Device $device): DeviceInput
    {
        $output = new DeviceInput();
        $output
            ->setCreationDate($device->getCreationDate())
            ->setValidationDate($device->getValidationDate())
            ->setUri(str_replace('/', '', $device->getUri()))
            ->setName($device->getName())
            ->setBlocks($this->transform2blocksArray($device->getBlocks()))
            ->setProtocol($this->transform2protocol($device->getProtocol()))
            ->setIndividualPU($device->isIndividualPU())
            ->setNotes($this->transform2NotesArray($device->getNotes()))
            ->setOutExperimentationZones($this->transform2OutExperimentationZoneArray($device->getOutExperimentationZones()));

        return $output;
    }

    /**
     * @param Collection<int, Block> $blocks
     *
     * @return BlockInput[]
     */
    private function transform2blocksArray(Collection $blocks): array
    {
        return array_map(
            function (Block $block) {
                $output = new BlockInput();
                $output
                    ->setUri(str_replace('/', '', $block->getUri()))
                    ->setName($block->getName())
                    ->setSubBlocks($this->transform2subBlocksArray($block->getSubBlocks()))
                    ->setUnitParcels($this->transform2parcelsArray($block->getUnitParcels()))
                    ->setNotes($this->transform2NotesArray($block->getNotes()))
                    ->setOutExperimentationZones($this->transform2OutExperimentationZoneArray($block->getOutExperimentationZones()));

                return $output;
            },
            $blocks->toArray()
        );
    }

    /**
     * @param Collection<int, SubBlock> $subBlocks
     *
     * @return SubBlockInput[]
     */
    private function transform2subBlocksArray(Collection $subBlocks): array
    {
        return array_map(
            function (SubBlock $subBlock) {
                $output = new SubBlockInput();
                $output
                    ->setUri(str_replace('/', '', $subBlock->getUri()))
                    ->setName($subBlock->getName())
                    ->setUnitParcels($this->transform2parcelsArray($subBlock->getUnitParcels()))
                    ->setNotes($this->transform2NotesArray($subBlock->getNotes()))
                    ->setOutExperimentationZones($this->transform2OutExperimentationZoneArray($subBlock->getOutExperimentationZones()));

                return $output;
            },
            $subBlocks->toArray()
        );
    }

    /**
     * @param Collection<int, UnitParcel> $parcels
     *
     * @return UnitParcelInput[]
     */
    private function transform2parcelsArray(Collection $parcels): array
    {
        return array_map(
            function (UnitParcel $parcel) {
                $output = new UnitParcelInput();
                $stateCode = null !== $parcel->getStateCode() ? $parcel->getStateCode()->getCode() : null;
                $output
                    ->setUri(str_replace('/', '', $parcel->getUri()))
                    ->setName($parcel->getName())
                    ->setX($parcel->getX())
                    ->setY($parcel->getY())
                    ->setAparitionDate($parcel->getApparitionDate())
                    ->setDemiseDate($parcel->getDemiseDate())
                    ->setTreatmentName(null === $parcel->getTreatment() ? null : $parcel->getTreatment()->getName())
                    ->setIndividuals($this->transform2individualsArray($parcel->getIndividuals()))
                    ->setIdent($parcel->getIdent())
                    ->setStateCode($stateCode)
                    ->setNotes($this->transform2NotesArray($parcel->getNotes()))
                    ->setOutExperimentationZones($this->transform2OutExperimentationZoneArray($parcel->getOutExperimentationZones()));

                return $output;
            },
            $parcels->toArray()
        );
    }

    /**
     * @param Collection<int, Individual> $individuals
     *
     * @return IndividualInput[]
     */
    private function transform2individualsArray(Collection $individuals): array
    {
        return array_map(
            function (Individual $individual) {
                $output = new IndividualInput();
                $stateCode = null !== $individual->getStateCode() ? $individual->getStateCode()->getCode() : null;
                $output
                    ->setAparitionDate($individual->getApparitionDate())
                    ->setDemiseDate($individual->getDemiseDate())
                    ->setUri(str_replace('/', '', $individual->getUri()))
                    ->setName($individual->getName())
                    ->setX($individual->getX())
                    ->setY($individual->getY())
                    ->setIdent($individual->getIdent())
                    ->setStateCode($stateCode)
                    ->setNotes($this->transform2NotesArray($individual->getNotes()));

                return $output;
            },
            $individuals->toArray()
        );
    }

    /**
     * @param Collection<int, EntryNote> $notes
     *
     * @return NoteInput[]
     */
    private function transform2NotesArray(Collection $notes): array
    {
        return array_map(
            function (EntryNote $note) {
                $output = new NoteInput();
                $output->setText($note->getText())
                    ->setDeleted($note->isDeleted())
                    ->setCreationDate($note->getCreationDate())
                    ->setCreatorUri($note->getCreator()->getUri())
                    ->setCreatorName($note->getCreator()->getUsername());

                return $output;
            },
            $notes->toArray()
        );
    }

    /**
     * @param Collection<int, OutExperimentationZone> $outExperimentationZones
     *
     * @return OutExperimentationZoneInput[]
     */
    private function transform2OutExperimentationZoneArray(Collection $outExperimentationZones): array
    {
        return array_map(
            function (OutExperimentationZone $outExperimentationZone) {
                $output = new OutExperimentationZoneInput();
                $output
                    ->setUri($outExperimentationZone->getUri())
                    ->setNatureZHEUri($outExperimentationZone->getNatureZHE()->getUri())
                    ->setNum($outExperimentationZone->getNumber())
                    ->setX($outExperimentationZone->getX())
                    ->setY($outExperimentationZone->getY());

                return $output;
            },
            $outExperimentationZones->toArray()
        );
    }

    private function transform2protocol(Protocol $protocol): ProtocolInput
    {
        $creatorLogin = null !== $protocol->getCreator() ? $protocol->getCreator()->getLogin() : null;
        $output = new ProtocolInput();
        $output
            ->setName($protocol->getName())
            ->setAim($protocol->getAim())
            ->setAlgorithm($protocol->getAlgorithm())
            ->setFactors($this->transform2factorsArray($protocol->getFactors()))
            ->setTreatments($this->transform2treatmentsArray($protocol->getTreatments()))
            ->setCreationdate($protocol->getCreationDate())
            ->setNbIndividualsPerParcel($protocol->getNbIndividualsPerParcel())
            ->setCreatorLogin($creatorLogin);

        return $output;
    }

    /**
     * @param Collection<int,  Factor> $factors
     *
     * @return FactorInput[]
     */
    private function transform2factorsArray(Collection $factors): array
    {
        return array_map(
            function (Factor $factor) {
                $output = new FactorInput();
                $output
                    ->setName($factor->getName())
                    ->setModalities($this->transform2modalitiesArray($factor->getModalities()));

                return $output;
            },
            $factors->toArray()
        );
    }

    /**
     * @param Collection<int,  Modality> $modalities
     *
     * @return ModalityInput[]
     */
    private function transform2modalitiesArray(Collection $modalities): array
    {
        return array_map(
            function (Modality $modality) {
                $output = new ModalityInput();
                $output->setValue($modality->getValue());

                return $output;
            },
            $modalities->toArray()
        );
    }

    /**
     * @param Collection<int,  Treatment> $treatments
     *
     * @return TreatmentInput[]
     */
    private function transform2treatmentsArray(Collection $treatments): array
    {
        return array_map(
            function (Treatment $treatment) {
                $output = new TreatmentInput();
                $output
                    ->setUri(str_replace('/', '', $treatment->getUri()))
                    ->setName($treatment->getName())
                    ->setShortName($treatment->getShortName())
                    ->setModalitiesValue($treatment->getModalitiesValue())
                    ->setRepetitions($treatment->getRepetitions());

                return $output;
            },
            $treatments->toArray()
        );
    }
}
