<?php


namespace Mobile\Shared\ApiTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use Mobile\Project\Dto\NatureZHEInput;
use Mobile\Project\Entity\NatureZHE;

class NatureZheOutputDataTransformer implements DataTransformerInterface
{

    public function transform($object, string $to, array $context = []): NatureZHEInput
    {
        /** @var NatureZHE $object */
        return $this->transform2natureZHE($object);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return $data instanceof NatureZHE && NatureZHEInput::class === $to ;
    }

    private function transform2natureZHE(NatureZHE $natureZHE): NatureZHEInput
    {
        $output = new NatureZHEInput();
        $output
            ->setName($natureZHE->getName())
            ->setColor($natureZHE->getColor())
            ->setUri($natureZHE->getUri())
            ->setTexture($natureZHE->getTexture());
        return $output;
    }
}