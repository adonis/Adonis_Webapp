<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Shared\Dto;

use Mobile\Measure\Dto\AnnotationInput;

/**
 * Class HasAnnotationInput.
 */
abstract class HasAnnotationInput
{
    /**
     * @var AnnotationInput[]|null
     */
    private $annotations;

    /**
     * @return AnnotationInput[]|null
     */
    public function getAnnotations(): ?array
    {
        return $this->annotations;
    }

    /**
     * @param AnnotationInput[]|null $annotations
     * @return HasAnnotationInput
     */
    public function setAnnotations(?array $annotations): HasAnnotationInput
    {
        $this->annotations = $annotations;
        return $this;
    }
}
