<?php

namespace Mobile\Shared\Dto;

/**
 * Class IdentifiedOI.
 */
class IdentifiedOI
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return IdentifiedOI
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
}
