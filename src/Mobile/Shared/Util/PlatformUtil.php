<?php


namespace Mobile\Shared\Util;


use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\BusinessObject;
use Mobile\Device\Entity\Device;
use Mobile\Device\Entity\SubBlock;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Project\Entity\Platform;

class PlatformUtil
{
    /**
     * @param Platform | BusinessObject $obj
     * @return ?Collection<BusinessObject>
     */
    public static function getChildren($obj): ?Collection{
        if($obj instanceof Platform){
            return $obj->getDevices();
        }elseif ($obj instanceof Device){
            return $obj->getBlocks();
        }elseif ($obj instanceof Block){
            if($obj->getSubBlocks() !== null && $obj->getSubBlocks()->count()>0){
                return $obj->getSubBlocks();
            }else{
                return $obj->getUnitParcels();
            }
        }elseif ($obj instanceof SubBlock){
            return $obj->getUnitParcels();
        }elseif ($obj instanceof UnitParcel){
            return $obj->getIndividuals();
        }
        return null;
    }
}