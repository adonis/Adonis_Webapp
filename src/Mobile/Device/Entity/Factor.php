<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\FactorRepository")
 *
 * @ORM\Table(name="factor", schema="adonis")
 */
class Factor extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Protocol", inversedBy="factors")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Protocol $protocol;

    /**
     * @var Collection<int, Modality>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\Modality", mappedBy="factor", cascade={"persist", "remove", "detach"})
     */
    private Collection $modalities;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name;

    /**
     * @param non-empty-string               $name
     * @param non-empty-array<int, Modality> $modalities
     */
    public static function build(string $name, array $modalities): self
    {
        $entity = new self($name);
        $entity->setModalities($modalities);

        return $entity;
    }

    public function __construct(string $name = '')
    {
        $this->modalities = new ArrayCollection();
        $this->name = $name;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(Protocol $protocol): self
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * @return Collection<int, Modality>
     *
     * @psalm-mutation-free
     */
    public function getModalities(): Collection
    {
        return $this->modalities;
    }

    /**
     * @param iterable<int, Modality> $modalities
     *
     * @return $this
     */
    public function setModalities(iterable $modalities): self
    {
        ArrayCollectionUtils::update($this->modalities, $modalities, function (Modality $modality) {
            $modality->setFactor($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addModality(Modality $modality): self
    {
        if (!$this->modalities->contains($modality)) {
            $modality->setFactor($this);
            $this->modalities->add($modality);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeModality(Modality $modality): self
    {
        if ($this->modalities->contains($modality)) {
            $this->modalities->removeElement($modality);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
