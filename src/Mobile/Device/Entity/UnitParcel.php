<?php

namespace Mobile\Device\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\StateCode;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\UnitParcelRepository")
 *
 * @ORM\Table(name="unit_plot", schema="adonis")
 */
class UnitParcel extends BusinessObject
{
    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $x = 0;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $y = 0;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\StateCode", cascade={"persist", "remove", "detach"})
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?StateCode $stateCode = null;

    /**
     * @var Collection<int, Individual>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\Individual", mappedBy="unitParcel", cascade={"persist", "remove", "detach"})
     */
    private Collection $individuals;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\SubBlock", inversedBy="unitParcels")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?SubBlock $subBlock = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Block", inversedBy="unitParcels")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Block $block = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Treatment")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Treatment $treatment = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $ident = null;

    /**
     * @var Collection<int, OutExperimentationZone>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\OutExperimentationZone", mappedBy="unitParcel", cascade={"persist", "remove", "detach"})
     */
    private Collection $outExperimentationZones;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $apparitionDate = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $demiseDate = null;

    public function __construct(string $name = '')
    {
        parent::__construct($name);
        $this->individuals = new ArrayCollection();
        $this->outExperimentationZones = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStateCode(): ?StateCode
    {
        return $this->stateCode;
    }

    /**
     * @return $this
     */
    public function setStateCode(?StateCode $stateCode): self
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * @return Collection<int,  Individual>
     *
     * @psalm-mutation-free
     */
    public function getIndividuals(): Collection
    {
        return $this->individuals;
    }

    /**
     * @param iterable<int,  Individual> $individuals
     */
    public function setIndividuals(iterable $individuals): self
    {
        ArrayCollectionUtils::update($this->individuals, $individuals, function (Individual $individual) {
            $individual->setUnitParcel($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addIndividual(Individual $individual): self
    {
        if (!$this->individuals->contains($individual)) {
            $individual->setUnitParcel($this);
            $this->individuals->add($individual);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeIndividual(Individual $individual): self
    {
        if ($this->individuals->contains($individual)) {
            $this->individuals->removeElement($individual);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlock(): ?SubBlock
    {
        return $this->subBlock;
    }

    /**
     * @return $this
     */
    public function setSubBlock(?SubBlock $subBlock): self
    {
        $this->subBlock = $subBlock;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlock(): ?Block
    {
        return $this->block;
    }

    /**
     * @return $this
     */
    public function setBlock(?Block $block): self
    {
        $this->block = $block;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTreatment(): ?Treatment
    {
        return $this->treatment;
    }

    /**
     * @return $this
     */
    public function setTreatment(?Treatment $treatment): self
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIdent(): ?string
    {
        return $this->ident;
    }

    /**
     * @return $this
     */
    public function setIdent(?string $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    /**
     * @return Collection<int,  OutExperimentationZone>
     *
     * @psalm-mutation-free
     */
    public function getOutExperimentationZones(): Collection
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param iterable<int,  OutExperimentationZone> $outExperimentationZones
     */
    public function setOutExperimentationZones(iterable $outExperimentationZones): UnitParcel
    {
        ArrayCollectionUtils::update($this->outExperimentationZones, $outExperimentationZones, function (OutExperimentationZone $outExperimentationZone) {
            $outExperimentationZone->setUnitParcel($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if (!$this->outExperimentationZones->contains($outExperimentationZone)) {
            $outExperimentationZone->setUnitParcel($this);
            $this->outExperimentationZones->add($outExperimentationZone);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if ($this->outExperimentationZones->contains($outExperimentationZone)) {
            $outExperimentationZone->setUnitParcel(null);
            $this->outExperimentationZones->removeElement($outExperimentationZone);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getApparitionDate(): ?\DateTime
    {
        return $this->apparitionDate;
    }

    /**
     * @return $this
     */
    public function setApparitionDate(?\DateTime $apparitionDate): self
    {
        $this->apparitionDate = $apparitionDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDemiseDate(): ?\DateTime
    {
        return $this->demiseDate;
    }

    /**
     * @return $this
     */
    public function setDemiseDate(?\DateTime $demiseDate): self
    {
        $this->demiseDate = $demiseDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function parent(): ?BusinessObject
    {
        return $this->getSubBlock() ?? $this->getBlock();
    }
}
