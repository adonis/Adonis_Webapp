<?php

namespace Mobile\Device\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Variable\StateCode;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\IndividualRepository")
 *
 * @ORM\Table(name="individual", schema="adonis")
 */
class Individual extends BusinessObject
{
    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $x = 0;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $y = 0;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Measure\Entity\Variable\StateCode", cascade={"persist", "remove", "detach"})
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?StateCode $stateCode = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\UnitParcel", inversedBy="individuals")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private UnitParcel $unitParcel;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $apparitionDate;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $demiseDate = null;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $ident = null;

    public function __construct(string $name = '')
    {
        parent::__construct($name);
        $this->apparitionDate = new \DateTime();
    }

    public function __toString(): string
    {
        return $this->getUri();
    }

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStateCode(): ?StateCode
    {
        return $this->stateCode;
    }

    /**
     * @return $this
     */
    public function setStateCode(?StateCode $stateCode): self
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitParcel(): UnitParcel
    {
        return $this->unitParcel;
    }

    /**
     * @return $this
     */
    public function setUnitParcel(UnitParcel $unitParcel): self
    {
        $this->unitParcel = $unitParcel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getApparitionDate(): \DateTime
    {
        return $this->apparitionDate;
    }

    /**
     * @return $this
     */
    public function setApparitionDate(\DateTime $apparitionDate): self
    {
        $this->apparitionDate = $apparitionDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDemiseDate(): ?\DateTime
    {
        return $this->demiseDate;
    }

    /**
     * @return $this
     */
    public function setDemiseDate(?\DateTime $demiseDate): self
    {
        $this->demiseDate = $demiseDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIdent(): ?string
    {
        return $this->ident;
    }

    /**
     * @return $this
     */
    public function setIdent(?string $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function parent(): ?BusinessObject
    {
        return $this->getUnitParcel();
    }
}
