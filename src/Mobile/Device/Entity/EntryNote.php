<?php

namespace Mobile\Device\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\EntryNoteRepository")
 *
 * @ORM\Table(name="note", schema="adonis")
 */
class EntryNote extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="text", nullable=false, options={"default"=""})
     */
    private string $text = '';

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $deleted = false;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\BusinessObject", inversedBy="notes")
     */
    private BusinessObject $businessObject;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     */
    private User $creator;

    /**
     * @psalm-mutation-free
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return $this
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return $this
     */
    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBusinessObject(): BusinessObject
    {
        return $this->businessObject;
    }

    /**
     * @return $this
     */
    public function setBusinessObject(BusinessObject $businessObject): self
    {
        $this->businessObject = $businessObject;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreator(): User
    {
        return $this->creator;
    }

    /**
     * @return $this
     */
    public function setCreator(User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }
}
