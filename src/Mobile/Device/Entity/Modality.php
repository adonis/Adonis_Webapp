<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\ModalityRepository")
 *
 * @ORM\Table(name="modality", schema="adonis")
 */
class Modality extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $value;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Factor", inversedBy="modalities")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Factor $factor;

    /**
     * @param non-empty-string $value
     */
    public static function build(string $value): self
    {
        return new self($value);
    }

    public function __construct(string $value = '')
    {
        $this->value = $value;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFactor(): Factor
    {
        return $this->factor;
    }

    /**
     * @return $this
     */
    public function setFactor(Factor $factor): self
    {
        $this->factor = $factor;

        return $this;
    }
}
