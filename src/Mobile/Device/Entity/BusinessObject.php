<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\ProjectObject;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 *
 * @ORM\Table("business_object", schema="adonis")
 *
 * @ORM\InheritanceType(value="JOINED")
 *
 * @ORM\DiscriminatorColumn("discr", type="string")
 *
 * @ORM\DiscriminatorMap({
 *     "Device" = "Mobile\Device\Entity\Device",
 *     "Block" = "Mobile\Device\Entity\Block",
 *     "SubBlock" = "Mobile\Device\Entity\SubBlock",
 *     "UnitParcel" = "Mobile\Device\Entity\UnitParcel",
 *     "Individual" = "Mobile\Device\Entity\Individual"
 * })
 */
abstract class BusinessObject extends ProjectObject
{
    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name;

    /**
     * @var Collection<int, EntryNote>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\EntryNote", mappedBy="businessObject", cascade={"persist", "remove", "detach"})
     */
    private Collection $notes;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Device\Entity\Anomaly", inversedBy="businessObject", cascade={"persist", "remove", "detach"})
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Anomaly $anomaly = null;

    public function __construct(string $name = '')
    {
        parent::__construct();
        $this->notes = new ArrayCollection();
        $this->name = $name;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int,  EntryNote>
     *
     * @psalm-mutation-free
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    /**
     * @param iterable<int,  EntryNote> $notes
     */
    public function setNotes(iterable $notes): self
    {
        ArrayCollectionUtils::update($this->notes, $notes, function (EntryNote $entryNote) {
            $entryNote->setBusinessObject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addNote(EntryNote $note): self
    {
        if (!$this->notes->contains($note)) {
            $note->setBusinessObject($this);
            $this->notes->add($note);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeNote(EntryNote $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAnomaly(): ?Anomaly
    {
        return $this->anomaly;
    }

    /**
     * @return $this
     */
    public function setAnomaly(?Anomaly $anomaly): self
    {
        $this->anomaly = $anomaly;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    abstract public function parent(): ?BusinessObject;
}
