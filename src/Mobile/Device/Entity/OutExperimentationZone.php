<?php

namespace Mobile\Device\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\NatureZHE;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\OutExperimentationZoneRepository")
 *
 * @ORM\Table(name="out_experimentation_zone", schema="adonis")
 */
class OutExperimentationZone extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $x = 0;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $y = 0;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $number = '';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\NatureZHE")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private NatureZHE $natureZHE;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Device", inversedBy="outExperimentationZones")
     */
    private ?Device $device = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Block", inversedBy="outExperimentationZones")
     */
    private ?Block $block = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\SubBlock", inversedBy="outExperimentationZones")
     */
    private ?SubBlock $subBlock = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\UnitParcel", inversedBy="outExperimentationZones")
     */
    private ?UnitParcel $unitParcel = null;

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @return $this
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getNatureZHE(): NatureZHE
    {
        return $this->natureZHE;
    }

    /**
     * @return $this
     */
    public function setNatureZHE(NatureZHE $natureZHE): self
    {
        $this->natureZHE = $natureZHE;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDevice(): ?Device
    {
        return $this->device;
    }

    /**
     * @return $this
     */
    public function setDevice(?Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlock(): ?Block
    {
        return $this->block;
    }

    /**
     * @return $this
     */
    public function setBlock(?Block $block): self
    {
        $this->block = $block;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlock(): ?SubBlock
    {
        return $this->subBlock;
    }

    /**
     * @return $this
     */
    public function setSubBlock(?SubBlock $subBlock): self
    {
        $this->subBlock = $subBlock;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitParcel(): ?UnitParcel
    {
        return $this->unitParcel;
    }

    /**
     * @return $this
     */
    public function setUnitParcel(?UnitParcel $unitParcel): self
    {
        $this->unitParcel = $unitParcel;

        return $this;
    }
}
