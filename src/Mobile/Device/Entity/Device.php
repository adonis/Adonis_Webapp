<?php

namespace Mobile\Device\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\Platform;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\DeviceRepository")
 *
 * @ORM\Table(name="experiment", schema="adonis")
 */
class Device extends BusinessObject
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\Platform", inversedBy="devices")
     *
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    private ?Platform $platform = null;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $individualPU = false;

    /**
     * @var Collection<int, Block>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\Block", mappedBy="device", cascade={"persist", "remove", "detach"})
     */
    private Collection $blocks;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Device\Entity\Protocol", mappedBy="device", cascade={"persist", "remove", "detach"})
     */
    private Protocol $protocol;

    /**
     * @var Collection<int, OutExperimentationZone>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\OutExperimentationZone", mappedBy="device", cascade={"persist", "remove", "detach"})
     */
    private Collection $outExperimentationZones;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $creationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $validationDate = null;

    /**
     * Device constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->blocks = new ArrayCollection();
        $this->outExperimentationZones = new ArrayCollection();
        $this->creationDate = new \DateTime();
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): ?Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(?Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isIndividualPU(): bool
    {
        return $this->individualPU;
    }

    /**
     * @return $this
     */
    public function setIndividualPU(bool $individualPU): self
    {
        $this->individualPU = $individualPU;

        return $this;
    }

    /**
     * @return Collection<int,  Block>
     *
     * @psalm-mutation-free
     */
    public function getBlocks(): Collection
    {
        return $this->blocks;
    }

    /**
     * @param iterable<int,  Block> $blocks
     */
    public function setBlocks(iterable $blocks): self
    {
        ArrayCollectionUtils::update($this->blocks, $blocks, function (Block $block) {
            $block->setDevice($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addBlock(Block $block): self
    {
        if (!$this->blocks->contains($block)) {
            $block->setDevice($this);
            $this->blocks->add($block);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeBlock(Block $block): self
    {
        if ($this->blocks->contains($block)) {
            $this->blocks->removeElement($block);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(Protocol $protocol): self
    {
        $this->protocol = $protocol;
        $protocol->setDevice($this);

        return $this;
    }

    /**
     * @return Collection<int,  OutExperimentationZone>
     *
     * @psalm-mutation-free
     */
    public function getOutExperimentationZones(): Collection
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param iterable<int,  OutExperimentationZone> $outExperimentationZones
     */
    public function setOutExperimentationZones(iterable $outExperimentationZones): self
    {
        ArrayCollectionUtils::update($this->outExperimentationZones, $outExperimentationZones, function (OutExperimentationZone $outExperimentationZone) {
            $outExperimentationZone->setDevice($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if (!$this->outExperimentationZones->contains($outExperimentationZone)) {
            $outExperimentationZone->setDevice($this);
            $this->outExperimentationZones->add($outExperimentationZone);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if ($this->outExperimentationZones->contains($outExperimentationZone)) {
            $outExperimentationZone->setDevice(null);
            $this->outExperimentationZones->removeElement($outExperimentationZone);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValidationDate(): ?\DateTime
    {
        return $this->validationDate;
    }

    /**
     * @return $this
     */
    public function setValidationDate(?\DateTime $validationDate): self
    {
        $this->validationDate = $validationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function parent(): ?BusinessObject
    {
        return null;
    }
}
