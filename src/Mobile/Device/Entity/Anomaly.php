<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\AnomalyRepository")
 *
 * @ORM\Table(name="anomaly", schema="adonis")
 *
 * @psalm-type AnomalyType = self::TYPE_*
 */
class Anomaly extends IdentifiedEntity
{
    public const TYPE_DEAD = 0;
    public const TYPE_BAD_TREATMENT = 1;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $description = '';

    /**
     * @var AnomalyType
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $type = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Treatment")
     */
    private ?Treatment $constatedTreatment = null;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Device\Entity\BusinessObject", mappedBy="anomaly")
     */
    private BusinessObject $businessObject;

    /**
     * @psalm-mutation-free
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return $this
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return AnomalyType
     *
     * @psalm-mutation-free
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param AnomalyType $type
     *
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getConstatedTreatment(): ?Treatment
    {
        return $this->constatedTreatment;
    }

    /**
     * @return $this
     */
    public function setConstatedTreatment(?Treatment $constatedTreatment): self
    {
        $this->constatedTreatment = $constatedTreatment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBusinessObject(): BusinessObject
    {
        return $this->businessObject;
    }

    /**
     * @return $this
     */
    public function setBusinessObject(BusinessObject $businessObject): self
    {
        $this->businessObject = $businessObject;

        return $this;
    }
}
