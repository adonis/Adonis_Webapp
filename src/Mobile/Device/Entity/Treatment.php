<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\TreatmentRepository")
 *
 * @ORM\Table(name="treatment", schema="adonis")
 */
class Treatment extends IdentifiedEntity
{
    public const DEFAULT_SHORT_NAME_FROM_CSV = '';
    public const DEFAULT_REPETITIONS_FROM_CSV = 1;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private string $shortName = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $repetitions = 0;

    /**
     * @var Collection<int, Modality>
     *
     * @ORM\ManyToMany(targetEntity="Mobile\Device\Entity\Modality", cascade={"persist", "remove", "detach"})
     *
     * @ORM\JoinTable(name="rel_treatment_modality", schema="adonis")
     */
    private Collection $modalities;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Protocol", inversedBy="treatments")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Protocol $protocol;

    /**
     * @param non-empty-string               $name
     * @param non-empty-string               $shortname
     * @param non-empty-array<int, Modality> $modalities
     */
    public static function build(string $name, string $shortname, array $modalities): self
    {
        $entity = new self();
        $entity->setName($name);
        $entity->setShortName($shortname);
        $entity->setModalities(new ArrayCollection($modalities));

        return $entity;
    }

    public function __construct()
    {
        $this->modalities = new ArrayCollection();
    }

    public function getModalitiesValue(): array
    {
        $modalitiesValue = [];
        foreach ($this->getModalities() as $modality) {
            $modalitiesValue[] = $modality->getValue();
        }

        return $modalitiesValue;
    }

    /**
     * @return Collection<int, Modality>
     *
     * @psalm-mutation-free
     */
    public function getModalities(): Collection
    {
        return $this->modalities;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return $this
     */
    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @return $this
     */
    public function setRepetitions(int $repetitions): self
    {
        $this->repetitions = $repetitions;

        return $this;
    }

    /**
     * @param Collection<int, Modality> $modalities
     */
    public function setModalities(Collection $modalities): void
    {
        ArrayCollectionUtils::update($this->modalities, $modalities);
    }

    /**
     * @return $this
     */
    public function addModality(Modality $modality): self
    {
        if (!$this->modalities->contains($modality)) {
            $this->modalities->add($modality);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeModality(Modality $modality): self
    {
        if ($this->modalities->contains($modality)) {
            $this->modalities->removeElement($modality);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(Protocol $protocol): self
    {
        $this->protocol = $protocol;

        return $this;
    }
}
