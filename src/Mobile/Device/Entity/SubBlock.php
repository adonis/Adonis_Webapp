<?php

namespace Mobile\Device\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="Mobile\Device\Repository\SubBlockRepository")
 *
 * @ORM\Table(name="sub_block", schema="adonis")
 */
class SubBlock extends BusinessObject
{
    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\ManyToOne(targetEntity="Mobile\Device\Entity\Block", inversedBy="subBlocks")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private Block $block;

    /**
     * @var Collection<int, UnitParcel>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\UnitParcel", mappedBy="subBlock", cascade={"persist", "remove", "detach"})
     */
    private Collection $unitParcels;

    /**
     * @var Collection<int, OutExperimentationZone>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\OutExperimentationZone", mappedBy="subBlock", cascade={"persist", "remove", "detach"})
     */
    private Collection $outExperimentationZones;

    public function __construct()
    {
        parent::__construct();
        $this->unitParcels = new ArrayCollection();
        $this->outExperimentationZones = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlock(): Block
    {
        return $this->block;
    }

    /**
     * @return $this
     */
    public function setBlock(Block $block): self
    {
        $this->block = $block;

        return $this;
    }

    /**
     * @return Collection<int,  UnitParcel>
     *
     * @psalm-mutation-free
     */
    public function getUnitParcels(): Collection
    {
        return $this->unitParcels;
    }

    /**
     * @param iterable<int, UnitParcel> $unitParcels
     */
    public function setUnitParcels(iterable $unitParcels): self
    {
        ArrayCollectionUtils::update($this->unitParcels, $unitParcels, function (UnitParcel $unitParcel) {
            $unitParcel->setBlock(null);
            $unitParcel->setSubBlock($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addUnitParcel(UnitParcel $unitParcel): self
    {
        if (!$this->unitParcels->contains($unitParcel)) {
            $unitParcel->setBlock(null);
            $unitParcel->setSubBlock($this);
            $this->unitParcels->add($unitParcel);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeUnitParcel(UnitParcel $unitParcel): self
    {
        if ($this->unitParcels->contains($unitParcel)) {
            $unitParcel->setSubBlock(null);
            $this->unitParcels->removeElement($unitParcel);
        }

        return $this;
    }

    /**
     * @return Collection<int,  OutExperimentationZone>
     *
     * @psalm-mutation-free
     */
    public function getOutExperimentationZones(): Collection
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param iterable<int, OutExperimentationZone> $outExperimentationZones
     */
    public function setOutExperimentationZones(iterable $outExperimentationZones): self
    {
        ArrayCollectionUtils::update($this->outExperimentationZones, $outExperimentationZones, function (OutExperimentationZone $outExperimentationZone) {
            $outExperimentationZone->setSubBlock($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if (!$this->outExperimentationZones->contains($outExperimentationZone)) {
            $outExperimentationZone->setSubBlock($this);
            $this->outExperimentationZones->add($outExperimentationZone);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if ($this->outExperimentationZones->contains($outExperimentationZone)) {
            $outExperimentationZone->setSubBlock(null);
            $this->outExperimentationZones->removeElement($outExperimentationZone);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function parent(): ?BusinessObject
    {
        return $this->getBlock();
    }
}
