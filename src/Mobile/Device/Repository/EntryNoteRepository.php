<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\Device;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\SubBlock;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Project\Entity\Platform;

/**
 * Class EntryNoteRepository.
 *
 * @method EntryNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method EntryNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method EntryNote[] findAll()
 * @method EntryNote[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntryNoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EntryNote::class);
    }

    public function findPlatformNotes(Platform $platform){

        $sqb = $this->createQueryBuilder('n');

        $experimentQb = $this->constructExperimentQb("exp");

        $blockQb = $this->constructBlockQb("blk");

        $subBlockQb = $this->constructSubBlockQb("sbk");

        $unitPlotQb = $this->constructUnitPlotQb("upl");

        $individualQb = $this->constructIndividualQb("ind");

        $sqb->andWhere($sqb->expr()->orX(
            $sqb->expr()->in('n.businessObject', $experimentQb->getDQL()),
            $sqb->expr()->in('n.businessObject', $blockQb->getDQL()),
            $sqb->expr()->in('n.businessObject', $subBlockQb->getDQL()),
            $sqb->expr()->in('n.businessObject', $unitPlotQb->getDQL()),
            $sqb->expr()->in('n.businessObject', $individualQb->getDQL()),
        ) )
            ->setParameter("platform", $platform);

        return $sqb->getQuery()->getResult();
    }

    private function constructExperimentQb(string $alias){
        $experimentQb = $this->getEntityManager()->getRepository(Device::class)->createQueryBuilder($alias);
        $experimentQb->andWhere($experimentQb->expr()->eq($alias.".platform", ":platform"));
        return $experimentQb;
    }

    private function constructBlockQb(string $alias){
        $blockQb = $this->getEntityManager()->getRepository(Block::class)->createQueryBuilder($alias);
        $blockQb->andWhere($blockQb->expr()->in($alias.".device", $this->constructExperimentQb("exp_".$alias)->getDQL()));
        return $blockQb;
    }

    private function constructSubBlockQb(string $alias){
        $subBlockQb = $this->getEntityManager()->getRepository(SubBlock::class)->createQueryBuilder($alias);
        $subBlockQb->andWhere($subBlockQb->expr()->in($alias.".block", $this->constructBlockQb("blk_".$alias)->getDQL()));
        return $subBlockQb;
    }

    private function constructUnitPlotQb(string $alias){
        $unitPlotQb = $this->getEntityManager()->getRepository(UnitParcel::class)->createQueryBuilder($alias);
        $unitPlotQb->andWhere($unitPlotQb->expr()->orX(
            $unitPlotQb->expr()->in($alias.".block", $this->constructBlockQb("blk_".$alias)->getDQL()),
            $unitPlotQb->expr()->in($alias.".subBlock", $this->constructSubBlockQb("sbk_".$alias)->getDQL()),
        ) );
        return $unitPlotQb;
    }

    private function constructIndividualQb(string $alias){
        $individualQb = $this->getEntityManager()->getRepository(Individual::class)->createQueryBuilder($alias);
        $individualQb->andWhere($individualQb->expr()->in($alias.".unitParcel", $this->constructUnitPlotQb("up_".$alias)->getDQL()));
        return $individualQb;
    }
}
