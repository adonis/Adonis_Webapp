<?php

declare(strict_types=1);

namespace Mobile\Device\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Device\Entity\SubBlock;

/**
 * Class SubBlocRepository.
 *
 * @method SubBlock|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubBlock|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubBlock[] findAll()
 * @method SubBlock[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubBlockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SubBlock::class);
    }
}
