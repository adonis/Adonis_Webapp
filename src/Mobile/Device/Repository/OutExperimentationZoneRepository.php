<?php

namespace Mobile\Device\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Device\Entity\OutExperimentationZone;

/**
 * Class OutExperimentationZoneRepositoryRepository.
 *
 * @method OutExperimentationZone|null find($id, $lockMode = null, $lockVersion = null)
 * @method OutExperimentationZone|null findOneBy(array $criteria, array $orderBy = null)
 * @method OutExperimentationZone[] findAll()
 * @method OutExperimentationZone[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OutExperimentationZoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OutExperimentationZone::class);
    }
}
