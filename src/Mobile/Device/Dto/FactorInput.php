<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

/**
 * Class FactorInput.
 */
class FactorInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var ModalityInput[]
     */
    private $modalities;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FactorInput
     */
    public function setName(string $name): FactorInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ModalityInput[]
     */
    public function getModalities(): array
    {
        return $this->modalities;
    }

    /**
     * @param ModalityInput[] $modalities
     * @return FactorInput
     */
    public function setModalities(array $modalities): FactorInput
    {
        $this->modalities = $modalities;
        return $this;
    }
}
