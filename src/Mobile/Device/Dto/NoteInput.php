<?php

namespace Mobile\Device\Dto;

use DateTime;

/**
 * Class NoteInput
 */
class NoteInput
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var bool
     */
    private $deleted;

    /**
     * @var DateTime
     */
    private $creationDate;

    /**
     * @var string
     */
    private $creatorUri;

    /**
     * @var string
     */
    private $creatorName;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return NoteInput
     */
    public function setText(string $text): NoteInput
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return NoteInput
     */
    public function setDeleted(bool $deleted): NoteInput
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     * @return NoteInput
     */
    public function setCreationDate(DateTime $creationDate): NoteInput
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatorUri(): string
    {
        return $this->creatorUri;
    }

    /**
     * @param string $creatorUri
     * @return NoteInput
     */
    public function setCreatorUri(string $creatorUri): NoteInput
    {
        $this->creatorUri = $creatorUri;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatorName(): string
    {
        return $this->creatorName;
    }

    /**
     * @param string $creatorName
     * @return NoteInput
     */
    public function setCreatorName(string $creatorName): NoteInput
    {
        $this->creatorName = $creatorName;
        return $this;
    }
}
