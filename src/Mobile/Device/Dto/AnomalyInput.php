<?php


namespace Mobile\Device\Dto;


class AnomalyInput
{
    /**
     * @var string
     */
    public $description;
    /**
     * @var int
     */
    public $type;
    /**
     * @var string|null
     */
    public $constatedTreatmentName;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return AnomalyInput
     */
    public function setDescription(string $description): AnomalyInput
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return AnomalyInput
     */
    public function setType(int $type): AnomalyInput
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getConstatedTreatmentName(): ?string
    {
        return $this->constatedTreatmentName;
    }

    /**
     * @param string|null $constatedTreatmentName
     * @return AnomalyInput
     */
    public function setConstatedTreatmentName(?string $constatedTreatmentName): AnomalyInput
    {
        $this->constatedTreatmentName = $constatedTreatmentName;
        return $this;
    }
}