<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

/**
 * Class SubBlockInput.
 */
class SubBlockInput extends EvaluableInput
{
    /**
     * @var UnitParcelInput[]
     */
    private $unitParcels;

    /**
     * @var OutExperimentationZoneInput[]
     */
    private $outExperimentationZones;

    /**
     * @return UnitParcelInput[]
     */
    public function getUnitParcels(): array
    {
        return $this->unitParcels;
    }

    /**
     * @param UnitParcelInput[] $unitParcels
     */
    public function setUnitParcels(array $unitParcels): SubBlockInput
    {
        $this->unitParcels = $unitParcels;
        return $this;
    }

    /**
     * @return OutExperimentationZoneInput[]
     */
    public function getOutExperimentationZones(): array
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param OutExperimentationZoneInput[] $outExperimentationZones
     * @return SubBlockInput
     */
    public function setOutExperimentationZones(array $outExperimentationZones): SubBlockInput
    {
        $this->outExperimentationZones = $outExperimentationZones;
        return $this;
    }
}
