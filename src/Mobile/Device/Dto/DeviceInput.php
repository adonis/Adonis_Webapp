<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

use DateTime;

/**
 * Class DeviceInput.
 */
class DeviceInput extends EvaluableInput
{
    /**
     * @var bool
     */
    private $individualPU;

    /**
     * @var BlockInput[]
     */
    private $blocks;

    /**
     * @var ProtocolInput
     */
    private $protocol;

    /**
     * @var DateTime
     */
    private $creationDate;

    /**
     * @var DateTime|null
     */
    private $validationDate;

    /**
     * @var OutExperimentationZoneInput[]
     */
    private $outExperimentationZones;

    /**
     * @return bool
     */
    public function isIndividualPU(): bool
    {
        return $this->individualPU;
    }

    /**
     * @param bool $individualPU
     * @return DeviceInput
     */
    public function setIndividualPU(bool $individualPU): DeviceInput
    {
        $this->individualPU = $individualPU;
        return $this;
    }

    /**
     * @return BlockInput[]
     */
    public function getBlocks(): array
    {
        return $this->blocks;
    }

    /**
     * @param BlockInput[] $blocks
     * @return DeviceInput
     */
    public function setBlocks(array $blocks): DeviceInput
    {
        $this->blocks = $blocks;
        return $this;
    }

    /**
     * @return ProtocolInput
     */
    public function getProtocol(): ProtocolInput
    {
        return $this->protocol;
    }

    /**
     * @param ProtocolInput $protocol
     * @return DeviceInput
     */
    public function setProtocol(ProtocolInput $protocol): DeviceInput
    {
        $this->protocol = $protocol;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     * @return DeviceInput
     */
    public function setCreationDate(DateTime $creationDate): DeviceInput
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getValidationDate(): ?DateTime
    {
        return $this->validationDate;
    }

    /**
     * @param DateTime|null $validationDate
     * @return DeviceInput
     */
    public function setValidationDate(?DateTime $validationDate): DeviceInput
    {
        $this->validationDate = $validationDate;
        return $this;
    }

    /**
     * @return OutExperimentationZoneInput[]
     */
    public function getOutExperimentationZones(): array
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param OutExperimentationZoneInput[] $outExperimentationZones
     * @return DeviceInput
     */
    public function setOutExperimentationZones(array $outExperimentationZones): DeviceInput
    {
        $this->outExperimentationZones = $outExperimentationZones;
        return $this;
    }
}
