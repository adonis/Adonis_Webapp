<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Dto;

abstract class PositionableInput extends EvaluableInput
{
    private int $x = 0;

    private int $y = 0;

    private ?int $stateCode = null;

    private ?string $ident = null;

    private ?AnomalyInput $anomaly = null;

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStateCode(): ?int
    {
        return $this->stateCode;
    }

    /**
     * @return $this
     */
    public function setStateCode(?int $stateCode): self
    {
        $this->stateCode = $stateCode;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIdent(): ?string
    {
        return $this->ident;
    }

    /**
     * @return $this
     */
    public function setIdent(?string $ident): self
    {
        $this->ident = $ident;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAnomaly(): ?AnomalyInput
    {
        return $this->anomaly;
    }

    public function setAnomaly(?AnomalyInput $anomaly): self
    {
        $this->anomaly = $anomaly;

        return $this;
    }
}
