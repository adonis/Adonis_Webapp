<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

/**
 * Class BlockInput.
 */
class BlockInput extends EvaluableInput
{
    /**
     * @var SubBlockInput[]
     */
    private $subBlocks;

    /**
     * @var UnitParcelInput[]
     */
    private $unitParcels;

    /**
     * @var OutExperimentationZoneInput[]
     */
    private $outExperimentationZones;

    /**
     * @return SubBlockInput[]
     */
    public function getSubBlocks(): array
    {
        return $this->subBlocks;
    }

    /**
     * @param SubBlockInput[] $subBlocks
     * @return BlockInput
     */
    public function setSubBlocks(array $subBlocks): BlockInput
    {
        $this->subBlocks = $subBlocks;
        return $this;
    }

    /**
     * @return UnitParcelInput[]
     */
    public function getUnitParcels(): array
    {
        return $this->unitParcels;
    }

    /**
     * @param UnitParcelInput[] $unitParcels
     * @return BlockInput
     */
    public function setUnitParcels(array $unitParcels): BlockInput
    {
        $this->unitParcels = $unitParcels;
        return $this;
    }

    /**
     * @return OutExperimentationZoneInput[]
     */
    public function getOutExperimentationZones(): array
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param OutExperimentationZoneInput[] $outExperimentationZones
     * @return BlockInput
     */
    public function setOutExperimentationZones(array $outExperimentationZones): BlockInput
    {
        $this->outExperimentationZones = $outExperimentationZones;
        return $this;
    }

}
