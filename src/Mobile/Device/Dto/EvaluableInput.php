<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Dto;

use Mobile\Shared\Dto\HasAnnotationInput;

abstract class EvaluableInput extends HasAnnotationInput
{
    private string $uri = '';

    private string $name = '';

    /**
     * @var NoteInput[]
     */
    private array $notes = [];

    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return $this
     */
    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return NoteInput[]
     */
    public function getNotes(): array
    {
        return $this->notes;
    }

    /**
     * @param NoteInput[] $notes
     *
     * @return $this
     */
    public function setNotes(array $notes): self
    {
        $this->notes = $notes;

        return $this;
    }
}
