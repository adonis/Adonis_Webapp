<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

use DateTime;

/**
 * Class IndividualInput.
 */
class IndividualInput extends PositionableInput
{
    /**
     * @var DateTime
     */
    private $aparitionDate;
    /**
     * @var DateTime|null
     */
    private $demiseDate;

    /**
     * @return DateTime
     */
    public function getAparitionDate(): DateTime
    {
        return $this->aparitionDate;
    }

    /**
     * @param DateTime $aparitionDate
     * @return IndividualInput
     */
    public function setAparitionDate(DateTime $aparitionDate): IndividualInput
    {
        $this->aparitionDate = $aparitionDate;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDemiseDate(): ?DateTime
    {
        return $this->demiseDate;
    }

    /**
     * @param DateTime|null $demiseDate
     * @return IndividualInput
     */
    public function setDemiseDate(?DateTime $demiseDate): IndividualInput
    {
        $this->demiseDate = $demiseDate;
        return $this;
    }

}
