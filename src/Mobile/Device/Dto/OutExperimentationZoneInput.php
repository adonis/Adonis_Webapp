<?php

namespace Mobile\Device\Dto;

/**
 * Class OutExperimentationZoneInput
 * @package Mobile\Device\Dto
 */
class OutExperimentationZoneInput
{
    /**
     * @var string
     */
    private $uri;
    /**
     * @var int
     */
    private $x;

    /**
     * @var int
     */
    private $y;

    /**
     * @var string
     */
    private $num;

    /**
     * @var string
     */
    private $natureZHEUri;

    /**
     * @return int
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @param int $x
     * @return OutExperimentationZoneInput
     */
    public function setX(int $x): OutExperimentationZoneInput
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return int
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @param int $y
     * @return OutExperimentationZoneInput
     */
    public function setY(int $y): OutExperimentationZoneInput
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return string
     */
    public function getNum(): string
    {
        return $this->num;
    }

    /**
     * @param string $num
     * @return OutExperimentationZoneInput
     */
    public function setNum(string $num): OutExperimentationZoneInput
    {
        $this->num = $num;
        return $this;
    }

    /**
     * @return string
     */
    public function getNatureZHEUri(): string
    {
        return $this->natureZHEUri;
    }

    /**
     * @param string $natureZHEUri
     * @return OutExperimentationZoneInput
     */
    public function setNatureZHEUri(string $natureZHEUri): OutExperimentationZoneInput
    {
        $this->natureZHEUri = $natureZHEUri;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return OutExperimentationZoneInput
     */
    public function setUri(string $uri): OutExperimentationZoneInput
    {
        $this->uri = $uri;
        return $this;
    }

}
