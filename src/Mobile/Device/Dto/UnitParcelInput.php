<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Device\Dto;

class UnitParcelInput extends PositionableInput
{
    /**
     * @var IndividualInput[]
     */
    private array $individuals = [];

    private ?string $treatmentName = null;

    /**
     * @var OutExperimentationZoneInput[]
     */
    private array $outExperimentationZones = [];

    private ?\DateTime $aparitionDate = null;

    private ?\DateTime $demiseDate = null;

    /**
     * @return IndividualInput[]
     */
    public function getIndividuals(): array
    {
        return $this->individuals;
    }

    /**
     * @param IndividualInput[] $individuals
     */
    public function setIndividuals(array $individuals): self
    {
        $this->individuals = $individuals;

        return $this;
    }

    public function getTreatmentName(): ?string
    {
        return $this->treatmentName;
    }

    public function setTreatmentName(?string $treatmentName): self
    {
        $this->treatmentName = $treatmentName;

        return $this;
    }

    /**
     * @return OutExperimentationZoneInput[]
     */
    public function getOutExperimentationZones(): array
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param OutExperimentationZoneInput[] $outExperimentationZones
     */
    public function setOutExperimentationZones(array $outExperimentationZones): self
    {
        $this->outExperimentationZones = $outExperimentationZones;

        return $this;
    }

    public function getAparitionDate(): ?\DateTime
    {
        return $this->aparitionDate;
    }

    public function setAparitionDate(?\DateTime $aparitionDate): self
    {
        $this->aparitionDate = $aparitionDate;

        return $this;
    }

    public function getDemiseDate(): ?\DateTime
    {
        return $this->demiseDate;
    }

    public function setDemiseDate(?\DateTime $demiseDate): self
    {
        $this->demiseDate = $demiseDate;

        return $this;
    }
}
