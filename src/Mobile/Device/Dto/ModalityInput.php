<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

/**
 * Class ModalityInput.
 */
class ModalityInput
{
    /**
     * @var string
     */
    private $value;

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ModalityInput
     */
    public function setValue(string $value): ModalityInput
    {
        $this->value = $value;
        return $this;
    }
}
