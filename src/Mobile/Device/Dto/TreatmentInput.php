<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

/**
 * Class TreatmentInput.
 */
class TreatmentInput
{
    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $shortName;

    /**
     * @var int
     */
    private $repetitions;

    /**
     * @var string[]|null
     */
    private $modalitiesValue;

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return TreatmentInput
     */
    public function setUri(string $uri): TreatmentInput
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TreatmentInput
     */
    public function setName(string $name): TreatmentInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return TreatmentInput
     */
    public function setShortName(string $shortName): TreatmentInput
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return int
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @param int $repetitions
     * @return TreatmentInput
     */
    public function setRepetitions(int $repetitions): TreatmentInput
    {
        $this->repetitions = $repetitions;
        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getModalitiesValue(): ?array
    {
        return $this->modalitiesValue;
    }

    /**
     * @param string[]|null $modalitiesValue
     * @return TreatmentInput
     */
    public function setModalitiesValue(?array $modalitiesValue): TreatmentInput
    {
        $this->modalitiesValue = $modalitiesValue;
        return $this;
    }
}
