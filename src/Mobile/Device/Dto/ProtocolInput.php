<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Device\Dto;

use DateTime;

/**
 * Class ProtocolInput.
 */
class ProtocolInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $aim;

    /**
     * @var string
     */
    private $algorithm;

    /**
     * @var FactorInput[]
     */
    private $factors;

    /**
     * @var TreatmentInput[]
     */
    private $treatments;

    /**
     * @var DateTime
     */
    private $creationdate;

    /**
     * @var string|null
     */
    private $creatorLogin;

    /**
     * @var int|null
     */
    private $nbIndividualsPerParcel;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProtocolInput
     */
    public function setName(string $name): ProtocolInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAim(): string
    {
        return $this->aim;
    }

    /**
     * @param string $aim
     * @return ProtocolInput
     */
    public function setAim(string $aim): ProtocolInput
    {
        $this->aim = $aim;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param string $algorithm
     * @return ProtocolInput
     */
    public function setAlgorithm(string $algorithm): ProtocolInput
    {
        $this->algorithm = $algorithm;
        return $this;
    }

    /**
     * @return FactorInput[]
     */
    public function getFactors(): array
    {
        return $this->factors;
    }

    /**
     * @param FactorInput[] $factors
     * @return ProtocolInput
     */
    public function setFactors(array $factors): ProtocolInput
    {
        $this->factors = $factors;
        return $this;
    }

    /**
     * @return TreatmentInput[]
     */
    public function getTreatments(): array
    {
        return $this->treatments;
    }

    /**
     * @param TreatmentInput[] $treatments
     * @return ProtocolInput
     */
    public function setTreatments(array $treatments): ProtocolInput
    {
        $this->treatments = $treatments;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationdate(): DateTime
    {
        return $this->creationdate;
    }

    /**
     * @param DateTime $creationdate
     * @return ProtocolInput
     */
    public function setCreationdate(DateTime $creationdate): ProtocolInput
    {
        $this->creationdate = $creationdate;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getNbIndividualsPerParcel(): ?int
    {
        return $this->nbIndividualsPerParcel;
    }

    /**
     * @param int|null $nbIndividualsPerParcel
     * @return ProtocolInput
     */
    public function setNbIndividualsPerParcel(?int $nbIndividualsPerParcel): ProtocolInput
    {
        $this->nbIndividualsPerParcel = $nbIndividualsPerParcel;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatorLogin(): ?string
    {
        return $this->creatorLogin;
    }

    /**
     * @param string|null $creatorLogin
     * @return ProtocolInput
     */
    public function setCreatorLogin(?string $creatorLogin): ProtocolInput
    {
        $this->creatorLogin = $creatorLogin;
        return $this;
    }
}
