<?php

declare(strict_types=1);

namespace Mobile\Project\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Project\Entity\Workpath;

/**
 * Class WorkpathRepository.
 *
 * @method Workpath|null find($id, $lockMode = null, $lockVersion = null)
 * @method Workpath|null findOneBy(array $criteria, array $orderBy = null)
 * @method Workpath[] findAll()
 * @method Workpath[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkpathRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Workpath::class);
    }
}
