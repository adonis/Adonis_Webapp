<?php

namespace Mobile\Project\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Project\Entity\NatureZHE;

/**
 * Class NatureZHERepositoryRepository.
 *
 * @method NatureZHE|null find($id, $lockMode = null, $lockVersion = null)
 * @method NatureZHE|null findOneBy(array $criteria, array $orderBy = null)
 * @method NatureZHE[] findAll()
 * @method NatureZHE[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NatureZHERepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, NatureZHE::class);
    }
}
