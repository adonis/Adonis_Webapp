<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Project\Entity\DesktopUser;

/**
 * Class DesktopUserRepository.
 *
 * @method DesktopUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method DesktopUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method DesktopUser[] findAll()
 * @method DesktopUser[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DesktopUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DesktopUser::class);
    }
}
