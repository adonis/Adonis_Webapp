<?php

declare(strict_types=1);

namespace Mobile\Project\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Mobile\Project\Entity\DataEntryProject;

/**
 * Class .DataEntryProjectRepository.
 *
 * @method DataEntryProject|null find($id, $lockMode = null, $lockVersion = null)
 * @method DataEntryProject|null findOneBy(array $criteria, array $orderBy = null)
 * @method DataEntryProject[] findAll()
 * @method DataEntryProject[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DataEntryProjectRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DataEntryProject::class);
    }
}
