<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Project\Repository\DesktopUserRepository")
 *
 * @ORM\Table(name="desktop_user", schema="adonis")
 */
class DesktopUser extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="desktopUsers")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private DataEntryProject $project;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $firstname = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $email = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $login = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $password = '';

    /**
     * @param non-empty-string $name
     * @param non-empty-string $firstname
     * @param non-empty-string $email
     * @param non-empty-string $login
     */
    public static function build(string $name, string $firstname, string $email, string $login, string $password = ''): self
    {
        $entity = new self();
        $entity->setName($name);
        $entity->setFirstname($firstname);
        $entity->setEmail($email);
        $entity->setLogin($login);
        $entity->setPassword($password);

        return $entity;
    }

    /**
     * Utiliser {@see self::build}.
     *
     * @psalm-internal Mobile\Project\Entity
     */
    public function __construct()
    {
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @return $this
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return $this
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return $this
     */
    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
