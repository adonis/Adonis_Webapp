<?php

namespace Mobile\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Project\Repository\GraphicalStructureRepository")
 *
 * @ORM\Table(name="graphical_structure", schema="adonis")
 */
class GraphicalStructure extends IdentifiedEntity
{
    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="graphicalStructure")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private DataEntryProject $project;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $platformColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $deviceColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $blockColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $subBlockColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $individualUnitParcelColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $surfaceUnitParcelColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $abnormalUnitParcelColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $individualColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $abnormalIndividualColor = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $emptyColor = '';

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $tooltipActive = false;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $deviceLabel = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $blockLabel = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $subBlockLabel = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $individualUnitParcelLabel = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $surfaceUnitParcelLabel = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $individualLabel = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $origin = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $baseWidth = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $baseHeight = null;

    /**
     * @psalm-mutation-free
     */
    public function getProject(): DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatformColor(): string
    {
        return $this->platformColor;
    }

    /**
     * @return $this
     */
    public function setPlatformColor(string $platformColor): self
    {
        $this->platformColor = $platformColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDeviceColor(): string
    {
        return $this->deviceColor;
    }

    /**
     * @return $this
     */
    public function setDeviceColor(string $deviceColor): self
    {
        $this->deviceColor = $deviceColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlockColor(): string
    {
        return $this->blockColor;
    }

    /**
     * @return $this
     */
    public function setBlockColor(string $blockColor): self
    {
        $this->blockColor = $blockColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlockColor(): string
    {
        return $this->subBlockColor;
    }

    /**
     * @return $this
     */
    public function setSubBlockColor(string $subBlockColor): self
    {
        $this->subBlockColor = $subBlockColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualUnitParcelColor(): string
    {
        return $this->individualUnitParcelColor;
    }

    /**
     * @return $this
     */
    public function setIndividualUnitParcelColor(string $individualUnitParcelColor): self
    {
        $this->individualUnitParcelColor = $individualUnitParcelColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSurfaceUnitParcelColor(): string
    {
        return $this->surfaceUnitParcelColor;
    }

    /**
     * @return $this
     */
    public function setSurfaceUnitParcelColor(string $surfaceUnitParcelColor): self
    {
        $this->surfaceUnitParcelColor = $surfaceUnitParcelColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAbnormalUnitParcelColor(): string
    {
        return $this->abnormalUnitParcelColor;
    }

    /**
     * @return $this
     */
    public function setAbnormalUnitParcelColor(string $abnormalUnitParcelColor): self
    {
        $this->abnormalUnitParcelColor = $abnormalUnitParcelColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualColor(): string
    {
        return $this->individualColor;
    }

    /**
     * @return $this
     */
    public function setIndividualColor(string $individualColor): self
    {
        $this->individualColor = $individualColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAbnormalIndividualColor(): string
    {
        return $this->abnormalIndividualColor;
    }

    /**
     * @return $this
     */
    public function setAbnormalIndividualColor(string $abnormalIndividualColor): self
    {
        $this->abnormalIndividualColor = $abnormalIndividualColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getEmptyColor(): string
    {
        return $this->emptyColor;
    }

    /**
     * @return $this
     */
    public function setEmptyColor(string $emptyColor): self
    {
        $this->emptyColor = $emptyColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isTooltipActive(): bool
    {
        return $this->tooltipActive;
    }

    /**
     * @return $this
     */
    public function setTooltipActive(bool $tooltipActive): self
    {
        $this->tooltipActive = $tooltipActive;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDeviceLabel(): string
    {
        return $this->deviceLabel;
    }

    /**
     * @return $this
     */
    public function setDeviceLabel(string $deviceLabel): self
    {
        $this->deviceLabel = $deviceLabel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlockLabel(): string
    {
        return $this->blockLabel;
    }

    /**
     * @return $this
     */
    public function setBlockLabel(string $blockLabel): self
    {
        $this->blockLabel = $blockLabel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlockLabel(): string
    {
        return $this->subBlockLabel;
    }

    /**
     * @return $this
     */
    public function setSubBlockLabel(string $subBlockLabel): self
    {
        $this->subBlockLabel = $subBlockLabel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualUnitParcelLabel(): string
    {
        return $this->individualUnitParcelLabel;
    }

    /**
     * @return $this
     */
    public function setIndividualUnitParcelLabel(string $individualUnitParcelLabel): self
    {
        $this->individualUnitParcelLabel = $individualUnitParcelLabel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSurfaceUnitParcelLabel(): string
    {
        return $this->surfaceUnitParcelLabel;
    }

    /**
     * @return $this
     */
    public function setSurfaceUnitParcelLabel(string $surfaceUnitParcelLabel): self
    {
        $this->surfaceUnitParcelLabel = $surfaceUnitParcelLabel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualLabel(): string
    {
        return $this->individualLabel;
    }

    /**
     * @return $this
     */
    public function setIndividualLabel(string $individualLabel): self
    {
        $this->individualLabel = $individualLabel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @return $this
     */
    public function setOrigin(?string $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBaseWidth(): ?int
    {
        return $this->baseWidth;
    }

    /**
     * @return $this
     */
    public function setBaseWidth(?int $baseWidth): self
    {
        $this->baseWidth = $baseWidth;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBaseHeight(): ?int
    {
        return $this->baseHeight;
    }

    /**
     * @return $this
     */
    public function setBaseHeight(?int $baseHeight): self
    {
        $this->baseHeight = $baseHeight;

        return $this;
    }
}
