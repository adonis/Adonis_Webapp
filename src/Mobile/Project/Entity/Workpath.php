<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Project\Repository\WorkpathRepository")
 *
 * @ORM\Table(name="workpath", schema="adonis")
 */
class Workpath extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $path = '';

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="workpaths")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private DataEntryProject $dataEntryProject;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $username = '';

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $startEnd = false;

    /**
     * @param non-empty-string $path
     * @param non-empty-string $username
     */
    public static function build(string $path, string $username): self
    {
        $entity = new self();
        $entity->setPath($path);
        $entity->setUsername($username);

        return $entity;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return $this
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDataEntryProject(): DataEntryProject
    {
        return $this->dataEntryProject;
    }

    /**
     * @return $this
     */
    public function setDataEntryProject(DataEntryProject $dataEntryProject): self
    {
        $this->dataEntryProject = $dataEntryProject;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isStartEnd(): bool
    {
        return $this->startEnd;
    }

    /**
     * @return $this
     */
    public function setStartEnd(bool $startEnd): self
    {
        $this->startEnd = $startEnd;

        return $this;
    }
}
