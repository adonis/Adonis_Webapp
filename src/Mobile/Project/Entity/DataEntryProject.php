<?php

/*
 * @author TRYDEA - 2024
 */

namespace Mobile\Project\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Measure\Entity\Annotation;
use Mobile\Measure\Entity\DataEntry;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\GeneratorVariable;
use Mobile\Measure\Entity\Variable\Material;
use Mobile\Measure\Entity\Variable\RequiredAnnotation;
use Mobile\Measure\Entity\Variable\StateCode;
use Mobile\Measure\Entity\Variable\UniqueVariable;
use Mobile\Project\Dto\DataEntryProjectInput;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Entity\ResponseFile;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "post"={
 *              "security"="is_granted('ROLE_USER')",
 *              "input"=DataEntryProjectInput::class,
 *              "output"=false,
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *             "security"="is_granted('ROLE_USER')",
 *             "output"=DataEntryProjectInput::class,
 *          }
 *     }
 * )
 *
 * @ORM\Table(name="project", schema="adonis")
 *
 * @ORM\Entity(repositoryClass="Mobile\Project\Repository\DataEntryProjectRepository")
 */
class DataEntryProject extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     */
    protected string $name = '';

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\DesktopUser", cascade={"persist", "remove", "detach"})
     */
    protected ?DesktopUser $creator = null;

    /**
     * @var Collection<int, DesktopUser>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Project\Entity\DesktopUser", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected Collection $desktopUsers;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\Platform", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected Platform $platform;

    /**
     * @var Collection<int, StateCode>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\StateCode",mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected Collection $stateCodes;

    /**
     * @var Collection<int, UniqueVariable>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\UniqueVariable", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected Collection $uniqueVariables;

    /**
     * @var Collection<int, GeneratorVariable>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\GeneratorVariable", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected Collection $generatorVariables;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\GraphicalStructure", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected GraphicalStructure $graphicalStructure;

    /**
     * @var Collection<int, NatureZHE>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Project\Entity\NatureZHE", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected Collection $naturesZHE;

    /**
     * @Groups({"status_project_webapp_return_data"})
     *
     * @ORM\OneToOne(targetEntity="Mobile\Measure\Entity\DataEntry", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected ?DataEntry $dataEntry = null;

    /**
     * @ORM\OneToOne(targetEntity="Webapp\FileManagement\Entity\RequestFile", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected ?RequestFile $originFile = null;

    /**
     * @Groups({"user_linked_file_job_read"})
     *
     * @ORM\OneToOne(targetEntity="Webapp\FileManagement\Entity\ResponseFile", mappedBy="project", cascade={"persist", "remove", "detach"})
     */
    protected ?ResponseFile $responseFile = null;

    /**
     * @ORM\Column(type="datetime",nullable=false)
     */
    protected \DateTime $creationDate;

    /**
     * @var Collection<int, Material>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\Material", mappedBy="project", cascade={"persist","remove", "detach"})
     */
    protected Collection $materials;

    /**
     * @var Collection<int, Workpath>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Project\Entity\Workpath", mappedBy="dataEntryProject", cascade={"persist", "remove", "detach"})
     */
    private Collection $workpaths;

    /**
     * @var Collection<int, UniqueVariable>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\UniqueVariable", mappedBy="connectedDataEntryProject", cascade={"persist", "remove", "detach"})
     */
    private Collection $connectedUniqueVariables;

    /**
     * @var Collection<int, GeneratorVariable>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\GeneratorVariable", mappedBy="connectedDataEntryProject", cascade={"persist", "remove", "detach"})
     */
    private Collection $connectedGeneratorVariables;

    /**
     * @var Collection<int, RequiredAnnotation>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Variable\RequiredAnnotation",mappedBy="dataEntryProject", cascade={"persist", "remove", "detach"})
     */
    private Collection $requiredAnnotations;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $improvised = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $benchmark = false;

    public static function build(string $name, \DateTime $creationDate): self
    {
        $project = new self();
        $project->setName($name);
        $project->setCreationDate($creationDate);

        return $project;
    }

    /**
     * Constructeur interne, utiliser {@see self::build} à la place.
     *
     * @internal
     */
    public function __construct()
    {
        $this->desktopUsers = new ArrayCollection();
        $this->stateCodes = new ArrayCollection();
        $this->uniqueVariables = new ArrayCollection();
        $this->generatorVariables = new ArrayCollection();
        $this->naturesZHE = new ArrayCollection();
        $this->materials = new ArrayCollection();
        $this->workpaths = new ArrayCollection();
        $this->connectedUniqueVariables = new ArrayCollection();
        $this->connectedGeneratorVariables = new ArrayCollection();
        $this->requiredAnnotations = new ArrayCollection();
        $this->creationDate = new \DateTime();
    }

    /**
     * @ApiProperty(readable=false, writable=false)
     *
     * @return Annotation[]
     */
    public function getAnnotations(): array
    {
        $res = [];
        $parcelCallback = /** @param Collection<int, UnitParcel> $unitPlotList */ function (Collection $unitPlotList) use (&$res): void {
            foreach ($unitPlotList as $unitPlot) {
                $res = array_merge($res, $unitPlot->getAnnotations()->toArray());
                foreach ($unitPlot->getIndividuals() as $individual) {
                    $res = array_merge($res, $individual->getAnnotations()->toArray());
                }
            }
        };
        foreach ($this->getPlatform()->getDevices() as $device) {
            $res = array_merge($res, $device->getAnnotations()->toArray());
            foreach ($device->getBlocks() as $block) {
                $res = array_merge($res, $block->getAnnotations()->toArray());
                foreach ($block->getSubBlocks() as $subBlock) {
                    $res = array_merge($res, $subBlock->getAnnotations()->toArray());
                    $parcelCallback($subBlock->getUnitParcels());
                }
                $parcelCallback($block->getUnitParcels());
            }
        }

        return $res;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreator(): ?DesktopUser
    {
        return $this->creator;
    }

    /**
     * @return $this
     */
    public function setCreator(DesktopUser $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection<int,  DesktopUser>
     *
     * @psalm-mutation-free
     */
    public function getDesktopUsers(): Collection
    {
        return $this->desktopUsers;
    }

    /**
     * @param iterable<int,  DesktopUser> $desktopUsers
     *
     * @return $this
     */
    public function setDesktopUsers(iterable $desktopUsers): self
    {
        ArrayCollectionUtils::update($this->desktopUsers, $desktopUsers, function (DesktopUser $desktopUser) {
            $desktopUser->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addDesktopUser(DesktopUser $desktopUser): self
    {
        if (!$this->desktopUsers->contains($desktopUser)) {
            $desktopUser->setProject($this);
            $this->desktopUsers->add($desktopUser);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeDesktopUser(DesktopUser $desktopUser): self
    {
        if ($this->desktopUsers->contains($desktopUser)) {
            $this->desktopUsers->removeElement($desktopUser);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(Platform $platform): self
    {
        $this->platform = $platform;
        $platform->setProject($this);

        return $this;
    }

    /**
     * @return Collection<int,  StateCode>
     */
    public function getStateCodes(): Collection
    {
        return $this->stateCodes;
    }

    /**
     * @param iterable<int, StateCode> $stateCodes
     *
     * @return $this
     */
    public function setStateCodes(iterable $stateCodes): self
    {
        ArrayCollectionUtils::update($this->stateCodes, $stateCodes, function (StateCode $stateCode) {
            $stateCode->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addStateCode(StateCode $stateCode): self
    {
        if (!$this->stateCodes->contains($stateCode)) {
            $stateCode->setProject($this);
            $this->stateCodes->add($stateCode);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeStateCode(StateCode $stateCode): self
    {
        if ($this->stateCodes->contains($stateCode)) {
            $this->stateCodes->removeElement($stateCode);
        }

        return $this;
    }

    /**
     * @return Collection<int,  UniqueVariable>
     *
     * @psalm-mutation-free
     */
    public function getUniqueVariables(): Collection
    {
        return $this->uniqueVariables;
    }

    /**
     * @param iterable<array-key, UniqueVariable> $uniqueVariables
     *
     * @return $this
     */
    public function setUniqueVariables(iterable $uniqueVariables): self
    {
        ArrayCollectionUtils::update($this->uniqueVariables, $uniqueVariables, function (UniqueVariable $uniqueVariable) {
            $uniqueVariable->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addUniqueVariable(UniqueVariable $variable): self
    {
        if (!$this->uniqueVariables->contains($variable)) {
            $variable->setProject($this);
            $this->uniqueVariables->add($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeUniqueVariable(UniqueVariable $variable): self
    {
        if ($this->uniqueVariables->contains($variable)) {
            $this->uniqueVariables->removeElement($variable);
        }

        return $this;
    }

    /**
     * @return Collection<int,  GeneratorVariable>
     *
     * @psalm-mutation-free
     */
    public function getGeneratorVariables(): Collection
    {
        return $this->generatorVariables;
    }

    /**
     * @param iterable<array-key, GeneratorVariable> $generatorVariables
     *
     * @return $this
     */
    public function setGeneratorVariables(iterable $generatorVariables): self
    {
        ArrayCollectionUtils::update($this->generatorVariables, $generatorVariables, function (GeneratorVariable $generatorVariable) {
            $generatorVariable->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addGeneratorVariable(GeneratorVariable $variable): self
    {
        if (!$this->generatorVariables->contains($variable)) {
            $variable->setProject($this);
            $this->generatorVariables->add($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGeneratorVariable(GeneratorVariable $variable): self
    {
        if ($this->generatorVariables->contains($variable)) {
            $this->generatorVariables->removeElement($variable);
        }

        return $this;
    }

    /**
     * @param Variable[] $variables
     *
     * @return $this
     */
    public function setVariables(array $variables): self
    {
        $this->setUniqueVariables(array_filter($variables, fn (Variable $variable) => $variable instanceof UniqueVariable));
        $this->setGeneratorVariables(array_filter($variables, fn (Variable $variable) => $variable instanceof GeneratorVariable));

        return $this;
    }

    /**
     * @return $this
     */
    public function addVariable(Variable $variable): self
    {
        if ($variable instanceof UniqueVariable) {
            $this->addUniqueVariable($variable);
        } elseif ($variable instanceof GeneratorVariable) {
            $this->addGeneratorVariable($variable);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getGraphicalStructure(): GraphicalStructure
    {
        return $this->graphicalStructure;
    }

    /**
     * @return $this
     */
    public function setGraphicalStructure(GraphicalStructure $graphicalStructure): self
    {
        $graphicalStructure->setProject($this);
        $this->graphicalStructure = $graphicalStructure;

        return $this;
    }

    /**
     * @return Collection<int,  NatureZHE>
     *
     * @psalm-mutation-free
     */
    public function getNaturesZHE(): Collection
    {
        return $this->naturesZHE;
    }

    /**
     * @param iterable<int, NatureZHE> $naturesZHE
     *
     * @return $this
     */
    public function setNaturesZHE(iterable $naturesZHE): self
    {
        ArrayCollectionUtils::update($this->naturesZHE, $naturesZHE, function (NatureZHE $natureZHE) {
            $natureZHE->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addNatureZHE(NatureZHE $natureZhe): self
    {
        if (!$this->naturesZHE->contains($natureZhe)) {
            $natureZhe->setProject($this);
            $this->naturesZHE->add($natureZhe);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeNatureZHE(NatureZHE $natureZhe): self
    {
        if ($this->naturesZHE->contains($natureZhe)) {
            $natureZhe->setProject(null);
            $this->naturesZHE->removeElement($natureZhe);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDataEntry(): ?DataEntry
    {
        return $this->dataEntry;
    }

    /**
     * @return $this
     */
    public function setDataEntry(?DataEntry $dataEntry): self
    {
        $this->dataEntry = $dataEntry;
        $dataEntry->setProject($this);

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOriginFile(): ?RequestFile
    {
        return $this->originFile;
    }

    /**
     * @return $this
     */
    public function setOriginFile(?RequestFile $originFile): self
    {
        $this->originFile = $originFile;
        if (null !== $originFile) {
            $originFile->setProject($this);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResponseFile(): ?ResponseFile
    {
        return $this->responseFile;
    }

    /**
     * @return $this
     */
    public function setResponseFile(?ResponseFile $responseFile): self
    {
        $this->responseFile = $responseFile;
        if (null !== $responseFile) {
            $responseFile->setProject($this);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return Collection<int,  Material>
     *
     * @psalm-mutation-free
     */
    public function getMaterials(): Collection
    {
        return $this->materials;
    }

    /**
     * @param iterable<int, Material> $materials
     *
     * @return $this
     */
    public function setMaterials(iterable $materials): self
    {
        ArrayCollectionUtils::update($this->materials, $materials, function (Material $material) {
            $material->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addMaterial(Material $material): self
    {
        if (!$this->materials->contains($material)) {
            $material->setProject($this);
            $this->materials->add($material);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeMaterial(Material $material): self
    {
        if ($this->materials->contains($material)) {
            $this->materials->removeElement($material);
        }

        return $this;
    }

    /**
     * @return Collection<int,  Workpath>
     *
     * @psalm-mutation-free
     */
    public function getWorkpaths(): Collection
    {
        return $this->workpaths;
    }

    /**
     * @param iterable<int,  Workpath> $workpaths
     *
     * @return $this
     */
    public function setWorkpaths(iterable $workpaths): self
    {
        ArrayCollectionUtils::update($this->workpaths, $workpaths, function (Workpath $workpath) {
            $workpath->setDataEntryProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addWorkpath(Workpath $workpath): self
    {
        if (!$this->workpaths->contains($workpath)) {
            $workpath->setDataEntryProject($this);
            $this->workpaths->add($workpath);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeWorkpath(Workpath $workpath): self
    {
        if ($this->workpaths->contains($workpath)) {
            $this->workpaths->removeElement($workpath);
        }

        return $this;
    }

    /**
     * @return Collection<int, UniqueVariable>
     *
     * @psalm-mutation-free
     */
    public function getConnectedUniqueVariables(): Collection
    {
        return $this->connectedUniqueVariables;
    }

    /**
     * @param iterable<array-key, UniqueVariable> $connectedUniqueVariables
     *
     * @return $this
     */
    public function setConnectedUniqueVariables(iterable $connectedUniqueVariables): self
    {
        ArrayCollectionUtils::update($this->connectedUniqueVariables, $connectedUniqueVariables, function (UniqueVariable $uniqueVariable) {
            $uniqueVariable->setConnectedDataEntryProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addConnectedUniqueVariable(UniqueVariable $variable): self
    {
        if (!$this->connectedUniqueVariables->contains($variable)) {
            $variable->setConnectedDataEntryProject($this);
            $this->connectedUniqueVariables->add($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeConnectedUniqueVariable(UniqueVariable $variable): self
    {
        if ($this->connectedUniqueVariables->contains($variable)) {
            $variable->setConnectedDataEntryProject(null);
            $this->connectedUniqueVariables->removeElement($variable);
        }

        return $this;
    }

    /**
     * @return Collection<int,  GeneratorVariable>
     */
    public function getConnectedGeneratorVariables(): Collection
    {
        return $this->connectedGeneratorVariables;
    }

    /**
     * @param iterable<int, GeneratorVariable> $connectedGeneratorVariables
     *
     * @return $this
     */
    public function setConnectedGeneratorVariables(iterable $connectedGeneratorVariables): self
    {
        ArrayCollectionUtils::update($this->connectedGeneratorVariables, $connectedGeneratorVariables, function (GeneratorVariable $connectedGeneratorVariable) {
            $connectedGeneratorVariable->setConnectedDataEntryProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addConnectedGeneratorVariable(GeneratorVariable $variable): self
    {
        if (!$this->connectedGeneratorVariables->contains($variable)) {
            $variable->setConnectedDataEntryProject($this);
            $this->connectedGeneratorVariables->add($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeConnectedGeneratorVariable(GeneratorVariable $variable): self
    {
        if ($this->connectedGeneratorVariables->contains($variable)) {
            $variable->setConnectedDataEntryProject(null);
            $this->connectedGeneratorVariables->removeElement($variable);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function addConnectedVariable(Variable $variable): self
    {
        if ($variable instanceof UniqueVariable) {
            $this->addConnectedUniqueVariable($variable);
        } elseif ($variable instanceof GeneratorVariable) {
            $this->addConnectedGeneratorVariable($variable);
        }

        return $this;
    }

    /**
     * @return Collection<int,  RequiredAnnotation>
     *
     * @psalm-mutation-free
     */
    public function getRequiredAnnotations(): Collection
    {
        return $this->requiredAnnotations;
    }

    /**
     * @param Collection<int,  RequiredAnnotation> $requiredAnnotations
     *
     * @return $this
     */
    public function setRequiredAnnotations(Collection $requiredAnnotations): self
    {
        foreach ($requiredAnnotations as $requiredAnnotation) {
            $requiredAnnotation->setDataEntryProject($this);
        }
        $this->requiredAnnotations = $requiredAnnotations;

        return $this;
    }

    /**
     * @return $this
     */
    public function addRequiredAnnotation(RequiredAnnotation $requiredAnnotation): self
    {
        if (!$this->requiredAnnotations->contains($requiredAnnotation)) {
            $requiredAnnotation->setDataEntryProject($this);
            $this->requiredAnnotations->add($requiredAnnotation);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeRequiredAnnotation(RequiredAnnotation $requiredAnnotation): self
    {
        if ($this->requiredAnnotations->contains($requiredAnnotation)) {
            $this->requiredAnnotations->removeElement($requiredAnnotation);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isImprovised(): bool
    {
        return $this->improvised;
    }

    /**
     * @return $this
     */
    public function setImprovised(bool $improvised): self
    {
        $this->improvised = $improvised;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isBenchmark(): bool
    {
        return $this->benchmark;
    }

    /**
     * @return $this
     */
    public function setBenchmark(bool $benchmark): self
    {
        $this->benchmark = $benchmark;

        return $this;
    }
}
