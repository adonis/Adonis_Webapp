<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Measure\Entity\Annotation;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity()
 *
 * @ORM\Table("project_object", schema="adonis")
 *
 * @ORM\InheritanceType(value="JOINED")
 *
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 *
 * @ORM\DiscriminatorMap({
 *     "BusinessObject" = "Mobile\Device\Entity\BusinessObject",
 *     "Measure" = "Mobile\Measure\Entity\Measure"
 * })
 */
abstract class ProjectObject extends IdentifiedEntity
{
    /**
     * @var Collection<int, Annotation>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Measure\Entity\Annotation", mappedBy="projectObject", cascade={"persist", "remove", "detach"})
     */
    private Collection $annotations;

    public function __construct()
    {
        $this->annotations = new ArrayCollection();
    }

    /**
     * @return Collection<int, Annotation>
     *
     * @psalm-mutation-free
     */
    public function getAnnotations(): Collection
    {
        return $this->annotations;
    }

    /**
     * @param iterable<int, Annotation> $annotations
     *
     * @return $this
     */
    public function setAnnotations(iterable $annotations): self
    {
        ArrayCollectionUtils::update($this->annotations, $annotations, function (Annotation $annotation) {
            $annotation->setProjectObject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addAnnotation(Annotation $annotation): self
    {
        if (!$this->annotations->contains($annotation)) {
            $annotation->setProjectObject($this);
            $this->annotations->add($annotation);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAnnotation(Annotation $annotation): self
    {
        if ($this->annotations->contains($annotation)) {
            $this->annotations->removeElement($annotation);
        }

        return $this;
    }
}
