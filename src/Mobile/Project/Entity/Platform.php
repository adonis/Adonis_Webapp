<?php

namespace Mobile\Project\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Device\Entity\Device;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;

/**
 * @ORM\Entity(repositoryClass="Mobile\Project\Repository\PlatformRepository")
 *
 * @ORM\Table(name="platform", schema="adonis")
 */
class Platform extends IdentifiedEntity
{
    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="platform")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private DataEntryProject $project;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';

    /**
     * @var Collection<int, Device>
     *
     * @ORM\OneToMany(targetEntity="Mobile\Device\Entity\Device", mappedBy="platform", cascade={"persist", "remove", "detach"})
     */
    private Collection $devices;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $creationDate;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $site = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $place = '';

    /**
     * Platform constructor.
     */
    public function __construct()
    {
        $this->devices = new ArrayCollection();
        $this->creationDate = new \DateTime();
    }

    public function getProject(): DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int,  Device>
     *
     * @psalm-mutation-free
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    /**
     * @param iterable<int,  Device> $devices
     *
     * @return $this
     */
    public function setDevices(iterable $devices): self
    {
        ArrayCollectionUtils::update($this->devices, $devices, function (Device $device) {
            $device->setPlatform($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $device->setPlatform($this);
            $this->devices->add($device);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(string $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlace(): string
    {
        return $this->place;
    }

    /**
     * @return $this
     */
    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }
}
