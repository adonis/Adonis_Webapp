<?php

namespace Mobile\Project\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ORM\Entity(repositoryClass="Mobile\Project\Repository\NatureZHERepository")
 *
 * @ORM\Table(name="nature_zhe", schema="adonis")
 */
class NatureZHE extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name = '';
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $color = null;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $texture = null;

    /**
     * @ORM\ManyToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="naturesZHE")
     */
    private ?DataEntryProject $project = null;

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTexture(): ?string
    {
        return $this->texture;
    }

    /**
     * @return $this
     */
    public function setTexture(?string $texture): self
    {
        $this->texture = $texture;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): ?DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(?DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }
}
