<?php

namespace Mobile\Project\Persister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Mobile\Project\Entity\DataEntryProject;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\ProjectData;

class DataEntryPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof DataEntryProject &&
            isset($context['collection_operation_name']) &&
            strtolower($context['collection_operation_name']) === strtolower(Request::METHOD_POST);
    }

    /**
     * @param DataEntryProject $data
     * @param array $context
     * @return object|void
     */
    public function persist($data, array $context = [])
    {
        set_time_limit(300);
        //$this->entityManager->persist($data);
        $this->entityManager->flush();
    }

    /**
     * @param DataEntryProject $data
     * @param array $context
     * @throws InternalErrorException
     */
    public function remove($data, array $context = [])
    {
        throw new InternalErrorException();
    }
}
