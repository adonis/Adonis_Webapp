<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\ApiTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Mobile\Device\Dto\AnomalyInput;
use Mobile\Device\Dto\BlockInput;
use Mobile\Device\Dto\DeviceInput;
use Mobile\Device\Dto\FactorInput;
use Mobile\Device\Dto\IndividualInput;
use Mobile\Device\Dto\ModalityInput;
use Mobile\Device\Dto\NoteInput;
use Mobile\Device\Dto\OutExperimentationZoneInput;
use Mobile\Device\Dto\ProtocolInput;
use Mobile\Device\Dto\SubBlockInput;
use Mobile\Device\Dto\TreatmentInput;
use Mobile\Device\Dto\UnitParcelInput;
use Mobile\Device\Entity\Anomaly;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\BusinessObject;
use Mobile\Device\Entity\Device;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\Factor;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\Modality;
use Mobile\Device\Entity\OutExperimentationZone;
use Mobile\Device\Entity\Protocol;
use Mobile\Device\Entity\SubBlock;
use Mobile\Device\Entity\Treatment;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Measure\Dto\AnnotationInput;
use Mobile\Measure\Dto\DataEntryInput;
use Mobile\Measure\Dto\FormFieldInput;
use Mobile\Measure\Dto\GeneratedFieldInput;
use Mobile\Measure\Dto\MeasureInput;
use Mobile\Measure\Dto\SessionInput;
use Mobile\Measure\Dto\Variable\DriverInput;
use Mobile\Measure\Dto\Variable\GeneratorVariableInput;
use Mobile\Measure\Dto\Variable\MaterialInput;
use Mobile\Measure\Dto\Variable\StateCodeInput;
use Mobile\Measure\Dto\Variable\UniqueVariableInput;
use Mobile\Measure\Dto\Variable\ValueHintInput;
use Mobile\Measure\Dto\Variable\ValueHintListInput;
use Mobile\Measure\Dto\Variable\VariableInput;
use Mobile\Measure\Entity\Annotation;
use Mobile\Measure\Entity\DataEntry;
use Mobile\Measure\Entity\FormField;
use Mobile\Measure\Entity\GeneratedField;
use Mobile\Measure\Entity\Measure;
use Mobile\Measure\Entity\Session;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\Driver;
use Mobile\Measure\Entity\Variable\GeneratorVariable;
use Mobile\Measure\Entity\Variable\Material;
use Mobile\Measure\Entity\Variable\StateCode;
use Mobile\Measure\Entity\Variable\UniqueVariable;
use Mobile\Measure\Entity\Variable\ValueHint;
use Mobile\Measure\Entity\Variable\ValueHintList;
use Mobile\Project\Dto\DataEntryProjectInput;
use Mobile\Project\Dto\DesktopUserInput;
use Mobile\Project\Dto\GraphicalStructureInput;
use Mobile\Project\Dto\NatureZHEInput;
use Mobile\Project\Dto\PlatformInput;
use Mobile\Project\Dto\WorkpathInput;
use Mobile\Project\Entity\DataEntryProject;
use Mobile\Project\Entity\DesktopUser;
use Mobile\Project\Entity\GraphicalStructure;
use Mobile\Project\Entity\NatureZHE;
use Mobile\Project\Entity\Platform;
use Mobile\Project\Entity\ProjectObject;
use Mobile\Project\Entity\Workpath;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use RuntimeException;
use Shared\Authentication\Entity\User;
use Shared\TransferSync\Entity\StatusDataEntry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Webapp\Core\Enumeration\VariableTypeEnum;

/**
 * Class ProjectInputDataTransformer.
 * @psalm-import-type VariableTypeEnumId from VariableTypeEnum
 */
class DataEntryProjectInputDataTransformer implements DataTransformerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    private ManagerRegistry $managerRegistry;

    private TokenStorageInterface $tokenStorage;

    /**
     * @var ArrayCollection
     */
    private $variablesMap;

    /**
     * @var ArrayCollection
     */
    private $objectsMap;

    /**
     * @var ArrayCollection
     */
    private $desktopUsersMap;

    /**
     * @var ArrayCollection
     */
    private $stateCodesMap;

    /**
     * @var ArrayCollection
     */
    private $treatmentsMap;

    /**
     * @var ArrayCollection
     */
    private $modalitiesMap;

    /**
     * @var ArrayCollection
     */
    private $testsToHandleMap;

    /**
     * @var ArrayCollection
     */
    private $previousValuesToHandleMap;

    /**
     * @var ArrayCollection
     */
    private $materialToHandleMap;

    /**
     * @var ArrayCollection
     */
    private $natureZHEMap;

    /**
     * DataEntryProjectInputDataTransformer constructor.
     * @param ManagerRegistry $managerRegistry
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ManagerRegistry       $managerRegistry,
                                TokenStorageInterface $tokenStorage)
    {
        $this->managerRegistry = $managerRegistry;
        $this->tokenStorage = $tokenStorage;
        $this->variablesMap = new ArrayCollection();
        $this->objectsMap = new ArrayCollection();
        $this->desktopUsersMap = new ArrayCollection();
        $this->stateCodesMap = new ArrayCollection();
        $this->treatmentsMap = new ArrayCollection();
        $this->modalitiesMap = new ArrayCollection();
        $this->testsToHandleMap = new ArrayCollection();
        $this->previousValuesToHandleMap = new ArrayCollection();
        $this->materialToHandleMap = new ArrayCollection();
        $this->natureZHEMap = new ArrayCollection();
    }

    /**
     * @inheritDoc
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        return DataEntryProject::class === $to && null !== ($context['input']['class'] ?? null);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function transform($object, string $to, array $context = []): object
    {
        /** @var $object DataEntryProjectInput */
        $this->logger->debug('Start DataEntryProjectInputDataTransformer.transform()');
        $dataEntryProject = new DataEntryProject();
        $this->buildNatureZHE($dataEntryProject, $object);
        $this->buildProjectStatus($dataEntryProject, $object);
        $this->buildMainBusinessStructure($dataEntryProject, $object);

        $this->managerRegistry->getManager()->persist($dataEntryProject);

        $this->buildDataEntry($dataEntryProject, $object);

        $this->logger->debug('End DataEntryProjectInputDataTransformer.transform()');
        return $dataEntryProject;
    }

    private function buildNatureZHE(DataEntryProject $dataEntryProject, DataEntryProjectInput $object)
    {
        $array = $object->getNaturesZHE();
        array_walk(
            $array,
            function (NatureZHEInput $getNatureZHE) use ($dataEntryProject) {
                $natureZHE = new NatureZHE();
                $natureZHE->setTexture($getNatureZHE->getTexture())
                    ->setColor($getNatureZHE->getColor())
                    ->setName($getNatureZHE->getName());
                $this->natureZHEMap->set($getNatureZHE->getUri(), $natureZHE);
                $dataEntryProject->addNatureZHE($natureZHE);
            }
        );
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param DataEntryProjectInput $object
     */
    private function buildProjectStatus(DataEntryProject $dataEntryProject, DataEntryProjectInput $object): void
    {
        try {
            $projectRepo = $this->managerRegistry->getRepository(StatusDataEntry::class);
            //TODO Tester si après avoir créé un projet de saisie improvisée à partir d'un autre projet et sion supprime l'original
            $filters = $this->managerRegistry->getManager()->getFilters();
            $filters->disable('soft_deleteable');
            $project = $projectRepo->findOneBy(['id' => $object->getProjectId()]);
            $filters->enable('soft_deleteable');
            if (is_null($project)) {
                $project = new StatusDataEntry($this->getLoggedUser(), StatusDataEntry::STATUS_WRITE_PENDING);
                $this->managerRegistry->getManager()->persist($project);
            } elseif ($object->isImprovised()) {
                $newProject = new StatusDataEntry($this->getLoggedUser(), StatusDataEntry::STATUS_WRITE_PENDING);
                $this->managerRegistry->getManager()->persist($newProject);
                $newProject->setWebappProject($project->getWebappProject());
                $newProject->setRequest($project->getRequest());
                $project = $newProject;
            }
            $project->setStatus(StatusDataEntry::STATUS_WRITE_PENDING);
            $project->setResponse($dataEntryProject);
            $this->transform2desktopUsersCollection($dataEntryProject, $object->getDesktopUsers());
            $this->transform2stateCodesCollection($dataEntryProject, $object->getStateCodes());
            $dataEntryProject->setImprovised($object->isImprovised());
            if ($object->getGraphicalStructure() !== null) {
                $this->transform2graphicalStructure($dataEntryProject, $object->getGraphicalStructure());
            }

        } catch (RuntimeException $exception) {
            $this->logger->error('Error while selecting/creating project base status StatusDataEntry.');
            throw $exception;
        }
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param DesktopUserInput[] $postUsers
     */
    private function transform2desktopUsersCollection(DataEntryProject $dataEntryProject, array $postUsers): void
    {
        array_walk(
            $postUsers,
            function (DesktopUserInput $postUser) use ($dataEntryProject) {
                $desktopUser = new DesktopUser();
                $desktopUser
                    ->setName($postUser->getName())
                    ->setFirstname($postUser->getFirstname())
                    ->setEmail($postUser->getEmail())
                    ->setLogin($postUser->getLogin())
                    ->setPassword($postUser->getPassword());

                $dataEntryProject->addDesktopUser($desktopUser);
                $this->desktopUsersMap->set($desktopUser->getLogin(), $desktopUser);
            }
        );
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param StateCodeInput[] $postStateCodes
     */
    private function transform2stateCodesCollection(DataEntryProject $dataEntryProject, array $postStateCodes): void
    {
        array_walk(
            $postStateCodes,
            function (StateCodeInput $postStateCode) use ($dataEntryProject) {
                $stateCode = new StateCode();
                $stateCode
                    ->setProject($dataEntryProject)
                    ->setType($postStateCode->getType())
                    ->setCode($postStateCode->getCode())
                    ->setPropagation($postStateCode->getPropagation())
                    ->setLabel($postStateCode->getLabel())
                    ->setDescription($postStateCode->getDescription())
                    ->setColor($postStateCode->getColor());

                if (0 > $stateCode->getCode()) {
                    $this->stateCodesMap->set($stateCode->getCode(), $stateCode);
                }
                $dataEntryProject->addStateCode($stateCode);
            }
        );
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param WorkpathInput[] $postWorkpaths
     */
    private function transform2workpathsCollection(DataEntryProject $dataEntryProject, array $postWorkpaths): void
    {
        array_walk(
            $postWorkpaths,
            function (WorkpathInput $postWorkpath) use ($dataEntryProject) {
                $workpath = new Workpath();
                $workpath
                    ->setStartEnd($postWorkpath->isStartEnd())
                    ->setUsername($postWorkpath->getUsername());

                $pathUris = explode('/', $postWorkpath->getPath());
                array_shift($pathUris); // First of array is empty as path starts with a "/".

                $pathUrisInNewProject = array_map(
                    fn($previousUri) => $this->getUriOfObject($this->objectsMap->get($previousUri)),
                    $pathUris,
                );

                $workpath->setPath('/' . implode('/', $pathUrisInNewProject));

                $dataEntryProject->addWorkpath($workpath);
            }
        );
    }

    private function getUriOfObject($object): string
    {
        $classFullpath = get_class($object);
        $classArray = explode('\\', $classFullpath);
        $classname = array_pop($classArray);

        return "$classname.{$object->getId()}";
    }

    private function transform2graphicalStructure(DataEntryProject $dataEntryProject, GraphicalStructureInput $getGraphicalStructure)
    {
        $graphicalStructure = new GraphicalStructure();
        $graphicalStructure->setproject($dataEntryProject)
            ->setplatformColor($getGraphicalStructure->getPlatformColor())
            ->setDeviceColor($getGraphicalStructure->getDeviceColor())
            ->setBlockColor($getGraphicalStructure->getBlockColor())
            ->setSubBlockColor($getGraphicalStructure->getSubBlockColor())
            ->setIndividualUnitParcelColor($getGraphicalStructure->getIndividualUnitParcelColor())
            ->setSurfaceUnitParcelColor($getGraphicalStructure->getSurfaceUnitParcelColor())
            ->setAbnormalUnitParcelColor($getGraphicalStructure->getAbnormalUnitParcelColor())
            ->setIndividualColor($getGraphicalStructure->getIndividualColor())
            ->setAbnormalIndividualColor($getGraphicalStructure->getAbnormalIndividualColor())
            ->setEmptyColor($getGraphicalStructure->getEmptyColor())
            ->setTooltipActive($getGraphicalStructure->isTooltipActive())
            ->setDeviceLabel($getGraphicalStructure->getDeviceLabel())
            ->setBlockLabel($getGraphicalStructure->getBlockLabel())
            ->setSubBlockLabel($getGraphicalStructure->getSubBlockLabel())
            ->setIndividualUnitParcelLabel($getGraphicalStructure->getIndividualUnitParcelLabel())
            ->setSurfaceUnitParcelLabel($getGraphicalStructure->getSurfaceUnitParcelLabel())
            ->setIndividualLabel($getGraphicalStructure->getIndividualLabel())
            ->setOrigin($getGraphicalStructure->getOrigin())
            ->setBaseWidth($getGraphicalStructure->getBaseWidth())
            ->setBaseHeight($getGraphicalStructure->getBaseHeight());
        $dataEntryProject->setGraphicalStructure($graphicalStructure);
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param DataEntryProjectInput $object
     */
    private function buildMainBusinessStructure(DataEntryProject $dataEntryProject, DataEntryProjectInput $object): void
    {
        try {
            $dataEntryProject
                ->setName($object->getName())
                ->setPlatform($this->transform2platform($object->getPlatform()))
                ->setCreationDate($object->getCreationDate());

            $this->transform2materialCollection($dataEntryProject, $object->getMaterials());
            $this->transform2connectedUniqueVariablesCollection($dataEntryProject, $object->getConnectedUniqueVariables());
            $this->transform2connectedGeneratorVariablesCollection($dataEntryProject, $object->getConnectedGeneratorVariables());
            $this->transform2uniqueVariablesCollection($dataEntryProject, $object->getUniqueVariables());
            $this->transform2generatorVariablesCollection($dataEntryProject, $object->getGeneratorVariables());

            $dataEntryProject->setCreator($this->getIfExists(
                $this->desktopUsersMap,
                $object->getCreatorLogin(),
                'buildMainBusinessStructure',
                'desktopUsersMap'
            ));
        } catch (RuntimeException $exception) {
            $this->logger->error('Error while build main business structure.');
            throw $exception;
        }
    }

    /**
     * @param PlatformInput $postPlatform
     * @return Platform
     */
    private function transform2platform(PlatformInput $postPlatform): Platform
    {
        $platform = new Platform();
        $platform->setName($postPlatform->getName())
            ->setCreationDate($postPlatform->getCreationDate())
            ->setPlace($postPlatform->getPlace())
            ->setSite($postPlatform->getSite());

        $this->transform2devicesCollection($platform, $postPlatform->getDevices());

        return $platform;
    }

    /**
     * @param Platform $platform
     * @param DeviceInput[] $postDevices
     */
    private function transform2devicesCollection(Platform $platform, array $postDevices): void
    {
        array_walk(
            $postDevices,
            function (DeviceInput $postDevice) use ($platform) {
                $device = new Device();
                $device
                    ->setCreationDate($postDevice->getCreationDate())
                    ->setValidationDate($postDevice->getValidationDate())
                    ->setName($postDevice->getName())
                    ->setIndividualPU($postDevice->isIndividualPU());

                $this->transform2notesCollection($device, $postDevice->getNotes());
                // Order is important here. First method will generate treatment mapping required to perform parcels creation.
                $device->setProtocol($this->transform2protocol($postDevice->getProtocol()));
                $this->transform2blocksCollection($device, $postDevice->getBlocks());
                if (!is_null($postDevice->getOutExperimentationZones())) {
                    foreach ($this->transform2OutExperimentationZoneCollection($postDevice->getOutExperimentationZones()) as $outExperimentationZone) {
                        $device->addOutExperimentationZone($outExperimentationZone);
                    }
                }
                if (!is_null($postDevice->getAnnotations())) {
                    $this->transform2AnnotationCollection($device, $postDevice->getAnnotations());
                }

                $platform->addDevice($device);
                $this->objectsMap->set($postDevice->getUri(), $device);
            }
        );
    }

    private function transform2notesCollection(BusinessObject $input, array $getNotes)
    {
        $userRepository = $this->managerRegistry->getRepository(User::class);
        array_walk(
            $getNotes,
            function (NoteInput $noteInput) use ($userRepository, $input) {
                $note = new EntryNote();
                $note->setDeleted($noteInput->isDeleted())
                    ->setText($noteInput->getText())
                    ->setCreationDate($noteInput->getCreationDate())
                    ->setCreator($userRepository->find(explode('.', $noteInput->getCreatorUri())[1]));
                $input->addNote($note);
            }
        );
    }

    /**
     * @param ProtocolInput $postProtocol
     * @return Protocol
     */
    private function transform2protocol(ProtocolInput $postProtocol): Protocol
    {
        $protocol = new Protocol();
        $protocol
            ->setCreationDate($postProtocol->getCreationdate())
            ->setName($postProtocol->getName())
            ->setAim($postProtocol->getAim())
            ->setAlgorithm($postProtocol->getAlgorithm())
            ->setCreator($this->getIfExists(
                $this->desktopUsersMap,
                $postProtocol->getCreatorLogin(),
                'transform2protocol',
                'desktopUsersMap',
                false
            ) ?? $this->getIfExists(
                $this->desktopUsersMap,
                $this->getLoggedUser()->getUsername(),
                'transform2protocol',
                'desktopUsersMap'
            ));

        // Order is important here. First method will generate entity mapping required to call second method.
        $this->transform2factorsCollection($protocol, $postProtocol->getFactors());
        $this->transform2treatmentsCollection($protocol, $postProtocol->getTreatments());

        return $protocol;
    }

    /**
     * @param Collection $collection
     * @param string|int|null $key
     * @param string $functionName
     * @param string $collectionName
     * @param bool $mustExists
     * @return mixed|null
     */
    private function getIfExists(Collection $collection, $key, string $functionName, string $collectionName, bool $mustExists = true)
    {
        if (is_null($key) || !$collection->containsKey($key)) {
            if ($mustExists) {
                throw new RuntimeException(sprintf(
                    'DataEntryProjectInputDataTransformer.%s(): $%s does not contain key "%s"',
                    $functionName,
                    $collectionName,
                    $key
                ));
            } else {
                return null;
            }
        }
        return $collection->get($key);
    }

    /**
     * @param Protocol $protocol
     * @param array $postFactors
     */
    private function transform2factorsCollection(Protocol $protocol, array $postFactors): void
    {
        array_walk(
            $postFactors,
            function (FactorInput $postFactor) use ($protocol) {
                $factor = new Factor();
                $factor->setName($postFactor->getName());
                $this->transform2modalitiesCollection($factor, $postFactor->getModalities());
                $protocol->addFactor($factor);
            }
        );
    }

    /**
     * @param Factor $factor
     * @param ModalityInput[] $postModalities
     */
    private function transform2modalitiesCollection(Factor $factor, array $postModalities): void
    {
        array_walk(
            $postModalities,
            function (ModalityInput $postModality) use ($factor) {
                $modality = new Modality();
                $modality->setValue($postModality->getValue());
                $factor->addModality($modality);
                $this->modalitiesMap->set($modality->getValue(), $modality);
            }
        );
    }

    /**
     * @param Protocol $protocol
     * @param TreatmentInput[] $postTreatments
     */
    private function transform2treatmentsCollection(Protocol $protocol, array $postTreatments): void
    {
        array_walk(
            $postTreatments,
            function (TreatmentInput $postTreatment) use ($protocol) {
                $treatment = new Treatment();
                $treatment
                    ->setName($postTreatment->getName())
                    ->setShortName($postTreatment->getShortName())
                    ->setRepetitions($postTreatment->getRepetitions());

                // Loop on all modalities to search for the entity instance corresponding to the modalities value.
                $postModalitiesValue = $postTreatment->getModalitiesValue();
                array_walk(
                    $postModalitiesValue,
                    function (string $postModalityValue) use ($treatment) {
                        $treatment->addModality($this->getIfExists(
                            $this->modalitiesMap,
                            $postModalityValue,
                            'transform2treatmentsCollection',
                            'modalitiesMap'
                        ));
                    }
                );

                $protocol->addTreatment($treatment);
                $this->treatmentsMap->set($treatment->getName(), $treatment);
            }
        );
    }

    /**
     * @param Device $device
     * @param BlockInput[] $postBlocks
     */
    private function transform2blocksCollection(Device $device, array $postBlocks): void
    {
        array_walk(
            $postBlocks,
            function (BlockInput $postBlock) use ($device) {
                $block = new Block();
                $block->setName($postBlock->getName());

                $this->transform2notesCollection($block, $postBlock->getNotes());
                $this->transform2subBlocksCollection($block, $postBlock->getSubBlocks());
                $this->transform2parcelsCollection($block, $postBlock->getUnitParcels());

                if (!is_null($postBlock->getAnnotations())) {
                    $this->transform2AnnotationCollection($block, $postBlock->getAnnotations());
                }
                if (!is_null($postBlock->getOutExperimentationZones())) {
                    foreach ($this->transform2OutExperimentationZoneCollection($postBlock->getOutExperimentationZones()) as $outExperimentationZone) {
                        $block->addOutExperimentationZone($outExperimentationZone);
                    }
                }

                $device->addBlock($block);
                $this->objectsMap->set($postBlock->getUri(), $block);
            }
        );
    }

    /**
     * @param Block $block
     * @param SubBlockInput[] $postSubBlocks
     */
    private function transform2subBlocksCollection(Block $block, array $postSubBlocks): void
    {
        array_walk(
            $postSubBlocks,
            function (SubBlockInput $postSubBlock) use ($block) {
                $subBlock = new SubBlock();
                $subBlock->setName($postSubBlock->getName());

                $this->transform2notesCollection($subBlock, $postSubBlock->getNotes());
                $this->transform2parcelsCollection($subBlock, $postSubBlock->getUnitParcels());

                if (!is_null($postSubBlock->getAnnotations())) {
                    $this->transform2AnnotationCollection($subBlock, $postSubBlock->getAnnotations());
                }
                if (!is_null($postSubBlock->getOutExperimentationZones())) {
                    foreach ($this->transform2OutExperimentationZoneCollection($postSubBlock->getOutExperimentationZones()) as $outExperimentationZone) {
                        $subBlock->addOutExperimentationZone($outExperimentationZone);
                    }
                }

                $block->addSubBlock($subBlock);
                $this->objectsMap->set($postSubBlock->getUri(), $subBlock);
            }
        );
    }

    /**
     * @param SubBlock|Block $block
     * @param UnitParcelInput[] $postParcels
     */
    private function transform2parcelsCollection($block, array $postParcels): void
    {
        array_walk(
            $postParcels,
            function (UnitParcelInput $postParcel) use ($block) {
                $parcel = (new UnitParcel())
                    ->setName($postParcel->getName())
                    ->setX($postParcel->getX())
                    ->setY($postParcel->getY())
                    ->setApparitionDate($postParcel->getAparitionDate())
                    ->setDemiseDate($postParcel->getDemiseDate())
                    ->setIdent($postParcel->getIdent());

                if ($postParcel->getAnomaly() !== null) {
                    $parcel->setAnomaly($this->transform2Anomaly($postParcel->getAnomaly())
                        ->setBusinessObject($parcel));
                }
                $parcel->setTreatment($this->getIfExists(
                    $this->treatmentsMap,
                    $postParcel->getTreatmentName(),
                    'transform2parcelsCollection',
                    'treatmentsMap',
                    false
                ));
                $parcel->setStateCode($this->getIfExists(
                    $this->stateCodesMap,
                    $postParcel->getStateCode(),
                    'transform2parcelsCollection',
                    'stateCodesMap',
                    false
                ));

                $this->transform2notesCollection($parcel, $postParcel->getNotes());
                $this->transform2individualsCollection($parcel, $postParcel->getIndividuals());

                if (!is_null($postParcel->getAnnotations())) {
                    $this->transform2AnnotationCollection($parcel, $postParcel->getAnnotations());
                }
                if (!is_null($postParcel->getOutExperimentationZones())) {
                    foreach ($this->transform2OutExperimentationZoneCollection($postParcel->getOutExperimentationZones()) as $outExperimentationZone) {
                        $parcel->addOutExperimentationZone($outExperimentationZone);
                    }
                }

                $block->addUnitParcel($parcel);
                $this->objectsMap->set($postParcel->getUri(), $parcel);
            }
        );
    }

    /**
     * @param UnitParcel $parcel
     * @param IndividualInput[] $postIndividuals
     */
    private function transform2individualsCollection(UnitParcel $parcel, array $postIndividuals): void
    {
        array_walk(
            $postIndividuals,
            function (IndividualInput $postIndividual) use ($parcel) {
                $individual = new Individual();
                $individual
                    ->setApparitionDate($postIndividual->getAparitionDate())
                    ->setDemiseDate($postIndividual->getDemiseDate())
                    ->setName($postIndividual->getName())
                    ->setX($postIndividual->getX())
                    ->setY($postIndividual->getY())
                    ->setIdent($postIndividual->getIdent());
                if ($postIndividual->getAnomaly() !== null) {
                    $individual->setAnomaly($this->transform2Anomaly($postIndividual->getAnomaly())
                        ->setBusinessObject($individual));
                }


                $individual->setStateCode($this->getIfExists(
                    $this->stateCodesMap,
                    $postIndividual->getStateCode(),
                    'transform2individualsCollection',
                    'stateCodesMap',
                    false
                ));
                if (!is_null($postIndividual->getAnnotations())) {
                    $this->transform2AnnotationCollection($individual, $postIndividual->getAnnotations());
                }

                $this->transform2notesCollection($individual, $postIndividual->getNotes());

                $parcel->addIndividual($individual);
                $this->objectsMap->set($postIndividual->getUri(), $individual);
            }
        );
    }

    /**
     * @param ProjectObject $projectObject
     * @param AnnotationInput[] $postAnnotation
     */
    private function transform2AnnotationCollection(ProjectObject $projectObject, array $postAnnotation): void
    {
        array_walk(
            $postAnnotation,
            function (AnnotationInput $postAnnotation) use ($projectObject) {
                $annotation = new Annotation();
                $annotation
                    ->setName($postAnnotation->getName())
                    ->setValue($postAnnotation->getValue())
                    ->setType($postAnnotation->getType())
                    ->setImage($postAnnotation->getImage())
                    ->setTimestamp($postAnnotation->getTimestamp())
                    ->setKeywords($postAnnotation->getKeywords())
                    ->setCategories($postAnnotation->getCategories());
                $projectObject->addAnnotation($annotation);
            }
        );
    }

    private function transform2OutExperimentationZoneCollection(array $getOutExperimentationZones): array
    {
        return array_map(
            function (OutExperimentationZoneInput $getOutExperimentationZone) {
                $outExperimentationZone = new OutExperimentationZone();
                $outExperimentationZone->setX($getOutExperimentationZone->getX())
                    ->setY($getOutExperimentationZone->getY())
                    ->setNumber($getOutExperimentationZone->getNum())
                    ->setNatureZHE($this->getIfExists(
                        $this->natureZHEMap,
                        $getOutExperimentationZone->getNatureZHEUri(),
                        'transform2OutExperimentationZoneCollection',
                        'natureZHEMap'
                    ));
                return $outExperimentationZone;
            },
            $getOutExperimentationZones
        );
    }

    private function transform2materialCollection(DataEntryProject $dataEntryProject, array $getMaterials)
    {
        array_walk(
            $getMaterials,
            function (MaterialInput $materialInput) use ($dataEntryProject) {
                $material = new Material();
                $material->setDriver($this->transform2Driver($materialInput->getDriver()))
                    ->setType($materialInput->getType())
                    ->setPhysicalDevice($materialInput->getPhysicalDevice())
                    ->setManufacturer($materialInput->getManufacturer())
                    ->setName($materialInput->getName());
                $dataEntryProject->addMaterial($material);
                $this->materialToHandleMap->set($materialInput->getUid(), $material);
            }
        );
    }

    private function transform2Driver(DriverInput $getDriver): Driver
    {
        return (new Driver())->setType($getDriver->getType())
            ->setPort($getDriver->getPort())
            ->setPush($getDriver->getPush())
            ->setFlowControl($getDriver->getFlowControl())
            ->setRequest($getDriver->getRequest())
            ->setParity($getDriver->getParity())
            ->setStopbit($getDriver->getStopbit())
            ->setBaudrate($getDriver->getBaudrate())
            ->setCsvSeparator($getDriver->getCsvSeparator())
            ->setFrameEnd($getDriver->getFrameEnd())
            ->setFrameStart($getDriver->getFrameStart())
            ->setFrameLength($getDriver->getFrameLength())
            ->setTimeout($getDriver->getTimeout())
            ->setDatabitsFormat($getDriver->getDatabitsFormat());
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param UniqueVariableInput[] $postVariables
     */
    private function transform2connectedUniqueVariablesCollection(DataEntryProject $dataEntryProject, array $postVariables): void
    {
        array_walk(
            $postVariables,
            function (UniqueVariableInput $postVariable) use ($dataEntryProject) {
                $variable = $this->transform2uniqueVariable($postVariable);
                $dataEntryProject->addConnectedUniqueVariable($variable);
                $this->variablesMap->set($postVariable->getUri(), $variable);
            }
        );
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param GeneratorVariableInput[] $postVariables
     */
    private function transform2connectedGeneratorVariablesCollection(DataEntryProject $dataEntryProject, array $postVariables): void
    {
        array_walk(
            $postVariables,
            function (GeneratorVariableInput $postVariable) use ($dataEntryProject) {
                // $variable = $this->transform2generatorVariable($postVariable);
                // In the case of connected variables, to handle the link with the test, we must link the test with the old variable
                /** @var $variable GeneratorVariable */
                $variable = $this->managerRegistry->getRepository(GeneratorVariable::class)->find(explode('.', $postVariable->getUri())[1]);
                $dataEntryProject->addGeneratorVariable($variable);
                $this->variablesMap->set($postVariable->getUri(), $variable);
            }
        );
    }

    /**
     * @param DataEntryProject|GeneratorVariable $hasVariable
     * @param UniqueVariableInput[] $postVariables
     */
    private function transform2uniqueVariablesCollection($hasVariable, array $postVariables): void
    {
        array_walk(
            $postVariables,
            function (UniqueVariableInput $postVariable) use ($hasVariable) {
                $variable = $this->transform2uniqueVariable($postVariable);
                $hasVariable->addUniqueVariable($variable);
                $this->variablesMap->set($postVariable->getUri(), $variable);
            }
        );
    }

    /**
     * @param UniqueVariableInput $postVariable
     * @return UniqueVariable
     */
    private function transform2uniqueVariable(UniqueVariableInput $postVariable): UniqueVariable
    {
        $variable = new UniqueVariable();
        $this->completeWithVariableData($variable, $postVariable);
        $variable
            ->setValueHintList($this->transform2valueHintList($postVariable->getValueHintList()));

        return $variable;
    }

    /**
     * @param Variable $variable
     * @param VariableInput $postVariable
     */
    private function completeWithVariableData(Variable $variable, VariableInput $postVariable): void
    {
        $variable
            ->setType($postVariable->getType())
            ->setRepetitions($postVariable->getRepetitions())
            ->setName($postVariable->getName())
            ->setShortName($postVariable->getShortName())
            ->setActive($postVariable->isActive())
            ->setMandatory($postVariable->isMandatory())
            ->setAskTimestamp($postVariable->isAskTimestamp())
            ->setDefaultValue($postVariable->getDefaultValue())
            ->setFormat($postVariable->getFormat())
            ->setOrder($postVariable->getOrder())
            ->setComment($postVariable->getComment())
            ->setPathLevel($postVariable->getPathLevel())
            ->setUnit($postVariable->getUnit())
            ->setCreationDate($postVariable->getCreationDate())
            ->setModificationDate($postVariable->getModificationDate())
            ->setFrameStartPosition($postVariable->getFrameStartPosition())
            ->setFrameEndPosition($postVariable->getFrameEndPosition())
            ->setMaterial($postVariable->getMaterialUid() === null ? null : $this->getIfExists(
                $this->materialToHandleMap,
                $postVariable->getMaterialUid(),
                'completeWithVariableData',
                'materialToHandleMap'
            ));

        $this->previousValuesToHandleMap->set($postVariable->getUri(), $postVariable->getPreviousValues());
        $this->testsToHandleMap->set($postVariable->getUri(), array_merge(
            $postVariable->getGrowthTests(),
            $postVariable->getRangeTests(),
            $postVariable->getCombinationTests(),
            $postVariable->getPreconditionedCalculations()
        ));
    }

    /**
     * @param ValueHintListInput|null $postValueHintList
     * @return ValueHintList|null
     */
    private function transform2valueHintList(?ValueHintListInput $postValueHintList): ?ValueHintList
    {
        $valueHintList = !is_null($postValueHintList) ? new ValueHintList() : null;
        if (!is_null($valueHintList)) {
            $valueHintList
                ->setName($postValueHintList->getName());
            $postValueHints = $postValueHintList->getValueHints();
            array_walk(
                $postValueHints,
                function (ValueHintInput $postValueHint) use ($valueHintList) {
                    $valueHint = new ValueHint();
                    $valueHint->setValue($postValueHint->getValue());
                    $valueHintList->addValueHint($valueHint);
                }
            );
        }

        return $valueHintList;
    }

    /**
     * @param DataEntryProject|GeneratorVariable $hasVariable
     * @param GeneratorVariableInput[] $postVariables
     */
    private function transform2generatorVariablesCollection($hasVariable, array $postVariables): void
    {
        array_walk(
            $postVariables,
            function (GeneratorVariableInput $postVariable) use ($hasVariable) {
                $variable = $this->transform2generatorVariable($postVariable);
                $hasVariable->addGeneratorVariable($variable);
                $this->variablesMap->set($postVariable->getUri(), $variable);
            }
        );
    }

    /**
     * @param GeneratorVariableInput $postVariable
     * @return GeneratorVariable
     */
    private function transform2generatorVariable(GeneratorVariableInput $postVariable): GeneratorVariable
    {
        $variable = new GeneratorVariable();
        $this->completeWithVariableData($variable, $postVariable);
        $variable->setGeneratedPrefix($postVariable->getGeneratedPrefix());
        $variable->setNumeralIncrement($postVariable->isNumeralIncrement());
        $this->transform2uniqueVariablesCollection($variable, $postVariable->getUniqueVariables());
        $this->transform2generatorVariablesCollection($variable, $postVariable->getGeneratorVariables());

        return $variable;
    }

    /**
     * @param DataEntryProject $dataEntryProject
     * @param DataEntryProjectInput $object
     */
    private function buildDataEntry(DataEntryProject $dataEntryProject, DataEntryProjectInput $object): void
    {
        try {
            $this->transform2workpathsCollection($dataEntryProject, $object->getWorkpaths());
            $dataEntryProject->setDataEntry($this->transform2dataEntry($object->getDataEntry()));
        } catch (RuntimeException $exception) {
            $this->logger->error('Error while build retrieving measures from data entry.');
            throw $exception;
        }
    }

    /**
     * @param DataEntryInput|null $postDataEntry
     * @return DataEntry
     */
    private function transform2dataEntry(?DataEntryInput $postDataEntry): ?DataEntry
    {
        $dataEntry = !is_null($postDataEntry) ? new DataEntry() : null;
        if (!is_null($dataEntry)) {
            $dataEntry
                ->setStatus($postDataEntry->getStatus())
                ->setStartedAt($postDataEntry->getStartedAt())
                ->setEndedAt($postDataEntry->getEndedAt())
                ->setCurrentPosition($postDataEntry->getCurrentPosition())
                ->setWorkpath($postDataEntry->getWorkpath());

            $this->transform2sessionsCollection($dataEntry, $postDataEntry->getSessions());
        }
        return $dataEntry;
    }

    /**
     * @param DataEntry $dataEntry
     * @param SessionInput[] $postSessions
     */
    private function transform2sessionsCollection(DataEntry $dataEntry, array $postSessions): void
    {
        array_walk(
            $postSessions,
            function (SessionInput $postSession) use ($dataEntry) {
                $session = new Session();
                $session
                    ->setStartedAt($postSession->getStartedAt())
                    ->setEndedAt($postSession->getEndedAt());

                $this->transform2formFieldsCollection($session, $postSession->getFormFields());

                $dataEntry->addSession($session);
            }
        );
    }

    /**
     * @param Session|GeneratedField $formFieldContainer
     * @param array $postFormFields
     */
    private function transform2formFieldsCollection($formFieldContainer, array $postFormFields): void
    {
        array_walk(
            $postFormFields,
            function (FormFieldInput $postFormField) use ($formFieldContainer) {
                $formField = new FormField();

                $formField->setTarget($this->getIfExists(
                    $this->objectsMap,
                    $postFormField->getTargetUri(),
                    'transform2formFieldsCollection',
                    'objectsMap'
                ));
                $formField->setVariable($this->getIfExists(
                    $this->variablesMap,
                    $postFormField->getVariableUri(),
                    'transform2formFieldsCollection',
                    'variablesMap'
                ));

                $varType = $formField->getVariable()->getType();
                $this->transform2measuresCollection($formField, $varType, $postFormField->getMeasures());
                $this->transform2generatedFieldsCollection($formField, $postFormField->getGeneratedFields());

                $formFieldContainer->addFormField($formField);
            }
        );
    }

    /**
     * @param FormField $formField
     * @param string $type
     * @psalm-param VariableTypeEnumId|'' $type
     * @param MeasureInput[] $postMeasures
     */
    private function transform2measuresCollection(FormField $formField, string $type, array $postMeasures): void
    {
        array_walk(
            $postMeasures,
            function (MeasureInput $postMeasure) use ($formField, $type) {
                $measure = new Measure();

                $measure
                    ->setRepetition($postMeasure->getRepetition())
                    ->setTimestamp($postMeasure->getTimestamp());

                switch ($type) {
                    case VariableTypeEnum::ALPHANUMERIC:
                    case VariableTypeEnum::HOUR:
                    case VariableTypeEnum::DATE:
                    case VariableTypeEnum::INTEGER:
                    case VariableTypeEnum::REAL:
                        $measure->setValue(strval($postMeasure->getValue()));
                        break;
                    case VariableTypeEnum::BOOLEAN:
                        $measure->setValue($postMeasure->getValue() ? "true" : "false");
                        break;
                    default:
                        $measure->setValue(null);
                }
                $measure->setState($this->getIfExists(
                    $this->stateCodesMap,
                    sprintf('%d', $postMeasure->getState()->getCode()),
                    'transform2measuresCollection',
                    'variablesMap',
                    false
                ));

                if (!is_null($postMeasure->getAnnotations())) {
                    $this->transform2AnnotationCollection($measure, $postMeasure->getAnnotations());
                }

                $formField->addMeasure($measure);
            }
        );
    }

    /**
     * @param FormField $formField
     * @param GeneratedFieldInput[]|null $postGeneratedFields
     */
    private function transform2generatedFieldsCollection(FormField $formField, ?array $postGeneratedFields): void
    {
        if (!is_null($postGeneratedFields)) {
            array_walk(
                $postGeneratedFields,
                function (GeneratedFieldInput $postGeneratedField) use ($formField) {
                    $generatedField = new GeneratedField();
                    $generatedField
                        ->setIndex($postGeneratedField->getIndex())
                        ->setPrefix($postGeneratedField->getPrefix())
                        ->setNumeralIncrement($postGeneratedField->isNumeralIncrement());

                    $this->transform2formFieldsCollection($generatedField, $postGeneratedField->getFormFields());

                    $formField->addGeneratedField($generatedField);
                }
            );
        }
    }

    private function transform2Anomaly(AnomalyInput $getAnomaly): Anomaly
    {
        $anomaly = new Anomaly();
        $anomaly->setDescription($getAnomaly->getDescription())
            ->setType($getAnomaly->getType())
            ->setConstatedTreatment($this->getIfExists(
                $this->treatmentsMap,
                $getAnomaly->getConstatedTreatmentName(),
                'transform2Anomaly',
                'treatmentsMap',
                false
            ));
        return $anomaly;
    }

    private function getLoggedUser(): User
    {
        $user = $this->tokenStorage->getToken()->getUser();
        assert($user instanceof User);

        return $user;
    }

}
