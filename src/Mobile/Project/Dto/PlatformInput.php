<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\Dto;

use DateTime;
use Mobile\Device\Dto\DeviceInput;

/**
 * Class PlatformInput.
 */
class PlatformInput
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var DeviceInput[]
     */
    private $devices;

    /**
     * @var DateTime
     */
    private $creationDate;

    /**
     * @var string
     */
    private $site;

    /**
     * @var string
     */
    private $place;


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return PlatformInput
     */
    public function setName(string $name): PlatformInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return DeviceInput[]
     */
    public function getDevices(): array
    {
        return $this->devices;
    }

    /**
     * @param DeviceInput[] $devices
     * @return PlatformInput
     */
    public function setDevices(array $devices): PlatformInput
    {
        $this->devices = $devices;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     * @return PlatformInput
     */
    public function setCreationDate(DateTime $creationDate): PlatformInput
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @param string $site
     * @return PlatformInput
     */
    public function setSite(string $site): PlatformInput
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string
     */
    public function getPlace(): string
    {
        return $this->place;
    }

    /**
     * @param string $place
     * @return PlatformInput
     */
    public function setPlace(string $place): PlatformInput
    {
        $this->place = $place;
        return $this;
    }


}
