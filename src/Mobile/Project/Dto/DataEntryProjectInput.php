<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\Dto;

use DateTime;
use Mobile\Measure\Dto\DataEntryInput;
use Mobile\Measure\Dto\Variable\GeneratorVariableInput;
use Mobile\Measure\Dto\Variable\MaterialInput;
use Mobile\Measure\Dto\Variable\RequiredAnnotationInput;
use Mobile\Measure\Dto\Variable\StateCodeInput;
use Mobile\Measure\Dto\Variable\UniqueVariableInput;

/**
 * Class DataEntryProjectInput.
 */
class DataEntryProjectInput
{
    /**
     * @var int|string
     */
    private $id;

    /**
     * @var int|null
     */
    private $projectId;

    /**
     * @var string|null
     */
    private $ownerIri;

    /**
     * @var string
     */
    private $name;

    /**
     * @var PlatformInput
     */
    private $platform;

    /**
     * @var string|null
     */
    private $creatorLogin;

    /**
     * @var UniqueVariableInput[]
     */
    private $uniqueVariables;

    /**
     * @var GeneratorVariableInput[]
     */
    private $generatorVariables;

    /**
     * @var DataEntryInput|null
     */
    private $dataEntry;

    /**
     * @var StateCodeInput[]
     */
    private $stateCodes;

    /**
     * @var DesktopUserInput[]
     */
    private $desktopUsers;

    /**
     * @var DateTime
     */
    private $creationDate;

    /**
     * @var MaterialInput[]
     */
    private $materials;

    /**
     * @var NatureZHEInput[]
     */
    private $naturesZHE;

    /**
     * @var GraphicalStructureInput
     */
    private $graphicalStructure;

    /**
     * @var WorkpathInput[]
     */
    private $workpaths;

    /**
     * @var UniqueVariableInput[]
     */
    private $connectedUniqueVariables;

    /**
     * @var GeneratorVariableInput[]
     */
    private $connectedGeneratorVariables;

    /**
     * @var RequiredAnnotationInput[]
     */
    private $requiredAnnotations;

    /**
     * @var bool
     */
    private $improvised;

    /**
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|string $id
     * @return DataEntryProjectInput
     */
    public function setId($id): DataEntryProjectInput
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getProjectId(): ?int
    {
        return $this->projectId;
    }

    /**
     * @param int|null $projectId
     * @return DataEntryProjectInput
     */
    public function setProjectId(?int $projectId): DataEntryProjectInput
    {
        $this->projectId = $projectId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOwnerIri(): ?string
    {
        return $this->ownerIri;
    }

    /**
     * @param string|null $ownerIri
     * @return DataEntryProjectInput
     */
    public function setOwnerIri(?string $ownerIri): DataEntryProjectInput
    {
        $this->ownerIri = $ownerIri;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DataEntryProjectInput
     */
    public function setName(string $name): DataEntryProjectInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return PlatformInput
     */
    public function getPlatform(): PlatformInput
    {
        return $this->platform;
    }

    /**
     * @param PlatformInput $platform
     * @return DataEntryProjectInput
     */
    public function setPlatform(PlatformInput $platform): DataEntryProjectInput
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatorLogin(): ?string
    {
        return $this->creatorLogin;
    }

    /**
     * @param string|null $creatorLogin
     * @return DataEntryProjectInput
     */
    public function setCreatorLogin(?string $creatorLogin): DataEntryProjectInput
    {
        $this->creatorLogin = $creatorLogin;
        return $this;
    }

    /**
     * @return UniqueVariableInput[]
     */
    public function getUniqueVariables(): array
    {
        return $this->uniqueVariables;
    }

    /**
     * @param UniqueVariableInput[] $uniqueVariables
     * @return DataEntryProjectInput
     */
    public function setUniqueVariables(array $uniqueVariables): DataEntryProjectInput
    {
        $this->uniqueVariables = $uniqueVariables;
        return $this;
    }

    /**
     * @return GeneratorVariableInput[]
     */
    public function getGeneratorVariables(): array
    {
        return $this->generatorVariables;
    }

    /**
     * @param GeneratorVariableInput[] $generatorVariables
     * @return DataEntryProjectInput
     */
    public function setGeneratorVariables(array $generatorVariables): DataEntryProjectInput
    {
        $this->generatorVariables = $generatorVariables;
        return $this;
    }

    /**
     * @return DataEntryInput|null
     */
    public function getDataEntry(): ?DataEntryInput
    {
        return $this->dataEntry;
    }

    /**
     * @param DataEntryInput|null $dataEntry
     * @return DataEntryProjectInput
     */
    public function setDataEntry(?DataEntryInput $dataEntry): DataEntryProjectInput
    {
        $this->dataEntry = $dataEntry;
        return $this;
    }

    /**
     * @return StateCodeInput[]
     */
    public function getStateCodes(): array
    {
        return $this->stateCodes;
    }

    /**
     * @param StateCodeInput[] $stateCodes
     * @return DataEntryProjectInput
     */
    public function setStateCodes(array $stateCodes): DataEntryProjectInput
    {
        $this->stateCodes = $stateCodes;
        return $this;
    }

    /**
     * @return DesktopUserInput[]
     */
    public function getDesktopUsers(): array
    {
        return $this->desktopUsers;
    }

    /**
     * @param DesktopUserInput[] $desktopUsers
     * @return DataEntryProjectInput
     */
    public function setDesktopUsers(array $desktopUsers): DataEntryProjectInput
    {
        $this->desktopUsers = $desktopUsers;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreationDate(): DateTime
    {
        return $this->creationDate;
    }

    /**
     * @param DateTime $creationDate
     * @return DataEntryProjectInput
     */
    public function setCreationDate(DateTime $creationDate): DataEntryProjectInput
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return MaterialInput[]
     */
    public function getMaterials(): array
    {
        return $this->materials;
    }

    /**
     * @param MaterialInput[] $materials
     * @return DataEntryProjectInput
     */
    public function setMaterials(array $materials): DataEntryProjectInput
    {
        $this->materials = $materials;
        return $this;
    }

    /**
     * @return NatureZHEInput[]
     */
    public function getNaturesZHE(): array
    {
        return $this->naturesZHE;
    }

    /**
     * @param NatureZHEInput[] $naturesZHE
     * @return DataEntryProjectInput
     */
    public function setNaturesZHE(array $naturesZHE): DataEntryProjectInput
    {
        $this->naturesZHE = $naturesZHE;
        return $this;
    }

    /**
     * @return GraphicalStructureInput
     */
    public function getGraphicalStructure(): GraphicalStructureInput
    {
        return $this->graphicalStructure;
    }

    /**
     * @param GraphicalStructureInput $graphicalStructure
     * @return DataEntryProjectInput
     */
    public function setGraphicalStructure(GraphicalStructureInput $graphicalStructure): DataEntryProjectInput
    {
        $this->graphicalStructure = $graphicalStructure;
        return $this;
    }

    /**
     * @return WorkpathInput[]
     */
    public function getWorkpaths(): array
    {
        return $this->workpaths;
    }

    /**
     * @param WorkpathInput[] $workpaths
     * @return DataEntryProjectInput
     */
    public function setWorkpaths(array $workpaths): DataEntryProjectInput
    {
        $this->workpaths = $workpaths;
        return $this;
    }

    /**
     * @return UniqueVariableInput[]
     */
    public function getConnectedUniqueVariables(): array
    {
        return $this->connectedUniqueVariables;
    }

    /**
     * @param UniqueVariableInput[] $connectedUniqueVariables
     * @return DataEntryProjectInput
     */
    public function setConnectedUniqueVariables(array $connectedUniqueVariables): DataEntryProjectInput
    {
        $this->connectedUniqueVariables = $connectedUniqueVariables;
        return $this;
    }

    /**
     * @return GeneratorVariableInput[]
     */
    public function getConnectedGeneratorVariables(): array
    {
        return $this->connectedGeneratorVariables;
    }

    /**
     * @param GeneratorVariableInput[] $connectedGeneratorVariables
     * @return DataEntryProjectInput
     */
    public function setConnectedGeneratorVariables(array $connectedGeneratorVariables): DataEntryProjectInput
    {
        $this->connectedGeneratorVariables = $connectedGeneratorVariables;
        return $this;
    }

    /**
     * @return RequiredAnnotationInput[]
     */
    public function getRequiredAnnotations(): array
    {
        return $this->requiredAnnotations;
    }

    /**
     * @param RequiredAnnotationInput[] $requiredAnnotations
     * @return DataEntryProjectInput
     */
    public function setRequiredAnnotations(array $requiredAnnotations): DataEntryProjectInput
    {
        $this->requiredAnnotations = $requiredAnnotations;
        return $this;
    }

    /**
     * @return bool
     */
    public function isImprovised(): bool
    {
        return $this->improvised;
    }

    /**
     * @param bool $improvised
     * @return DataEntryProjectInput
     */
    public function setImprovised(bool $improvised): DataEntryProjectInput
    {
        $this->improvised = $improvised;
        return $this;
    }
}
