<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\Dto;

/**
 * Class DesktopUserInput.
 */
class DesktopUserInput
{
    /**
     * @var int|null
     */
    private ?int $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return DesktopUserInput
     */
    public function setId(?int $id): DesktopUserInput
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DesktopUserInput
     */
    public function setName(string $name): DesktopUserInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return DesktopUserInput
     */
    public function setFirstname(string $firstname): DesktopUserInput
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return DesktopUserInput
     */
    public function setLogin(string $login): DesktopUserInput
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return DesktopUserInput
     */
    public function setPassword(string $password): DesktopUserInput
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return DesktopUserInput
     */
    public function setEmail(string $email): DesktopUserInput
    {
        $this->email = $email;
        return $this;
    }
}
