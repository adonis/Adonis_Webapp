<?php

namespace Mobile\Project\Dto;

/**
 * Class NatureZHEInput.
 */
class NatureZHEInput
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string|null
     */
    private $color;
    /**
     * @var string|null
     */
    private $texture;
    /**
     * @var string
     */
    private $uri;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return NatureZHEInput
     */
    public function setName(string $name): NatureZHEInput
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     * @return NatureZHEInput
     */
    public function setColor(?string $color): NatureZHEInput
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTexture(): ?string
    {
        return $this->texture;
    }

    /**
     * @param string|null $texture
     * @return NatureZHEInput
     */
    public function setTexture(?string $texture): NatureZHEInput
    {
        $this->texture = $texture;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return NatureZHEInput
     */
    public function setUri(string $uri): NatureZHEInput
    {
        $this->uri = $uri;
        return $this;
    }

}
