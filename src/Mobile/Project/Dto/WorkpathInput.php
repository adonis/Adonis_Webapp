<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Mobile\Project\Dto;

/**
 * Class WorkpathInput.
 */
class WorkpathInput
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $path;

    /**
     * @var bool
     */
    private $startEnd;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return WorkpathInput
     */
    public function setUsername(string $username): WorkpathInput
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return WorkpathInput
     */
    public function setPath(string $path): WorkpathInput
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStartEnd(): bool
    {
        return $this->startEnd;
    }

    /**
     * @param bool $startEnd
     * @return WorkpathInput
     */
    public function setStartEnd(bool $startEnd): WorkpathInput
    {
        $this->startEnd = $startEnd;
        return $this;
    }
}
