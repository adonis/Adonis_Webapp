<?php

namespace Mobile\Project\Dto;

/**
 * Class GraphicalStructureInput.
 */
class GraphicalStructureInput
{
    /**
     * @var string
     */
    private $platformColor;

    /**
     * @var string
     */
    private $deviceColor;

    /**
     * @var string
     */
    private $blockColor;

    /**
     * @var string
     */
    private $subBlockColor;

    /**
     * @var string
     */
    private $individualUnitParcelColor;

    /**
     * @var string
     */
    private $surfaceUnitParcelColor;

    /**
     * @var string
     */
    private $abnormalUnitParcelColor;

    /**
     * @var string
     */
    private $individualColor;

    /**
     * @var string
     */
    private $abnormalIndividualColor;

    /**
     * @var string
     */
    private $emptyColor;

    /**
     * @var bool
     */
    private $tooltipActive;

    /**
     * @var string
     */
    private $deviceLabel;

    /**
     * @var string
     */
    private $blockLabel;

    /**
     * @var string
     */
    private $subBlockLabel;

    /**
     * @var string
     */
    private $individualUnitParcelLabel;

    /**
     * @var string
     */
    private $surfaceUnitParcelLabel;

    /**
     * @var string
     */
    private $individualLabel;

    /**
     * @var string|null
     */
    private $origin;


    /**
     * @var int|null
     */
    private $baseWidth;


    /**
     * @var int|null
     */
    private $baseHeight;

    /**
     * @return string
     */
    public function getPlatformColor(): string
    {
        return $this->platformColor;
    }

    /**
     * @param string $platformColor
     * @return GraphicalStructureInput
     */
    public function setPlatformColor(string $platformColor): GraphicalStructureInput
    {
        $this->platformColor = $platformColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceColor(): string
    {
        return $this->deviceColor;
    }

    /**
     * @param string $deviceColor
     * @return GraphicalStructureInput
     */
    public function setDeviceColor(string $deviceColor): GraphicalStructureInput
    {
        $this->deviceColor = $deviceColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getBlockColor(): string
    {
        return $this->blockColor;
    }

    /**
     * @param string $blockColor
     * @return GraphicalStructureInput
     */
    public function setBlockColor(string $blockColor): GraphicalStructureInput
    {
        $this->blockColor = $blockColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubBlockColor(): string
    {
        return $this->subBlockColor;
    }

    /**
     * @param string $subBlockColor
     * @return GraphicalStructureInput
     */
    public function setSubBlockColor(string $subBlockColor): GraphicalStructureInput
    {
        $this->subBlockColor = $subBlockColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndividualUnitParcelColor(): string
    {
        return $this->individualUnitParcelColor;
    }

    /**
     * @param string $individualUnitParcelColor
     * @return GraphicalStructureInput
     */
    public function setIndividualUnitParcelColor(string $individualUnitParcelColor): GraphicalStructureInput
    {
        $this->individualUnitParcelColor = $individualUnitParcelColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurfaceUnitParcelColor(): string
    {
        return $this->surfaceUnitParcelColor;
    }

    /**
     * @param string $surfaceUnitParcelColor
     * @return GraphicalStructureInput
     */
    public function setSurfaceUnitParcelColor(string $surfaceUnitParcelColor): GraphicalStructureInput
    {
        $this->surfaceUnitParcelColor = $surfaceUnitParcelColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndividualColor(): string
    {
        return $this->individualColor;
    }

    /**
     * @param string $individualColor
     * @return GraphicalStructureInput
     */
    public function setIndividualColor(string $individualColor): GraphicalStructureInput
    {
        $this->individualColor = $individualColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getAbnormalIndividualColor(): string
    {
        return $this->abnormalIndividualColor;
    }

    /**
     * @param string $abnormalIndividualColor
     * @return GraphicalStructureInput
     */
    public function setAbnormalIndividualColor(string $abnormalIndividualColor): GraphicalStructureInput
    {
        $this->abnormalIndividualColor = $abnormalIndividualColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmptyColor(): string
    {
        return $this->emptyColor;
    }

    /**
     * @param string $emptyColor
     * @return GraphicalStructureInput
     */
    public function setEmptyColor(string $emptyColor): GraphicalStructureInput
    {
        $this->emptyColor = $emptyColor;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @param string|null $origin
     * @return GraphicalStructureInput
     */
    public function setOrigin(?string $origin): GraphicalStructureInput
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBaseWidth(): ?int
    {
        return $this->baseWidth;
    }

    /**
     * @param int|null $baseWidth
     * @return GraphicalStructureInput
     */
    public function setBaseWidth(?int $baseWidth): GraphicalStructureInput
    {
        $this->baseWidth = $baseWidth;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBaseHeight(): ?int
    {
        return $this->baseHeight;
    }

    /**
     * @param int|null $baseHeight
     * @return GraphicalStructureInput
     */
    public function setBaseHeight(?int $baseHeight): GraphicalStructureInput
    {
        $this->baseHeight = $baseHeight;
        return $this;
    }

    /**
     * @return string
     */
    public function getAbnormalUnitParcelColor(): string
    {
        return $this->abnormalUnitParcelColor;
    }

    /**
     * @param string $abnormalUnitParcelColor
     * @return GraphicalStructureInput
     */
    public function setAbnormalUnitParcelColor(string $abnormalUnitParcelColor): GraphicalStructureInput
    {
        $this->abnormalUnitParcelColor = $abnormalUnitParcelColor;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTooltipActive(): bool
    {
        return $this->tooltipActive;
    }

    /**
     * @param bool $tooltipActive
     * @return GraphicalStructureInput
     */
    public function setTooltipActive(bool $tooltipActive): GraphicalStructureInput
    {
        $this->tooltipActive = $tooltipActive;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceLabel(): string
    {
        return $this->deviceLabel;
    }

    /**
     * @param string $deviceLabel
     * @return GraphicalStructureInput
     */
    public function setDeviceLabel(string $deviceLabel): GraphicalStructureInput
    {
        $this->deviceLabel = $deviceLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function getBlockLabel(): string
    {
        return $this->blockLabel;
    }

    /**
     * @param string $blockLabel
     * @return GraphicalStructureInput
     */
    public function setBlockLabel(string $blockLabel): GraphicalStructureInput
    {
        $this->blockLabel = $blockLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubBlockLabel(): string
    {
        return $this->subBlockLabel;
    }

    /**
     * @param string $subBlockLabel
     * @return GraphicalStructureInput
     */
    public function setSubBlockLabel(string $subBlockLabel): GraphicalStructureInput
    {
        $this->subBlockLabel = $subBlockLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndividualUnitParcelLabel(): string
    {
        return $this->individualUnitParcelLabel;
    }

    /**
     * @param string $individualUnitParcelLabel
     * @return GraphicalStructureInput
     */
    public function setIndividualUnitParcelLabel(string $individualUnitParcelLabel): GraphicalStructureInput
    {
        $this->individualUnitParcelLabel = $individualUnitParcelLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function getSurfaceUnitParcelLabel(): string
    {
        return $this->surfaceUnitParcelLabel;
    }

    /**
     * @param string $surfaceUnitParcelLabel
     * @return GraphicalStructureInput
     */
    public function setSurfaceUnitParcelLabel(string $surfaceUnitParcelLabel): GraphicalStructureInput
    {
        $this->surfaceUnitParcelLabel = $surfaceUnitParcelLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndividualLabel(): string
    {
        return $this->individualLabel;
    }

    /**
     * @param string $individualLabel
     * @return GraphicalStructureInput
     */
    public function setIndividualLabel(string $individualLabel): GraphicalStructureInput
    {
        $this->individualLabel = $individualLabel;
        return $this;
    }

}
