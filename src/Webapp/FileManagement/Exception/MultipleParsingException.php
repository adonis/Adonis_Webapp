<?php

namespace Webapp\FileManagement\Exception;

use Exception;

/**
 * Class MultipleParsingException.
 */
class MultipleParsingException extends Exception
{
    public $errors;

    public function __construct(array $errors)
    {
        $this->errors = $errors;
        parent::__construct("");
    }

}
