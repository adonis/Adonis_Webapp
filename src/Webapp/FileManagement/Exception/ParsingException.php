<?php

namespace Webapp\FileManagement\Exception;

use Exception;
use Webapp\FileManagement\Entity\RequestFile;
use Throwable;

/**
 * Class ParsingException.
 */
class ParsingException extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        switch ($code) {
            case RequestFile::ERROR_INCORRECT_FILE_TYPE:
                $message = 'ERROR_INCORRECT_FILE_TYPE';
                break;
            case RequestFile::ERROR_DURING_UNZIPPING:
                $message = 'ERROR_DURING_UNZIPPING';
                break;
            case RequestFile::ERROR_EXTRACTED_FILE_NOT_FOUND:
                $message = 'ERROR_EXTRACTED_FILE_NOT_FOUND';
                break;
            case RequestFile::ERROR_DATA_ENTRY_PROJECT_MISSING:
                $message = 'ERROR_DATA_ENTRY_PROJECT_MISSING';
                break;
            case RequestFile::ERROR_PLATFORM_MISSING:
                $message = 'ERROR_PLATFORM_MISSING';
                break;
            case RequestFile::ERROR_NO_DEVICE:
                $message = 'ERROR_NO_DEVICE';
                break;
            case RequestFile::ERROR_NO_BLOC:
                $message = 'ERROR_NO_BLOC';
                break;
            case RequestFile::ERROR_NO_BLOC_CHILDREN:
                $message = 'ERROR_NO_BLOC_CHILDREN';
                break;
            case RequestFile::ERROR_NO_WORKPATH:
                $message = 'ERROR_NO_WORKPATH';
                break;
            case RequestFile::ERROR_DUPLICATED_URI:
                $message = 'ERROR_DUPLICATED_URI';
                break;
            case RequestFile::ERROR_WORKPATH_ITEM_NOT_FOUND:
                $message = 'ERROR_WORKPATH_ITEM_NOT_FOUND';
                break;
            case RequestFile::ERROR_NO_PROTOCOL:
                $message = 'ERROR_NO_PROTOCOL';
                break;
            case RequestFile::ERROR_PROTOCOL_INFO_MISSING:
                $message = 'ERROR_PROTOCOL_INFO_MISSING';
                break;
            case RequestFile::ERROR_MODALITY_ITEM_NOT_FOUND:
                $message = 'ERROR_MODALITY_ITEM_NOT_FOUND';
                break;
            case RequestFile::ERROR_STATE_CODE_INFO_MISSING:
                $message = 'ERROR_STATE_CODE_INFO_MISSING';
                break;
            case RequestFile::ERROR_NO_GENERATED_VARIABLE:
                $message = 'NO_GENERATED_VARIABLE';
                break;
            case RequestFile::ERROR_TEST_INFO_MISSING:
                $message = 'ERROR_TEST_INFO_MISSING';
                break;
            case RequestFile::ERROR_UNKNOWN_TEST_TYPE:
                $message = 'UNKNOWN_TEST_TYPE';
                break;
            case RequestFile::ERROR_UNKNOWN_TEST_LINKED_VARIABLE:
                $message = 'UNKNOWN_TEST_LINKED_VARIABLE';
                break;
            case RequestFile::ERROR_PREVIOUS_VALUE_INFO_MISSING:
                $message = 'ERROR_PREVIOUS_VALUE_INFO_MISSING';
                break;
            case RequestFile::ERROR_UNKNOWN_PREVIOUS_VALUE_LINKED_OBJECT:
                $message = 'UNKNOWN_PREVIOUS_VALUE_LINKED_OBJECT';
                break;
        }
        parent::__construct($message, $code, $previous);
    }

}
