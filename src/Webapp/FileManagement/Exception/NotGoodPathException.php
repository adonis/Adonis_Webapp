<?php

namespace Webapp\FileManagement\Exception;

use Exception;

/**
 * Class NotGoodPathException.
 */
class NotGoodPathException extends Exception
{
}
