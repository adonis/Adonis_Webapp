<?php

namespace Webapp\FileManagement\Service;

use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Storage\StorageInterface;
use Webapp\FileManagement\Entity\Base\UserLinkedFileJob;

/**
 * Class AbstractWriterService.
 */
abstract class AbstractWriterService extends AbstractFileService
{

    public const NS_XMI = "http://www.omg.org/XMI";
    public const NS_SITE = "http:///adonis/modeleMetier/site.ecore";
    public const NS_VARIABLES = "http:///adonis/modeleMetier/projetDeSaisie/variables.ecore";
    public const NS_PROJET_SAISIE = "http:///adonis/modeleMetier/projetDeSaisie.ecore";
    public const NS_XSI = "http://www.w3.org/2001/XMLSchema-instance";
    public const NS_CHEMINEMENT = "http:///adonis/modeleMetier/projetDeSaisie/cheminement.ecore";
    public const NS_UTILISATEUR = "http:///adonis/modeleMetier/utilisateur.ecore";
    public const NS_CONCEPT_DE_BASE = "http:///adonis/modeleMetier/conceptsDeBase.ecore";
    public const NS_GRAPHIQUE = "http:///adonis/modeleMetier/graphique.ecore";
    public const NS_PLATEFORME = "http:///adonis/modeleMetier/plateforme.ecore";
    public const NS_PREFERENCE = "http:///adonis/modeleMetier/preference.ecore";
    public const NS_SAISIE_TERRAIN = "http:///adonis/modeleMetier/saisieTerrain.ecore";
    public const dateFormat = 'Y_m_d_H_i_s';

    public function __construct(EntityManagerInterface $entityManager,
                                DirectoryNamer         $directoryNameManager,
                                StorageInterface       $storage,
                                string                 $tmpDir)
    {
        parent::__construct($entityManager, $directoryNameManager, $storage, $tmpDir);
    }

    protected function copyFileFromTmpDir(UserLinkedFileJob $projectFile)
    {
        copy($this->getTmpDir($projectFile) . $projectFile->getFilePath(), $this->storage->resolvePath($projectFile, 'file'));
    }
}
