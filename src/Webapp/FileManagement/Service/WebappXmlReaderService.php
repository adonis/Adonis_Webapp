<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service;

use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Webapp\FileManagement\Dto\Webapp\ExperimentDto;
use Webapp\FileManagement\Dto\Webapp\PlatformDto;
use Webapp\FileManagement\Dto\Webapp\VariableCollectionDto;
use Webapp\FileManagement\Service\XmlUtils\CustomXmlEncoder;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractRootNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;

final class WebappXmlReaderService
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function readPlatform(string $xml, Site $site, User $user, ?string $filesPath = null): PlatformDto
    {
        return $this->deserialize($xml, PlatformDto::class, [
            PlatformDtoNormalizer::SITE => $site,
            PlatformDtoNormalizer::USER => $user,
            AbstractRootNormalizer::FILES_PATH => $filesPath,
        ]);
    }

    public function readExperiment(string $xml, Site $site, User $user, ?string $filesPath = null): ExperimentDto
    {
        return $this->deserialize($xml, ExperimentDto::class, [
            PlatformDtoNormalizer::SITE => $site,
            PlatformDtoNormalizer::USER => $user,
            AbstractRootNormalizer::FILES_PATH => $filesPath,
        ]);
    }

    public function readVariableCollection(string $xml, Site $site, User $user, ?string $filesPath = null): VariableCollectionDto
    {
        return $this->deserialize($xml, VariableCollectionDto::class, [
            PlatformDtoNormalizer::SITE => $site,
            PlatformDtoNormalizer::USER => $user,
            AbstractRootNormalizer::FILES_PATH => $filesPath,
        ]);
    }

    /**
     * @template T
     *
     * @param class-string<T> $dtoClass
     *
     * @return T
     */
    private function deserialize(string $xml, string $dtoClass, array $context = [])
    {
        return $this->serializer->deserialize($xml, $dtoClass, CustomXmlEncoder::FORMAT, array_merge([
            XmlEncoder::AS_COLLECTION => true,
            XmlEncoder::ENCODING => 'UTF-8',
            DateTimeNormalizer::FORMAT_KEY => \DateTimeInterface::RFC3339_EXTENDED,
            AbstractXmlNormalizer::IMPORT_XML => true,
        ], $context));
    }
}
