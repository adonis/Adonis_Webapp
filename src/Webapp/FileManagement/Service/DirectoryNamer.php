<?php


namespace Webapp\FileManagement\Service;

use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

/**
 * Class DirectoryNamer.
 *
 * @template-implements DirectoryNamerInterface<ProjectFileInterface>
 */
class DirectoryNamer implements DirectoryNamerInterface
{
    /**
     * Returns the name of a directory where files will be uploaded
     *
     * Directory name is formed based on user ID and media type
     *
     * @param ProjectFileInterface $object
     * @param PropertyMapping|null $mapping
     *
     * @return string
     */
    public function directoryName($object, ?PropertyMapping $mapping = null): string
    {
        $userId = $object->getUser()->getUsername();
        return $userId . '/' . $object->getName() . ($object->getUniqDirectoryName() ?? '') . '/';
    }
}
