<?php

namespace Webapp\FileManagement\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;

/**
 * Class FileNamer.
 * Defines the way updloaded/generated files will be named
 *
 * @template-implements NamerInterface<ProjectFileInterface>
 */
class FileNamer implements NamerInterface
{
    /**
     * @param ProjectFileInterface $object
     */
    public function name($object, PropertyMapping $mapping): string
    {
        /* @var $file UploadedFile */
        $file = $mapping->getFile($object);
        $name = $file->getClientOriginalName();
        return $name;
    }
}
