<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

class ReferenceContext
{
    private string $reference;
    /** @var int[] */
    private array $index = [];

    public function __construct(string $reference, int $rootIndex = 0)
    {
        $this->reference = $reference;
        $this->index['null'] = $rootIndex;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function index(?string $type): int
    {
        $key = $type ?? 'null';
        if (!isset($this->index[$key])) {
            $this->index[$key] = 0;
        }

        return $this->index[$key]++;
    }
}
