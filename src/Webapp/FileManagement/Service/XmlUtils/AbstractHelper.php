<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

abstract class AbstractHelper
{
    public function createReference(string $parentReference, ?string $type = null, ?int $index = null): string
    {
        if (null === $type && null === $index) {
            throw new \LogicException('Type and/or index must be set');
        }

        $reference = $parentReference.'/';
        if (null !== $type) {
            $reference .= '@'.$type;
        }
        if (null !== $type && null !== $index) {
            $reference .= '.';
        }
        if (null !== $index) {
            $reference .= $index;
        }

        return $reference;
    }
}
