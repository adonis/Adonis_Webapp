<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

use Shared\Authentication\Entity\IdentifiedEntity;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;

final class ReaderHelper extends AbstractHelper implements ReaderHelperInterface
{
    /** @var array<string, IdentifiedEntity> */
    private array $map = [];
    /** @var array<int, string> */
    private array $uriMap = [];

    /**
     * @throws ParsingException
     */
    public function add(string $reference, IdentifiedEntity $object): void
    {
        if (isset($this->map[$reference])) {
            throw new ParsingException('', RequestFile::ERROR_DUPLICATED_URI);
        }

        $this->map[$reference] = $object;
        $this->uriMap[spl_object_id($object)] = $reference;
    }

    /**
     * @template T
     *
     * @param class-string<T> $class
     *
     * @return T
     *
     * @throws ParsingException
     */
    public function get(string $reference, string $class): object
    {
        $object = $this->getObject($reference);

        if (!$object instanceof $class) {
            throw new ParsingException(\sprintf('Wrong type for URI "%s"', $reference));
        }

        return $object;
    }

    /**
     * @throws ParsingException
     */
    public function getObject(string $reference): IdentifiedEntity
    {
        $reference = trim($reference);
        if (!isset($this->map[$reference])) {
            throw new ParsingException(\sprintf('URI "%s" not found', $reference));
        }

        return $this->map[$reference];
    }

    public function exists(string $reference): bool
    {
        return isset($this->map[$reference]);
    }

    /**
     * @throws ParsingException
     */
    public function getReference(IdentifiedEntity $object): string
    {
        $id = spl_object_id($object);
        if (!\array_key_exists($id, $this->uriMap)) {
            throw new ParsingException('Object not registered');
        }

        return $this->uriMap[$id];
    }
}
