<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer;

use Doctrine\Common\Collections\Collection;
use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\OezNature;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\Project;
use Webapp\Core\Entity\ProjectData;
use Webapp\FileManagement\Dto\Webapp\PlatformDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp\PlatformNormalizer;

/**
 * @template-extends AbstractRootNormalizer<PlatformDto, PlatformDtoXmlDto>
 *
 * @psalm-type PlatformDtoXmlDto =array{
 *      "adonis.modeleMetier.plateforme:NatureZhe": OezNature[],
 *      "adonis.modeleMetier.plateforme:Plateforme": ?Platform,
 *      "adonis.modeleMetier.projetDeSaisie:ProjetDeSaisie": Collection<int, Project>,
 *      "adonis.modeleMetier.saisieTerrain:Saisie": Collection<int, ProjectData>
 * }
 */
class PlatformDtoNormalizer extends AbstractRootNormalizer
{
    public const SITE = 'site';
    public const USER = 'user';

    protected const TAG_UTILISATEUR = 'adonis.modeleMetier.utilisateur:Utilisateur';
    protected const TAG_NATURE_ZHE = 'adonis.modeleMetier.plateforme:NatureZhe';
    protected const TAG_PLATEFORME = 'adonis.modeleMetier.plateforme:Plateforme';
    protected const TAG_SAISIE = 'adonis.modeleMetier.saisieTerrain:Saisie';
    protected const TAG_PROJET_DE_SAISIE = 'adonis.modeleMetier.projetDeSaisie:ProjetDeSaisie';

    protected function getClass(): string
    {
        return PlatformDto::class;
    }

    public function denormalize($data, string $type, ?string $format = null, array $context = []): PlatformDto
    {
        self::initDenormalizeContext($context);

        return parent::denormalize($data, $type, $format, $context);
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::TAG_NATURE_ZHE => XmlImportConfig::values(OezNature::class, true),
            self::TAG_PLATEFORME => XmlImportConfig::value(Platform::class, true),
            self::TAG_PROJET_DE_SAISIE => XmlImportConfig::collection(Project::class, true),
            self::TAG_SAISIE => XmlImportConfig::collection(ProjectData::class, true),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::TAG_PLATEFORME])) {
            throw new ParsingException('No platform found in the xml file');
        }
        if ($context[self::SITE]->getPlatforms()->exists(fn (int $key, Platform $existingPlatform) => $data[self::TAG_PLATEFORME][0][PlatformNormalizer::ATTR_NOM] === $existingPlatform->getName())) {
            throw new ParsingException('The name is already in use', RequestFile::ERROR_NAME_ALREADY_IN_USE);
        }
    }

    protected function generateImportedObject($data, array $context): PlatformDto
    {
        return new PlatformDto();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): PlatformDto
    {
        /** @var Platform $platform */
        $platform = $data[self::TAG_PLATEFORME];

        $platform->setProjects($data[self::TAG_PROJET_DE_SAISIE]);

        /** @var ProjectData[] $saisies */
        $saisies = $data[self::TAG_SAISIE] ?? [];
        foreach ($saisies as $saisie) {
            $project = $platform->getProjects()->filter(fn (Project $project) => spl_object_id($project) === spl_object_id($saisie->getProject()))->first();
            if (false === $project) {
                throw new ParsingException('Unable to find data entry project un platform.');
            }

            $saisie->setName($project->getName());
            $project->addProjectData($saisie);
        }

        $object->user = $context[self::USER];
        $object->platform = $platform;
        $object->oezNatures = $data[self::TAG_NATURE_ZHE] ?? [];
        $object->withData = true;

        return $object;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        self::initNormalizeContext($object->user, $context, 1); // Pass index 0 which correspond to AdonisVersion

        return parent::normalize($object, $format, $context);
    }

    protected function extractData($object, array $context): array
    {
        if (null === $object->user) {
            throw new \LogicException('$object->user must not be null');
        }
        if (null === $object->platform) {
            throw new \LogicException('$object->platform must not be null');
        }

        if ($object->withData) {
            $users = self::usersArrayUnique(array_merge([$object->user], $this->extractSessionsUsers($object->platform)));
        } else {
            $users = $object->user;
        }

        $data = [
            self::TAG_UTILISATEUR => new XmlExportConfig($users, false),
            self::TAG_NATURE_ZHE => new XmlExportConfig($object->platform->getSite()->getOezNatures(), false),
            self::TAG_PLATEFORME => new XmlExportConfig($object->platform, false),
        ];

        if ($object->withData) {
            $saisies = array_reduce(
                $object->platform->getProjects()->getValues(),
                fn (array $carry, Project $project) => array_merge($carry, $project->getProjectDatas()->getValues()),
                []
            );
            $data = array_merge($data, [
                self::TAG_PROJET_DE_SAISIE => new XmlExportConfig($object->platform->getProjects()->getValues(), false),
                self::TAG_SAISIE => new XmlExportConfig($saisies, false),
            ]);
        }

        return array_merge(parent::extractData($object, $context), $data);
    }

    /**
     * @return User[]
     */
    protected function extractSessionsUsers(Platform $object): array
    {
        $users = [];
        foreach ($object->getProjects() as $project) {
            foreach ($project->getProjectDatas() as $projectData) {
                foreach ($projectData->getSessions() as $session) {
                    $users[] = $session->getUser();
                }
            }
        }

        return $users;
    }

    /**
     * @param User[] $users
     *
     * @return User[]
     */
    private static function usersArrayUnique(array $users): array
    {
        $unique = [];
        foreach ($users as $user) {
            if (0 === \count(array_filter($unique, fn (User $existingUser) => $existingUser->getId() === $user->getId()))) {
                $unique[] = $user;
            }
        }

        return $unique;
    }
}
