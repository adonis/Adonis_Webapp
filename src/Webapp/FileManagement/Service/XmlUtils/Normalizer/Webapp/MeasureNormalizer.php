<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Enumeration\VariableTypeEnum;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Dto\Common\ReferencesDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Measure, array{}>
 */
class MeasureNormalizer extends AbstractXmlNormalizer
{
    private const VARIABLE_TYPE_MAP = [
        VariableTypeEnum::ALPHANUMERIC => 'adonis.modeleMetier.saisieTerrain:MesureVariableAlphanumerique',
        VariableTypeEnum::REAL => 'adonis.modeleMetier.saisieTerrain:MesureVariableReel',
        VariableTypeEnum::BOOLEAN => 'adonis.modeleMetier.saisieTerrain:MesureVariableBooleen',
        VariableTypeEnum::INTEGER => 'adonis.modeleMetier.saisieTerrain:MesureVariableEntiere',
        VariableTypeEnum::DATE => 'adonis.modeleMetier.saisieTerrain:MesureVariableDate',
        VariableTypeEnum::HOUR => 'adonis.modeleMetier.saisieTerrain:MesureVariableHeure',
    ];
    protected const ATTR_HORODATAGE = '@horodatage';
    protected const ATTR_CODE_ETAT = '@codeEtat';
    protected const ATTR_VALEUR = '@valeur';
    protected const ATTR_VARIABLE = '@variable';
    protected const ATTR_OBJET_METIER = '@objetMetier';
    protected const ATTR_INDICE_GENERATRICE = '@indiceGeneratrice';
    protected const ATTR_MESURE_GENERATRICE = '@mesureGeneratrice';
    protected const ATTR_MESURES_GENEREES = '@mesuresGenerees';

    protected function getClass(): string
    {
        return Measure::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function generateImportedObject($data, array $context): Measure
    {
        throw new \LogicException('Method not implemented');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Measure
    {
        throw new \LogicException('Method not implemented');
    }

    protected function extractData($object, array $context): array
    {
        $formField = $object->getFormField();
        $variable = $formField->getVariable();

        $valeur = null === $object->getState() ? $object->getValue() : null;
        if ($variable instanceof GeneratorVariable) {
            $xsiType = 'adonis.modeleMetier.saisieTerrain:MesureVariableEntiere';
        } else {
            $xsiType = self::VARIABLE_TYPE_MAP[$variable->getType()] ?? null;
            if (VariableTypeEnum::REAL === $variable->getType() && null === $object->getState() && null !== $variable->getFormat() && null !== $object->getValue()) {
                $valeur = number_format((float) $object->getValue(), (int) $variable->getFormat(), '.', '');
            }
        }

        if ($formField->getFieldGenerations()->count() > 0) {
            // In the generator variable case, only one measure is concerned by the formField
            $generatedMeasures = [];
            foreach ($formField->getFieldGenerations() as $generatedField) {
                foreach ($generatedField->getChildren() as $generatedFormField) {
                    $generatedMeasures = [...$generatedMeasures, ...$generatedFormField->getMeasures()->getValues()];
                }
            }
            $mesuresGenerees = new ReferencesDto($generatedMeasures);
        } else {
            $mesuresGenerees = null;
        }

        return [
            self::ATTR_HORODATAGE => $object->getTimestamp(),
            self::ATTR_CODE_ETAT => null !== $object->getState() ? new ReferenceDto($object->getState()) : null,
            self::ATTR_VALEUR => $valeur,
            self::ATTR_XSI_TYPE => $xsiType,
            self::ATTR_VARIABLE => new ReferenceDto($variable),
            self::ATTR_OBJET_METIER => null !== $formField->getTarget() ? new ReferenceDto($formField->getTarget()) : null,
            self::ATTR_INDICE_GENERATRICE => null !== $formField->getFieldParent() ? $formField->getFieldParent()->getIndex() : null,
            self::ATTR_MESURE_GENERATRICE => null !== $formField->getFieldParent() && \count($formField->getFieldParent()->getFormField()->getMeasures()) > 0 ? new ReferenceDto($formField->getFieldParent()->getFormField()->getMeasures()->first()) : null,
            self::ATTR_MESURES_GENEREES => $mesuresGenerees,
        ];
    }
}
