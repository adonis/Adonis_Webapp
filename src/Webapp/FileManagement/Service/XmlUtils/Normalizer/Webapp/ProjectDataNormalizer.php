<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\Device;
use Webapp\Core\Entity\Project;
use Webapp\Core\Entity\ProjectData;
use Webapp\Core\Entity\Session;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Dto\Webapp\VariableAndTestDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<ProjectData, ProjectDataXmlDto>
 *
 * @psalm-type ProjectDataXmlDto = array{
 *      "@projetDeSaisie": ?Project,
 *      materiel: Device[],
 *      sessions: Collection<int, Session>,
 *      variables: VariableAndTestDto[]
 * }
 */
class ProjectDataNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_PROJET_DE_SAISIE = '@projetDeSaisie';
    protected const ATTR_DATE_DEBUT = '@dateDebut';
    protected const ATTR_STATUS_SAISIE = '@statusSaisie';
    protected const ATTR_CHEMINEMENT_IMPOSE = '@cheminementImpose';
    protected const ATTR_PLATEFORME = '@plateforme';
    protected const TAG_VARIABLES = 'variables';
    protected const TAG_MATERIEL = 'materiel';
    protected const TAG_SESSIONS = 'sessions';

    protected function getClass(): string
    {
        return ProjectData::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_PROJET_DE_SAISIE => XmlImportConfig::reference(Project::class),
            self::TAG_MATERIEL => XmlImportConfig::values(Device::class),
            self::TAG_VARIABLES => XmlImportConfig::values(VariableAndTestDto::class),
            self::TAG_SESSIONS => XmlImportConfig::collection(Session::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_PROJET_DE_SAISIE])) {
            throw new ParsingException('', RequestFile::ERROR_DATA_ENTRY_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): ProjectData
    {
        return new ProjectData();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): ProjectData
    {
        $variables = VariableAndTestDtoNormalizer::getVariablesAndCreateTests($data[self::TAG_VARIABLES], $this->serializer, $format, $context);

        $object
            ->setUser($context[PlatformDtoNormalizer::USER])
            ->setProject($data[self::ATTR_PROJET_DE_SAISIE])
            ->setVariables($variables)
            ->setSessions($data[self::TAG_SESSIONS]);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        $devices = [];
        foreach ($object->getSemiAutomaticVariables() as $semiAutomaticVariable) {
            $device = $semiAutomaticVariable->getDevice();
            if (null !== $device && !\in_array($device, $devices, true)) {
                $devices[] = $device;
            }
        }

        return [
            self::ATTR_PROJET_DE_SAISIE => new ReferenceDto($object->getProject()),
            self::ATTR_DATE_DEBUT => $object->getStart(),
            self::ATTR_STATUS_SAISIE => 'terminee',
            self::ATTR_CHEMINEMENT_IMPOSE => 'false',
            self::ATTR_PLATEFORME => null !== $object->getProject()->getPlatform() ? new ReferenceDto($object->getProject()->getPlatform()) : null,
            self::TAG_VARIABLES => $object->getVariables(),
            self::TAG_MATERIEL => $devices,
            self::TAG_SESSIONS => $object->getSessions(),
        ];
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $overrideContext = self::overrideWriterHelper($context, [self::TAG_VARIABLES, self::TAG_MATERIEL]);

        $data = parent::normalize($object, $format, $overrideContext);
        \assert($object instanceof ProjectData);

        foreach ($object->getVariables() as $i => $variable) {
            $additionalDatas = $this->normalizeDataArray(AbstractVariableNormalizer::completeVariables($variable), $format, $overrideContext);
            $data[self::TAG_VARIABLES][$i] = array_merge($data[self::TAG_VARIABLES][$i], $additionalDatas);
        }

        return $data;
    }
}
