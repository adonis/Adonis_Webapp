<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Entity\Test;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template T of AbstractVariable
 * @template TImportDto of AbstractVariableXmlDto&array<array-key, mixed>
 *
 * @template-extends AbstractXmlNormalizer<T, TImportDto>
 *
 * @psalm-type AbstractVariableXmlDto = array{
 *      "@nom": string,
 *      "@nomCourt": string,
 *      "@dateCreation": \DateTime,
 *      "@dateDerniereModification": \DateTime,
 *      "@saisieObligatoire": ?bool,
 *      "@nbRepetitionSaisies": int,
 *      "@ordre": ?int,
 *      "@commentaire": ?string,
 *      "@unite": ?string,
 *      "@valeurDefaut": ?bool,
 *      "@typeVariable": ?string,
 *      "@uniteParcoursType": ?string,
 *      "tests": Collection<int, Test>,
 * }
 */
abstract class AbstractVariableNormalizer extends AbstractXmlNormalizer
{
    private const PATH_LEVEL_MAP = [
        PathLevelEnum::INDIVIDUAL => 'Individu',
        // Just do nothing, default type
        PathLevelEnum::UNIT_PLOT => null,
        PathLevelEnum::SURFACIC_UNIT_PLOT => null,
        PathLevelEnum::SUB_BLOCK => 'SousBloc',
        PathLevelEnum::BLOCK => 'Bloc',
        PathLevelEnum::EXPERIMENT => 'Dispositif',
        PathLevelEnum::PLATFORM => 'Plateforme',
    ];

    private const REVERSE_PATH_LEVEL_MAP = [
        'parcelle' => PathLevelEnum::UNIT_PLOT,
        'bloc' => PathLevelEnum::BLOCK,
        'sousBloc' => PathLevelEnum::SUB_BLOCK,
        'dispositif' => PathLevelEnum::EXPERIMENT,
        'plateforme' => PathLevelEnum::PLATFORM,
        'individu' => PathLevelEnum::INDIVIDUAL,
    ];

    protected const TYPE_ALPHANUMERIQUE = 'alphanumerique';
    protected const TYPE_REEL = 'reel';
    protected const TYPE_BOOLEEN = 'booleen';
    protected const TYPE_ENTIERE = 'entiere';
    protected const TYPE_DATE = 'date';
    protected const TYPE_HEURE = 'heure';

    protected const TYPE_MAP = [
        VariableTypeEnum::ALPHANUMERIC => self::TYPE_ALPHANUMERIQUE,
        VariableTypeEnum::BOOLEAN => self::TYPE_BOOLEEN,
        VariableTypeEnum::DATE => self::TYPE_DATE,
        VariableTypeEnum::HOUR => self::TYPE_HEURE,
        VariableTypeEnum::INTEGER => self::TYPE_ENTIERE,
        VariableTypeEnum::REAL => self::TYPE_REEL,
        '' => '',
    ];

    protected const REVERSE_TYPE_MAP = [
        self::TYPE_ALPHANUMERIQUE => VariableTypeEnum::ALPHANUMERIC,
        self::TYPE_REEL => VariableTypeEnum::REAL,
        self::TYPE_BOOLEEN => VariableTypeEnum::BOOLEAN,
        self::TYPE_ENTIERE => VariableTypeEnum::INTEGER,
        self::TYPE_DATE => VariableTypeEnum::DATE,
        self::TYPE_HEURE => VariableTypeEnum::HOUR,
    ];

    protected const ATTR_NOM = '@nom';
    protected const ATTR_NOM_COURT = '@nomCourt';
    protected const ATTR_NB_REPETITION_SAISIES = '@nbRepetitionSaisies';
    protected const ATTR_UNITE = '@unite';
    protected const ATTR_HORODATAGE = '@horodatage';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_DATE_DERNIERE_MODIFICATION = '@dateDerniereModification';
    protected const ATTR_COMMENTAIRE = '@commentaire';
    protected const ATTR_ORDRE = '@ordre';
    protected const ATTR_SAISIE_OBLIGATOIRE = '@saisieObligatoire';
    protected const ATTR_UNITE_PARCOURS_TYPE = '@uniteParcoursType';
    protected const ATTR_TYPE_VARIABLE = '@typeVariable';
    protected const ATTR_VALEUR_DEFAUT = '@valeurDefaut';
    protected const ATTR_MATERIEL = '@materiel';
    protected const TAG_TESTS = 'tests';

    protected function validateImportData($data, array $context): void
    {
        if (
            !isset($data[self::ATTR_NOM])
            || !isset($data[self::ATTR_NOM_COURT])
            || !isset($data[self::ATTR_NB_REPETITION_SAISIES])
            || !isset($data[self::ATTR_DATE_CREATION])
            || !isset($data[self::ATTR_DATE_DERNIERE_MODIFICATION])
        ) {
            throw new ParsingException('', RequestFile::ERROR_STATE_VARIABLE_INFO_MISSING);
        }
    }

    /**
     * @return array{
     *       "@nom": string|XmlImportConfig,
     *       "@nomCourt": string|XmlImportConfig,
     *       "@dateCreation": string|XmlImportConfig,
     *       "@dateDerniereModification": string|XmlImportConfig,
     *       "@saisieObligatoire": string|XmlImportConfig,
     *       "@nbRepetitionSaisies": string|XmlImportConfig,
     *       "@ordre": string|XmlImportConfig,
     *       "@commentaire": string|XmlImportConfig,
     *       "@unite": string|XmlImportConfig,
     *       "@valeurDefaut": string|XmlImportConfig,
     *       "@typeVariable": string|XmlImportConfig,
     *       "@uniteParcoursType": string|XmlImportConfig,
     *  }
     */
    protected function getCommonImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_NOM_COURT => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_DATE_DERNIERE_MODIFICATION => \DateTime::class,
            self::ATTR_SAISIE_OBLIGATOIRE => 'bool',
            self::ATTR_NB_REPETITION_SAISIES => 'int',
            self::ATTR_ORDRE => 'int',
            self::ATTR_COMMENTAIRE => 'string',
            self::ATTR_UNITE => 'string',
            self::ATTR_VALEUR_DEFAUT => 'bool',
            self::ATTR_TYPE_VARIABLE => 'string',
            self::ATTR_UNITE_PARCOURS_TYPE => 'string',
        ];
    }

    /**
     * @param T          $object
     * @param TImportDto $data
     *
     * @return T
     *
     * @psalm-suppress MoreSpecificImplementedParamType
     */
    protected function completeImportedObject($object, $data, ?string $format, array $context)
    {
        $object
            ->setCreated($data[self::ATTR_DATE_CREATION])
            ->setLastModified($data[self::ATTR_DATE_DERNIERE_MODIFICATION])
            ->setMandatory($data[self::ATTR_SAISIE_OBLIGATOIRE] ?? false)
            ->setName($data[self::ATTR_NOM])
            ->setShortName($data[self::ATTR_NOM_COURT])
            ->setRepetitions($data[self::ATTR_NB_REPETITION_SAISIES])
            ->setOrder($data[self::ATTR_ORDRE] ?? 0)
            ->setPathLevel(self::REVERSE_PATH_LEVEL_MAP[mb_strtolower($data[self::ATTR_UNITE_PARCOURS_TYPE] ?? 'parcelle')])
            ->setComment($data[self::ATTR_COMMENTAIRE])
            ->setDefaultTrueValue($data[self::ATTR_VALEUR_DEFAUT])
            ->setUnit($data[self::ATTR_UNITE]);

        return $object;
    }

    /**
     * @param T $object
     */
    protected function extractCommonData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_NOM_COURT => $object->getShortName(),
            self::ATTR_NB_REPETITION_SAISIES => $object->getRepetitions(),
            self::ATTR_UNITE => $object->getUnit(),
            self::ATTR_HORODATAGE => true,
            self::ATTR_DATE_CREATION => $object->getCreated(),
            self::ATTR_DATE_DERNIERE_MODIFICATION => $object->getLastModified(),
            self::ATTR_COMMENTAIRE => $object->getComment(),
            self::ATTR_ORDRE => $object->getOrder() ?? 1,
            self::ATTR_SAISIE_OBLIGATOIRE => $object->isMandatory(),
            self::ATTR_UNITE_PARCOURS_TYPE => self::PATH_LEVEL_MAP[$object->getPathLevel()] ?? null,
            self::ATTR_TYPE_VARIABLE => self::TYPE_MAP[$object->getType()] ?? $object->getType(),
            self::ATTR_VALEUR_DEFAUT => $object->getDefaultTrueValue(),
        ];
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable $variable
     */
    public static function completeVariables(AbstractVariable $variable): array
    {
        return [
            self::ATTR_MATERIEL => $variable instanceof SemiAutomaticVariable && null !== $variable->getDevice() ? new ReferenceDto($variable->getDevice()) : null,
            self::TAG_TESTS => $variable->getTests(),
        ];
    }
}
