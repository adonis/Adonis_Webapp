<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Platform;
use Webapp\FileManagement\Dto\Common\GraphicalStructureDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Platform, PlatformXmlDto>
 *
 * @psalm-type PlatformXmlDto = array{
 *      "@dateCreation": ?\DateTime,
 *      "@nom": ?string,
 *      "@nomLieu": ?string,
 *      "@nomSite": ?string,
 *      dispositifs: Collection<int, Experiment>,
 *      structureGraphique: ?GraphicalStructureDto
 * }
 */
class PlatformNormalizer extends AbstractXmlNormalizer
{
    public const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_NOM_SITE = '@nomSite';
    protected const ATTR_NOM_LIEU = '@nomLieu';
    protected const TAG_DISPOSITIFS = 'dispositifs';
    protected const TAG_STRUCTURE_GRAPHIQUE = 'structureGraphique';

    protected function getClass(): string
    {
        return Platform::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_NOM_SITE => 'string',
            self::ATTR_NOM_LIEU => 'string',
            self::TAG_DISPOSITIFS => XmlImportConfig::collection(Experiment::class),
            self::TAG_STRUCTURE_GRAPHIQUE => GraphicalStructureDto::class,
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM])
            || !isset($data[self::ATTR_NOM_SITE])
            || !isset($data[self::ATTR_NOM_LIEU])
            || !isset($data[self::ATTR_DATE_CREATION])
        ) {
            throw new ParsingException('', RequestFile::ERROR_PLATFORM_MISSING);
        }
        if (!isset($data[self::TAG_DISPOSITIFS])) {
            throw new ParsingException('', RequestFile::ERROR_NO_DEVICE);
        }
    }

    protected function generateImportedObject($data, array $context): Platform
    {
        return new Platform();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Platform
    {
        $object
            ->setSite($context[PlatformDtoNormalizer::SITE])
            ->setOwner($context[PlatformDtoNormalizer::USER])
            ->setName($data[self::ATTR_NOM])
            ->setCreated($data[self::ATTR_DATE_CREATION])
            ->setSiteName($data[self::ATTR_NOM_SITE])
            ->setPlaceName($data[self::ATTR_NOM_LIEU])
            ->setExperiments($data[self::TAG_DISPOSITIFS]);

        if ($data[self::TAG_STRUCTURE_GRAPHIQUE] instanceof GraphicalStructureDto) {
            $object
                ->setXMesh($data[self::TAG_STRUCTURE_GRAPHIQUE]->xMesh)
                ->setYMesh($data[self::TAG_STRUCTURE_GRAPHIQUE]->yMesh)
                ->setOrigin($data[self::TAG_STRUCTURE_GRAPHIQUE]->origin);
        }

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreated(),
            self::ATTR_NOM_SITE => $object->getSiteName(),
            self::ATTR_NOM_LIEU => $object->getPlaceName(),
            self::TAG_DISPOSITIFS => $object->getExperiments(),
        ];
    }
}
