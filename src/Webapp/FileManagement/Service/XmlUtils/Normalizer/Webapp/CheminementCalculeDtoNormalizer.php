<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use ApiPlatform\Core\Api\IriConverterInterface;
use Webapp\FileManagement\Dto\Webapp\CheminementCalculeDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<CheminementCalculeDto, CheminementCalculeDtoXmlDto>
 *
 * @psalm-type CheminementCalculeDtoXmlDto = array{
 *      "@declenchement": ?string,
 *      "@objects": ?string
 * }
 */
class CheminementCalculeDtoNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_DECLENCHEMENT = '@declenchement';
    protected const ATTR_OBJECTS = '@objects';
    private IriConverterInterface $iriConverter;

    public function __construct(IriConverterInterface $iriConverter)
    {
        $this->iriConverter = $iriConverter;
    }

    protected function getClass(): string
    {
        return CheminementCalculeDto::class;
    }

    protected function extractData($object, array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_DECLENCHEMENT => 'string',
            self::ATTR_OBJECTS => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): CheminementCalculeDto
    {
        return new CheminementCalculeDto();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): CheminementCalculeDto
    {
        $readerHelper = self::getReaderHelper($context);

        $object->declenchement = $data[self::ATTR_DECLENCHEMENT];
        $object->objects = array_map(fn (string $item) => $this->iriConverter->getIriFromItem($readerHelper->getObject($item)), explode(' ', $data[self::ATTR_OBJECTS] ?? ''));

        return $object;
    }
}
