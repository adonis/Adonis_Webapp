<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Webapp\Core\Entity\ValueListItem;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<ValueListItem, ValueListItemXmlDto>
 *
 * @psalm-type ValueListItemXmlDto = array{
 *      "#": ?string
 * }
 */
class ValueListItemNormalizer extends AbstractXmlNormalizer
{
    protected const VAL_NAME = '#';

    protected function getClass(): string
    {
        return ValueListItem::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::VAL_NAME => $object->getName(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::VAL_NAME => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): ValueListItem
    {
        return new ValueListItem('');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): ValueListItem
    {
        $object->setName($data[self::VAL_NAME]);

        return $object;
    }
}
