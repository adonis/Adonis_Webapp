<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\Treatment;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Protocol, ProtocolXmlDto>
 *
 * @psalm-type ProtocolXmlDto = array{
 *       "@dateCreation": \DateTime,
 *       "@nom": string,
 *       "@objectifs": ?string,
 *       facteurs: Collection<int, Factor>,
 *       traitements: Collection<int, Treatment>
 *  }
 */
class ProtocolNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_OBJECTIFS = '@objectifs';
    protected const ATTR_CREATEUR = '@createur';
    protected const ATTR_ALGORITHME_TIRAGE = '@algorithmeTirage';
    protected const TAG_FACTEURS = 'facteurs';
    protected const TAG_TRAITEMENTS = 'traitements';

    protected function getClass(): string
    {
        return Protocol::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_OBJECTIFS => 'string',
            self::TAG_FACTEURS => XmlImportConfig::collection(Factor::class),
            self::TAG_TRAITEMENTS => XmlImportConfig::collection(Treatment::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (
            !isset($data[self::ATTR_NOM])
            || !isset($data[self::ATTR_DATE_CREATION])
            || !isset($data[self::ATTR_CREATEUR])
            || !isset($data[self::ATTR_ALGORITHME_TIRAGE])
        ) {
            throw new ParsingException('', RequestFile::ERROR_PROTOCOL_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Protocol
    {
        return new Protocol();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Protocol
    {
        /** @var Collection<int, Factor> $factors */
        $factors = $data[self::TAG_FACTEURS] ?? new ArrayCollection();
        foreach ($factors as $i => $factor) {
            $factor->setOrder($i);
        }

        $object
            ->setSite($context[PlatformDtoNormalizer::SITE])
            ->setOwner($context[PlatformDtoNormalizer::USER])
            ->setName($data[self::ATTR_NOM])
            ->setCreated($data[self::ATTR_DATE_CREATION])
            ->setAim($data[self::ATTR_OBJECTIFS] ?? '')
            ->setFactors($factors)
            ->setTreatments($data[self::TAG_TRAITEMENTS]);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        $writerHelper = self::getWriterHelper($context);

        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreated(),
            self::ATTR_OBJECTIFS => $object->getAim(),
            self::ATTR_CREATEUR => null !== $writerHelper->getConnectedUser() ? new ReferenceDto($writerHelper->getConnectedUser()) : null,
            self::ATTR_ALGORITHME_TIRAGE => null !== $object->getAlgorithm() ? $object->getAlgorithm()->getName() : 'Sans Tirage',
            self::TAG_FACTEURS => $object->getFactors(),
            self::TAG_TRAITEMENTS => $object->getTreatments(),
        ];
    }
}
