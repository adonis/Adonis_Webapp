<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\VariableScale;
use Webapp\Core\Entity\VariableScaleItem;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<VariableScale, VariableScaleDto>
 *
 * @psalm-type VariableScaleDto = array{
 *     "@nom": string,
 *     "@exclusif": ?bool,
 *     "@valeurMin": ?int,
 *     "@valeurMax": ?int,
 *     "notations": Collection<array-key, VariableScaleItem>
 * }
 */
class VariableScaleNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_VALEUR_MIN = '@valeurMin';
    protected const ATTR_VALEUR_MAX = '@valeurMax';
    protected const ATTR_EXCLUSIF = '@exclusif';
    protected const TAG_NOTATIONS = 'notations';

    protected function getClass(): string
    {
        return VariableScale::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_EXCLUSIF => 'bool',
            self::ATTR_VALEUR_MIN => 'string',
            self::ATTR_VALEUR_MAX => 'string',
            self::TAG_NOTATIONS => XmlImportConfig::collection(VariableScaleItem::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM])) {
            throw new ParsingException('', RequestFile::ERROR_SCALE_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): VariableScale
    {
        return new VariableScale();
    }

    /**
     * @param VariableScale    $object
     * @param VariableScaleDto $data
     */
    protected function completeImportedObject($object, $data, ?string $format, array $context): VariableScale
    {
        $object
            ->setName($data[self::ATTR_NOM])
            ->setOpen(!($data[self::ATTR_EXCLUSIF] ?? false))
            ->setMinValue($data[self::ATTR_VALEUR_MIN] ?? 0)
            ->setMaxValue($data[self::ATTR_VALEUR_MAX] ?? 0)
            ->setValues($data[self::TAG_NOTATIONS]);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_VALEUR_MIN => $object->getMinValue(),
            self::ATTR_VALEUR_MAX => $object->getMaxValue(),
            self::ATTR_EXCLUSIF => !$object->isOpen(),
            self::TAG_NOTATIONS => $object->getValues()->getValues(),
        ];
    }
}
