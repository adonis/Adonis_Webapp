<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Webapp\Core\Entity\VariableScaleItem;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractRootNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<VariableScaleItem, VariableScaleItemDto>
 *
 * @psalm-type VariableScaleItemDto = array{
 *     "@image": ?string,
 *     "@texte": ?string,
 *     "@valeur": ?int
 * }
 */
class VariableScaleItemNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_TEXTE = '@texte';
    protected const ATTR_VALEUR = '@valeur';
    protected const ATTR_IMAGE = '@image';

    protected function getClass(): string
    {
        return VariableScaleItem::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_TEXTE => $object->getText(),
            self::ATTR_VALEUR => $object->getValue(),
            self::ATTR_IMAGE => $this->createPicture($object->getPic(), $context),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_TEXTE => 'string',
            self::ATTR_VALEUR => 'int',
            self::ATTR_IMAGE => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (($data[self::ATTR_IMAGE] ?? '') !== '' && !file_exists($this->getPicturePath($data[self::ATTR_IMAGE], $context))) {
            throw new ParsingException('', RequestFile::ERROR_FILE_NOT_FOUND);
        }
    }

    protected function generateImportedObject($data, array $context): VariableScaleItem
    {
        return new VariableScaleItem(0);
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): VariableScaleItem
    {
        $object
            ->setText($data[self::ATTR_TEXTE] ?? '')
            ->setValue($data[self::ATTR_VALEUR] ?? 0)
            ->setPic(null !== $data[self::ATTR_IMAGE] ? $this->getPicture($data[self::ATTR_IMAGE], $context) : null);

        return $object;
    }

    /**
     * Get picture path in working path.
     */
    private function getPicturePath(string $pictureFile, array $context): string
    {
        return $this->getWorkspacePath($context).\DIRECTORY_SEPARATOR.$pictureFile;
    }

    private function getWorkspacePath(array $context): string
    {
        $path = $context[AbstractRootNormalizer::FILES_PATH] ?? null;
        if (null === $path) {
            throw new \LogicException('Project path is not defined in context.');
        }

        return $path;
    }

    /**
     * Convert picture file to base 64 URL.
     */
    private function getPicture(string $pictureFile, array $context): string
    {
        $path = $this->getPicturePath($pictureFile, $context);
        $type = pathinfo($path, \PATHINFO_EXTENSION);
        $data = file_get_contents($path);

        return 'data:image/'.$type.';base64,'.base64_encode($data);
    }

    private function createPicture(?string $content, array $context): ?string
    {
        $matches = [];
        if (preg_match('~data:(.*);base64,(.*)~', $content ?? '', $matches)) {
            $data = $matches[2];
            $path = $this->getPicturePath(uniqid(), $context);

            file_put_contents($path, base64_decode($data, true));

            return self::getWriterHelper($context)->addMarkFile($path);
        }

        return null;
    }
}
