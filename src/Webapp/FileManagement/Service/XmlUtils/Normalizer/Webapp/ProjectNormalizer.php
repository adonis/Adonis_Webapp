<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\Device;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\PathBase;
use Webapp\Core\Entity\Project;
use Webapp\Core\Entity\RequiredAnnotation;
use Webapp\Core\Entity\StateCode;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Dto\Common\ReferencesDto;
use Webapp\FileManagement\Dto\Webapp\VariableAndTestDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Project, ProjectXmlDto>
 *
 * @psalm-type ProjectXmlDto = array{
 *      "@dateCreation": \DateTime,
 *      "@dispositifs": Collection<int, Experiment>,
 *      "@nom": string,
 *      codesEtats: Collection<int, StateCode>,
 *      materiel: Collection<int, Device>,
 *      metadonneesASaisir: Collection<int, RequiredAnnotation>,
 *      sectionCheminements: ?PathBase,
 *      variables: VariableAndTestDto[]
 * }
 */
class ProjectNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_CREATEUR = '@createur';
    protected const ATTR_PLATEFORME = '@plateforme';
    protected const ATTR_DISPOSITIFS = '@dispositifs';
    protected const ATTR_ETAT = '@etat';
    protected const ATTR_EXPERIMENTATEURS = '@experimentateurs';
    protected const TAG_VARIABLES = 'variables';
    protected const TAG_MATERIEL = 'materiel';
    protected const TAG_CODES_ETATS = 'codesEtats';
    protected const TAG_METADONNEES = 'metadonneesASaisir';
    protected const TAG_SECTION_CHEMINEMENTS = 'sectionCheminements';

    protected function getClass(): string
    {
        return Project::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::TAG_SECTION_CHEMINEMENTS => XmlImportConfig::value(PathBase::class),
            self::ATTR_DISPOSITIFS => XmlImportConfig::references(Experiment::class),
            self::TAG_CODES_ETATS => XmlImportConfig::collection(StateCode::class),
            self::TAG_MATERIEL => XmlImportConfig::collection(Device::class),
            self::TAG_VARIABLES => XmlImportConfig::values(VariableAndTestDto::class),
            self::TAG_METADONNEES => XmlImportConfig::collection(RequiredAnnotation::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM])
            || !isset($data[self::ATTR_DATE_CREATION])
        ) {
            throw new ParsingException('', RequestFile::ERROR_DATA_ENTRY_PROJECT_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Project
    {
        return new Project();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Project
    {
        $variables = VariableAndTestDtoNormalizer::getVariablesAndCreateTests($data[self::TAG_VARIABLES], $this->serializer, $format, $context);

        $object
            ->setOwner($context[PlatformDtoNormalizer::USER])
            ->setName($data[self::ATTR_NOM])
            ->setCreated($data[self::ATTR_DATE_CREATION])
            ->setPathBase($data[self::TAG_SECTION_CHEMINEMENTS])
            ->setExperiments($data[self::ATTR_DISPOSITIFS])
            ->setStateCodes($data[self::TAG_CODES_ETATS])
            ->setDevices($data[self::TAG_MATERIEL])
            ->setVariables($variables)
            ->setRequiredAnnotations($data[self::TAG_METADONNEES]);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        $writerHelper = self::getWriterHelper($context);
        $user = $writerHelper->getConnectedUser();

        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreated(),
            self::ATTR_CREATEUR => null !== $user ? new ReferenceDto($user) : null,
            self::ATTR_PLATEFORME => null !== $object->getPlatform() ? new ReferenceDto($object->getPlatform()) : null,
            self::ATTR_DISPOSITIFS => new ReferencesDto($object->getExperiments()),
            self::ATTR_ETAT => 'transferer',
            self::ATTR_EXPERIMENTATEURS => null !== $user ? new ReferenceDto($user) : null,
            self::TAG_VARIABLES => $object->getVariables(),
            self::TAG_MATERIEL => $object->getDevices(),
            self::TAG_CODES_ETATS => $object->getStateCodes(),
        ];
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $data = parent::normalize($object, $format, $context);
        \assert($object instanceof Project);

        foreach ($object->getVariables() as $i => $variable) {
            $additionalDatas = $this->normalizeDataArray(AbstractVariableNormalizer::completeVariables($variable), $format, $context);
            $data[self::TAG_VARIABLES][$i] = array_merge($data[self::TAG_VARIABLES][$i], $additionalDatas);
        }

        return $data;
    }
}
