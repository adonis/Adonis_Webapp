<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Webapp\Core\Entity\PathLevelAlgorithm;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\PossibleMoveEnum;
use Webapp\Core\Enumeration\PossibleStartPointEnum;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<PathLevelAlgorithm, PathLevelAlgorithmXmlDto>
 *
 * @psalm-type PathLevelAlgorithmXmlDto = array{
 *      "@niveau": ?string,
 *      "@sensDepart": ?string,
 *      "@sensDeplacement": ?string
 * }
 */
class PathLevelArlgorithmNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NIVEAU = '@niveau';
    protected const ATTR_SENS_DEPLACEMENT = '@sensDeplacement';
    protected const ATTR_SENS_DEPART = '@sensDepart';

    protected const PATH_LEVEL_PARCELLE = 'parcelle';
    protected const PATH_LEVEL_BLOC = 'bloc';
    protected const PATH_LEVEL_SOUS_BLOC = 'sousBloc';
    protected const PATH_LEVEL_DISPOSITIF = 'dispositif';
    protected const PATH_LEVEL_PLATEFORME = 'plateforme';
    protected const PATH_LEVEL_INDIVIDU = 'individu';

    protected const REVERSE_PATH_LEVEL_MAP = [
        self::PATH_LEVEL_PARCELLE => PathLevelEnum::UNIT_PLOT,
        self::PATH_LEVEL_BLOC => PathLevelEnum::BLOCK,
        self::PATH_LEVEL_SOUS_BLOC => PathLevelEnum::SUB_BLOCK,
        self::PATH_LEVEL_DISPOSITIF => PathLevelEnum::EXPERIMENT,
        self::PATH_LEVEL_PLATEFORME => PathLevelEnum::PLATFORM,
        self::PATH_LEVEL_INDIVIDU => PathLevelEnum::INDIVIDUAL,
    ];

    protected const MOVE_ALLER_SIMPLE = 'allerSimple';
    protected const MOVE_ALLER_RETOUR = 'allerRetour';
    protected const MOVE_SERPENTIN = 'serpentin';
    protected const MOVE_DEMI_SERPENTIN = 'demiSerpentin';
    protected const MOVE_GRAPHIQUE = 'graphique';
    protected const MOVE_ORDONNANCEUR = 'ordonnanceur';

    protected const REVERESE_MOVE_MAP = [
        self::MOVE_ALLER_SIMPLE => PossibleMoveEnum::ALLER_SIMPLE,
        self::MOVE_ALLER_RETOUR => PossibleMoveEnum::ALLER_RETOUR,
        self::MOVE_SERPENTIN => PossibleMoveEnum::SERPENTIN,
        self::MOVE_DEMI_SERPENTIN => PossibleMoveEnum::DEMI_SERPENTIN,
        self::MOVE_GRAPHIQUE => PossibleMoveEnum::GRAPHIQUE,
        self::MOVE_ORDONNANCEUR => PossibleMoveEnum::ORDONNANCEUR,
    ];

    protected const SENS_BAS_GAUCHE = 'basGauche';
    protected const SENS_BAS_DROITE = 'basDroite';
    protected const SENS_HAUT_GAUCHE = 'hautGauche';
    protected const SENS_HAUT_DROITE = 'hautDroite';
    protected const START_POINT_GAUCHE_DROITE = 'gaucheDroite';
    protected const START_POINT_BAS_HAUT = 'basHaut';
    protected const START_POINT_DROITE_GAUCHE = 'droiteGauche';
    protected const START_POINT_HAUT_BAS = 'hautBas';

    protected function getClass(): string
    {
        return PathLevelAlgorithm::class;
    }

    protected function extractData($object, array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NIVEAU => 'string',
            self::ATTR_SENS_DEPLACEMENT => 'string',
            self::ATTR_SENS_DEPART => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): PathLevelAlgorithm
    {
        return new PathLevelAlgorithm(PossibleMoveEnum::LIBRE, PathLevelEnum::UNIT_PLOT);
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): PathLevelAlgorithm
    {
        $object
            ->setPathLevel(self::REVERSE_PATH_LEVEL_MAP[mb_strtolower($data[self::ATTR_NIVEAU] ?? self::PATH_LEVEL_PARCELLE)])
            ->setMove(self::REVERESE_MOVE_MAP[$data[self::ATTR_SENS_DEPLACEMENT] ?? ''] ?? PossibleMoveEnum::LIBRE)
            ->setStartPoint($this->getStartPoint($data));

        return $object;
    }

    protected function getStartPoint(array $data): ?string
    {
        $startPoint = null;
        switch ($data[self::ATTR_SENS_DEPLACEMENT]) {
            case self::SENS_BAS_GAUCHE:
                switch ($data[self::ATTR_SENS_DEPART]) {
                    case self::START_POINT_GAUCHE_DROITE:
                        $startPoint = PossibleStartPointEnum::BOT_LFT_DRGT;
                        break;
                    case self::START_POINT_BAS_HAUT:
                        $startPoint = PossibleStartPointEnum::BOT_LFT_DTOP;
                        break;
                }
                break;

            case self::SENS_BAS_DROITE:
                switch ($data[self::ATTR_SENS_DEPART]) {
                    case self::START_POINT_DROITE_GAUCHE:
                        $startPoint = PossibleStartPointEnum::BOT_RGT_DLFT;
                        break;
                    case self::START_POINT_BAS_HAUT:
                        $startPoint = PossibleStartPointEnum::BOT_RGT_DTOP;
                        break;
                }
                break;

            case self::SENS_HAUT_GAUCHE:
                switch ($data[self::ATTR_SENS_DEPART]) {
                    case self::START_POINT_GAUCHE_DROITE:
                        $startPoint = PossibleStartPointEnum::TOP_LFT_DRGT;
                        break;
                    case self::START_POINT_HAUT_BAS:
                        $startPoint = PossibleStartPointEnum::TOP_LFT_DBOT;
                        break;
                }
                break;

            case self::SENS_HAUT_DROITE:
                switch ($data[self::ATTR_SENS_DEPART]) {
                    case self::START_POINT_DROITE_GAUCHE:
                        $startPoint = PossibleStartPointEnum::TOP_RGT_DLFT;
                        break;
                    case self::START_POINT_HAUT_BAS:
                        $startPoint = PossibleStartPointEnum::TOP_RGT_DBOT;
                        break;
                }
                break;
        }

        return $startPoint;
    }
}
