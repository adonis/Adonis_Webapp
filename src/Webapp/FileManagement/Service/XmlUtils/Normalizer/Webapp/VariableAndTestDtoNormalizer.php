<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\Test;
use Webapp\FileManagement\Dto\Webapp\VariableAndTestDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;
use Webapp\FileManagement\Service\XmlUtils\ReferenceContext;

/**
 * @template-extends AbstractXmlNormalizer<array, VariableAndTestDto>
 *
 * @psalm-import-type VariableType from AbstractVariable
 */
class VariableAndTestDtoNormalizer implements ContextAwareDenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /**
     * @param mixed $data
     */
    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return VariableAndTestDto::class === $type;
    }

    /**
     * @param mixed $data
     *
     * @throws ExceptionInterface
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): VariableAndTestDto
    {
        \assert($this->serializer instanceof Serializer);

        $object = new VariableAndTestDto();
        $object->variable = $this->serializer->denormalize($data, AbstractVariable::class, $format, $context);
        $object->testDtos = $data['tests'] ?? [];

        return $object;
    }

    /**
     * @param VariableAndTestDto[] $data
     *
     * @return VariableType[]
     *
     * @throws ExceptionInterface
     */
    public static function getVariablesAndCreateTests(array $data, Serializer $serializer, ?string $format = null, array $context = []): array
    {
        $readerHelper = AbstractXmlNormalizer::getReaderHelper($context);

        $variables = [];
        foreach ($data as $datum) {
            if (null === $datum->variable) {
                throw new \LogicException('Variable $datum->variable is null');
            }

            $variableReference = $readerHelper->getReference($datum->variable);
            $variableContext = array_merge([], $context, [AbstractXmlNormalizer::REFERENCE => new ReferenceContext($variableReference)]);

            $variables[] = $datum->variable;
            foreach ($datum->testDtos as $i => $item) {
                $childrenContext = AbstractXmlNormalizer::childrenReaderContext($variableContext, $data, XmlImportConfig::value(Test::class), 'tests', $i);
                $test = $serializer->denormalize($item, Test::class, $format, $childrenContext);

                $datum->variable->addTest($test);
            }
        }

        return $variables;
    }
}
