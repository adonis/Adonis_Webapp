<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Entity\Test;
use Webapp\Core\Entity\ValueList;
use Webapp\Core\Entity\VariableScale;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractVariableNormalizer<SimpleVariable, SimpleVariableXmlDto>
 *
 * @psalm-type SimpleVariableXmlDto = array{
 *       "@nom": string,
 *       "@nomCourt": string,
 *       "@dateCreation": \DateTime,
 *       "@dateDerniereModification": \DateTime,
 *       "@saisieObligatoire": ?bool,
 *       "@nbRepetitionSaisies": int,
 *       "@ordre": ?int,
 *       "@commentaire": ?string,
 *       "@unite": ?string,
 *       "@valeurDefaut": ?bool,
 *       "@typeVariable": ?string,
 *       "@uniteParcoursType": ?string,
 *       "tests": Collection<int, Test>,
 *      echelle: ?VariableScale,
 *      listevaleur: ?ValueList,
 * }
 */
class SimpleVariableNormalizer extends AbstractVariableNormalizer
{
    protected const TAG_ECHELLE = 'echelle';
    protected const TAG_LISTE_VALEUR = 'listevaleur';

    protected function getClass(): string
    {
        return SimpleVariable::class;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        if (parent::supportsDenormalization($data, $type, $format, $context)) {
            return true;
        } elseif (AbstractVariable::class !== $type) {
            return false;
        }

        // Manage target $type AbstractVariable
        if (isset($data[GeneratorVariableNormalizer::ATTR_NOM_GENERE])) {
            // It is a generator variable.
            return false;
        }

        if (($data[self::ATTR_XSI_TYPE] ?? '') === SemiAutomaticVariableNormalizer::XSI_TYPE_VARIABLE_SEMI_AUTO || isset($data[SemiAutomaticVariableNormalizer::ATTR_MATERIEL])) {
            // It is a semi automatic variable.
            return false;
        }

        return true;
    }

    protected function getImportDataConfig(array $context): array
    {
        return array_merge(parent::getCommonImportDataConfig($context), [
            self::TAG_LISTE_VALEUR => XmlImportConfig::value(ValueList::class),
            self::TAG_ECHELLE => XmlImportConfig::value(VariableScale::class),
        ]);
    }

    protected function generateImportedObject($data, array $context): SimpleVariable
    {
        return new SimpleVariable();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): SimpleVariable
    {
        $object = parent::completeImportedObject($object, $data, $format, $context);
        $object
            ->setType(self::REVERSE_TYPE_MAP[$data[self::ATTR_TYPE_VARIABLE] ?? self::TYPE_ALPHANUMERIQUE])
            ->setValueList($data[self::TAG_LISTE_VALEUR])
            ->setScale($data[self::TAG_ECHELLE]);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        $data = parent::extractCommonData($object, $context);
        $data[self::TAG_LISTE_VALEUR] = $object->getValueList();
        $data[self::TAG_ECHELLE] = $object->getScale();

        return $data;
    }
}
