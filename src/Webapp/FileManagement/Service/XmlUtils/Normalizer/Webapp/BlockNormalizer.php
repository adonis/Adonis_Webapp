<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Note;
use Webapp\Core\Entity\OutExperimentationZone;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\UnitPlot;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Block, BlockXmlDto>
 *
 * @psalm-type BlockXmlDto = array{
 *      "@numero": ?string,
 *      notes: Collection<int, Note>,
 *      parcellesUnitaire: array<int, UnitPlot|SurfacicUnitPlot>,
 *      sousBlocs: Collection<int, SubBlock>,
 *      zheAvecEpaisseurs: Collection<int, OutExperimentationZone>
 * }
 */
class BlockNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NUMERO = '@numero';
    protected const TAG_SOUS_BLOCS = 'sousBlocs';
    protected const TAG_PARCELLES_UNITAIRE = 'parcellesUnitaire';
    protected const TAG_ZHE_AVEC_EPAISSEURS = 'zheAvecEpaisseurs';
    protected const TAG_NOTES = 'notes';

    protected function getClass(): string
    {
        return Block::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NUMERO => 'string',
            self::TAG_ZHE_AVEC_EPAISSEURS => XmlImportConfig::collection(OutExperimentationZone::class),
            self::TAG_PARCELLES_UNITAIRE => XmlImportConfig::values(UnitPlot::class),
            self::TAG_SOUS_BLOCS => XmlImportConfig::collection(SubBlock::class),
            self::TAG_NOTES => XmlImportConfig::collection(Note::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::TAG_PARCELLES_UNITAIRE])
            && !isset($data[self::TAG_SOUS_BLOCS])) {
            throw new ParsingException('', RequestFile::ERROR_NO_BLOC_CHILDREN);
        }
    }

    protected function generateImportedObject($data, array $context): Block
    {
        return new Block();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Block
    {
        $object
            ->setNumber($data[self::ATTR_NUMERO] ?? '0')
            ->setOutExperimentationZones($data[self::TAG_ZHE_AVEC_EPAISSEURS])
            ->setNotes($data[self::TAG_NOTES]);

        if (\count($data[self::TAG_PARCELLES_UNITAIRE]) > 0) {
            foreach ($data[self::TAG_PARCELLES_UNITAIRE] as $item) {
                if ($item instanceof UnitPlot) {
                    $object->addUnitPlots($item);
                } else {
                    $object->addSurfacicUnitPlots($item);
                }
            }
        } else {
            $object->setSubBlocks($data[self::TAG_SOUS_BLOCS]);
        }

        return $object;
    }

    public function extractData($object, array $context): array
    {
        return [
            self::ATTR_NUMERO => $object->getNumber(),
            self::TAG_SOUS_BLOCS => $object->getSubBlocks(),
            self::TAG_PARCELLES_UNITAIRE => array_merge($object->getUnitPlots()->getValues(), $object->getSurfacicUnitPlots()->getValues()),
            self::TAG_ZHE_AVEC_EPAISSEURS => $object->getOutExperimentationZones(),
            self::TAG_NOTES => $object->getNotes(),
        ];
    }
}
