<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Webapp\Core\Enumeration\CommunicationProtocolEnum;
use Webapp\FileManagement\Dto\Webapp\DeviceDriverDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<DeviceDriverDto, DeviceDriverDtoXmlDto>
 *
 * @psalm-type DeviceDriverDtoXmlDto = array{
 *      "@baudrate": ?int,
 *      "@databitsFormat": ?int,
 *      "@debutTrame": ?string,
 *      "@finTrame": ?string,
 *      "@flowcontrol": ?string,
 *      "@parity": ?string,
 *      "@push": ?bool,
 *      "@separateurCsv": ?string,
 *      "@stopbit": ?float,
 *      "@tailleTrame": ?int,
 *      "@type": ?string,
 *      "@xsi:type": ?string
 * }
 */
class DeviceDriverDtoNormalizer extends AbstractXmlNormalizer
{
    protected const XSI_TYPE_DRIVER_COM = 'adonis.modeleMetier.projetDeSaisie.variables:DriverCom';

    protected const ATTR_TYPE = '@type';
    protected const ATTR_BAUDRATE = '@baudrate';
    protected const ATTR_STOPBIT = '@stopbit';
    protected const ATTR_PUSH = '@push';
    protected const ATTR_PARITY = '@parity';
    protected const ATTR_FLOWCONTROL = '@flowcontrol';
    protected const ATTR_DATABITS_FORMAT = '@databitsFormat';
    protected const ATTR_DEBUT_TRAME = '@debutTrame';
    protected const ATTR_FIN_TRAME = '@finTrame';
    protected const ATTR_SEPARATEUR_CSV = '@separateurCsv';
    protected const ATTR_TAILLE_TRAME = '@tailleTrame';

    protected function getClass(): string
    {
        return DeviceDriverDto::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_XSI_TYPE => 'string',
            self::ATTR_TYPE => 'string',
            self::ATTR_BAUDRATE => 'int',
            self::ATTR_STOPBIT => 'float',
            self::ATTR_PARITY => 'string',
            self::ATTR_FLOWCONTROL => 'string',
            self::ATTR_PUSH => 'bool',
            self::ATTR_DATABITS_FORMAT => 'int',
            self::ATTR_TAILLE_TRAME => 'int',
            self::ATTR_DEBUT_TRAME => 'string',
            self::ATTR_FIN_TRAME => 'string',
            self::ATTR_SEPARATEUR_CSV => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): DeviceDriverDto
    {
        return new DeviceDriverDto();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): DeviceDriverDto
    {
        if (self::XSI_TYPE_DRIVER_COM === $data[self::ATTR_XSI_TYPE]) {
            $object->type = CommunicationProtocolEnum::RS232;
            $object->baudrate = $data[self::ATTR_BAUDRATE];
            $object->stopbit = $data[self::ATTR_STOPBIT];
            $object->parity = $data[self::ATTR_PARITY];
            $object->flowcontrol = $data[self::ATTR_FLOWCONTROL];
            $object->push = $data[self::ATTR_PUSH];
            $object->databitsFormat = $data[self::ATTR_DATABITS_FORMAT];
        } elseif ('bluetooth' === $data[self::ATTR_TYPE]) {
            $object->type = CommunicationProtocolEnum::BLUETOOTH;
        } elseif ('usb' === $data[self::ATTR_TYPE]) {
            $object->type = CommunicationProtocolEnum::USB;
        }

        $object->tailleTrame = $data[self::ATTR_TAILLE_TRAME] ?? 0;
        $object->debutTrame = $data[self::ATTR_DEBUT_TRAME];
        $object->finTrame = $data[self::ATTR_FIN_TRAME];
        $object->separateurCsv = $data[self::ATTR_SEPARATEUR_CSV];

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        if (CommunicationProtocolEnum::RS232 === $object->type) {
            $driverConfig = [
                self::ATTR_XSI_TYPE => self::XSI_TYPE_DRIVER_COM,
                self::ATTR_BAUDRATE => $object->baudrate,
                self::ATTR_STOPBIT => $object->stopbit,
                self::ATTR_PUSH => $object->push,
                self::ATTR_PARITY => $object->parity,
                self::ATTR_FLOWCONTROL => $object->flowcontrol,
                self::ATTR_DATABITS_FORMAT => $object->databitsFormat,
            ];
        } else {
            $driverConfig = [self::ATTR_TYPE => $object->type];
        }

        return array_merge([
            self::ATTR_DEBUT_TRAME => $object->debutTrame,
            self::ATTR_FIN_TRAME => $object->finTrame,
            self::ATTR_SEPARATEUR_CSV => $object->separateurCsv,
            self::ATTR_TAILLE_TRAME => $object->tailleTrame,
        ], $driverConfig);
    }
}
