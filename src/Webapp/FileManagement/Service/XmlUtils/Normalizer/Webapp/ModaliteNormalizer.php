<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Webapp\Core\Entity\Modality;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Modality, ModalityXmlDto>
 *
 * @psalm-type ModalityXmlDto = array{
 *      "@identifiant": ?string,
 *      "@nomCourt": ?string,
 *      "@valeur": ?string
 * }
 */
class ModaliteNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_VALEUR = '@valeur';
    protected const ATTR_IDENTIFIANT = '@identifiant';
    protected const ATTR_NOM_COURT = '@nomCourt';

    protected function getClass(): string
    {
        return Modality::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_IDENTIFIANT => 'string',
            self::ATTR_NOM_COURT => 'string',
            self::ATTR_VALEUR => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_VALEUR])) {
            throw new ParsingException('', RequestFile::ERROR_PROTOCOL_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Modality
    {
        return new Modality();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Modality
    {
        $object
            ->setIdentifier($data[self::ATTR_IDENTIFIANT])
            ->setShortName($data[self::ATTR_NOM_COURT])
            ->setValue($data[self::ATTR_VALEUR]);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_VALEUR => $object->getValue(),
        ];
    }
}
