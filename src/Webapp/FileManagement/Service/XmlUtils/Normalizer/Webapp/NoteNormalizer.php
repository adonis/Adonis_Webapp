<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Shared\Authentication\Repository\UserRepository;
use Webapp\Core\Entity\Note;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Note, NoteXmlDto>
 *
 * @psalm-type NoteXmlDto = array{
 *      "@dateCreation": ?\DateTime,
 *      "@loginCreateur": ?string,
 *      "@supprime": ?bool,
 *      "@texte": ?string
 * }
 */
class NoteNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_TEXTE = '@texte';
    protected const ATTR_ONLINE_USER_URI = '@onlineUserUri';
    protected const ATTR_LOGIN_CREATEUR = '@loginCreateur';
    protected const ATTR_SUPPRIME = '@supprime';

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    protected function getClass(): string
    {
        return Note::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_SUPPRIME => 'bool',
            self::ATTR_TEXTE => 'string',
            self::ATTR_LOGIN_CREATEUR => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_LOGIN_CREATEUR]) || null === $this->userRepository->findOneBy(['username' => $data[self::ATTR_LOGIN_CREATEUR]])) {
            throw new ParsingException('', RequestFile::ERROR_NOTE_CREATOR_NOT_FOUND);
        }
    }

    protected function generateImportedObject($data, array $context): Note
    {
        return new Note();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Note
    {
        $user = $this->userRepository->findOneBy(['username' => $data[self::ATTR_LOGIN_CREATEUR]]);
        if (null === $user) {
            throw new ParsingException('Unable to find user');
        }

        $object
            ->setCreator($user)
            ->setCreationDate($data[self::ATTR_DATE_CREATION])
            ->setText($data[self::ATTR_TEXTE])
            ->setDeleted($data[self::ATTR_SUPPRIME] ?? false);

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_DATE_CREATION => $object->getCreationDate(),
            self::ATTR_TEXTE => $object->getText(),
            self::ATTR_ONLINE_USER_URI => $object->getCreator()->getUri(),
            self::ATTR_LOGIN_CREATEUR => $object->getCreator()->getUsername(),
            self::ATTR_SUPPRIME => $object->isDeleted(),
        ];
    }
}
