<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\Annotation;
use Webapp\Core\Entity\FieldGeneration;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\Session;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Dto\Webapp\MeasureDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Session, SessionXmlDto>
 *
 * @psalm-type SessionXmlDto = array{
 *     "@dateDebut": \DateTime,
 *     "@dateFin": \DateTime,
 *     "@horodatage": ?\DateTime,
 *     "@experimentateur": ?string,
 *     "metadonnees": Collection<int, Annotation>,
 *     "mesureVariables": MeasureDto[]
 * }
 */
class SessionNormalizer extends AbstractXmlNormalizer
{
    private const ATTR_DATE_DEBUT = '@dateDebut';
    private const ATTR_DATE_FIN = '@dateFin';
    private const ATTR_IS_AUTHENTICATED = '@isAuthenticated';
    private const ATTR_EXPERIMENTATEUR = '@experimentateur';
    private const TAG_METADONNEES = 'metadonnees';
    private const TAG_MESURE_VARIABLES = 'mesureVariables';
    private const ATTR_HORODATAGE = '@horodatage';

    protected function getClass(): string
    {
        return Session::class;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        return parent::normalize($object, $format, self::overrideWriterHelper($context, [self::TAG_MESURE_VARIABLES]));
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_DATE_DEBUT => \DateTime::class,
            self::ATTR_DATE_FIN => \DateTime::class,
            self::ATTR_EXPERIMENTATEUR => 'string',
            self::ATTR_HORODATAGE => \DateTime::class,
            self::TAG_MESURE_VARIABLES => XmlImportConfig::values(MeasureDto::class),
            self::TAG_METADONNEES => XmlImportConfig::collection(Annotation::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_DATE_DEBUT]) || !isset($data[self::ATTR_DATE_FIN])) {
            throw new ParsingException('', RequestFile::ERROR_DATA_ENTRY_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Session
    {
        return new Session();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Session
    {
        $object
            ->setStartedAt($data[self::ATTR_DATE_DEBUT])
            ->setEndedAt($data[self::ATTR_DATE_FIN])
            ->setAnnotations($data[self::TAG_METADONNEES]);

        $readerHelper = self::getReaderHelper($context);
        $sessionUsername = $data[self::ATTR_EXPERIMENTATEUR] ?? null;
        $user = (null !== $sessionUsername && $readerHelper->exists($sessionUsername)) ? $readerHelper->get($sessionUsername, User::class) : null;
        if (null === $user) {
            $object->setComment("Experimentateur non trouvé, expérimentateur d'origine : ".$sessionUsername);
        }
        $object->setUser($user ?? $context[PlatformDtoNormalizer::USER]);

        $readerHelper = self::getReaderHelper($context);
        /** @var array<string, FieldGeneration> $generatedVariables `generatedMeasureUri` as key */
        $generatedVariables = [];
        /** @var array<string, FieldMeasure> $generatorVariables `generatorMeasureUri` as key */
        $generatorVariables = [];
        /** @var array<string, FieldGeneration> $generatorFields `generatorIndex . generatorMeasureUri` as key */
        $generatorFields = [];
        /** @var array<string, FieldMeasure> $repetitionMap `generatorIndex . generatorMeasure . variable . businessObject` as key */
        $repetitionMap = [];

        /** @var MeasureDto $item */
        foreach ($data[self::TAG_MESURE_VARIABLES] as $item) {
            //            $item->measure->setTimestamp($data[self::ATTR_HORODATAGE]);
            $measureUri = $readerHelper->getReference($item->measure);

            // Handle repetitions (not available on generatorVariables)
            $repetitionUri = ($item->indiceGeneratrice ?? '')
                .($item->mesureGeneratrice ?? '')
                .$readerHelper->getReference($item->variable)
                .$readerHelper->getReference($item->objetMetier);
            if (!isset($repetitionMap[$repetitionUri])) {
                $fieldMeasure = (new FieldMeasure())
                    ->setVariable($item->variable)
                    ->setTarget($item->objetMetier);

                $repetitionMap[$repetitionUri] = $fieldMeasure;

                // Generator values
                if (null !== $item->mesuresGenerees) {
                    foreach (explode(' ', $item->mesuresGenerees) as $measureUri) {
                        $generatorVariables[$measureUri] = $fieldMeasure;
                        if (isset($generatedVariables[$measureUri])) {
                            $fieldMeasure->addFieldGeneration($generatedVariables[$measureUri]);
                        }
                    }
                }

                // Generated values
                if (null !== $item->mesureGeneratrice) {
                    $generatorFieldUri = ($item->indiceGeneratrice ?? '').$item->mesureGeneratrice;
                    if (!isset($generatorFields[$generatorFieldUri])) {
                        $generatorFields[$generatorFieldUri] = (new FieldGeneration())
                            ->setIndex($item->indiceGeneratrice ?? 0)
                            ->setNumeralIncrement(false) // TODO prendre les vraies valeur ou retirer ces attributs de la classe (redondance avec la variable)
                            ->setPrefix($fieldMeasure->getVariable()->getGeneratorVariable()->getGeneratedPrefix());
                    }
                    $generatorFields[$generatorFieldUri]->addChild($fieldMeasure);

                    // If the generator measure was already found before
                    if (isset($generatorVariables[$item->mesureGeneratrice])) {
                        $generatorVariables[$item->mesureGeneratrice]->addFieldGeneration($generatorFields[$generatorFieldUri]);
                    } else {
                        $generatedVariables[$measureUri] = $generatorFields[$generatorFieldUri];
                    }
                } else {
                    $object->addFieldMeasure($fieldMeasure);
                }
            }
            $item->measure->setRepetition($repetitionMap[$repetitionUri]->getMeasures()->count());
            $repetitionMap[$repetitionUri]->addMeasure($item->measure);
        }

        return $object;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_DATE_DEBUT => $object->getStartedAt(),
            self::ATTR_DATE_FIN => $object->getEndedAt(),
            self::ATTR_IS_AUTHENTICATED => true,
            self::ATTR_EXPERIMENTATEUR => new ReferenceDto($object->getUser()),
            self::TAG_MESURE_VARIABLES => $this->extractMeasures($object->getFieldMeasures(), $context),
            self::TAG_METADONNEES => $object->getAnnotations(),
        ];
    }

    /**
     * @param Collection<int, FieldMeasure> $fieldMeasures
     */
    private function extractMeasures(Collection $fieldMeasures, array $context): array
    {
        $measures = [];
        foreach ($fieldMeasures as $fieldMeasure) {
            $measures = array_merge($measures, $fieldMeasure->getMeasures()->getValues());
            foreach ($fieldMeasure->getFieldGenerations() as $fieldGeneration) {
                $measures = array_merge($measures, $this->extractMeasures($fieldGeneration->getChildren(), $context));
            }
        }

        foreach ($measures as $i => $measure) {
            self::getWriterHelper($context)->register($measure, $context[self::REFERENCE]->getReference(), self::TAG_MESURE_VARIABLES, $i);
        }

        return $measures;
    }
}
