<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Note;
use Webapp\Core\Entity\OutExperimentationZone;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Enumeration\ExperimentStateEnum;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\PlatformDtoNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlExportConfig;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @psalm-type Etat = self::ETAT_*
 *
 * @template-extends AbstractXmlNormalizer<Experiment, ExperimentXmlDto>
 *
 * @psalm-type ExperimentXmlDto = array{
 *       "@dateCreation": ?\DateTime,
 *       "@dateValidation": ?\DateTime,
 *       "@etat": ?string,
 *       "@nom": ?string,
 *       "@puSurfacique": ?bool,
 *       blocs: Collection<int, Block>,
 *       notes: Collection<int, Note>,
 *       protocoles: Protocol,
 *       zheAvecEpaisseurs: Collection<int, OutExperimentationZone>
 * }
 */
class ExperimentNormalizer extends AbstractXmlNormalizer
{
    public const ETAT_CREATED = 'cree';
    public const ETAT_VALIDATED = 'valide';
    public const ETAT_LOCKED = 'verrouille';
    public const ETAT_NON_UNLOCKABLE = 'nonDeverrouillable';

    private const STATE_MAP = [
        ExperimentStateEnum::CREATED => self::ETAT_CREATED,
        ExperimentStateEnum::VALIDATED => self::ETAT_VALIDATED,
        ExperimentStateEnum::LOCKED => self::ETAT_LOCKED,
        ExperimentStateEnum::NON_UNLOCKABLE => self::ETAT_NON_UNLOCKABLE,
    ];
    private const REVERSE_STATE_MAP = [
        self::ETAT_CREATED => ExperimentStateEnum::CREATED,
        self::ETAT_VALIDATED => ExperimentStateEnum::VALIDATED,
        self::ETAT_LOCKED => ExperimentStateEnum::LOCKED,
        self::ETAT_NON_UNLOCKABLE => ExperimentStateEnum::LOCKED,
    ];

    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_SOURCE = '@source';
    protected const ATTR_PU_SURFACIQUE = '@puSurfacique';
    protected const ATTR_DATE_VALIDATION = '@dateValidation';
    protected const ATTR_ETAT = '@etat';
    protected const TAG_PROTOCOLES = 'protocoles';
    protected const TAG_BLOCS = 'blocs';
    protected const TAG_ZHE_AVEC_EPAISSEURS = 'zheAvecEpaisseurs';
    protected const TAG_NOTES = 'notes';

    protected function getClass(): string
    {
        return Experiment::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_DATE_VALIDATION => \DateTime::class,
            self::ATTR_PU_SURFACIQUE => 'bool',
            self::ATTR_ETAT => 'string',
            self::TAG_PROTOCOLES => XmlImportConfig::value(Protocol::class),
            self::TAG_BLOCS => XmlImportConfig::collection(Block::class),
            self::TAG_ZHE_AVEC_EPAISSEURS => XmlImportConfig::collection(OutExperimentationZone::class),
            self::TAG_NOTES => XmlImportConfig::collection(Note::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::TAG_PROTOCOLES])) {
            throw new ParsingException('', RequestFile::ERROR_NO_PROTOCOL);
        }
    }

    protected function generateImportedObject($data, array $context): Experiment
    {
        return new Experiment();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Experiment
    {
        $object
            ->setSite($context[PlatformDtoNormalizer::SITE])
            ->setOwner($context[PlatformDtoNormalizer::USER])
            ->setName($data[self::ATTR_NOM] ?? '')
            ->setCreated($data[self::ATTR_DATE_CREATION] ?? new \DateTime())
            ->setValidated($data[self::ATTR_DATE_VALIDATION])
            ->setState(self::REVERSE_STATE_MAP[$data[self::ATTR_ETAT] ?? self::ETAT_CREATED])
            ->setProtocol($data[self::TAG_PROTOCOLES])
            ->setBlocks($data[self::TAG_BLOCS])
            ->setOutExperimentationZones($data[self::TAG_ZHE_AVEC_EPAISSEURS])
            ->setNotes($data[self::TAG_NOTES]);

        return $object;
    }

    public function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreated(),
            self::ATTR_SOURCE => $object->getProtocol()->getName(),
            self::ATTR_PU_SURFACIQUE => !$object->isIndividualUP(),
            self::ATTR_DATE_VALIDATION => $object->getValidated(),
            self::ATTR_ETAT => self::STATE_MAP[$object->getState()],
            self::TAG_PROTOCOLES => new XmlExportConfig($object->getProtocol(), null, XmlExportConfig::NONE),
            self::TAG_BLOCS => $object->getBlocks(),
            self::TAG_ZHE_AVEC_EPAISSEURS => $object->getOutExperimentationZones(),
            self::TAG_NOTES => $object->getNotes(),
        ];
    }
}
