<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Shared\Authentication\Entity\User;
use Shared\Authentication\Repository\UserRepository;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<User, UserXmlDto>
 *
 * @psalm-type UserXmlDto = array{
 *     "@login": ?string
 * }
 */
class UserNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_LOGIN = '@login';
    protected const ATTR_NOM = '@nom';
    protected const ATTR_PRENOM = '@prenom';
    protected const ATTR_EMAIL = '@email';
    protected const ATTR_MOT_DE_PASSE = '@motDePasse';

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    protected function getClass(): string
    {
        return User::class;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_LOGIN => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): User
    {
        $login = $data[self::ATTR_LOGIN] ?? '';
        $user = $this->userRepository->findOneBy(['username' => $login]);
        if (null === $user) {
            throw new ParsingException('User not found');
        }

        return $user;
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): User
    {
        return $object;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getSurname(),
            self::ATTR_PRENOM => $object->getName(),
            self::ATTR_EMAIL => $object->getEmail(),
            self::ATTR_LOGIN => $object->getUsername(),
            self::ATTR_MOT_DE_PASSE => 'NO',
        ];
    }
}
