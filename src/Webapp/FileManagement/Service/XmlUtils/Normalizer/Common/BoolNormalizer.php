<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class BoolNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    private const TRUE = 'true';
    private const FALSE = 'false';

    /**
     * @param mixed $data
     */
    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return 'bool' === $type && ($context[AbstractXmlNormalizer::IMPORT_XML] ?? false) === true;
    }

    /**
     * @param mixed $data
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return \is_bool($data) && ($context[AbstractXmlNormalizer::EXPORT_XML] ?? false) === true;
    }

    /**
     * @param mixed $data
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): bool
    {
        if (!\is_scalar($data)) {
            throw new UnexpectedValueException('$data must be a scalar');
        }

        return \in_array($data, [self::TRUE, true, 1, '1'], true);
    }

    /**
     * @psalm-param mixed $object
     */
    public function normalize($object, ?string $format = null, array $context = []): string
    {
        if (!\is_bool($object)) {
            throw new InvalidArgumentException('$object must be a boolean');
        }

        return $object ? self::TRUE : self::FALSE;
    }
}
