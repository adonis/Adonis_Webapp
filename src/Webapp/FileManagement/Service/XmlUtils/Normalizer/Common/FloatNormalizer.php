<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class FloatNormalizer implements ContextAwareDenormalizerInterface
{
    /**
     * @param mixed $data
     */
    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return 'float' === $type && ($context[AbstractXmlNormalizer::IMPORT_XML] ?? false) === true;
    }

    /**
     * @param mixed $data
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): float
    {
        if (!is_numeric($data)) {
            throw new UnexpectedValueException('$data must be a numeric');
        }

        return (float) $data;
    }
}
