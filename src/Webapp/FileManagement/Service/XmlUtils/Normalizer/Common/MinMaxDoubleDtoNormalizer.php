<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Webapp\FileManagement\Dto\Common\MinMaxDoubleDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class MinMaxDoubleDtoNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    private const ATTR_MAX_DOUBLE = '@maxDouble';
    private const ATTR_MIN_DOUBLE = '@minDouble';

    public function denormalize($data, string $type, ?string $format = null, array $context = []): MinMaxDoubleDto
    {
        return new MinMaxDoubleDto(
            isset($data[self::ATTR_MIN_DOUBLE]) ? (float) ($data[self::ATTR_MIN_DOUBLE]) : null,
            isset($data[self::ATTR_MAX_DOUBLE]) ? (float) ($data[self::ATTR_MAX_DOUBLE]) : null
        );
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        if (!$object instanceof MinMaxDoubleDto) {
            throw new InvalidArgumentException('$object must be MinMaxDoubleDto');
        }

        return [
            self::ATTR_MIN_DOUBLE => $object->min,
            self::ATTR_MAX_DOUBLE => $object->max,
        ];
    }

    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof MinMaxDoubleDto && ($context[AbstractXmlNormalizer::EXPORT_XML] ?? false) === true;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return MinMaxDoubleDto::class === $type && ($context[AbstractXmlNormalizer::IMPORT_XML] ?? false) === true;
    }
}
