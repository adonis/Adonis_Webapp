<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Webapp\FileManagement\Dto\Common\ReferencesDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class ReferencesNormalizer implements ContextAwareNormalizerInterface
{
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof ReferencesDto && ($context[AbstractXmlNormalizer::EXPORT_XML] ?? false) === true;
    }

    public function normalize($object, ?string $format = null, array $context = []): string
    {
        if (!$object instanceof ReferencesDto) {
            throw new InvalidArgumentException('$object must be a ReferencesDto');
        }
        $writerHelper = AbstractXmlNormalizer::getWriterHelper($context);

        return implode(' ', array_map(fn (IdentifiedEntity $reference) => $writerHelper->get($reference->getUri()), $object->getReferences()));
    }
}
