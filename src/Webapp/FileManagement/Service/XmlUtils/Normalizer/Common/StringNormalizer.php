<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class StringNormalizer implements ContextAwareDenormalizerInterface
{
    /**
     * @param mixed $data
     */
    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return 'string' === $type && ($context[AbstractXmlNormalizer::IMPORT_XML] ?? false) === true;
    }

    /**
     * @param mixed $data
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): string
    {
        if (!\is_scalar($data)) {
            throw new UnexpectedValueException('$data must be a scalar');
        }

        return (string) $data;
    }
}
