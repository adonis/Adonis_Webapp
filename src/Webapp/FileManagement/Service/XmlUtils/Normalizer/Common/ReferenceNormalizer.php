<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class ReferenceNormalizer implements ContextAwareNormalizerInterface
{
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof ReferenceDto && ($context[AbstractXmlNormalizer::EXPORT_XML] ?? false) === true;
    }

    public function normalize($object, ?string $format = null, array $context = []): string
    {
        if (!$object instanceof ReferenceDto) {
            throw new InvalidArgumentException('$object must be a ReferenceDto');
        }
        $writerHelper = AbstractXmlNormalizer::getWriterHelper($context);

        return $writerHelper->get($object->getEntity()->getUri());
    }
}
