<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Common;

use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Webapp\FileManagement\Dto\Common\ColorDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class ColorNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface
{
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof ColorDto && ($context[AbstractXmlNormalizer::EXPORT_XML] ?? false) === true;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return ColorDto::class === $type && ($context[AbstractXmlNormalizer::IMPORT_XML] ?? false) === true;
    }

    public function normalize($object, ?string $format = null, array $context = []): string
    {
        if (!$object instanceof ColorDto) {
            throw new InvalidArgumentException('$object must be a ColorDto');
        }

        $b = $object->value % 256;
        $g = round((($object->value - $b) % 256 ** 2) / 256);
        $r = round(($object->value - $g - $b) / 256 ** 2);

        return 'RGB{'.$r.','.$g.','.$b.'}';
    }

    public function denormalize($data, string $type, ?string $format = null, array $context = []): int
    {
        if (!\is_string($data)) {
            throw new UnexpectedValueException('$data must be a string');
        }

        preg_match('/.*{([0-9]+), *([0-9]+), *([0-9]+)}/', $data, $matches);

        return (int) $matches[1] * 256 * 256 + (int) $matches[2] * 256 + (int) $matches[3];
    }
}
