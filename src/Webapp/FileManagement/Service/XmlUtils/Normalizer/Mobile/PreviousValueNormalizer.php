<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\PreviousValue;
use Mobile\Measure\Entity\Variable\StateCode;
use Shared\Authentication\Entity\IdentifiedEntity;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<PreviousValue, PreviousValueXmlDto>
 *
 * @psalm-type PreviousValueXmlDto = array{
 *      "@codeEtat": ?StateCode,
 *      "@horodatage": ?\DateTime,
 *      "@objetMetier": ?IdentifiedEntity,
 *      "@valeur": ?string,
 *      "@variable": ?Variable
 * }
 */
class PreviousValueNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_OBJET_METIER = '@objetMetier';
    protected const ATTR_VARIABLE = '@variable';
    protected const ATTR_CODE_ETAT = '@codeEtat';
    protected const ATTR_HORODATAGE = '@horodatage';
    protected const ATTR_VALEUR = '@valeur';

    protected function getClass(): string
    {
        return PreviousValue::class;
    }

    protected function extractData($object, array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_VARIABLE => XmlImportConfig::value(Variable::class),
            self::ATTR_HORODATAGE => \DateTime::class,
            self::ATTR_CODE_ETAT => XmlImportConfig::value(StateCode::class),
            self::ATTR_VALEUR => 'string',
            self::ATTR_OBJET_METIER => XmlImportConfig::value(IdentifiedEntity::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_OBJET_METIER]) || !isset($data[self::ATTR_VARIABLE])) {
            throw new ParsingException('', RequestFile::ERROR_PREVIOUS_VALUE_INFO_MISSING);
        }

        $readerHelper = self::getReaderHelper($context);
        if (
            isset($data[self::ATTR_CODE_ETAT])
            && $readerHelper->exists($data[self::ATTR_CODE_ETAT])
            && !$context[CommonDtoNormalizer::STATE_CODES]->exists($data[self::ATTR_CODE_ETAT])
        ) {
            throw new ParsingException("Code d'état utilisé dans les mesures de variable connecté non présent dans le projet courant", RequestFile::ERROR_URI_NOT_FOUND);
        }
        if (!$readerHelper->exists($data[self::ATTR_OBJET_METIER])) {
            throw new ParsingException('', RequestFile::ERROR_UNKNOWN_PREVIOUS_VALUE_LINKED_OBJECT);
        }
    }

    protected function generateImportedObject($data, array $context): PreviousValue
    {
        return new PreviousValue();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): PreviousValue
    {
        $object
            ->setOriginVariable($data[self::ATTR_VARIABLE])
            ->setDate($data[self::ATTR_HORODATAGE])
            ->setState($data[self::ATTR_CODE_ETAT])
            ->setValue($data[self::ATTR_VALEUR])
            ->setObjectUri($data[self::ATTR_OBJET_METIER]->getUri());

        return $object;
    }
}
