<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Measure;
use Webapp\Core\Enumeration\VariableTypeEnum;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Measure, array{}>
 */
class MeasureNormalizer extends AbstractXmlNormalizer
{
    private const XSI_TYPE_MAP = [
        VariableTypeEnum::ALPHANUMERIC => 'adonis.modeleMetier.saisieTerrain:MesureVariableAlphanumerique',
        VariableTypeEnum::REAL => 'adonis.modeleMetier.saisieTerrain:MesureVariableReel',
        VariableTypeEnum::BOOLEAN => 'adonis.modeleMetier.saisieTerrain:MesureVariableBooleen',
        VariableTypeEnum::INTEGER => 'adonis.modeleMetier.saisieTerrain:MesureVariableEntiere',
        VariableTypeEnum::DATE => 'adonis.modeleMetier.saisieTerrain:MesureVariableDate',
        VariableTypeEnum::HOUR => 'adonis.modeleMetier.saisieTerrain:MesureVariableHeure',
    ];
    protected const ATTR_HORODATAGE = '@horodatage';
    protected const ATTR_VARIABLE = '@variable';
    protected const ATTR_OBJET_METIER = '@objetMetier';
    protected const ATTR_INDICE_GENERATRICE = '@indiceGeneratrice';
    protected const ATTR_MESURE_GENERATRICE = '@mesureGeneratrice';
    protected const ATTR_CODE_ETAT = '@codeEtat';
    protected const ATTR_VALEUR = '@valeur';
    public const ATTR_MESURES_GENEREES = '@mesuresGenerees';

    protected function getClass(): string
    {
        return Measure::class;
    }

    protected function extractData($object, array $context): array
    {
        $formField = $object->getFormField();
        $variable = $formField->getVariable();

        $codeEtat = null;
        $valeur = null;
        if (null !== $object->getState()) {
            if (null === $object->getState()->getPropagation()) {
                $codeEtatUri = $object->getState()->getUri();
            } else {
                $codeEtatUri = $object->getFormField()->getTarget()->getUri().$object->getState()->getUri();
            }
            $codeEtat = self::getWriterHelper($context)->get($codeEtatUri);
        } elseif (VariableTypeEnum::REAL === $variable->getType() && null !== $variable->getFormat() && null !== $object->getValue()) {
            $valeur = number_format((float) $object->getValue(), (int) $variable->getFormat(), '.', '');
        } else {
            $valeur = $object->getValue();
        }

        return [
            self::ATTR_HORODATAGE => $object->getTimestamp(),
            self::ATTR_CODE_ETAT => $codeEtat,
            self::ATTR_VALEUR => $valeur,
            self::ATTR_XSI_TYPE => self::XSI_TYPE_MAP[$variable->getType()],
            self::ATTR_VARIABLE => new ReferenceDto($variable),
            self::ATTR_OBJET_METIER => new ReferenceDto($formField->getTarget()),
            self::ATTR_INDICE_GENERATRICE => null !== $formField->getGeneratedField() ? $formField->getGeneratedField()->getIndex() : null,
            self::ATTR_MESURE_GENERATRICE => null !== $formField->getGeneratedField() ? new ReferenceDto($formField->getGeneratedField()->getFormField()->getMeasures()->first()) : null,
//            self::ATTR_MESURES_GENEREES => // Set in session normalizer to avoid reference loop
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function generateImportedObject($data, array $context)
    {
        throw new \LogicException('Method not implemented');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context)
    {
        throw new \LogicException('Method not implemented');
    }
}
