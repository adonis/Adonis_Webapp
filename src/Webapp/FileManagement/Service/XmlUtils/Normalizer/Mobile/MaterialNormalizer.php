<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\Material;
use Webapp\FileManagement\Dto\Common\ReferencesDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Material, array{}>
 */
class MaterialNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_FABRIQUANT = '@fabriquant';
    protected const ATTR_APPAREIL = '@appareil';
    protected const ATTR_TYPE = '@type';
    protected const TAG_DRIVER = 'driver';
    protected const ATTR_VARIABLES = '@variables';

    protected function getClass(): string
    {
        return Material::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_FABRIQUANT => $object->getManufacturer(),
            self::ATTR_APPAREIL => $object->getPhysicalDevice(),
            self::ATTR_TYPE => $object->getType(),
            self::TAG_DRIVER => $object->getDriver(),
            self::ATTR_VARIABLES => new ReferencesDto(array_merge($object->getUniqueVariables()->getValues(), $object->getGeneratorVariables()->getValues())),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function generateImportedObject($data, array $context): Material
    {
        throw new \LogicException('Method not implemented');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Material
    {
        throw new \LogicException('Method not implemented');
    }
}
