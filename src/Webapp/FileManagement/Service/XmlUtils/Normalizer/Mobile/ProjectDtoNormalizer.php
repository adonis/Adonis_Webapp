<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Webapp\FileManagement\Dto\Mobile\CommonDataDto;
use Webapp\FileManagement\Dto\Mobile\ProjectDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlExportConfig;

/**
 * @template-extends CommonDtoNormalizer<ProjectDto>
 */
final class ProjectDtoNormalizer extends CommonDtoNormalizer
{
    protected function getClass(): string
    {
        return ProjectDto::class;
    }

    protected function extractData($object, array $context): array
    {
        $project = $object->dataEntryProject;
        if (null === $project) {
            throw new \LogicException('$object->dataEntryProject must not be null');
        }

        return array_merge(parent::extractData($object, $context), [
            'adonis.modeleMetier.utilisateur:Utilisateur' => new XmlExportConfig($project->getDesktopUsers(), false),
            'adonis.modeleMetier.plateforme:NatureZhe' => new XmlExportConfig($project->getNaturesZHE(), false),
            'adonis.modeleMetier.plateforme:Plateforme' => new XmlExportConfig($project->getPlatform(), false),
            'adonis.modeleMetier.projetDeSaisie:ProjetDeSaisie' => new XmlExportConfig($project, false),
        ]);
    }

    protected function generateImportedObject($data, array $context): ProjectDto
    {
        return new ProjectDto();
    }

    protected function mapMobileDto(CommonDataDto $mobileDto, $data, ?string $format, array $context): ProjectDto
    {
        $object = new ProjectDto();
        $object->dataEntryProject = $mobileDto->dataEntryProject;

        return $object;
    }
}
