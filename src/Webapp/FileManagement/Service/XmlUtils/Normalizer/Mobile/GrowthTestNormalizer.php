<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Test\GrowthTest;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Webapp\FileManagement\Dto\Common\MinMaxDoubleDto;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends TestNormalizer<GrowthTest, GrowthTestXmlDto>
 *
 * @psalm-type GrowthTestXmlDto = array{
 *     "@active": ?bool,
 *     "@nom": ?string,
 *     "@variableDeComparaison": ?Variable,
 *      ecart: ?MinMaxDoubleDto,
 * }
 */
class GrowthTestNormalizer extends TestNormalizer
{
    protected const ATTR_VARIABLE_DE_COMPARAISON = '@variableDeComparaison';
    protected const TAG_ECART = 'ecart';

    protected function getClass(): string
    {
        return GrowthTest::class;
    }

    protected function extractData($object, array $context): array
    {
        return array_merge(parent::extractCommonData($object, $context), [
            self::ATTR_XSI_TYPE => 'adonis.modeleMetier.projetDeSaisie.variables:TestsDeComparaison',
            self::ATTR_NOM => 'Test d\'accroissement',
            self::ATTR_VARIABLE_DE_COMPARAISON => new ReferenceDto($object->getComparisonVariable()),
            self::TAG_ECART => new MinMaxDoubleDto($object->getMinDiff(), $object->getMaxDiff()),
        ]);
    }

    protected function getImportDataConfig(array $context): array
    {
        return array_merge(parent::getCommonImportDataConfig($context), [
            self::ATTR_VARIABLE_DE_COMPARAISON => XmlImportConfig::value(Variable::class),
            self::TAG_ECART => XmlImportConfig::value(MinMaxDoubleDto::class),
        ]);
    }

    protected function validateImportData($data, array $context): void
    {
        parent::validateImportData($data, $context);

        $readerHelper = self::getReaderHelper($context);
        if (!$readerHelper->exists($data[self::ATTR_VARIABLE_DE_COMPARAISON])) {
            throw new ParsingException('', RequestFile::ERROR_UNKNOWN_TEST_LINKED_VARIABLE);
        }
    }

    protected function generateImportedObject($data, array $context): GrowthTest
    {
        return new GrowthTest();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): GrowthTest
    {
        $object = parent::completeImportedObject($object, $data, $format, $context);
        $object
            ->setMinDiff($data[self::TAG_ECART]->min ?? 0)
            ->setMaxDiff($data[self::TAG_ECART]->max ?? 0)
            ->setComparisonVariable($data[self::ATTR_VARIABLE_DE_COMPARAISON]);

        return $object;
    }
}
