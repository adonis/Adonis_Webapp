<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\Individual;
use Mobile\Device\Entity\OutExperimentationZone;
use Mobile\Device\Entity\Treatment;
use Mobile\Device\Entity\UnitParcel;
use Mobile\Measure\Entity\Variable\StateCode;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlExportConfig;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<UnitParcel, UnitParcelXmlDto>
 *
 * @psalm-type UnitParcelXmlDto = array{
 *      "@dateApparition": ?\DateTime,
 *      "@dateDisparition": ?\DateTime,
 *      "@idRfidCodeBarre": ?string,
 *      "@numero": ?string,
 *      "@traitement": ?Treatment,
 *      "@x": ?int,
 *      "@y": ?int,
 *      codeEtat: ?StateCode,
 *      individus: Collection<int, Individual>,
 *      notes: Collection<int, EntryNote>,
 *      zheAvecEpaisseurs: Collection<int, OutExperimentationZone>
 * }
 */
class UnitParcelNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NUMERO = '@numero';
    protected const ATTR_X = '@x';
    protected const ATTR_Y = '@y';
    protected const ATTR_DATE_DISPARITION = '@dateDisparition';
    protected const ATTR_DATE_APPARITION = '@dateApparition';
    protected const ATTR_TRAITEMENT = '@traitement';
    protected const ATTR_ID_RFID_CODE_BARRE = '@idRfidCodeBarre';

    protected const TAG_NOTES = 'notes';
    protected const TAG_INDIVIDUS = 'individus';
    protected const TAG_ZHE_AVEC_EPAISSEURS = 'zheAvecEpaisseurs';
    protected const TAG_CODE_ETAT = 'codeEtat';
    protected const ATTR_NB_INDIVIDUS = '@nbIndividus';

    protected function getClass(): string
    {
        return UnitParcel::class;
    }

    protected function extractData($object, array $context): array
    {
        self::getWriterHelper($context)->addSessionAnnotations(AnnotationDto::fromCollection($object->getAnnotations(), $object));

        if (null !== $object->getAnomaly()) {
            self::getWriterHelper($context)->addAnomaly($object->getAnomaly());
        }

        $device = null === $object->getSubBlock() ? $object->getBlock()->getDevice() : $object->getSubBlock()->getBlock()->getDevice();
        if ($device->isIndividualPU()) {
            $forType = [
                self::ATTR_XSI_TYPE => 'adonis.modeleMetier.plateforme:PuIndividuel',
                self::ATTR_NB_INDIVIDUS => $object->getIndividuals()->count(),
                self::ATTR_DATE_APPARITION => $object->getApparitionDate(),
            ];
        } else {
            $forType = [
                self::ATTR_XSI_TYPE => 'adonis.modeleMetier.plateforme:PuSurfacique',
            ];
            if (null !== $object->getIdent() && '' !== $object->getIdent()) {
                $forType[self::ATTR_ID_RFID_CODE_BARRE] = $object->getIdent();
            }
        }

        return array_merge($forType, [
            self::ATTR_X => $object->getX(),
            self::ATTR_Y => $object->getY(),
            self::ATTR_NUMERO => $object->getName(),
            self::ATTR_DATE_DISPARITION => $object->getDemiseDate(),
            // parcels without treatments are filtered by parents object
            self::ATTR_TRAITEMENT => null !== $object->getTreatment() ? new ReferenceDto($object->getTreatment()) : null,
            self::TAG_CODE_ETAT => new XmlExportConfig($object->getStateCode(), null, XmlExportConfig::NONE),
            self::TAG_INDIVIDUS => $object->getIndividuals(),
            self::TAG_ZHE_AVEC_EPAISSEURS => $object->getOutExperimentationZones(),
            self::TAG_NOTES => $object->getNotes(),
        ]);
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NUMERO => 'string',
            self::ATTR_X => 'int',
            self::ATTR_Y => 'int',
            self::ATTR_DATE_APPARITION => \DateTime::class,
            self::ATTR_DATE_DISPARITION => \DateTime::class,
            self::ATTR_ID_RFID_CODE_BARRE => 'string',
            self::ATTR_TRAITEMENT => XmlImportConfig::reference(Treatment::class),
            self::TAG_INDIVIDUS => XmlImportConfig::collection(Individual::class),
            self::TAG_ZHE_AVEC_EPAISSEURS => XmlImportConfig::collection(OutExperimentationZone::class),
            self::TAG_CODE_ETAT => XmlImportConfig::value(StateCode::class),
            self::TAG_NOTES => XmlImportConfig::collection(EntryNote::class),
        ];
    }

    protected function generateImportedObject($data, array $context): UnitParcel
    {
        return new UnitParcel();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): UnitParcel
    {
        $object
            ->setX($data[self::ATTR_X] ?? 0)
            ->setY($data[self::ATTR_Y] ?? 0)
            ->setDemiseDate($data[self::ATTR_DATE_DISPARITION])
            ->setApparitionDate($data[self::ATTR_DATE_APPARITION])
            ->setIdent($data[self::ATTR_ID_RFID_CODE_BARRE])
            ->setTreatment($data[self::ATTR_TRAITEMENT])
            ->setIndividuals($data[self::TAG_INDIVIDUS])
            ->setOutExperimentationZones($data[self::TAG_ZHE_AVEC_EPAISSEURS])
            ->setStateCode(null !== $data[self::TAG_CODE_ETAT] ? $context[CommonDtoNormalizer::STATE_CODES]->get($data[self::TAG_CODE_ETAT]) : null)
            ->setNotes($data[self::TAG_NOTES])
            ->setName($data[self::ATTR_NUMERO] ?? '0');

        return $object;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $normalized = parent::normalize($object, $format, $context);

        $writerHelper = self::getWriterHelper($context);

        \assert($object instanceof UnitParcel);
        if (null !== $object->getStateCode()) {
            // Replace URI for state code to make it specific for the UnitParcel
            $writerHelper->remove($object->getStateCode()->getUri());
            $writerHelper->add($object->getUri().$object->getStateCode()->getUri(), $writerHelper->get($object->getUri()).'/@codeEtat');
        }

        return $normalized;
    }
}
