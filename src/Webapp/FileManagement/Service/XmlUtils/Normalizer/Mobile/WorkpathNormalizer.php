<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Project\Entity\DesktopUser;
use Mobile\Project\Entity\Workpath;
use Shared\Authentication\Entity\IdentifiedEntity;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Workpath, WorkpathXmlDto>
 *
 * @psalm-type WorkpathXmlDto = array{
 *     "@objets": IdentifiedEntity[],
 *     "@nom": ?string, "@declenchement": ?string
 * }
 */
class WorkpathNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_OBJETS = '@objets';
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DECLENCHEMENT = '@declenchement';
    protected const ATTR_UTILISATEUR = '@utilisateur';

    protected function getClass(): string
    {
        return Workpath::class;
    }

    protected function extractData($object, array $context): array
    {
        $user = $object->getDataEntryProject()->getDesktopUsers()->filter(
            fn (DesktopUser $desktopUser) => $desktopUser->getLogin() === $object->getUsername()
        )->first();
        if (false === $user) {
            throw new \LogicException(\sprintf('Workpath user "%s" does not exist', $object->getUsername()));
        }

        $writerHelper = self::getWriterHelper($context);
        $objectPathExploded = explode('/', $object->getPath());
        array_shift($objectPathExploded);
        $objectPath = implode(' ', array_map(fn ($item) => $writerHelper->get('/'.$item), $objectPathExploded));

        return [
            self::ATTR_NOM => $object->getUsername(),
            self::ATTR_UTILISATEUR => new ReferenceDto($user),
            self::ATTR_OBJETS => $objectPath,
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_OBJETS => XmlImportConfig::values(IdentifiedEntity::class),
            self::ATTR_DECLENCHEMENT => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): Workpath
    {
        return new Workpath();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Workpath
    {
        if (!isset($data[self::ATTR_OBJETS]) || !isset($data[self::ATTR_NOM])) {
            return $object;
        }

        $object
            ->setUsername($data[self::ATTR_NOM])
            ->setStartEnd(null !== $data[self::ATTR_DECLENCHEMENT]);

        $idPath = '';
        foreach ($data[self::ATTR_OBJETS] as $item) {
            $className = (new \ReflectionClass(\get_class($item)))->getShortName();
            $idPath = $idPath.'/'.$className.'.'.$item->getId();
        }
        $object->setPath($idPath);

        return $object;
    }
}
