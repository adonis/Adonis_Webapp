<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Variable\ValueHint;
use Mobile\Measure\Entity\Variable\ValueHintList;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<ValueHintList, ValueHintListXmlDto>
 *
 * @psalm-type ValueHintListXmlDto = array{
 *      "@nom": ?string,
 *      valeurs: Collection<int, ValueHint>
 * }
 */
class ValueHintListNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    public const TAG_VALEURS = 'valeurs';

    protected function getClass(): string
    {
        return ValueHintList::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::TAG_VALEURS => $object->getValueHints(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::TAG_VALEURS => XmlImportConfig::collection(ValueHint::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM])) {
            throw new ParsingException('', RequestFile::ERROR_VALUE_HINT_LIST_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): ValueHintList
    {
        return new ValueHintList();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): ValueHintList
    {
        $object
            ->setName($data[self::ATTR_NOM])
            ->setValueHints($data[self::TAG_VALEURS]);

        return $object;
    }
}
