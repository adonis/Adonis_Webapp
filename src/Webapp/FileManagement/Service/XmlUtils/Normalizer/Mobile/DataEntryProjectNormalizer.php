<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Variable\Material;
use Mobile\Measure\Entity\Variable\RequiredAnnotation;
use Mobile\Project\Entity\DataEntryProject;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Dto\Common\ReferencesDto;
use Webapp\FileManagement\Dto\Mobile\VariableAndTestDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<DataEntryProject, DataEntryProjectXmlDto>
 *
 * @psalm-type DataEntryProjectXmlDto = array{
 *      "@dateCreation": \DateTime,
 *      "@nom": string,
 *      materiel: Collection<int, Material>,
 *      metadonneesASaisir: Collection<int, RequiredAnnotation>,
 *      variables: VariableAndTestDto[]
 * }
 */
class DataEntryProjectNormalizer extends AbstractXmlNormalizer
{
    protected const TAG_METADONNEES_A_SAISIR = 'metadonneesASaisir';
    protected const ATTR_TYPE_SAISIE = 'typeSaisie';
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const TAG_MATERIEL = 'materiel';
    protected const TAG_VARIABLES = 'variables';
    protected const ATTR_CREATEUR = '@createur';
    protected const ATTR_PLATEFORME = '@plateforme';
    protected const ATTR_DISPOSITIFS = '@dispositifs';
    protected const ATTR_ETAT = '@etat';
    protected const ATTR_EXPERIMENTATEURS = '@experimentateurs';
    protected const TAG_CODES_ETATS = 'codesEtats';
    protected const TAG_SECTION_CHEMINEMENTS = 'sectionCheminements';
    protected const ATTR_CHEMINEMENTS_CALCULES = '@cheminementsCalcules';
    protected const TAG_CHEMINEMENT_CALCULES = 'cheminementCalcules';
    protected const ATTR_NAME = '@name';
    protected const TAG_VARIABLES_CONNECTEES = 'variablesConnectees';

    protected function getClass(): string
    {
        return DataEntryProject::class;
    }

    protected function extractData($object, array $context): array
    {
        $hasCheminementsCalcules = null === $object->getResponseFile() && $object->getWorkpaths()->count() > 0;
        $sectionCheminements = $hasCheminementsCalcules ? [
            self::ATTR_NAME => 'Cheminement',
            self::TAG_CHEMINEMENT_CALCULES => $object->getWorkpaths(),
        ] : null;
        if ($hasCheminementsCalcules) {
            $writerHelper = self::getWriterHelper($context);
            $sectionCheminementsReference = $writerHelper->createReference($context[self::REFERENCE]->getReference(), self::TAG_SECTION_CHEMINEMENTS);
            foreach ($object->getWorkpaths() as $i => $workpath) {
                self::getWriterHelper($context)->register($workpath, $sectionCheminementsReference, self::TAG_CHEMINEMENT_CALCULES, $i);
            }
        }

        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreationDate(),
            self::ATTR_CREATEUR => $object->getCreator() ? new ReferenceDto($object->getCreator()) : null,
            self::ATTR_PLATEFORME => new ReferenceDto($object->getPlatform()),
            self::ATTR_DISPOSITIFS => new ReferencesDto($object->getPlatform()->getDevices()),
            self::ATTR_ETAT => $object->isImprovised() ? 'improvise' : 'transferer',
            self::ATTR_EXPERIMENTATEURS => new ReferencesDto($object->getDesktopUsers()),
            self::TAG_VARIABLES => array_merge($object->getUniqueVariables()->getValues(), $object->getGeneratorVariables()->getValues()),
            self::TAG_MATERIEL => $object->getMaterials(),
            self::TAG_CODES_ETATS => $object->getStateCodes(),
            self::TAG_SECTION_CHEMINEMENTS => $sectionCheminements,
            self::ATTR_CHEMINEMENTS_CALCULES => $hasCheminementsCalcules ? new ReferencesDto($object->getWorkpaths()) : null,
            self::TAG_VARIABLES_CONNECTEES => array_merge($object->getConnectedUniqueVariables()->getValues(), $object->getConnectedGeneratorVariables()->getValues()),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::TAG_METADONNEES_A_SAISIR => XmlImportConfig::collection(RequiredAnnotation::class),
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::TAG_MATERIEL => XmlImportConfig::collection(Material::class),
            self::TAG_VARIABLES => XmlImportConfig::values(VariableAndTestDto::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (($data[self::ATTR_TYPE_SAISIE] ?? '') === 'spatialisation') {
            throw new ParsingException('', RequestFile::ERROR_INCORRECT_FILE_TYPE);
        }
        if (($data[self::ATTR_NOM] ?? '') === '' || ($data[self::ATTR_DATE_CREATION] ?? '') === '') {
            throw new ParsingException('', RequestFile::ERROR_DATA_ENTRY_PROJECT_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): DataEntryProject
    {
        return new DataEntryProject();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): DataEntryProject
    {
        $variables = VariableAndTestDtoNormalizer::getVariablesAndCreateTests($data[self::TAG_VARIABLES], $this->serializer);

        $object
            ->setImprovised(false)
            ->setRequiredAnnotations($data[self::TAG_METADONNEES_A_SAISIR])
            ->setName($data[self::ATTR_NOM])
            ->setCreationDate($data[self::ATTR_DATE_CREATION])
            ->setMaterials($data[self::TAG_MATERIEL])
            ->setVariables($variables);

        return $object;
    }
}
