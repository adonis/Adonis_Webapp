<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Project\Entity\NatureZHE;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<NatureZHE, NatureZHEXmlDto>
 *
 * @psalm-type NatureZHEXmlDto = array{
 *      "@couleur": ?string,
 *      "@nom": ?string,
 *      "@texture": ?string
 * }
 */
class NatureZheNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_COULEUR = '@couleur';
    protected const ATTR_TEXTURE = '@texture';

    protected function getClass(): string
    {
        return NatureZHE::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_COULEUR => $object->getColor(),
            self::ATTR_NOM => $object->getName(),
            self::ATTR_TEXTURE => $object->getTexture(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_COULEUR => 'string',
            self::ATTR_TEXTURE => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): NatureZHE
    {
        return new NatureZHE();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): NatureZHE
    {
        $object
            ->setName($data[self::ATTR_NOM] ?? '')
            ->setColor($data[self::ATTR_COULEUR])
            ->setTexture($data[self::ATTR_TEXTURE]);

        return $object;
    }
}
