<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\Device;
use Mobile\Project\Entity\Platform;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Platform, PlatformXmlDto>
 *
 * @psalm-type PlatformXmlDto = array{
 *      "@dateCreation": ?\DateTime,
 *      "@nom": ?string,
 *      "@nomLieu": ?string,
 *      "@nomSite": ?string,
 *      dispositifs: Collection<int, Device>
 * }
 */
class PlatformNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM_SITE = '@nomSite';
    protected const ATTR_NOM_LIEU = '@nomLieu';
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const TAG_DISPOSITIFS = 'dispositifs';

    protected function getClass(): string
    {
        return Platform::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreationDate(),
            self::ATTR_NOM_SITE => $object->getSite(),
            self::ATTR_NOM_LIEU => $object->getPlace(),
            self::TAG_DISPOSITIFS => $object->getDevices(),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (
            !isset($data[self::ATTR_NOM])
            || !isset($data[self::ATTR_NOM_SITE])
            || !isset($data[self::ATTR_NOM_LIEU])
            || !isset($data[self::ATTR_DATE_CREATION])
        ) {
            throw new ParsingException('', RequestFile::ERROR_PLATFORM_MISSING);
        }

        if (!isset($data[self::TAG_DISPOSITIFS])) {
            throw new ParsingException('', RequestFile::ERROR_NO_DEVICE);
        }
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM_SITE => 'string',
            self::ATTR_NOM_LIEU => 'string',
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::TAG_DISPOSITIFS => XmlImportConfig::collection(Device::class),
        ];
    }

    protected function generateImportedObject($data, array $context): Platform
    {
        return new Platform();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Platform
    {
        $object
            ->setSite($data[self::ATTR_NOM_SITE] ?? '')
            ->setPlace($data[self::ATTR_NOM_LIEU] ?? '')
            ->setName($data[self::ATTR_NOM] ?? '')
            ->setCreationDate($data[self::ATTR_DATE_CREATION])
            ->setDevices($data[self::TAG_DISPOSITIFS]);

        return $object;
    }
}
