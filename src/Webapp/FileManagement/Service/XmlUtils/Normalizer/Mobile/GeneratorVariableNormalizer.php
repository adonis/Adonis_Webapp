<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\GeneratorVariable;
use Mobile\Measure\Entity\Variable\PreviousValue;
use Mobile\Measure\Entity\Variable\Scale;
use Webapp\Core\Entity\Device;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends VariableNormalizer<GeneratorVariable, GeneratorVariableXmlDto>
 *
 * @psalm-type GeneratorVariableXmlDto = array{
 *       "@active": ?bool,
 *       "@commentaire": ?string,
 *       "@dateCreation": ?\DateTime,
 *       "@dateDerniereModification": ?\DateTime,
 *       "@format": ?string,
 *       "@horodatage": ?bool,
 *       "@materiel": ?Device,
 *       "@nbRepetitionSaisies": ?int,
 *       "@nom": ?string,
 *       "@nomCourt": ?string,
 *       "@ordre": ?int,
 *       "@saisieObligatoire": ?bool,
 *       "@typeVariable": ?string,
 *       "@unite": ?string,
 *       "@uniteParcoursType": ?string,
 *       "@valeurDefaut": ?string,
 *       echelle: ?Scale,
 *       mesuresVariablesPrechargees: Collection<int, PreviousValue>,
 *      "@extension": ?string,
 *      "@nomGenere": ?string,
 *      variablesGenerees: Collection<int, Variable>,
 * }
 */
class GeneratorVariableNormalizer extends VariableNormalizer
{
    public const TAG_VARIABLES_GENEREES = 'variablesGenerees';
    protected const ATTR_EXTENSION = '@extension';

    protected function getClass(): string
    {
        return GeneratorVariable::class;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return parent::supportsDenormalization($data, $type, $format, $context)
            || (Variable::class === $type && isset($data[self::ATTR_NOM_GENERE]));
    }

    protected function extractData($object, array $context): array
    {
        return array_merge(parent::extractCommonData($object, $context), [
            self::ATTR_NOM_GENERE => $object->getGeneratedPrefix(),
            self::ATTR_EXTENSION => $object->isNumeralIncrement() ? 'numerique' : 'alphabetique',
            self::ATTR_XSI_TYPE => 'adonis.modeleMetier.projetDeSaisie.variables:VariableGeneratrice',
            self::TAG_VARIABLES_GENEREES => array_merge($object->getUniqueVariables()->getValues(), $object->getGeneratorVariables()->getValues()),
        ]);
    }

    protected function getImportDataConfig(array $context): array
    {
        return array_merge(parent::getCommonImportDataConfig($context), [
            self::ATTR_NOM_GENERE => 'string',
            self::ATTR_EXTENSION => 'string',
            self::TAG_VARIABLES_GENEREES => XmlImportConfig::values(Variable::class),
        ]);
    }

    protected function generateImportedObject($data, array $context): GeneratorVariable
    {
        return new GeneratorVariable();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context)
    {
        $object = parent::completeImportedObject($object, $data, $format, $context);
        $object
            ->setGeneratedPrefix($data[self::ATTR_NOM_GENERE] ?? '')
            ->setNumeralIncrement(null !== $data[self::ATTR_EXTENSION])
            ->setGeneratedVariables($data[self::TAG_VARIABLES_GENEREES]);

        return $object;
    }
}
