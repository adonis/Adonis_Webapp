<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Project\Entity\GraphicalStructure;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<GraphicalStructure, GraphicalStructureXmlDto>
 *
 * @psalm-type GraphicalStructureXmlDto = array{
 *      "@couleurBloc": ?string,
 *      "@couleurDispositif": ?string,
 *      "@couleurIndAnomalie": ?string,
 *      "@couleurIndividu": ?string,
 *      "@couleurMailleVide": ?string,
 *      "@couleurPlateforme": ?string,
 *      "@couleurPuAnomalie": ?string,
 *      "@couleurPuIndividuel": ?string,
 *      "@couleurPuSurfacique": ?string,
 *      "@couleurSousBloc": ?string,
 *      "@tooltipActive": ?bool,
 *      labelsBloc: ?string,
 *      labelsDispo: ?string,
 *      labelsIndividu: ?string,
 *      labelsPuIndividuel: ?string,
 *      labelsPuSurfacique: ?string,
 *      labelsSousBloc: ?string
 * }
 */
class GraphicalStructureNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_COULEUR_IND_ANOMALIE = '@couleurIndAnomalie';
    protected const ATTR_TOOLTIP_ACTIVE = '@tooltipActive';
    protected const ATTR_COULEUR_PU_SURFACIQUE = '@couleurPuSurfacique';
    protected const ATTR_COULEUR_INDIVIDU = '@couleurIndividu';
    protected const ATTR_COULEUR_SOUS_BLOC = '@couleurSousBloc';
    protected const ATTR_COULEUR_MAILLE_VIDE = '@couleurMailleVide';
    protected const ATTR_COULEUR_PLATEFORME = '@couleurPlateforme';
    protected const ATTR_COULEUR_PU_INDIVIDUEL = '@couleurPuIndividuel';
    protected const ATTR_COULEUR_DISPOSITIF = '@couleurDispositif';
    protected const ATTR_COULEUR_BLOC = '@couleurBloc';
    protected const ATTR_COULEUR_PU_ANOMALIE = '@couleurPuAnomalie';
    protected const TAG_LABELS_PU_SURFACIQUE = 'labelsPuSurfacique';
    protected const TAG_LABELS_SOUS_BLOC = 'labelsSousBloc';
    protected const TAG_LABELS_PU_INDIVIDUEL = 'labelsPuIndividuel';
    protected const TAG_LABELS_INDIVIDU = 'labelsIndividu';
    protected const TAG_LABELS_DISPO = 'labelsDispo';
    protected const TAG_LABELS_BLOC = 'labelsBloc';

    protected function getClass(): string
    {
        return GraphicalStructure::class;
    }

    protected function extractData($object, array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_COULEUR_IND_ANOMALIE => 'string',
            self::ATTR_TOOLTIP_ACTIVE => 'bool',
            self::ATTR_COULEUR_PU_SURFACIQUE => 'string',
            self::ATTR_COULEUR_INDIVIDU => 'string',
            self::ATTR_COULEUR_SOUS_BLOC => 'string',
            self::ATTR_COULEUR_MAILLE_VIDE => 'string',
            self::ATTR_COULEUR_PLATEFORME => 'string',
            self::ATTR_COULEUR_PU_INDIVIDUEL => 'string',
            self::ATTR_COULEUR_DISPOSITIF => 'string',
            self::ATTR_COULEUR_BLOC => 'string',
            self::ATTR_COULEUR_PU_ANOMALIE => 'string',
            self::TAG_LABELS_PU_SURFACIQUE => 'string',
            self::TAG_LABELS_SOUS_BLOC => 'string',
            self::TAG_LABELS_PU_INDIVIDUEL => 'string',
            self::TAG_LABELS_INDIVIDU => 'string',
            self::TAG_LABELS_DISPO => 'string',
            self::TAG_LABELS_BLOC => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): GraphicalStructure
    {
        return new GraphicalStructure();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): GraphicalStructure
    {
        $object
            ->setAbnormalIndividualColor($data[self::ATTR_COULEUR_IND_ANOMALIE] ?? '')
            ->setTooltipActive($data[self::ATTR_TOOLTIP_ACTIVE] ?? false)
            ->setSurfaceUnitParcelColor($data[self::ATTR_COULEUR_PU_SURFACIQUE] ?? '')
            ->setIndividualColor($data[self::ATTR_COULEUR_INDIVIDU] ?? '')
            ->setSubBlockColor($data[self::ATTR_COULEUR_SOUS_BLOC] ?? '')
            ->setEmptyColor($data[self::ATTR_COULEUR_MAILLE_VIDE] ?? '')
            ->setPlatformColor($data[self::ATTR_COULEUR_PLATEFORME] ?? '')
            ->setIndividualUnitParcelColor($data[self::ATTR_COULEUR_PU_INDIVIDUEL] ?? '')
            ->setDeviceColor($data[self::ATTR_COULEUR_DISPOSITIF] ?? '')
            ->setBlockColor($data[self::ATTR_COULEUR_BLOC] ?? '')
            ->setAbnormalUnitParcelColor($data[self::ATTR_COULEUR_PU_ANOMALIE] ?? '')
            ->setSurfaceUnitParcelLabel($data[self::TAG_LABELS_PU_SURFACIQUE] ?? '')
            ->setSubBlockLabel($data[self::TAG_LABELS_SOUS_BLOC] ?? '')
            ->setIndividualUnitParcelLabel($data[self::TAG_LABELS_PU_INDIVIDUEL] ?? '')
            ->setIndividualLabel($data[self::TAG_LABELS_INDIVIDU] ?? '')
            ->setDeviceLabel($data[self::TAG_LABELS_DISPO] ?? '')
            ->setBlockLabel($data[self::TAG_LABELS_BLOC] ?? '');

        return $object;
    }
}
