<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Device\Entity\Modality;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Modality, ModalityXmlDto>
 *
 * @psalm-type ModalityXmlDto = array{"@valeur": ?string}
 */
class ModalityNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_VALEUR = '@valeur';

    protected function getClass(): string
    {
        return Modality::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_VALEUR => $object->getValue(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_VALEUR => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_VALEUR])) {
            throw new ParsingException('', RequestFile::ERROR_PROTOCOL_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Modality
    {
        return new Modality();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Modality
    {
        $object
            ->setValue($data[self::ATTR_VALEUR]);

        return $object;
    }
}
