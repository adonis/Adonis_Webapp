<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\StateCode;
use Webapp\FileManagement\Exception\ParsingException;

class StateCodeContext
{
    private array $stateCodes = [];

    /**
     * @param StateCode[] $stateCodes
     */
    public function set(array $stateCodes): void
    {
        $this->stateCodes = [];
        foreach ($stateCodes as $stateCode) {
            $this->add($stateCode);
        }
    }

    public function add(StateCode $stateCode): void
    {
        $this->stateCodes[$stateCode->getCode()] = $stateCode;
    }

    public function get(StateCode $stateCode): StateCode
    {
        if (!$this->exists($stateCode->getCode())) {
            throw new ParsingException("State code '{$stateCode->getCode()}' does not exist");
        }

        return $this->stateCodes[$stateCode->getCode()];
    }

    public function exists(string $code): bool
    {
        return \array_key_exists($code, $this->stateCodes);
    }
}
