<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\DataEntry;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<DataEntry, array{}>
 */
class DataEntryNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_PROJET_DE_SAISIE = '@projetDeSaisie';
    protected const ATTR_DATE_DEBUT = '@dateDebut';
    protected const ATTR_DATE_FIN = '@dateFin';
    protected const ATTR_STATUS_SAISIE = '@statusSaisie';
    protected const ATTR_CHEMINEMENT_IMPOSE = '@cheminementImpose';
    protected const ATTR_PLATEFORME = '@plateforme';
    protected const TAG_VARIABLES = 'variables';
    protected const TAG_MATERIEL = 'materiel';
    protected const TAG_SESSIONS = 'sessions';

    protected function getClass(): string
    {
        return DataEntry::class;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $overrideContext = self::overrideWriterHelper($context, [
            self::TAG_VARIABLES,
            self::TAG_MATERIEL,
            GeneratorVariableNormalizer::TAG_VARIABLES_GENEREES,
            UniqueVariableNormalizer::TAG_LISTEVALEUR,
            ValueHintListNormalizer::TAG_VALEURS,
        ]);

        return parent::normalize($object, $format, $overrideContext);
    }

    protected function extractData($object, array $context): array
    {
        $project = $object->getProject();

        return [
            self::ATTR_PROJET_DE_SAISIE => new ReferenceDto($project),
            self::ATTR_DATE_DEBUT => $object->getStartedAt(),
            self::ATTR_DATE_FIN => $object->getEndedAt(),
            self::ATTR_STATUS_SAISIE => 'terminee',
            self::ATTR_CHEMINEMENT_IMPOSE => $project->getWorkpaths()->count() > 0,
            self::ATTR_PLATEFORME => new ReferenceDto($project->getPlatform()),
            self::TAG_VARIABLES => array_merge($project->getUniqueVariables()->getValues(), $project->getGeneratorVariables()->getValues()),
            self::TAG_MATERIEL => $project->getMaterials(),
            self::TAG_SESSIONS => $object->getSessions(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function generateImportedObject($data, array $context)
    {
        throw new \LogicException('Method not implemented');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context)
    {
        throw new \LogicException('Method not implemented');
    }
}
