<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Annotation;
use Mobile\Measure\Entity\FormField;
use Mobile\Measure\Entity\GeneratedField;
use Mobile\Measure\Entity\Measure;
use Mobile\Measure\Entity\Session;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;
use Webapp\FileManagement\Dto\Mobile\MeasureDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Session, SessionXmlDto>
 *
 * @psalm-type SessionXmlDto = array{
 *      "@dateDebut": ?\DateTime,
 *      "@dateFin": ?\DateTime,
 *      "@isAuthenticated": ?bool,
 *      mesureVariables: MeasureDto[],
 *      metadonnees: Collection<int, Annotation>
 * }
 */
class SessionNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_DATE_DEBUT = '@dateDebut';
    protected const ATTR_DATE_FIN = '@dateFin';
    protected const ATTR_IS_AUTHENTICATED = '@isAuthenticated';
    protected const TAG_MESURE_VARIABLES = 'mesureVariables';
    protected const TAG_METADONNEES = 'metadonnees';

    protected function getClass(): string
    {
        return Session::class;
    }

    protected function extractData($object,
        array $context): array
    {
        $measures = $this->extractMeasures($object);

        /** @var AnnotationDto[] $annotations */
        $annotations = [];
        foreach ($object->getFormFields() as $formField) {
            foreach ($formField->getMeasures() as $measure) {
                $annotationsDtos = AnnotationDto::fromCollection($measure->getAnnotations(), $measure);
                $annotations = array_merge($annotations, $annotationsDtos);
            }
        }

        if ($object === $object->getDataEntry()->getSessions()->last()) {
            $otherAnnotations = self::getWriterHelper($context)->getSessionAnnotations();
            $annotations = array_merge($annotations, $otherAnnotations);
        }

        return [
            self::ATTR_DATE_DEBUT => $object->getStartedAt(),
            self::ATTR_DATE_FIN => $object->getEndedAt(),
            self::ATTR_IS_AUTHENTICATED => true,
            self::TAG_MESURE_VARIABLES => $measures,
            self::TAG_METADONNEES => $annotations,
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_DATE_DEBUT => \DateTime::class,
            self::ATTR_DATE_FIN => \DateTime::class,
            self::ATTR_IS_AUTHENTICATED => 'bool',
            self::TAG_MESURE_VARIABLES => XmlImportConfig::values(MeasureDto::class),
            self::TAG_METADONNEES => XmlImportConfig::values(Annotation::class),
        ];
    }

    protected function generateImportedObject($data, array $context): Session
    {
        return new Session();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Session
    {
        $object
            ->setStartedAt($data[self::ATTR_DATE_DEBUT])
            ->setEndedAt($data[self::ATTR_DATE_FIN]);

        $readerHelper = self::getReaderHelper($context);
        /** @var array<string, GeneratedField> $generatedVariables `generatedMeasureUri` as key */
        $generatedVariables = [];
        /** @var array<string, FormField> $generatorVariables `generatorMeasureUri` as key */
        $generatorVariables = [];
        /** @var array<string, GeneratedField> $generatorFields `generatorIndex . generatorMeasureUri` as key */
        $generatorFields = [];
        /** @var array<string, FormField> $repetitionMap `generatorIndex . generatorMeasure . variable . businessObject` as key */
        $repetitionMap = [];

        /** @var MeasureDto $item */
        foreach ($data[self::TAG_MESURE_VARIABLES] as $item) {
            //            $item->measure->setTimestamp($data[self::ATTR_HORODATAGE]);
            $measureUri = $readerHelper->getReference($item->measure);

            // Handle repetitions (not available on generatorVariables)
            $repetitionUri = ($item->indiceGeneratrice ?? '')
                .($item->mesureGeneratrice ?? '')
                .$readerHelper->getReference($item->variable)
                .$readerHelper->getReference($item->objetMetier);
            if (!isset($repetitionMap[$repetitionUri])) {
                $fieldMeasure = (new FormField())
                    ->setVariable($item->variable)
                    ->setTarget($item->objetMetier);

                $repetitionMap[$repetitionUri] = $fieldMeasure;

                // Generator values
                if (null !== $item->mesuresGenerees) {
                    foreach (explode(' ', $item->mesuresGenerees) as $measureUri) {
                        $generatorVariables[$measureUri] = $fieldMeasure;
                        if (isset($generatedVariables[$measureUri])) {
                            $fieldMeasure->addGeneratedField($generatedVariables[$measureUri]);
                        }
                    }
                }

                // Generated values
                if (null !== $item->mesureGeneratrice) {
                    $generatorFieldUri = ($item->indiceGeneratrice ?? '').$item->mesureGeneratrice;
                    if (!isset($generatorFields[$generatorFieldUri])) {
                        $generatorFields[$generatorFieldUri] = (new GeneratedField())
                            ->setIndex($item->indiceGeneratrice ?? 0)
                            ->setNumeralIncrement(false) // TODO prendre les vraies valeur ou retirer ces attributs de la classe (redondance avec la variable)
                            ->setPrefix($fieldMeasure->getVariable()->getGeneratorVariable()->getGeneratedPrefix());
                    }
                    $generatorFields[$generatorFieldUri]->addFormField($fieldMeasure);

                    // If the generator measure was already found before
                    if (isset($generatorVariables[$item->mesureGeneratrice])) {
                        $generatorVariables[$item->mesureGeneratrice]->addGeneratedField($generatorFields[$generatorFieldUri]);
                    } else {
                        $generatedVariables[$measureUri] = $generatorFields[$generatorFieldUri];
                    }
                } else {
                    $object->addFormField($fieldMeasure);
                }
            }
            $item->measure->setRepetition($repetitionMap[$repetitionUri]->getMeasures()->count());
            $repetitionMap[$repetitionUri]->addMeasure($item->measure);
        }

        return $object;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $data = parent::normalize($object, $format, $context);
        \assert($object instanceof Session);

        $writerHelper = self::getWriterHelper($context);

        $measures = array_map(fn (Measure $measure) => $measure->getUri(), $this->extractMeasures($object));
        foreach ($object->getFormFields() as $formField) {
            if ($formField->getGeneratedFields()->count() > 0) {
                $measureGeneratedReferences = [];
                foreach ($formField->getGeneratedFields() as $generatedField) {
                    foreach ($generatedField->getFormFields() as $generatedFormField) {
                        foreach ($generatedFormField->getMeasures() as $generatedMeasure) {
                            $measureGeneratedReferences[] = $writerHelper->get($generatedMeasure->getUri());
                        }
                    }
                }
                foreach ($formField->getMeasures() as $measure) {
                    $measureIndex = array_search($measure->getUri(), $measures, true);
                    $data[self::TAG_MESURE_VARIABLES][$measureIndex][MeasureNormalizer::ATTR_MESURES_GENEREES] = implode(' ', $measureGeneratedReferences);
                }
            }
        }

        return $data;
    }

    /**
     * @return Measure[]
     */
    protected function extractMeasures(Session $object): array
    {
        $measures = array_reduce($object->getFormFields()->getValues(), fn (array $carry, FormField $formField) => array_merge($carry, $formField->getMeasures()->getValues()), []);
        foreach ($object->getFormFields() as $formField) {
            // In the generator variable case, only one measure is concerned by the formField
            foreach ($formField->getGeneratedFields() as $generatedField) {
                foreach ($generatedField->getFormFields() as $generatedFormField) {
                    $measures = array_merge($measures, $generatedFormField->getMeasures()->getValues());
                }
            }
        }

        return $measures;
    }
}
