<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\ValueHint;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<ValueHint, ValueHintXmlDto>
 *
 * @psalm-type ValueHintXmlDto = array{
 *     "#": ?string
 * }
 */
class ValueHintNormalizer extends AbstractXmlNormalizer
{
    protected function getClass(): string
    {
        return ValueHint::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            '#' => $object->getValue(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            '#' => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): ValueHint
    {
        return new ValueHint();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): ValueHint
    {
        $object
            ->setValue($data['#']);

        return $object;
    }
}
