<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\PreviousValue;
use Mobile\Measure\Entity\Variable\Scale;
use Mobile\Measure\Entity\Variable\UniqueVariable;
use Mobile\Measure\Entity\Variable\ValueHintList;
use Webapp\Core\Entity\Device;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends VariableNormalizer<UniqueVariable, UniqueVariableXmlDto>
 *
 * @psalm-type UniqueVariableXmlDto = array{
 *        "@active": ?bool,
 *        "@commentaire": ?string,
 *        "@dateCreation": ?\DateTime,
 *        "@dateDerniereModification": ?\DateTime,
 *        "@format": ?string,
 *        "@horodatage": ?bool,
 *        "@materiel": ?Device,
 *        "@nbRepetitionSaisies": ?int,
 *        "@nom": ?string,
 *        "@nomCourt": ?string,
 *        "@ordre": ?int,
 *        "@saisieObligatoire": ?bool,
 *        "@typeVariable": ?string,
 *        "@unite": ?string,
 *        "@uniteParcoursType": ?string,
 *        "@valeurDefaut": ?string,
 *        echelle: ?Scale,
 *        mesuresVariablesPrechargees: Collection<int, PreviousValue>,
 *        listevaleur: ?ValueHintList,
 * }
 */
class UniqueVariableNormalizer extends VariableNormalizer
{
    public const TAG_LISTEVALEUR = 'listevaleur';

    protected function getClass(): string
    {
        return UniqueVariable::class;
    }

    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return parent::supportsDenormalization($data, $type, $format, $context)
            || (Variable::class === $type && !isset($data[self::ATTR_NOM_GENERE]));
    }

    protected function extractData($object, array $context): array
    {
        return array_merge(parent::extractCommonData($object, $context), [
            self::TAG_LISTEVALEUR => $object->getValueHintList(),
        ]);
    }

    protected function getImportDataConfig(array $context): array
    {
        return array_merge(parent::getCommonImportDataConfig($context), [
            self::TAG_LISTEVALEUR => XmlImportConfig::value(ValueHintList::class),
        ]);
    }

    protected function generateImportedObject($data, array $context): UniqueVariable
    {
        return new UniqueVariable();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context)
    {
        $object = parent::completeImportedObject($object, $data, $format, $context);
        $object
            ->setValueHintList($data[self::TAG_LISTEVALEUR]);

        return $object;
    }
}
