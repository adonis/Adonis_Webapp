<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Device\Entity\Anomaly;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Anomaly, array{}>
 */
class AnomalyNormalizer extends AbstractXmlNormalizer
{
    protected function getClass(): string
    {
        return Anomaly::class;
    }

    protected function extractData($object, array $context): array
    {
        $badTreatment = [];
        if (Anomaly::TYPE_BAD_TREATMENT === $object->getType()) {
            $badTreatment['@typeAnomalie'] = 'individuMauvaisTraitement';

            if (null !== $object->getConstatedTreatment()) {
                $badTreatment['@traitementConstate'] = new ReferenceDto($object->getConstatedTreatment());
            }
        }

        return array_merge([
            '@objetMetier' => new ReferenceDto($object->getBusinessObject()),
            '@description' => $object->getDescription(),
        ], $badTreatment);
    }

    protected function getImportDataConfig(array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function generateImportedObject($data, array $context): Anomaly
    {
        throw new \LogicException('Method not implemented');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Anomaly
    {
        throw new \LogicException('Method not implemented');
    }
}
