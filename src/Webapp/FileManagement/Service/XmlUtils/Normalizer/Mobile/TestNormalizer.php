<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Test\Base\Test;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template U of Test
 * @template UImportType of TestXmlDto&array<array-key, mixed>
 *
 * @template-extends AbstractXmlNormalizer<U, UImportType>
 *
 * @psalm-type TestXmlDto = array{
 *      "@active": ?bool,
 *      "@nom": ?string
 * }
 */
abstract class TestNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_ACTIVE = '@active';
    protected const ATTR_NOM = '@nom';

    protected const ATTR_MAX_DOUBLE = '@maxDouble';
    protected const ATTR_MIN_DOUBLE = '@minDouble';

    protected function getClass(): string
    {
        return Test::class;
    }

    /**
     * @param U $object
     */
    abstract protected function extractData($object, array $context): array;

    /**
     * @param U $object
     */
    final protected function extractCommonData($object, array $context): array
    {
        return [
            self::ATTR_ACTIVE => $object->isActive(),
        ];
    }

    abstract protected function getImportDataConfig(array $context): array;

    /**
     * @return array{"@active": string, "@nom": string}
     */
    final protected function getCommonImportDataConfig(array $context): array
    {
        return [
            self::ATTR_ACTIVE => 'bool',
            self::ATTR_NOM => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM]) || !isset($data[self::ATTR_ACTIVE])) {
            throw new ParsingException('', RequestFile::ERROR_TEST_INFO_MISSING);
        }
    }

    /**
     * @param U     $object
     * @param array $data
     *
     * @return U
     */
    protected function completeImportedObject($object, $data, ?string $format, array $context): Test
    {
        $object
            ->setActive($data[self::ATTR_ACTIVE])
            ->setName($data[self::ATTR_NOM]);

        return $object;
    }
}
