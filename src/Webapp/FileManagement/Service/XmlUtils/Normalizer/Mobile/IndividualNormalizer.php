<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\Individual;
use Mobile\Measure\Entity\Variable\StateCode;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Individual, IndividualXmlDto>
 *
 * @psalm-type IndividualXmlDto = array{
 *      "@dateApparition": ?\DateTime,
 *      "@dateDisparition": ?\DateTime,
 *      "@idRfidCodeBarre": ?string,
 *      "@numero": ?string,
 *      "@x": ?int,
 *      "@y": ?int,
 *      codeEtat: ?StateCode,
 *      notes: Collection<int, EntryNote>
 * }
 */
class IndividualNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_X = '@x';
    protected const ATTR_Y = '@y';
    protected const ATTR_NUMERO = '@numero';
    protected const ATTR_DATE_APPARITION = '@dateApparition';
    protected const ATTR_DATE_DISPARITION = '@dateDisparition';
    protected const ATTR_ID_RFID_CODE_BARRE = '@idRfidCodeBarre';
    protected const TAG_CODE_ETAT = 'codeEtat';
    protected const TAG_NOTES = 'notes';

    protected function getClass(): string
    {
        return Individual::class;
    }

    protected function extractData($object, array $context): array
    {
        self::getWriterHelper($context)->addSessionAnnotations(AnnotationDto::fromCollection($object->getAnnotations(), $object));

        if (null !== $object->getAnomaly()) {
            self::getWriterHelper($context)->addAnomaly($object->getAnomaly());
        }

        return [
            self::ATTR_X => $object->getX(),
            self::ATTR_Y => $object->getY(),
            self::ATTR_NUMERO => $object->getName(),
            self::ATTR_DATE_DISPARITION => $object->getDemiseDate(),
            self::ATTR_DATE_APPARITION => $object->getApparitionDate(),
            self::ATTR_ID_RFID_CODE_BARRE => $object->getIdent(),
            self::TAG_CODE_ETAT => $object->getStateCode(),
            self::TAG_NOTES => $object->getNotes(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_X => 'int',
            self::ATTR_Y => 'int',
            self::ATTR_NUMERO => 'string',
            self::ATTR_DATE_APPARITION => \DateTime::class,
            self::ATTR_DATE_DISPARITION => \DateTime::class,
            self::ATTR_ID_RFID_CODE_BARRE => 'string',
            self::TAG_CODE_ETAT => XmlImportConfig::value(StateCode::class),
            self::TAG_NOTES => XmlImportConfig::collection(EntryNote::class),
        ];
    }

    protected function generateImportedObject($data, array $context): Individual
    {
        return new Individual();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Individual
    {
        $object
            ->setX($data[self::ATTR_X])
            ->setY($data[self::ATTR_Y])
            ->setApparitionDate($data[self::ATTR_DATE_APPARITION])
            ->setDemiseDate($data[self::ATTR_DATE_DISPARITION])
            ->setIdent($data[self::ATTR_ID_RFID_CODE_BARRE])
            ->setStateCode($data[self::TAG_CODE_ETAT])
            ->setNotes($data[self::TAG_NOTES])
            ->setName($data[self::ATTR_NUMERO] ?? '0');

        return $object;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        $normalized = parent::normalize($object, $format, $context);

        $writerHelper = self::getWriterHelper($context);

        \assert($object instanceof Individual);
        if (null !== $object->getStateCode()) {
            // Replace URI for state code to make it specific for the Individual
            $writerHelper->remove($object->getStateCode()->getUri());
            $writerHelper->add($object->getUri().$object->getStateCode()->getUri(), $writerHelper->get($object->getUri()).'/@codeEtat');
        }

        return $normalized;
    }
}
