<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Device\Entity\UnitParcel;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\StateCode;
use Mobile\Project\Entity\DataEntryProject;
use Mobile\Project\Entity\DesktopUser;
use Mobile\Project\Entity\GraphicalStructure;
use Mobile\Project\Entity\NatureZHE;
use Mobile\Project\Entity\Platform;
use Mobile\Project\Entity\Workpath;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Webapp\FileManagement\Dto\Mobile\CommonDataDto;
use Webapp\FileManagement\Dto\Mobile\CommonDto;
use Webapp\FileManagement\Dto\Mobile\DataEntryWithMaterialsAndVariablesDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\CustomXmlEncoder;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractRootNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;
use Webapp\FileManagement\Service\XmlUtils\ReferenceContext;

/**
 * @template T of CommonDto
 *
 * @template-extends AbstractRootNormalizer<T, array{}>
 */
abstract class CommonDtoNormalizer extends AbstractRootNormalizer
{
    use SerializerAwareTrait;

    public const STATE_CODES = 'stateCodes';

    protected const TAG_PROJET_SAISIE = 'adonis.modeleMetier.projetDeSaisie:ProjetDeSaisie';
    protected const TAG_PROJECT_STATE_CODE = 'codesEtats';
    protected const TAG_STATE_CODE = 'adonis.modeleMetier.conceptsDeBase:CodeEtat';
    protected const TAG_UTILISATEUR = 'adonis.modeleMetier.utilisateur:Utilisateur';
    protected const ATTR_PROJECT_CREATOR = '@createur';
    protected const TAG_NATURE_ZHE = 'adonis.modeleMetier.plateforme:NatureZhe';
    protected const TAG_PLATFORM = 'adonis.modeleMetier.plateforme:Plateforme';
    protected const TAG_VARIABLES = 'adonis.modeleMetier.projetDeSaisie.variables:'; // TODO
    protected const TAG_GRAPHICAL_STRUCTURE = 'adonis.modeleMetier.graphique:'; // TODO
    protected const TAG_WORKPATH = 'adonis.modeleMetier.projetDeSaisie.cheminement:'; // TODO
    protected const TAG_SAISIE = 'adonis.modeleMetier.saisieTerrain:Saisie';
    protected const TAG_WORKPATH_SECTION_CHEMINEMENTS = 'sectionCheminements';
    protected const TAG_WORKPATH_CHEMINEMENT_CALCULES = 'cheminementCalcules';
    protected const TAG_PLATFORM_STRUCTURE_GRAPHIQUE = 'structureGraphique';
    protected const TAG_STRUCTURE_GRAPHIQUE_ORIGINE = 'origine';
    protected const TAG_STRUCTURE_GRAPHIQUE_HAUTEUR_MAILLE = 'hauteurMaille';
    protected const TAG_STRUCTURE_GRAPHIQUE_LARGEUR_MAILLE = 'largeurMaille';

    protected int $currentParsingStateError = 0;

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        self::initNormalizeContext(null, $context, 1); // Pass index 0 which correspond to AdonisVersion

        return parent::normalize($object, $format, $context);
    }

    /**
     * @param mixed $data
     *
     * @return T
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = []): CommonDto
    {
        AbstractXmlNormalizer::initDenormalizeContext($context);
        $context[self::STATE_CODES] = new StateCodeContext();

        return parent::denormalize($data, $type, $format, $context);
    }

    final protected function getImportDataConfig(array $context): array
    {
        return [];
    }

    /**
     * @param mixed $data
     *
     * @return T
     *
     * @throws ParsingException
     * @throws ExceptionInterface
     */
    protected function denormalizeDataArray(array $config, $data, ?string $format, array $context): CommonDto
    {
        $this->generateImportedObject($data, $context);
        $this->validateImportData($data, $context);

        $mobileDto = $this->extractMobileDto($data, $format, $context);

        return $this->mapMobileDto($mobileDto, $data, $format, $context);
    }

    /**
     * @param T       $object
     * @param array{} $data
     *
     * @return T
     */
    protected function completeImportedObject($object, $data, ?string $format, array $context): CommonDto
    {
        throw new \LogicException('Method must not be called');
    }

    /**
     * @param mixed $data
     *
     * @throws ParsingException
     * @throws ExceptionInterface
     */
    private function extractMobileDto($data, ?string $format, array $context): CommonDataDto
    {
        $mobileDto = new CommonDataDto();
        $mobileDto->stateCodes = $this->readStateCodeCollection($data, $format, $context);
        $mobileDto->desktopUsers = $this->readDesktopUserCollection($data, $format, $context);
        $mobileDto->creatorLogin = $this->readProjectCreatorLogin($data, $format, $context);
        $mobileDto->naturesZhe = $this->readNatureZheCollection($data, $format, $context);
        $mobileDto->platform = $this->readPlatform($data, $format, $context);
        $mobileDto->connectedVariables = $this->readConnectedVariableCollection($data, $format, $context);
        $mobileDto->dataEntryProject = $this->readProject($data, $format, $context);
        $mobileDto->graphicalStructure = $this->readGraphicalStructure($data, $format, $context);
        $mobileDto->paths = $this->readPath($data, $format, $context);
        $mobileDto->dataEntries = $this->readDataEntries($data, $format, $context);

        foreach ($mobileDto->stateCodes as $stateCode) {
            $mobileDto->dataEntryProject->addStateCode($stateCode);
        }
        foreach ($mobileDto->desktopUsers as $user) {
            $mobileDto->dataEntryProject->addDesktopUser($user);
            if ($mobileDto->creatorLogin === $user->getLogin() && !$mobileDto->dataEntryProject->getCreator()) {
                $mobileDto->dataEntryProject->setCreator($user);
            }
        }
        foreach ($mobileDto->naturesZhe as $natureZHE) {
            $mobileDto->dataEntryProject->addNatureZHE($natureZHE);
        }
        $mobileDto->dataEntryProject->setPlatform($mobileDto->platform);
        foreach ($mobileDto->connectedVariables as $connectedVariable) {
            $mobileDto->dataEntryProject->addConnectedVariable($connectedVariable);
        }
        if (null !== $mobileDto->graphicalStructure) {
            $mobileDto->dataEntryProject->setGraphicalStructure($mobileDto->graphicalStructure);
        }
        foreach ($mobileDto->paths as $path) {
            $mobileDto->dataEntryProject->addWorkpath($path);
        }
        foreach ($mobileDto->dataEntries as $dataEntry) {
            $mobileDto->dataEntryProject->setDataEntry($dataEntry->dataEntry);
            foreach ($dataEntry->materials as $material) {
                $mobileDto->dataEntryProject->addMaterial($material);
            }
            foreach ($dataEntry->variables as $variable) {
                $mobileDto->dataEntryProject->addVariable($variable);
            }
        }
        $this->updatePlatformStateCode($mobileDto->dataEntryProject);

        return $mobileDto;
    }

    /**
     * @throws ParsingException
     */
    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::TAG_PROJET_SAISIE])) {
            throw new ParsingException('', RequestFile::ERROR_INCORRECT_FILE_TYPE);
        }
    }

    /**
     * @param mixed $data
     *
     * @return T
     */
    abstract protected function mapMobileDto(CommonDataDto $mobileDto, $data, ?string $format, array $context): CommonDto;

    /**
     * @throws ParsingException
     */
    protected function readProjectCreatorLogin(array $data, ?string $format, array $context): string
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_PROJECT_CREATOR;

        if ('' === trim($data[self::TAG_PROJET_SAISIE][0][self::ATTR_PROJECT_CREATOR] ?? '')) {
            throw new ParsingException('', RequestFile::ERROR_NO_PROJECT_CREATOR);
        }

        return self::getReaderHelper($context)->get($data[self::TAG_PROJET_SAISIE][0][self::ATTR_PROJECT_CREATOR], DesktopUser::class)->getLogin();
    }

    /**
     * @return StateCode[]
     *
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readStateCodeCollection(array $data, ?string $format, array $context): array
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_STATE_CODE;

        $stateCodes = [];
        foreach ($data[self::TAG_PROJET_SAISIE] as $item) {
            $projetSaisieReference = AbstractXmlNormalizer::getReaderHelper($context)->createReference('', null, $item[CustomXmlEncoder::NODE_INDEX]);
            $childrenContext = array_merge([], $context);
            $childrenContext[AbstractXmlNormalizer::REFERENCE] = new ReferenceContext($projetSaisieReference);

            $projectStateCodes = $this->denormalizeDataItem(
                XmlImportConfig::values(StateCode::class),
                $item[self::TAG_PROJECT_STATE_CODE] ?? [],
                self::TAG_PROJECT_STATE_CODE,
                $format,
                $childrenContext
            );

            foreach ($projectStateCodes as $stateCode) {
                $stateCodes[$stateCode->getCode()] = $stateCode;
            }
        }

        // Need to loop over codeEtat namespace to get all previous values state code.
        $globalStateCodes = $this->denormalizeDataItem(
            XmlImportConfig::values(StateCode::class, true),
            $data[self::TAG_STATE_CODE] ?? [], self::TAG_STATE_CODE,
            $format,
            $context
        );
        foreach ($globalStateCodes as $stateCode) {
            if (!\array_key_exists($stateCode->getCode(), $stateCodes)) {
                $stateCodes[$stateCode->getCode()] = $stateCode;
            }
        }

        $context[self::STATE_CODES]->set($stateCodes);

        return $stateCodes;
    }

    /**
     * @return DesktopUser[]
     *
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readDesktopUserCollection(array $data, ?string $format, array $context): array
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_DESKTOP_USER_COLLECTION;

        return $this->denormalizeDataItem(
            XmlImportConfig::values(DesktopUser::class, true),
            $data[self::TAG_UTILISATEUR] ?? [],
            self::TAG_UTILISATEUR,
            $format,
            $context
        );
    }

    /**
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readNatureZheCollection(array $data, ?string $format, array $context): array
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_NATURE_ZHE;

        return $this->denormalizeDataItem(
            XmlImportConfig::values(NatureZHE::class, true),
            $data[self::TAG_NATURE_ZHE] ?? [],
            self::TAG_NATURE_ZHE,
            $format,
            $context
        );
    }

    /**
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readPlatform(array $data, ?string $format, array $context): Platform
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_PLATFORM;

        $platforms = $this->denormalizeDataItem(
            XmlImportConfig::values(Platform::class, true),
            $data[self::TAG_PLATFORM] ?? [],
            self::TAG_PLATFORM,
            $format,
            $context
        );

        if (0 === \count($platforms)) {
            throw new ParsingException('', RequestFile::ERROR_NO_PLATFORM);
        } elseif (\count($platforms) > 1) {
            throw new ParsingException('More than one platform are defined');
        }

        return current($platforms);
    }

    /**
     * @return Variable[]
     *
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readConnectedVariableCollection(array $data, ?string $format, array $context): array
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_CONNECTED_VARIABLE_COLLECTION;

        return $this->denormalizeDataItem(
            XmlImportConfig::values(Variable::class, true),
            $data[self::TAG_VARIABLES] ?? [],
            self::TAG_VARIABLES,
            $format,
            $context
        );
    }

    /**
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readProject(array $data, ?string $format, array $context): DataEntryProject
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_PROJECT;

        $dataEntryProjects = $this->denormalizeDataItem(
            XmlImportConfig::values(DataEntryProject::class, true),
            $data[self::TAG_PROJET_SAISIE] ?? [],
            self::TAG_PROJET_SAISIE,
            $format,
            $context
        );

        if (0 === \count($dataEntryProjects)) {
            throw new ParsingException('No project are defined');
        } elseif (\count($dataEntryProjects) > 1) {
            throw new ParsingException('More than one project are defined');
        }

        return current($dataEntryProjects);
    }

    /**
     * @throws ExceptionInterface
     */
    protected function readGraphicalStructure(array $data, ?string $format, array $context): ?GraphicalStructure
    {
        $this->currentParsingStateError = RequestFile::PARSING_STATE_READ_GRAPHICAL_STRUCTURE;

        if (($data[self::TAG_GRAPHICAL_STRUCTURE] ?? null) !== null) {
            $graphicalStructure = $this->serializer->denormalize($data[self::TAG_GRAPHICAL_STRUCTURE] ?? null, GraphicalStructure::class, $format, $context);
        } else {
            $graphicalStructure = null;
        }

        if (null !== $graphicalStructure && isset($data[self::TAG_PLATFORM][self::TAG_PLATFORM_STRUCTURE_GRAPHIQUE])) {
            $graphicalStructure
                ->setOrigin($data[self::TAG_PLATFORM][self::TAG_PLATFORM_STRUCTURE_GRAPHIQUE][self::TAG_STRUCTURE_GRAPHIQUE_ORIGINE])
                ->setBaseHeight((int) $data[self::TAG_PLATFORM][self::TAG_PLATFORM_STRUCTURE_GRAPHIQUE][self::TAG_STRUCTURE_GRAPHIQUE_HAUTEUR_MAILLE])
                ->setBaseWidth((int) $data[self::TAG_PLATFORM][self::TAG_PLATFORM_STRUCTURE_GRAPHIQUE][self::TAG_STRUCTURE_GRAPHIQUE_LARGEUR_MAILLE]);
        }

        return $graphicalStructure;
    }

    /**
     * @return Workpath[]
     *
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readPath(array $data, ?string $format, array $context): array
    {
        return $this->denormalizeDataItem(
            XmlImportConfig::values(DataEntryProject::class, true),
            $data[self::TAG_WORKPATH][self::TAG_WORKPATH_SECTION_CHEMINEMENTS][self::TAG_WORKPATH_CHEMINEMENT_CALCULES] ?? [],
            self::TAG_WORKPATH.'.'.self::TAG_WORKPATH_SECTION_CHEMINEMENTS.'.'.self::TAG_WORKPATH_CHEMINEMENT_CALCULES,
            $format,
            $context
        );
    }

    /**
     * @return DataEntryWithMaterialsAndVariablesDto[]
     *
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function readDataEntries(array $data, ?string $format, array $context): array
    {
        return $this->denormalizeDataItem(
            XmlImportConfig::values(DataEntryWithMaterialsAndVariablesDto::class, true),
            $data[self::TAG_SAISIE] ?? [],
            self::TAG_SAISIE,
            $format,
            $context
        );
    }

    /**
     * Replace state codes referencing individual or plot with platform state codes.
     */
    private function updatePlatformStateCode(DataEntryProject $dataEntryProject): void
    {
        $dataEntry = $dataEntryProject->getDataEntry();
        if (null === $dataEntry) {
            return;
        }

        // Get all data entry project state codes.
        $stateCodes = [];
        foreach ($dataEntryProject->getStateCodes() as $stateCode) {
            $stateCodes[$stateCode->getCode()] = $stateCode;
        }

        // Replace state code with data entry project state codes.
        foreach ($dataEntry->getProject()->getPlatform()->getDevices() as $device) {
            foreach ($device->getBlocks() as $block) {
                $this->updateUnitParcelsStateCodes($block->getUnitParcels()->getValues(), $stateCodes);
                foreach ($block->getSubBlocks() as $subBlock) {
                    $this->updateUnitParcelsStateCodes($subBlock->getUnitParcels()->getValues(), $stateCodes);
                }
            }
        }
        foreach ($dataEntry->getSessions() as $session) {
            foreach ($session->getFormFields() as $formField) {
                foreach ($formField->getMeasures() as $measure) {
                    if (null !== $measure->getState()) {
                        $measure->setState($stateCodes[$measure->getState()->getCode()] ?? null);
                    }
                }
            }
        }
    }

    /**
     * @param UnitParcel[]          $unitParcels
     * @param array<int, StateCode> $stateCodes
     */
    private function updateUnitParcelsStateCodes(array $unitParcels, array $stateCodes): void
    {
        foreach ($unitParcels as $unitParcel) {
            if (null !== $unitParcel->getStateCode()) {
                $unitParcel->setStateCode($stateCodes[$unitParcel->getStateCode()->getCode()] ?? null);
            }
            foreach ($unitParcel->getIndividuals() as $individual) {
                if (null !== $individual->getStateCode()) {
                    $individual->setStateCode($stateCodes[$individual->getStateCode()->getCode()] ?? null);
                }
            }
        }
    }
}
