<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Test\Base\Test;
use Mobile\Measure\Entity\Test\RangeTest;
use Webapp\FileManagement\Dto\Common\MinMaxDoubleDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends TestNormalizer<RangeTest, RangeTestXmlDto>
 *
 * @psalm-type RangeTestXmlDto = array{
 *      "@active": ?bool,
 *      "@nom": ?string,
 *      limiteObligatoire: ?MinMaxDoubleDto,
 *      limiteOptionnel: ?MinMaxDoubleDto,
 * }
 */
class RangeTestNormalizer extends TestNormalizer
{
    protected const TAG_LIMITE_OPTIONNEL = 'limiteOptionnel';
    protected const TAG_LIMITE_OBLIGATOIRE = 'limiteObligatoire';

    protected function getClass(): string
    {
        return RangeTest::class;
    }

    protected function extractData($object, array $context): array
    {
        return array_merge(parent::extractCommonData($object, $context), [
            self::ATTR_XSI_TYPE => 'adonis.modeleMetier.projetDeSaisie.variables:TestSurIntervalles',
            self::ATTR_NOM => 'Test sur intervalle',
            self::TAG_LIMITE_OPTIONNEL => new MinMaxDoubleDto($object->getOptionalMinValue(), $object->getOptionalMaxValue()),
            self::TAG_LIMITE_OBLIGATOIRE => new MinMaxDoubleDto($object->getMandatoryMinValue(), $object->getMandatoryMaxValue()),
        ]);
    }

    protected function getImportDataConfig(array $context): array
    {
        return array_merge(parent::getCommonImportDataConfig($context), [
            self::TAG_LIMITE_OBLIGATOIRE => XmlImportConfig::value(MinMaxDoubleDto::class),
            self::TAG_LIMITE_OPTIONNEL => XmlImportConfig::value(MinMaxDoubleDto::class),
        ]);
    }

    protected function generateImportedObject($data, array $context): RangeTest
    {
        return new RangeTest();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Test
    {
        $object = parent::completeImportedObject($object, $data, $format, $context);
        $object
            ->setOptionalMinValue($data[self::TAG_LIMITE_OPTIONNEL]->min ?? 0)
            ->setOptionalMaxValue($data[self::TAG_LIMITE_OPTIONNEL]->max ?? 0)
            ->setMandatoryMinValue($data[self::TAG_LIMITE_OBLIGATOIRE]->min ?? 0)
            ->setMandatoryMaxValue($data[self::TAG_LIMITE_OBLIGATOIRE]->max ?? 0);

        return $object;
    }
}
