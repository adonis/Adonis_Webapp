<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\Mark;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractRootNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Mark, MarkXmlDto>
 *
 * @psalm-type MarkXmlDto = array{
 *       "@image": ?string,
 *       "@texte": ?string,
 *       "@valeur": ?int
 *  }
 */
class MarkNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_TEXTE = '@texte';
    protected const ATTR_VALEUR = '@valeur';
    protected const ATTR_IMAGE = '@image';

    protected function getClass(): string
    {
        return Mark::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_TEXTE => $object->getText(),
            self::ATTR_VALEUR => $object->getValue(),
            self::ATTR_IMAGE => null !== $object->getImage() ? self::getWriterHelper($context)->addMarkFile($object->getImage()) : null,
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_IMAGE => 'string',
            self::ATTR_VALEUR => 'int',
            self::ATTR_TEXTE => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if ('' !== $data[self::ATTR_IMAGE] && !file_exists($this->getPicturePath($data[self::ATTR_IMAGE], $context))) {
            throw new ParsingException('', RequestFile::ERROR_FILE_NOT_FOUND);
        }
    }

    protected function generateImportedObject($data, array $context): Mark
    {
        return new Mark();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Mark
    {
        $object->setText($data[self::ATTR_TEXTE]);
        $object->setValue($data[self::ATTR_VALEUR] ?? 0);
        if (($data[self::ATTR_IMAGE] ?? '') !== '') {
            $object->setImageName($this->getPicturePath($data[self::ATTR_IMAGE], $context));
        }

        return $object;
    }

    private function getPicturePath(string $pictureFile, array $context): string
    {
        $path = $context[AbstractRootNormalizer::FILES_PATH] ?? null;
        if (null === $path) {
            throw new \LogicException('Project path is not defined in context.');
        }

        return $path.'echelles/'.$pictureFile;
    }
}
