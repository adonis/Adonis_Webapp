<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Project\Entity\DesktopUser;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<DesktopUser, DesktopUserXmlDto>
 *
 * @psalm-type DesktopUserXmlDto = array{
 *      "@email": ?string,
 *      "@login": ?string,
 *      "@motDePasse": ?string,
 *      "@nom": ?string,
 *      "@prenom": ?string
 * }
 */
class DesktopUserNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_LOGIN = '@login';
    protected const ATTR_NOM = '@nom';
    protected const ATTR_PRENOM = '@prenom';
    protected const ATTR_EMAIL = '@email';
    protected const ATTR_MOT_DE_PASSE = '@motDePasse';

    protected function getClass(): string
    {
        return DesktopUser::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_PRENOM => $object->getFirstname(),
            self::ATTR_EMAIL => $object->getEmail(),
            self::ATTR_LOGIN => $object->getLogin(),
            self::ATTR_MOT_DE_PASSE => '1f83246b9b6a1409d273de4901c98514',
        ];
    }

    protected function getMobileReaderStep(): int
    {
        return RequestFile::PARSING_STATE_READ_DESKTOP_USER_COLLECTION;
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_LOGIN => 'string',
            self::ATTR_NOM => 'string',
            self::ATTR_PRENOM => 'string',
            self::ATTR_EMAIL => 'string',
            self::ATTR_MOT_DE_PASSE => 'string',
        ];
    }

    protected function generateImportedObject($data, array $context): DesktopUser
    {
        return new DesktopUser();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): DesktopUser
    {
        $object
            ->setLogin($data[self::ATTR_LOGIN] ?? '')
            ->setName($data[self::ATTR_NOM] ?? '')
            ->setFirstname($data[self::ATTR_PRENOM] ?? '')
            ->setEmail($data[self::ATTR_EMAIL] ?? '')
            ->setPassword($data[self::ATTR_MOT_DE_PASSE] ?? '');

        return $object;
    }
}
