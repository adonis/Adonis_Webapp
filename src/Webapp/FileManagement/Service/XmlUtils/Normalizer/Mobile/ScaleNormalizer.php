<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Variable\Mark;
use Mobile\Measure\Entity\Variable\Scale;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Scale, ScaleXmlDto>
 *
 * @psalm-type ScaleXmlDto = array{
 *      "@nom": string,
 *      "@valeurMax": ?int,
 *      "@valeurMin": ?int,
 *      notations: Collection<int, Mark>
 * }
 */
class ScaleNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_VALEUR_MIN = '@valeurMin';
    protected const ATTR_VALEUR_MAX = '@valeurMax';
    protected const TAG_NOTATIONS = 'notations';
    protected const ATTR_EXCLUSIF = '@exclusif';

    protected function getClass(): string
    {
        return Scale::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_VALEUR_MIN => $object->getMinValue(),
            self::ATTR_VALEUR_MAX => $object->getMaxValue(),
            self::ATTR_EXCLUSIF => $object->isExclusive(),
            self::TAG_NOTATIONS => $object->getMarks(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_VALEUR_MIN => 'int',
            self::ATTR_VALEUR_MAX => 'int',
            self::TAG_NOTATIONS => XmlImportConfig::collection(Mark::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM])) {
            throw new ParsingException('', RequestFile::ERROR_SCALE_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Scale
    {
        return new Scale();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Scale
    {
        $object
            ->setName($data[self::ATTR_NOM])
            ->setMinValue($data[self::ATTR_VALEUR_MIN] ?? 0)
            ->setMaxValue($data[self::ATTR_VALEUR_MAX] ?? 0)
            ->setMarks($data[self::TAG_NOTATIONS]);

        return $object;
    }
}
