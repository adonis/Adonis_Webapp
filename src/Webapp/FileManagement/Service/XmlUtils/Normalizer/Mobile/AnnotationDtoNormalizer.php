<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Measure;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

class AnnotationDtoNormalizer implements ContextAwareNormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof AnnotationDto;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        if (!$object instanceof AnnotationDto) {
            throw new InvalidArgumentException('$object must be an instance of AnnotationsDto');
        }

        \assert($this->serializer instanceof Serializer);

        $targetAttribute = $object->businessObject instanceof Measure ? '@mesureVariable' : '@objetMetier';

        $data = $this->serializer->normalize($object->annotation, $format, $context);
        \assert(\is_array($data));
        $data[$targetAttribute] = AbstractXmlNormalizer::getWriterHelper($context)->get($object->businessObject->getUri());

        return $data;
    }
}
