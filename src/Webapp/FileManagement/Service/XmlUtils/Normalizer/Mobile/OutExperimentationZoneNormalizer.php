<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Device\Entity\OutExperimentationZone;
use Mobile\Project\Entity\NatureZHE;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<OutExperimentationZone, OutExperimentationZoneXmlDto>
 *
 * @psalm-type OutExperimentationZoneXmlDto = array{
 *      "@natureZhe": ?NatureZHE,
 *      "@numero": ?string,
 *      "@x": ?int,
 *      "@y": ?int
 * }
 */
class OutExperimentationZoneNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NUMERO = '@numero';
    protected const ATTR_NATURE_ZHE = '@natureZhe';
    protected const ATTR_X = '@x';
    protected const ATTR_Y = '@y';

    protected function getClass(): string
    {
        return OutExperimentationZone::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_X => $object->getX(),
            self::ATTR_Y => $object->getY(),
            self::ATTR_NUMERO => $object->getNumber(),
            self::ATTR_NATURE_ZHE => new ReferenceDto($object->getNatureZHE()),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_X => 'int',
            self::ATTR_Y => 'int',
            self::ATTR_NUMERO => 'string',
            self::ATTR_NATURE_ZHE => XmlImportConfig::value(NatureZHE::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (
            !isset($data[self::ATTR_NUMERO])
            || !isset($data[self::ATTR_NATURE_ZHE])
        ) {
            throw new ParsingException('', RequestFile::ERROR_OUT_EXPERIMENTATION_ZONE_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): OutExperimentationZone
    {
        return new OutExperimentationZone();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): OutExperimentationZone
    {
        $object
            ->setX($data[self::ATTR_X] ?? 0)
            ->setY($data[self::ATTR_Y] ?? 0)
            ->setNumber($data[self::ATTR_NUMERO])
            ->setNatureZHE($data[self::ATTR_NATURE_ZHE]);

        return $object;
    }
}
