<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Device\Entity\EntryNote;
use Shared\Authentication\Entity\User;
use Shared\Authentication\Repository\UserRepository;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<EntryNote, EntryNoteXmlDto>
 *
 * @psalm-type EntryNoteXmlDto = array{
 *      "@dateCreation": ?\DateTime,
 *      "@loginCreateur": ?string,
 *      "@supprime": ?bool,
 *      "@texte": ?string
 * }
 */
class EntryNoteNormalizer extends AbstractXmlNormalizer
{
    private const ATTR_DATE_CREATION = '@dateCreation';
    private const ATTR_SUPPRIME = '@supprime';
    private const ATTR_TEXTE = '@texte';
    private const ATTR_LOGIN_CREATEUR = '@loginCreateur';
    private const ATTR_ONLINE_USER_URI = '@onlineUserUri';

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    protected function getClass(): string
    {
        return EntryNote::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_DATE_CREATION => $object->getCreationDate(),
            self::ATTR_TEXTE => $object->getText(),
            self::ATTR_ONLINE_USER_URI => $object->getCreator()->getUri(),
            self::ATTR_LOGIN_CREATEUR => $object->getCreator()->getUsername(),
            self::ATTR_SUPPRIME => $object->isDeleted(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_SUPPRIME => 'bool',
            self::ATTR_TEXTE => 'string',
            self::ATTR_LOGIN_CREATEUR => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (null === $this->loadUser($data[self::ATTR_LOGIN_CREATEUR])) {
            throw new ParsingException('', RequestFile::ERROR_NOTE_CREATOR_NOT_FOUND);
        }
    }

    protected function generateImportedObject($data, array $context): EntryNote
    {
        return new EntryNote();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): EntryNote
    {
        $creator = $this->loadUser($data[self::ATTR_LOGIN_CREATEUR]);
        \assert(null !== $creator);

        $object
            ->setCreationDate($data[self::ATTR_DATE_CREATION])
            ->setDeleted($data[self::ATTR_SUPPRIME] ?? false)
            ->setText($data[self::ATTR_TEXTE] ?? '')
            ->setCreator($creator);

        return $object;
    }

    private function loadUser(string $username): ?User
    {
        return $this->userRepository->findOneBy(['username' => $username]);
    }
}
