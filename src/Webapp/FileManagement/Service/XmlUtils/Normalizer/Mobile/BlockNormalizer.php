<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\EntryNote;
use Mobile\Device\Entity\OutExperimentationZone;
use Mobile\Device\Entity\SubBlock;
use Mobile\Device\Entity\UnitParcel;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Block, BlockXmlDto>
 *
 * @psalm-type BlockXmlDto = array{
 *      "@numero": ?string,
 *      notes: Collection<int, EntryNote>,
 *      parcellesUnitaire: Collection<int, UnitParcel>,
 *      sousBlocs: Collection<int, SubBlock>,
 *      zheAvecEpaisseurs: Collection<int, OutExperimentationZone>
 * }
 */
class BlockNormalizer extends AbstractXmlNormalizer
{
    protected const TAG_PARCELLES_UNITAIRE = 'parcellesUnitaire';
    protected const TAG_SOUS_BLOCS = 'sousBlocs';
    protected const ATTR_NUMERO = '@numero';
    protected const TAG_ZHE_AVEC_EPAISSEURS = 'zheAvecEpaisseurs';
    protected const TAG_NOTES = 'notes';
    protected const TAG_METADONNEES = 'metadonnees';

    protected function getClass(): string
    {
        return Block::class;
    }

    protected function extractData($object, array $context): array
    {
        $parcellesUnitaires = [];
        $outExperimentationZones = $object->getOutExperimentationZones();
        foreach ($object->getUnitParcels() as $unitParcel) {
            if (null === $unitParcel->getTreatment()) {
                // Case when the unit parcel comes from a treatment error
                foreach ($unitParcel->getOutExperimentationZones() as $outExperimentationZone) {
                    $outExperimentationZones[] = $outExperimentationZone;
                }
            } else {
                $parcellesUnitaires[] = $unitParcel;
            }
        }

        self::getWriterHelper($context)->addSessionAnnotations(AnnotationDto::fromCollection($object->getAnnotations(), $object));

        return [
            self::ATTR_NUMERO => $object->getName(),
            self::TAG_SOUS_BLOCS => $object->getSubBlocks(),
            self::TAG_PARCELLES_UNITAIRE => $parcellesUnitaires,
            self::TAG_ZHE_AVEC_EPAISSEURS => $outExperimentationZones,
            self::TAG_NOTES => $object->getNotes(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NUMERO => 'string',
            self::TAG_ZHE_AVEC_EPAISSEURS => XmlImportConfig::collection(OutExperimentationZone::class),
            self::TAG_PARCELLES_UNITAIRE => XmlImportConfig::collection(UnitParcel::class),
            self::TAG_SOUS_BLOCS => XmlImportConfig::collection(SubBlock::class),
            self::TAG_NOTES => XmlImportConfig::collection(EntryNote::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (
            !isset($data[self::TAG_PARCELLES_UNITAIRE])
            && !isset($data[self::TAG_SOUS_BLOCS])
        ) {
            throw new ParsingException('', RequestFile::ERROR_NO_BLOC_CHILDREN);
        }
    }

    protected function generateImportedObject($data, array $context): Block
    {
        return new Block();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Block
    {
        $object
            ->setOutExperimentationZones($data[self::TAG_ZHE_AVEC_EPAISSEURS])
            ->setNotes($data[self::TAG_NOTES])
            ->setName($data[self::ATTR_NUMERO] ?? '0');

        /** @var Collection<int, UnitParcel> $unitParcels */
        $unitParcels = $data[self::TAG_PARCELLES_UNITAIRE]->filter(fn (UnitParcel $unitParcel) => null !== $unitParcel->getTreatment());
        if ($unitParcels->count() > 0) {
            $object->setUnitParcels($data[self::TAG_PARCELLES_UNITAIRE]);
        } elseif (!$data[self::TAG_SOUS_BLOCS]->isEmpty()) {
            $object->setSubBlocks($data[self::TAG_SOUS_BLOCS]);
        }

        return $object;
    }
}
