<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\Factor;
use Mobile\Device\Entity\Modality;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Factor, FactorXmlDto>
 *
 * @psalm-type FactorXmlDto = array{
 *      "@nom": ?string,
 *      modalites: Collection<int, Modality>
 * }
 */
class FactorNormalizer extends AbstractXmlNormalizer
{
    protected const TAG_MODALITES = 'modalites';
    protected const ATTR_NOM = '@nom';

    protected function getClass(): string
    {
        return Factor::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::TAG_MODALITES => $object->getModalities(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::TAG_MODALITES => XmlImportConfig::collection(Modality::class),
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_NOM])) {
            throw new ParsingException('', RequestFile::ERROR_PROTOCOL_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Factor
    {
        return new Factor();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Factor
    {
        $object
            ->setName($data[self::ATTR_NOM])
            ->setModalities($data[self::TAG_MODALITES]);

        return $object;
    }
}
