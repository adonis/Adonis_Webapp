<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Annotation;
use Mobile\Project\Entity\ProjectObject;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Annotation, array{
 *     "@nom": ?string,
 *     "@date": \DateTime,
 *     "@nature": ?string,
 *     "@donnee": ?string,
 *     "@objetMetier": ?ProjectObject,
 *     "@mesureVariable": ?ProjectObject,
 *     "categorie": string[],
 *     "motscles": string[],
 * }>
 */
class AnnotationNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE = '@date';
    protected const TAG_CATEGORIE = 'categorie';
    protected const TAG_MOTSCLES = 'motscles';
    protected const ATTR_NATURE = '@nature';
    protected const ATTR_DONNEE = '@donnee';
    protected const ATTR_OBJET_METIER = '@objetMetier';
    protected const ATTR_MESURE_VARIABLE = '@mesureVariable';

    protected function getClass(): string
    {
        return Annotation::class;
    }

    protected function extractData($object, array $context): array
    {
        $forType = [];
        switch ($object->getType()) {
            case Annotation::ANNOT_TYPE_TEXT:
                $forType = [
                    self::ATTR_NATURE => 'texte',
                    self::ATTR_DONNEE => $object->getValue(),
                ];
                break;
            case Annotation::ANNOT_TYPE_IMAG:
                $forType = [
                    self::ATTR_NATURE => 'photo',
                    self::ATTR_DONNEE => null !== $object->getImage() ? self::getWriterHelper($context)->addAnnotationFile($object->getImage()) : null,
                ];
                break;
        }

        return array_merge([
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE => $object->getTimestamp(),
            self::TAG_CATEGORIE => array_map(fn (string $categorie) => ['#' => $categorie], $object->getCategories()),
            self::TAG_MOTSCLES => array_map(fn (string $motCle) => ['#' => $motCle], $object->getKeywords()),
        ], $forType);
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_DATE => \DateTime::class,
            self::ATTR_NATURE => 'string',
            self::ATTR_DONNEE => 'string',
            self::ATTR_OBJET_METIER => XmlImportConfig::reference(ProjectObject::class),
            self::TAG_CATEGORIE => XmlImportConfig::values('string'),
            self::TAG_MOTSCLES => XmlImportConfig::values('string'),
        ];
    }

    protected function generateImportedObject($data, array $context): Annotation
    {
        return new Annotation();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Annotation
    {
        if ('photo' === $data[self::ATTR_NATURE]) {
            $type = Annotation::ANNOT_TYPE_IMAG;
        } else {
            $type = Annotation::ANNOT_TYPE_TEXT;
        }

        $object
            ->setName($data[self::ATTR_NOM])
            ->setTimestamp($data[self::ATTR_DATE])
            ->setType($type)
            ->setValue($data[self::ATTR_DONNEE])
            ->setCategories($data[self::TAG_CATEGORIE])
            ->setKeywords($data[self::TAG_MOTSCLES]);

        /** @var ProjectObject|null $target */
        $target = $data[self::ATTR_OBJET_METIER] ?? $data[self::ATTR_MESURE_VARIABLE];
        if (null !== $target) {
            $target->addAnnotation($object);
        }

        return $object;
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_OBJET_METIER]) && !isset($data[self::ATTR_MESURE_VARIABLE])) {
            throw new ParsingException('Attribute "objetMetier" or "mesureVariable" is missing in "metadonnees"', RequestFile::ERROR_TEST_INFO_MISSING);
        }
        if (!isset($data[self::ATTR_DATE])) {
            throw new ParsingException('Attribute "date" is missing in "metadonnees"', RequestFile::ERROR_TEST_INFO_MISSING);
        }
    }
}
