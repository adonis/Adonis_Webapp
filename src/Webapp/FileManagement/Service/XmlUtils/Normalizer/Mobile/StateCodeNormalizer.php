<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\StateCode;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<StateCode, StateCodeXmlDto>
 *
 * @psalm-type StateCodeXmlDto = array{
 *      "@code": ?int,
 *      "@couleur": ?string,
 *      "@intitule": ?string,
 *      "@propagation": ?string,
 *      "@signification": ?string
 * }
 */
class StateCodeNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_CODE = '@code';

    private const REVERSE_TYPE_MAP = [
        'Mort' => StateCode::DEAD_STATE_CODE,
        'Donnée Manquante' => StateCode::MISSING_STATE_CODE,
    ];

    private const PROPAGATION_INDIVIDU = 'individu';
    private const PROPAGATION_PARCELLE = 'parcelle';
    private const PROPAGATION_MAP = [
        PathLevelEnum::INDIVIDUAL => self::PROPAGATION_INDIVIDU,
        PathLevelEnum::UNIT_PLOT => self::PROPAGATION_PARCELLE,
    ];

    private const REVERSE_PROPAGATION_MAP = [
        self::PROPAGATION_INDIVIDU => PathLevelEnum::INDIVIDUAL,
        self::PROPAGATION_PARCELLE => PathLevelEnum::UNIT_PLOT,
    ];

    protected const ATTR_SIGNIFICATION = '@signification';
    protected const ATTR_PROPAGATION = '@propagation';
    protected const ATTR_COULEUR = '@couleur';
    protected const ATTR_INTITULE = '@intitule';

    protected function getClass(): string
    {
        return StateCode::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_CODE => $object->getCode(),
            self::ATTR_INTITULE => $object->getLabel(),
            self::ATTR_SIGNIFICATION => $object->getDescription(),
            self::ATTR_PROPAGATION => self::PROPAGATION_MAP[$object->getPropagation() ?? ''] ?? $object->getPropagation(),
            self::ATTR_COULEUR => $object->getColor(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_INTITULE => 'string',
            self::ATTR_CODE => 'int',
            self::ATTR_SIGNIFICATION => 'string',
            self::ATTR_PROPAGATION => 'string',
            self::ATTR_COULEUR => 'string',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (!isset($data[self::ATTR_CODE]) || !isset($data[self::ATTR_INTITULE])) {
            throw new ParsingException('', RequestFile::ERROR_STATE_CODE_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): StateCode
    {
        return new StateCode();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): StateCode
    {
        $object
            ->setCode($data[self::ATTR_CODE] ?? 0)
            ->setLabel($data[self::ATTR_INTITULE] ?? '')
            ->setType(self::REVERSE_TYPE_MAP[$object->getLabel()] ?? StateCode::NON_PERMANENT_STATE_CODE)
            ->setDescription($data[self::ATTR_SIGNIFICATION])
            ->setPropagation(self::REVERSE_PROPAGATION_MAP[$data[self::ATTR_PROPAGATION] ?? ''] ?? null)
            ->setColor($data[self::ATTR_COULEUR]);

        return $object;
    }
}
