<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Device\Entity\Factor;
use Mobile\Device\Entity\Protocol;
use Mobile\Device\Entity\Treatment;
use Mobile\Project\Entity\DesktopUser;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends AbstractXmlNormalizer<Protocol, ProtocolXmlDto>
 *
 * @psalm-type ProtocolXmlDto = array{
 *      "@algorithmeTirage": ?string,
 *      "@createur": ?DesktopUser,
 *      "@dateCreation": ?\DateTime,
 *      "@nom": ?string,
 *      "@objectifs": ?string,
 *      facteurs: Collection<int, Factor>,
 *      structureInitiale: array,
 *      traitements: Collection<int, Treatment>
 * }
 */
class ProtocolNormalizer extends AbstractXmlNormalizer
{
    protected const ATTR_NOM = '@nom';
    protected const ATTR_DATE_CREATION = '@dateCreation';
    protected const ATTR_CREATEUR = '@createur';
    protected const ATTR_ALGORITHME_TIRAGE = '@algorithmeTirage';
    protected const ATTR_OBJECTIFS = '@objectifs';
    protected const TAG_FACTEURS = 'facteurs';
    protected const TAG_TRAITEMENTS = 'traitements';
    protected const TAG_STRUCTURE_INITIALE = 'structureInitiale';

    protected function getClass(): string
    {
        return Protocol::class;
    }

    protected function extractData($object, array $context): array
    {
        return [
            self::ATTR_NOM => $object->getName(),
            self::ATTR_DATE_CREATION => $object->getCreationDate(),
            self::ATTR_OBJECTIFS => $object->getAim(),
            self::ATTR_CREATEUR => null !== $object->getCreator() ? new ReferenceDto($object->getCreator()) : null,
            self::ATTR_ALGORITHME_TIRAGE => $object->getAlgorithm(),
            self::TAG_FACTEURS => $object->getFactors(),
            self::TAG_TRAITEMENTS => $object->getTreatments(),
        ];
    }

    protected function getImportDataConfig(array $context): array
    {
        return [
            self::ATTR_NOM => 'string',
            self::ATTR_DATE_CREATION => \DateTime::class,
            self::ATTR_OBJECTIFS => 'string',
            self::ATTR_ALGORITHME_TIRAGE => 'string',
            self::ATTR_CREATEUR => XmlImportConfig::reference(DesktopUser::class),
            self::TAG_FACTEURS => XmlImportConfig::collection(Factor::class),
            self::TAG_TRAITEMENTS => XmlImportConfig::collection(Treatment::class),
            self::TAG_STRUCTURE_INITIALE => 'array',
        ];
    }

    protected function validateImportData($data, array $context): void
    {
        if (
            !isset($data[self::ATTR_NOM])
            || !isset($data[self::ATTR_DATE_CREATION])
            || !isset($data[self::ATTR_CREATEUR])
            || !isset($data[self::ATTR_ALGORITHME_TIRAGE])
        ) {
            throw new ParsingException('', RequestFile::ERROR_PROTOCOL_INFO_MISSING);
        }
    }

    protected function generateImportedObject($data, array $context): Protocol
    {
        return new Protocol();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Protocol
    {
        $object
            ->setName($data[self::ATTR_NOM])
            ->setCreationDate($data[self::ATTR_DATE_CREATION])
            ->setAim($data[self::ATTR_OBJECTIFS] ?? '')
            ->setAlgorithm($data[self::ATTR_ALGORITHME_TIRAGE] ?? '')
            ->setCreator($data[self::ATTR_CREATEUR])
            ->setFactors($data[self::TAG_FACTEURS])
            ->setTreatments($data[self::TAG_TRAITEMENTS]);

        // When protocole has a child "structureInitiale", it meens that the protocol defines information such as
        // the number of individual to insert in a parcel when working on spacialization project.
        if (\count($data[self::TAG_STRUCTURE_INITIALE]) > 0) {
            $nbIndividuals = null;
            if (isset($data[self::TAG_STRUCTURE_INITIALE][0]['parcellesUnitaire'])) {
                $nbIndividuals = (int) $data[self::TAG_STRUCTURE_INITIALE][0]['parcellesUnitaire'][0]['nbIndividus'];
            }

            $object->setNbIndividualsPerParcel($nbIndividuals);
        }

        return $object;
    }
}
