<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Test\CombinationTest;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Webapp\Core\Enumeration\CombinationOperationEnum;
use Webapp\FileManagement\Dto\Common\MinMaxDoubleDto;
use Webapp\FileManagement\Dto\Common\ReferenceDto;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\XmlImportConfig;

/**
 * @template-extends TestNormalizer<CombinationTest, CombinationTestXmlDto>
 *
 * @psalm-type CombinationTestXmlDto = array{
 *      "@active": ?bool,
 *      "@nom": ?string,
 *      "@operande1": ?Variable,
 *      "@operande2": ?Variable,
 *      "@operateur": ?string,
 *      limite: ?array,
 * }
 */
class CombinationTestNormalizer extends TestNormalizer
{
    protected const ATTR_OPERANDE_1 = '@operande1';
    protected const ATTR_OPERANDE_2 = '@operande2';
    protected const ATTR_OPERATEUR = '@operateur';

    protected const TAG_LIMITE = 'limite';

    protected const OPERATEUR_SOUSTRACTION = 'soustraction';
    protected const OPERATEUR_DIVISION = 'division';
    protected const OPERATEUR_MULTIPLICATION = 'multiplication';
    protected const OPERATEUR_ADDITION = 'addition';

    protected const OPERATEUR_MAP = [
        CombinationOperationEnum::SUBSTRACTION => self::OPERATEUR_SOUSTRACTION,
        CombinationOperationEnum::DIVISION => self::OPERATEUR_DIVISION,
        CombinationOperationEnum::MULTIPLICATION => self::OPERATEUR_MULTIPLICATION,
    ];

    protected const REVERSE_OPERATEUR_MAP = [
        self::OPERATEUR_SOUSTRACTION => CombinationOperationEnum::SUBSTRACTION,
        self::OPERATEUR_DIVISION => CombinationOperationEnum::DIVISION,
        self::OPERATEUR_MULTIPLICATION => CombinationOperationEnum::MULTIPLICATION,
        self::OPERATEUR_ADDITION => CombinationOperationEnum::ADIDITION,
    ];

    protected function getClass(): string
    {
        return CombinationTest::class;
    }

    protected function extractData($object, array $context): array
    {
        return array_merge(parent::extractCommonData($object, $context), [
            self::ATTR_XSI_TYPE => 'adonis.modeleMetier.projetDeSaisie.variables:TestCombinaisonEntreVariables',
            self::ATTR_NOM => 'Test sur combinaison entre variables',
            self::ATTR_OPERANDE_1 => new ReferenceDto($object->getFirstOperand()),
            self::ATTR_OPERANDE_2 => new ReferenceDto($object->getSecondOperand()),
            self::ATTR_OPERATEUR => self::OPERATEUR_MAP[$object->getOperator()],
            self::TAG_LIMITE => new MinMaxDoubleDto($object->getLowLimit(), $object->getHighLimit()),
        ]);
    }

    protected function getImportDataConfig(array $context): array
    {
        return array_merge(parent::getCommonImportDataConfig($context), [
            self::ATTR_OPERATEUR => 'string',
            self::ATTR_OPERANDE_1 => XmlImportConfig::value(Variable::class),
            self::ATTR_OPERANDE_2 => XmlImportConfig::value(Variable::class),
            self::TAG_LIMITE => 'array',
        ]);
    }

    protected function validateImportData($data, array $context): void
    {
        parent::validateImportData($data, $context);

        $readerHelper = self::getReaderHelper($context);
        if (!$readerHelper->exists($data[self::ATTR_OPERANDE_1])) {
            throw new ParsingException('', RequestFile::ERROR_UNKNOWN_TEST_LINKED_VARIABLE);
        }
        if (!$readerHelper->exists($data[self::ATTR_OPERANDE_2])) {
            throw new ParsingException('', RequestFile::ERROR_UNKNOWN_TEST_LINKED_VARIABLE);
        }
    }

    protected function generateImportedObject($data, array $context): CombinationTest
    {
        return new CombinationTest();
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): CombinationTest
    {
        $object = parent::completeImportedObject($object, $data, $format, $context);
        $object
            ->setOperator(self::REVERSE_OPERATEUR_MAP[$data[self::ATTR_OPERATEUR] ?? ''] ?? CombinationOperationEnum::ADIDITION)
            ->setFirstOperand($data[self::ATTR_OPERANDE_1])
            ->setSecondOperand($data[self::ATTR_OPERANDE_2])
            ->setHighLimit($data[self::TAG_LIMITE]->max ?? 0)
            ->setLowLimit($data[self::TAG_LIMITE]->min ?? 0);

        return $object;
    }
}
