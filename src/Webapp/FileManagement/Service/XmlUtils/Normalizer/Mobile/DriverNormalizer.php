<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer\Mobile;

use Mobile\Measure\Entity\Variable\Driver;
use Webapp\FileManagement\Service\XmlUtils\Normalizer\AbstractXmlNormalizer;

/**
 * @template-extends AbstractXmlNormalizer<Driver, array{}>
 */
class DriverNormalizer extends AbstractXmlNormalizer
{
    protected function getClass(): string
    {
        return Driver::class;
    }

    protected function extractData($object, array $context): array
    {
        if (Driver::RS232 !== $object->getType()) {
            $driverConfig = [
                '@type' => $object->getType(),
            ];
        } else {
            $driverConfig = [
                self::ATTR_XSI_TYPE => 'adonis.modeleMetier.projetDeSaisie.variables:DriverCom',
                '@baudrate' => $object->getBaudrate(),
                '@stopbit' => $object->getStopbit(),
                '@push' => $object->getPush() ? 'true' : 'false',
                '@parity' => $object->getParity(),
                '@request' => $object->getRequest(),
                '@flowcontrol' => $object->getFlowControl(),
                '@port' => $object->getPort(),
                '@databitsFormat' => $object->getDatabitsFormat(),
            ];
        }

        return array_merge([
            '@debutTrame' => $object->getFrameStart(),
            '@finTrame' => $object->getFrameEnd(),
            '@separateurCsv' => $object->getCsvSeparator(),
            '@timeout' => $object->getTimeout(),
            '@tailleTrame' => $object->getFrameLength(),
        ], $driverConfig);
    }

    protected function getImportDataConfig(array $context): array
    {
        throw new \LogicException('Method not implemented');
    }

    protected function generateImportedObject($data, array $context): Driver
    {
        throw new \LogicException('Method not implemented');
    }

    protected function completeImportedObject($object, $data, ?string $format, array $context): Driver
    {
        throw new \LogicException('Method not implemented');
    }
}
