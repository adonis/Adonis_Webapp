<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer;

/**
 * @psalm-type DataType = class-string|'int'|'float'|'bool'|'string'|'array'
 *
 * @psalm-immutable
 */
class XmlImportConfig
{
    public const NONE = 0;
    public const ARRAY = 1;
    public const COLLECTION = 2;
    public const REFERENCE = 4;

    /** @var DataType */
    private string $type;
    private bool $root;
    private int $flags;

    /**
     * @param DataType $type
     */
    public static function value(string $type, bool $root = false): self
    {
        return new self($type, $root);
    }

    /**
     * @param DataType $type
     */
    public static function values(string $type, bool $root = false): self
    {
        return new self($type, $root, self::ARRAY);
    }

    /**
     * @param DataType $type
     */
    public static function collection(string $type, bool $root = false): self
    {
        return new self($type, $root, self::COLLECTION);
    }

    /**
     * @param DataType $type
     */
    public static function reference(string $type, bool $root = false): self
    {
        return new self($type, $root, self::REFERENCE);
    }

    /**
     * @param DataType $type
     */
    public static function references(string $type, bool $root = false): self
    {
        return new self($type, $root, self::COLLECTION | self::REFERENCE);
    }

    /**
     * @param DataType $type
     */
    public function __construct(string $type, bool $root = false, int $flags = self::NONE)
    {
        $this->type = $type;
        $this->root = $root;
        $this->flags = $flags;
    }

    /**
     * @return DataType
     */
    public function getType(): string
    {
        return $this->type;
    }

    public function isRoot(): bool
    {
        return $this->root;
    }

    public function getFlags(): int
    {
        return $this->flags;
    }

    public function isArray(): bool
    {
        return (bool) ($this->flags & (self::ARRAY | self::COLLECTION));
    }
}
