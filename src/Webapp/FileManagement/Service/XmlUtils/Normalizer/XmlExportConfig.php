<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer;

/**
 * @psalm-immutable
 */
class XmlExportConfig
{
    public const NONE = 0;
    public const EMPTY_ARRAY_AS_NULL = 1;
    public const EMPTY_STRING_AS_NULL = 2;
    public const REMOVE_NULL = 4;
    public const REFERENCE_INDEXED = 8;

    private const DEFAULT_FLAGS =
        self::EMPTY_ARRAY_AS_NULL
        | self::EMPTY_STRING_AS_NULL
        | self::REMOVE_NULL
        | self::REFERENCE_INDEXED;

    /** @var mixed */
    private $value;
    /** @var string|false|null */
    private $refType;
    private int $flags;

    /**
     * @param mixed             $value
     * @param string|false|null $refType
     */
    public function __construct($value, $refType = null, int $flags = self::DEFAULT_FLAGS)
    {
        $this->value = $value;
        $this->refType = $refType;
        $this->flags = $flags;
    }

    /** @return mixed */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string|false|null
     */
    public function getRefType()
    {
        return $this->refType;
    }

    public function getFlags(): int
    {
        return $this->flags;
    }
}
