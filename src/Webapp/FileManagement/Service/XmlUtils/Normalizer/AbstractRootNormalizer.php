<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer;

/**
 * @template T
 * @template TImportDto as array<array-key, mixed>
 *
 * @template-extends AbstractXmlNormalizer<T, TImportDto>
 */
abstract class AbstractRootNormalizer extends AbstractXmlNormalizer
{
    public const FILES_PATH = 'filesPath';

    private string $desktopVersion;

    public function __construct(string $desktopVersion)
    {
        $this->desktopVersion = $desktopVersion;
    }

    protected function extractData($object, array $context): array
    {
        return [
            '@xmlns:xmi' => 'http://www.omg.org/XMI',
            '@xmi:version' => '2.0',
            '@xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
            '@xmlns:adonis.modeleMetier.plateforme' => 'http:///adonis/modeleMetier/plateforme.ecore',
            '@xmlns:adonis.modeleMetier.projetDeSaisie' => 'http:///adonis/modeleMetier/projetDeSaisie.ecore',
            '@xmlns:adonis.modeleMetier.projetDeSaisie.cheminement' => 'http:///adonis/modeleMetier/projetDeSaisie/cheminement.ecore',
            '@xmlns:adonis.modeleMetier.projetDeSaisie.variables' => 'http:///adonis/modeleMetier/projetDeSaisie/variables.ecore',
            '@xmlns:adonis.modeleMetier.saisieTerrain' => 'http:///adonis/modeleMetier/saisieTerrain.ecore',
            '@xmlns:adonis.modeleMetier.site' => 'http:///adonis/modeleMetier/site.ecore',
            '@xmlns:adonis.modeleMetier.utilisateur' => 'http:///adonis/modeleMetier/utilisateur.ecore',
            '@xmlns:adonis.modeleMetier.conceptsDeBase' => 'http:///adonis/modeleMetier/conceptsDeBase.ecore',
            'adonis.modeleMetier.site:AdonisVersion' => [
                '@version' => $this->desktopVersion,
            ],
        ];
    }
}
