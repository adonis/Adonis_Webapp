<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils\Normalizer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;
use Symfony\Component\Serializer\Normalizer\ContextAwareDenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerAwareTrait;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\XmlUtils\CustomXmlEncoder;
use Webapp\FileManagement\Service\XmlUtils\OverrideWriterHelper;
use Webapp\FileManagement\Service\XmlUtils\ReaderHelper;
use Webapp\FileManagement\Service\XmlUtils\ReaderHelperInterface;
use Webapp\FileManagement\Service\XmlUtils\ReferenceContext;
use Webapp\FileManagement\Service\XmlUtils\WriterHelper;
use Webapp\FileManagement\Service\XmlUtils\WriterHelperInterface;

/**
 * @template T
 * @template TImportDto of array<array-key, mixed>
 *
 * @psalm-import-type DataType from XmlImportConfig
 */
abstract class AbstractXmlNormalizer implements ContextAwareNormalizerInterface, ContextAwareDenormalizerInterface, SerializerAwareInterface
{
    use SerializerAwareTrait;

    /**
     * @var Serializer
     *
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     * @psalm-suppress NonInvariantDocblockPropertyType
     */
    protected $serializer;

    public const EXPORT_XML = 'export_xml';
    public const IMPORT_XML = 'import_xml';

    public const WRITER_HELPER = 'writer_helper';
    public const READER_HELPER = 'reader_helper';

    public const REFERENCE = 'reference';

    protected const ATTR_XSI_TYPE = '@xsi:type';

    protected function getMobileReaderStep(): int
    {
        return -1;
    }

    public static function initDenormalizeContext(array &$context): void
    {
        $context[self::READER_HELPER] = new ReaderHelper();
        $context[self::REFERENCE] = new ReferenceContext('');
    }

    public static function initNormalizeContext(?User $user, array &$context, int $rootIndex = 0): void
    {
        if (!isset($context[self::WRITER_HELPER])) {
            $context[self::WRITER_HELPER] = new WriterHelper($user);
        }
        $context[self::REFERENCE] = new ReferenceContext('', $rootIndex);
    }

    public static function getWriterHelper(array $context): WriterHelperInterface
    {
        if (!isset($context[self::WRITER_HELPER]) || !$context[self::WRITER_HELPER] instanceof WriterHelperInterface) {
            throw new \LogicException('Writer helper is not initialized');
        }

        return $context[self::WRITER_HELPER];
    }

    public static function getReaderHelper(array $context): ReaderHelperInterface
    {
        if (!isset($context[self::READER_HELPER]) || !$context[self::READER_HELPER] instanceof ReaderHelperInterface) {
            throw new \LogicException('Reader helper is not initialized');
        }

        return $context[self::READER_HELPER];
    }

    public static function childrenWriterContext(array $context, IdentifiedEntity $object, XmlExportConfig $value, ?string $key): array
    {
        $type = false === $value->getRefType() ? null : ($value->getRefType() ?? $key);
        $indexed = (bool) ($value->getFlags() & XmlExportConfig::REFERENCE_INDEXED);

        return self::childrenContext($context, $type, $indexed, null, fn (string $reference, ?int $index) => self::getWriterHelper($context)->register(
            $object,
            $reference,
            $type,
            $index
        ));
    }

    /**
     * @param mixed $data
     */
    public static function childrenReaderContext(array $context, $data, XmlImportConfig $config, ?string $key, ?int $index = null): array
    {
        $type = $config->isRoot() ? null : $key;
        $indexed = $config->isArray() || $config->isRoot();
        $index = $config->isRoot() ? (\is_array($data) && \array_key_exists(CustomXmlEncoder::NODE_INDEX, $data) ? $data[CustomXmlEncoder::NODE_INDEX] : null) : $index;

        return self::childrenContext($context, $type, $indexed, $index, fn (string $reference, ?int $autoIndex) => self::getReaderHelper($context)->createReference(
            $reference,
            $type,
            $autoIndex
        ));
    }

    protected static function childrenContext(array $context, ?string $type, bool $indexed, ?int $index, callable $createReference): array
    {
        $parentReferenceContext = $context[self::REFERENCE];
        \assert($parentReferenceContext instanceof ReferenceContext);

        if ($indexed && null === $index) {
            $index = $parentReferenceContext->index($type);
        }

        $reference = $createReference(
            $parentReferenceContext->getReference(),
            $indexed ? $index : null
        );

        $childrenContext = array_merge([], $context);
        $childrenContext[self::REFERENCE] = new ReferenceContext($reference);

        return $childrenContext;
    }

    /**
     * @throws ExceptionInterface
     */
    public function normalizeDataArray(array $dataArray, ?string $format, array $context): array
    {
        $values = [];
        foreach ($dataArray as $key => $item) {
            $value = $item instanceof XmlExportConfig ? $item : new XmlExportConfig($item);
            $data = $value->getValue();
            if (($value->getFlags() & XmlExportConfig::EMPTY_ARRAY_AS_NULL) && $this->isEmptyArrayOrCollection($data)) {
                $data = null;
            }
            if (($value->getFlags() & XmlExportConfig::EMPTY_STRING_AS_NULL) && \is_string($data) && '' === trim($data)) {
                $data = null;
            }
            if (($value->getFlags() & XmlExportConfig::REMOVE_NULL) && null === $data) {
                continue;
            }

            $childrenContext = $context;
            if ($data instanceof IdentifiedEntity) {
                $childrenContext = self::childrenWriterContext($context, $data, $value, $key);
            } elseif ($data instanceof Collection) {
                $data = $data->toArray();
            }
            if (\is_array($data) && array_is_list($data) && \count($data) > 0 && $data[0] instanceof IdentifiedEntity) {
                $data = array_map(
                    /**
                     * @throws ExceptionInterface
                     */
                    function (IdentifiedEntity $datum) use ($format, $childrenContext, $context, $key, $value) {
                        $childrenContext = self::childrenWriterContext($context, $datum, $value, $key);

                        return $this->serializer->normalize($datum, $format, $childrenContext);
                    },
                    $data
                );
            }

            $values[$key] = $this->serializer->normalize($data, $format, $childrenContext);
        }

        return $values;
    }

    /**
     * @param mixed $value
     */
    private function isEmptyArrayOrCollection($value): bool
    {
        return
            null === $value
            || '' === $value
            || \is_array($value) && 0 === \count($value)
            || $value instanceof Collection && $value->isEmpty();
    }

    /**
     * @param mixed $data
     */
    public function supportsDenormalization($data, string $type, ?string $format = null, array $context = []): bool
    {
        return $type === $this->getClass() && ($context[self::IMPORT_XML] ?? false) === true;
    }

    /**
     * @param mixed $data
     */
    public function supportsNormalization($data, ?string $format = null, array $context = []): bool
    {
        return is_a($data, $this->getClass()) && ($context[self::EXPORT_XML] ?? false) === true;
    }

    public function normalize($object, ?string $format = null, array $context = []): array
    {
        if (!is_a($object, $this->getClass())) {
            throw new InvalidArgumentException('$object must be an instance of '.$this->getClass());
        }

        $datas = $this->extractData($object, $context);

        return $this->normalizeDataArray($datas, $format, $context);
    }

    /**
     * @param mixed $data
     *
     * @return T
     *
     * @throws ParsingException
     * @throws ExceptionInterface
     */
    public function denormalize($data, string $type, ?string $format = null, array $context = [])
    {
        $config = $this->getImportDataConfig($context);

        return $this->denormalizeDataArray($config, $data, $format, $context);
    }

    /**
     * @param array<key-of<TImportDto>, string|XmlImportConfig> $config
     * @param mixed                                             $data
     *
     * @return T
     *
     * @throws ParsingException
     * @throws ExceptionInterface
     */
    protected function denormalizeDataArray(array $config, $data, ?string $format, array $context)
    {
        $object = $this->generateImportedObject($data, $context);

        if ($object instanceof IdentifiedEntity) {
            $referenceContext = $context[self::REFERENCE];
            \assert($referenceContext instanceof ReferenceContext);
            self::getReaderHelper($context)->add($referenceContext->getReference(), $object);
        }

        if (\is_array($data)) {
            $data = array_filter($data, fn ($datum) => null !== $datum && '' !== $datum);
        } elseif (\is_string($data)) {
            $data = ['#' => $data];
        }

        $this->validateImportData($data, $context);

        $values = [];
        foreach ($config as $key => $item) {
            $values[$key] = $this->denormalizeDataItem($item, $data[$key] ?? null, $key, $format, $context);
        }

        return $this->completeImportedObject($object, $values, $format, $context);
    }

    /**
     * @param mixed $data
     *
     * @throws ParsingException
     */
    protected function validateImportData($data, array $context): void
    {
    }

    /**
     * @return class-string<T>
     */
    abstract protected function getClass(): string;

    /**
     * @psalm-param T $object
     */
    abstract protected function extractData($object, array $context): array;

    /**
     * @return array<key-of<TImportDto>, string|XmlImportConfig>
     */
    abstract protected function getImportDataConfig(array $context): array;

    /**
     * @param mixed $data
     *
     * @return T
     */
    abstract protected function generateImportedObject($data, array $context);

    /**
     * @param T          $object
     * @param TImportDto $data
     *
     * @return T
     */
    abstract protected function completeImportedObject($object, $data, ?string $format, array $context);

    /**
     * @param XmlImportConfig|DataType $config
     * @param mixed                    $data
     * @param array-key                $key
     *
     * @return mixed
     *
     * @throws ExceptionInterface
     * @throws ParsingException
     */
    protected function denormalizeDataItem($config, $data, $key, ?string $format, array $context)
    {
        $config = $config instanceof XmlImportConfig ? $config : new XmlImportConfig($config);
        $type = $config->getType();

        if (!$config->isArray() && \is_array($data) && array_is_list($data)) {
            $data = \count($data) > 0 ? current($data) : null;
        }

        if (null === $data) {
            $denormalized = ($config->isArray() || 'array' === $config->getType()) ? [] : null;
        } elseif ($config->getFlags() & XmlImportConfig::REFERENCE) {
            $denormalized = $this->denormalizeDataItemReference($data, $key, $config, $format, $context);
        } elseif (is_a($type, IdentifiedEntity::class, true)) {
            $denormalized = $this->denormalizeDataItemEntity($data, $key, $config, $format, $context);
        } else {
            $denormalized = $this->denormalizeDataItemDefault($data, $key, $config, $format, $context);
        }

        if ($config->getFlags() & XmlImportConfig::COLLECTION) {
            $denormalized = new ArrayCollection($denormalized ?? []);
        }

        return $denormalized;
    }

    /**
     * @param mixed     $data
     * @param array-key $key
     *
     * @return IdentifiedEntity|IdentifiedEntity[]
     *
     * @throws ParsingException
     */
    protected function denormalizeDataItemReference($data, $key, XmlImportConfig $config, ?string $format, array $context)
    {
        $type = $config->getType();
        if (!\is_string($data) || !is_a($type, IdentifiedEntity::class, true)) {
            throw new UnexpectedValueException();
        }

        $denormalizeReference =
            /**
             * @throws ParsingException
             */
            fn (string $datum): IdentifiedEntity => self::getReaderHelper($context)->get($datum, $type);

        if ($config->isArray()) {
            $data = explode(' ', trim($data));
            $denormalized = array_map($denormalizeReference, $data);
        } else {
            $denormalized = $denormalizeReference($data);
        }

        return $denormalized;
    }

    /**
     * @param mixed     $data
     * @param array-key $key
     *
     * @return IdentifiedEntity|IdentifiedEntity[]
     *
     * @throws ExceptionInterface
     */
    protected function denormalizeDataItemEntity($data, $key, XmlImportConfig $config, ?string $format, array $context)
    {
        $type = $config->getType();
        if (!\is_array($data) || !is_a($type, IdentifiedEntity::class, true)) {
            throw new UnexpectedValueException();
        }

        $denormalizeEntity =
            /**
             * @param mixed $datum
             *
             * @throws ExceptionInterface
             */
            fn ($datum, ?int $index): IdentifiedEntity => $this->denormalizeIdentifiedEntity($context, $config, $key, $index, $datum, $type, $format);

        if ($config->isArray()) {
            $denormalized = [];
            foreach ($data as $i => $datum) {
                if ('' === $datum) {
                    // Serializer send empty string instead of empty array for empty XML tag
                    $datum = [];
                }
                $denormalized[] = $denormalizeEntity($datum, $i);
            }
        } else {
            $denormalized = $denormalizeEntity($data, null);
        }

        return $denormalized;
    }

    /**
     * @param array-key $key
     *
     * @return mixed
     *
     * @throws ExceptionInterface
     */
    protected function denormalizeDataItemDefault($data, $key, XmlImportConfig $config, ?string $format, array $context)
    {
        $denormalizeDefault =
            /**
             * @param mixed $datum
             *
             * @return mixed
             *
             * @throws ExceptionInterface
             */
            function ($datum, ?int $index) use ($key, $config, $format, $context) {
                $childrenContext = self::childrenReaderContext($context, $datum, $config, $key, $index);

                return $this->serializer->denormalize($datum, $config->getType(), $format, $childrenContext);
            };

        if ($config->isArray()) {
            $denormalized = [];
            foreach ($data as $i => $datum) {
                $denormalized[] = $denormalizeDefault($datum, $i);
            }
        } elseif (\is_string($data) && '' === trim($data)) {
            $denormalized = null;
        } else {
            $denormalized = $denormalizeDefault($data, null);
        }

        return $denormalized;
    }

    /**
     * @template TM of IdentifiedEntity
     *
     * @param array-key        $key
     * @param class-string<TM> $type
     * @param mixed            $data
     *
     * @return TM
     *
     * @throws ExceptionInterface
     */
    protected function denormalizeIdentifiedEntity(array $context, XmlImportConfig $exportConfig, $key, ?int $index, $data, string $type, ?string $format): IdentifiedEntity
    {
        $childrenContext = self::childrenReaderContext($context, $data, $exportConfig, $key, $index);
        $entity = $this->serializer->denormalize($data, $type, $format, $childrenContext);
        if (!$entity instanceof IdentifiedEntity) {
            throw new \LogicException('');
        }

        return $entity;
    }

    /**
     * @param string[] $overridableTypes
     */
    protected static function overrideWriterHelper(array $context, array $overridableTypes): array
    {
        return array_merge($context, [
            self::WRITER_HELPER => new OverrideWriterHelper(self::getWriterHelper($context), $overridableTypes),
        ]);
    }
}
