<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

use Mobile\Device\Entity\Anomaly;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;

final class WriterHelper extends AbstractHelper implements WriterHelperInterface
{
    /** @var array<string, string> */
    private array $map = [];
    private ?User $connectedUser;
    /** @var string[] */
    private array $annotationFiles = [];
    /** @var array<string, string> */
    private array $markFiles = [];
    /** @var AnnotationDto[] */
    private array $sessionAnnotations = [];
    /** @var Anomaly[] */
    private array $anomalies = [];

    public function __construct(?User $connectedUser)
    {
        $this->connectedUser = $connectedUser;
    }

    public function getConnectedUser(): ?User
    {
        return $this->connectedUser;
    }

    public function register(IdentifiedEntity $object, string $parentReference, ?string $type = null, ?int $index = null): string
    {
        $reference = $this->createReference($parentReference, $type, $index);
        $this->add($object->getUri(), $reference);

        return $reference;
    }

    public function add(string $uri, string $xmlReference): void
    {
        if (isset($this->map[$uri])) {
            throw new \LogicException(\sprintf('URI "%s" already exists. Can not add "%s" reference.', $uri, $xmlReference));
        }

        $this->map[$uri] = $xmlReference;
    }

    public function remove(string $uri): void
    {
        unset($this->map[$uri]);
    }

    public function get(string $uri): string
    {
        if (!$this->exists($uri)) {
            throw new \RuntimeException(\sprintf('URI "%s" not found', $uri));
        }

        return $this->map[$uri];
    }

    public function exists(string $uri): bool
    {
        return isset($this->map[$uri]);
    }

    public function addAnnotationFile(string $workspacePath): int
    {
        $key = \count($this->annotationFiles);
        $extension = $this->getFileExtension($workspacePath);
        $filename = "metadonnees/$key.$extension";

        $this->annotationFiles[$filename] = $workspacePath;

        return $key;
    }

    public function addMarkFile(string $workspacePath): string
    {
        $key = uniqid();
        $extension = $this->getFileExtension($workspacePath);
        $fileName = "echelles/$key.$extension";

        $this->markFiles[$fileName] = $workspacePath;

        return $fileName;
    }

    protected function getFileExtension(string $workspacePath): string
    {
        $size = getimagesize($workspacePath);
        if (false === $size) {
            throw new \LogicException(\sprintf('Unable to get image informations for "%s"', $workspacePath));
        }

        $extension = mb_substr($size['mime'], 6);
        if (false === $extension) {
            throw new \LogicException(\sprintf('Unmanaged mime type "%s" for "%s"', $size['mime'], $workspacePath));
        }

        return $extension;
    }

    public function addSessionAnnotations(array $annotationsDto): void
    {
        foreach ($annotationsDto as $annotationDto) {
            $this->sessionAnnotations[] = $annotationDto;
        }
    }

    public function getSessionAnnotations(): array
    {
        return $this->sessionAnnotations;
    }

    public function addAnomaly(Anomaly $anomaly): void
    {
        $this->anomalies[] = $anomaly;
    }

    public function getAnomalies(): array
    {
        return $this->anomalies;
    }

    public function getAnnotationFiles(): array
    {
        return $this->annotationFiles;
    }

    public function getMarkFiles(): array
    {
        return $this->markFiles;
    }
}
