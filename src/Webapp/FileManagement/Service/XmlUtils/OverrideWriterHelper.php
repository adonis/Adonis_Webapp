<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

use Mobile\Device\Entity\Anomaly;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;

/**
 * Writer helper that allow to override a reference in a new context.
 */
final class OverrideWriterHelper extends AbstractHelper implements WriterHelperInterface
{
    private WriterHelperInterface $writerHelper;

    /** @var array<string, string> */
    private array $map = [];
    /** @var string[] */
    private array $overridableTypes;

    /**
     * @param string[] $overridableTypes
     */
    public function __construct(WriterHelperInterface $writerHelper, array $overridableTypes)
    {
        $this->writerHelper = $writerHelper;
        $this->overridableTypes = $overridableTypes;
    }

    public function getConnectedUser(): ?User
    {
        return $this->writerHelper->getConnectedUser();
    }

    public function register(IdentifiedEntity $object, string $parentReference, ?string $type = null, ?int $index = null): string
    {
        $reference = $this->createReference($parentReference, $type, $index);
        $this->add($object->getUri(), $reference);

        return $reference;
    }

    public function add(string $uri, string $xmlReference): void
    {
        if ($this->writerHelper->exists($uri) && $this->isOverridableType($xmlReference)) {
            $this->map[$uri] = $xmlReference;

            return;
        }

        $this->writerHelper->add($uri, $xmlReference);
    }

    public function remove(string $uri): void
    {
        if (isset($this->map[$uri])) {
            unset($this->map[$uri]);
        }

        $this->writerHelper->remove($uri);
    }

    public function get(string $uri): string
    {
        if (isset($this->map[$uri])) {
            return $this->map[$uri];
        }

        return $this->writerHelper->get($uri);
    }

    public function exists(string $uri): bool
    {
        return isset($this->map[$uri]) || $this->writerHelper->exists($uri);
    }

    public function addAnnotationFile(string $workspacePath): int
    {
        return $this->writerHelper->addAnnotationFile($workspacePath);
    }

    public function addMarkFile(string $workspacePath): string
    {
        return $this->writerHelper->addMarkFile($workspacePath);
    }

    public function addSessionAnnotations(array $annotationsDto): void
    {
        $this->writerHelper->addSessionAnnotations($annotationsDto);
    }

    public function getSessionAnnotations(): array
    {
        return $this->writerHelper->getSessionAnnotations();
    }

    public function addAnomaly(Anomaly $anomaly): void
    {
        $this->writerHelper->addAnomaly($anomaly);
    }

    public function getAnomalies(): array
    {
        return $this->writerHelper->getAnomalies();
    }

    public function getAnnotationFiles(): array
    {
        return $this->writerHelper->getAnnotationFiles();
    }

    public function getMarkFiles(): array
    {
        return $this->writerHelper->getMarkFiles();
    }

    private function isOverridableType(string $xmlReference): bool
    {
        $matches = [];

        return preg_match('/^.*\/@(\w+)(?:.\d+)?$/', $xmlReference, $matches) && \in_array($matches[1], $this->overridableTypes, true);
    }
}
