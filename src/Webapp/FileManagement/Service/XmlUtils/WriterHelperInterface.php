<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

use Mobile\Device\Entity\Anomaly;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Webapp\FileManagement\Dto\Mobile\AnnotationDto;

interface WriterHelperInterface
{
    /**
     * Add a business object and return its reference.
     *
     * @return string Business object's reference
     */
    public function createReference(string $parentReference, ?string $type = null, ?int $index = null): string;

    public function getConnectedUser(): ?User;

    /**
     * Add a business object and return its reference.
     *
     * @return string Business object's reference
     */
    public function register(IdentifiedEntity $object, string $parentReference, ?string $type = null, ?int $index = null): string;

    public function add(string $uri, string $xmlReference): void;

    public function remove(string $uri): void;

    /**
     * @throw \RuntimeException
     */
    public function get(string $uri): string;

    public function exists(string $uri): bool;

    public function addAnnotationFile(string $workspacePath): int;

    public function addMarkFile(string $workspacePath): string;

    /**
     * @param AnnotationDto[] $annotationsDto
     */
    public function addSessionAnnotations(array $annotationsDto): void;

    /**
     * @return AnnotationDto[]
     */
    public function getSessionAnnotations(): array;

    public function addAnomaly(Anomaly $anomaly): void;

    /**
     * @return Anomaly[]
     */
    public function getAnomalies(): array;

    /** @return string[] */
    public function getAnnotationFiles(): array;

    /** @return string[] */
    public function getMarkFiles(): array;
}
