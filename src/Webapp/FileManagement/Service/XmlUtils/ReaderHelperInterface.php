<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service\XmlUtils;

use Shared\Authentication\Entity\IdentifiedEntity;
use Webapp\FileManagement\Exception\ParsingException;

interface ReaderHelperInterface
{
    /**
     * Create a reference for a new business object.
     *
     * @return string Business object's reference
     */
    public function createReference(string $parentReference, ?string $type = null, ?int $index = null): string;

    /**
     * Add a business object.
     *
     * @throws ParsingException
     */
    public function add(string $reference, IdentifiedEntity $object): void;

    /**
     * Get a business object from reference.
     *
     * @template T of IdentifiedEntity
     *
     * @param class-string<T> $class
     *
     * @return T
     *
     * @throws ParsingException
     */
    public function get(string $reference, string $class): object;

    /**
     * Get a business object from reference without type check.
     *
     * @throws ParsingException
     */
    public function getObject(string $reference): IdentifiedEntity;

    /**
     * Check if a reference is present.
     */
    public function exists(string $reference): bool;

    /**
     * Get the reference of a business object already registered.
     *
     * @throws ParsingException
     */
    public function getReference(IdentifiedEntity $object): string;
}
