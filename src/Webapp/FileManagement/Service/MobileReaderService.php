<?php

namespace Webapp\FileManagement\Service;

use Doctrine\ORM\EntityManagerInterface;
use Mobile\Device\Entity\Block;
use Mobile\Device\Entity\Device;
use Mobile\Shared\Util\PlatformCrawler;
use Shared\FileManagement\Entity\UserLinkedJob;
use Shared\TransferSync\Entity\StatusDataEntry;
use Vich\UploaderBundle\Storage\StorageInterface;
use Webapp\FileManagement\Entity\ParsingJob;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;

class MobileReaderService extends AbstractReaderService
{
    private MobileXmlReaderService $mobileXmlReaderService;

    public function __construct(
        EntityManagerInterface $entityManager,
        DirectoryNamer         $directoryNameManager,
        StorageInterface       $storage,
        MobileXmlReaderService $mobileXmlReaderService,
        string                 $tmpDir
    )
    {
        parent::__construct($entityManager, $directoryNameManager, $storage, $tmpDir);
        $this->mobileXmlReaderService = $mobileXmlReaderService;
    }

    /**
     * @param int $projectFileId
     *
     * @throws ParsingException
     */
    public function parseProjectFile(int $projectFileId): void
    {
        $requestFile = $this->entityManager->getRepository(RequestFile::class)->find($projectFileId);
        $this->currentParsingStateError = -1;
        $requestFile->setStatus(RequestFile::STATUS_RUNNING);
        $this->entityManager->flush();

        $this->copyFileInTmpDir($requestFile);

        $xml = $this->getSimpleXMLElement($requestFile);
        $projectDto = $this->mobileXmlReaderService->readProjectFile($xml->asXML(), $this->getTmpDir($requestFile));

        // Proceed to data association.
        $projectDto->dataEntryProject->setOriginFile($requestFile);

        // Create status project to allow synchronization.
        $project = new StatusDataEntry($requestFile->getUser(), StatusDataEntry::STATUS_NEW);
        $project->setSyncable(false);
        $project->setRequest($projectDto->dataEntryProject);

        $this->verifyProjectIntegrity($project);

        $this->cleanTmpDir($requestFile);

        $this->currentParsingStateError = RequestFile::PARSING_STATE_DATABASE_SAVE;
        $this->entityManager->persist($project);
        $requestFile->setStatus(UserLinkedJob::STATUS_SUCCESS);
        $this->entityManager->flush();
    }

    /**
     * @param int $id
     *
     * @throws ParsingException
     */
    public function parseReturnFile(int $id): void
    {
        $parsingJob = $this->entityManager->getRepository(ParsingJob::class)->find($id);
        $this->currentParsingStateError = -1;
        $parsingJob->setStatus(UserLinkedJob::STATUS_RUNNING);
        $this->entityManager->flush();

        $this->copyFileInTmpDir($parsingJob);

        $xml = $this->getSimpleXMLElement($parsingJob);
        $statusDataEntry = $parsingJob->getStatusDataEntry();
        $returnFileDto = $this->mobileXmlReaderService->readReturnFile(
            $xml->asXML(),
            $statusDataEntry->getRequest()->getPlatform()->getName(),
            $statusDataEntry->getRequest()->getName(),
            $this->getTmpDir($parsingJob)
        );

        $statusDataEntry
            ->setResponse($returnFileDto->dataEntryProject)
            ->setStatus(StatusDataEntry::STATUS_WRITE_PENDING);
        //Needed to delete the status
        $parsingJob->setStatusDataEntry(null);

        $this->cleanTmpDir($parsingJob);

        $this->currentParsingStateError = RequestFile::PARSING_STATE_DATABASE_SAVE;
        $parsingJob->setStatus(UserLinkedJob::STATUS_SUCCESS);
        $this->entityManager->flush();
    }

    private function verifyProjectIntegrity(StatusDataEntry $project): void
    {
        $crawler = new PlatformCrawler();
        $blockNumbers = [];
        //The crawler use a depth-first algorithm by calling the function when entering a node.
        //So we assume that the block callback is called in his parent device context setup (here initialised bu an empty array)
        $crawler->setDeviceCallable(function (Device $d) use (&$blockNumbers) {
            $blockNumbers = [];
        });
        $crawler->setBlockCallable(function (Block $b) use (&$blockNumbers) {
            //If the number is already used in the device, we stop the crawler and throw an error
            if (isset($blockNumbers[$b->getName()])) {
                throw new ParsingException("", RequestFile::SEMANTIC_DUPLICATE_BLOCK_NUMBER);
            }
            $blockNumbers[$b->getName()] = true;
        });
        $crawler->crawl($project->getRequest()->getPlatform());
    }
}
