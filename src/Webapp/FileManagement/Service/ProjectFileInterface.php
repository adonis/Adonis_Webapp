<?php

namespace Webapp\FileManagement\Service;

use Shared\Authentication\Entity\User;

interface ProjectFileInterface
{
    public function getUser(): User;

    public function getName(): string;

    public function getUniqDirectoryName(): ?string;
}
