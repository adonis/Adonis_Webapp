<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Service;

use Symfony\Component\Finder\Finder;
use Webapp\FileManagement\Entity\Base\UserLinkedFileJob;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Exception\ParsingException;

/**
 * Class WebappParserService.
 */
abstract class AbstractReaderService extends AbstractFileService
{
    /**
     * @var int state at which the process has throw an error
     */
    public int $currentParsingStateError = 0;

    /**
     * Method to extract the simpleXMLElement from a zip project file.
     *
     * @throws ParsingException
     */
    protected function getSimpleXMLElement(UserLinkedFileJob $parsingJob): \SimpleXMLElement
    {
        $tmpDir = $this->getTmpDir($parsingJob);
        if (str_ends_with($parsingJob->getFilePath(), '.zip')) {
            $zip = new \ZipArchive();
            $zipFilePath = $tmpDir.$parsingJob->getFilePath();
            $res = $zip->open($zipFilePath);
            if (true === $res) {
                $zip->extractTo($tmpDir);
                $zip->close();
            } else {
                throw new ParsingException('Error during unzipping', RequestFile::ERROR_DURING_UNZIPPING);
            }
            if (is_dir($tmpDir.'/'.$parsingJob->getName())) {
                // Move files from root zip path to working path.
                $finder = new Finder();
                foreach ($finder->in($tmpDir.'/'.$parsingJob->getName()) as $file) {
                    rename($file->getPathname(), $tmpDir.'/'.$file->getFilename());
                }
                rmdir($tmpDir.'/'.$parsingJob->getName());
            }
        }
        $xmlFilePath = file_exists($tmpDir.$parsingJob->getName().'.xml') ? $tmpDir.$parsingJob->getName().'.xml' : $tmpDir.'transfert.xml';
        if (!file_exists($xmlFilePath)) {
            throw new ParsingException('File not found');
        }
        $xml = simplexml_load_file($xmlFilePath, \SimpleXMLElement::class, \LIBXML_PARSEHUGE);
        if (false === $xml) {
            throw new ParsingException('Invalid Xml');
        }

        return $xml;
    }

    protected function copyFileInTmpDir(UserLinkedFileJob $projectFile): void
    {
        $source = $this->storage->resolvePath($projectFile, 'file');
        $target = $this->getTmpDir($projectFile).$projectFile->getFilePath();

        copy($source, $target);
    }
}
