<?php

declare(strict_types=1);

namespace Webapp\FileManagement\Worker;

use Doctrine\ORM\EntityManagerInterface;
use Shared\FileManagement\Entity\UserLinkedJob;
use Throwable;
use Webapp\FileManagement\Entity\ParsingJob;
use Webapp\FileManagement\Exception\MultipleParsingException;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\MobileReaderService;
use Webapp\FileManagement\Service\WebappReaderService;

class ParsingWorker extends AbstractParsingWorker
{
    private WebappReaderService $parserService;

    private MobileReaderService $mobileParserService;

    public function __construct(
        EntityManagerInterface $entityManager,
        WebappReaderService    $parserService,
        MobileReaderService    $mobileParserService
    )
    {
        parent::__construct($entityManager);
        $this->parserService = $parserService;
        $this->mobileParserService = $mobileParserService;
    }

    /**
     * Read RequestFile.
     */
    public function readExperiment(int $parsingJobId): void
    {
        try {
            $this->parserService->readExperiment($parsingJobId);
        } catch (ParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, [0 => [$e->getCode()]]);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readExperimentCsv(int $parsingJobId): void
    {
        try {
            $this->parserService->readExperimentCsv($parsingJobId);
        } catch (MultipleParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, $e->errors);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readProtocolCsv(int $parsingJobId): void
    {
        try {
            $this->parserService->readProtocolCsv($parsingJobId);
        } catch (MultipleParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, $e->errors);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readPathCsv(int $parsingJobId): void
    {
        try {
            $this->parserService->readPathCsv($parsingJobId);
        } catch (MultipleParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, $e->errors);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readDataEntryCsv(int $parsingJobId): void
    {
        try {
            $this->parserService->readDataEntryCsv($parsingJobId);
        } catch (MultipleParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, $e->errors);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readPlatform(int $parsingJobId): void
    {
        try {
            $this->parserService->readPlatform($parsingJobId);
        } catch (ParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, [0 => [$e->getCode()]]);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId, []);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readVariableCollection(int $parsingJobId): void
    {
        try {
            $this->parserService->readVariableCollection($parsingJobId);
        } catch (ParsingException $e) {
            $this->onError($e);
            $this->later()->setCsvError($parsingJobId, [0 => [$e->getCode()]]);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId, []);
        }
    }

    /**
     * Read RequestFile.
     */
    public function readDataEntryReturn(int $parsingJobId): void
    {
        try {
            $this->mobileParserService->parseReturnFile($parsingJobId);
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($parsingJobId);
        }
    }

    /**
     * Get worker's name
     */
    public function getName(): string
    {
        return 'ParsingWorker';
    }

    function setError($parsingJobId)
    {
        $projectFile = $this->entityManager->getRepository(ParsingJob::class)->find($parsingJobId);
        $projectFile->setStatus(UserLinkedJob::STATUS_ERROR);
        $projectFile->setStatusDataEntry(null);
        $this->entityManager->flush();
    }

    function setCsvError($parsingJobId, array $errors)
    {
        /** @var ParsingJob $projectFile */
        $projectFile = $this->entityManager->getRepository(ParsingJob::class)->find($parsingJobId);
        $projectFile->setErrors($errors);
        $projectFile->setStatus(UserLinkedJob::STATUS_ERROR);
        $this->entityManager->flush();
    }
}
