<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Worker;

use Doctrine\ORM\EntityManagerInterface;
use Throwable;
use Webapp\FileManagement\Exception\ParsingException;
use Webapp\FileManagement\Service\MobileReaderService;

class RequestDataEntryProjectWorker extends AbstractRequestWorker
{
    private MobileReaderService $fileReader;

    public function __construct(
        EntityManagerInterface $entityManager,
        MobileReaderService    $fileReader
    )
    {
        parent::__construct($entityManager);
        $this->fileReader = $fileReader;
    }

    /**
     * Read RequestFile.
     */
    public function readFile(int $projectFileId): void
    {
        try {
            $this->fileReader->parseProjectFile($projectFileId);
        } catch (ParsingException $e) {
            parent::onError($e);
            $this->later()->setError($projectFileId, $e->getCode());
        } catch (Throwable $e) {
            $this->onError($e);
            $this->later()->setError($projectFileId, $this->fileReader->currentParsingStateError);
        }
    }

    /**
     * Get worker's name
     */
    public function getName(): string
    {
        return 'RequestDataEntryProjectWorker';
    }

    protected function onError(Throwable $e): void
    {
        $this->entityManager->close();
        parent::onError($e);
    }
}
