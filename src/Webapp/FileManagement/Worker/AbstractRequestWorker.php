<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Worker;

use Psr\Log\LoggerAwareInterface;
use Shared\FileManagement\Entity\UserLinkedJob;
use Webapp\FileManagement\Entity\RequestFile;

/**
 * Class AbstractRequestWorker
 * @package Webapp\FileManagement\Worker
 */
abstract class AbstractRequestWorker extends AbstractParsingWorker implements LoggerAwareInterface
{

    /**
     * Defines an error in execution process.
     * @param int $projectFileId
     * @param int $error
     */
    public function setError(int $projectFileId, int $error)
    {
        $this->setErrors($projectFileId, [0 => [$error]]);
    }

    /**
     * Defines multiple errors in execution process.
     * @param int $projectFileId
     * @param array $errors
     */
    public function setErrors(int $projectFileId, array $errors)
    {
        $projectFile = $this->entityManager->getRepository(RequestFile::class)->find($projectFileId);
        $projectFile->setStatus(UserLinkedJob::STATUS_ERROR);
        $projectFile->setLinedError($errors);
        $this->entityManager->flush();
    }
}
