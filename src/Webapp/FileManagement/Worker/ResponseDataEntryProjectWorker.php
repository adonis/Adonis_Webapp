<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Worker;

use Doctrine\ORM\EntityManagerInterface;
use Shared\TransferSync\Entity\StatusDataEntry;
use Throwable;
use Webapp\FileManagement\Exception\WritingException;
use Webapp\FileManagement\Service\MobileWriterService;

/**
 * Class ResponseDataEntryProjectWorker
 * @package Webapp\FileManagement\Worker
 */
class ResponseDataEntryProjectWorker extends AbstractResponseWorker
{
    private MobileWriterService $fileWriter;

    /**
     * ResponseDataEntryProjectWorker constructor.
     * @param EntityManagerInterface $entityManager
     * @param MobileWriterService $fileWriter
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        MobileWriterService    $fileWriter
    )
    {
        parent::__construct($entityManager);
        $this->fileWriter = $fileWriter;
    }

    /**
     * Create response file for a DataEntryProject instance.
     * @param int $responseFileId
     * @param int|null $statusProjectId
     */
    public function createFile(int $responseFileId, ?int $statusProjectId = null): void
    {
        try {

            $this->fileWriter->createFile($responseFileId, $statusProjectId);

        } catch (WritingException $writingException) {

            $this->onError($writingException);
            $this->setError($responseFileId, $writingException);
            $this->setSyncStateToDataEntryStatus($statusProjectId);

        } catch (Throwable $error) {

            $this->onError($error);
            $this->setError($responseFileId, $error);
            $this->setSyncStateToDataEntryStatus($statusProjectId);

        }
    }

    /**
     * @param int|null $statusProjectId
     */
    private function setSyncStateToDataEntryStatus(?int $statusProjectId)
    {
        if (!is_null($statusProjectId)) {
            /** @var $statusProject StatusDataEntry */
            $statusProject = $this->entityManager->getRepository(StatusDataEntry::class)->find($statusProjectId);
            $statusProject->setStatus(StatusDataEntry::STATUS_SYNCHRONIZED);
        }
    }

    /**
     * Get worker's name.
     */
    public function getName(): string
    {
        return 'ResponseDataEntryProjectWorker';
    }
}
