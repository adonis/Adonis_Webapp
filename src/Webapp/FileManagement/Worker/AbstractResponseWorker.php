<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Worker;

use Psr\Log\LoggerAwareInterface;
use Shared\FileManagement\Entity\UserLinkedJob;
use Throwable;
use Webapp\FileManagement\Entity\ResponseFile;

/**
 * Class AbstractResponseWorker
 * @package Webapp\FileManagement\Worker
 */
abstract class AbstractResponseWorker extends AbstractParsingWorker implements LoggerAwareInterface
{

    /**
     * Update the state of the project in error
     * @param int $projectFileId
     * @param Throwable $error
     */
    public function setError(int $projectFileId, Throwable $error)
    {
        $this->logger->error($error);
        /** @var $projectFile ResponseFile */
        $projectFile = $this->entityManager->getRepository(ResponseFile::class)->find($projectFileId);
        $projectFile->setStatus(UserLinkedJob::STATUS_ERROR);
        $this->entityManager->flush();
    }
}
