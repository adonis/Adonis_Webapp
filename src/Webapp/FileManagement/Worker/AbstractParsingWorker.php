<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Worker;

use Doctrine\ORM\EntityManagerInterface;
use Dtc\QueueBundle\Model\Worker;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class AbstractParsingWorker.
 */
abstract class AbstractParsingWorker extends Worker implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected EntityManagerInterface $entityManager;

    /**
     * AbstractRequestWorker constructor.
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    protected function onError(\Throwable $e): void
    {
        $this->logger->error('Parsing exception.', ['code' => $e->getCode(), 'message' => $e->getMessage()], $e);
        $this->entityManager->clear();
    }
}
