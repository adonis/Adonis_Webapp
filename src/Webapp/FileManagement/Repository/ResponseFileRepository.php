<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Repository;

use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Webapp\FileManagement\Entity\ResponseFile;

/**
 * @template-extends ServiceEntityRepository<ResponseFile>
 */
class ResponseFileRepository extends ServiceEntityRepository
{
    /**
     * ResponseFileRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResponseFile::class);
    }

    /**
     * Retrieve all response files that have been deleted before a given date.
     *
     * @param DateTime $deletionDateLimit
     * @param int | null $maxResults
     *
     * @return ResponseFile[]
     */
    public function getDeletedBeforeDate(DateTime $deletionDateLimit, int $maxResults = null): array
    {
        $qb = $this->createQueryBuilder('rf');
        $qb->where(
            $qb->expr()->lt('rf.deleted', ':deletionDate')
        );
        $qb->setParameter('deletionDate', $deletionDateLimit);
        if (!is_null($maxResults)) {
            $qb->setMaxResults($maxResults);
        }
        return $qb->getQuery()->getResult();
    }
}
