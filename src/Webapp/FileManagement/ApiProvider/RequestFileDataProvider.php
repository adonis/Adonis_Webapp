<?php

namespace Webapp\FileManagement\ApiProvider;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\ContextAwareQueryResultCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGenerator;
use ApiPlatform\Core\DataProvider\ContextAwareCollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Doctrine\Persistence\ManagerRegistry;
use Shared\Authentication\Entity\User;
use Symfony\Component\Security\Core\Security;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Repository\RequestFileRepository;

/**
 * Class RequestFileDataProvider.
 */
class RequestFileDataProvider implements ContextAwareCollectionDataProviderInterface, RestrictedDataProviderInterface
{
    /**
     * @var User
     */
    private $currentUser;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var iterable
     */
    private $collectionExtensions;

    public function __construct(Security $security, ManagerRegistry $managerRegistry, iterable $collectionExtensions)
    {
        $this->currentUser = $security->getUser();
        $this->managerRegistry = $managerRegistry;
        $this->collectionExtensions = $collectionExtensions;
    }

    public function getCollection(string $resourceClass, string $operationName = null, array $context = []): iterable
    {
        $manager = $this->managerRegistry->getManagerForClass(RequestFile::class);
        /** @var RequestFileRepository $repository */
        $repository = $manager->getRepository(RequestFile::class);
        $queryBuilder = $repository->createQueryBuilder('req');
        $queryNameGenerator = new QueryNameGenerator();
        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('req.user', ':current_user')
            )
        );
        $queryBuilder->setParameter('current_user', $this->currentUser);

        foreach ($this->collectionExtensions as $extension) {
            $extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass, $operationName, $context);
            if ($extension instanceof ContextAwareQueryResultCollectionExtensionInterface && $extension->supportsResult($resourceClass, $operationName, $context)) {
                return $extension->getResult($queryBuilder, $resourceClass, $operationName, $context);
            }
        }

        return $queryBuilder->getQuery()->execute();
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return RequestFile::class === $resourceClass && $operationName === "get";
    }
}
