<?php

namespace Webapp\FileManagement\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Worker\RequestDataEntryProjectWorker;

/**
 * Class FileUploadSubscriber.
 */
final class OnFileUploadSubscriber implements EventSubscriberInterface
{
    private RequestDataEntryProjectWorker $requestDataEntryProjectWorker;

    /**
     * OnFileUploadSubscriber constructor.
     * @param RequestDataEntryProjectWorker $requestDataEntryProjectWorker
     */
    public function __construct(
        RequestDataEntryProjectWorker $requestDataEntryProjectWorker
    )
    {
        $this->requestDataEntryProjectWorker = $requestDataEntryProjectWorker;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['startParsing', EventPriorities::POST_WRITE],
        ];
    }

    public function startParsing(ViewEvent $event)
    {
        $result = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$result instanceof RequestFile || Request::METHOD_POST !== $method) {
            return;
        }
        $requestFile = $result;
        $this->requestDataEntryProjectWorker->later()->readFile($requestFile->getId());
    }

}
