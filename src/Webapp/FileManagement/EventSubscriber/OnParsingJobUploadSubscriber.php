<?php

namespace Webapp\FileManagement\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Webapp\FileManagement\Entity\ParsingJob;
use Webapp\FileManagement\Worker\ParsingWorker;

/**
 * Class FileUploadSubscriber.
 */
final class OnParsingJobUploadSubscriber implements EventSubscriberInterface
{
    private ParsingWorker $parsingWorker;

    public function __construct(
        ParsingWorker $parsingWorker
    )
    {
        $this->parsingWorker = $parsingWorker;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['startParsing', EventPriorities::POST_WRITE],
        ];
    }

    public function startParsing(ViewEvent $event)
    {
        $result = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$result instanceof ParsingJob || Request::METHOD_POST !== $method) {
            return;
        }
        $parsingJob = $result;

        switch ($parsingJob->getObjectType()) {
            case ParsingJob::TYPE_EXPERIMENT_CSV:
                $this->parsingWorker->later()->readExperimentCsv($parsingJob->getId());
                break;
            case ParsingJob::TYPE_PROTOCOL_CSV:
                $this->parsingWorker->later()->readProtocolCsv($parsingJob->getId());
                break;
            case ParsingJob::TYPE_DATA_ENTRY_CSV:
                $this->parsingWorker->later()->readDataEntryCsv($parsingJob->getId());
                break;
            case ParsingJob::TYPE_PATH_BASE_CSV:
                $this->parsingWorker->later()->readPathCsv($parsingJob->getId());
                break;
            case ParsingJob::TYPE_VARIABLE_COLLECTION:
                $this->parsingWorker->later()->readVariableCollection($parsingJob->getId());
                break;
            case ParsingJob::TYPE_EXPERIMENT:
                $this->parsingWorker->later()->readExperiment($parsingJob->getId());
                break;
            case ParsingJob::TYPE_PLATFORM:
                $this->parsingWorker->later()->readPlatform($parsingJob->getId());
                break;
            case ParsingJob::TYPE_PROJECT_DATA_ZIP:
                $this->parsingWorker->later()->readDataEntryReturn($parsingJob->getId());
                break;
            default:
                break;
        }

    }
}
