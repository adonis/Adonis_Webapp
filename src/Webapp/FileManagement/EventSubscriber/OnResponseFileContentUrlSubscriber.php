<?php

namespace Webapp\FileManagement\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Entity\ResponseFile;
use function is_a;

/**
 * Class ResolveResponseFileContentUrlSubscriber.
 */
final class OnResponseFileContentUrlSubscriber implements EventSubscriberInterface
{
    private UploaderHelper $helper;

    private string $baseURL;

    public function __construct(UploaderHelper $helper, string $baseURL)
    {
        $this->helper = $helper;
        $this->baseURL = $baseURL;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !(
                is_a($attributes['resource_class'], RequestFile::class, true) ||
                is_a($attributes['resource_class'], ResponseFile::class, true)
            )
        ) {
            return;
        }

        $items = $controllerResult;
        if (!is_iterable($items)) {
            $items = [$items];
        }
        foreach ($items as $item) {
            if ($item instanceof RequestFile) {
                $item->setContentUrl($this->helper->asset($item, 'file'));
            } elseif ($item instanceof ResponseFile) {
                $item->setContentUrl($this->baseURL . 'download/response-files/' . $item->getId());
            }
        }
    }
}
