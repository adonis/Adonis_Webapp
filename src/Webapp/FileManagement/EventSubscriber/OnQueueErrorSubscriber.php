<?php

namespace Webapp\FileManagement\EventSubscriber;

use Doctrine\ORM\EntityManagerInterface;
use Dtc\QueueBundle\EventDispatcher\Event;
use Dtc\QueueBundle\EventDispatcher\EventSubscriberInterface;

/**
 * Class QueueErrorHandler.
 */
class OnQueueErrorSubscriber implements EventSubscriberInterface
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return array(
            Event::POST_JOB => 'onPostJob'
        );
    }

    public function onPostJob()
    {
        $this->entityManager->clear();
    }

}
