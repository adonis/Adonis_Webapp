<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\DataEntryProject;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Webapp\FileManagement\ApiOperation\CreateRequestFileOperation;
use Webapp\FileManagement\Entity\Base\UserLinkedFileJob;
use Webapp\FileManagement\Service\ProjectFileInterface;

/**
 * @ApiResource(
 *     normalizationContext={
 *         "groups"={"request_file_read", "user_linked_file_job_read", "id_read"}
 *     },
 *     denormalizationContext={
 *         "groups"={"request_file_write"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateRequestFileOperation::class,
 *             "deserialize"=false,
 *             "access_control"="is_granted('ROLE_USER')",
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     },
 *                                     "type"={
 *                                          "type"="string",
 *                                          "enum"={"dataEntryProject"},
 *                                          "example"="dataEntryProject"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get" = {}
 *     },
 *     itemOperations={
 *         "get"={ "is_granted('ROLE_USER') && security"="object.getUser() == user" },
 *         "patch"={ "is_granted('ROLE_USER') && security"="object.getUser() == user" },
 *         "delete" = { "security"= "is_granted('ROLE_USER') && object.getUser() == user" }
 *     },
 *     attributes={
 *          "order"={"id":"DESC"},
 *          "security"="is_granted('ROLE_USER')"
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="Webapp\FileManagement\Repository\RequestFileRepository")
 *
 * @ORM\Table(name="file_request", schema="webapp")
 *
 * @Vich\Uploadable
 */
class RequestFile extends UserLinkedFileJob implements ProjectFileInterface
{
    public const ERROR_INCORRECT_FILE_TYPE = 1;
    public const ERROR_DURING_UNZIPPING = 2;
    public const ERROR_EXTRACTED_FILE_NOT_FOUND = 3;
    public const ERROR_DATA_ENTRY_PROJECT_MISSING = 4;
    public const ERROR_PLATFORM_MISSING = 5;
    public const ERROR_NO_DEVICE = 6;
    public const ERROR_NO_BLOC = 7;
    public const ERROR_NO_BLOC_CHILDREN = 8;
    public const ERROR_NO_WORKPATH = 9;
    public const ERROR_DUPLICATED_URI = 10;
    public const ERROR_WORKPATH_ITEM_NOT_FOUND = 11;
    public const ERROR_NO_PROTOCOL = 12;
    public const ERROR_PROTOCOL_INFO_MISSING = 13;
    public const ERROR_MODALITY_ITEM_NOT_FOUND = 14;
    public const ERROR_STATE_CODE_INFO_MISSING = 15;
    public const ERROR_NO_GENERATED_VARIABLE = 16;
    public const ERROR_TEST_INFO_MISSING = 17;
    public const ERROR_UNKNOWN_TEST_TYPE = 18;
    public const ERROR_UNKNOWN_TEST_LINKED_VARIABLE = 19;
    public const ERROR_PREVIOUS_VALUE_INFO_MISSING = 20;
    public const ERROR_UNKNOWN_PREVIOUS_VALUE_LINKED_OBJECT = 21;
    public const ERROR_URI_NOT_FOUND = 23;
    public const ERROR_FILE_NOT_FOUND = 24;
    public const ERROR_INCORRECT_ANNOTATION_TYPE = 27;
    public const ERROR_UNKNOWN_MATERIAL = 28;
    public const ERROR_NO_PLATFORM = 29;
    public const ERROR_NOTE_CREATOR_NOT_FOUND = 30;
    public const ERROR_VALUE_HINT_LIST_INFO_MISSING = 31;
    public const ERROR_SCALE_INFO_MISSING = 32;
    public const ERROR_OUT_EXPERIMENTATION_ZONE_INFO_MISSING = 33;
    public const ERROR_DATA_ENTRY_INFO_MISSING = 34;
    public const ERROR_WRONG_PLATFORM_NAME = 35;
    public const ERROR_NAME_ALREADY_IN_USE = 36;
    public const ERROR_STATE_VARIABLE_INFO_MISSING = 37;
    public const WARNING_UNKNOWN_SESSION_USER = 38;
    public const ERROR_NO_PROJECT_CREATOR = 39;

    public const ERROR_CSV_MISSING_FACTOR_NAME = 100;
    public const ERROR_CSV_MISSING_X = 101;
    public const ERROR_CSV_MISSING_Y = 102;
    public const ERROR_CSV_MISSING_BLOCK_NUMBER = 103;
    public const ERROR_CSV_MISSING_UNIT_PARCEL_NAME = 104;
    public const ERROR_CSV_MISSING_DEVICE_NAME = 105;
    public const ERROR_CSV_MISSING_TREATMENT = 106;
    public const ERROR_CSV_ALREADY_USED_POSITION = 107;
    public const ERROR_CSV_UNIT_PARCEL_MULTIPLE_TREATMENT = 108;
    public const ERROR_CSV_TREATMENT_MULTIPLE_FACTOR_COMBINATIONS = 109;
    public const ERROR_CSV_NON_NUMERIC_POSITION = 110;
    public const ERROR_CSV_EMPTY_VARIABLE_VALUE = 111;
    public const ERROR_CSV_DATA_ENTRY_OBJECT_NOT_FOUND = 112;
    public const ERROR_CSV_OBJECT_NOT_FOUND = 113;
    public const ERROR_CSV_MISSING_ORDER = 114;
    public const ERROR_CSV_MISSING_IDENTIFIER = 115;
    public const ERROR_CSV_MISSING_OPENSILEX_URI = 116;
    public const ERROR_CSV_ONLY_ONE_GERMPLASM = 117;
    public const ERROR_CSV_DIFFERENT_URI_FOR_SAME_MODALITY = 118;
    public const ERROR_CSV_EXPERIMENT_WKT_MALFORMED = 119;
    public const ERROR_CSV_BLOCK_WKT_MALFORMED = 120;
    public const ERROR_CSV_SUBBLOCK_WKT_MALFORMED = 121;
    public const ERROR_CSV_UP_WKT_MALFORMED = 122;
    public const ERROR_CSV_INDIVIDUAL_WKT_MALFORMED = 123;
    public const ERROR_CSV_OEZ_WKT_MALFORMED = 124;
    public const ERROR_CSV_INDIVIDUAL_ID_EXISTS = 125;
    public const ERROR_CSV_DIFFERENTS_TREATMENTS_WITH_SAME_MODALITIES = 126;
    public const PARSING_STATE_FIND_NAMESPACES = 201;
    public const PARSING_STATE_READ_STATE_CODE = 202;
    public const PARSING_STATE_READ_PROJECT_CREATOR = 203;
    public const PARSING_STATE_READ_DESKTOP_USER_COLLECTION = 204;
    public const PARSING_STATE_READ_NATURE_ZHE = 205;
    public const PARSING_STATE_READ_PLATFORM = 206;
    public const PARSING_STATE_READ_CONNECTED_VARIABLE_COLLECTION = 207;
    public const PARSING_STATE_READ_PROJECT = 208;
    public const PARSING_STATE_READ_GRAPHICAL_STRUCTURE = 209;
    public const PARSING_STATE_DATABASE_SAVE = 210;

    public const SEMANTIC_DUPLICATE_BLOCK_NUMBER = 300;

    /**
     * @Assert\NotNull(groups={"media_object_create"})
     *
     * @Vich\UploadableField(mapping="project_file", fileNameProperty="filePath")
     */
    protected $file;

    /**
     * @Groups({"request_file_read"})
     *
     * @ORM\Column(type="string")
     */
    protected string $name = '';

    /**
     * @var int[][]|null
     *
     * @Groups({"request_file_read"})
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $linedError = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $csvBindings = null;

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="originFile")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?DataEntryProject $project = null;

    /**
     * @Groups({"request_file_read", "request_file_write"})
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default"=false})
     */
    private bool $disabled = false;

    /**
     * @return int[][]|null
     *
     * @psalm-mutation-free
     */
    public function getLinedError(): ?array
    {
        return $this->linedError;
    }

    /**
     * @param int[][]|null $linedError
     *
     * @return $this
     */
    public function setLinedError(?array $linedError): self
    {
        $this->linedError = $linedError;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCsvBindings(): ?string
    {
        return $this->csvBindings;
    }

    /**
     * @return $this
     */
    public function setCsvBindings(?string $csvBindings): self
    {
        $this->csvBindings = $csvBindings;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): ?DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(?DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @return $this
     */
    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }
}
