<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Entity\Base;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Exception\NotInitializedPropertyException;
use Shared\FileManagement\Entity\UserLinkedJob;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Webapp\FileManagement\Service\ProjectFileInterface;

/**
 * @ORM\MappedSuperclass()
 *
 * @Gedmo\SoftDeleteable()
 *
 * @Vich\Uploadable()
 */
abstract class UserLinkedFileJob extends UserLinkedJob implements ProjectFileInterface
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="string")
     */
    protected string $name = '';

    /**
     * @Groups({"user_linked_file_job_read"})
     *
     * @ORM\Column(type="string")
     */
    protected string $status = '';

    /**
     * @ORM\Column(nullable=true)
     */
    protected ?string $filePath = null;

    /**
     * @Vich\UploadableField(mapping="project_file", fileNameProperty="filePath")
     */
    protected $file;

    /**
     * @Groups({"user_linked_file_job_read"})
     */
    protected ?string $contentUrl = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected ?string $uniqDirectoryName = null;

    /**
     * @Groups({"user_linked_file_job_read"})
     *
     * @ORM\Column(type="datetime")
     */
    protected ?\DateTime $uploadDate = null;

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function setFile($file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @return $this
     */
    public function setFilePath(?string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    /**
     * @return $this
     */
    public function setContentUrl(?string $contentUrl): self
    {
        $this->contentUrl = $contentUrl;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUniqDirectoryName(): ?string
    {
        return $this->uniqDirectoryName;
    }

    /**
     * @return $this
     */
    public function setUniqDirectoryName(?string $uniqDirectoryName): self
    {
        $this->uniqDirectoryName = $uniqDirectoryName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUploadDate(): \DateTime
    {
        if (null === $this->uploadDate) {
            throw new NotInitializedPropertyException(self::class, 'uploadDate');
        }

        return $this->uploadDate;
    }

    /**
     * @return $this
     */
    public function setUploadDate(\DateTime $uploadDate): self
    {
        $this->uploadDate = $uploadDate;

        return $this;
    }
}
