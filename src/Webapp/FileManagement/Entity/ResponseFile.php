<?php

namespace Webapp\FileManagement\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Mobile\Project\Entity\DataEntryProject;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\FileManagement\Entity\Base\UserLinkedFileJob;

/**
 * @ApiResource(
 *     normalizationContext={
 *         "groups"={"response_file_read", "user_linked_file_job_read", "id_read"}
 *     },
 *     collectionOperations={
 *         "get" = {
 *         }
 *     },
 *     itemOperations={
 *         "get" = {
 *             "security"= "is_granted('ROLE_USER') && object.getUser() == user"
 *         },
 *         "delete" = {
 *             "security"= "is_granted('ROLE_USER') && object.getUser() == user"
 *         }
 *     },
 *     attributes={
 *          "order"={"id": "DESC"},
 *          "security"="is_granted('ROLE_USER')"
 *     }
 * )
 *
 * @ORM\Entity(repositoryClass="Webapp\FileManagement\Repository\ResponseFileRepository")
 *
 * @ORM\Table(name="file_response", schema="webapp")
 */
class ResponseFile extends UserLinkedFileJob
{
    /**
     * @Groups({"response_file_read"})
     *
     * @ORM\Column(type="string")
     */
    protected string $name = '';

    /**
     * @ORM\OneToOne(targetEntity="Mobile\Project\Entity\DataEntryProject", inversedBy="responseFile")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private ?DataEntryProject $project = null;

    /**
     * @psalm-mutation-free
     */
    public function getProject(): ?DataEntryProject
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(?DataEntryProject $project): self
    {
        $this->project = $project;

        return $this;
    }
}
