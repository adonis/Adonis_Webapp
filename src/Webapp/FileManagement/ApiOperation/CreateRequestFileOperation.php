<?php

namespace Webapp\FileManagement\ApiOperation;

use DateTime;
use Shared\Authentication\Entity\User;
use Shared\FileManagement\Entity\UserLinkedJob;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Webapp\FileManagement\Entity\RequestFile;

/**
 * Class CreateProjectFileAction.
 */
final class CreateRequestFileOperation
{
    public function __invoke(Request $request, Security $security): RequestFile
    {
        /** @var $user User */
        $user = $security->getUser();
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        if (!$uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }
        if (
            $uploadedFile->getClientMimeType() !== 'application/zip' &&
            $uploadedFile->getClientMimeType() !== 'application/zip-compressed' &&
            $uploadedFile->getClientMimeType() !== 'application/x-zip-compressed'
        ) {
            throw new BadRequestHttpException(sprintf('"file" must be a zip file %s obtained', $uploadedFile->getClientMimeType()));
        }
        $dataEntryFile = new RequestFile();
        $dataEntryFile->setUniqDirectoryName(uniqid());
        $dataEntryFile->setFile($uploadedFile);
        $dataEntryFile->setUser($user);
        $dataEntryFile->setStatus(UserLinkedJob::STATUS_PENDING);
        $dataEntryFile->setName(pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME));
        $dataEntryFile->setUploadDate(new DateTime());

        return $dataEntryFile;
    }
}
