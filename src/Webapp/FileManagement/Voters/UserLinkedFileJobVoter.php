<?php

namespace Webapp\FileManagement\Voters;

use LogicException;
use Webapp\FileManagement\Entity\Base\UserLinkedFileJob;
use Shared\Authentication\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class UserLinkedFileJobVoter.
 */
class UserLinkedFileJobVoter extends Voter
{
    const DOWNLOAD = 'download';

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::DOWNLOAD])) {
            return false;
        }

        // only vote on `UserLinkedFileJob` objects
        if (!$subject instanceof UserLinkedFileJob) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     *
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }
        /** @var UserLinkedFileJob $userLinkedFileJob */
        $userLinkedFileJob = $subject;

        switch ($attribute) {
            case self::DOWNLOAD:
                return $this->canDownload($userLinkedFileJob, $user);
        }

        throw new LogicException('This code should not be reached!');
    }

    private function canDownload(UserLinkedFileJob $post, User $user)
    {
        return $post->getUser() === $user;
    }
}
