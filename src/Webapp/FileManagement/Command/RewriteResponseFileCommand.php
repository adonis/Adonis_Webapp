<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Command;

use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Shared\FileManagement\Entity\UserLinkedJob;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Webapp\FileManagement\Entity\ResponseFile;
use Webapp\FileManagement\EventListener\SoftDeletionListener;
use Webapp\FileManagement\Worker\ResponseDataEntryProjectWorker;

/**
 * Class RewriteResponseFileCommand: Really delete files (request/response) that have been deleted by operators.
 */
class RewriteResponseFileCommand extends Command
{
    const ARG_FILE_RESPONSE_ID = 'fileResponseId';

    private ResponseDataEntryProjectWorker $dataEntryProjectWriter;

    private ManagerRegistry $managerRegistry;

    /**
     * RewriteResponseFileCommand constructor.
     * @param ResponseDataEntryProjectWorker $dataEntryProjectWriter
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(
        ResponseDataEntryProjectWorker $dataEntryProjectWriter,
        ManagerRegistry                $managerRegistry
    )
    {
        parent::__construct("adonis:file:rewrite");
        $this->dataEntryProjectWriter = $dataEntryProjectWriter;
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @see Command
     */
    protected function configure()
    {
        $this->setDescription(
            'Rewrite a response file.'
        );
        $this->addArgument(
            static::ARG_FILE_RESPONSE_ID,
            InputArgument::REQUIRED,
            'The file response id'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Delay in days from execution date.
        $fileId = $input->getArgument(static::ARG_FILE_RESPONSE_ID);
        $fileRepo = $this->managerRegistry->getRepository(ResponseFile::class);
        $responseFile = $fileRepo->find($fileId);

        if (is_null($responseFile)) {

            $output->writeln("No ResponseFile entity");

        } else {

            $output->writeln("ResponseFile entity id : " . $responseFile->getId());

            try {

                $responseFile->setStatus(UserLinkedJob::STATUS_PENDING);
                $this->managerRegistry->getManager()->flush();

                $this->dataEntryProjectWriter->createFile($responseFile->getId());

            } catch (Exception $exc) {
                $output->writeln("Une erreur est survenue, impossible de supprimer le ResponseFile existant.");
                $output->writeln($exc->getMessage());
                return 1;
            }

            $output->writeln("    Done");

        }
        return 0;
    }
}
