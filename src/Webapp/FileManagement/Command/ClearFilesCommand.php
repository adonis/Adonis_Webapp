<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\FileManagement\Command;

use DateTime;
use Doctrine\Persistence\ManagerRegistry;
use Shared\TransferSync\Entity\StatusDataEntry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Webapp\FileManagement\Entity\RequestFile;
use Webapp\FileManagement\Entity\ResponseFile;
use Webapp\FileManagement\Repository\RequestFileRepository;
use Webapp\FileManagement\Repository\ResponseFileRepository;

/**
 * Class CleanDeletedFileCommand: Really delete files (request/response) that have been deleted by operators.
 */
class ClearFilesCommand extends Command
{
    const ARG_DELAY = 'delay';

    const DEFAULT_DELAY = 30;

    const MAX_LOADING = 2;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * CleanDeletedFileCommand constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct("adonis:file:clear");
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @see Command
     */
    protected function configure()
    {
        $this->setDescription(
            'Clear files in database. An operator can mark files as deleted,'
            . 'this command actually remove them'
        );
        $this->addArgument(
            static::ARG_DELAY,
            InputArgument::REQUIRED,
            'Delay in days, files older than this delay will be removed');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // Delay in days from execution date.
        $delay = $input->getArgument(static::ARG_DELAY);
        $deletionDateLimit = date_create();
        $deletionDateLimit->modify("-${delay} days");
        $output->writeln(
            'Delete all files that have been deleted before date: '
            . $deletionDateLimit->format('Y-m-d')
        );
        // Deleting request files.
        $this->clearRequestFiles($deletionDateLimit, $output);
        // Deleting response files.
        $this->clearResponseFiles($deletionDateLimit, $output);
        return 0;
    }

    /**
     * Delete all request files that have been marked deleted before the given date.
     *
     * @param DateTime $deletionDateLimit
     * @param OutputInterface $output
     */
    protected function clearRequestFiles(DateTime $deletionDateLimit, OutputInterface $output): void
    {
        $output->writeln('Deleting request files...');

        $requestFileRepo = $this->managerRegistry->getRepository(RequestFile::class);
        $this->handle($requestFileRepo, $deletionDateLimit, $output);
        $output->writeln('... Request files deleted');
    }

    /**
     * Delete all response files that have been marked deleted before the given date.
     *
     * @param DateTime $deletionDateLimit
     * @param OutputInterface $output
     */
    protected function clearResponseFiles(DateTime $deletionDateLimit, OutputInterface $output): void
    {
        $output->writeln('Deleting response files...');

        $responseFileRepo = $this->managerRegistry->getRepository(ResponseFile::class);
        $this->handle($responseFileRepo, $deletionDateLimit, $output);
        $output->writeln('... Response files deleted');
    }

    /**
     * @param RequestFile[]|ResponseFile[] $files
     */
    protected function deleteAllFiles(array $files): void
    {
        $manager = $this->managerRegistry->getManager();
        array_walk(
            $files,
            function ($file) use ($manager) {
                /** @var $file RequestFile|ResponseFile */
                // handle project status.
                $status = $this->updateProjectStatus($file);
                if (!is_null($status) && is_null($status->getRequest()) && is_null($status->getResponse())) {
                    $manager->remove($status);
                }
                // Handle file removal, or project if exists (will cascade on file itself).
                if (is_null($file->getProject())) {
                    $manager->remove($file);
                } else {
                    $manager->remove($file->getProject());
                }
            }
        );
        $manager->flush();
    }

    /**
     * Update project status if exists to delete the resource.
     *
     * @param RequestFile|ResponseFile $file
     *
     * @return StatusDataEntry
     * @throws InternalErrorException
     */
    protected function updateProjectStatus($file): StatusDataEntry
    {
        $repo = $this->managerRegistry->getRepository(StatusDataEntry::class);

        if ($file instanceof RequestFile) {
            $status = $repo->findOneBy(['request' => $file->getProject()]);
            if (!is_null($status)) {
                $status->setRequest(null);
            }
        } else {
            $status = $repo->findOneBy(['response' => $file->getProject()]);
            if (!is_null($status)) {
                $status->setResponse(null);
            }
        }
        if ($status === null) {
            throw new InternalErrorException();
        }
        return $status;
    }

    /**
     * Handle loop deletion to call deleteAllFiles method while there are files to delete.
     *
     * @param ResponseFileRepository|RequestFileRepository $fileRepo
     * @param DateTime $deletionDateLimit
     * @param OutputInterface $output
     */
    protected function handle($fileRepo,
                              DateTime $deletionDateLimit,
                              OutputInterface $output): void
    {
        $total = 0;
        do {

            $fileToDelete = $fileRepo->getDeletedBeforeDate($deletionDateLimit, static::MAX_LOADING);
            $count = count($fileToDelete);
            $total += $count;
            $output->writeln("    => Delete ${count} files");
            $this->deleteAllFiles($fileToDelete);

        } while (0 < $count);

        $output->writeln("    => Total ${total} files deleted");
    }
}
