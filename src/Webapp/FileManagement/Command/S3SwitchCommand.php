<?php

declare(strict_types=1);

namespace Webapp\FileManagement\Command;

use Aws\S3\S3Client;
use Aws\S3\Transfer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class S3SwitchCommand : Move the files from filesystem to S3 and vice versa
 */
class S3SwitchCommand extends Command
{
    const OPT_REVERSE = 'reverse';

    private string $uploadDir;

    private S3Client $s3Client;

    private string $s3BucketName;

    public function __construct(
        string   $uploadDir,
        string   $s3BucketName,
        S3Client $s3Client)
    {
        parent::__construct("adonis:s3:move_files");
        $this->uploadDir = $uploadDir;
        $this->s3Client = $s3Client;
        $this->s3BucketName = $s3BucketName;
    }

    /**
     * @see Command
     */
    protected function configure()
    {
        $this->setDescription(
            'Copy files from file system to Aws S3 or S3 to filesystem'
        );
        $this->addOption(
            static::OPT_REVERSE,
            ['r'],
            InputOption::VALUE_NONE,
            'Reverse : copy from S3 to filesystem');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$input->getOption(static::OPT_REVERSE)) {
            $transfer = new Transfer(
                $this->s3Client,
                $this->uploadDir,
                's3://' . $this->s3BucketName
            );
            $transfer->transfer();
        } else {
            $transfer = new Transfer(
                $this->s3Client,
                's3://' . $this->s3BucketName,
                $this->uploadDir
            );
            $transfer->transfer();
        }
        return 0;
    }


}
