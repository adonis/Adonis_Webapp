<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Webapp;

use Doctrine\Common\Collections\Collection;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\Device;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\SimpleVariable;

class VariableCollectionDto
{
    /** @var AbstractVariable[] */
    public array $variables = [];
    /** @var Device[] */
    public array $materiels = [];

    /**
     * @param AbstractVariable[]|Collection<int, AbstractVariable>|Collection<int, SimpleVariable>|Collection<int, SemiAutomaticVariable>|Collection<int, GeneratorVariable> $variables
     */
    public function addVariables($variables): void
    {
        foreach ($variables as $variable) {
            if (!\in_array($variable, $this->variables, true)) {
                $this->variables[] = $variable;
            }
        }
    }

    /** @param Device[]|Collection<int, Device> $materiels */
    public function addMateriels($materiels): void
    {
        foreach ($materiels as $materiel) {
            if (!\in_array($materiel, $this->materiels, true)) {
                $this->materiels[] = $materiel;
            }
        }
    }
}
