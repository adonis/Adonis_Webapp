<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Webapp;

use Webapp\Core\Entity\Device;

class DeviceDriverDto
{
    public ?string $type = null;
    public ?int $baudrate = null;
    public ?string $stopbit = null;
    public ?bool $push = null;
    public ?string $parity = null;
    public ?string $flowcontrol = null;
    public ?int $databitsFormat = null;
    public ?string $debutTrame = null;
    public ?string $finTrame = null;
    public ?string $separateurCsv = null;
    public ?int $tailleTrame = null;

    public static function fromDevice(Device $device): self
    {
        $object = new self();
        $object->type = $device->getCommunicationProtocol();
        $object->baudrate = $device->getBaudrate();
        $object->stopbit = $device->getStopbit();
        $object->push = $device->getRemoteControl();
        $object->parity = $device->getParity();
        $object->flowcontrol = $device->getFlowcontrol();
        $object->databitsFormat = $device->getBitFormat();
        $object->debutTrame = $device->getFrameStart();
        $object->finTrame = $device->getFrameEnd();
        $object->separateurCsv = $device->getFrameCsv();
        $object->tailleTrame = $device->getFrameLength();

        return $object;
    }

    public function toDevice(Device $device): void
    {
        $device
            ->setCommunicationProtocol($this->type ?? '')
            ->setBaudrate($this->baudrate)
            ->setStopBit($this->stopbit)
            ->setParity($this->parity)
            ->setFlowcontrol($this->flowcontrol)
            ->setRemoteControl($this->push ?? false)
            ->setBitFormat($this->databitsFormat)
            ->setFrameLength($this->tailleTrame ?? 0)
            ->setFrameStart($this->debutTrame)
            ->setFrameEnd($this->finTrame)
            ->setFrameCsv($this->separateurCsv);
    }
}
