<?php

namespace Webapp\FileManagement\Dto\Webapp;

class CheminementCalculeDto
{
    public ?string $declenchement = null;
    /** @var string[]|null */
    public ?array $objects = null;
}
