<?php

namespace Webapp\FileManagement\Dto\Webapp;

use Webapp\Core\Entity\ProjectData;

class DataEntryReturnDto
{
    public ?ProjectData $projectData = null;
}
