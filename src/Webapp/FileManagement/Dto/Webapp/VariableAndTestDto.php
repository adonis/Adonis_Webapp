<?php

namespace Webapp\FileManagement\Dto\Webapp;

use Webapp\Core\Entity\AbstractVariable;

class VariableAndTestDto
{
    public ?AbstractVariable $variable = null;

    public array $testDtos = [];
}
