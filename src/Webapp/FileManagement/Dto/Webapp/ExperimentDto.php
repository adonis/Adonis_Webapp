<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Webapp;

use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\OezNature;

class ExperimentDto
{
    public ?User $user;
    public ?Experiment $experiment;
    /** @var OezNature[] */
    public array $oezNatures;

    public function __construct(
        ?User $user = null,
        ?Experiment $experiment = null
    ) {
        $this->user = $user;
        $this->experiment = $experiment;

        if (null !== $experiment && null !== $experiment->getSite()) {
            $site = $experiment->getSite();
        } elseif (null !== $experiment && null !== $experiment->getPlatform()) {
            $site = $experiment->getPlatform()->getSite();
        } else {
            $site = null;
        }
        $this->oezNatures = $site ? $site->getOezNatures()->getValues() : [];
    }
}
