<?php

namespace Webapp\FileManagement\Dto\Webapp;

use Shared\Authentication\Entity\IdentifiedEntity;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Entity\StateCode;

class MeasureDto
{
    public Measure $measure;
    public ?StateCode $codeEtat = null;
    public ?string $valeur = null;
    public ?int $indiceGeneratrice = null;
    public ?string $mesureGeneratrice = null;
    public ?string $mesuresGenerees = null;
    public ?AbstractVariable $variable = null;
    public ?IdentifiedEntity $objetMetier = null;

    public function __construct(Measure $measure)
    {
        $this->measure = $measure;
    }
}
