<?php

namespace Webapp\FileManagement\Dto\Webapp;

use Shared\Authentication\Entity\User;
use Webapp\Core\Entity\OezNature;
use Webapp\Core\Entity\Platform;

class PlatformDto
{
    public ?User $user = null;
    public ?Platform $platform = null;
    /** @var OezNature[] */
    public array $oezNatures = [];
    public bool $withData = false;

    /**
     * @param OezNature[] $oezNatures
     */
    public function __construct(
        ?User $user = null,
        ?Platform $platform = null,
        array $oezNatures = [],
        bool $withData = false
    ) {
        $this->user = $user;
        $this->platform = $platform;
        $this->oezNatures = $oezNatures;
        $this->withData = $withData;
    }
}
