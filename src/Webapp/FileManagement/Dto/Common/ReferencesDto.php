<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Common;

use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @template T of IdentifiedEntity
 */
class ReferencesDto
{
    /**
     * @var T[]
     */
    private array $references;

    /**
     * @param iterable<array-key, T> $references
     */
    public function __construct(iterable $references)
    {
        $this->references = \is_array($references) ? $references : iterator_to_array($references);
    }

    /**
     * @return T[]
     *
     * @psalm-mutation-free
     */
    public function getReferences(): array
    {
        return $this->references;
    }
}
