<?php

namespace Webapp\FileManagement\Dto\Common;

class MinMaxDoubleDto
{
    public ?float $min;
    public ?float $max;

    public function __construct(?float $min = null, ?float $max = null)
    {
        $this->min = $min;
        $this->max = $max;
    }
}
