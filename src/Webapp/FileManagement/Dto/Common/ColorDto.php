<?php

namespace Webapp\FileManagement\Dto\Common;

use Symfony\Component\Serializer\Annotation as Serializer;

class ColorDto
{
    /**
     * @Serializer\SerializedName("#")
     */
    public int $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }
}
