<?php

namespace Webapp\FileManagement\Dto\Common;

class PieceJointeDto
{
    public ?string $workspacePath = '';
    public ?string $archivePath = '';

    public function __construct(?string $workspacePath, ?string $archivePath)
    {
        $this->workspacePath = $workspacePath;
        $this->archivePath = $archivePath;
    }
}
