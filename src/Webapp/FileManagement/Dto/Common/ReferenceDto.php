<?php

namespace Webapp\FileManagement\Dto\Common;

use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @template T of IdentifiedEntity
 */
class ReferenceDto
{
    private IdentifiedEntity $entity;

    public function __construct(IdentifiedEntity $entity)
    {
        $this->entity = $entity;
    }

    public function getEntity(): IdentifiedEntity
    {
        return $this->entity;
    }
}
