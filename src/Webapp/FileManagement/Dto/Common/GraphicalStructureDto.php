<?php

namespace Webapp\FileManagement\Dto\Common;

class GraphicalStructureDto
{
    public int $xMesh;
    public int $yMesh;
    public int $origin;

    public function __construct(int $xMesh, int $yMesh, int $origin)
    {
        $this->xMesh = $xMesh;
        $this->yMesh = $yMesh;
        $this->origin = $origin;
    }
}
