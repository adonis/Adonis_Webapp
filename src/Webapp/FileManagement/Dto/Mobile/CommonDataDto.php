<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Mobile;

use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\StateCode;
use Mobile\Project\Entity\DataEntryProject;
use Mobile\Project\Entity\DesktopUser;
use Mobile\Project\Entity\GraphicalStructure;
use Mobile\Project\Entity\NatureZHE;
use Mobile\Project\Entity\Platform;
use Mobile\Project\Entity\Workpath;

/**
 * Common datas for common DTO files.
 */
final class CommonDataDto
{
    /** @var StateCode[] */
    public array $stateCodes = [];

    /** @var DesktopUser[] */
    public array $desktopUsers = [];

    public ?string $creatorLogin = null;

    /** @var NatureZHE[] */
    public array $naturesZhe = [];

    public ?Platform $platform = null;

    /** @var Variable[] */
    public array $connectedVariables = [];

    public ?DataEntryProject $dataEntryProject = null;

    public ?GraphicalStructure $graphicalStructure = null;

    /** @var Workpath[] */
    public array $paths = [];

    /** @var DataEntryWithMaterialsAndVariablesDto[] */
    public array $dataEntries = [];
}
