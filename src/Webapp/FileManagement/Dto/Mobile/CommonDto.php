<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Mobile;

use Mobile\Project\Entity\DataEntryProject;

abstract class CommonDto
{
    public ?DataEntryProject $dataEntryProject;

    public function __construct(?DataEntryProject $dataEntryProject = null)
    {
        $this->dataEntryProject = $dataEntryProject;
    }
}
