<?php

namespace Webapp\FileManagement\Dto\Mobile;

class XmlWithFiles
{
    private string $xml;

    /**
     * @var array<string, string>
     */
    private array $files;

    public function __construct(string $xml, array $files)
    {
        $this->xml = $xml;
        $this->files = $files;
    }

    public function getXml(): string
    {
        return $this->xml;
    }

    /**
     * @return array<string, string>
     */
    public function getFiles(): array
    {
        return $this->files;
    }
}
