<?php

namespace Webapp\FileManagement\Dto\Mobile;

use Doctrine\Common\Collections\Collection;
use Mobile\Measure\Entity\Annotation;
use Shared\Authentication\Entity\IdentifiedEntity;

class AnnotationDto
{
    public Annotation $annotation;
    public IdentifiedEntity $businessObject;

    /**
     * @param Collection<int,  Annotation> $annotations
     *
     * @return self[]
     */
    public static function fromCollection(Collection $annotations, IdentifiedEntity $businessObject): array
    {
        return array_map(fn (Annotation $annotation) => new self($annotation, $businessObject), $annotations->getValues());
    }

    public function __construct(Annotation $annotation, IdentifiedEntity $businessObject)
    {
        $this->annotation = $annotation;
        $this->businessObject = $businessObject;
    }
}
