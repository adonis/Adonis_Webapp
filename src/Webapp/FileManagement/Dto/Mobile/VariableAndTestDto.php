<?php

namespace Webapp\FileManagement\Dto\Mobile;

use Mobile\Measure\Entity\Variable\Base\Variable;

class VariableAndTestDto
{
    public ?Variable $variable = null;

    public array $testDtos = [];
}
