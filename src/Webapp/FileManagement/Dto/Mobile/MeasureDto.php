<?php

namespace Webapp\FileManagement\Dto\Mobile;

use Mobile\Measure\Entity\Measure;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\StateCode;
use Shared\Authentication\Entity\IdentifiedEntity;

class MeasureDto
{
    public Measure $measure;
    public ?StateCode $codeEtat = null;
    public ?string $valeur = null;
    public ?int $indiceGeneratrice = null;
    public ?string $mesureGeneratrice = null;
    public ?string $mesuresGenerees = null;
    public ?Variable $variable = null;
    public ?IdentifiedEntity $objetMetier = null;

    public function __construct(Measure $measure)
    {
        $this->measure = $measure;
    }
}
