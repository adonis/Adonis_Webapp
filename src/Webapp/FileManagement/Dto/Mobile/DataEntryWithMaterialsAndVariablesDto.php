<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\FileManagement\Dto\Mobile;

use Mobile\Measure\Entity\DataEntry;
use Mobile\Measure\Entity\Variable\Base\Variable;
use Mobile\Measure\Entity\Variable\Material;

class DataEntryWithMaterialsAndVariablesDto
{
    public ?DataEntry $dataEntry = null;
    /** @var Material[] */
    public array $materials = [];
    /** @var Variable[] */
    public array $variables = [];
}
