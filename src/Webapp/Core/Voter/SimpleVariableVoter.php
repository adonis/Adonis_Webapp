<?php

namespace Webapp\Core\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Service\TestRepository;

class SimpleVariableVoter extends Voter
{
    public const SIMPLE_VARIABLE_DELETE = 'SIMPLE_VARIABLE_DELETE';

    private TestRepository $testRepository;

    public function __construct(
        TestRepository $testRepository
    )
    {
        $this->testRepository = $testRepository;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var SimpleVariable $simpleVariable */
        $simpleVariable = $subject;

        if (self::SIMPLE_VARIABLE_DELETE === $attribute) {
            if ($this->testRepository->isVariableInUse($simpleVariable)) {
                throw new AccessDeniedException('simpleVariable.deleteOperation.testOrSaisieStillConnected');
            }
        }

        return true;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::SIMPLE_VARIABLE_DELETE]) && is_a($subject, SimpleVariable::class);
    }
}
