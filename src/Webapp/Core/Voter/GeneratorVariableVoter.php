<?php

namespace Webapp\Core\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Service\TestRepository;

class GeneratorVariableVoter extends Voter
{
    public const GENERATOR_VARIABLE_DELETE = 'GENERATOR_VARIABLE_DELETE';

    private TestRepository $testRepository;

    public function __construct(
        TestRepository $testRepository
    ) {
        $this->testRepository = $testRepository;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        /** @var GeneratorVariable $generatorVariable */
        $generatorVariable = $subject;

        if (self::GENERATOR_VARIABLE_DELETE === $attribute) {
            if ($this->testRepository->isVariableInUse($generatorVariable)) {
                throw new AccessDeniedException('generatorVariable.deleteOperation.testOrSaisieStillConnected');
            }
        }

        return true;
    }

    protected function supports(string $attribute, $subject): bool
    {
        return in_array($attribute, [self::GENERATOR_VARIABLE_DELETE]) && is_a($subject, GeneratorVariable::class);
    }
}
