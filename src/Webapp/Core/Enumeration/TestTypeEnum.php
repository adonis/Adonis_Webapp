<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class TestTypeEnum
 * @package Webapp\Core\Enumeration
 */
class TestTypeEnum extends BasicEnum
{
    public const INTERVAL = 'interval';
    public const GROWTH = 'growth';
    public const COMBINATION = 'combination';
    public const PRECONDITIONED = 'preconditioned';
}
