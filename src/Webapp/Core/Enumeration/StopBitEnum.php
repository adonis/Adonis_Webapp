<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class StopBitEnum
 * @package Webapp\Core\Enumeration
 */
class StopBitEnum extends BasicEnum
{
    public const ONE = '1';
    public const ONE_HALF = '1.5';
    public const TWO = '2';
}
