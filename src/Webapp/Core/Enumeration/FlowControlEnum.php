<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class FlowControlEnum
 * @package Webapp\Core\Enumeration
 */
class FlowControlEnum extends BasicEnum
{
    public const NONE = 'none';
    public const XONXOFF = 'xonxoff';
    public const RTSCTS = 'rtscts';
}
