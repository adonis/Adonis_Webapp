<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class PossibleStartPointEnum
 * @package Webapp\Core\Enumeration
 */
class PossibleStartPointEnum extends BasicEnum
{
    public const BOT_LFT_DTOP = 'bottom left top';
    public const BOT_LFT_DRGT = 'bottom left right';
    public const BOT_RGT_DTOP = 'bottom right top';
    public const BOT_RGT_DLFT = 'bottom right left';
    public const TOP_LFT_DBOT = 'top left bottom';
    public const TOP_LFT_DRGT = 'top left right';
    public const TOP_RGT_DBOT = 'top right bottom';
    public const TOP_RGT_DLFT = 'top right left';
}
