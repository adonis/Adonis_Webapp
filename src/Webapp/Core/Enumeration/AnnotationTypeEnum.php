<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class AnnotationTypeEnum
 * @package Webapp\Core\Enumeration
 */
class AnnotationTypeEnum extends BasicEnum
{
    const ANNOT_TYPE_TEXT = 0;
    const ANNOT_TYPE_IMAG = 1;
    const ANNOT_TYPE_SONG = 2;
}
