<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class ChangeTypeEnum
 * @package Webapp\Core\Enumeration
 */
class ChangeTypeEnum extends BasicEnum
{
    const DEAD = 'dead';
    const REPLANTED = 'replanted';
}
