<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class PathLevelEnum
 * @package Webapp\Core\Enumeration
 *
 * @psalm-type PathLevelEnumId = self::*
 */
class PathLevelEnum extends BasicEnum
{
    public const OEZ = 'oez';
    public const INDIVIDUAL = 'individual';
    public const UNIT_PLOT = 'unitPlot';
    public const SURFACIC_UNIT_PLOT = 'surfacicUnitPlot';
    public const SUB_BLOCK = 'subBlock';
    public const BLOCK = 'block';
    public const EXPERIMENT = 'experiment';
    public const PLATFORM = 'platform';
}
