<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class VariableFormatEnum
 * @package Webapp\Core\Enumeration
 *
 * @psalm-type VariableFormatEnumId = self::*
 */
class VariableFormatEnum extends BasicEnum
{
    const FREE = 'free';
    const CHARACTER_NUMBER = 'characterNumber';
    const DECIMAL_NUMBER = 'decimalNumber';
    const JJMMYYYY = 'jjmmyyyy';
    const QUANTIEM = 'quantieme';
}
