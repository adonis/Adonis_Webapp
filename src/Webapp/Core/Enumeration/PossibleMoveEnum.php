<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class PossibleMoveEnum
 * @package Webapp\Core\Enumeration
 */
class PossibleMoveEnum extends BasicEnum
{
    public const LIBRE = 'libre';
    public const GRAPHIQUE = 'graphique';
    public const ORDONNANCEUR = 'ordonnanceur';
    public const ALLER_SIMPLE = 'allerSimple';
    public const ALLER_RETOUR = 'allerRetour';
    public const SERPENTIN = 'serpentin';
    public const DEMI_SERPENTIN = 'demiSerpentin';
}
