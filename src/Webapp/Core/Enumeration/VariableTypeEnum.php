<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * @psalm-type VariableTypeEnumId = self::*
 */
class VariableTypeEnum extends BasicEnum
{
    public const REAL = 'real';
    public const ALPHANUMERIC = 'alphanumeric';
    public const BOOLEAN = 'boolean';
    public const INTEGER = 'integer';
    public const DATE = 'date';
    public const HOUR = 'time';
}
