<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class PropagationEnum
 * @package Webapp\Core\Enumeration
 */
class SpreadingEnum extends BasicEnum
{
    const INDIVIDUAL = 'individual';
    const UNIT_PLOT = 'unit_plot';
}
