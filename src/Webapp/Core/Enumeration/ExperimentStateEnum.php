<?php

declare(strict_types=1);

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * @psalm-type ExperimentStateId = self::*
 */
class ExperimentStateEnum extends BasicEnum
{
    public const CREATED = 0;
    public const VALIDATED = 1;
    public const LOCKED = 2;
    public const NON_UNLOCKABLE = 3;
}
