<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class CombinationOperationEnum
 * @package Webapp\Core\Enumeration
 */
class CombinationOperationEnum extends BasicEnum
{
const ADIDITION = '+';
const SUBSTRACTION = '-';
const MULTIPLICATION = '*';
const DIVISION = '/';
}
