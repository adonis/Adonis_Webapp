<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class ParityEnum
 * @package Webapp\Core\Enumeration
 */
class ParityEnum extends BasicEnum
{
    public const NONE = 'none';
    public const EVEN = 'even';
    public const ODD = 'odd';
    public const MARK = 'mark';
    public const SPACE = 'space';
}
