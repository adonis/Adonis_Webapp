<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class CommunicationProtocolEnum
 * @package Webapp\Core\Enumeration
 */
class CommunicationProtocolEnum extends BasicEnum
{
    public const RS232 = 'rs232';
    public const USB = 'usb';
    public const BLUETOOTH = 'bluetooth';
}
