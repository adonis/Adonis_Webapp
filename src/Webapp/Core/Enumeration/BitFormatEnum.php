<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class BitFormatEnum
 * @package Webapp\Core\Enumeration
 */
class BitFormatEnum extends BasicEnum
{
    public const EIGHT = 8;
    public const SEVEN = 7;
    public const SIX = 6;
    public const FIVE = 5;
}
