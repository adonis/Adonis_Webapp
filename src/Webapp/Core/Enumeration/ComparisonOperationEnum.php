<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class ComparisonOperationEnum
 * @package Webapp\Core\Enumeration
 */
class ComparisonOperationEnum extends BasicEnum
{
    const SUP = '>';
    const SUP_EQUAL = '>=';
    const EQUAL = '=';
    const INF_EQUAL = '<=';
    const INF = '<';
}
