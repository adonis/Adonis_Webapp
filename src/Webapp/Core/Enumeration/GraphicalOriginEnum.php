<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class GraphicalOriginEnum
 * @package Webapp\Core\Enumeration
 */
class GraphicalOriginEnum extends BasicEnum
{
    public const TOP_LEFT = 0;
    public const TOP_RIGHT = 1;
    public const BOTTOM_RIGHT = 2;
    public const BOTTOM_LEFT = 3;
}
