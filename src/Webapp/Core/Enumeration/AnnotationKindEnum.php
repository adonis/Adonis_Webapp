<?php

namespace Webapp\Core\Enumeration;

use Shared\Enumeration\BasicEnum;

/**
 * Class AnnotationKindEnum
 * @package Webapp\Core\Enumeration
 */
class AnnotationKindEnum extends BasicEnum
{
    public const TEXT = 0;
    public const PICTURE = 1;
    public const SOUND = 2;
}
