<?php

namespace Webapp\Core\Annotation;

/**
 * Class GraphicallyDeletable
 * @Annotation
 * @Target("CLASS")
 */
class GraphicallyDeletable
{
    /**
     *
     * @var string field name of the deleted field
     */
    public string $graphicallyDeletableField = 'graphicallyDeletedAt';

}
