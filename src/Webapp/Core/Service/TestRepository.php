<?php
namespace Webapp\Core\Service;

use Doctrine\ORM\EntityManagerInterface;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\Test;

class TestRepository
{
    private EntityManagerInterface $objectManager;

    public function __construct(
        EntityManagerInterface $objectManager
    )
    {
        $this->objectManager = $objectManager;
    }

    public function isVariableInUse($variable): bool {

        if (is_a($variable, GeneratorVariable::class)) {
            return $this->checkAttributes([
                'variableGenerator',
                'comparedVariable_generator',
                'combinedVariable1_generator',
                'combinedVariable2_generator',
                'comparedVariable1_generator',
                'comparedVariable2_generator',
            ], $variable);
        } else if (is_a($variable, SimpleVariable::class)) {
            return $this->checkAttributes([
                'variableSimple',
                'comparedVariable_simple', 
                'combinedVariable1_simple',
                'combinedVariable2_simple',
                'comparedVariable1_simple',
                'comparedVariable2_simple',
            ], $variable);
        } else if (is_a($variable, SemiAutomaticVariable::class)) {
            return $this->checkAttributes([
                'variableSemiAutomatic',
                'comparedVariable_semiAutomatic',
                'combinedVariable1_semiAutomatic',
                'combinedVariable2_semiAutomatic',
                'comparedVariable1_semiAutomatic',
                'comparedVariable2_semiAutomatic',
            ], $variable);
        }
        return false;
    }

    private function checkAttributes(array $fields, $variable): bool {
        $alias = 'test';

        $qb = $this->objectManager->createQueryBuilder();
        $qb
            ->from(Test::class, $alias)
            ->select($qb->expr()->count("$alias"));

        $conditions = [];
        foreach ($fields as $field) {
            $conditions[] = $qb->expr()->eq("$alias.$field", ':variable');
        }

        $qb
            ->where($qb->expr()->orX(...$conditions))
            ->setParameter('variable', $variable);

        return 0 < $qb->getQuery()->getSingleScalarResult();
    }
}
