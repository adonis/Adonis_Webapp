<?php

namespace Webapp\Core\Service;

use ApiPlatform\Core\Api\IriConverterInterface;
use Shared\Authentication\Entity\Site;
use Webapp\Core\Entity\Annotation;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Device;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\FieldGeneration;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\OezNature;
use Webapp\Core\Entity\OutExperimentationZone;
use Webapp\Core\Entity\PathBase;
use Webapp\Core\Entity\PathUserWorkflow;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\Project;
use Webapp\Core\Entity\ProjectData;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\RequiredAnnotation;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\Session;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Entity\StateCode;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\Test;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;
use Webapp\Core\Entity\ValueList;
use Webapp\Core\Entity\ValueListItem;
use Webapp\Core\Entity\VariableConnection;
use Webapp\Core\Entity\VariableScale;
use Webapp\Core\Entity\VariableScaleItem;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * Class CloneService.
 */
final class CloneService
{
    private IriConverterInterface $iriConverter;

    public function __construct(IriConverterInterface $iriConverter)
    {
        $this->iriConverter = $iriConverter;
    }

    /**
     * @param Site $site
     * @param string $newName
     * @return Site
     */
    public function cloneSite(Site $site, string $newName): Site
    {
        return (new Site())
            ->setLabel($newName);
    }

    /**
     * @param Project $project
     * @param string|null $newName
     * @param array $iriMap
     * @return Project
     */
    public function cloneProject(Project $project, string $newName = null, array &$iriMap = []): Project
    {
        $newName = $newName ?? $project->getName();

        $res = (new Project())
            ->setOwner($project->getOwner())
            ->setComment($project->getComment())
            ->setCreated($project->getCreated())
            ->setName($newName)
            ->setPathBase($this->clonePathBase($project->getPathBase()));

        foreach ($project->getGeneratorVariables() as $variable) {
            $resVariable = $this->cloneGeneratorVariable($variable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($variable)] = $resVariable;
            $res->addGeneratorVariable($resVariable);

        }
        foreach ($project->getSimpleVariables() as $variable) {
            $resVariable = $this->cloneSimpleVariable($variable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($variable)] = $resVariable;
            $res->addSimpleVariable($resVariable);
        }
        foreach ($project->getDevices() as $device) {
            $res->addDevice($this->cloneDevice($device, $iriMap));
        }


        foreach ($project->getSimpleVariables() as $variable) {
            foreach ($variable->getTests() as $test) {
                $iriMap[$this->iriConverter->getIriFromItem($variable)]->addTest($this->cloneTest($test, $iriMap));
            }
        }
        foreach ($project->getGeneratorVariables() as $variable) {
            foreach ($variable->getTests() as $test) {
                $iriMap[$this->iriConverter->getIriFromItem($variable)]->addTest($this->cloneTest($test, $iriMap));
            }
        }
        foreach ($project->getDevices() as $device) {
            foreach ($device->getManagedVariables() as $variable) {
                foreach ($variable->getTests() as $test) {
                    $iriMap[$this->iriConverter->getIriFromItem($variable)]->addTest($this->cloneTest($test, $iriMap));
                }
            }
        }


        foreach ($project->getRequiredAnnotations() as $requiredAnnotation) {
            $res->addRequiredAnnotation($this->cloneRequiredAnnotation($requiredAnnotation));
        }
        foreach ($project->getStateCodes() as $stateCode) {
            if (!isset($iriMap[$this->iriConverter->getIriFromItem($stateCode)])) {
                $resStateCode = $this->cloneStateCode($stateCode);
                $iriMap[$this->iriConverter->getIriFromItem($stateCode)] = $resStateCode;
            }
            $res->addStateCode($iriMap[$this->iriConverter->getIriFromItem($stateCode)]);
        }

        return $res;
    }


    public function cloneSimpleVariable(SimpleVariable $variable, &$iriMap = []): SimpleVariable
    {
        $res = (new SimpleVariable("", "", 0, "", false, ""))
            ->setType($variable->getType())
            ->setName($variable->getName())
            ->setCreated($variable->getCreated())
            ->setIdentifier($variable->getIdentifier())
            ->setOrder($variable->getOrder())
            ->setComment($variable->getComment())
            ->setDefaultTrueValue($variable->getDefaultTrueValue())
            ->setFormat($variable->getFormat())
            ->setFormatLength($variable->getFormatLength())
            ->setLastModified($variable->getLastModified())
            ->setMandatory($variable->isMandatory())
            ->setPathLevel($variable->getPathLevel())
            ->setRepetitions($variable->getRepetitions())
            ->setShortName($variable->getShortName())
            ->setUnit($variable->getUnit())
            ->setValueList($variable->getValueList() !== null ? $this->cloneValueList($variable->getValueList()) : null)
            ->setScale($variable->getScale() !== null ? $this->cloneScale($variable->getScale()) : null)
            ->setOpenSilexUri($variable->getOpenSilexUri())
            ->setOpenSilexInstance($variable->getOpenSilexInstance());

        foreach ($variable->getConnectedVariables() as $variableConnection) {
            $res->addVariableConnection($this->cloneVariableConnection($variableConnection, $iriMap));
        }
        return $res;
    }


    public function cloneDevice(Device $device, array &$iriMap = []): Device
    {
        $res = (new Device())
            ->setAlias($device->getAlias())
            ->setName($device->getName())
            ->setManufacturer($device->getManufacturer())
            ->setType($device->getType())
            ->setCommunicationProtocol($device->getCommunicationProtocol())
            ->setFrameLength($device->getFrameLength())
            ->setFrameStart($device->getFrameStart())
            ->setFrameEnd($device->getFrameEnd())
            ->setFrameCsv($device->getFrameCsv())
            ->setBaudrate($device->getBaudrate())
            ->setBitFormat($device->getBitFormat())
            ->setFlowControl($device->getFlowControl())
            ->setParity($device->getParity())
            ->setStopBit($device->getStopBit())
            ->setRemoteControl($device->getRemoteControl());
        foreach ($device->getManagedVariables() as $managedVariable) {
            $resVariable = $this->cloneSemiAutomaticVariable($managedVariable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($managedVariable)] = $resVariable;
            $res->addManagedVariable($resVariable);
        }

        return $res;
    }

    private function cloneSemiAutomaticVariable(SemiAutomaticVariable $variable, &$iriMap = []): SemiAutomaticVariable
    {
        $res = (new SemiAutomaticVariable("", "", 0, "", false, "", 0, 0))
            ->setType($variable->getType())
            ->setName($variable->getName())
            ->setCreated($variable->getCreated())
            ->setIdentifier($variable->getIdentifier())
            ->setStart($variable->getStart())
            ->setEnd($variable->getEnd())
            ->setOrder($variable->getOrder())
            ->setComment($variable->getComment())
            ->setDefaultTrueValue($variable->getDefaultTrueValue())
            ->setFormat($variable->getFormat())
            ->setFormatLength($variable->getFormatLength())
            ->setLastModified($variable->getLastModified())
            ->setMandatory($variable->isMandatory())
            ->setPathLevel($variable->getPathLevel())
            ->setRepetitions($variable->getRepetitions())
            ->setShortName($variable->getShortName())
            ->setUnit($variable->getUnit());
        foreach ($variable->getConnectedVariables() as $variableConnection) {
            $res->addVariableConnection($this->cloneVariableConnection($variableConnection, $iriMap));
        }
        return $res;
    }

    public function cloneGeneratorVariable(GeneratorVariable $variable, array &$iriMap = []): GeneratorVariable
    {
        $res = (new GeneratorVariable("", "", 0, "", false, "", false, false, false))
            ->setName($variable->getName())
            ->setCreated($variable->getCreated())
            ->setIdentifier($variable->getIdentifier())
            ->setNumeralIncrement($variable->isNumeralIncrement())
            ->setDirectCounting($variable->isDirectCounting())
            ->setGeneratedPrefix($variable->getGeneratedPrefix())
            ->setPathWayHorizontal($variable->isPathWayHorizontal())
            ->setOrder($variable->getOrder())
            ->setComment($variable->getComment())
            ->setDefaultTrueValue($variable->getDefaultTrueValue())
            ->setFormat($variable->getFormat())
            ->setFormatLength($variable->getFormatLength())
            ->setLastModified($variable->getLastModified())
            ->setMandatory($variable->isMandatory())
            ->setPathLevel($variable->getPathLevel())
            ->setRepetitions($variable->getRepetitions())
            ->setShortName($variable->getShortName())
            ->setUnit($variable->getUnit());
        foreach ($variable->getGeneratedGeneratorVariables() as $generatedGeneratorVariable) {
            $resVariable = $this->cloneGeneratorVariable($generatedGeneratorVariable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($generatedGeneratorVariable)] = $resVariable;
            $res->addGeneratedGeneratorVariable($resVariable);
        }
        foreach ($variable->getGeneratedSimpleVariables() as $generatedSimpleVariable) {
            $resVariable = $this->cloneSimpleVariable($generatedSimpleVariable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($generatedSimpleVariable)] = $resVariable;
            $res->addGeneratedSimpleVariable($resVariable);
        }
        foreach ($variable->getConnectedVariables() as $variableConnection) {
            $res->addVariableConnection($this->cloneVariableConnection($variableConnection, $iriMap));
        }
        return $res;
    }

    public function cloneStateCode(StateCode $stateCode): StateCode
    {
        return (new StateCode())
            ->setColor($stateCode->getColor())
            ->setCode($stateCode->getCode())
            ->setMeaning($stateCode->getMeaning())
            ->setPermanent($stateCode->isPermanent())
            ->setSpreading($stateCode->getSpreading())
            ->setTitle($stateCode->getTitle());
    }

    private function cloneRequiredAnnotation(RequiredAnnotation $requiredAnnotation): RequiredAnnotation
    {
        return (new RequiredAnnotation())
            ->setComment($requiredAnnotation->getComment())
            ->setType($requiredAnnotation->getType())
            ->setAskWhenEntering($requiredAnnotation->isAskWhenEntering())
            ->setLevel($requiredAnnotation->getLevel());
    }

    public function cloneValueList(?ValueList $valueList)
    {
        $res = (new ValueList())
            ->setName($valueList->getName());
        foreach ($valueList->getValues() as $value) {
            $res->addValue((new ValueListItem($value->getName())));
        }
        return $res;
    }

    private function cloneScale(?VariableScale $scale)
    {
        $res = (new VariableScale())
            ->setName($scale->getName())
            ->setOpen($scale->isOpen())
            ->setMinValue($scale->getMinValue())
            ->setMaxValue($scale->getMaxValue());
        foreach ($scale->getValues() as $value) {
            $res->addValue((new VariableScaleItem($value->getValue()))
                ->setPic($value->getPic())
                ->setText($value->getText()));
        }
        return $res;
    }

    private function cloneVariableConnection(VariableConnection $variableConnection, &$iriMap = [])
    {
        $variable = $variableConnection->getDataEntryVariable();
        $iriMap[$this->iriConverter->getIriFromItem($variable)] = $variable;
        return (new VariableConnection())
            ->setDataEntryVariable($variable);
    }

    private function clonePathBase(?PathBase $pathBase): ?PathBase
    {
        if ($pathBase === null) {
            return null;
        }
        $res = (new PathBase())
            ->setName($pathBase->getName())
            ->setAskWhenEntering($pathBase->isAskWhenEntering())
            ->setOrderedIris($pathBase->getOrderedIris())
            ->setSelectedIris($pathBase->getSelectedIris());
        foreach ($pathBase->getUserPaths() as $userPath) {
            $res->addPathUserWorkflow($this->cloneUserPath($userPath));
        }
        return $res;
    }

    private function cloneUserPath(PathUserWorkflow $userPath): PathUserWorkflow
    {
        return (new PathUserWorkflow())
            ->setUser($userPath->getUser())
            ->setWorkflow($userPath->getWorkflow());
    }

    public function cloneTest(Test $test, array $iriMap = []): Test
    {
        return (new Test())
            ->setType($test->getType())
            ->setAuthIntervalMin($test->getAuthIntervalMin())
            ->setProbIntervalMin($test->getProbIntervalMin())
            ->setProbIntervalMax($test->getProbIntervalMax())
            ->setAuthIntervalMax($test->getAuthIntervalMax())
            ->setComparedVariable($test->getComparedVariable() === null ?
                null :
                $iriMap[$this->iriConverter->getIriFromItem($test->getComparedVariable())])
            ->setMinGrowth($test->getMinGrowth())
            ->setMaxGrowth($test->getMaxGrowth())
            ->setCombinedVariable1($test->getCombinedVariable1() === null ?
                null :
                $iriMap[$this->iriConverter->getIriFromItem($test->getCombinedVariable1())])
            ->setCombinationOperation($test->getCombinationOperation())
            ->setCombinedVariable2($test->getCombinedVariable2() === null ?
                null :
                $iriMap[$this->iriConverter->getIriFromItem($test->getCombinedVariable2())])
            ->setMinLimit($test->getMinLimit())
            ->setMaxLimit($test->getMaxLimit())
            ->setCompareWithVariable($test->getCompareWithVariable())
            ->setComparedVariable1($test->getComparedVariable1() === null ?
                null :
                $iriMap[$this->iriConverter->getIriFromItem($test->getComparedVariable1())])
            ->setComparisonOperation($test->getComparisonOperation())
            ->setComparedVariable2($test->getComparedVariable2() === null ?
                null :
                $iriMap[$this->iriConverter->getIriFromItem($test->getComparedVariable2())])
            ->setComparedValue($test->getComparedValue())
            ->setAssignedValue($test->getAssignedValue());
    }

    public function cloneProtocol(Protocol $protocol, array &$iriMap = []): Protocol
    {
        $res = (new Protocol())
            ->setName($protocol->getName())
            ->setAim($protocol->getAim())
            ->setComment($protocol->getComment())
            ->setAlgorithm($protocol->getAlgorithm())
            ->setOwner($protocol->getOwner());

        foreach ($protocol->getFactors() as $factor) {
            $res->addFactors($this->cloneFactor($factor, $iriMap));
        }

        foreach ($protocol->getTreatments() as $treatment) {
            $resTreatment = $this->cloneTreatment($treatment, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($treatment)] = $resTreatment;
            $res->addTreatments($resTreatment);
        }

        return $res;
    }

    public function cloneFactor(Factor $factor, &$iriMap = []): Factor
    {
        $res = (new Factor())
            ->setName($factor->getName())
            ->setOrder($factor->getOrder());
        foreach ($factor->getModalities() as $modality) {
            $resModality = $this->cloneModality($modality, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($modality)] = $resModality;
            $res->addModality($resModality);
        }
        return $res;
    }


    public function cloneModality(Modality $modality, array &$iriMap = []): Modality
    {
        return (new Modality())->setValue($modality->getValue());
    }

    public function cloneTreatment(Treatment $treatment, array &$iriMap = []): Treatment
    {
        $res = (new Treatment())
            ->setName($treatment->getName())
            ->setShortName($treatment->getShortName())
            ->setRepetitions($treatment->getRepetitions());
        foreach ($treatment->getModalities() as $modality) {
            $res->addModalities($iriMap[$this->iriConverter->getIriFromItem($modality)]);
        }

        return $res;
    }

    public function clonePlatform(Platform $platform, &$iriMap = []): Platform
    {
        $res = (new Platform())
            ->setName($platform->getName())
            ->setOwner($platform->getOwner())
            ->setPlaceName($platform->getPlaceName())
            ->setComment($platform->getComment())
            ->setOrigin($platform->getOrigin())
            ->setXMesh($platform->getXMesh())
            ->setYMesh($platform->getYMesh())
            ->setColor($platform->getColor())
            ->setSiteName($platform->getSiteName());

        foreach ($platform->getExperiments() as $experiment) {
            $resExperiment = $this->cloneExperiment($experiment, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($experiment)] = $resExperiment;
            $res->addExperiment($resExperiment);
        }

        return $res;
    }

    public function cloneExperiment(Experiment $experiment, array &$iriMap = []): Experiment
    {
        $res = (new Experiment())
            ->setOwner($experiment->getOwner())
            ->setName($experiment->getName())
            ->setState($experiment->getState())
            ->setColor($experiment->getColor())
            ->setComment($experiment->getComment())
            ->setValidated($experiment->getValidated())
            ->setIndividualUP($experiment->isIndividualUP());

        $res->setProtocol($this->cloneProtocol($experiment->getProtocol(), $iriMap));

        foreach ($experiment->getBlocks() as $block) {
            $resBlock = $this->cloneBlock($block, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($block)] = $resBlock;
            $res->addBlocks($resBlock);
        }

        foreach ($experiment->getOutExperimentationZones() as $oez) {
            $res->addOutExperimentationZone($this->cloneOutExperimentationZone($oez, $iriMap));
        }

        return $res;
    }

    public function cloneOEZNature(OezNature $oezNature, array &$iriMap = []): OezNature
    {
        return (new OezNature())
            ->setNature($oezNature->getNature())
            ->setColor($oezNature->getColor())
            ->setTexture($oezNature->getTexture());
    }

    public function cloneOutExperimentationZone(OutExperimentationZone $outExperimentationZone, array &$iriMap, array &$maxNumbers = null): OutExperimentationZone
    {
        return (new OutExperimentationZone())
            ->setX($outExperimentationZone->getX())
            ->setY($outExperimentationZone->getY())
            ->setColor($outExperimentationZone->getColor())
            ->setNumber($maxNumbers !== null ? ++$maxNumbers[PathLevelEnum::OEZ] : $outExperimentationZone->getNumber())
            ->setNature($iriMap[$this->iriConverter->getIriFromItem($outExperimentationZone->getNature())]);
    }

    public function cloneBlock(Block $block, array &$iriMap, array &$maxNumbers = null): Block
    {
        $res = (new Block())
            ->setComment($block->getComment())
            ->setColor($block->getColor())
            ->setNumber($maxNumbers !== null ? ++$maxNumbers[PathLevelEnum::BLOCK] : $block->getNumber());

        foreach ($block->getSubBlocks() as $subBlock) {
            $res->addSubBlocks($this->cloneSubBloc($subBlock, $iriMap, $maxNumbers));
        }

        foreach ($block->getOutExperimentationZones() as $oez) {
            $res->addOutExperimentationZone($this->cloneOutExperimentationZone($oez, $iriMap, $maxNumbers));
        }
        foreach ($block->getSurfacicUnitPlots() as $unitPlot) {
            $resUnitPlot = $this->cloneSurfacicUnitPlot($unitPlot, $iriMap, $maxNumbers);
            $iriMap[$this->iriConverter->getIriFromItem($unitPlot)] = $resUnitPlot;
            $res->addSurfacicUnitPlots($resUnitPlot);
        }
        foreach ($block->getUnitPlots() as $unitPlot) {
            $resUnitPlot = $this->cloneUnitPlot($unitPlot, $iriMap, $maxNumbers);
            $iriMap[$this->iriConverter->getIriFromItem($unitPlot)] = $resUnitPlot;
            $res->addUnitPlots($resUnitPlot);
        }

        return $res;
    }

    public function cloneSubBloc(SubBlock $subBlock, array &$iriMap = [], array &$maxNumbers = null): SubBlock
    {
        $res = (new SubBlock())
            ->setComment($subBlock->getComment())
            ->setColor($subBlock->getColor())
            ->setNumber($maxNumbers !== null ? ++$maxNumbers[PathLevelEnum::SUB_BLOCK] : $subBlock->getNumber());

        foreach ($subBlock->getOutExperimentationZones() as $oez) {
            $res->addOutExperimentationZone($this->cloneOutExperimentationZone($oez, $iriMap, $maxNumbers));
        }
        foreach ($subBlock->getSurfacicUnitPlots() as $unitPlot) {
            $resUnitPlot = $this->cloneSurfacicUnitPlot($unitPlot, $iriMap, $maxNumbers);
            $iriMap[$this->iriConverter->getIriFromItem($unitPlot)] = $resUnitPlot;
            $res->addSurfacicUnitPlots($resUnitPlot);
        }
        foreach ($subBlock->getUnitPlots() as $unitPlot) {
            $resUnitPlot = $this->cloneUnitPlot($unitPlot, $iriMap, $maxNumbers);
            $iriMap[$this->iriConverter->getIriFromItem($unitPlot)] = $resUnitPlot;
            $res->addUnitPlots($resUnitPlot);
        }


        return $res;
    }

    public function cloneUnitPlot(UnitPlot $unitPlot, array &$iriMap = [], array &$maxNumbers = null): UnitPlot
    {
        $res = (new UnitPlot())
            ->setNumber($maxNumbers !== null ? ++$maxNumbers[PathLevelEnum::UNIT_PLOT] : $unitPlot->getNumber())
            ->setComment($unitPlot->getComment())
            ->setColor($unitPlot->getColor())
            ->setTreatment($iriMap[$this->iriConverter->getIriFromItem($unitPlot->getTreatment())]);

        foreach ($unitPlot->getIndividuals() as $individual) {
            $resIndividual = $this->cloneIndividual($individual, $maxNumbers);
            $iriMap[$this->iriConverter->getIriFromItem($individual)] = $resIndividual;
            $res->addIndividual($resIndividual);
        }

        foreach ($unitPlot->getOutExperimentationZones() as $oez) {
            $res->addOutExperimentationZone($this->cloneOutExperimentationZone($oez, $iriMap, $maxNumbers));
        }
        return $res;
    }

    public function cloneSurfacicUnitPlot(SurfacicUnitPlot $unitPlot, array &$iriMap = [], array &$maxNumbers = null): SurfacicUnitPlot
    {
        return (new SurfacicUnitPlot())
            ->setNumber($maxNumbers !== null ? ++$maxNumbers[PathLevelEnum::SURFACIC_UNIT_PLOT] : $unitPlot->getNumber())
            ->setX($unitPlot->getX())
            ->setY($unitPlot->getY())
            ->setComment($unitPlot->getComment())
            ->setColor($unitPlot->getColor())
            ->setHeight($unitPlot->getHeight())
            ->setLatitude($unitPlot->getLatitude())
            ->setLongitude($unitPlot->getLongitude())
            ->setIdentifier($unitPlot->getIdentifier())
            ->setTreatment($iriMap[$this->iriConverter->getIriFromItem($unitPlot->getTreatment())]);
    }

    public function cloneIndividual(Individual $individual, array &$maxNumbers = null): Individual
    {
        return (new Individual())
            ->setX($individual->getX())
            ->setY($individual->getY())
            ->setNumber($maxNumbers !== null ? ++$maxNumbers[PathLevelEnum::INDIVIDUAL] : $individual->getNumber())
            ->setComment($individual->getComment())
            ->setColor($individual->getColor())
            ->setHeight($individual->getHeight())
            ->setLatitude($individual->getLatitude())
            ->setLongitude($individual->getLongitude())
            ->setIdentifier($individual->getIdentifier());
    }

    public function cloneProjectData(ProjectData $projectData, array &$iriMap = []): ProjectData
    {
        $res = (new ProjectData())
            ->setUser($projectData->getUser())
            ->setComment($projectData->getComment())
            ->setName($projectData->getName())
            ->setFusion($projectData->isFusion());
        foreach ($projectData->getSimpleVariables() as $variable) {
            $resSimpleVariable = $this->cloneSimpleVariable($variable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($variable)] = $resSimpleVariable;
            $res->addSimpleVariable($resSimpleVariable);
        }
        foreach ($projectData->getGeneratorVariables() as $variable) {
            $resGeneratorVariable = $this->cloneGeneratorVariable($variable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($variable)] = $resGeneratorVariable;
            $res->addGeneratorVariable($resGeneratorVariable);
        }
        foreach ($projectData->getSemiAutomaticVariables() as $variable) {
            $resSemiAutoVar = $this->cloneSemiAutomaticVariable($variable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($variable)] = $resSemiAutoVar;
            $res->addSemiAutomaticVariable($resSemiAutoVar);
        }
        foreach ($projectData->getSessions() as $session) {
            $res->addSession($this->cloneSession($session, $iriMap));
        }

        return $res;
    }

    private function cloneSession(Session $session, array &$iriMap = []): Session
    {
        $res = (new Session())
            ->setComment($session->getComment())
            ->setUser($session->getUser())
            ->setStartedAt($session->getStartedAt())
            ->setEndedAt($session->getEndedAt());
        foreach ($session->getFieldMeasures() as $fieldMeasure) {
            $res->addFieldMeasure($this->cloneFieldMeasure($fieldMeasure, $iriMap));
        }
        foreach ($session->getAnnotations() as $annotation) {
            $res->addAnnotation($this->cloneAnnotation($annotation, $iriMap));
        }
        return $res;
    }

    private function cloneAnnotation(Annotation $annotation, array &$iriMap = []): Annotation
    {
        return (new Annotation())
            ->setName($annotation->getName())
            ->setType($annotation->getType())
            ->setImage($annotation->getImage())
            ->setCategories($annotation->getCategories())
            ->setKeywords($annotation->getKeywords())
            ->setTarget($iriMap[$this->iriConverter->getIriFromItem($annotation->getTarget())])
            ->setTimestamp($annotation->getTimestamp())
            ->setValue($annotation->getValue());
    }

    private function cloneFieldMeasure(FieldMeasure $fieldMeasure, array &$iriMap): FieldMeasure
    {
        $res = (new FieldMeasure())
            ->setTarget($iriMap[$this->iriConverter->getIriFromItem($fieldMeasure->getTarget())])
            ->setVariable($iriMap[$this->iriConverter->getIriFromItem($fieldMeasure->getVariable())]);
        foreach ($fieldMeasure->getMeasures() as $measure) {
            $resMeasure = $this->cloneMeasure($measure, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($measure)] = $resMeasure;
            $res->addMeasure($resMeasure);
        }
        foreach ($fieldMeasure->getFieldGenerations() as $fieldGeneration) {
            $res->addFieldGeneration($this->cloneFieldGeneration($fieldGeneration, $iriMap));
        }
        return $res;
    }

    private function cloneMeasure(Measure $measure, array &$iriMap): Measure
    {

        if ($measure->getState() !== null && !isset($iriMap[$this->iriConverter->getIriFromItem($measure->getState())])) {
            $resStateCode = $this->cloneStateCode($measure->getState());
            $iriMap[$this->iriConverter->getIriFromItem($measure->getState())] = $resStateCode;
        }
        return (new Measure())
            ->setTimestamp($measure->getTimestamp())
            ->setValue($measure->getValue())
            ->setRepetition($measure->getRepetition())
            ->setState($measure->getState() !== null ? $iriMap[$this->iriConverter->getIriFromItem($measure->getState())] : null);
    }

    private function cloneFieldGeneration(FieldGeneration $fieldGeneration, array &$iriMap): FieldGeneration
    {
        $res = (new FieldGeneration())
            ->setNumeralIncrement($fieldGeneration->isNumeralIncrement())
            ->setPrefix($fieldGeneration->getPrefix())
            ->setIndex($fieldGeneration->getIndex());
        foreach ($fieldGeneration->getChildren() as $child) {
            $res->addChild($this->cloneFieldMeasure($child, $iriMap));
        }
        return $res;
    }

}
