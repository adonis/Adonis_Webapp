<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Enumeration\Annotation\EnumType;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Dto\Test\TestInputDto;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER') && object.getVariable().getProject() !== null",
 *              "input"=TestInputDto::class,
 *          },
 *     },
 *     itemOperations={
 *          "get"={},
 *          "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getVariable().getSite())"
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getVariable().getSite())"
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"variable": "exact"})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="test", schema="webapp")
 */
class Test extends IdentifiedEntity
{
    /**
     * @EnumType(class="Webapp\Core\Enumeration\TestTypeEnum")
     *
     * @UniqueAttributeInParent(parentsAttributes={"variable.tests"})
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"project_explorer_view", "project_synthesis", "variable_synthesis"})
     */
    private string $type = '';

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable", inversedBy="tests")
     */
    private ?SimpleVariable $variableSimple = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable", inversedBy="tests")
     */
    private ?SemiAutomaticVariable $variableSemiAutomatic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable", inversedBy="tests")
     */
    private ?GeneratorVariable $variableGenerator = null;

    /* ----------------------------------------- */

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $authIntervalMin = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $probIntervalMin = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $probIntervalMax = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $authIntervalMax = null;

    /* ----------------------------------------- */

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable")
     */
    private ?SimpleVariable $comparedVariable_simple = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable")
     */
    private ?SemiAutomaticVariable $comparedVariable_semiAutomatic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable")
     */
    private ?GeneratorVariable $comparedVariable_generator = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $minGrowth = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $maxGrowth = null;

    /* ----------------------------------------- */

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable")
     */
    private ?SimpleVariable $combinedVariable1_simple = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable")
     */
    private ?SemiAutomaticVariable $combinedVariable1_semiAutomatic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable")
     */
    private ?GeneratorVariable $combinedVariable1_generator = null;

    /**
     * @EnumType(class="Webapp\Core\Enumeration\CombinationOperationEnum", nullable=true)
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?string $combinationOperation = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable")
     */
    private ?SimpleVariable $combinedVariable2_simple = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable")
     */
    private ?SemiAutomaticVariable $combinedVariable2_semiAutomatic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable")
     */
    private ?GeneratorVariable $combinedVariable2_generator = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $minLimit = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $maxLimit = null;

    /* ----------------------------------------- */

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?bool $compareWithVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable")
     */
    private ?SimpleVariable $comparedVariable1_simple = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable")
     */
    private ?SemiAutomaticVariable $comparedVariable1_semiAutomatic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable")
     */
    private ?GeneratorVariable $comparedVariable1_generator = null;

    /**
     * @EnumType(class="Webapp\Core\Enumeration\ComparisonOperationEnum", nullable=true)
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?string $comparisonOperation = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable")
     */
    private ?SimpleVariable $comparedVariable2_simple = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable")
     */
    private ?SemiAutomaticVariable $comparedVariable2_semiAutomatic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable")
     */
    private ?GeneratorVariable $comparedVariable2_generator = null;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?float $comparedValue = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"project_synthesis"})
     */
    private ?string $assignedValue = null;

    /**
     * @psalm-mutation-free
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): ?AbstractVariable
    {
        return $this->variableSimple ?? $this->variableGenerator ?? $this->variableSemiAutomatic;
    }

    /**
     * @return $this
     */
    public function setVariable(?AbstractVariable $variable): self
    {
        if ($variable instanceof SimpleVariable) {
            $this->variableSimple = $variable;
        } elseif ($variable instanceof GeneratorVariable) {
            $this->variableGenerator = $variable;
        } elseif ($variable instanceof SemiAutomaticVariable) {
            $this->variableSemiAutomatic = $variable;
        } elseif (null === $variable) {
            $this->variableSimple = null;
            $this->variableGenerator = null;
            $this->variableSemiAutomatic = null;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAuthIntervalMin(): ?float
    {
        return $this->authIntervalMin;
    }

    /**
     * @return $this
     */
    public function setAuthIntervalMin(?float $authIntervalMin): self
    {
        $this->authIntervalMin = $authIntervalMin;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProbIntervalMin(): ?float
    {
        return $this->probIntervalMin;
    }

    /**
     * @return $this
     */
    public function setProbIntervalMin(?float $probIntervalMin): self
    {
        $this->probIntervalMin = $probIntervalMin;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProbIntervalMax(): ?float
    {
        return $this->probIntervalMax;
    }

    /**
     * @return $this
     */
    public function setProbIntervalMax(?float $probIntervalMax): self
    {
        $this->probIntervalMax = $probIntervalMax;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAuthIntervalMax(): ?float
    {
        return $this->authIntervalMax;
    }

    /**
     * @return $this
     */
    public function setAuthIntervalMax(?float $authIntervalMax): self
    {
        $this->authIntervalMax = $authIntervalMax;

        return $this;
    }

    /**
     * @Groups({"project_synthesis"})
     *
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     *
     * @psalm-mutation-free
     */
    public function getComparedVariable(): ?AbstractVariable
    {
        if (null !== $this->comparedVariable_simple) {
            return $this->comparedVariable_simple;
        } elseif (null !== $this->comparedVariable_generator) {
            return $this->comparedVariable_generator;
        } else {
            return $this->comparedVariable_semiAutomatic;
        }
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $comparedVariable
     *
     * @return $this
     */
    public function setComparedVariable(?AbstractVariable $comparedVariable): self
    {
        $this->comparedVariable_simple = null;
        $this->comparedVariable_generator = null;
        $this->comparedVariable_semiAutomatic = null;
        if ($comparedVariable instanceof SimpleVariable) {
            $this->comparedVariable_simple = $comparedVariable;
        } elseif ($comparedVariable instanceof GeneratorVariable) {
            $this->comparedVariable_generator = $comparedVariable;
        } elseif ($comparedVariable instanceof SemiAutomaticVariable) {
            $this->comparedVariable_semiAutomatic = $comparedVariable;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMinGrowth(): ?float
    {
        return $this->minGrowth;
    }

    /**
     * @return $this
     */
    public function setMinGrowth(?float $minGrowth): self
    {
        $this->minGrowth = $minGrowth;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaxGrowth(): ?float
    {
        return $this->maxGrowth;
    }

    /**
     * @return $this
     */
    public function setMaxGrowth(?float $maxGrowth): self
    {
        $this->maxGrowth = $maxGrowth;

        return $this;
    }

    /**
     * @Groups({"project_synthesis"})
     *
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     *
     * @psalm-mutation-free
     */
    public function getCombinedVariable1(): ?AbstractVariable
    {
        if (null !== $this->combinedVariable1_simple) {
            return $this->combinedVariable1_simple;
        } elseif (null !== $this->combinedVariable1_generator) {
            return $this->combinedVariable1_generator;
        } else {
            return $this->combinedVariable1_semiAutomatic;
        }
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $combinedVariable1
     */
    public function setCombinedVariable1(?AbstractVariable $combinedVariable1): self
    {
        $this->combinedVariable1_simple = null;
        $this->combinedVariable1_generator = null;
        $this->combinedVariable1_semiAutomatic = null;
        if ($combinedVariable1 instanceof SimpleVariable) {
            $this->combinedVariable1_simple = $combinedVariable1;
        } elseif ($combinedVariable1 instanceof GeneratorVariable) {
            $this->combinedVariable1_generator = $combinedVariable1;
        } elseif ($combinedVariable1 instanceof SemiAutomaticVariable) {
            $this->combinedVariable1_semiAutomatic = $combinedVariable1;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCombinationOperation(): ?string
    {
        return $this->combinationOperation;
    }

    /**
     * @return $this
     */
    public function setCombinationOperation(?string $combinationOperation): self
    {
        $this->combinationOperation = $combinationOperation;

        return $this;
    }

    /**
     * @Groups({"project_synthesis"})
     *
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     */
    public function getCombinedVariable2(): ?AbstractVariable
    {
        if (null !== $this->combinedVariable2_simple) {
            return $this->combinedVariable2_simple;
        } elseif (null !== $this->combinedVariable2_generator) {
            return $this->combinedVariable2_generator;
        } else {
            return $this->combinedVariable2_semiAutomatic;
        }
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $combinedVariable2
     *
     * @return $this
     */
    public function setCombinedVariable2(?AbstractVariable $combinedVariable2): self
    {
        $this->combinedVariable2_simple = null;
        $this->combinedVariable2_generator = null;
        $this->combinedVariable2_semiAutomatic = null;
        if ($combinedVariable2 instanceof SimpleVariable) {
            $this->combinedVariable2_simple = $combinedVariable2;
        } elseif ($combinedVariable2 instanceof GeneratorVariable) {
            $this->combinedVariable2_generator = $combinedVariable2;
        } elseif ($combinedVariable2 instanceof SemiAutomaticVariable) {
            $this->combinedVariable2_semiAutomatic = $combinedVariable2;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMinLimit(): ?float
    {
        return $this->minLimit;
    }

    /**
     * @return $this
     */
    public function setMinLimit(?float $minLimit): self
    {
        $this->minLimit = $minLimit;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaxLimit(): ?float
    {
        return $this->maxLimit;
    }

    /**
     * @return $this
     */
    public function setMaxLimit(?float $maxLimit): self
    {
        $this->maxLimit = $maxLimit;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCompareWithVariable(): ?bool
    {
        return $this->compareWithVariable;
    }

    /**
     * @return $this
     */
    public function setCompareWithVariable(?bool $compareWithVariable): self
    {
        $this->compareWithVariable = $compareWithVariable;

        return $this;
    }

    /**
     * @Groups({"project_synthesis"})
     *
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     */
    public function getComparedVariable1(): ?AbstractVariable
    {
        if (null !== $this->comparedVariable1_simple) {
            return $this->comparedVariable1_simple;
        } elseif (null !== $this->comparedVariable1_generator) {
            return $this->comparedVariable1_generator;
        } else {
            return $this->comparedVariable1_semiAutomatic;
        }
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $comparedVariable1
     *
     * @return $this
     */
    public function setComparedVariable1(?AbstractVariable $comparedVariable1): self
    {
        $this->comparedVariable1_simple = null;
        $this->comparedVariable1_generator = null;
        $this->comparedVariable1_semiAutomatic = null;
        if ($comparedVariable1 instanceof SimpleVariable) {
            $this->comparedVariable1_simple = $comparedVariable1;
        } elseif ($comparedVariable1 instanceof GeneratorVariable) {
            $this->comparedVariable1_generator = $comparedVariable1;
        } elseif ($comparedVariable1 instanceof SemiAutomaticVariable) {
            $this->comparedVariable1_semiAutomatic = $comparedVariable1;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComparisonOperation(): ?string
    {
        return $this->comparisonOperation;
    }

    /**
     * @return $this
     */
    public function setComparisonOperation(?string $comparisonOperation): self
    {
        $this->comparisonOperation = $comparisonOperation;

        return $this;
    }

    /**
     * @Groups({"project_synthesis"})
     *
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     */
    public function getComparedVariable2(): ?AbstractVariable
    {
        if (null !== $this->comparedVariable2_simple) {
            return $this->comparedVariable2_simple;
        } elseif (null !== $this->comparedVariable2_generator) {
            return $this->comparedVariable2_generator;
        } else {
            return $this->comparedVariable2_semiAutomatic;
        }
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $comparedVariable2
     *
     * @return $this
     */
    public function setComparedVariable2(?AbstractVariable $comparedVariable2): self
    {
        $this->comparedVariable2_simple = null;
        $this->comparedVariable2_generator = null;
        $this->comparedVariable2_semiAutomatic = null;
        if ($comparedVariable2 instanceof SimpleVariable) {
            $this->comparedVariable2_simple = $comparedVariable2;
        } elseif ($comparedVariable2 instanceof GeneratorVariable) {
            $this->comparedVariable2_generator = $comparedVariable2;
        } elseif ($comparedVariable2 instanceof SemiAutomaticVariable) {
            $this->comparedVariable2_semiAutomatic = $comparedVariable2;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComparedValue(): ?float
    {
        return $this->comparedValue;
    }

    /**
     * @return $this
     */
    public function setComparedValue(?float $comparedValue): self
    {
        $this->comparedValue = $comparedValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAssignedValue(): ?string
    {
        return $this->assignedValue;
    }

    /**
     * @return $this
     */
    public function setAssignedValue(?string $assignedValue): self
    {
        $this->assignedValue = $assignedValue;

        return $this;
    }
}
