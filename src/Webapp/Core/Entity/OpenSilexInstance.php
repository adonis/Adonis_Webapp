<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\RightManagement\Annotation\AdvancedRight;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_SITE_ADMIN', object.getSite())",
 *          },
 *     },
 *     itemOperations={
 *          "get"={},
 *          "patch"={
 *              "security"="is_granted('ROLE_SITE_ADMIN', object.getSite())",
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_SITE_ADMIN', object.getSite())"
 *          },
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"site": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"admin_explorer_view"}})
 *
 * @AdvancedRight(siteAttribute="site")
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="opensilex_instance", schema="webapp")
 */
class OpenSilexInstance extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="openSilexInstances")
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"admin_explorer_view"})
     */
    private Site $site;

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @Groups({"admin_explorer_view"})
     */
    private string $url = '';

    /**
     * @psalm-mutation-free
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return $this
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
