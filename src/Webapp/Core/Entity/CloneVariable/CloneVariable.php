<?php

namespace Webapp\Core\Entity\CloneVariable;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\CloneVariableToProjectOperation;
use Webapp\Core\Entity\Project;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "controller"=CloneVariableToProjectOperation::class,
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Clone a variable in a project",
 *                  "description": "Create a copy of the given variable inside a data entry project"
 *              },
 *          },
 *     },
 *     itemOperations={
 *     "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }}
 * )
 */
class CloneVariable
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    private Project $project;

    private string $variable = '';

    private string $result = '';

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): string
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(string $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @return $this
     */
    public function setResult(string $result): self
    {
        $this->result = $result;

        return $this;
    }
}
