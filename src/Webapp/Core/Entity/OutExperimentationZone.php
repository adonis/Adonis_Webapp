<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Annotation\GraphicallyDeletable;
use Webapp\Core\Traits\GraphicallyDeletableEntity;

/**
 * @ApiResource(
 *     normalizationContext={
 *         "groups"={"platform_full_view", "id_read"}
 *     },
 *     collectionOperations={
 *          "get"={}
 *     },
 *     itemOperations={
 *          "get"={},
 *          "patch"={
 *              "denormalization_context"={"groups"={"oez_edit"}}
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"experiment": "exact", "block": "exact", "subBlock": "exact", "unitPlot": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"design_explorer_view", "platform_full_view", "parent_view"}})
 *
 * @Gedmo\SoftDeleteable()
 *
 * @GraphicallyDeletable()
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="out_experimentation_zone", schema="webapp")
 */
class OutExperimentationZone extends IdentifiedEntity implements BusinessObject
{
    use GraphicallyDeletableEntity;

    use HasGeometryEntity;

    use OpenSilexEntity;

    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view"})
     */
    private string $number = '';

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view"})
     */
    private int $x = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view"})
     */
    private int $y = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view", "oez_edit"})
     */
    private ?string $comment = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\OezNature", inversedBy="outExperimentationZones")
     *
     * @ORM\JoinColumn(nullable=false)
     *
     * @Groups({"platform_full_view", "design_explorer_view"})
     */
    private OezNature $nature;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\UnitPlot", inversedBy="outExperimentationZones")
     *
     * @Groups({"webapp_data_view", "parent_view"})
     */
    private ?UnitPlot $unitPlot = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SubBlock", inversedBy="outExperimentationZones")
     *
     * @Groups({"webapp_data_view", "parent_view"})
     */
    private ?SubBlock $subBlock = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Block", inversedBy="outExperimentationZones")
     *
     * @Groups({"webapp_data_view", "parent_view"})
     */
    private ?Block $block = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Experiment", inversedBy="outExperimentationZones")
     *
     * @Groups({"webapp_data_view", "parent_view"})
     */
    private ?Experiment $experiment = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"platform_full_view"})
     */
    private ?int $color = null;

    /**
     * @Groups({"platform_full_view"})
     *
     * @psalm-mutation-free
     */
    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @psalm-mutation-free
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @return $this
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitPlot(): ?UnitPlot
    {
        return $this->unitPlot;
    }

    /**
     * @return $this
     */
    public function setUnitPlot(?UnitPlot $unitPlot): self
    {
        $this->unitPlot = $unitPlot;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlock(): ?SubBlock
    {
        return $this->subBlock;
    }

    /**
     * @return $this
     */
    public function setSubBlock(?SubBlock $subBlock): self
    {
        $this->subBlock = $subBlock;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlock(): ?Block
    {
        return $this->block;
    }

    /**
     * @return $this
     */
    public function setBlock(?Block $block): self
    {
        $this->block = $block;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperiment(): ?Experiment
    {
        return $this->experiment;
    }

    /**
     * @psalm-mutation-free
     */
    public function getNature(): OezNature
    {
        return $this->nature;
    }

    /**
     * @return $this
     */
    public function setNature(OezNature $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * @return $this
     */
    public function setExperiment(?Experiment $experiment): self
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?int
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?int $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @psalm-mutation-free
     *
     * @return UnitPlot|SubBlock|Block|Experiment
     */
    public function parent(): ?BusinessObject
    {
        return $this->unitPlot ?? $this->subBlock ?? $this->block ?? $this->experiment;
    }

    public function children(): array
    {
        return [];
    }
}
