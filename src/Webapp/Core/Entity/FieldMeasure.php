<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\CustomFilters\OrSearchFilter;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     },
 *     itemOperations={
 *         "get"={}
 *     }
 * )
 *
 * @ApiFilter(OrSearchFilter::class, properties={"search_variable"={"generatorVariable": "exact", "simpleVariable": "exact", "semiAutomaticVariable": "exact"}})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"graphical_measure_view"}})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="field_measure", schema="webapp")
 */
class FieldMeasure extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Session", inversedBy="fieldMeasures")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Session $session = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\FieldGeneration", inversedBy="children")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?FieldGeneration $fieldParent = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable", cascade={"persist"}, fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?GeneratorVariable $generatorVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable", cascade={"persist"}, fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?SimpleVariable $simpleVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable", cascade={"persist"}, fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?SemiAutomaticVariable $semiAutomaticVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Individual", fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Individual $individualTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\UnitPlot", fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?UnitPlot $unitPlotTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SurfacicUnitPlot", fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?SurfacicUnitPlot $surfacicUnitPlotTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Block", fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Block $blockTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SubBlock", fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?SubBlock $subBlockTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Experiment", fetch="EAGER")
     *
     * @ORM\JoinColumn(nullable=true)
     */
    private ?Experiment $experimentTarget = null;

    /**
     * @var Collection<int, Measure>
     *
     * @Groups({"webapp_data_view", "data_entry_synthesis", "variable_synthesis", "fusion_result", "data_view_item", "graphical_measure_view"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Measure", mappedBy="formField", cascade={"persist", "remove", "detach"}, fetch="EAGER")
     */
    private Collection $measures;

    /**
     * @var Collection<int, FieldGeneration>
     *
     * @Groups({"webapp_data_view", "data_view_item"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\FieldGeneration", mappedBy="formField", cascade={"persist", "remove", "detach"}, fetch="EAGER")
     */
    private Collection $fieldGenerations;

    public function __construct()
    {
        $this->measures = new ArrayCollection();
        $this->fieldGenerations = new ArrayCollection();
    }

    /**
     * @Groups({"webapp_data_view", "data_entry_synthesis", "fusion_result", "data_view_item", "graphical_measure_view"})
     *
     * @psalm-mutation-free
     */
    public function getVariable(): ?AbstractVariable
    {
        return $this->simpleVariable ?? $this->generatorVariable ?? $this->semiAutomaticVariable;
    }

    /**
     * @Groups({"webapp_data_view", "data_entry_synthesis", "variable_synthesis", "fusion_result", "graphical_measure_view"})
     *
     * @return Block|Experiment|Individual|SubBlock|UnitPlot|SurfacicUnitPlot|null
     *
     * @psalm-mutation-free
     */
    public function getTarget()
    {
        return $this->individualTarget ?? $this->unitPlotTarget ?? $this->subBlockTarget ?? $this->blockTarget ?? $this->experimentTarget ?? $this->surfacicUnitPlotTarget;
    }

    /**
     * @Groups({"webapp_data_view", "data_entry_synthesis", "fusion_result"})
     *
     * @psalm-mutation-free
     */
    public function getTargetType(): ?string
    {
        $target = $this->getTarget();
        if ($target instanceof Individual) {
            return PathLevelEnum::INDIVIDUAL;
        } elseif ($target instanceof UnitPlot) {
            return PathLevelEnum::UNIT_PLOT;
        } elseif ($target instanceof SubBlock) {
            return PathLevelEnum::SUB_BLOCK;
        } elseif ($target instanceof Block) {
            return PathLevelEnum::BLOCK;
        } elseif ($target instanceof Experiment) {
            return PathLevelEnum::EXPERIMENT;
        } elseif ($target instanceof SurfacicUnitPlot) {
            return PathLevelEnum::SURFACIC_UNIT_PLOT;
        } else {
            return null;
        }
    }

    /**
     * @param Block|Experiment|Individual|SubBlock|UnitPlot|SurfacicUnitPlot|null $target
     *
     * @return $this
     */
    public function setTarget($target): self
    {
        if ($target instanceof Individual) {
            $this->setIndividualTarget($target);
        } elseif ($target instanceof UnitPlot) {
            $this->setUnitPlotTarget($target);
        } elseif ($target instanceof SubBlock) {
            $this->setSubBlockTarget($target);
        } elseif ($target instanceof Block) {
            $this->setBlockTarget($target);
        } elseif ($target instanceof Experiment) {
            $this->setExperimentTarget($target);
        } elseif ($target instanceof SurfacicUnitPlot) {
            $this->setSurfacicUnitPlotTarget($target);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSession(): ?Session
    {
        return $this->session;
    }

    /**
     * @return $this
     */
    public function setSession(?Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFieldParent(): ?FieldGeneration
    {
        return $this->fieldParent;
    }

    /**
     * @return $this
     */
    public function setFieldParent(?FieldGeneration $fieldParent): self
    {
        $this->fieldParent = $fieldParent;

        return $this;
    }

    /**
     * @return $this
     */
    public function setVariable(AbstractVariable $variable): self
    {
        if ($variable instanceof GeneratorVariable) {
            $this->setGeneratorVariable($variable);
        } elseif ($variable instanceof SimpleVariable) {
            $this->setSimpleVariable($variable);
        } elseif ($variable instanceof SemiAutomaticVariable) {
            $this->setSemiAutomaticVariable($variable);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getGeneratorVariable(): ?GeneratorVariable
    {
        return $this->generatorVariable;
    }

    /**
     * @return $this
     */
    public function setGeneratorVariable(GeneratorVariable $generatorVariable): self
    {
        $this->simpleVariable = null;
        $this->generatorVariable = $generatorVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSimpleVariable(): ?SimpleVariable
    {
        return $this->simpleVariable;
    }

    /**
     * @return $this
     */
    public function setSimpleVariable(SimpleVariable $simpleVariable): self
    {
        $this->simpleVariable = $simpleVariable;
        $this->generatorVariable = null;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSemiAutomaticVariable(): ?SemiAutomaticVariable
    {
        return $this->semiAutomaticVariable;
    }

    /**
     * @return $this
     */
    public function setSemiAutomaticVariable(?SemiAutomaticVariable $semiAutomaticVariable): self
    {
        $this->semiAutomaticVariable = $semiAutomaticVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualTarget(): ?Individual
    {
        return $this->individualTarget;
    }

    /**
     * @return $this
     */
    public function setIndividualTarget(?Individual $individualTarget): self
    {
        $this->individualTarget = $individualTarget;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitPlotTarget(): ?UnitPlot
    {
        return $this->unitPlotTarget;
    }

    /**
     * @return $this
     */
    public function setUnitPlotTarget(?UnitPlot $unitPlotTarget): self
    {
        $this->unitPlotTarget = $unitPlotTarget;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSurfacicUnitPlotTarget(): ?SurfacicUnitPlot
    {
        return $this->surfacicUnitPlotTarget;
    }

    /**
     * @return $this
     */
    public function setSurfacicUnitPlotTarget(?SurfacicUnitPlot $surfacicUnitPlotTarget): self
    {
        $this->surfacicUnitPlotTarget = $surfacicUnitPlotTarget;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlockTarget(): ?Block
    {
        return $this->blockTarget;
    }

    /**
     * @return $this
     */
    public function setBlockTarget(?Block $blockTarget): self
    {
        $this->blockTarget = $blockTarget;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlockTarget(): ?SubBlock
    {
        return $this->subBlockTarget;
    }

    /**
     * @return $this
     */
    public function setSubBlockTarget(?SubBlock $subBlockTarget): self
    {
        $this->subBlockTarget = $subBlockTarget;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperimentTarget(): ?Experiment
    {
        return $this->experimentTarget;
    }

    /**
     * @return $this
     */
    public function setExperimentTarget(?Experiment $experimentTarget): self
    {
        $this->experimentTarget = $experimentTarget;

        return $this;
    }

    /**
     * @return Collection<int,  Measure>
     *
     * @psalm-mutation-free
     */
    public function getMeasures(): Collection
    {
        return $this->measures;
    }

    public function addMeasure(Measure $measure): self
    {
        if (!$this->measures->contains($measure)) {
            $this->measures->add($measure);
            $measure->setFormField($this);
        }

        return $this;
    }

    public function removeMeasure(Measure $measure): self
    {
        if ($this->measures->contains($measure)) {
            $this->measures->removeElement($measure);
        }

        return $this;
    }

    /**
     * @return Collection<int,  FieldGeneration>
     */
    public function getFieldGenerations(): Collection
    {
        return $this->fieldGenerations;
    }

    /**
     * @return $this
     */
    public function addFieldGeneration(FieldGeneration $fieldGeneration): self
    {
        if (!$this->fieldGenerations->contains($fieldGeneration)) {
            $this->fieldGenerations->add($fieldGeneration);
            $fieldGeneration->setFormField($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeFieldGeneration(FieldGeneration $fieldGeneration): self
    {
        if ($this->fieldGenerations->contains($fieldGeneration)) {
            $this->fieldGenerations->removeElement($fieldGeneration);
        }

        return $this;
    }
}
