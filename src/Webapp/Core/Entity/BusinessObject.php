<?php

namespace Webapp\Core\Entity;

interface BusinessObject
{
    /**
     * @psalm-mutation-free
     */
    public function parent(): ?BusinessObject;

    /**
     * @return BusinessObject[]
     */
    public function children(): array;

    /**
     * @return $this
     */
    public function setGraphicallyDeletedAt(?\DateTime $deletedAt = null): self;

    /**
     * @psalm-mutation-free
     */
    public function getGraphicallyDeletedAt(): ?\DateTime;
}
