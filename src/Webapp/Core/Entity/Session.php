<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\ApiOperation\ProjectWeightOperation;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *          "getWeight"={
 *                "controller"=ProjectWeightOperation::class,
 *                "method"="GET",
 *                "path"="/sessions/weight",
 *                "read"=false,
 *                "validate"=false,
 *                "openapi_context"={
 *                    "summary": "Restore deleted protocol",
 *                    "description": "Remove the deleted state"
 *                },
 *           },
 *     },
 *     itemOperations={
 *         "get"={},
 *         "patch"={"denormalization_context"={"groups"={"edit"}}}
 *     },
 * )
 *
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"webapp_data_view", "id_read"}})
 * @ApiFilter(SearchFilter::class, properties={"projectData": "exact", "id": "exact"})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="session_data", schema="webapp")
 */
class Session extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\ProjectData", inversedBy="sessions")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ProjectData $projectData;

    /**
     * @Groups({"data_explorer_view", "data_entry_synthesis", "variable_synthesis"})
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     */
    private User $user;

    /**
     * @Groups({"webapp_data_view", "data_explorer_view", "data_entry_synthesis"})
     *
     * @ORM\Column(type="datetime")
     */
    private \DateTime $startedAt;

    /**
     * @Groups({"webapp_data_view", "data_explorer_view", "data_entry_synthesis"})
     *
     * @ORM\Column(type="datetime")
     */
    private \DateTime $endedAt;

    /**
     * @var Collection<int, FieldMeasure>
     *
     * @Groups({"webapp_data_view", "data_entry_synthesis", "variable_synthesis"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\FieldMeasure", mappedBy="session", cascade={"persist", "remove", "detach"})
     */
    private Collection $fieldMeasures;

    /**
     * @var Collection<int, Annotation>
     *
     * @Groups({"webapp_data_view", "data_explorer_view", "data_entry_synthesis"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Annotation", mappedBy="session", cascade={"persist", "remove", "detach"})
     */
    private Collection $annotations;

    /**
     * @Groups({"edit"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $comment = null;

    /**
     * Session constructor.
     */
    public function __construct()
    {
        $this->fieldMeasures = new ArrayCollection();
        $this->annotations = new ArrayCollection();
        $this->startedAt = new \DateTime();
        $this->endedAt = new \DateTime();
    }

    /**
     * @psalm-mutation-free
     */
    public function getProjectData(): ProjectData
    {
        return $this->projectData;
    }

    /**
     * @return $this
     */
    public function setProjectData(ProjectData $projectData): self
    {
        $this->projectData = $projectData;

        return $this;
    }

    /**
     * @Groups({"webapp_data_view"})
     */
    public function getUserName(): string
    {
        return $this->user->getUsername();
    }

    /**
     * @psalm-mutation-free
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStartedAt(): \DateTime
    {
        return $this->startedAt;
    }

    /**
     * @return $this
     */
    public function setStartedAt(\DateTime $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getEndedAt(): \DateTime
    {
        return $this->endedAt;
    }

    /**
     * @return $this
     */
    public function setEndedAt(\DateTime $endedAt): self
    {
        $this->endedAt = $endedAt;

        return $this;
    }

    /**
     * @return Collection<int, FieldMeasure>
     *
     * @psalm-mutation-free
     */
    public function getFieldMeasures(): Collection
    {
        return $this->fieldMeasures;
    }

    /**
     * @return $this
     */
    public function addFieldMeasure(FieldMeasure $fieldMeasure): self
    {
        if (!$this->fieldMeasures->contains($fieldMeasure)) {
            $this->fieldMeasures->add($fieldMeasure);
            $fieldMeasure->setSession($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeFieldMeasure(FieldMeasure $fieldMeasure): self
    {
        if ($this->fieldMeasures->contains($fieldMeasure)) {
            $this->fieldMeasures->removeElement($fieldMeasure);
            $fieldMeasure->setSession(null);
        }

        return $this;
    }

    /**
     * @return Collection<int,  Annotation>
     *
     * @psalm-mutation-free
     */
    public function getAnnotations(): Collection
    {
        return $this->annotations;
    }

    /**
     * @param iterable<int,  Annotation> $annotations
     *
     * @return $this
     */
    public function setAnnotations(iterable $annotations): self
    {
        ArrayCollectionUtils::update($this->annotations, $annotations, function (Annotation $annotation) {
            $annotation->setSession($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addAnnotation(Annotation $annotation): self
    {
        if (!$this->annotations->contains($annotation)) {
            $this->annotations->add($annotation);
            $annotation->setSession($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAnnotation(Annotation $annotation): self
    {
        if ($this->annotations->contains($annotation)) {
            $this->annotations->removeElement($annotation);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
