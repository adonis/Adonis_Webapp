<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\Enumeration\Annotation\EnumType;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *          },
 *     },
 *     itemOperations={
 *          "get"={},
 *          "patch"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())"
 *          },
 *          "delete"={
 *              "security"="!object.isPermanent() && is_granted('ROLE_PLATFORM_MANAGER', object.getSite())"
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"site": "exact", "code": "exact", "project": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"project_explorer_view"}})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="state_code", schema="webapp")
 */
class StateCode extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"project_explorer_view", "state_code_post", "webapp_data_view", "data_entry_synthesis", "variable_synthesis", "data_view_item", "graphical_measure_view"})
     *
     * @UniqueAttributeInParent(parentsAttributes={"project.stateCodes", "site.stateCodes"})
     */
    private int $code = 0;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"project_explorer_view", "state_code_post", "data_entry_synthesis", "variable_synthesis", "fusion_result"})
     */
    private string $title = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"state_code_post"})
     */
    private ?string $meaning = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @EnumType(class="Webapp\Core\Enumeration\SpreadingEnum", nullable=true)
     *
     * @Groups({"state_code_post"})
     */
    private ?string $spreading = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"state_code_post", "graphical_measure_view"})
     */
    private ?int $color = null;

    /**
     * @ORM\Column(type="boolean")
     *
     * @ApiProperty(writable=false)
     *
     * @Groups({"project_explorer_view", "data_entry_synthesis"})
     */
    private bool $permanent = false;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="stateCodes")
     *
     * @Groups({"state_code_post"})
     */
    private ?Site $site = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Project", inversedBy="stateCodes")
     *
     * @Groups({"state_code_post"})
     */
    private ?Project $project = null;

    /**
     * @psalm-mutation-free
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return $this
     */
    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMeaning(): ?string
    {
        return $this->meaning;
    }

    /**
     * @return $this
     */
    public function setMeaning(?string $meaning): self
    {
        $this->meaning = $meaning;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSpreading(): ?string
    {
        return $this->spreading;
    }

    /**
     * @return $this
     */
    public function setSpreading(?string $spreading): self
    {
        $this->spreading = $spreading;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?int
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?int $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): ?Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(?Site $site): self
    {
        $this->site = $site;
        $this->project = null;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(?Project $project): self
    {
        $this->project = $project;
        $this->site = null;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isPermanent(): bool
    {
        return $this->permanent;
    }

    /**
     * @return $this
     */
    public function setPermanent(bool $permanent): self
    {
        $this->permanent = $permanent;

        return $this;
    }
}
