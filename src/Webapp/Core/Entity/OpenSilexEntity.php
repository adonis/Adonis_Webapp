<?php

namespace Webapp\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait OpenSilexEntity
{
    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"factor_post", "protocol_full_view", "simple_variable_post"})
     */
    private ?string $openSilexUri = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\OpenSilexInstance")
     *
     * @Groups({"factor_post", "protocol_full_view", "simple_variable_post"})
     */
    private ?OpenSilexInstance $openSilexInstance = null;

    /**
     * @psalm-mutation-free
     */
    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    /**
     * @return $this
     */
    public function setOpenSilexUri(?string $openSilexUri): self
    {
        $this->openSilexUri = $openSilexUri;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    /**
     * @return $this
     */
    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): self
    {
        $this->openSilexInstance = $openSilexInstance;

        return $this;
    }
}
