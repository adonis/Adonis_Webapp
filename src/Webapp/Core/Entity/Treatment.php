<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER', object.getProtocol().getSite())"},
 *          "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getProtocol().getSite())",
 *              "denormalization_context"={"groups"={"edit"}}
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"protocol": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"design_explorer_view"}})
 *
 * @Gedmo\SoftDeleteable()
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="treatment", schema="webapp")
 */
class Treatment extends IdentifiedEntity
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view", "edit", "change_report", "protocol_synthesis", "protocol_full_view", "data_view_item"})
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view", "edit", "protocol_synthesis", "protocol_full_view", "data_view_item"})
     */
    private string $shortName = '';

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view", "protocol_synthesis", "protocol_full_view"})
     */
    private int $repetitions = 0;

    /**
     * @var Collection<int, Modality>
     *
     * @ORM\ManyToMany(targetEntity="Webapp\Core\Entity\Modality", inversedBy="treatments")
     *
     * @ORM\JoinTable(name="rel_treatment_modality", schema="webapp")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "webapp_data_view", "protocol_synthesis", "protocol_full_view", "data_view_item"})
     */
    private Collection $modalities;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Protocol", inversedBy="treatments")
     */
    private Protocol $protocol;

    public function __construct()
    {
        $this->modalities = new ArrayCollection();
    }

    /**
     * @Groups({"platform_full_view"})
     *
     * @psalm-mutation-free
     */
    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return $this
     */
    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @return $this
     */
    public function setRepetitions(int $repetitions): self
    {
        $this->repetitions = $repetitions;

        return $this;
    }

    /**
     * @return Collection<int,  Modality>
     *
     * @psalm-mutation-free
     */
    public function getModalities(): Collection
    {
        return $this->modalities;
    }

    /**
     * @param iterable<int,  Modality> $modalities
     *
     * @return $this
     */
    public function setModalities(iterable $modalities): self
    {
        ArrayCollectionUtils::update($this->modalities, $modalities);

        return $this;
    }

    /**
     * @return $this
     */
    public function addModalities(Modality $modality): self
    {
        if (!$this->modalities->contains($modality)) {
            $this->modalities->add($modality);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeModalities(Modality $modality): self
    {
        if ($this->modalities->contains($modality)) {
            $this->modalities->removeElement($modality);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(Protocol $protocol): self
    {
        $this->protocol = $protocol;

        return $this;
    }
}
