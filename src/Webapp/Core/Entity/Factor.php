<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\RightManagement\Annotation\AdvancedRight;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *              "denormalization_context"={"groups"={"factor_post"}}
 *          }
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "patch"={
 *              "security"="object.getProtocol() === null && is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *              "denormalization_context"={"groups"={"factor_post"}}
 *          },
 *         "delete"={"security"="object.getProtocol() === null && is_granted('ROLE_PLATFORM_MANAGER', object.getSite())"},
 *     }
 *
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"protocol": "exact", "site": "exact", "name": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"design_explorer_view"}})
 *
 * @AdvancedRight(siteAttribute="site", parentFields={"protocol"})
 *
 * @Gedmo\SoftDeleteable()
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="factor", schema="webapp")
 */
class Factor extends IdentifiedEntity
{
    use OpenSilexEntity;

    use SoftDeleteableEntity;

    /**
     * @Assert\NotBlank
     *
     * @UniqueAttributeInParent(parentsAttributes={"site.factors", "protocol.factors"})
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"factor_post","design_explorer_view", "platform_full_view", "webapp_data_view", "protocol_synthesis", "protocol_full_view"})
     */
    private string $name = '';

    /**
     * @var Collection<int, Modality>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Modality", mappedBy="factor", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @Groups({"factor_post","design_explorer_view", "platform_full_view", "protocol_synthesis", "protocol_full_view"})
     */
    private Collection $modalities;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="factors")
     *
     * @Groups({"factor_post"})
     */
    private ?Site $site = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Protocol", inversedBy="factors")
     */
    private ?Protocol $protocol = null;

    /**
     * @ORM\Column(name="factor_order", type="integer", nullable=true)
     *
     * @Groups({"factor_post","design_explorer_view", "platform_full_view", "webapp_data_view", "protocol_synthesis", "protocol_full_view", "data_view_item"})
     */
    private ?int $order = null;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"factor_post","design_explorer_view", "protocol_full_view"})
     */
    private bool $germplasm = false;

    public function __construct()
    {
        $this->modalities = new ArrayCollection();
    }

    /**
     * @Groups({"platform_full_view"})
     *
     * @psalm-mutation-free
     */
    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int,  Modality>
     *
     * @psalm-mutation-free
     */
    public function getModalities(): Collection
    {
        return $this->modalities;
    }

    /**
     * @param iterable<int,  Modality> $modalities
     *
     * @return $this
     */
    public function setModalities(iterable $modalities): self
    {
        ArrayCollectionUtils::update($this->modalities, $modalities, function (Modality $modality) {
            $modality->setFactor($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addModality(Modality $modality): self
    {
        if (!$this->modalities->contains($modality)) {
            $this->modalities->add($modality);
            $modality->setFactor($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeModality(Modality $modality): self
    {
        if ($this->modalities->contains($modality)) {
            $this->modalities->removeElement($modality);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): ?Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): ?Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(?Protocol $protocol): self
    {
        $this->site = null;
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOrder(): ?int
    {
        return $this->order;
    }

    /**
     * @return $this
     */
    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return $this
     */
    public function setDeletedAt(?\DateTime $deletedAt = null): self
    {
        $this->deletedAt = $deletedAt;
        if (null === $deletedAt) {
            foreach ($this->getModalities() as $child) {
                $child->setDeletedAt($deletedAt);
            }
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isGermplasm(): bool
    {
        return $this->germplasm;
    }

    /**
     * @return $this
     */
    public function setGermplasm(bool $germplasm): self
    {
        $this->germplasm = $germplasm;

        return $this;
    }
}
