<?php

namespace Webapp\Core\Entity\Fusion;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\ApiOperation\FusionDataEntriesOperation;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\ProjectData;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "controller"=FusionDataEntriesOperation::class,
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Fusion one or more dataEntries",
 *                  "description": "Create a new data entry by merging multiple dataEntries"
 *              },
 *              "normalization_context"={"groups"={"fusion_result"}}
 *          },
 *     },
 *     itemOperations={
 *     "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }}
 * )
 *
 * @psalm-type SpecificOrderForItem = array{"objectIri": string, "orderedProjectDatasIris": string[]}[]
 */
class DataEntryFusion
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    private string $name = '';

    /**
     * Project datas ordered by priority.
     *
     * @var ProjectData[]
     */
    private array $orderedDefaultProjectDatasIris = [];

    /**
     * IRIs of variables to merge.
     *
     * @var string[]
     */
    private array $selectedVariableIris = [];

    /**
     * Specific order defined for business object.
     *
     * @psalm-var SpecificOrderForItem
     */
    private array $specificOrderForItem = [];

    /**
     * @Groups("fusion_result")
     */
    private ?ProjectData $result = null;

    /**
     * Conflicts detected.
     *
     * @var FieldMeasure[][]|null
     *
     * @Groups("fusion_result")
     */
    private ?array $conflicts = [];

    /**
     * Priority defined for conflicting datas.
     *
     * @var array<int, FieldMeasure>|null
     */
    private ?array $mergePriority = [];

    public function __construct()
    {
        $this->mergePriority = [];
    }

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ProjectData[]
     *
     * @psalm-mutation-free
     */
    public function getOrderedDefaultProjectDatasIris(): array
    {
        return $this->orderedDefaultProjectDatasIris;
    }

    /**
     * @param ProjectData[] $orderedDefaultProjectDatasIris
     *
     * @return $this
     */
    public function setOrderedDefaultProjectDatasIris(array $orderedDefaultProjectDatasIris): self
    {
        $this->orderedDefaultProjectDatasIris = $orderedDefaultProjectDatasIris;

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getSelectedVariableIris(): array
    {
        return $this->selectedVariableIris;
    }

    /**
     * @param string[] $selectedVariableIris
     *
     * @return $this
     */
    public function setSelectedVariableIris(array $selectedVariableIris): self
    {
        $this->selectedVariableIris = $selectedVariableIris;

        return $this;
    }

    /**
     * @return SpecificOrderForItem
     *
     * @psalm-mutation-free
     */
    public function getSpecificOrderForItem(): array
    {
        return $this->specificOrderForItem;
    }

    /**
     * @param SpecificOrderForItem $specificOrderForItem
     *
     * @return $this
     */
    public function setSpecificOrderForItem(array $specificOrderForItem): self
    {
        $this->specificOrderForItem = $specificOrderForItem;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResult(): ?ProjectData
    {
        return $this->result;
    }

    /**
     * @return $this
     */
    public function setResult(?ProjectData $result): self
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @Groups("fusion_result")
     *
     * @psalm-mutation-free
     */
    public function getPlatform(): Platform
    {
        return $this->orderedDefaultProjectDatasIris[0]->getProject()->getPlatform();
    }

    /**
     * @return FieldMeasure[][]
     *
     * @psalm-mutation-free
     */
    public function getConflicts(): ?array
    {
        return $this->conflicts;
    }

    /**
     * @param FieldMeasure[][] $conflicts
     *
     * @return $this
     */
    public function setConflicts(?array $conflicts): self
    {
        $this->conflicts = $conflicts;

        return $this;
    }

    /**
     * @return FieldMeasure[]
     *
     * @psalm-mutation-free
     */
    public function getMergePriority(): ?array
    {
        return $this->mergePriority;
    }

    /**
     * @param FieldMeasure[] $mergePriority
     *
     * @return $this
     */
    public function setMergePriority(?array $mergePriority): self
    {
        $this->mergePriority = $mergePriority;

        return $this;
    }
}
