<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     itemOperations={
 *         "get"={},
 *         "delete"={},
 *         "patch"={}
 *     }
 * )
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="scale", schema="webapp")
 */
class VariableScale extends IdentifiedEntity
{
    /**
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string")
     *
     * @Groups({"webapp_data_view", "project_explorer_view", "simple_variable_get", "simple_variable_post", "project_synthesis"})
     */
    private string $name = '';

    /**
     * @Groups({"webapp_data_view", "simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="integer")
     */
    private int $minValue = 0;

    /**
     * @Groups({"webapp_data_view", "simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="integer")
     */
    private int $maxValue = 0;

    /**
     * @Groups({"webapp_data_view", "simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="boolean")
     */
    private bool $open = false;

    /**
     * @ORM\OneToOne(targetEntity="Webapp\Core\Entity\SimpleVariable", inversedBy="scale")
     */
    private SimpleVariable $variable;

    /**
     * @var Collection<int, VariableScaleItem>
     *
     * @Groups({"webapp_data_view", "simple_variable_get", "simple_variable_post"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\VariableScaleItem", mappedBy="scale", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private Collection $values;

    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMinValue(): int
    {
        return $this->minValue;
    }

    /**
     * @return $this
     */
    public function setMinValue(int $minValue): self
    {
        $this->minValue = $minValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMaxValue(): int
    {
        return $this->maxValue;
    }

    /**
     * @return $this
     */
    public function setMaxValue(int $maxValue): self
    {
        $this->maxValue = $maxValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isOpen(): bool
    {
        return $this->open;
    }

    /**
     * @return $this
     */
    public function setOpen(bool $open): self
    {
        $this->open = $open;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): SimpleVariable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(SimpleVariable $variable): self
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @return Collection<int,  VariableScaleItem>
     *
     * @psalm-mutation-free
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    /**
     * @param iterable<int,  VariableScaleItem> $values
     *
     * @return $this
     */
    public function setValues(iterable $values): self
    {
        ArrayCollectionUtils::update($this->values, $values, function (VariableScaleItem $variableScaleItem) {
            $variableScaleItem->setScale($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addValue(VariableScaleItem $variableScaleItem): self
    {
        if (!$this->values->contains($variableScaleItem)) {
            $this->values->add($variableScaleItem);
            $variableScaleItem->setScale($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeValue(VariableScaleItem $variableScaleItem): self
    {
        if ($this->values->contains($variableScaleItem)) {
            $this->values->removeElement($variableScaleItem);
            $variableScaleItem->setScale(null);
        }

        return $this;
    }
}
