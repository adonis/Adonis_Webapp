<?php

namespace Webapp\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="algorithm_parameter", schema="webapp")
 */
class AlgorithmParameter extends IdentifiedEntity
{
    public const SPECIAL_PARAM_NBR_TREATMENT = 'treatmentNumber';
    public const SPECIAL_PARAM_CONSTANT_REPETITION = 'repetitionNumber';
    public const SPECIAL_PARAM_NBR_FACTOR = 'factorNumber';

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private string $name = '';

    /**
     * @ORM\Column(name="algorithm_order", type="integer")
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private int $order = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private ?string $specialParam = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Algorithm", inversedBy="algorithmParameters")
     */
    private Algorithm $algorithm;

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @return $this
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSpecialParam(): ?string
    {
        return $this->specialParam;
    }

    /**
     * @return $this
     */
    public function setSpecialParam(?string $specialParam): self
    {
        $this->specialParam = $specialParam;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAlgorithm(): Algorithm
    {
        return $this->algorithm;
    }

    /**
     * @return $this
     */
    public function setAlgorithm(Algorithm $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }
}
