<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;

/**
 * @ApiResource(
 *     itemOperations={
 *          "get"={"security"="is_granted('ROLE_ADMIN')"},
 *          "patch"={"security"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="parameters", schema="webapp")
 */
class Parameters extends IdentifiedEntity
{
    /**
     * @ORM\Id()
     *
     * @ORM\Column(name="id", type="integer")
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(type="integer")
     */
    private int $daysBeforeFileDelete = 0;

    /**
     * @psalm-mutation-free
     */
    public function getDaysBeforeFileDelete(): int
    {
        return $this->daysBeforeFileDelete;
    }

    /**
     * @return $this
     */
    public function setDaysBeforeFileDelete(int $daysBeforeFileDelete): self
    {
        $this->daysBeforeFileDelete = $daysBeforeFileDelete;

        return $this;
    }
}
