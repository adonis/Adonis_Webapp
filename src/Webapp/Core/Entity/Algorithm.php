<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"}
 *     }
 * )
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="algorithm", schema="webapp")
 */
class Algorithm extends IdentifiedEntity
{
    public const SPECIAL_ALGORITHM_SPLIT_PLOT = 'SplitPlot';

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"platform_full_view", "platform_synthesis", "protocol_synthesis", "protocol_full_view"})
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private string $scriptName = '';

    /**
     * @var Collection<int, AlgorithmParameter>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\AlgorithmParameter", mappedBy="algorithm", cascade={"persist", "remove"})
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private Collection $algorithmParameters;

    /**
     * @var Collection<int, AlgorithmCondition>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\AlgorithmCondition", mappedBy="algorithm", cascade={"persist", "remove"})
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private Collection $algorithmConditions;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private bool $withSubBlock = false;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private bool $withRepartition = false;

    public function __construct()
    {
        $this->algorithmParameters = new ArrayCollection();
        $this->algorithmConditions = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): Algorithm
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getScriptName(): string
    {
        return $this->scriptName;
    }

    /**
     * @return $this
     */
    public function setScriptName(string $scriptName): Algorithm
    {
        $this->scriptName = $scriptName;

        return $this;
    }

    /**
     * @return Collection<int, AlgorithmParameter>
     *
     * @psalm-mutation-free
     */
    public function getAlgorithmParameters(): Collection
    {
        return $this->algorithmParameters;
    }

    /**
     * @param iterable<array-key, AlgorithmParameter> $algorithmParameters
     *
     * @return $this
     */
    public function setAlgorithmParameters(iterable $algorithmParameters): self
    {
        ArrayCollectionUtils::update($this->algorithmParameters, $algorithmParameters, function (AlgorithmParameter $parameter) {
            $parameter->setAlgorithm($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addAlgorithmParameter(AlgorithmParameter $algorithmParameter): self
    {
        if (!$this->algorithmParameters->contains($algorithmParameter)) {
            $this->algorithmParameters->add($algorithmParameter);
            $algorithmParameter->setAlgorithm($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAlgorithmParameter(AlgorithmParameter $algorithmParameter): self
    {
        if ($this->algorithmParameters->contains($algorithmParameter)) {
            $this->algorithmParameters->removeElement($algorithmParameter);
        }

        return $this;
    }

    /**
     * @return Collection<int, AlgorithmCondition>
     *
     * @psalm-mutation-free
     */
    public function getAlgorithmConditions(): Collection
    {
        return $this->algorithmConditions;
    }

    /**
     * @param iterable<array-key, AlgorithmCondition> $algorithmConditions
     *
     * @return $this
     */
    public function setAlgorithmConditions(iterable $algorithmConditions): self
    {
        ArrayCollectionUtils::update($this->algorithmConditions, $algorithmConditions, function (AlgorithmCondition $condition) {
            $condition->setAlgorithm($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addAlgorithmCondition(AlgorithmCondition $algorithmCondition): self
    {
        if (!$this->algorithmConditions->contains($algorithmCondition)) {
            $this->algorithmConditions->add($algorithmCondition);
            $algorithmCondition->setAlgorithm($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAlgorithmCondition(AlgorithmCondition $algorithmCondition): self
    {
        if ($this->algorithmConditions->contains($algorithmCondition)) {
            $this->algorithmConditions->removeElement($algorithmCondition);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isWithSubBlock(): bool
    {
        return $this->withSubBlock;
    }

    /**
     * @return $this
     */
    public function setWithSubBlock(bool $withSubBlock): self
    {
        $this->withSubBlock = $withSubBlock;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isWithRepartition(): bool
    {
        return $this->withRepartition;
    }

    /**
     * @return $this
     */
    public function setWithRepartition(bool $withRepartition): self
    {
        $this->withRepartition = $withRepartition;

        return $this;
    }
}
