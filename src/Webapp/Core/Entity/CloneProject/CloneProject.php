<?php

namespace Webapp\Core\Entity\CloneProject;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\CloneProjectOperation;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\Project;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "controller"=CloneProjectOperation::class,
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Clone a data entry project",
 *                  "description": "Create a copy of the given project giving him a new name"
 *              },
 *          },
 *     },
 *     itemOperations={
 *     "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }}
 * )
 */
class CloneProject
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    private Project $project;

    private string $newName = '';

    private Project $result;

    private Platform $platform;

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getNewName(): string
    {
        return $this->newName;
    }

    /**
     * @return $this
     */
    public function setNewName(string $newName): self
    {
        $this->newName = $newName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResult(): Project
    {
        return $this->result;
    }

    /**
     * @return $this
     */
    public function setResult(Project $result): self
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }
}
