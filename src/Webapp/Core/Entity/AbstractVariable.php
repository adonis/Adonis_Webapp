<?php

namespace Webapp\Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Enumeration\Annotation\EnumType;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Enumeration\PathLevelEnum;
use Webapp\Core\Enumeration\VariableFormatEnum;
use Webapp\Core\Enumeration\VariableTypeEnum;

/**
 * @ORM\MappedSuperclass()
 *
 * @psalm-type VariableType = SimpleVariable | GeneratorVariable | SemiAutomaticVariable
 *
 * @psalm-import-type PathLevelEnumId from PathLevelEnum
 * @psalm-import-type VariableTypeEnumId from VariableTypeEnum
 * @psalm-import-type VariableFormatEnumId from VariableFormatEnum
 */
abstract class AbstractVariable extends IdentifiedEntity
{
    use OpenSilexEntity;

    protected string $name = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "webapp_data_view", "data_entry_synthesis", "project_synthesis", "variable_synthesis", "webapp_data_fusion"})
     */
    protected string $shortName = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "webapp_data_view", "data_entry_synthesis", "project_synthesis", "variable_synthesis"})
     */
    protected int $repetitions = 0;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "webapp_data_view", "data_entry_synthesis", "project_synthesis", "variable_synthesis"})
     */
    protected ?string $unit = null;

    /**
     * @psalm-var PathLevelEnumId|''
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @EnumType(class="Webapp\Core\Enumeration\PathLevelEnum")
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "webapp_data_view", "simple_variable_get", "simple_variable_post", "connected_variables", "data_entry_synthesis", "project_synthesis", "variable_synthesis", "webapp_data_path", "webapp_data_fusion"})
     */
    protected string $pathLevel = '';

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "variable_synthesis"})
     */
    protected ?string $comment = null;

    /**
     * @ORM\Column(type="integer", name="print_order", nullable=true)
     *
     * @Groups({"project_explorer_view", "generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "project_synthesis"})
     */
    protected ?int $order = null;

    /**
     * @var VariableFormatEnumId|numeric-string|null
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @EnumType(class="Webapp\Core\Enumeration\VariableFormatEnum", nullable=true)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "webapp_data_view", "data_view_item", "graphical_measure_view"})
     */
    protected ?string $format = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post"})
     */
    protected ?int $formatLength = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post"})
     */
    protected ?bool $defaultTrueValue = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post", "webapp_data_view"})
     */
    protected ?string $identifier = null;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @Groups({"generator_variable_post", "semi_automatic_variable", "simple_variable_get", "simple_variable_post"})
     */
    protected bool $mandatory = false;

    /**
     * @var Collection<int, Test>
     */
    protected Collection $tests;

    /**
     * @Groups({"simple_variable_get"})
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     */
    protected \DateTime $created;

    /**
     * @Groups({"simple_variable_get"})
     *
     * @Gedmo\Timestampable()
     *
     * @ORM\Column(type="datetime")
     */
    protected \DateTime $lastModified;

    /**
     * @var Collection<int, VariableConnection>
     */
    protected Collection $connectedVariables;

    public function __construct()
    {
        $this->connectedVariables = new ArrayCollection();
        $this->tests = new ArrayCollection();
        $this->created = new \DateTime();
        $this->lastModified = new \DateTime();
    }

    /**
     * @psalm-mutation-free
     */
    abstract public function getProject(): ?Project;

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return VariableTypeEnumId|''
     *
     * @psalm-mutation-free
     */
    abstract public function getType(): string;

    /**
     * @psalm-mutation-free
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @return $this
     */
    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @return $this
     */
    public function setRepetitions(int $repetitions): self
    {
        $this->repetitions = $repetitions;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @return $this
     */
    public function setUnit(?string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * @psalm-return PathLevelEnumId|''
     *
     * @psalm-mutation-free
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @psalm-param PathLevelEnumId|'' $pathLevel
     *
     * @return $this
     */
    public function setPathLevel(string $pathLevel): self
    {
        $this->pathLevel = $pathLevel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOrder(): ?int
    {
        return $this->order;
    }

    /**
     * @return $this
     */
    public function setOrder(?int $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @psalm-return VariableFormatEnumId|numeric-string|null
     *
     * @psalm-mutation-free
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @psalm-param VariableFormatEnumId|numeric-string|null $format
     *
     * @return $this
     */
    public function setFormat(?string $format): self
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFormatLength(): ?int
    {
        return $this->formatLength;
    }

    /**
     * @return $this
     */
    public function setFormatLength(?int $formatLength): self
    {
        $this->formatLength = $formatLength;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDefaultTrueValue(): ?bool
    {
        return $this->defaultTrueValue;
    }

    /**
     * @return $this
     */
    public function setDefaultTrueValue(?bool $defaultTrueValue): self
    {
        $this->defaultTrueValue = $defaultTrueValue;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @return $this
     */
    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isMandatory(): bool
    {
        return $this->mandatory;
    }

    /**
     * @return $this
     */
    public function setMandatory(bool $mandatory): self
    {
        $this->mandatory = $mandatory;

        return $this;
    }

    /**
     * @return Collection<int, Test>
     *
     * @psalm-mutation-free
     */
    public function getTests(): Collection
    {
        return $this->tests;
    }

    /**
     * @param iterable<int, Test> $tests
     *
     * @return $this
     */
    public function setTests(iterable $tests): self
    {
        ArrayCollectionUtils::update($this->tests, $tests, function (Test $test) {
            $test->setVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addTest(Test $test): self
    {
        if (!$this->tests->contains($test)) {
            $this->tests->add($test);
            $test->setVariable($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeTest(Test $test): self
    {
        if ($this->tests->contains($test)) {
            $this->tests->removeElement($test);
            $test->setVariable(null);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return $this
     */
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLastModified(): \DateTime
    {
        return $this->lastModified;
    }

    /**
     * @return $this
     */
    public function setLastModified(\DateTime $lastModified): self
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * @return Collection<int, VariableConnection>
     *
     * @psalm-mutation-free
     */
    public function getConnectedVariables(): Collection
    {
        return $this->connectedVariables;
    }

    /**
     * @param iterable<int, VariableConnection> $connectedVariables
     *
     * @return $this
     */
    public function setConnectedVariables(iterable $connectedVariables): self
    {
        ArrayCollectionUtils::update($this->connectedVariables, $connectedVariables, function (VariableConnection $variableConnection) {
            $variableConnection->setProjectVariable($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addVariableConnection(VariableConnection $variableConnection): self
    {
        if (!$this->connectedVariables->contains($variableConnection)) {
            $this->connectedVariables->add($variableConnection);
            $variableConnection->setProjectVariable($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeVariableConnection(VariableConnection $variableConnection): self
    {
        if ($this->connectedVariables->contains($variableConnection)) {
            $this->connectedVariables->removeElement($variableConnection);
        }

        return $this;
    }
}
