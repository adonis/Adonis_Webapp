<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Shared\RightManagement\Annotation\AdvancedRight;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\ApiOperation\DeleteProjectDataOperation;
use Webapp\Core\ApiOperation\ProjectWeightOperation;
use Webapp\Core\Dto\ProjectData\ExportOpenSilex\ProjectDataExportOpenSilex;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "getWeight"={
 *               "controller"=ProjectWeightOperation::class,
 *               "method"="GET",
 *               "path"="/project_datas/weight",
 *               "read"=false,
 *               "validate"=false,
 *               "openapi_context"={
 *                   "summary": "Restore deleted protocol",
 *                   "description": "Remove the deleted state"
 *               },
 *          },
 *     },
 *     itemOperations={
 *         "get"={},
 *         "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getProject().getPlatform().getSite())",
 *              "controller"=DeleteProjectDataOperation::class,
 *          },
 *         "patch"={"denormalization_context"={"groups"={"edit"}}},
 *         "exportOpenSilex"={
 *               "method"="GET",
 *               "path"="/project_datas/{id}/openSilexExport",
 *               "output"=ProjectDataExportOpenSilex::class
 *         },
 *           "updateOpenSilexUri"={
 *               "method"="patch",
 *               "path"="/project_datas/{id}/opensilexUris",
 *               "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *               "input"=ProjectDataExportOpenSilex::class,
 *               "validate"=false
 *           },
 *     },
 * )
 *
 * @AdvancedRight(parentFields={"project"})
 *
 * @ApiFilter(SearchFilter::class, properties={"project.platform.projects": "exact", "project": "exact", "id": "exact", "sessions": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"webapp_data_view", "id_read", "data_explorer_view", "change_report", "data_entry_synthesis", "webapp_data_path", "webapp_data_fusion"}})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="project_data", schema="webapp")
 *
 * @psalm-import-type VariableType from AbstractVariable
 */
class ProjectData extends IdentifiedEntity
{
    use OpenSilexEntity;

    /**
     * @ORM\ManyToOne(targetEntity="\Webapp\Core\Entity\Project", inversedBy="projectDatas")
     *
     * @Groups({"webapp_data_view", "data_explorer_view", "change_report", "data_entry_synthesis", "variable_synthesis", "webapp_data_fusion"})
     */
    private Project $project;

    /**
     * @var Collection<int, Session>
     *
     * @Groups({"webapp_data_view", "data_explorer_view", "data_entry_synthesis", "variable_synthesis", "webapp_data_path", "webapp_data_fusion"})
     *
     * @ORM\OneToMany(targetEntity="\Webapp\Core\Entity\Session", mappedBy="projectData", cascade={"persist", "remove", "detach"})
     */
    private Collection $sessions;

    /**
     * @var Collection<int, ChangeReport>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\ChangeReport", mappedBy="projectData", cascade={"persist", "remove", "detach"})
     *
     * @Groups({"change_report", "data_entry_synthesis"})
     */
    private Collection $changeReports;

    /**
     * @var Collection<int, SimpleVariable>
     *
     * @Groups({"webapp_data_view", "data_explorer_view", "webapp_data_path", "webapp_data_fusion"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\SimpleVariable", mappedBy="projectData", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private Collection $simpleVariables;

    /**
     * @var Collection<int, GeneratorVariable>
     *
     * @Groups({"webapp_data_view", "data_explorer_view", "webapp_data_path", "webapp_data_fusion"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\GeneratorVariable", mappedBy="projectData", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private Collection $generatorVariables;

    /**
     * @var Collection<int, SemiAutomaticVariable>
     *
     * @Groups({"webapp_data_view", "data_explorer_view", "webapp_data_path", "webapp_data_fusion"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable", mappedBy="projectData", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private Collection $semiAutomaticVariables;

    /**
     * @Groups({"data_explorer_view", "webapp_data_fusion", "webapp_data_path"})
     *
     * @ORM\Column(type="boolean")
     */
    private bool $fusion = false;

    /**
     * @Groups({"data_explorer_view", "data_entry_synthesis", "variable_synthesis"})
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     */
    private User $user;

    /**
     * @Groups({"edit", "data_entry_synthesis", "variable_synthesis"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $comment = null;

    /**
     * @Groups({"data_explorer_view", "webapp_data_view", "edit", "data_entry_synthesis", "webapp_data_path", "webapp_data_fusion"})
     *
     * @ORM\Column(type="string")
     */
    private string $name = '';

    public function __construct()
    {
        $this->sessions = new ArrayCollection();
        $this->changeReports = new ArrayCollection();
        $this->simpleVariables = new ArrayCollection();
        $this->generatorVariables = new ArrayCollection();
        $this->semiAutomaticVariables = new ArrayCollection();
    }

    /**
     * @Groups({"data_explorer_view", "webapp_data_view", "change_report", "data_entry_synthesis", "project_synthesis", "variable_synthesis", "webapp_data_path", "webapp_data_fusion"})
     */
    public function getStart(): \DateTime
    {
        $iterator = $this->sessions->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->getStartedAt() < $b->getStartedAt()) ? -1 : 1;
        });

        return 0 === $this->sessions->count() ? new \DateTime() : $iterator->current()->getStartedAt();
    }

    /**
     * @Groups({"webapp_data_view", "data_entry_synthesis", "variable_synthesis", "webapp_data_fusion", "data_explorer_view"})
     */
    public function getEnd(): \DateTime
    {
        $iterator = $this->sessions->getIterator();
        $iterator->uasort(function ($a, $b) {
            return ($a->getEndedAt() > $b->getEndedAt()) ? -1 : 1;
        });

        return 0 === $this->sessions->count() ? new \DateTime() : $iterator->current()->getEndedAt();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return Collection<int,  Session>
     *
     * @psalm-mutation-free
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    /**
     * @param iterable<int,  Session> $sessions
     *
     * @return $this
     */
    public function setSessions(iterable $sessions): self
    {
        ArrayCollectionUtils::update($this->sessions, $sessions, function (Session $session) {
            $session->setProjectData($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions->add($session);
            $session->setProjectData($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSession(Session $session): self
    {
        if ($this->sessions->contains($session)) {
            $this->sessions->removeElement($session);
        }

        return $this;
    }

    /**
     * @return Collection<int, ChangeReport>
     *
     * @psalm-mutation-free
     */
    public function getChangeReports(): Collection
    {
        return $this->changeReports;
    }

    /**
     * @param iterable<int, ChangeReport> $changeReports
     *
     * @return $this
     */
    public function setChangeReports(iterable $changeReports): self
    {
        ArrayCollectionUtils::update($this->changeReports, $changeReports, function (ChangeReport $changeReport) {
            $changeReport->setProjectData($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addChangeReport(ChangeReport $changeReport): self
    {
        if (!$this->changeReports->contains($changeReport)) {
            $this->changeReports->add($changeReport);
            $changeReport->setProjectData($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeChangeReport(ChangeReport $changeReport): self
    {
        if ($this->changeReports->contains($changeReport)) {
            $this->changeReports->removeElement($changeReport);
        }

        return $this;
    }

    /**
     * @return Collection<int, SimpleVariable>
     *
     * @psalm-mutation-free
     */
    public function getSimpleVariables(): Collection
    {
        return $this->simpleVariables;
    }

    /**
     * @param iterable<int, SimpleVariable> $simpleVariables
     *
     * @return $this
     */
    public function setSimpleVariables(iterable $simpleVariables): self
    {
        ArrayCollectionUtils::update($this->simpleVariables, $simpleVariables, function (SimpleVariable $simpleVariable) {
            $simpleVariable->setProjectData($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addSimpleVariable(SimpleVariable $simpleVariable): self
    {
        if (!$this->simpleVariables->contains($simpleVariable)) {
            $this->simpleVariables->add($simpleVariable);
            $simpleVariable->setProjectData($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSimpleVariable(SimpleVariable $simpleVariable): self
    {
        if ($this->simpleVariables->contains($simpleVariable)) {
            $this->simpleVariables->removeElement($simpleVariable);
            $simpleVariable->setProjectData(null);
        }

        return $this;
    }

    /**
     * @return Collection<int, GeneratorVariable>
     *
     * @psalm-mutation-free
     */
    public function getGeneratorVariables(): Collection
    {
        return $this->generatorVariables;
    }

    /**
     * @param iterable<int,  GeneratorVariable> $generatorVariables
     *
     * @return $this
     */
    public function setGeneratorVariables(iterable $generatorVariables): self
    {
        ArrayCollectionUtils::update($this->generatorVariables, $generatorVariables, function (GeneratorVariable $generatorVariable) {
            $generatorVariable->setProjectData($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addGeneratorVariable(GeneratorVariable $generatorVariable): self
    {
        if (!$this->generatorVariables->contains($generatorVariable)) {
            $this->generatorVariables->add($generatorVariable);
            $generatorVariable->setProjectData($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGeneratorVariable(GeneratorVariable $generatorVariable): self
    {
        if ($this->generatorVariables->contains($generatorVariable)) {
            $this->generatorVariables->removeElement($generatorVariable);
            $generatorVariable->setProjectData(null);
        }

        return $this;
    }

    /**
     * @return Collection<int, SemiAutomaticVariable>
     *
     * @psalm-mutation-free
     */
    public function getSemiAutomaticVariables(): Collection
    {
        return $this->semiAutomaticVariables;
    }

    /**
     * @param iterable<int, SemiAutomaticVariable> $semiAutomaticVariables
     *
     * @return $this
     */
    public function setSemiAutomaticVariables(iterable $semiAutomaticVariables): self
    {
        ArrayCollectionUtils::update($this->semiAutomaticVariables, $semiAutomaticVariables, function (SemiAutomaticVariable $semiAutomaticVariable) {
            $semiAutomaticVariable->setProjectData($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addSemiAutomaticVariable(SemiAutomaticVariable $semiAutomaticVariable): self
    {
        if (!$this->semiAutomaticVariables->contains($semiAutomaticVariable)) {
            $this->semiAutomaticVariables->add($semiAutomaticVariable);
            $semiAutomaticVariable->setProjectData($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSemiAutomaticVariable(SemiAutomaticVariable $semiAutomaticVariable): self
    {
        if ($this->semiAutomaticVariables->contains($semiAutomaticVariable)) {
            $this->semiAutomaticVariables->removeElement($semiAutomaticVariable);
            $semiAutomaticVariable->setProjectData(null);
        }

        return $this;
    }

    /**
     * @param array<array-key, VariableType> $variables
     *
     * @return $this
     */
    public function setVariables(array $variables): self
    {
        $this->setSimpleVariables(array_filter($variables, fn (AbstractVariable $variable) => $variable instanceof SimpleVariable));
        $this->setGeneratorVariables(array_filter($variables, fn (AbstractVariable $variable) => $variable instanceof GeneratorVariable));
        $this->setSemiAutomaticVariables(array_filter($variables, fn (AbstractVariable $variable) => $variable instanceof SemiAutomaticVariable));

        return $this;
    }

    /**
     * @param VariableType $variable
     *
     * @return $this
     */
    public function addVariable(AbstractVariable $variable): self
    {
        if ($variable instanceof SimpleVariable) {
            $this->addSimpleVariable($variable);
        } elseif ($variable instanceof GeneratorVariable) {
            $this->addGeneratorVariable($variable);
        } elseif ($variable instanceof SemiAutomaticVariable) {
            $this->addSemiAutomaticVariable($variable);
        } else {
            throw new \InvalidArgumentException('$variable must be a SimpleVariable or GeneratorVariable or SemiAutomaticVariable');
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isFusion(): bool
    {
        return $this->fusion;
    }

    /**
     * @return $this
     */
    public function setFusion(bool $fusion): self
    {
        $this->fusion = $fusion;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @Groups({"webapp_data_view", "webapp_data_fusion"})
     *
     * @psalm-mutation-free
     */
    public function getUserName(): string
    {
        return $this->user->getUsername();
    }

    /**
     * @Groups({"data_entry_synthesis", "platform_synthesis"})
     *
     * @return (SimpleVariable|SemiAutomaticVariable|GeneratorVariable)[]
     */
    public function getVariables(): array
    {
        return array_merge($this->getSimpleVariables()->getValues(), $this->getSemiAutomaticVariables()->getValues(), $this->getGeneratorVariables()->getValues());
    }
}
