<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Shared\RightManagement\Annotation\AdvancedRight;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={"security_post_denormalize"="is_granted('ROLE_SITE_ADMIN', object.getSite())"}
 *     },
 *     itemOperations={
 *         "get"={},
 *         "patch"={"security"="is_granted('ROLE_SITE_ADMIN', object.getSite())"},
 *         "delete"={"security"="is_granted('ROLE_SITE_ADMIN', object.getSite())"},
 *     }
 *
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"site": "exact", "label": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"admin_explorer_view"}})
 *
 * @AdvancedRight(siteAttribute="site")
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="user_group", schema="webapp")
 */
class UserGroup extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"admin_explorer_view"})
     *
     * @Assert\NotBlank
     *
     * @UniqueAttributeInParent(parentsAttributes={"site.userGroups"})
     */
    private string $name = '';

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="userGroups")
     */
    private Site $site;

    /**
     * @var Collection<int, User>
     *
     * @ORM\ManyToMany(targetEntity="Shared\Authentication\Entity\User", inversedBy="groups")
     *
     * @ORM\JoinTable(name="rel_user_user_group", schema="webapp")
     */
    private Collection $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return Collection<int,  User>
     *
     * @psalm-mutation-free
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param iterable<int,  User> $users
     *
     * @return $this
     */
    public function setUsers(iterable $users): self
    {
        ArrayCollectionUtils::update($this->users, $users);

        return $this;
    }
}
