<?php

namespace Webapp\Core\Entity\DataView;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"data_view_item"}},
 *      collectionOperations={
 *          "get"={},
 *      },
 *      itemOperations={
 *          "get"={},
 *      },
 *  )
 *
 * @ApiFilter(SearchFilter::class, properties={"projectData": "exact", "session": "exact"})
 *
 * @ORM\Entity(readOnly=true)
 *
 * @ORM\Table(name="view_data_business_object", schema="webapp")
 */
class DataViewBusinessObject
{
    /**
     * @ORM\Id()
     *
     * @ORM\Column()
     */
    private string $fakeId = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $platform = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $experiment = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $block = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $subBlock = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $surfacicUnitPlot = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $unitPlot = '';

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $individual = '';

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Experiment")
     */
    private ?Experiment $experimentTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Block")
     */
    private ?Block $blockTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SubBlock")
     */
    private ?SubBlock $subBlockTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SurfacicUnitPlot")
     */
    private ?SurfacicUnitPlot $surfacicUnitPlotTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\UnitPlot")
     */
    private ?UnitPlot $unitPlotTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Individual")
     */
    private ?Individual $individualTarget = null;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private bool $dead = false;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private \DateTime $appeared;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private \DateTime $disappeared;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Treatment")
     */
    private ?Treatment $treatment = null;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private int $x = 0;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private int $y = 0;

    /**
     * @Groups({"data_view_item"})
     *
     * @ORM\Column()
     */
    private string $identifier = '';

    public function __construct()
    {
        $this->appeared = new \DateTime();
        $this->disappeared = new \DateTime();
    }

    /**
     * @Groups({"data_view_item"})
     *
     * @psalm-mutation-free
     */
    public function getTargetType(): ?string
    {
        $target = $this->getTarget();
        if ($target instanceof Individual) {
            return PathLevelEnum::INDIVIDUAL;
        } elseif ($target instanceof SurfacicUnitPlot) {
            return PathLevelEnum::SURFACIC_UNIT_PLOT;
        } elseif ($target instanceof UnitPlot) {
            return PathLevelEnum::UNIT_PLOT;
        } elseif ($target instanceof SubBlock) {
            return PathLevelEnum::SUB_BLOCK;
        } elseif ($target instanceof Block) {
            return PathLevelEnum::BLOCK;
        } elseif ($target instanceof Experiment) {
            return PathLevelEnum::EXPERIMENT;
        } else {
            return null;
        }
    }

    /**
     * @Groups({"data_view_item"})
     *
     * @return Block|Experiment|Individual|SubBlock|SurfacicUnitPlot|UnitPlot|null
     *
     * @psalm-mutation-free
     */
    public function getTarget()
    {
        return $this->individualTarget ?? $this->surfacicUnitPlotTarget ?? $this->unitPlotTarget ?? $this->subBlockTarget ?? $this->blockTarget ?? $this->experimentTarget;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): string
    {
        return $this->platform;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperiment(): string
    {
        return $this->experiment;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlock(): string
    {
        return $this->block;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlock(): string
    {
        return $this->subBlock;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSurfacicUnitPlot(): string
    {
        return $this->surfacicUnitPlot;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitPlot(): string
    {
        return $this->unitPlot;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividual(): string
    {
        return $this->individual;
    }

    /**
     * @psalm-mutation-free
     */
    public function isDead(): bool
    {
        return $this->dead;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAppeared(): \DateTime
    {
        return $this->appeared;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDisappeared(): \DateTime
    {
        return $this->disappeared;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTreatment(): ?Treatment
    {
        return $this->treatment;
    }

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFakeId(): string
    {
        return $this->fakeId;
    }
}
