<?php

namespace Webapp\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="algorithm_condition", schema="webapp")
 */
class AlgorithmCondition extends IdentifiedEntity
{
    public const CONDITION_MIN_2_FACTORS = 'min2Factors';
    public const CONDITION_CONSTANT_REPETITION_NUMBER = 'constantRepNumber';
    public const CONDITION_REPETITION_EQUALS_TREATMENTS = 'repEqTreatments';

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"platform_full_view", "protocol_full_view"})
     */
    private ?string $type = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Algorithm", inversedBy="algorithmConditions")
     */
    private Algorithm $algorithm;

    /**
     * @psalm-mutation-free
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAlgorithm(): Algorithm
    {
        return $this->algorithm;
    }

    /**
     * @return $this
     */
    public function setAlgorithm(Algorithm $algorithm): self
    {
        $this->algorithm = $algorithm;

        return $this;
    }
}
