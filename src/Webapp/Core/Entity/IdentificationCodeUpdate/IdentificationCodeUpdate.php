<?php

namespace Webapp\Core\Entity\IdentificationCodeUpdate;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\UpdateIdentificationCodesOperation;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "controller"=UpdateIdentificationCodesOperation::class,
 *              "read"=false,
 *              "deserialize"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Update identification codes",
 *                  "description": "Replace all identification codes by the ones given in params"
 *              },
 *          },
 *     },
 *     itemOperations={
 *     "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }}
 * )
 */
class IdentificationCodeUpdate
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }
}
