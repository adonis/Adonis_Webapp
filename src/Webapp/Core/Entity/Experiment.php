<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Authentication\CustomFilters\DeletedFilter;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Shared\Enumeration\Annotation\EnumType;
use Shared\RightManagement\Annotation\AdvancedRight;
use Shared\RightManagement\Traits\HasOwnerEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Webapp\Core\Annotation\GraphicallyDeletable;
use Webapp\Core\ApiOperation\RestoreObjectOperation;
use Webapp\Core\Dto\BusinessObject\BusinessObjectCsvOutputDto;
use Webapp\Core\Dto\Experiment\ExperimentInputDto;
use Webapp\Core\Dto\Experiment\ExportOpenSilex\ExperimentExportOpenSilex;
use Webapp\Core\Dto\Experiment\ExportOpenSilex\ExperimentExportOpenSilexScientificObject;
use Webapp\Core\Dto\Notes\NotesCsvOutputDto;
use Webapp\Core\Entity\Attachment\ExperimentAttachment;
use Webapp\Core\Enumeration\ExperimentStateEnum;
use Webapp\Core\Traits\GraphicallyDeletableEntity;
use Webapp\Core\Validator\Experiment\ExperimentPatchConstraint;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *
 *     collectionOperations={
 *         "get"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *              "input"=ExperimentInputDto::class,
 *          },
 *     },
 *     itemOperations={
 *          "get"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *          "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *          "delete"={
 *              "security"="object.getState() === 0"
 *          },
 *          "restore"={
 *              "controller"=RestoreObjectOperation::class,
 *              "method"="PATCH",
 *              "path"="/experiments/{id}/restore",
 *              "security"="is_granted('ROLE_SITE_ADMIN')",
 *              "read"=false,
 *              "validate"=false,
 *              "openapi_context"={
 *                  "summary": "Restore deleted protocol",
 *                  "description": "Remove the deleted state"
 *              },
 *          },
 *          "exportNotes"={
 *              "method"="GET",
 *              "path"="/experiments/{id}/notes/exportCsv",
 *              "formats"={"csv"={"text/csv"}},
 *              "pagination_enabled"=false,
 *              "output"=NotesCsvOutputDto::class
 *          },
 *          "export"={
 *              "method"="GET",
 *              "path"="/experiments/{id}/exportCsv",
 *              "formats"={"csv"={"text/csv"}},
 *              "pagination_enabled"=false,
 *              "output"=BusinessObjectCsvOutputDto::class
 *          },
 *          "exportOpenSilex"={
 *              "method"="GET",
 *              "path"="/experiments/{id}/openSilexExport",
 *              "output"=ExperimentExportOpenSilex::class
 *          },
 *          "exportOpenSilexScientificObjects"={
 *              "method"="GET",
 *              "path"="/experiments/{id}/openSilexExportScientificObjects",
 *              "formats"={"json"={"application/json"}},
 *              "pagination_enabled"=false,
 *              "output"=ExperimentExportOpenSilexScientificObject::class
 *          },
 *          "updateOpenSilexUri"={
 *              "method"="patch",
 *              "path"="/experiments/{id}/opensilexUris",
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "input"=ExperimentExportOpenSilex::class,
 *              "validate"=false
 *          },
 *     }
 * )
 *
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"design_explorer_view", "platform_full_view", "admin_explorer_view", "note_view"}})
 * @ApiFilter(ExistsFilter::class, properties={"deletedAt"})
 * @ApiFilter(SearchFilter::class, properties={
 *     "site": "exact",
 *     "name": "exact",
 *     "platform": "exact",
 *     "id": "exact",
 *     "state": "exact",
 *     "projects": "exact",
 *     "protocol.name": "exact"
 * })
 * @ApiFilter(DeletedFilter::class)
 *
 * @AdvancedRight(classIdentifier="webapp_experiment", ownerField="owner", siteAttribute="site", parentFields={"platform"})
 *
 * @Gedmo\SoftDeleteable()
 *
 * @GraphicallyDeletable()
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="experiment", schema="webapp")
 *
 * @ExperimentPatchConstraint
 */
class Experiment extends IdentifiedEntity implements BusinessObject
{
    use GraphicallyDeletableEntity;

    use HasGeometryEntity;

    use HasOwnerEntity;

    use OpenSilexEntity;

    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\NotBlank
     *
     * @UniqueAttributeInParent(parentsAttributes={"site.experiments", "platform.experiments"})
     *
     * @Groups({"design_explorer_view", "platform_full_view", "project_explorer_view", "webapp_data_view", "admin_explorer_view", "change_report", "data_entry_synthesis", "platform_synthesis", "project_synthesis", "variable_synthesis"})
     */
    private string $name = '';

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"platform_full_view", "platform_synthesis"})
     */
    private bool $individualUP = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"platform_full_view"})
     */
    private ?string $comment = null;

    /**
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"platform_full_view", "platform_synthesis"})
     */
    private \DateTime $created;

    /**
     * @Gedmo\Timestampable(on="change", field="state", value=1)
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $validated = null;

    /**
     * @ORM\OneToOne(targetEntity="Webapp\Core\Entity\Protocol", mappedBy="experiment", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @Groups({"design_explorer_view", "platform_full_view", "platform_synthesis"})
     */
    private Protocol $protocol;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="experiments")
     */
    private ?Site $site = null;

    /**
     * @var ?User the owner of the entity
     *
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "platform_synthesis"})
     */
    private ?User $owner = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Platform", inversedBy="experiments")
     *
     * @Groups({"webapp_data_view"})
     */
    private ?Platform $platform = null;

    /**
     * @var Collection<int, Block>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Block", cascade={"persist", "remove"}, mappedBy="experiment")
     *
     * @Groups({"platform_full_view"})
     */
    private Collection $blocks;

    /**
     * @var Collection<int, OutExperimentationZone>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\OutExperimentationZone", mappedBy="experiment", cascade={"persist", "remove"})
     *
     * @Groups({"platform_full_view"})
     */
    private Collection $outExperimentationZones;

    /**
     * @var Collection<int, Note>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Note", mappedBy="experimentTarget", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @Groups({"note_view"})
     *
     * @ApiSubresource()
     */
    private Collection $notes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"platform_full_view"})
     */
    private ?int $color = null;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"design_explorer_view", "platform_full_view", "platform_synthesis"})
     *
     * @EnumType(class="Webapp\Core\Enumeration\ExperimentStateEnum")
     */
    private int $state = ExperimentStateEnum::CREATED;

    /**
     * @var Collection<int, Project>
     *
     * @Groups({"platform_synthesis"})
     *
     * @ORM\ManyToMany(targetEntity="Webapp\Core\Entity\Project", mappedBy="experiments")
     */
    private Collection $projects;

    /**
     * @var Collection<int, ExperimentAttachment>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Attachment\ExperimentAttachment", mappedBy="experiment", cascade={"persist", "remove"})
     */
    private Collection $experimentAttachments;

    public function __construct()
    {
        $this->blocks = new ArrayCollection();
        $this->outExperimentationZones = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->experimentAttachments = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * @Groups({"platform_full_view", "design_explorer_view"})
     *
     * @psalm-mutation-free
     */
    public function getId(): ?int
    {
        return parent::getId();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isIndividualUP(): bool
    {
        return $this->individualUP;
    }

    /**
     * @return $this
     */
    public function setIndividualUP(bool $individualUP): self
    {
        $this->individualUP = $individualUP;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return $this
     */
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValidated(): ?\DateTime
    {
        return $this->validated;
    }

    /**
     * @return $this
     */
    public function setValidated(?\DateTime $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(Protocol $protocol): self
    {
        $this->protocol = $protocol;
        $protocol->setExperiment($this);

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): ?Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(?Site $site): self
    {
        $this->site = $site;
        $this->platform = null;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @return $this
     */
    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): ?Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(?Platform $platform): self
    {
        $this->site = null;
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return Collection<int,  Block>
     *
     * @psalm-mutation-free
     */
    public function getBlocks(): Collection
    {
        return $this->blocks;
    }

    /**
     * @param iterable<int, Block> $blocks
     *
     * @return $this
     */
    public function setBlocks(iterable $blocks): self
    {
        ArrayCollectionUtils::update($this->blocks, $blocks, function (Block $block) {
            $block->setExperiment($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addBlocks(Block $block): self
    {
        if (!$this->blocks->contains($block)) {
            $this->blocks->add($block);
            $block->setExperiment($this);
        }

        return $this;
    }

    /**
     * @return Collection<int,  Note>
     *
     * @psalm-mutation-free
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    /**
     * @param iterable<int, Note> $notes
     *
     * @return $this
     */
    public function setNotes(iterable $notes): self
    {
        ArrayCollectionUtils::update($this->notes, $notes, function (Note $note) {
            $note->setTarget($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes->add($note);
            $note->setTarget($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeNote(Note $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            $note->setTarget(null);
        }

        return $this;
    }

    /**
     * @return Collection<int, OutExperimentationZone>
     *
     * @psalm-mutation-free
     */
    public function getOutExperimentationZones(): Collection
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param iterable<int, OutExperimentationZone> $outExperimentationZones
     *
     * @return $this
     */
    public function setOutExperimentationZones(iterable $outExperimentationZones): self
    {
        ArrayCollectionUtils::update($this->outExperimentationZones, $outExperimentationZones, function (OutExperimentationZone $outExperimentationZone) {
            $outExperimentationZone->setExperiment($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if (!$this->outExperimentationZones->contains($outExperimentationZone)) {
            $this->outExperimentationZones->add($outExperimentationZone);
            $outExperimentationZone->setExperiment($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeOutExperimentationZone(OutExperimentationZone $outExperimentationZone): self
    {
        if ($this->outExperimentationZones->contains($outExperimentationZone)) {
            $this->outExperimentationZones->removeElement($outExperimentationZone);
            $outExperimentationZone->setExperiment(null);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?int
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?int $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @return $this
     */
    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection<int,  Project>
     *
     * @psalm-mutation-free
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @return $this
     */
    public function setDeletedAt(?\DateTime $deletedAt = null): self
    {
        $this->deletedAt = $deletedAt;
        if (null === $deletedAt) {
            foreach ($this->children() as $child) {
                $child->setDeletedAt($deletedAt);
            }
            foreach ($this->getNotes() as $child) {
                $child->setDeletedAt($deletedAt);
            }
            $this->getProtocol()->setDeletedAt($deletedAt);
        }

        return $this;
    }

    /**
     * @return Collection<int,  ExperimentAttachment>
     *
     * @psalm-mutation-free
     */
    public function getExperimentAttachments(): Collection
    {
        return $this->experimentAttachments;
    }

    /**
     * @param iterable<int, ExperimentAttachment> $experimentAttachments
     *
     * @return $this
     */
    public function setExperimentAttachments(iterable $experimentAttachments): self
    {
        ArrayCollectionUtils::update($this->experimentAttachments, $experimentAttachments, function (ExperimentAttachment $experimentAttachment) {
            $experimentAttachment->setExperiment($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addExperimentAttachment(ExperimentAttachment $experimentAttachment): self
    {
        if (!$this->experimentAttachments->contains($experimentAttachment)) {
            $this->experimentAttachments->add($experimentAttachment);
            $experimentAttachment->setExperiment($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeExperimentAttachment(ExperimentAttachment $experimentAttachment): self
    {
        if ($this->experimentAttachments->contains($experimentAttachment)) {
            $this->experimentAttachments->removeElement($experimentAttachment);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function parent(): ?BusinessObject
    {
        return $this->platform;
    }

    /**
     * @return (Block|OutExperimentationZone)[]
     */
    public function children(): array
    {
        return [
            ...$this->blocks->getValues(),
            ...$this->outExperimentationZones->getValues(),
        ];
    }
}
