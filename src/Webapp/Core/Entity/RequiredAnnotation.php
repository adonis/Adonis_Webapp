<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Enumeration\Annotation\EnumType;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          }
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *         "delete"={"is_granted('ROLE_PLATFORM_MANAGER')"},
 *     }
 * )
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="required_annotation", schema="webapp")
 *
 * @psalm-import-type PathLevelEnumId from PathLevelEnum
 */
class RequiredAnnotation extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Project", inversedBy="requiredAnnotations")
     */
    private ?Project $project = null;

    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     * @EnumType(class="Webapp\Core\Enumeration\AnnotationKindEnum")
     *
     * @Groups({"project_explorer_view"})
     */
    private int $type = 0;

    /**
     * @psalm-var PathLevelEnumId|''
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Groups({"project_explorer_view", "data_entry_synthesis"})
     */
    private string $level = '';

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $askWhenEntering = false;

    /**
     * @ORM\Column(type="string")
     */
    private string $comment = '';

    /**
     * @psalm-mutation-free
     */
    public function getProject(): ?Project
    {
        return $this->project;
    }

    /**
     * @return $this
     */
    public function setProject(?Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-return PathLevelEnumId|''
     *
     * @psalm-mutation-free
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @psalm-param PathLevelEnumId|'' $level
     *
     * @return $this
     */
    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isAskWhenEntering(): bool
    {
        return $this->askWhenEntering;
    }

    /**
     * @return $this
     */
    public function setAskWhenEntering(bool $askWhenEntering): self
    {
        $this->askWhenEntering = $askWhenEntering;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
