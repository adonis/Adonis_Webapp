<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={},
 *     },
 *     itemOperations={
 *          "get"={},
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"session": "exact"})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="annotation", schema="webapp")
 */
class Annotation extends IdentifiedEntity
{
    use OpenSilexEntity;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Measure", inversedBy="annotations")
     */
    private ?Measure $targetMeasure = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Individual")
     */
    private ?Individual $targetIndividual = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SurfacicUnitPlot")
     */
    private ?SurfacicUnitPlot $targetSurfacicUnitPlot = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\UnitPlot")
     */
    private ?UnitPlot $targetUnitPlot = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SubBlock")
     */
    private ?SubBlock $targetSubBlock = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Block")
     */
    private ?Block $targetBlock = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Experiment")
     */
    private ?Experiment $targetExperiment = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Session", inversedBy="annotations")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Session $session;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"webapp_data_view"})
     */
    private ?string $name = null;

    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Groups({"webapp_data_view"})
     */
    private int $type = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"webapp_data_view"})
     */
    private ?string $value = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"webapp_data_view"})
     */
    private ?string $image = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Groups({"webapp_data_view"})
     */
    private \DateTime $timestamp;

    /**
     * @var string[]
     *
     * @ORM\Column(type="array", nullable=false)
     *
     * @Groups({"webapp_data_view", "data_entry_synthesis"})
     */
    private array $categories = [];

    /**
     * @var string[]
     *
     * @ORM\Column(type="array", nullable=false)
     *
     * @Groups({"webapp_data_view"})
     */
    private array $keywords = [];

    /**
     * @return Block|Experiment|Individual|Measure|SubBlock|SurfacicUnitPlot|UnitPlot|null
     *
     * @Groups({"webapp_data_view"})
     *
     * @psalm-mutation-free
     */
    public function getTarget()
    {
        return $this->targetExperiment ?? $this->targetBlock ?? $this->targetSubBlock ?? $this->targetUnitPlot ?? $this->targetSurfacicUnitPlot ?? $this->targetIndividual ?? $this->targetMeasure;
    }

    /**
     * @Groups({"webapp_data_view", "data_entry_synthesis"})
     *
     * @psalm-mutation-free
     */
    public function getTargetType(): ?string
    {
        $target = $this->getTarget();
        if ($target instanceof Individual) {
            return PathLevelEnum::INDIVIDUAL;
        } elseif ($target instanceof UnitPlot) {
            return PathLevelEnum::UNIT_PLOT;
        } elseif ($target instanceof SubBlock) {
            return PathLevelEnum::SUB_BLOCK;
        } elseif ($target instanceof Block) {
            return PathLevelEnum::BLOCK;
        } elseif ($target instanceof Experiment) {
            return PathLevelEnum::EXPERIMENT;
        } elseif ($target instanceof SurfacicUnitPlot) {
            return PathLevelEnum::SURFACIC_UNIT_PLOT;
        } else {
            return null;
        }
    }

    /**
     * @param Block|Experiment|Individual|Measure|SubBlock|SurfacicUnitPlot|UnitPlot|null $target
     *
     * @return $this
     */
    public function setTarget($target): self
    {
        if ($target instanceof Individual) {
            $this->targetIndividual = $target;
        } elseif ($target instanceof UnitPlot) {
            $this->targetUnitPlot = $target;
        } elseif ($target instanceof SubBlock) {
            $this->targetSubBlock = $target;
        } elseif ($target instanceof Block) {
            $this->targetBlock = $target;
        } elseif ($target instanceof Experiment) {
            $this->targetExperiment = $target;
        } elseif ($target instanceof SurfacicUnitPlot) {
            $this->targetSurfacicUnitPlot = $target;
        } elseif ($target instanceof Measure) {
            $this->targetMeasure = $target;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSession(): Session
    {
        return $this->session;
    }

    /**
     * @return $this
     */
    public function setSession(Session $session): self
    {
        $this->session = $session;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @return $this
     */
    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return $this
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    /**
     * @return $this
     */
    public function setTimestamp(\DateTime $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * @param string[] $categories
     *
     * @return $this
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return string[]
     *
     * @psalm-mutation-free
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param string[] $keywords
     *
     * @return $this
     */
    public function setKeywords(array $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }
}
