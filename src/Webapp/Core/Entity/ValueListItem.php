<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="value_list_item", schema="webapp")
 */
class ValueListItem extends IdentifiedEntity
{
    /**
     * @Groups({"simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\ValueList", inversedBy="values")
     */
    private ?ValueList $list = null;

    public function __construct(string $name = '')
    {
        $this->name = $name;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getList(): ?ValueList
    {
        return $this->list;
    }

    /**
     * @return $this
     */
    public function setList(?ValueList $list): self
    {
        $this->list = $list;

        return $this;
    }
}
