<?php

namespace Webapp\Core\Entity;

use Brick\Geo\Exception\GeometryIOException;
use Brick\Geo\IO\WKTReader;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait HasGeometryEntity
{
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"edit"})
     */
    private ?string $geometry = null;

    /**
     * @psalm-mutation-free
     */
    public function getGeometry(): ?string
    {
        return $this->geometry;
    }

    /**
     * @return $this
     *
     * @throws GeometryIOException
     */
    public function setGeometry(?string $geometry): self
    {
        if ($geometry) {
            (new WKTReader())->read($geometry);
        }
        $this->geometry = $geometry;

        return $this;
    }
}
