<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *     },
 *     itemOperations={
 *          "get"={},
 *          "patch"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "denormalization_context"={"groups"={"measure_edit"}}
 *          },
 *     }
 * )
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="measure", schema="webapp")
 */
class Measure extends IdentifiedEntity
{
    use OpenSilexEntity;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\FieldMeasure", inversedBy="measures")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private FieldMeasure $formField;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"webapp_data_view", "measure_edit", "variable_synthesis", "fusion_result", "data_view_item", "graphical_measure_view"})
     */
    private ?string $value = null;

    /**
     * Define the repetition number of the variable.
     *
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Groups({"webapp_data_view", "data_view_item"})
     */
    private int $repetition = 0;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Groups({"webapp_data_view", "data_view_item"})
     */
    private \DateTime $timestamp;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\StateCode")
     *
     * @ORM\JoinColumn(nullable=true)
     *
     * @Groups({"webapp_data_view", "measure_edit", "data_entry_synthesis", "variable_synthesis", "fusion_result", "data_view_item", "graphical_measure_view"})
     */
    private ?StateCode $state = null;

    /**
     * @var Collection<int, Annotation>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Annotation", mappedBy="targetMeasure", cascade={"persist", "remove"})
     *
     * @Groups({"webapp_data_view", "data_entry_synthesis", "data_view_item"})
     */
    private Collection $annotations;

    public function __construct()
    {
        $this->annotations = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getFormField(): FieldMeasure
    {
        return $this->formField;
    }

    /**
     * @return $this
     */
    public function setFormField(FieldMeasure $formField): self
    {
        $this->formField = $formField;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getRepetition(): int
    {
        return $this->repetition;
    }

    /**
     * @return $this
     */
    public function setRepetition(int $repetition): self
    {
        $this->repetition = $repetition;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }

    /**
     * @return $this
     */
    public function setTimestamp(\DateTime $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getState(): ?StateCode
    {
        return $this->state;
    }

    /**
     * @return $this
     */
    public function setState(?StateCode $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection<int, Annotation>
     */
    public function getAnnotations(): Collection
    {
        return $this->annotations;
    }

    /**
     * @param iterable<array-key, Annotation> $annotations
     *
     * @return $this
     */
    public function setAnnotations(iterable $annotations): self
    {
        ArrayCollectionUtils::update($this->annotations, $annotations, function (Annotation $annotation) {
            $annotation->setTarget($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addAnnotation(Annotation $annotation): self
    {
        if (!$this->annotations->contains($annotation)) {
            $this->annotations->add($annotation);
            $annotation->setTarget($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeAnnotation(Annotation $annotation): self
    {
        if ($this->annotations->contains($annotation)) {
            $this->annotations->removeElement($annotation);
            $annotation->setTarget(null);
        }

        return $this;
    }
}
