<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={},
 *     },
 *     itemOperations={
 *          "get"={},
 *          "delete"={},
 *     }
 * )
 *
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"project_explorer_view"}})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="variable_connection", schema="webapp")
 */
class VariableConnection extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable", inversedBy="connectedVariables")
     *
     * @Groups({"connected_variables"})
     */
    private ?SimpleVariable $projectSimpleVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable", inversedBy="connectedVariables")
     *
     * @Groups({"connected_variables"})
     */
    private ?GeneratorVariable $projectGeneratorVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable", inversedBy="connectedVariables")
     *
     * @Groups({"connected_variables"})
     */
    private ?SemiAutomaticVariable $projectSemiAutomaticVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SimpleVariable")
     *
     * @Groups({"project_explorer_view", "connected_variables"})
     */
    private ?SimpleVariable $dataEntrySimpleVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\GeneratorVariable")
     *
     * @Groups({"project_explorer_view", "connected_variables"})
     */
    private ?GeneratorVariable $dataEntryGeneratorVariable = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SemiAutomaticVariable")
     *
     * @Groups({"project_explorer_view", "connected_variables"})
     */
    private ?SemiAutomaticVariable $dataEntrySemiAutomaticVariable = null;

    /**
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     *
     * @psalm-mutation-free
     */
    public function getProjectVariable(): ?AbstractVariable
    {
        return $this->projectSimpleVariable ?? $this->projectGeneratorVariable ?? $this->projectSemiAutomaticVariable;
    }

    /**
     * @return SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null
     *
     * @psalm-mutation-free
     */
    public function getDataEntryVariable(): ?AbstractVariable
    {
        return $this->dataEntrySimpleVariable ?? $this->dataEntryGeneratorVariable ?? $this->dataEntrySemiAutomaticVariable;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProjectSimpleVariable(): ?SimpleVariable
    {
        return $this->projectSimpleVariable;
    }

    /**
     * @return $this
     */
    public function setProjectSimpleVariable(?SimpleVariable $projectSimpleVariable): self
    {
        $this->projectSimpleVariable = $projectSimpleVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProjectGeneratorVariable(): ?GeneratorVariable
    {
        return $this->projectGeneratorVariable;
    }

    /**
     * @return $this
     */
    public function setProjectGeneratorVariable(?GeneratorVariable $projectGeneratorVariable): self
    {
        $this->projectGeneratorVariable = $projectGeneratorVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProjectSemiAutomaticVariable(): ?SemiAutomaticVariable
    {
        return $this->projectSemiAutomaticVariable;
    }

    /**
     * @return $this
     */
    public function setProjectSemiAutomaticVariable(?SemiAutomaticVariable $projectSemiAutomaticVariable): self
    {
        $this->projectSemiAutomaticVariable = $projectSemiAutomaticVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDataEntrySimpleVariable(): ?SimpleVariable
    {
        return $this->dataEntrySimpleVariable;
    }

    /**
     * @return $this
     */
    public function setDataEntrySimpleVariable(?SimpleVariable $dataEntrySimpleVariable): self
    {
        $this->dataEntrySimpleVariable = $dataEntrySimpleVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDataEntryGeneratorVariable(): ?GeneratorVariable
    {
        return $this->dataEntryGeneratorVariable;
    }

    /**
     * @return $this
     */
    public function setDataEntryGeneratorVariable(?GeneratorVariable $dataEntryGeneratorVariable): self
    {
        $this->dataEntryGeneratorVariable = $dataEntryGeneratorVariable;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDataEntrySemiAutomaticVariable(): ?SemiAutomaticVariable
    {
        return $this->dataEntrySemiAutomaticVariable;
    }

    /**
     * @return $this
     */
    public function setDataEntrySemiAutomaticVariable(?SemiAutomaticVariable $dataEntrySemiAutomaticVariable): self
    {
        $this->dataEntrySemiAutomaticVariable = $dataEntrySemiAutomaticVariable;

        return $this;
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $variable
     *
     * @return $this
     */
    public function setProjectVariable(AbstractVariable $variable): self
    {
        if ($variable instanceof SimpleVariable) {
            $this->setProjectSimpleVariable($variable);
        } elseif ($variable instanceof SemiAutomaticVariable) {
            $this->setProjectSemiAutomaticVariable($variable);
        } elseif ($variable instanceof GeneratorVariable) {
            $this->setProjectGeneratorVariable($variable);
        }

        return $this;
    }

    /**
     * @param SimpleVariable|GeneratorVariable|SemiAutomaticVariable|null $variable
     *
     * @return $this
     */
    public function setDataEntryVariable(AbstractVariable $variable): self
    {
        if ($variable instanceof SimpleVariable) {
            $this->setDataEntrySimpleVariable($variable);
        } elseif ($variable instanceof SemiAutomaticVariable) {
            $this->setDataEntrySemiAutomaticVariable($variable);
        } elseif ($variable instanceof GeneratorVariable) {
            $this->setDataEntryGeneratorVariable($variable);
        }

        return $this;
    }
}
