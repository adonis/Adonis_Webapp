<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *          }
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *          },
 *         "delete"={"security"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())"},
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"nature": "exact", "site": "exact"})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="oez_nature", schema="webapp")
 */
class OezNature extends IdentifiedEntity
{
    /**
     * @UniqueAttributeInParent(parentsAttributes={"site.oezNatures"})
     *
     * @Groups({"design_explorer_view", "platform_full_view"})
     *
     * @ORM\Column(type="string")
     */
    private string $nature = '';

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $color = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"platform_full_view"})
     */
    private ?int $texture = null;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="oezNatures")
     */
    private Site $site;

    /**
     * @var Collection<int, OutExperimentationZone>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\OutExperimentationZone", mappedBy="nature")
     */
    private Collection $outExperimentationZones;

    public function __construct()
    {
        $this->outExperimentationZones = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getNature(): string
    {
        return $this->nature;
    }

    /**
     * @return $this
     */
    public function setNature(string $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): int
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(int $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTexture(): ?int
    {
        return $this->texture;
    }

    /**
     * @return $this
     */
    public function setTexture(?int $texture): self
    {
        $this->texture = $texture;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return Collection<int, OutExperimentationZone>
     */
    public function getOutExperimentationZones(): Collection
    {
        return $this->outExperimentationZones;
    }

    /**
     * @param iterable<array-key, OutExperimentationZone> $outExperimentationZones
     *
     * @return $this
     */
    public function setOutExperimentationZones(iterable $outExperimentationZones): self
    {
        ArrayCollectionUtils::update($this->outExperimentationZones, $outExperimentationZones);

        return $this;
    }
}
