<?php

namespace Webapp\Core\Entity\Move;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\ApiOperation\CreateBusinessObjectOperation;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\OezNature;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "create"={
 *              "controller"=CreateBusinessObjectOperation::class,
 *              "method"="PATCH",
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Create one or more buisness object within the same parent",
 *                  "description": "Fill a rectangle with objects"
 *              },
 *              "normalization_context"={"groups"={"platform_full_view"}}
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }
 *     }
 * )
 */
class Create
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var UnitPlot|SubBlock|Block|Experiment|null
     */
    private $parent;

    /**
     * @var Block[]|SubBlock[]|UnitPlot[]|Individual[]|SurfacicUnitPlot[]
     *
     * @Groups({"platform_full_view"})
     */
    private array $createdObjects = [];

    private ?Treatment $treatment = null;

    private bool $useSubBlock = false;

    private ?OezNature $nature = null;

    private int $x1 = 0;

    private int $x2 = 0;

    private int $y1 = 0;

    private int $y2 = 0;

    private string $pathLevel = '';

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Block|Experiment|SubBlock|UnitPlot|null
     *
     * @psalm-mutation-free
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Block|Experiment|SubBlock|UnitPlot|null $parent
     *
     * @return $this
     */
    public function setParent($parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Block[]|SubBlock[]|UnitPlot[]|Individual[]|SurfacicUnitPlot[]
     *
     * @psalm-mutation-free
     */
    public function getCreatedObjects(): array
    {
        return $this->createdObjects;
    }

    /**
     * @param Block[]|SubBlock[]|UnitPlot[]|Individual[]|SurfacicUnitPlot[] $createdObjects
     *
     * @return $this
     */
    public function setCreatedObjects(array $createdObjects): self
    {
        $this->createdObjects = $createdObjects;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTreatment(): ?Treatment
    {
        return $this->treatment;
    }

    /**
     * @return $this
     */
    public function setTreatment(?Treatment $treatment): self
    {
        $this->treatment = $treatment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isUseSubBlock(): bool
    {
        return $this->useSubBlock;
    }

    /**
     * @return $this
     */
    public function setUseSubBlock(bool $useSubBlock): self
    {
        $this->useSubBlock = $useSubBlock;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getX1(): int
    {
        return $this->x1;
    }

    /**
     * @return $this
     */
    public function setX1(int $x1): self
    {
        $this->x1 = $x1;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getX2(): int
    {
        return $this->x2;
    }

    /**
     * @return $this
     */
    public function setX2(int $x2): self
    {
        $this->x2 = $x2;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY1(): int
    {
        return $this->y1;
    }

    /**
     * @return $this
     */
    public function setY1(int $y1): self
    {
        $this->y1 = $y1;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY2(): int
    {
        return $this->y2;
    }

    /**
     * @return $this
     */
    public function setY2(int $y2): self
    {
        $this->y2 = $y2;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @return $this
     */
    public function setPathLevel(string $pathLevel): self
    {
        $this->pathLevel = $pathLevel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getNature(): ?OezNature
    {
        return $this->nature;
    }

    /**
     * @return $this
     */
    public function setNature(?OezNature $nature): self
    {
        $this->nature = $nature;

        return $this;
    }
}
