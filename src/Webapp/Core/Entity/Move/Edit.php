<?php

namespace Webapp\Core\Entity\Move;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\EditBusinessObjectOperation;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "move"={
 *              "controller"=EditBusinessObjectOperation::class,
 *              "method"="PATCH",
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Edit one or more buisness object with the same delta",
 *                  "description": "Edit all items with given IRI to a new position"
 *              }
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }
 *     }
 * )
 */
class Edit
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var string[][]
     */
    private array $objectIris = [];
    private ?int $dx = null;
    private ?int $dy = null;

    private ?int $color = null;

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string[][]
     *
     * @psalm-mutation-free
     */
    public function getObjectIris(): array
    {
        return $this->objectIris;
    }

    /**
     * @param string[][] $objectIris
     *
     * @return $this
     */
    public function setObjectIris(array $objectIris): self
    {
        $this->objectIris = $objectIris;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDx(): ?int
    {
        return $this->dx;
    }

    /**
     * @return $this
     */
    public function setDx(?int $dx): self
    {
        $this->dx = $dx;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDy(): ?int
    {
        return $this->dy;
    }

    /**
     * @return $this
     */
    public function setDy(?int $dy): self
    {
        $this->dy = $dy;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?int
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?int $color): self
    {
        $this->color = $color;

        return $this;
    }
}
