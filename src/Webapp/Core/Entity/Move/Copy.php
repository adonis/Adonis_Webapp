<?php

namespace Webapp\Core\Entity\Move;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\ApiOperation\CopyBusinessObjectOperation;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\UnitPlot;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "copy"={
 *              "controller"=CopyBusinessObjectOperation::class,
 *              "method"="PATCH",
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Copy one or more buisness object with the same delta",
 *                  "description": "Copy all items with given IRI to a new position and a new parent"
 *              },
 *              "normalization_context"={"groups"={"platform_full_view"}}
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }
 *     }
 * )
 */
class Copy
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var string[][]
     */
    private array $objectIris = [];
    private ?int $dx = null;
    private ?int $dy = null;
    /**
     * @var UnitPlot|SubBlock|Block|Experiment|null
     */
    private $parent;

    /**
     * @var Block[]|SubBlock[]|UnitPlot[]|Individual[]|SurfacicUnitPlot[]
     *
     * @Groups({"platform_full_view"})
     */
    private array $createdObjects = [];

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string[][]
     *
     * @psalm-mutation-free
     */
    public function getObjectIris(): array
    {
        return $this->objectIris;
    }

    /**
     * @param string[][] $objectIris
     *
     * @return $this
     */
    public function setObjectIris(array $objectIris): self
    {
        $this->objectIris = $objectIris;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDx(): ?int
    {
        return $this->dx;
    }

    /**
     * @return $this
     */
    public function setDx(?int $dx): self
    {
        $this->dx = $dx;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDy(): ?int
    {
        return $this->dy;
    }

    /**
     * @return $this
     */
    public function setDy(?int $dy): self
    {
        $this->dy = $dy;

        return $this;
    }

    /**
     * @return Block|Experiment|SubBlock|UnitPlot|null
     *
     * @psalm-mutation-free
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Block|Experiment|SubBlock|UnitPlot|null $parent
     *
     * @return $this
     */
    public function setParent($parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Block[]|Individual[]|SubBlock[]|SurfacicUnitPlot[]|UnitPlot[]
     *
     * @psalm-mutation-free
     */
    public function getCreatedObjects(): array
    {
        return $this->createdObjects;
    }

    /**
     * @param Block[]|Individual[]|SubBlock[]|SurfacicUnitPlot[]|UnitPlot[] $createdObjects
     *
     * @return $this
     */
    public function setCreatedObjects(array $createdObjects): self
    {
        $this->createdObjects = $createdObjects;

        return $this;
    }
}
