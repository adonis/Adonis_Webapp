<?php

namespace Webapp\Core\Entity\Move;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\DeleteBusinessObjectOperation;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "delete"={
 *              "controller"=DeleteBusinessObjectOperation::class,
 *              "method"="PATCH",
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Delete one or more buisness object given in parameter",
 *                  "description": "Delete all items with given IRI"
 *              }
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }
 *     }
 * )
 */
class Delete
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var string[][]
     */
    private array $objectIris = [];

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string[][]
     *
     * @psalm-mutation-free
     */
    public function getObjectIris(): array
    {
        return $this->objectIris;
    }

    /**
     * @param string[][] $objectIris
     *
     * @return $this
     */
    public function setObjectIris(array $objectIris): self
    {
        $this->objectIris = $objectIris;

        return $this;
    }
}
