<?php

namespace Webapp\Core\Entity\Move;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\PlaceExperimentOperation;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Platform;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "move"={
 *              "controller"=PlaceExperimentOperation::class,
 *              "method"="PATCH",
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Place an experiment inside an other platform",
 *                  "description": "Move all items inside the experiment and link it to given platform"
 *              }
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }
 *     }
 * )
 */
class PlaceExperiment
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    private Experiment $experiment;
    private int $dx = 0;
    private int $dy = 0;

    private ?Platform $platform = null;

    public function __construct(Experiment $experiment)
    {
        $this->experiment = $experiment;
    }

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperiment(): Experiment
    {
        return $this->experiment;
    }

    /**
     * @return $this
     */
    public function setExperiment(Experiment $experiment): self
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDx(): int
    {
        return $this->dx;
    }

    /**
     * @return $this
     */
    public function setDx(int $dx): self
    {
        $this->dx = $dx;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDy(): int
    {
        return $this->dy;
    }

    /**
     * @return $this
     */
    public function setDy(int $dy): self
    {
        $this->dy = $dy;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): ?Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(?Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }
}
