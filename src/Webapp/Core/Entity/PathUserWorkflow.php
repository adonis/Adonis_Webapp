<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={}
 *     },
 *     itemOperations={
 *         "get"={},
 *         "patch"={},
 *         "delete"={}
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"pathBase": "exact"})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="path_user_workflow", schema="webapp")
 */
class PathUserWorkflow extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private array $workflow = [];

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     *
     * @Assert\NotBlank
     */
    private User $user;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\PathBase", inversedBy="userPaths")
     *
     * @Assert\NotBlank
     */
    private PathBase $pathBase;

    /**
     * @psalm-mutation-free
     */
    public function getWorkflow(): array
    {
        return $this->workflow;
    }

    /**
     * @return $this
     */
    public function setWorkflow(array $workflow): self
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * @Groups({"project_explorer_view"})
     *
     * @psalm-mutation-free
     */
    public function getUsername(): string
    {
        return $this->user->getUsername();
    }

    /**
     * @psalm-mutation-free
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPathBase(): PathBase
    {
        return $this->pathBase;
    }

    /**
     * @return $this
     */
    public function setPathBase(PathBase $pathBase): self
    {
        $this->pathBase = $pathBase;

        return $this;
    }
}
