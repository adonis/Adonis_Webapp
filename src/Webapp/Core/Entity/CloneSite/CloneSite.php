<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity\CloneSite;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\Site;
use Shared\Authentication\Entity\User;
use Shared\Exception\NotInitializedPropertyException;
use Shared\FileManagement\Entity\UserLinkedJob;
use Webapp\Core\ApiOperation\CloneSiteOperation;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "controller"=CloneSiteOperation::class,
 *              "security"="is_granted('ROLE_SITE_ADMIN')",
 *              "openapi_context"={
 *                  "summary": "Clone a site",
 *                  "description": "Create a copy of the given site giving him a new name and wich obects to duplicate"
 *              },
 *          },
 *     },
 *     itemOperations={
 *         "get"={ "is_granted('ROLE_USER') && security"="object.getUser() == user" },
 *         "put"={ "is_granted('ROLE_USER') && security"="object.getUser() == user" },
 *         "delete" = { "security"= "is_granted('ROLE_USER') && object.getUser() == user" }
 *     }
 * )
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="clone_site", schema="webapp")
 */
class CloneSite extends UserLinkedJob
{
    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site")
     *
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private ?Site $originSite = null;

    /**
     * @ORM\Column(type="string")
     */
    private string $name = '';

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $cloneLibraries = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $cloneUsers = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $clonePlatforms = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $cloneProjects = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $cloneEntries = false;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", cascade={"persist"})
     *
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?Site $result = null;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User", cascade={"persist"})
     *
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private ?User $admin = null;

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOriginSite(): Site
    {
        if (null === $this->originSite) {
            throw new NotInitializedPropertyException(self::class, 'originSite');
        }

        return $this->originSite;
    }

    /**
     * @return $this
     */
    public function setOriginSite(Site $originSite): self
    {
        $this->originSite = $originSite;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isCloneLibraries(): bool
    {
        return $this->cloneLibraries;
    }

    /**
     * @return $this
     */
    public function setCloneLibraries(bool $cloneLibraries): self
    {
        $this->cloneLibraries = $cloneLibraries;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isCloneUsers(): bool
    {
        return $this->cloneUsers;
    }

    /**
     * @return $this
     */
    public function setCloneUsers(bool $cloneUsers): self
    {
        $this->cloneUsers = $cloneUsers;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isClonePlatforms(): bool
    {
        return $this->clonePlatforms;
    }

    /**
     * @return $this
     */
    public function setClonePlatforms(bool $clonePlatforms): self
    {
        $this->clonePlatforms = $clonePlatforms;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isCloneProjects(): bool
    {
        return $this->cloneProjects;
    }

    /**
     * @return $this
     */
    public function setCloneProjects(bool $cloneProjects): self
    {
        $this->cloneProjects = $cloneProjects;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isCloneEntries(): bool
    {
        return $this->cloneEntries;
    }

    /**
     * @return $this
     */
    public function setCloneEntries(bool $cloneEntries): self
    {
        $this->cloneEntries = $cloneEntries;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResult(): ?Site
    {
        return $this->result;
    }

    /**
     * @return $this
     */
    public function setResult(?Site $result): self
    {
        $this->result = $result;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    /**
     * @return $this
     */
    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
}
