<?php

namespace Webapp\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Enumeration\Annotation\EnumType;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="path_base_level_algorithm", schema="webapp")
 */
class PathLevelAlgorithm extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"variable_synthesis"})
     *
     * @EnumType(class="Webapp\Core\Enumeration\PathLevelEnum")
     */
    private string $pathLevel = '';

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"variable_synthesis"})
     *
     * @EnumType(class="Webapp\Core\Enumeration\PossibleMoveEnum")
     */
    private string $move;

    /**
     * @Groups({"variable_synthesis"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $startPoint = null; // TODO PossibleStartPointEnum

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private array $orderedIris = [];

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\PathBase", inversedBy="pathLevelAlgorithms")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ?PathBase $pathBase;

    public function __construct(string $move, string $pathLevel)
    {
        $this->move = $move; // TODO delete when https://github.com/api-platform/core/issues/3974 is solved
        $this->pathLevel = $pathLevel; // TODO delete when https://github.com/api-platform/core/issues/3974 is solved
    }

    /**
     * @psalm-mutation-free
     */
    public function getPathLevel(): string
    {
        return $this->pathLevel;
    }

    /**
     * @return $this
     */
    public function setPathLevel(string $pathLevel): self
    {
        $this->pathLevel = $pathLevel;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getMove(): string
    {
        return $this->move;
    }

    /**
     * @return $this
     */
    public function setMove(string $move): self
    {
        $this->move = $move;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getStartPoint(): ?string
    {
        return $this->startPoint;
    }

    /**
     * @return $this
     */
    public function setStartPoint(?string $startPoint): self
    {
        $this->startPoint = $startPoint;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getOrderedIris(): array
    {
        return $this->orderedIris;
    }

    /**
     * @return $this
     */
    public function setOrderedIris(array $orderedIris): self
    {
        $this->orderedIris = $orderedIris;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPathBase(): ?PathBase
    {
        return $this->pathBase;
    }

    /**
     * @return $this
     */
    public function setPathBase(?PathBase $pathBase): self
    {
        $this->pathBase = $pathBase;

        return $this;
    }
}
