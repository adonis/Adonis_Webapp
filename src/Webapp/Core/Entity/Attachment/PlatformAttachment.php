<?php

namespace Webapp\Core\Entity\Attachment;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Application\Entity\File;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\RightManagement\Annotation\AdvancedRight;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Entity\Platform;

/**
 * @ApiResource(
 *     normalizationContext={
 *         "groups"={"attachment_read", "id_read"}
 *     },
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *          "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *     }
 *
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"platform": "exact"})
 *
 * @AdvancedRight(parentFields={"platform"})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="attachment_platform", schema="webapp")
 */
class PlatformAttachment extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Platform", inversedBy="platformAttachments")
     */
    private Platform $platform;

    /**
     * @ORM\OneToOne(targetEntity="Shared\Application\Entity\File", cascade={"persist", "remove"})
     *
     * @Groups({"attachment_read", "platform_post"})
     */
    private File $file;

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }
}
