<?php

namespace Webapp\Core\Entity\Attachment;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Application\Entity\File;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\RightManagement\Annotation\AdvancedRight;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Entity\Protocol;

/**
 * @ApiResource(
 *     normalizationContext={
 *         "groups"={"attachment_read", "id_read"}
 *     },
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *          "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *     }
 *
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"protocol": "exact"})
 *
 * @AdvancedRight(parentFields={"protocol"})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="attachment_protocol", schema="webapp")
 */
class ProtocolAttachment extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Protocol", inversedBy="protocolAttachments")
     */
    private Protocol $protocol;

    /**
     * @ORM\OneToOne(targetEntity="Shared\Application\Entity\File", cascade={"persist", "remove"})
     *
     * @Groups({"attachment_read"})
     */
    private File $file;

    /**
     * @psalm-mutation-free
     */
    public function getProtocol(): Protocol
    {
        return $this->protocol;
    }

    /**
     * @return $this
     */
    public function setProtocol(Protocol $protocol): ProtocolAttachment
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function setFile(File $file): ProtocolAttachment
    {
        $this->file = $file;

        return $this;
    }
}
