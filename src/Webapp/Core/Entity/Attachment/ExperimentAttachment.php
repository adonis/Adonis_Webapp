<?php

namespace Webapp\Core\Entity\Attachment;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Application\Entity\File;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Entity\Experiment;

/**
 * @ApiResource(
 *     normalizationContext={
 *         "groups"={"attachment_read", "id_read"}
 *     },
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *          "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')"
 *          },
 *     }
 *
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"experiment": "exact"})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="attachment_experiment", schema="webapp")
 */
class ExperimentAttachment extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Experiment", inversedBy="experimentAttachments")
     */
    private Experiment $experiment;

    /**
     * @ORM\OneToOne(targetEntity="Shared\Application\Entity\File", cascade={"persist", "remove"})
     *
     * @Groups({"attachment_read"})
     */
    private File $file;

    /**
     * @psalm-mutation-free
     */
    public function getExperiment(): Experiment
    {
        return $this->experiment;
    }

    /**
     * @return $this
     */
    public function setExperiment(Experiment $experiment): self
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getFile(): File
    {
        return $this->file;
    }

    /**
     * @return $this
     */
    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }
}
