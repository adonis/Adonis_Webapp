<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     },
 *     itemOperations={
 *         "get"={},
 *         "patch"={}
 *     },
 *     normalizationContext={"groups"={"webapp_data_view", "id_read"}},
 *     denormalizationContext={"groups"={"note_edit"}}
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"target": "exact"})
 *
 * @Gedmo\SoftDeleteable()
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="note", schema="webapp")
 */
class Note extends IdentifiedEntity
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"webapp_data_view", "note_edit"})
     */
    private ?string $text = null;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"webapp_data_view", "note_edit"})
     */
    private bool $deleted = false;

    /**
     * @ORM\Column(type="datetime")
     *
     * @Groups({"webapp_data_view"})
     */
    private \DateTime $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\User")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private User $creator;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Individual", inversedBy="notes")
     */
    private ?Individual $individualTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\UnitPlot", inversedBy="notes")
     */
    private ?UnitPlot $unitPlotTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SurfacicUnitPlot", inversedBy="notes")
     */
    private ?SurfacicUnitPlot $surfacicUnitPlotTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Block", inversedBy="notes")
     */
    private ?Block $blockTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SubBlock", inversedBy="notes")
     */
    private ?SubBlock $subBlockTarget = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Experiment", inversedBy="notes")
     */
    private ?Experiment $experimentTarget = null;

    public function __construct()
    {
        $this->creationDate = new \DateTime();
    }

    /**
     * @return Block|Experiment|Individual|SubBlock|UnitPlot|SurfacicUnitPlot|null
     *
     * @psalm-mutation-free
     */
    public function getTarget()
    {
        return $this->individualTarget ?? $this->unitPlotTarget ?? $this->subBlockTarget ?? $this->blockTarget ?? $this->experimentTarget ?? $this->surfacicUnitPlotTarget;
    }

    /**
     * @Groups({"webapp_data_view"})
     *
     * @psalm-mutation-free
     */
    public function getCreatorLogin(): string
    {
        return $this->creator->getUsername();
    }

    /**
     * @Groups({"webapp_data_view"})
     *
     * @psalm-mutation-free
     */
    public function getCreatorAvatar(): ?string
    {
        return $this->creator->getAvatar();
    }

    /**
     * @psalm-mutation-free
     */
    public function getTargetType(): ?string
    {
        $target = $this->getTarget();
        if ($target instanceof Individual) {
            return PathLevelEnum::INDIVIDUAL;
        } elseif ($target instanceof UnitPlot) {
            return PathLevelEnum::UNIT_PLOT;
        } elseif ($target instanceof SubBlock) {
            return PathLevelEnum::SUB_BLOCK;
        } elseif ($target instanceof Block) {
            return PathLevelEnum::BLOCK;
        } elseif ($target instanceof Experiment) {
            return PathLevelEnum::EXPERIMENT;
        } elseif ($target instanceof SurfacicUnitPlot) {
            return PathLevelEnum::SURFACIC_UNIT_PLOT;
        } else {
            return null;
        }
    }

    /**
     * @return $this
     */
    public function setTarget($target): self
    {
        if ($target instanceof Individual) {
            $this->individualTarget = $target;
        } elseif ($target instanceof UnitPlot) {
            $this->unitPlotTarget = $target;
        } elseif ($target instanceof SubBlock) {
            $this->subBlockTarget = $target;
        } elseif ($target instanceof Block) {
            $this->blockTarget = $target;
        } elseif ($target instanceof Experiment) {
            $this->experimentTarget = $target;
        } elseif ($target instanceof SurfacicUnitPlot) {
            $this->surfacicUnitPlotTarget = $target;
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return $this
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return $this
     */
    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }

    /**
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreator(): User
    {
        return $this->creator;
    }

    /**
     * @return $this
     */
    public function setCreator(User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }
}
