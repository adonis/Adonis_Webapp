<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Enumeration\Annotation\EnumType;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={}
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="change_report", schema="webapp")
 */
class ChangeReport extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Individual")
     *
     * @Groups({"change_report"})
     */
    private ?Individual $involvedIndividual = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\SurfacicUnitPlot")
     *
     * @Groups({"change_report"})
     */
    private ?SurfacicUnitPlot $involvedSurfacicUnitPlot = null;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Groups({"change_report", "data_entry_synthesis"})
     */
    private bool $aproved = false;

    /**
     * @ORM\Column(type="string")
     *
     * @EnumType(class="Webapp\Core\Enumeration\ChangeTypeEnum")
     *
     * @Groups({"change_report", "data_entry_synthesis"})
     */
    private string $changeType = '';

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Groups({"change_report"})
     */
    private \DateTime $lastChangeDate;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @Groups({"change_report"})
     */
    private \DateTime $changeDate;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\ProjectData", inversedBy="changeReports")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private ProjectData $projectData;

    public function __construct()
    {
        $this->lastChangeDate = new \DateTime();
        $this->changeDate = new \DateTime();
    }

    /**
     * @psalm-mutation-free
     */
    public function getInvolvedIndividual(): ?Individual
    {
        return $this->involvedIndividual;
    }

    /**
     * @return $this
     */
    public function setInvolvedIndividual(?Individual $involvedIndividual): self
    {
        $this->involvedIndividual = $involvedIndividual;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getInvolvedSurfacicUnitPlot(): ?SurfacicUnitPlot
    {
        return $this->involvedSurfacicUnitPlot;
    }

    /**
     * @return $this
     */
    public function setInvolvedSurfacicUnitPlot(?SurfacicUnitPlot $involvedSurfacicUnitPlot): self
    {
        $this->involvedSurfacicUnitPlot = $involvedSurfacicUnitPlot;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isAproved(): bool
    {
        return $this->aproved;
    }

    /**
     * @return $this
     */
    public function setAproved(bool $aproved): self
    {
        $this->aproved = $aproved;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getChangeType(): string
    {
        return $this->changeType;
    }

    /**
     * @return $this
     */
    public function setChangeType(string $changeType): self
    {
        $this->changeType = $changeType;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getProjectData(): ProjectData
    {
        return $this->projectData;
    }

    /**
     * @return $this
     */
    public function setProjectData(ProjectData $projectData): self
    {
        $this->projectData = $projectData;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getChangeDate(): \DateTime
    {
        return $this->changeDate;
    }

    /**
     * @return $this
     */
    public function setChangeDate(\DateTime $changeDate): self
    {
        $this->changeDate = $changeDate;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLastChangeDate(): \DateTime
    {
        return $this->lastChangeDate;
    }

    /**
     * @return $this
     */
    public function setLastChangeDate(\DateTime $lastChangeDate): self
    {
        $this->lastChangeDate = $lastChangeDate;

        return $this;
    }
}
