<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity()
 *
 * @ORM\Table(name="variable_scale_item", schema="webapp")
 */
class VariableScaleItem extends IdentifiedEntity
{
    /**
     * @Groups({"webapp_data_view", "simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="integer")
     */
    private int $value;

    /**
     * @Groups({"webapp_data_view", "simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $text = null;

    /**
     * Picture as base64 URL (sample : 'data:image/jpg;base64,picture_content_base64_encoded').
     *
     * @Groups({"simple_variable_get", "simple_variable_post"})
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $pic = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\VariableScale", inversedBy="values")
     */
    private ?VariableScale $scale = null;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @psalm-mutation-free
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @return $this
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @return $this
     */
    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string|null picture as base64 URL (sample : 'data:image/jpg;base64,picture_content_base64_encoded')
     *
     * @psalm-mutation-free
     */
    public function getPic(): ?string
    {
        return $this->pic;
    }

    /**
     * @param string|null $pic picture as base64 URL (sample : 'data:image/jpg;base64,picture_content_base64_encoded')
     *
     * @return $this
     */
    public function setPic(?string $pic): self
    {
        $this->pic = $pic;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getScale(): ?VariableScale
    {
        return $this->scale;
    }

    /**
     * @return $this
     */
    public function setScale(?VariableScale $scale): self
    {
        $this->scale = $scale;

        return $this;
    }
}
