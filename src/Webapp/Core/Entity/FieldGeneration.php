<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2020
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *     },
 *     itemOperations={
 *          "get"={},
 *     }
 * )
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="field_generation", schema="webapp")
 */
class FieldGeneration extends IdentifiedEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="FieldMeasure", inversedBy="fieldGenerations")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private FieldMeasure $formField;

    /**
     * @Groups({"webapp_data_view", "data_view_item"})
     *
     * @ORM\Column(type="integer")
     */
    private int $index = 0;

    /**
     * @Groups({"webapp_data_view", "data_view_item"})
     *
     * @ORM\Column(type="string")
     */
    private string $prefix = '';

    /**
     * @Groups({"webapp_data_view", "data_view_item"})
     *
     * @ORM\Column(type="boolean")
     */
    private bool $numeralIncrement = false;

    /**
     * @var Collection<int, FieldMeasure>
     *
     * @Groups({"webapp_data_view", "data_view_item"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\FieldMeasure", mappedBy="fieldParent", cascade={"persist", "remove", "detach"})
     */
    private Collection $children;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getFormField(): FieldMeasure
    {
        return $this->formField;
    }

    /**
     * @return $this
     */
    public function setFormField(FieldMeasure $formField): self
    {
        $this->formField = $formField;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndex(): int
    {
        return $this->index;
    }

    /**
     * @return $this
     */
    public function setIndex(int $index): self
    {
        $this->index = $index;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * @return $this
     */
    public function setPrefix(string $prefix): self
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function isNumeralIncrement(): bool
    {
        return $this->numeralIncrement;
    }

    /**
     * @return $this
     */
    public function setNumeralIncrement(bool $numeralIncrement): self
    {
        $this->numeralIncrement = $numeralIncrement;

        return $this;
    }

    /**
     * @return Collection<int,  FieldMeasure>
     *
     * @psalm-mutation-free
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * @return $this
     */
    public function addChild(FieldMeasure $formField): self
    {
        if (!$this->children->contains($formField)) {
            $this->children->add($formField);
            $formField->setFieldParent($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeChild(FieldMeasure $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
        }

        return $this;
    }
}
