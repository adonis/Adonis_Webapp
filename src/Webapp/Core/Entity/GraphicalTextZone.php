<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\RightManagement\Annotation\AdvancedRight;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={}
 *     },
 *     itemOperations={
 *          "get"={},
 *          "delete"={},
 *          "patch"={}
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"platform": "exact"})
 *
 * @AdvancedRight(parentFields={"platform"})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="graphical_text_zone", schema="webapp")
 */
class GraphicalTextZone extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="text")
     *
     * @Groups({"platform_full_view"})
     */
    private string $text = '';

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Groups({"platform_full_view"})
     */
    private ?int $color = null;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $x = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $y = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $width = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $height = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $size = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Platform", inversedBy="graphicalTextZones")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Platform $platform;

    /**
     * @psalm-mutation-free
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return $this
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getColor(): ?int
    {
        return $this->color;
    }

    /**
     * @return $this
     */
    public function setColor(?int $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return $this
     */
    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return $this
     */
    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return $this
     */
    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }
}
