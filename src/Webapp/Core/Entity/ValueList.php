<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Webapp\Core\ApiOperation\UpdateValueListOperation;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('POST_VALUE_LIST', object)",
 *          },
 *     },
 *     itemOperations={
 *          "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *          "patch"={
 *              "security_post_denormalize"="is_granted('POST_VALUE_LIST', object)"
 *          },
 *          "update"={
 *              "controller"=UpdateValueListOperation::class,
 *              "method"="PATCH",
 *              "path"="value_lists/{id}/update",
 *              "security"="is_granted('ROLE_SITE_ADMIN')",
 *              "openapi_context"={
 *                  "summary": "Update project value list items",
 *                  "description": "Update the project value list with items in the provided list"
 *              },
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())"
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"site": "exact"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={"project_explorer_view"}})
 *
 * @ORM\Entity()
 *
 * @ORM\Table(name="value_list", schema="webapp")
 */
class ValueList extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"project_explorer_view", "simple_variable_get", "simple_variable_post", "data_explorer_view"})
     */
    private string $name = '';

    /**
     * @var Collection<int, ValueListItem>
     *
     * @Groups({"simple_variable_get", "simple_variable_post"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\ValueListItem", mappedBy="list", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private Collection $values;

    /**
     * @ORM\ManyToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="valueLists")
     */
    private ?Site $site = null;

    /**
     * @ORM\OneToOne(targetEntity="Webapp\Core\Entity\SimpleVariable", inversedBy="valueList")
     */
    private ?SimpleVariable $variable = null;

    public function __construct()
    {
        $this->values = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int,  ValueListItem>
     *
     * @psalm-mutation-free
     */
    public function getValues(): Collection
    {
        return $this->values;
    }

    /**
     * @param iterable<int,  ValueListItem> $values
     *
     * @return $this
     */
    public function setValues(iterable $values): self
    {
        ArrayCollectionUtils::update($this->values, $values, function (ValueListItem $valueListItem) {
            $valueListItem->setList($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addValue(ValueListItem $value): self
    {
        if (!$this->values->contains($value)) {
            $this->values->add($value);
            $value->setList($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeValue(ValueListItem $value): self
    {
        if ($this->values->contains($value)) {
            $this->values->removeElement($value);
            $value->setList(null);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): ?Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(?Site $site): self
    {
        $this->site = $site;
        $this->variable = null;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getVariable(): ?SimpleVariable
    {
        return $this->variable;
    }

    /**
     * @return $this
     */
    public function setVariable(?SimpleVariable $variable): self
    {
        $this->variable = $variable;
        $this->site = null;

        return $this;
    }
}
