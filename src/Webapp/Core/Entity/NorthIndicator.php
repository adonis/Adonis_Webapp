<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\RightManagement\Annotation\AdvancedRight;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={}
 *     },
 *     itemOperations={
 *          "get"={},
 *          "delete"={},
 *          "patch"={}
 *     }
 * )
 *
 * @AdvancedRight(parentFields={"platform"})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="north_indicator", schema="webapp")
 */
class NorthIndicator extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $orientation = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $x = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $y = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $width = 0;

    /**
     * @ORM\Column(type="integer")
     *
     * @Groups({"platform_full_view"})
     */
    private int $height = 0;

    /**
     * @ORM\OneToOne(targetEntity="Webapp\Core\Entity\Platform", inversedBy="northIndicator")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Platform $platform;

    /**
     * @psalm-mutation-free
     */
    public function getOrientation(): int
    {
        return $this->orientation;
    }

    /**
     * @return $this
     */
    public function setOrientation(int $orientation): self
    {
        $this->orientation = $orientation;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getX(): int
    {
        return $this->x;
    }

    /**
     * @return $this
     */
    public function setX(int $x): self
    {
        $this->x = $x;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getY(): int
    {
        return $this->y;
    }

    /**
     * @return $this
     */
    public function setY(int $y): self
    {
        $this->y = $y;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return $this
     */
    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @return $this
     */
    public function setHeight(int $height): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }
}
