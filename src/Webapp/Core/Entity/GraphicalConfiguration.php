<?php

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\Site;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *          }
 *     },
 *     itemOperations={
 *         "get"={"security"="is_granted('ROLE_PLATFORM_MANAGER')"},
 *         "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getSite())",
 *          },
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"site": "exact"})
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="graphical_configuration", schema="webapp")
 */
class GraphicalConfiguration extends IdentifiedEntity
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $individualColor = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $unitPlotColor = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $blockColor = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $subBlockColor = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $experimentColor = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $platformColor = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $selectedItemColor = null;

    /**
     * @ORM\OneToOne(targetEntity="Shared\Authentication\Entity\Site", inversedBy="graphicalConfiguration")
     *
     * @ORM\JoinColumn(nullable=false)
     */
    private Site $site;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private ?array $experimentLabels = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private ?array $blocLabels = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private ?array $subBlocLabels = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private ?array $unitPlotLabels = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private ?array $surfacicUnitPlotLabels = null;

    /**
     * @ORM\Column(type="simple_array", nullable=true)
     */
    private ?array $individualLabels = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $activeBreadcrumb = false;

    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualColor(): ?int
    {
        return $this->individualColor;
    }

    /**
     * @return $this
     */
    public function setIndividualColor(?int $individualColor): self
    {
        $this->individualColor = $individualColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitPlotColor(): ?int
    {
        return $this->unitPlotColor;
    }

    /**
     * @return $this
     */
    public function setUnitPlotColor(?int $unitPlotColor): self
    {
        $this->unitPlotColor = $unitPlotColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlockColor(): ?int
    {
        return $this->blockColor;
    }

    /**
     * @return $this
     */
    public function setBlockColor(?int $blockColor): self
    {
        $this->blockColor = $blockColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlockColor(): ?int
    {
        return $this->subBlockColor;
    }

    /**
     * @return $this
     */
    public function setSubBlockColor(?int $subBlockColor): self
    {
        $this->subBlockColor = $subBlockColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperimentColor(): ?int
    {
        return $this->experimentColor;
    }

    /**
     * @return $this
     */
    public function setExperimentColor(?int $experimentColor): self
    {
        $this->experimentColor = $experimentColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatformColor(): ?int
    {
        return $this->platformColor;
    }

    /**
     * @return $this
     */
    public function setPlatformColor(?int $platformColor): self
    {
        $this->platformColor = $platformColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSelectedItemColor(): ?int
    {
        return $this->selectedItemColor;
    }

    /**
     * @return $this
     */
    public function setSelectedItemColor(?int $selectedItemColor): self
    {
        $this->selectedItemColor = $selectedItemColor;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * @return $this
     */
    public function setSite(Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperimentLabels(): ?array
    {
        return $this->experimentLabels;
    }

    /**
     * @return $this
     */
    public function setExperimentLabels(?array $experimentLabels): self
    {
        $this->experimentLabels = $experimentLabels;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getBlocLabels(): ?array
    {
        return $this->blocLabels;
    }

    /**
     * @return $this
     */
    public function setBlocLabels(?array $blocLabels): self
    {
        $this->blocLabels = $blocLabels;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSubBlocLabels(): ?array
    {
        return $this->subBlocLabels;
    }

    /**
     * @return $this
     */
    public function setSubBlocLabels(?array $subBlocLabels): self
    {
        $this->subBlocLabels = $subBlocLabels;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getUnitPlotLabels(): ?array
    {
        return $this->unitPlotLabels;
    }

    /**
     * @return $this
     */
    public function setUnitPlotLabels(?array $unitPlotLabels): self
    {
        $this->unitPlotLabels = $unitPlotLabels;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getSurfacicUnitPlotLabels(): ?array
    {
        return $this->surfacicUnitPlotLabels;
    }

    /**
     * @return $this
     */
    public function setSurfacicUnitPlotLabels(?array $surfacicUnitPlotLabels): self
    {
        $this->surfacicUnitPlotLabels = $surfacicUnitPlotLabels;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getIndividualLabels(): ?array
    {
        return $this->individualLabels;
    }

    /**
     * @return $this
     */
    public function setIndividualLabels(?array $individualLabels): self
    {
        $this->individualLabels = $individualLabels;

        return $this;
    }

    public function isActiveBreadcrumb(): bool
    {
        return $this->activeBreadcrumb;
    }

    /**
     * @return $this
     */
    public function setActiveBreadcrumb(bool $activeBreadcrumb): self
    {
        $this->activeBreadcrumb = $activeBreadcrumb;

        return $this;
    }
}
