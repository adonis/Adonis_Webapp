<?php

namespace Webapp\Core\Entity\CloneExperiment;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Webapp\Core\ApiOperation\CloneExperimentOperation;
use Webapp\Core\Entity\Experiment;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "controller"=CloneExperimentOperation::class,
 *              "read"=false,
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER')",
 *              "openapi_context"={
 *                  "summary": "Clone an experiment",
 *                  "description": "Create a copy of the given experiment giving him a new name and place it in the library"
 *              },
 *          },
 *     },
 *     itemOperations={
 *     "get"={
 *             "controller"=NotFoundAction::class,
 *             "read"=false,
 *             "output"=false,
 *          }}
 * )
 */
class CloneExperiment
{
    /**
     * @ApiProperty(identifier=true)
     */
    private $id;

    private Experiment $experiment;

    private string $newName = '';

    private Experiment $result;

    /**
     * @psalm-mutation-free
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getExperiment(): Experiment
    {
        return $this->experiment;
    }

    /**
     * @return $this
     */
    public function setExperiment(Experiment $experiment): self
    {
        $this->experiment = $experiment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getNewName(): string
    {
        return $this->newName;
    }

    /**
     * @return $this
     */
    public function setNewName(string $newName): self
    {
        $this->newName = $newName;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getResult(): Experiment
    {
        return $this->result;
    }

    /**
     * @return $this
     */
    public function setResult(Experiment $result): self
    {
        $this->result = $result;

        return $this;
    }
}
