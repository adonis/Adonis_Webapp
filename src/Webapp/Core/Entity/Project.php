<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\GroupFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Shared\Authentication\CustomFilters\DeletedFilter;
use Shared\Authentication\Entity\IdentifiedEntity;
use Shared\Authentication\Entity\User;
use Shared\RightManagement\Annotation\AdvancedRight;
use Shared\RightManagement\Traits\HasOwnerEntity;
use Shared\TransferSync\Controller\TransferToMobileAction;
use Shared\TransferSync\Entity\StatusDataEntry;
use Shared\Utils\ArrayCollectionUtils;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Webapp\Core\ApiOperation\RestoreObjectOperation;
use Webapp\Core\Dto\Project\UserSelectionInput;
use Webapp\Core\Validator\Project\ProjectPostConstraint;
use Webapp\Core\Validator\UniqueAttributeInParent;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"={},
 *         "post"={
 *              "security_post_denormalize"="is_granted('ROLE_PLATFORM_MANAGER', object.getPlatform().getSite())",
 *          },
 *     },
 *     itemOperations={
 *          "get"={},
 *          "patch"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getPlatform().getSite())",
 *          },
 *          "post_transfer"={
 *              "method"="PATCH",
 *              "path"="/projects/{id}/transfer",
 *              "controller"=TransferToMobileAction::class,
 *              "input"=UserSelectionInput::class,
 *          },
 *          "delete"={
 *              "security"="is_granted('ROLE_PLATFORM_MANAGER', object.getPlatform().getSite()) && object.getProjectDatas().count() === 0"
 *          },
 *          "restore"={
 *              "controller"=RestoreObjectOperation::class,
 *              "method"="PATCH",
 *              "path"="/projects/{id}/restore",
 *              "security"="is_granted('ROLE_SITE_ADMIN')",
 *              "read"=false,
 *              "validate"=false,
 *              "openapi_context"={
 *                  "summary": "Restore deleted protocol",
 *                  "description": "Remove the deleted state"
 *              },
 *          }
 *     }
 * )
 *
 * @ApiFilter(SearchFilter::class, properties={"platform.site": "exact", "name": "exact", "pathBase": "exact"})
 * @ApiFilter(ExistsFilter::class, properties={"transferDate", "deletedAt"})
 * @ApiFilter(GroupFilter::class, arguments={"whitelist"={
 *     "project_explorer_view",
 *     "transfer_explorer_view",
 *     "data_explorer_view",
 *     "admin_explorer_view",
 *     "project_synthesis"
 * }})
 * @ApiFilter(DeletedFilter::class)
 *
 * @AdvancedRight(classIdentifier="webapp_project", ownerField="owner", parentFields="platform")
 *
 * @Gedmo\SoftDeleteable()
 *
 * @ORM\Entity
 *
 * @ORM\Table(name="project", schema="webapp")
 *
 * @ProjectPostConstraint
 */
class Project extends IdentifiedEntity
{
    use HasOwnerEntity;

    use SoftDeleteableEntity;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"project_explorer_view", "data_explorer_view", "transfer_explorer_view", "status_project_webapp_view", "admin_explorer_view", "change_report", "data_entry_synthesis", "project_synthesis", "variable_synthesis"})
     *
     * @Assert\NotBlank
     *
     * @UniqueAttributeInParent(parentsAttributes={"site.projects"})
     */
    private string $name = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $comment = null;

    /**
     * @ORM\ManyToOne(targetEntity="Webapp\Core\Entity\Platform", inversedBy="projects")
     *
     * @Groups({"data_entry_synthesis", "project_synthesis", "change_report", "variable_synthesis", "status_project_mobile_view"})
     */
    private ?Platform $platform = null;

    /**
     * @var Collection<int, Experiment>
     *
     * @ORM\ManyToMany(targetEntity="Webapp\Core\Entity\Experiment", inversedBy="projects")
     *
     * @ORM\JoinTable(schema="webapp", name="rel_project_experiment")
     *
     * @Groups({"project_explorer_view", "change_report", "data_entry_synthesis", "project_synthesis", "variable_synthesis"})
     */
    private Collection $experiments;

    /**
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     *
     * @Groups({"project_synthesis"})
     */
    private \DateTime $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Groups({"project_explorer_view", "transfer_explorer_view"})
     */
    private ?\DateTime $transferDate = null;

    /**
     * @var Collection<int, SimpleVariable>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\SimpleVariable", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     *
     * @Groups({"project_explorer_view"})
     */
    private Collection $simpleVariables;

    /**
     * @var Collection<int, GeneratorVariable>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\GeneratorVariable", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     *
     * @Groups({"project_explorer_view"})
     */
    private Collection $generatorVariables;

    /**
     * @var Collection<int, Device>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\Device", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     *
     * @Groups({"project_explorer_view"})
     */
    private Collection $devices;

    /**
     * @var Collection<int, StateCode>
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\StateCode", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     *
     * @Groups({"project_explorer_view", "variable_synthesis"})
     */
    private Collection $stateCodes;

    /**
     * @var Collection<int, ProjectData>
     *
     * @Groups({"data_explorer_view", "project_explorer_view", "platform_synthesis", "project_synthesis"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\ProjectData", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private Collection $projectDatas;

    /**
     * @ORM\OneToOne(targetEntity="Webapp\Core\Entity\PathBase", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     *
     * @Groups({"project_explorer_view", "project_synthesis", "variable_synthesis"})
     */
    private ?PathBase $pathBase = null;

    /**
     * @var Collection<int, RequiredAnnotation>
     *
     * @Groups({"project_explorer_view", "data_entry_synthesis"})
     *
     * @ORM\OneToMany(targetEntity="Webapp\Core\Entity\RequiredAnnotation", mappedBy="project", cascade={"remove", "persist"}, orphanRemoval=true)
     */
    private Collection $requiredAnnotations;

    /**
     * @var Collection<int, StatusDataEntry>
     *
     * @ORM\OneToMany(targetEntity="Shared\TransferSync\Entity\StatusDataEntry", mappedBy="webappProject", cascade={"remove"})
     */
    private Collection $statusDataEntries;

    public function __construct()
    {
        $this->experiments = new ArrayCollection();
        $this->simpleVariables = new ArrayCollection();
        $this->generatorVariables = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->stateCodes = new ArrayCollection();
        $this->projectDatas = new ArrayCollection();
        $this->requiredAnnotations = new ArrayCollection();
        $this->statusDataEntries = new ArrayCollection();
        $this->created = new \DateTime();
    }

    /**
     * @Groups({"project_synthesis"})
     *
     * @return (SimpleVariable|SemiAutomaticVariable|GeneratorVariable)[]
     */
    public function getVariables(): array
    {
        return array_merge(
            $this->getSimpleVariables()->getValues(),
            array_reduce(
                $this->getDevices()->getValues(),
                fn ($acc, Device $device) => array_merge($acc, $device->getManagedVariables()->getValues()),
                []
            ),
            $this->getGeneratorVariables()->getValues());
    }

    /**
     * @param (SimpleVariable|GeneratorVariable)[] $variables
     *
     * @return $this
     */
    public function setVariables(array $variables): self
    {
        $this->setSimpleVariables(array_filter($variables, fn (AbstractVariable $variable) => $variable instanceof SimpleVariable));
        $this->setGeneratorVariables(array_filter($variables, fn (AbstractVariable $variable) => $variable instanceof GeneratorVariable));

        return $this;
    }

    /**
     * @Groups({"project_explorer_view", "status_project_webapp_view", "project_synthesis", "variable_synthesis"})
     *
     * @psalm-mutation-free
     */
    public function getOwner(): ?User
    {
        return $this->owner;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @return $this
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPlatform(): ?Platform
    {
        return $this->platform;
    }

    /**
     * @return $this
     */
    public function setPlatform(?Platform $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * @return Collection<int,  Experiment>
     *
     * @psalm-mutation-free
     */
    public function getExperiments(): Collection
    {
        return $this->experiments;
    }

    /**
     * @param iterable<int,  Experiment> $experiments
     *
     * @return $this
     */
    public function setExperiments(iterable $experiments): self
    {
        ArrayCollectionUtils::update($this->experiments, $experiments);

        return $this;
    }

    /**
     * @return $this
     */
    public function addExperiment(Experiment $experiment): self
    {
        if (!$this->experiments->contains($experiment)) {
            $this->experiments->add($experiment);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeExperiment(Experiment $experiment): self
    {
        if ($this->experiments->contains($experiment)) {
            $this->experiments->removeElement($experiment);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return $this
     */
    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTransferDate(): ?\DateTime
    {
        return $this->transferDate;
    }

    /**
     * @return $this
     */
    public function setTransferDate(?\DateTime $transferDate): self
    {
        $this->transferDate = $transferDate;

        return $this;
    }

    /**
     * @return Collection<int,  SimpleVariable>
     *
     * @psalm-mutation-free
     */
    public function getSimpleVariables(): Collection
    {
        return $this->simpleVariables;
    }

    /**
     * @param iterable<int,  SimpleVariable> $simpleVariables
     *
     * @return $this
     */
    public function setSimpleVariables(iterable $simpleVariables): self
    {
        ArrayCollectionUtils::update($this->simpleVariables, $simpleVariables, function (SimpleVariable $simpleVariable) {
            $simpleVariable->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addSimpleVariable(SimpleVariable $simpleVariable): self
    {
        if (!$this->simpleVariables->contains($simpleVariable)) {
            $this->simpleVariables->add($simpleVariable);
            $simpleVariable->setProject($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeSimpleVariable(SimpleVariable $simpleVariable): self
    {
        if ($this->simpleVariables->contains($simpleVariable)) {
            $this->simpleVariables->removeElement($simpleVariable);
            $simpleVariable->setProject(null);
        }

        return $this;
    }

    /**
     * @return Collection<int,  GeneratorVariable>
     *
     * @psalm-mutation-free
     */
    public function getGeneratorVariables(): Collection
    {
        return $this->generatorVariables;
    }

    /**
     * @param iterable<int,  GeneratorVariable> $generatorVariables
     *
     * @return $this
     */
    public function setGeneratorVariables(iterable $generatorVariables): self
    {
        ArrayCollectionUtils::update($this->generatorVariables, $generatorVariables, function (GeneratorVariable $generatorVariable) {
            $generatorVariable->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addGeneratorVariable(GeneratorVariable $generatorVariable): self
    {
        if (!$this->generatorVariables->contains($generatorVariable)) {
            $this->generatorVariables->add($generatorVariable);
            $generatorVariable->setProject($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeGeneratorVariable(GeneratorVariable $generatorVariable): self
    {
        if ($this->generatorVariables->contains($generatorVariable)) {
            $this->generatorVariables->removeElement($generatorVariable);
            $generatorVariable->setProject(null);
        }

        return $this;
    }

    /**
     * @return Collection<int,  Device>
     *
     * @psalm-mutation-free
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    /**
     * @param iterable<int,  Device> $devices
     *
     * @return $this
     */
    public function setDevices(iterable $devices): self
    {
        ArrayCollectionUtils::update($this->devices, $devices, function (Device $device) {
            $device->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices->add($device);
            $device->setProject($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            $device->setProject(null);
        }

        return $this;
    }

    /**
     * @return Collection<int,  StateCode>
     *
     * @psalm-mutation-free
     */
    public function getStateCodes(): Collection
    {
        return $this->stateCodes;
    }

    /**
     * @param iterable<int,  StateCode> $stateCodes
     *
     * @return $this
     */
    public function setStateCodes(iterable $stateCodes): self
    {
        ArrayCollectionUtils::update($this->stateCodes, $stateCodes, function (StateCode $stateCode) {
            $stateCode->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addStateCode(StateCode $stateCode): self
    {
        if (!$this->stateCodes->contains($stateCode)) {
            $this->stateCodes->add($stateCode);
            $stateCode->setProject($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeStateCode(StateCode $stateCode): self
    {
        if ($this->stateCodes->contains($stateCode)) {
            $this->stateCodes->removeElement($stateCode);
            $stateCode->setProject(null);
        }

        return $this;
    }

    /**
     * @return Collection<int,  ProjectData>
     *
     * @psalm-mutation-free
     */
    public function getProjectDatas(): Collection
    {
        return $this->projectDatas;
    }

    /**
     * @return $this
     */
    public function addProjectData(ProjectData $projectData): self
    {
        if (!$this->projectDatas->contains($projectData)) {
            $this->projectDatas->add($projectData);
            $projectData->setProject($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeProjectData(ProjectData $projectData): self
    {
        if ($this->projectDatas->contains($projectData)) {
            $this->projectDatas->removeElement($projectData);
        }

        return $this;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPathBase(): ?PathBase
    {
        return $this->pathBase;
    }

    /**
     * @return $this
     */
    public function setPathBase(?PathBase $pathBase): self
    {
        $this->pathBase = $pathBase;
        if (null !== $pathBase) {
            $pathBase->setProject($this);
        }

        return $this;
    }

    /**
     * @return Collection<int,  RequiredAnnotation>
     *
     * @psalm-mutation-free
     */
    public function getRequiredAnnotations(): Collection
    {
        return $this->requiredAnnotations;
    }

    /**
     * @param iterable<int,  RequiredAnnotation> $requiredAnnotations
     *
     * @return $this
     */
    public function setRequiredAnnotations(iterable $requiredAnnotations): self
    {
        ArrayCollectionUtils::update($this->requiredAnnotations, $requiredAnnotations, function (RequiredAnnotation $requiredAnnotation) {
            $requiredAnnotation->setProject($this);
        });

        return $this;
    }

    /**
     * @return $this
     */
    public function addRequiredAnnotation(RequiredAnnotation $requiredAnnotation): self
    {
        if (!$this->requiredAnnotations->contains($requiredAnnotation)) {
            $this->requiredAnnotations->add($requiredAnnotation);
            $requiredAnnotation->setProject($this);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function removeRequiredAnnotation(RequiredAnnotation $requiredAnnotation): self
    {
        if ($this->requiredAnnotations->contains($requiredAnnotation)) {
            $this->requiredAnnotations->removeElement($requiredAnnotation);
            $requiredAnnotation->setProject(null);
        }

        return $this;
    }

    /**
     * @return Collection<int,  StatusDataEntry>
     *
     * @psalm-mutation-free
     */
    public function getStatusDataEntries(): Collection
    {
        return $this->statusDataEntries;
    }
}
