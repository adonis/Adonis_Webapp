<?php

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\ProjectData;

/**
 * Class DeleteSessionOperation.
 */
final class DeleteProjectDataOperation
{
    protected Security $security;
    protected IriConverterInterface $iriConverter;
    protected EntityManagerInterface $entityManager;

    public function __construct(Security $security, IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->iriConverter = $iriConverter;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param ProjectData $data
     * @return mixed
     */
    public function __invoke(Request $request, $data)
    {
        $fieldMeasureDeleteCallback = function (FieldMeasure $fieldMeasure) use (&$fieldMeasureDeleteCallback) {
            foreach ($fieldMeasure->getFieldGenerations() as $fieldGeneration) {
                foreach ($fieldGeneration->getChildren() as $child) {
                    $fieldMeasureDeleteCallback($child);
                    $this->entityManager->flush();
                }
                $this->entityManager->remove($fieldGeneration);
            }
            $this->entityManager->remove($fieldMeasure);
        };

        foreach ($data->getSessions() as $session) {
            foreach ($session->getFieldMeasures() as $fieldMeasure) {
                $fieldMeasureDeleteCallback($fieldMeasure);
            }
        }

        return $data;
    }

}
