<?php

/*
 * @author TRYDEA - 2024
 */

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Shared\Authentication\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\Annotation;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\FieldGeneration;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\Fusion\DataEntryFusion;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Entity\ProjectData;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\Session;
use Webapp\Core\Entity\SimpleVariable;
use Webapp\Core\Entity\StateCode;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\UnitPlot;

/**
 * @psalm-import-type VariableType from AbstractVariable
 *
 * @psalm-type IriMap = array<string, VariableType|Measure>
 * @psalm-type TargetType = Block|Experiment|Individual|SubBlock|UnitPlot|SurfacicUnitPlot
 */
final class FusionDataEntriesOperation
{
    private const MISSING_DATA = 'Donnée Manquante';

    private IriConverterInterface $iriConverter;

    private EntityManagerInterface $entityManager;

    private Security $security;

    public function __construct(IriConverterInterface $iriConverter, EntityManagerInterface $entityManager, Security $security)
    {
        $this->iriConverter = $iriConverter;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    /**
     * @return BadRequestHttpException|DataEntryFusion
     */
    public function __invoke(Request $request, DataEntryFusion $data)
    {
        if (0 === \count($data->getOrderedDefaultProjectDatasIris()) || 0 === \count($data->getSelectedVariableIris())) {
            return new BadRequestHttpException('No project data selected or no variable to merge.');
        }

        $project = $data->getOrderedDefaultProjectDatasIris()[0]->getProject();

        // Add new project data to project
        $fusionProjectData = $this->createFusionProjectData($data);
        $project->addProjectData($fusionProjectData);

        // Add new variables to project data
        [$variablesByIri, $variablesByName] = $this->createFusionVariables($data);
        array_walk($variablesByIri, /** @psalm-param VariableType $variable */ fn (AbstractVariable $variable) => $fusionProjectData->addVariable($variable));

        // Detect variables to merge
        $variablesToMerge = $this->detectVariablesToMerge($variablesByName);

        // Get order for specific object
        $orderedIriForObjectMap = $this->getSpecificObjectOrder($data);

        // Check conflicts
        $conflicts = $this->checkConflicts($data, $variablesToMerge);

        if ($this->hasUnsolvedConflicts($conflicts, $data)) {
            // Return conflicts
            $data->setConflicts(array_values($conflicts));
            $fusionProjectData = null;
        } else {
            // Duplicate sessions and create new merge session
            $this->duplicateSessions($data, $fusionProjectData, $variablesByIri);
            $this->createFusionSession($data, $fusionProjectData, $variablesByIri, $variablesToMerge, $orderedIriForObjectMap, $conflicts);
            $this->entityManager->persist($fusionProjectData);
            $this->entityManager->flush();
        }

        return $data
            ->setId(0)
            ->setResult($fusionProjectData);
    }

    private function createFusionProjectData(DataEntryFusion $data): ProjectData
    {
        return (new ProjectData())
            ->setFusion(true)
            ->setName($data->getName())
            ->setUser($this->getUser());
    }

    /**
     * @return array{array<string, VariableType>, array<string, VariableType[]>}
     */
    private function createFusionVariables(DataEntryFusion $data): array
    {
        /** @var array<string, VariableType> $variablesByIri Variables added to new project data by IRI */
        $variablesByIri = [];
        /** @var array<string, VariableType[]> $variablesByName Variables to merge mapped by name */
        $variablesByName = [];
        foreach ($data->getSelectedVariableIris() as $selectedVariableIri) {
            $variable = $this->iriConverter->getItemFromIri($selectedVariableIri);
            \assert($variable instanceof SimpleVariable || $variable instanceof GeneratorVariable || $variable instanceof SemiAutomaticVariable);

            if (isset($variablesByName[$variable->getName()])) {
                $variablesByName[$variable->getName()][] = $variable;
                $variablesByIri[$selectedVariableIri] = $variablesByIri[$this->iriConverter->getIriFromItem($variablesByName[$variable->getName()][0])];
            } else {
                $variablesByName[$variable->getName()] = [$variable];
                $variablesByIri[$selectedVariableIri] = $this->duplicateVariable($variable, $variablesByIri);
            }
        }

        return [$variablesByIri, $variablesByName];
    }

    /**
     * @param IriMap                        $variablesByIri
     * @param string[]                      $variablesToMerge
     * @param array<string, ProjectData[]>  $orderedIriForObjectMap
     * @param array<string, FieldMeasure[]> $conflicts
     */
    protected function createFusionSession(DataEntryFusion $data, ProjectData $fusionProjectData, array $variablesByIri, array $variablesToMerge, array $orderedIriForObjectMap, array $conflicts): void
    {
        $targetedObjects = $this->extractTargets($data, $variablesByIri);

        $date = new \DateTime();
        $fusionSession = (new Session())
            ->setUser($this->getUser())
            ->setEndedAt($date)
            ->setStartedAt($date)
            ->setComment('fusion');

        foreach ($variablesToMerge as $variableIri) {
            foreach ($targetedObjects[$variableIri] as $targetedObject) {
                $projectDatasOrdered = array_values($orderedIriForObjectMap[$this->iriConverter->getIriFromItem($targetedObject)] ?? $data->getOrderedDefaultProjectDatasIris());
                $projectDatasMeasures = [];
                foreach ($projectDatasOrdered as $projectData) {
                    $projectDataFieldMeasure = $this->getProjectDataFieldMeasure($projectData, $variablesByIri[$variableIri], $targetedObject);
                    if (null === $projectDataFieldMeasure) {
                        // No measure for this variable and target
                        continue;
                    }

                    $projectDatasMeasures[] = $projectDataFieldMeasure;
                    $stateCode = \count($projectDataFieldMeasure->getMeasures()) > 0 ? $projectDataFieldMeasure->getMeasures()[0]->getState() : null;
                    if (\in_array($projectDataFieldMeasure, $data->getMergePriority() ?? [], true)
                        || (!\in_array($variableIri, $conflicts, true) && !$this->isMissingStateCode($stateCode))) {
                        // Merge priority is defined
                        // Or there is no conflict and it is not missing data state code
                        $fusionSession->addFieldMeasure($this->duplicateFieldMeasure($projectDataFieldMeasure, $variablesByIri));
                        continue 2;
                    }
                }

                // Only missing data state code
                if (\count($projectDatasMeasures) > 0) {
                    $fusionSession->addFieldMeasure($this->duplicateFieldMeasure($projectDatasMeasures[0], $variablesByIri));
                }
            }
        }

        foreach ($data->getOrderedDefaultProjectDatasIris() as $projectData) {
            foreach ($projectData->getSessions() as $session) {
                foreach ($session->getAnnotations() as $annotation) {
                    if (!$annotation->getTarget() instanceof Measure || isset($variablesByIri[$this->iriConverter->getIriFromItem($annotation->getTarget())])) {
                        $fusionSession->addAnnotation($this->duplicateAnnotation($annotation, $variablesByIri));
                    }
                }
            }
        }

        $fusionProjectData->addSession($fusionSession);
    }

    /**
     * @param TargetType|null $target
     */
    private function getProjectDataFieldMeasure(ProjectData $projectData, AbstractVariable $variable, $target): ?FieldMeasure
    {
        if (null === $target) {
            return null;
        }

        $targetIri = $this->iriConverter->getIriFromItem($target);

        $sessions = $projectData->getSessions()->getValues();
        // Sort sessions of this project data to get the good field measure.
        usort($sessions, fn (Session $session1, Session $session2) => (int) $session1->getEndedAt()->format('U') - (int) $session2->getEndedAt()->format('U'));
        foreach (array_reverse($sessions) as $session) {
            foreach ($session->getFieldMeasures() as $fieldMeasure) {
                $fieldMeasureTargetIri = $fieldMeasure->getTarget() ? $this->iriConverter->getIriFromItem($fieldMeasure->getTarget()) : null;
                if ($fieldMeasure->getVariable()->getName() === $variable->getName() && $fieldMeasureTargetIri === $targetIri) {
                    return $fieldMeasure;
                }
            }
        }

        return null;
    }

    /**
     * @param IriMap $variablesByIri
     */
    protected function duplicateSessions(DataEntryFusion $data, ProjectData $fusionProjectData, array $variablesByIri): void
    {
        foreach ($data->getOrderedDefaultProjectDatasIris() as $projectData) {
            foreach ($projectData->getSessions()->getValues() as $session) {
                $fusionProjectData->addSession($this->duplicateSession($session, $variablesByIri));
            }
        }
    }

    /**
     * @param IriMap $variablesByIri
     */
    private function duplicateSession(Session $session, array &$variablesByIri): Session
    {
        $duplicatedSession = (new Session())
            ->setUser($session->getUser())
            ->setEndedAt($session->getEndedAt())
            ->setStartedAt($session->getStartedAt());

        foreach ($session->getFieldMeasures() as $fieldMeasure) {
            $variableIri = $this->iriConverter->getIriFromItem($fieldMeasure->getVariable());
            if (!\array_key_exists($variableIri, $variablesByIri)) {
                continue;
            }

            $duplicatedSession->addFieldMeasure($this->duplicateFieldMeasure($fieldMeasure, $variablesByIri));
        }

        foreach ($session->getAnnotations() as $annotation) {
            $duplicatedSession->addAnnotation($this->duplicateAnnotation($annotation, $variablesByIri));
        }

        return $duplicatedSession;
    }

    /**
     * @param IriMap $iriMap
     */
    private function duplicateAnnotation(Annotation $annotation, array $iriMap): Annotation
    {
        return (new Annotation())
            ->setValue($annotation->getValue())
            ->setTimestamp($annotation->getTimestamp())
            ->setType($annotation->getType())
            ->setTarget($annotation->getTarget() instanceof Measure ?
                $iriMap[$this->iriConverter->getIriFromItem($annotation->getTarget())] :
                $annotation->getTarget())
            ->setKeywords($annotation->getKeywords())
            ->setCategories($annotation->getKeywords())
            ->setName($annotation->getName())
            ->setImage($annotation->getImage());
    }

    /**
     * @psalm-param VariableType $variable
     *
     * @param array<string, VariableType> $variablesByIri
     *
     * @psalm-return VariableType
     */
    private function duplicateVariable(AbstractVariable $variable, array &$variablesByIri): AbstractVariable
    {
        if ($variable instanceof SimpleVariable) {
            $duplicateVariable = $this->duplicateSimpleVariable($variable);
        } elseif ($variable instanceof SemiAutomaticVariable) {
            $duplicateVariable = $this->duplicateSemiAutomaticVariable($variable);
        } elseif ($variable instanceof GeneratorVariable) {
            $duplicateVariable = $this->duplicateGeneratorVariable($variable, $variablesByIri);
        } else {
            throw new \LogicException(\sprintf("Type de variable '%s' non supporté.", get_debug_type($variable)));
        }

        return $duplicateVariable;
    }

    private function duplicateSimpleVariable(SimpleVariable $variable): SimpleVariable
    {
        return (new SimpleVariable('', '', 0, '', false, ''))
            ->setType($variable->getType())
            ->setName($variable->getName())
            ->setCreated($variable->getCreated())
            ->setIdentifier($variable->getIdentifier())
            ->setOrder($variable->getOrder())
            ->setComment($variable->getComment())
            ->setDefaultTrueValue($variable->getDefaultTrueValue())
            ->setFormat($variable->getFormat())
            ->setFormatLength($variable->getFormatLength())
            ->setLastModified($variable->getLastModified())
            ->setMandatory($variable->isMandatory())
            ->setPathLevel($variable->getPathLevel())
            ->setRepetitions($variable->getRepetitions())
            ->setShortName($variable->getShortName())
            ->setUnit($variable->getUnit());
    }

    private function duplicateSemiAutomaticVariable(SemiAutomaticVariable $variable): SemiAutomaticVariable
    {
        return (new SemiAutomaticVariable('', '', 0, '', false, '', 0, 0))
            ->setType($variable->getType())
            ->setName($variable->getName())
            ->setCreated($variable->getCreated())
            ->setIdentifier($variable->getIdentifier())
            ->setStart($variable->getStart())
            ->setEnd($variable->getEnd())
            ->setDevice($variable->getDevice())
            ->setOrder($variable->getOrder())
            ->setComment($variable->getComment())
            ->setDefaultTrueValue($variable->getDefaultTrueValue())
            ->setFormat($variable->getFormat())
            ->setFormatLength($variable->getFormatLength())
            ->setLastModified($variable->getLastModified())
            ->setMandatory($variable->isMandatory())
            ->setPathLevel($variable->getPathLevel())
            ->setRepetitions($variable->getRepetitions())
            ->setShortName($variable->getShortName())
            ->setUnit($variable->getUnit());
    }

    /**
     * @param array<string, VariableType> $iriMap
     */
    private function duplicateGeneratorVariable(GeneratorVariable $variable, array &$iriMap): GeneratorVariable
    {
        $res = (new GeneratorVariable('', '', 0, '', false, '', false, false, false))
            ->setName($variable->getName())
            ->setCreated($variable->getCreated())
            ->setIdentifier($variable->getIdentifier())
            ->setNumeralIncrement($variable->isNumeralIncrement())
            ->setDirectCounting($variable->isDirectCounting())
            ->setGeneratedPrefix($variable->getGeneratedPrefix())
            ->setPathWayHorizontal($variable->isPathWayHorizontal())
            ->setOrder($variable->getOrder())
            ->setComment($variable->getComment())
            ->setDefaultTrueValue($variable->getDefaultTrueValue())
            ->setFormat($variable->getFormat())
            ->setFormatLength($variable->getFormatLength())
            ->setLastModified($variable->getLastModified())
            ->setMandatory($variable->isMandatory())
            ->setPathLevel($variable->getPathLevel())
            ->setRepetitions($variable->getRepetitions())
            ->setShortName($variable->getShortName())
            ->setUnit($variable->getUnit());

        foreach ($variable->getGeneratedGeneratorVariables() as $generatedGeneratorVariable) {
            $resVariable = $this->duplicateGeneratorVariable($generatedGeneratorVariable, $iriMap);
            $iriMap[$this->iriConverter->getIriFromItem($generatedGeneratorVariable)] = $resVariable;
            $res->addGeneratedGeneratorVariable($resVariable);
        }

        foreach ($variable->getGeneratedSimpleVariables() as $generatedSimpleVariable) {
            $resVariable = $this->duplicateSimpleVariable($generatedSimpleVariable);
            $iriMap[$this->iriConverter->getIriFromItem($generatedSimpleVariable)] = $resVariable;
            $res->addGeneratedSimpleVariable($resVariable);
        }

        return $res;
    }

    /**
     * @param IriMap $iriMap
     */
    private function duplicateFieldMeasure(FieldMeasure $fieldMeasure, array &$iriMap): FieldMeasure
    {
        $res = (new FieldMeasure())
            ->setTarget($fieldMeasure->getTarget())
            ->setVariable($iriMap[$this->iriConverter->getIriFromItem($fieldMeasure->getVariable())]);

        foreach ($fieldMeasure->getFieldGenerations() as $fieldGeneration) {
            $res->addFieldGeneration($this->duplicateFieldGeneration($fieldGeneration, $iriMap));
        }

        foreach ($fieldMeasure->getMeasures() as $measure) {
            $resMeasure = $this->duplicateMeasure($measure);
            $iriMap[$this->iriConverter->getIriFromItem($measure)] = $resMeasure;
            $res->addMeasure($resMeasure);
        }

        return $res;
    }

    /**
     * @param IriMap $iriMap
     */
    private function duplicateFieldGeneration(FieldGeneration $fieldGeneration, array &$iriMap): FieldGeneration
    {
        $res = (new FieldGeneration())
            ->setIndex($fieldGeneration->getIndex())
            ->setNumeralIncrement($fieldGeneration->getIndex())
            ->setPrefix($fieldGeneration->getPrefix());

        foreach ($fieldGeneration->getChildren() as $child) {
            $res->addChild($this->duplicateFieldMeasure($child, $iriMap));
        }

        return $res;
    }

    private function duplicateMeasure(Measure $measure): Measure
    {
        return (new Measure())
            ->setState($measure->getState())
            ->setTimestamp($measure->getTimestamp())
            ->setValue($measure->getValue())
            ->setRepetition($measure->getRepetition());
    }

    /**
     * @param array<string, VariableType[]> $variablesByName
     *
     * @return string[]
     */
    protected function detectVariablesToMerge(array $variablesByName): array
    {
        /** @var string[] $conflictingVariableIris */
        $conflictingVariables = array_values(
            array_filter($variablesByName, /** @psalm-param VariableType[] $variables */ fn (array $variables) => \count($variables) > 1)
        );

        return array_map(
            /** @psalm-param VariableType $variable */ fn (AbstractVariable $variable) => $this->iriConverter->getIriFromItem($variable),
            array_merge(...$conflictingVariables)
        );
    }

    /**
     * @return array<string, ProjectData[]>
     */
    protected function getSpecificObjectOrder(DataEntryFusion $data): array
    {
        /** @var array<string, ProjectData[]> $orderedIriForObjectMap */
        $orderedIriForObjectMap = [];
        foreach ($data->getSpecificOrderForItem() as $object) {
            $orderedIriForObjectMap[$object['objectIri']] = array_map(
                fn (string $item) => $this->iriConverter->getItemFromIri($item),
                $object['orderedProjectDatasIris']
            );
        }

        return $orderedIriForObjectMap;
    }

    /**
     * Check conflicts (datas with state code "Donnée manquante" are ignored).
     *
     * @param string[] $variablesToMerge
     *
     * @return array<string, FieldMeasure[]>
     */
    protected function checkConflicts(DataEntryFusion $data, array $variablesToMerge): array
    {
        /** @var array<string, FieldMeasure[]> $conflicts */
        $conflicts = [];
        // map variableName + TargetIri => prio fieldMeasure
        /** @var array<string, FieldMeasure> $priorityFieldMeasures */
        $priorityFieldMeasures = [];

        foreach ($data->getOrderedDefaultProjectDatasIris() as $projectData) {
            foreach ($projectData->getSessions() as $session) {
                foreach ($session->getFieldMeasures() as $fieldMeasure) {
                    $variableName = $fieldMeasure->getVariable()->getName();
                    $targetIri = $this->iriConverter->getIriFromItem($fieldMeasure->getTarget());
                    $variableIri = $this->iriConverter->getIriFromItem($fieldMeasure->getVariable());

                    $fieldMeasureStateCode = $fieldMeasure->getMeasures()[0]->getState();
                    if (!\in_array($variableIri, $variablesToMerge, true)) {
                        // Variable not to merge
                        continue;
                    }

                    if (null !== $fieldMeasureStateCode && $this->isMissingStateCode($fieldMeasureStateCode)) {
                        // Missing data sate code does not generate conflict
                        continue;
                    }

                    $mapKey = $variableName.$targetIri;

                    if (!isset($priorityFieldMeasures[$mapKey])) {
                        $priorityFieldMeasures[$mapKey] = $fieldMeasure;
                    } else {
                        $priorityFielMeasureStateCode = $priorityFieldMeasures[$mapKey]->getMeasures()[0]->getState();
                        if ($this->isStateCodeConflict($fieldMeasureStateCode, $priorityFielMeasureStateCode)) {
                            if (!isset($conflicts[$mapKey])) {
                                $conflicts[$mapKey] = [$priorityFieldMeasures[$mapKey]];
                            }
                            $conflicts[$mapKey][] = $fieldMeasure;
                        }
                    }
                }
            }
        }

        return $conflicts;
    }

    private function isStateCodeConflict(?StateCode $stateCode1, ?StateCode $stateCode2): bool
    {
        return
            // State code + data
            (null === $stateCode1 xor null === $stateCode2)
            // Different state code
            || (null !== $stateCode1 && null !== $stateCode2 && $stateCode1->getCode() !== $stateCode2->getCode());
    }

    /**
     * @param array<string, FieldMeasure[]> $conflicts
     */
    protected function hasUnsolvedConflicts(array $conflicts, DataEntryFusion $data): bool
    {
        return !empty(array_filter($conflicts, /** @param FieldMeasure[] $measures */ fn (array $measures) => empty(array_uintersect(
            $measures,
            $data->getMergePriority() ?? [],
            fn ($a, $b) => strcmp($this->iriConverter->getIriFromItem($a), $this->iriConverter->getIriFromItem($b))
        ))));
    }

    private function isMissingStateCode(?StateCode $stateCode): bool
    {
        return null !== $stateCode && self::MISSING_DATA === $stateCode->getTitle();
    }

    private function getUser(): User
    {
        $user = $this->security->getUser();
        \assert($user instanceof User);

        return $user;
    }

    /**
     * @param IriMap $variablesByIri
     *
     * @return array<string, TargetType[]>
     */
    protected function extractTargets(DataEntryFusion $data, array $variablesByIri): array
    {
        $targetedObjects = [];
        foreach ($data->getOrderedDefaultProjectDatasIris() as $projectData) {
            foreach ($projectData->getSessions() as $session) {
                foreach ($session->getFieldMeasures() as $fieldMeasure) {
                    $variableIri = $this->iriConverter->getIriFromItem($fieldMeasure->getVariable());
                    if (!\array_key_exists($variableIri, $variablesByIri)) {
                        continue;
                    }

                    if (!\array_key_exists($variableIri, $targetedObjects)) {
                        $targetedObjects[$variableIri] = [];
                    }

                    $target = $fieldMeasure->getTarget();
                    if (null !== $target && !\in_array($target, $targetedObjects, true)) {
                        $targetedObjects[$variableIri][] = $target;
                    }
                }
            }
        }

        return $targetedObjects;
    }
}
