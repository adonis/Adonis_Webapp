<?php

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Webapp\Core\Entity\CloneVariable\CloneVariable;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Service\CloneService;

/**
 * Class CloneProjectOperation.
 */
final class CloneVariableToProjectOperation
{
    private CloneService $cloneService;

    private EntityManagerInterface $entityManager;

    private IriConverterInterface $iriConverter;

    public function __construct(EntityManagerInterface $entityManager, CloneService $cloneService, IriConverterInterface $iriConverter)
    {
        $this->cloneService = $cloneService;
        $this->entityManager = $entityManager;
        $this->iriConverter = $iriConverter;
    }

    /**
     * @param Request $request
     * @param CloneVariable $data
     * @return CloneVariable
     */
    public function __invoke(Request $request, $data)
    {
        $variable = $this->iriConverter->getItemFromIri($data->getVariable());
        if ($variable instanceof GeneratorVariable) {
            $res = $this->cloneService->cloneGeneratorVariable($variable);
        } else {
            $res = $this->cloneService->cloneSimpleVariable($variable);
        }
        $res->setProject($data->getProject());

        $this->entityManager->persist($res);
        $this->entityManager->flush();
        return $data->setId(0)->setResult($this->iriConverter->getIriFromItem($res));
    }


}
