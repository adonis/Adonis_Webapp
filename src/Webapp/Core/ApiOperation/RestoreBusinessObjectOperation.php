<?php

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\BusinessObject;
use Webapp\Core\Entity\Move\Delete;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * Class DeleteBusinessObjectOperation.
 */
final class RestoreBusinessObjectOperation extends MultipleBusinessObjectAbstractOperation
{
    public function __construct(Security $security, IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        parent::__construct($security, $iriConverter, $entityManager);
    }

    /**
     * @param Request $request
     * @param Delete $data
     * @return mixed
     */
    public function __invoke(Request $request, $data)
    {
        if ($this->entityManager->getFilters()->isEnabled('graphically_deletable')) {
            $this->entityManager->getFilters()->disable('graphically_deletable');
        }

        $restoredObjectsIris = [];
        // TODO Faire les tests de sécurité sur le parent
        foreach ([
                     PathLevelEnum::OEZ,
                     PathLevelEnum::INDIVIDUAL,
                     PathLevelEnum::SURFACIC_UNIT_PLOT,
                     PathLevelEnum::UNIT_PLOT,
                     PathLevelEnum::SUB_BLOCK,
                     PathLevelEnum::BLOCK,
                     PathLevelEnum::EXPERIMENT,
                     PathLevelEnum::PLATFORM,
                 ] as $level) {
            if (isset($data->getObjectIris()[$level])) {
                foreach ($data->getObjectIris()[$level] as $itemIri) {
                    $this->restoreItem($this->iriConverter->getItemFromIri($itemIri), $restoredObjectsIris);
                }
            }
        }

        $this->entityManager->flush();
        $data->setId(0)->setObjectIris($restoredObjectsIris);

        return $data;
    }

    private function restoreItem(BusinessObject $item, array &$restoredObjects)
    {
        $itemIri = $this->iriConverter->getIriFromItem($item);
        $item->setGraphicallyDeletedAt(null);
        $restoredObjects[] = $itemIri;
        if ($item->parent() !== null && $item->parent()->getGraphicallyDeletedAt() !== null) {
            $this->restoreItem($item->parent(), $restoredObjects);
        }
    }

}
