<?php

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\ProjectData;
use Webapp\Core\Entity\Session;
use function Doctrine\ORM\QueryBuilder;

/**
 * Class ProjectWeightOperation.
 */
final class ProjectWeightOperation
{


    /**
     * @param Request $request
     * @return int
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager, IriConverterInterface $iriConverter): int
    {
        $sessions = [];
        foreach ($request->get('id') as $iri){
            /** @var ProjectData | Session $object */
            $object = $iriConverter->getItemFromIri($iri);
            if($object instanceof Session){
                $sessions[] = $object;
            } else {
                $sessions = array_merge($sessions, $object->getSessions()->toArray());
            }
        }
        $qb = $entityManager->getRepository(FieldMeasure::class)->createQueryBuilder('fm');

        return (int) $qb
            ->select('count(fm.id)')
            ->andWhere($qb->expr()->in('fm.session', ':sessions'))
            ->setParameter('sessions', $sessions)
            ->getQuery()->getSingleScalarResult();
    }

}
