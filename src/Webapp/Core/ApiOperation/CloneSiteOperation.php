<?php

namespace Webapp\Core\ApiOperation;

use Shared\FileManagement\Entity\UserLinkedJob;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\CloneSite\CloneSite;

/**
 * Class CloneSiteOperation.
 */
final class CloneSiteOperation
{

    private Security $security;

    public function __construct(
        Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param CloneSite $data
     * @return CloneSite
     */
    public function __invoke(Request $request, CloneSite $data)
    {
        $data->setUser($this->security->getUser());
        $data->setStatus(UserLinkedJob::STATUS_PENDING);
        return $data;
    }
}
