<?php

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\Move\PlaceExperiment;

/**
 * Class MoveBusinessObjectOperation.
 */
final class PlaceExperimentOperation extends MultipleBusinessObjectAbstractOperation
{

    public function __construct(Security $security, IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        parent::__construct($security, $iriConverter, $entityManager);
    }

    /**
     * @param Request $request
     * @param $data PlaceExperiment
     * @return mixed
     */
    public function __invoke(Request $request, $data)
    {
        if($data->getPlatform() === null){
            if($data->getExperiment()->getPlatform() === null){
                throw new BadRequestHttpException();
            }

            $experimentLeafs = [];
            $this->handleExperiment($data->getExperiment(), $experimentLeafs, false);

            foreach ($experimentLeafs as $item) {
                $item->setX($item->getX() + $data->getDx());
                $item->setY($item->getY() + $data->getDY());
            }
            $data->getExperiment()->setSite($data->getExperiment()->getPlatform()->getSite());

        } else {
            if($data->getExperiment()->getPlatform() !== null){
                throw new BadRequestHttpException();
            }
            if(count($data->getPlatform()->getExperiments()) > 0 && $data->getPlatform()->getExperiments()[0]->isIndividualUP() !== $data->getExperiment()->isIndividualUP() ){
                throw new BadRequestHttpException();
            }

            $experimentLeafs = [];
            $platformLeafs = [];
            $this->handleExperiment($data->getExperiment(), $experimentLeafs, false);
            $this->handlePlatform($data->getPlatform(), $platformLeafs, false);

            foreach ($experimentLeafs as $item) {
                $item->setX($item->getX() + $data->getDx());
                $item->setY($item->getY() + $data->getDY());
                if (
                    $item->getX() < 1 || $item->getY() < 1 || (
                    isset($platformLeafs[$item->getX() . ',' . $item->getY()])
                    )
                ) {
                    throw new BadRequestHttpException();
                }
            }
            $data->getPlatform()->addExperiment($data->getExperiment());

        }

        $this->entityManager->flush();
        $data->setId(0);

        return $data;
    }
}
