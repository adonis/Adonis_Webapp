<?php

namespace Webapp\Core\ApiOperation;

use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Move\Create;
use Webapp\Core\Entity\OutExperimentationZone;
use Webapp\Core\Entity\Platform;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\UnitPlot;
use Webapp\Core\Enumeration\PathLevelEnum;

/**
 * Class CreateBusinessObjectOperation.
 */
final class CreateBusinessObjectOperation extends MultipleBusinessObjectAbstractOperation
{

    public function __construct(Security $security, IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        parent::__construct($security, $iriConverter, $entityManager);
    }

    /**
     * @param Request $request
     * @param Create $data
     * @return mixed
     */
    public function __invoke(Request $request, $data)
    {
        /** @var Platform | Experiment $parent common parent to do security checks */
        $parent = null;

        /** @var array<SurfacicUnitPlot | Individual> $objectLeafs x,y => leaf object */
        $objectLeafs = [];

        $check = false;
        $selectedObject = $data->getParent();
        if ($selectedObject instanceof UnitPlot) {
            $this->handleUnitPlot($selectedObject, $objectLeafs, false);
            $check = $this->checkUnitPlotParent($selectedObject, $parent);
        } elseif ($selectedObject instanceof SubBlock) {
            $this->handleSubBlock($selectedObject, $objectLeafs, false);
            $check = $this->checkSubBlockParent($selectedObject, $parent);
        } elseif ($selectedObject instanceof Block) {
            $this->handleBlock($selectedObject, $objectLeafs, false);
            $check = $this->checkBlockParent($selectedObject, $parent);
        } elseif ($selectedObject instanceof Experiment) {
            $this->handleExperiment($selectedObject, $objectLeafs, false);
            $check = $this->checkExperimentParent($selectedObject, $parent);
        }
        if (!$check) {
            throw new BadRequestHttpException();
        }

        /** @var array<SurfacicUnitPlot | Individual> $positionMap x,y => all parent's leaf objects */
        $positionMap = [];
        /** @var array<int> $maxNumbers PathlevelEnum => maxNumber */
        $maxNumbers = [PathLevelEnum::BLOCK => 0, PathLevelEnum::SUB_BLOCK => 0, PathLevelEnum::UNIT_PLOT => 0, PathLevelEnum::SURFACIC_UNIT_PLOT => 0, PathLevelEnum::INDIVIDUAL => 0, PathLevelEnum::OEZ => 0];
        $individualUp = null;
        if ($parent instanceof Platform) {
            $this->handlePlatform($parent, $positionMap, false, $maxNumbers, $individualUp);
        } else {
            $this->handleExperiment($parent, $positionMap, false, $maxNumbers, $individualUp);
        }

        // Construct parents if needed
        $newObjects = [];
        $leafParent = $selectedObject;
        switch ($data->getPathLevel()) {
            case PathLevelEnum::BLOCK:
                $tmp = (new Block())->setNumber(++$maxNumbers[PathLevelEnum::BLOCK]);
                $leafParent->addBlocks($tmp);
                $leafParent = $tmp;
                $newObjects[] = $tmp;
            case PathLevelEnum::SUB_BLOCK:
                if ($data->isUseSubBlock() || $data->getPathLevel() === PathLevelEnum::SUB_BLOCK) {
                    $tmp = (new SubBlock())->setNumber(++$maxNumbers[PathLevelEnum::SUB_BLOCK]);
                    $leafParent->addSubBlocks($tmp);
                    $leafParent = $tmp;
                    if (count($newObjects) === 0) {
                        $newObjects[] = $tmp;
                    }
                }
            case PathLevelEnum::UNIT_PLOT:
                if ($individualUp) {
                    $tmp = (new UnitPlot())->setNumber(++$maxNumbers[PathLevelEnum::UNIT_PLOT])
                        ->setTreatment($data->getTreatment());
                    $leafParent->addUnitPlots($tmp);
                    $leafParent = $tmp;
                    if (count($newObjects) === 0) {
                        $newObjects[] = $tmp;
                    }
                }
        }

        $neighbourFound = false;
        for ($x = $data->getX1(); $x <= $data->getX2(); $x++) {
            for ($y = $data->getY1(); $y <= $data->getY2(); $y++) {
                // Test if one of the edge is near the selected object
                if (!$neighbourFound) {
                    $neighbourFound = isset($objectLeafs[($x - 1) . ',' . $y]) ||
                        isset($objectLeafs[($x + 1) . ',' . $y]) ||
                        isset($objectLeafs[$x . ',' . ($y - 1)]) ||
                        isset($objectLeafs[$x . ',' . ($y + 1)]);
                }
                if (isset($positionMap[$x . ',' . $y])) {
                    throw new BadRequestHttpException();
                }
                if ($data->getPathLevel() === PathLevelEnum::OEZ) {
                    $oez = (new OutExperimentationZone())
                        ->setX($x)
                        ->setY($y)
                        ->setNature($data->getNature())
                        ->setNumber(++$maxNumbers[PathLevelEnum::OEZ]);
                    $newObjects[] = $oez;
                    $leafParent->addOutExperimentationZone($oez);
                } elseif ($individualUp) {
                    $individual = (new Individual())
                        ->setX($x)
                        ->setY($y)
                        ->setNumber(++$maxNumbers[PathLevelEnum::INDIVIDUAL]);
                    $leafParent->addIndividual($individual);
                    if ($data->getPathLevel() === PathLevelEnum::INDIVIDUAL) {
                        $newObjects[] = $individual;
                    }
                } else {
                    $surfacicUnitPlot = (new SurfacicUnitPlot())
                        ->setX($x)
                        ->setY($y)
                        ->setTreatment($data->getTreatment())
                        ->setNumber(++$maxNumbers[PathLevelEnum::SURFACIC_UNIT_PLOT]);
                    $leafParent->addSurfacicUnitPlots($surfacicUnitPlot);
                    if ($data->getPathLevel() === PathLevelEnum::SURFACIC_UNIT_PLOT) {
                        $newObjects[] = $surfacicUnitPlot;
                    }
                }
            }
        }
        if (!$neighbourFound) {
            throw new BadRequestHttpException();
        }
        $this->entityManager->flush();
        $data->setId(0)->setCreatedObjects($newObjects);

        return $data;
    }
}
