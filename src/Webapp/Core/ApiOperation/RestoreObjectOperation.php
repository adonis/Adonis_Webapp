<?php

namespace Webapp\Core\ApiOperation;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RestoreObjectOperation.
 */
final class RestoreObjectOperation
{

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @param $data
     * @param $id
     * @return mixed
     */
    public function __invoke(Request $request, $data, $id)
    {
        if($this->entityManager->getFilters()->isEnabled('soft_deleteable')){
            $this->entityManager->getFilters()->disable('soft_deleteable');
        }
        return $this->entityManager->getRepository(get_class($data))->find($id)->setDeletedAt(null);
    }
}
