<?php

namespace Webapp\Core\ApiOperation;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\CloneProject\CloneProject;
use Webapp\Core\Service\CloneService;

/**
 * Class CloneProjectOperation.
 */
final class CloneProjectOperation
{
    private CloneService $cloneService;

    private EntityManagerInterface $entityManager;
    private Security $security;

    public function __construct(EntityManagerInterface $entityManager, CloneService $cloneService, Security $security)
    {
        $this->cloneService = $cloneService;
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param CloneProject $data
     * @return CloneProject
     */
    public function __invoke(Request $request, $data)
    {
        $res = $this->cloneService->cloneProject($data->getProject(), $data->getNewName())
            ->setOwner($this->security->getUser())
            ->setPlatform($data->getProject()->getPlatform())
            ->setExperiments($data->getProject()->getExperiments());

        $this->entityManager->persist($res);
        $this->entityManager->flush();
        return $data->setId(0)->setResult($res)->setPlatform($res->getPlatform());
    }



}
