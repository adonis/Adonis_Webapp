<?php

namespace Webapp\Core\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Webapp\Core\Dto\BusinessObject\BusinessObjectCsvOutputDto;

class BusinessObjectCSVNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    private SerializerInterface $serializer;

    /**
     * @param BusinessObjectCsvOutputDto $object
     * @param string|null $format
     * @param array $context
     * @return array|\ArrayObject|bool|float|int|string|void|null
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        return [
            'x' => $object->getX(),
            'y' => $object->getY(),
            'id' => $object->getId(),
            'up' => $object->getUp(),
            'dead' => $object->getDead(),
            'appeared' => $this->serializer->normalize($object->getAppeared()),
            'disapeared' => $this->serializer->normalize($object->getDisapeared()),
            'treatment' => $object->getTreatment(),
            'block' => $object->getBlock(),
            'subBlock' => $object->getSubBlock(),
            'experiment' => $object->getExperiment(),
            'platform' => $object->getPlatform(),
            'shortTreatment' => $object->getShortTreatment(),
            'factor1' => $object->getFactor1(),
            'factor1ShortName' => $object->getFactor1ShortName(),
            'factor1Id' => $object->getFactor1Id(),
            'factor2' => $object->getFactor2(),
            'factor2ShortName' => $object->getFactor2ShortName(),
            'factor2Id' => $object->getFactor2Id(),
            'factor3' => $object->getFactor3(),
            'factor3ShortName' => $object->getFactor3ShortName(),
            'factor3Id' => $object->getFactor3Id(),
            'ZHE' => $object->getZHE(),
            'individualGeometry' => $object->getIndividualGeometry(),
            'upGeometry' => $object->getUpGeometry(),
            'blockGeometry' => $object->getBlockGeometry(),
            'subBlockGeometry' => $object->getSubBlockGeometry(),
            'experimentGeometry' => $object->getExperimentGeometry(),
            'ZHEGeometry' => $object->getZHEGeometry(),
        ];
    }

    public function supportsNormalization($data, string $format = null)
    {
        return $data instanceof BusinessObjectCsvOutputDto;
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
}
