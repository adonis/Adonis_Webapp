<?php

namespace Webapp\Core\SQLFilter;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Shared\RightManagement\EventSubscriber\FilterConfigurator;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\GeneratorVariable;
use Webapp\Core\Entity\SemiAutomaticVariable;
use Webapp\Core\Entity\SimpleVariable;

class VariableReportFilter extends SQLFilter
{
    /**
     * @var FilterConfigurator
     */
    protected $listener;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @param string $targetTableAlias
     *
     * @return string
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {

        if ($targetEntity->getName() !== FieldMeasure::class) {
            return '';
        }

        if($this->getListener()->getRequestStack()->getCurrentRequest() === null){
            return '';
        }

        $requestedGroup = $this->getListener()->getRequestStack()->getCurrentRequest()->get("groups");
        if (!is_array($requestedGroup) || !in_array('variable_synthesis', $requestedGroup)) {
            return '';
        }

        $requestedVariableIri = $this->getListener()->getRequestStack()->getCurrentRequest()->getPathInfo();

        $requestedVariable = $this->getListener()->getIriConverter()->getItemFromIri($requestedVariableIri);

        if ($requestedVariable instanceof GeneratorVariable) {
            $variableMapping = 'generatorVariable';
        } elseif ($requestedVariable instanceof SimpleVariable) {
            $variableMapping = 'simpleVariable';
        } elseif ($requestedVariable instanceof SemiAutomaticVariable) {
            $variableMapping = 'semiAutomaticVariable';
        } else {
            return '';
        }

        $variableColumnName = $targetEntity->getSingleAssociationJoinColumnName($variableMapping);

        return sprintf('%s.%s = %s', $targetTableAlias, $variableColumnName, $requestedVariable->getId());
    }

    /**
     * @return FilterConfigurator
     *
     * @throws \RuntimeException
     */
    protected function getListener()
    {
        if (null === $this->listener) {
            $em = $this->getEntityManager();
            $evm = $em->getEventManager();

            foreach ($evm->getListeners() as $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof FilterConfigurator) {
                        $this->listener = $listener;

                        break 2;
                    }
                }
            }

            if (null === $this->listener) {
                throw new \RuntimeException('Listener "GraphicallyDeletableFilterConfigurator" was not added to the EventManager!');
            }
        }

        return $this->listener;
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getEntityManager()
    {
        if (null === $this->entityManager) {
            $refl = new \ReflectionProperty('Doctrine\ORM\Query\Filter\SQLFilter', 'em');
            $refl->setAccessible(true);
            $this->entityManager = $refl->getValue($this);
        }

        return $this->entityManager;
    }
}
