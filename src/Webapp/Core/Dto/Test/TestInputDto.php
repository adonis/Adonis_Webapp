<?php


namespace Webapp\Core\Dto\Test;


class TestInputDto
{
    private string $type;
    private string $variable;

    private ?float $authIntervalMin;
    private ?float $probIntervalMin;
    private ?float $probIntervalMax;
    private ?float $authIntervalMax;

    private ?string $comparedVariable;
    private ?float $minGrowth;
    private ?float $maxGrowth;

    private ?string $combinedVariable1;
    private ?string $combinationOperation;
    private ?string $combinedVariable2;
    private ?float $minLimit;
    private ?float $maxLimit;

    private ?bool $compareWithVariable;
    private ?string $comparedVariable1;
    private ?string $comparisonOperation;
    private ?string $comparedVariable2;
    private ?float $comparedValue;
    private ?string $assignedValue;

    public function __construct()
    {
        $this->authIntervalMin = null;
        $this->probIntervalMin = null;
        $this->probIntervalMax = null;
        $this->authIntervalMax = null;

        $this->comparedVariable = null;
        $this->minGrowth = null;
        $this->maxGrowth = null;

        $this->combinedVariable1 = null;
        $this->combinationOperation = null;
        $this->combinedVariable2 = null;
        $this->minLimit = null;
        $this->maxLimit = null;

        $this->compareWithVariable = null;
        $this->comparedVariable1 = null;
        $this->comparisonOperation = null;
        $this->comparedVariable2 = null;
        $this->comparedValue = null;
        $this->assignedValue = null;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return TestInputDto
     */
    public function setType(string $type): TestInputDto
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getVariable(): string
    {
        return $this->variable;
    }

    /**
     * @param string $variable
     * @return TestInputDto
     */
    public function setVariable(string $variable): TestInputDto
    {
        $this->variable = $variable;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAuthIntervalMin(): ?float
    {
        return $this->authIntervalMin;
    }

    /**
     * @param float|null $authIntervalMin
     * @return TestInputDto
     */
    public function setAuthIntervalMin(?float $authIntervalMin): TestInputDto
    {
        $this->authIntervalMin = $authIntervalMin;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getProbIntervalMin(): ?float
    {
        return $this->probIntervalMin;
    }

    /**
     * @param float|null $probIntervalMin
     * @return TestInputDto
     */
    public function setProbIntervalMin(?float $probIntervalMin): TestInputDto
    {
        $this->probIntervalMin = $probIntervalMin;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getProbIntervalMax(): ?float
    {
        return $this->probIntervalMax;
    }

    /**
     * @param float|null $probIntervalMax
     * @return TestInputDto
     */
    public function setProbIntervalMax(?float $probIntervalMax): TestInputDto
    {
        $this->probIntervalMax = $probIntervalMax;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAuthIntervalMax(): ?float
    {
        return $this->authIntervalMax;
    }

    /**
     * @param float|null $authIntervalMax
     * @return TestInputDto
     */
    public function setAuthIntervalMax(?float $authIntervalMax): TestInputDto
    {
        $this->authIntervalMax = $authIntervalMax;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComparedVariable(): ?string
    {
        return $this->comparedVariable;
    }

    /**
     * @param string|null $comparedVariable
     * @return TestInputDto
     */
    public function setComparedVariable(?string $comparedVariable): TestInputDto
    {
        $this->comparedVariable = $comparedVariable;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinGrowth(): ?float
    {
        return $this->minGrowth;
    }

    /**
     * @param float|null $minGrowth
     * @return TestInputDto
     */
    public function setMinGrowth(?float $minGrowth): TestInputDto
    {
        $this->minGrowth = $minGrowth;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxGrowth(): ?float
    {
        return $this->maxGrowth;
    }

    /**
     * @param float|null $maxGrowth
     * @return TestInputDto
     */
    public function setMaxGrowth(?float $maxGrowth): TestInputDto
    {
        $this->maxGrowth = $maxGrowth;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCombinedVariable1(): ?string
    {
        return $this->combinedVariable1;
    }

    /**
     * @param string|null $combinedVariable1
     * @return TestInputDto
     */
    public function setCombinedVariable1(?string $combinedVariable1): TestInputDto
    {
        $this->combinedVariable1 = $combinedVariable1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCombinationOperation(): ?string
    {
        return $this->combinationOperation;
    }

    /**
     * @param string|null $combinationOperation
     * @return TestInputDto
     */
    public function setCombinationOperation(?string $combinationOperation): TestInputDto
    {
        $this->combinationOperation = $combinationOperation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCombinedVariable2(): ?string
    {
        return $this->combinedVariable2;
    }

    /**
     * @param string|null $combinedVariable2
     * @return TestInputDto
     */
    public function setCombinedVariable2(?string $combinedVariable2): TestInputDto
    {
        $this->combinedVariable2 = $combinedVariable2;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinLimit(): ?float
    {
        return $this->minLimit;
    }

    /**
     * @param float|null $minLimit
     * @return TestInputDto
     */
    public function setMinLimit(?float $minLimit): TestInputDto
    {
        $this->minLimit = $minLimit;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getMaxLimit(): ?float
    {
        return $this->maxLimit;
    }

    /**
     * @param float|null $maxLimit
     * @return TestInputDto
     */
    public function setMaxLimit(?float $maxLimit): TestInputDto
    {
        $this->maxLimit = $maxLimit;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getCompareWithVariable(): ?bool
    {
        return $this->compareWithVariable;
    }

    /**
     * @param bool|null $compareWithVariable
     * @return TestInputDto
     */
    public function setCompareWithVariable(?bool $compareWithVariable): TestInputDto
    {
        $this->compareWithVariable = $compareWithVariable;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComparedVariable1(): ?string
    {
        return $this->comparedVariable1;
    }

    /**
     * @param string|null $comparedVariable1
     * @return TestInputDto
     */
    public function setComparedVariable1(?string $comparedVariable1): TestInputDto
    {
        $this->comparedVariable1 = $comparedVariable1;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComparisonOperation(): ?string
    {
        return $this->comparisonOperation;
    }

    /**
     * @param string|null $comparisonOperation
     * @return TestInputDto
     */
    public function setComparisonOperation(?string $comparisonOperation): TestInputDto
    {
        $this->comparisonOperation = $comparisonOperation;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComparedVariable2(): ?string
    {
        return $this->comparedVariable2;
    }

    /**
     * @param string|null $comparedVariable2
     * @return TestInputDto
     */
    public function setComparedVariable2(?string $comparedVariable2): TestInputDto
    {
        $this->comparedVariable2 = $comparedVariable2;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getComparedValue(): ?float
    {
        return $this->comparedValue;
    }

    /**
     * @param float|null $comparedValue
     * @return TestInputDto
     */
    public function setComparedValue(?float $comparedValue): TestInputDto
    {
        $this->comparedValue = $comparedValue;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAssignedValue(): ?string
    {
        return $this->assignedValue;
    }

    /**
     * @param string|null $assignedValue
     * @return TestInputDto
     */
    public function setAssignedValue(?string $assignedValue): TestInputDto
    {
        $this->assignedValue = $assignedValue;
        return $this;
    }

}