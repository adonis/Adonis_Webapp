<?php


namespace Webapp\Core\Dto\Test;


use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use Webapp\Core\Entity\Test;
use Webapp\Core\Enumeration\TestTypeEnum;

class TestPostTransformer implements DataTransformerInterface
{
    private IriConverterInterface $iriConverter;
    private ValidatorInterface $validator;

    public function __construct(IriConverterInterface $iriConverter, ValidatorInterface $validator)
    {
        $this->iriConverter = $iriConverter;
        $this->validator = $validator;
    }

    /**
     * @param TestInputDto $object
     * @param string $to
     * @param array $context
     * @return object
     */
    public function transform($object, string $to, array $context = []): object
    {
        if (isset($context[AbstractItemNormalizer::OBJECT_TO_POPULATE])) {
            $test = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        } else {
            $test = (new Test())
                ->setVariable($this->iriConverter->getItemFromIri($object->getVariable()))
                ->setType($object->getType());
        }
        switch ($test->getType()) {
            case TestTypeEnum::INTERVAL:
                $test->setAuthIntervalMin($object->getAuthIntervalMin());
                $test->setProbIntervalMin($object->getProbIntervalMin());
                $test->setProbIntervalMax($object->getProbIntervalMax());
                $test->setAuthIntervalMax($object->getAuthIntervalMax());
                break;
            case TestTypeEnum::GROWTH:
                $test->setComparedVariable($this->iriConverter->getItemFromIri($object->getComparedVariable()));
                $test->setMinGrowth($object->getMinGrowth());
                $test->setMaxGrowth($object->getMaxGrowth());
                break;
            case TestTypeEnum::COMBINATION:
                $test->setCombinedVariable1($this->iriConverter->getItemFromIri($object->getCombinedVariable1()));
                $test->setCombinationOperation($object->getCombinationOperation());
                $test->setCombinedVariable2($this->iriConverter->getItemFromIri($object->getCombinedVariable2()));
                $test->setMinLimit($object->getMinLimit());
                $test->setMaxLimit($object->getMaxLimit());
                break;
            case TestTypeEnum::PRECONDITIONED:
                $test->setCompareWithVariable($object->getCompareWithVariable());
                $test->setComparedVariable1($this->iriConverter->getItemFromIri($object->getComparedVariable1()));
                $test->setComparisonOperation($object->getComparisonOperation());
                $test->setComparedVariable2($object->getComparedVariable2() !== null ? $this->iriConverter->getItemFromIri($object->getComparedVariable2()) : null);
                $test->setComparedValue($object->getComparedValue());
                $test->setAssignedValue($object->getAssignedValue());

                break;
        }
        $this->validator->validate($test);
        return $test;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Test) {
            return false;
        }
        return Test::class === $to &&
            $context['operation_type'] === OperationType::COLLECTION &&
            (
                $context['collection_operation_name'] === 'post' ||
                $context['collection_operation_name'] === 'put'
            ) &&
            null !== ($context['input']['class'] ?? null);
    }
}
