<?php


namespace Webapp\Core\Dto\Protocol;


class ProtocolInputTreatmentDto
{
    private string $name;
    private string $shortName;
    private int $repetitions;

    /**
     * @var string[]
     */
    private array $modalities;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProtocolInputTreatmentDto
     */
    public function setName(string $name): ProtocolInputTreatmentDto
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }

    /**
     * @param string $shortName
     * @return ProtocolInputTreatmentDto
     */
    public function setShortName(string $shortName): ProtocolInputTreatmentDto
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return int
     */
    public function getRepetitions(): int
    {
        return $this->repetitions;
    }

    /**
     * @param int $repetitions
     * @return ProtocolInputTreatmentDto
     */
    public function setRepetitions(int $repetitions): ProtocolInputTreatmentDto
    {
        $this->repetitions = $repetitions;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getModalities(): array
    {
        return $this->modalities;
    }

    /**
     * @param string[] $modalities
     * @return ProtocolInputTreatmentDto
     */
    public function setModalities(array $modalities): ProtocolInputTreatmentDto
    {
        $this->modalities = $modalities;
        return $this;
    }
}
