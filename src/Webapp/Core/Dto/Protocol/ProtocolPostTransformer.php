<?php


namespace Webapp\Core\Dto\Protocol;


use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\Treatment;

class ProtocolPostTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param ProtocolInputDto $object
     * @param string $to
     * @param array $context
     * @return object
     */
    public function transform($object, string $to, array $context = []): object
    {
        $this->validator->validate($object);

        $protocol = (new Protocol())
            ->setSite($object->getSite())
            ->setName($object->getName())
            ->setAim($object->getAim())
            ->setComment($object->getComment())
            ->setAlgorithm($object->getAlgorithm());
        foreach ($object->getProtocolAttachments() as $protocolAttachment) {
            $protocol->addProtocolAttachment($protocolAttachment);
        }

        //TODO faire une vérification des conditions de l'algorithme

        $idModalityMap = [];

        foreach ($object->getFactors() as $factorDto){
            $factor = (new Factor())
                ->setOrder($factorDto->getOrder())
                ->setOpenSilexInstance($factorDto->getOpenSilexInstance())
                ->setGermplasm($factorDto->isGermplasm())
                ->setOpenSilexUri($factorDto->getOpenSilexUri())
                ->setName($factorDto->getName());
            foreach ($factorDto->getModalities() as $modalityDto){
                $modality = (new Modality())
                    ->setValue($modalityDto->getValue())
                    ->setIdentifier($modalityDto->getIdentifier())
                    ->setOpenSilexUri($modalityDto->getOpenSilexUri())
                    ->setOpenSilexInstance($modalityDto->getOpenSilexInstance())
                    ->setShortName($modalityDto->getShortName());
                $factor->addModality($modality);
                $idModalityMap[$modalityDto->getUniqId()] = $modality;
            }
            $protocol->addFactors($factor);
        }

        foreach ($object->getTreatments() as $treatmentDto){
            $treatment = (new Treatment())
                ->setShortName($treatmentDto->getShortName())
                ->setName($treatmentDto->getName())
                ->setRepetitions($treatmentDto->getRepetitions());
            foreach ($treatmentDto->getModalities() as $modalityUniqId){
                $treatment->addModalities($idModalityMap[$modalityUniqId]);
            }
            $protocol->addTreatments($treatment);
        }

        return $protocol;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Protocol) {
            return false;
        }
        return Protocol::class === $to &&
            $context['operation_type'] === OperationType::COLLECTION &&
            $context['collection_operation_name'] === 'post' &&
            null !== ($context['input']['class'] ?? null);
    }
}
