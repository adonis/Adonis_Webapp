<?php


namespace Webapp\Core\Dto\Protocol;


use Symfony\Component\Validator\Constraints as Assert;

class ProtocolInputPutDto extends ProtocolInputDto
{
    /**
     * @Assert\NotBlank
     * @var string
     */
    protected string $name;

}
