<?php


namespace Webapp\Core\Dto\Protocol;


use Webapp\Core\Entity\OpenSilexInstance;

class ProtocolInputModalityDto
{
    private string $uniqId;
    private string $value;
    private ?string $shortName = null;
    private ?string $identifier = null;
    private ?string $openSilexUri = null;
    private ?OpenSilexInstance $openSilexInstance = null;

    /**
     * @return string
     */
    public function getUniqId(): string
    {
        return $this->uniqId;
    }

    /**
     * @param string $uniqId
     * @return ProtocolInputModalityDto
     */
    public function setUniqId(string $uniqId): ProtocolInputModalityDto
    {
        $this->uniqId = $uniqId;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ProtocolInputModalityDto
     */
    public function setValue(string $value): ProtocolInputModalityDto
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    /**
     * @param string|null $shortName
     * @return ProtocolInputModalityDto
     */
    public function setShortName(?string $shortName): ProtocolInputModalityDto
    {
        $this->shortName = $shortName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     * @return ProtocolInputModalityDto
     */
    public function setIdentifier(?string $identifier): ProtocolInputModalityDto
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    public function setOpenSilexUri(?string $openSilexUri): ProtocolInputModalityDto
    {
        $this->openSilexUri = $openSilexUri;
        return $this;
    }

    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): ProtocolInputModalityDto
    {
        $this->openSilexInstance = $openSilexInstance;
        return $this;
    }

}
