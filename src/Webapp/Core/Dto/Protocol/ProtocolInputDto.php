<?php


namespace Webapp\Core\Dto\Protocol;


use Shared\Authentication\Entity\Site;
use Symfony\Component\Validator\Constraints as Assert;
use Webapp\Core\Entity\Algorithm;
use Webapp\Core\Entity\Attachment\ProtocolAttachment;

abstract class ProtocolInputDto
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @Assert\NotBlank
     * @var string
     */
    private string $aim;

    /**
     * @var ProtocolInputFactorDto[]
     */
    private array $factors;

    /**
     * @var Site|null
     */
    private ?Site $site;

    /**
     * @var string|null
     */
    private ?string $comment;

    /**
     * @var ProtocolInputTreatmentDto[]
     */
    private array $treatments;

    /**
     * @var Algorithm | null
     */
    private ?Algorithm $algorithm;

    /**
     * @var array<ProtocolAttachment>
     */
    private $protocolAttachments;

    public function __construct()
    {
        $this->site = null;
        $this->algorithm = null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProtocolInputDto
     */
    public function setName(string $name): ProtocolInputDto
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAim(): string
    {
        return $this->aim;
    }

    /**
     * @param string $aim
     * @return ProtocolInputDto
     */
    public function setAim(string $aim): ProtocolInputDto
    {
        $this->aim = $aim;
        return $this;
    }

    /**
     * @return ProtocolInputFactorDto[]
     */
    public function getFactors(): array
    {
        return $this->factors;
    }

    /**
     * @param ProtocolInputFactorDto[] $factors
     * @return ProtocolInputDto
     */
    public function setFactors(array $factors): ProtocolInputDto
    {
        $this->factors = $factors;
        return $this;
    }

    /**
     * @return Site|null
     */
    public function getSite(): ?Site
    {
        return $this->site;
    }

    /**
     * @param Site|null $site
     * @return ProtocolInputDto
     */
    public function setSite(?Site $site): ProtocolInputDto
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     * @return ProtocolInputDto
     */
    public function setComment(?string $comment): ProtocolInputDto
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return ProtocolInputTreatmentDto[]
     */
    public function getTreatments(): array
    {
        return $this->treatments;
    }

    /**
     * @param ProtocolInputTreatmentDto[] $treatments
     * @return ProtocolInputDto
     */
    public function setTreatments(array $treatments): ProtocolInputDto
    {
        $this->treatments = $treatments;
        return $this;
    }

    /**
     * @return Algorithm|null
     */
    public function getAlgorithm(): ?Algorithm
    {
        return $this->algorithm;
    }

    /**
     * @param Algorithm|null $algorithm
     * @return ProtocolInputDto
     */
    public function setAlgorithm(?Algorithm $algorithm): ProtocolInputDto
    {
        $this->algorithm = $algorithm;
        return $this;
    }

    /**
     * @return ProtocolAttachment[]
     */
    public function getProtocolAttachments(): array
    {
        return $this->protocolAttachments;
    }

    /**
     * @param ProtocolAttachment[] $protocolAttachments
     * @return ProtocolInputDto
     */
    public function setProtocolAttachments(array $protocolAttachments): ProtocolInputDto
    {
        $this->protocolAttachments = $protocolAttachments;
        return $this;
    }

}
