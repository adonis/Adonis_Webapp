<?php


namespace Webapp\Core\Dto\Protocol;

use Webapp\Core\Entity\OpenSilexInstance;

class ProtocolInputFactorDto
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var ProtocolInputModalityDto[]
     */
    private array $modalities;

    /**
     * @var int
     */
    private int $order;

    private bool $germplasm = false;
    private ?OpenSilexInstance $openSilexInstance = null;
    private ?string $openSilexUri = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ProtocolInputFactorDto
     */
    public function setName(string $name): ProtocolInputFactorDto
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ProtocolInputModalityDto[]
     */
    public function getModalities(): array
    {
        return $this->modalities;
    }

    /**
     * @param ProtocolInputModalityDto[] $modalities
     * @return ProtocolInputFactorDto
     */
    public function setModalities(array $modalities): ProtocolInputFactorDto
    {
        $this->modalities = $modalities;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return ProtocolInputFactorDto
     */
    public function setOrder(int $order): ProtocolInputFactorDto
    {
        $this->order = $order;
        return $this;
    }

    public function isGermplasm(): bool
    {
        return $this->germplasm;
    }

    public function setGermplasm(bool $germplasm): ProtocolInputFactorDto
    {
        $this->germplasm = $germplasm;
        return $this;
    }

    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): ProtocolInputFactorDto
    {
        $this->openSilexInstance = $openSilexInstance;
        return $this;
    }

    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    public function setOpenSilexUri(?string $openSilexUri): ProtocolInputFactorDto
    {
        $this->openSilexUri = $openSilexUri;
        return $this;
    }
}
