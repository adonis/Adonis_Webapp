<?php


namespace Webapp\Core\Dto\Protocol;


use Symfony\Component\Validator\Constraints as Assert;
use Webapp\Core\Validator\UniqueAttributeInParent;

class ProtocolInputPostDto extends ProtocolInputDto
{
    /**
     * @Assert\NotBlank
     * @UniqueAttributeInParent(parentsAttributes={"site.protocols"})
     * @var string
     */
    protected string $name;

}
