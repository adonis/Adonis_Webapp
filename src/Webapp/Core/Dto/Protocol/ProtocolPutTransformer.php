<?php


namespace Webapp\Core\Dto\Protocol;


use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\Treatment;

class ProtocolPutTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param ProtocolInputDto $object
     * @param string $to
     * @param array $context
     * @return object
     */
    public function transform($object, string $to, array $context = []): object
    {
        $this->validator->validate($object);

        /** @var Protocol $protocol */
        $protocol = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        $protocol
            ->setTreatments(new ArrayCollection())
            ->setSite($object->getSite())
            ->setName($object->getName())
            ->setAim($object->getAim())
            ->setComment($object->getComment())
            ->setAlgorithm($object->getAlgorithm());

        //TODO faire une vérification des conditions de l'algorithme

        foreach ($protocol->getFactors() as $factor){
            $protocol->removeFactors($factor);
        }

        $idModalityMap = [];

        foreach ($object->getFactors() as $factorDto){
            $factor = (new Factor())
                ->setOrder($factorDto->getOrder())
                ->setOpenSilexInstance($factorDto->getOpenSilexInstance())
                ->setGermplasm($factorDto->isGermplasm())
                ->setOpenSilexUri($factorDto->getOpenSilexUri())
                ->setName($factorDto->getName());
            foreach ($factorDto->getModalities() as $modalityDto){
                $modality = (new Modality())
                    ->setValue($modalityDto->getValue())
                    ->setIdentifier($modalityDto->getIdentifier())
                    ->setOpenSilexUri($modalityDto->getOpenSilexUri())
                    ->setOpenSilexInstance($modalityDto->getOpenSilexInstance())
                    ->setShortName($modalityDto->getShortName());
                $factor->addModality($modality);
                $idModalityMap[$modalityDto->getUniqId()] = $modality;
            }
            $protocol->addFactors($factor);
        }

        foreach ($object->getTreatments() as $treatmentDto){
            $treatment = (new Treatment())
                ->setShortName($treatmentDto->getShortName())
                ->setName($treatmentDto->getName())
                ->setRepetitions($treatmentDto->getRepetitions());
            foreach ($treatmentDto->getModalities() as $modalityUniqId){
                $treatment->addModalities($idModalityMap[$modalityUniqId]);
            }
            $protocol->addTreatments($treatment);
        }

        return $protocol;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Protocol) {
            return false;
        }
        return Protocol::class === $to &&
            $context['operation_type'] === OperationType::ITEM &&
            $context['item_operation_name'] === 'put' &&
            null !== ($context['input']['class'] ?? null);
    }
}
