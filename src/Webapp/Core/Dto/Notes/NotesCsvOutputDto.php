<?php


namespace Webapp\Core\Dto\Notes;

/**
 * Class NotesCsvOutputDto
 * @package Webapp\Core\Dto\BusinessObject
 */
class NotesCsvOutputDto
{
    private $platform;
    private $experiment;
    private $block;
    private $subBlock;
    private $up;
    private $treatment;
    private $shortTreatment;
    private $individual;
    private $x;
    private $y;
    private $id;
    private $dead;
    private $appeared;
    private $disapeared;
    private $factor_1;
    private $factor_1_shortName;
    private $factor_1_id;
    private $factor_2;
    private $factor_2_shortName;
    private $factor_2_id;
    private $factor_3;
    private $factor_3_shortName;
    private $factor_3_id;
    private $owner;
    private $created;
    private $note;

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return NotesCsvOutputDto
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param mixed $experiment
     * @return NotesCsvOutputDto
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param mixed $block
     * @return NotesCsvOutputDto
     */
    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubBlock()
    {
        return $this->subBlock;
    }

    /**
     * @param mixed $subBlock
     * @return NotesCsvOutputDto
     */
    public function setSubBlock($subBlock)
    {
        $this->subBlock = $subBlock;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUp()
    {
        return $this->up;
    }

    /**
     * @param mixed $up
     * @return NotesCsvOutputDto
     */
    public function setUp($up)
    {
        $this->up = $up;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTreatment()
    {
        return $this->treatment;
    }

    /**
     * @param mixed $treatment
     * @return NotesCsvOutputDto
     */
    public function setTreatment($treatment)
    {
        $this->treatment = $treatment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortTreatment()
    {
        return $this->shortTreatment;
    }

    /**
     * @param mixed $shortTreatment
     * @return NotesCsvOutputDto
     */
    public function setShortTreatment($shortTreatment)
    {
        $this->shortTreatment = $shortTreatment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIndividual()
    {
        return $this->individual;
    }

    /**
     * @param mixed $individual
     * @return NotesCsvOutputDto
     */
    public function setIndividual($individual)
    {
        $this->individual = $individual;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     * @return NotesCsvOutputDto
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     * @return NotesCsvOutputDto
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return NotesCsvOutputDto
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDead()
    {
        return $this->dead;
    }

    /**
     * @param mixed $dead
     * @return NotesCsvOutputDto
     */
    public function setDead($dead)
    {
        $this->dead = $dead;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppeared()
    {
        return $this->appeared;
    }

    /**
     * @param mixed $appeared
     * @return NotesCsvOutputDto
     */
    public function setAppeared($appeared)
    {
        $this->appeared = $appeared;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisapeared()
    {
        return $this->disapeared;
    }

    /**
     * @param mixed $disapeared
     * @return NotesCsvOutputDto
     */
    public function setDisapeared($disapeared)
    {
        $this->disapeared = $disapeared;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor1()
    {
        return $this->factor_1;
    }

    /**
     * @param mixed $factor_1
     * @return NotesCsvOutputDto
     */
    public function setFactor1($factor_1)
    {
        $this->factor_1 = $factor_1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor1ShortName()
    {
        return $this->factor_1_shortName;
    }

    /**
     * @param mixed $factor_1_shortName
     * @return NotesCsvOutputDto
     */
    public function setFactor1ShortName($factor_1_shortName)
    {
        $this->factor_1_shortName = $factor_1_shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor1Id()
    {
        return $this->factor_1_id;
    }

    /**
     * @param mixed $factor_1_id
     * @return NotesCsvOutputDto
     */
    public function setFactor1Id($factor_1_id)
    {
        $this->factor_1_id = $factor_1_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor2()
    {
        return $this->factor_2;
    }

    /**
     * @param mixed $factor_2
     * @return NotesCsvOutputDto
     */
    public function setFactor2($factor_2)
    {
        $this->factor_2 = $factor_2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor2ShortName()
    {
        return $this->factor_2_shortName;
    }

    /**
     * @param mixed $factor_2_shortName
     * @return NotesCsvOutputDto
     */
    public function setFactor2ShortName($factor_2_shortName)
    {
        $this->factor_2_shortName = $factor_2_shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor2Id()
    {
        return $this->factor_2_id;
    }

    /**
     * @param mixed $factor_2_id
     * @return NotesCsvOutputDto
     */
    public function setFactor2Id($factor_2_id)
    {
        $this->factor_2_id = $factor_2_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor3()
    {
        return $this->factor_3;
    }

    /**
     * @param mixed $factor_3
     * @return NotesCsvOutputDto
     */
    public function setFactor3($factor_3)
    {
        $this->factor_3 = $factor_3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor3ShortName()
    {
        return $this->factor_3_shortName;
    }

    /**
     * @param mixed $factor_3_shortName
     * @return NotesCsvOutputDto
     */
    public function setFactor3ShortName($factor_3_shortName)
    {
        $this->factor_3_shortName = $factor_3_shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor3Id()
    {
        return $this->factor_3_id;
    }

    /**
     * @param mixed $factor_3_id
     * @return NotesCsvOutputDto
     */
    public function setFactor3Id($factor_3_id)
    {
        $this->factor_3_id = $factor_3_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     * @return NotesCsvOutputDto
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return NotesCsvOutputDto
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     * @return NotesCsvOutputDto
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }
}
