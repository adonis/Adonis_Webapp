<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\Dto\Project;

/**
 * Class UserSelectionInput
 * @package Webapp\Core\Dto\Project
 */
class UserSelectionInput
{
    private array $users;

    /**
     * @return string[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }

    /**
     * @param string[] $users
     * @return UserSelectionInput
     */
    public function setUsers(array $users): UserSelectionInput
    {
        $this->users = $users;
        return $this;
    }
}
