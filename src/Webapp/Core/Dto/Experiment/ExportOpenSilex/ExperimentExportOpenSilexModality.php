<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

/**
 *
 */
class ExperimentExportOpenSilexModality
{
    private ?int $id = null;
    /**
     * @var string|null
     */
    private ?string $openSilexUri = null;

    /**
     * @var string
     */
    private ?string $value = null;
    private ?string $iri = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ExperimentExportOpenSilexModality
    {
        $this->id = $id;
        return $this;
    }

    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    public function setOpenSilexUri(?string $openSilexUri): ExperimentExportOpenSilexModality
    {
        $this->openSilexUri = $openSilexUri;
        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): ExperimentExportOpenSilexModality
    {
        $this->value = $value;
        return $this;
    }

    public function getIri(): ?string
    {
        return $this->iri;
    }

    public function setIri(?string $iri): ExperimentExportOpenSilexModality
    {
        $this->iri = $iri;
        return $this;
    }

}
