<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Webapp\Core\Entity\Experiment;

class ExperimentOpenSilexExportTransformer implements DataTransformerInterface
{
    private IriConverterInterface $iriConverter;
    private EntityManagerInterface $entityManager;

    public function __construct(IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        $this->iriConverter = $iriConverter;
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $object
     * @param string $to
     * @param array $context
     * @return ExperimentExportOpenSilex
     */
    public function transform($object, string $to, array $context = []): ExperimentExportOpenSilex
    {
        /** @var Experiment $object */
        $experiment = (new ExperimentExportOpenSilex())
            ->setName($object->getName())
            ->setOpenSilexInstance($object->getOpenSilexInstance());

        $protocol = new ExperimentExportOpenSilexProtocol();
        $protocolItem = $object->getProtocol();
        $protocol->setName($protocolItem->getName())->setIri($this->iriConverter->getIriFromItem($protocolItem))->setAim($protocolItem->getAim());
        $factors = [];
        foreach ($protocolItem->getFactors() as $factorItem){
            $factor = new ExperimentExportOpenSilexFactor();
            $factor
                ->setId($factorItem->getId())
                ->setName($factorItem->getName())
                ->setGermplasm($factorItem->isGermplasm())
                ->setOpenSilexUri($factorItem->getOpenSilexUri())
                ->setIri($this->iriConverter->getIriFromItem($factorItem));
            $modalities = [];
            foreach ($factorItem->getModalities() as $modalityItem){
                $modality = new ExperimentExportOpenSilexModality();
                $modality
                    ->setId($modalityItem->getId())
                    ->setValue($modalityItem->getValue())
                    ->setOpenSilexUri($modalityItem->getOpenSilexUri())
                    ->setIri($this->iriConverter->getIriFromItem($modalityItem));
                $modalities[] = $modality;
            }
            $factor->setModalities($modalities);
            $factors[] = $factor;
        }
        $treatments = [];
        foreach ($protocolItem->getTreatments() as $treatmentItem){
            $treatment = new ExperimentExportOpenSilexTreatment();
            $treatment->setIri($this->iriConverter->getIriFromItem($treatmentItem));
            $modalities = [];
            foreach ($treatmentItem->getModalities() as $modalityItem){
                $modalities[] = $this->iriConverter->getIriFromItem($modalityItem);
            }
            $treatment->setModalities($modalities);
            $treatments[] = $treatment;
        }
        $protocol->setFactors($factors)->setTreatments($treatments);

        $experiment->setProtocol($protocol);

        return $experiment;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ExperimentExportOpenSilex) {
            return false;
        }
        return ExperimentExportOpenSilex::class === $to &&
            $context['operation_type'] === OperationType::ITEM &&
            $context['item_operation_name'] === 'exportOpenSilex';
    }
}
