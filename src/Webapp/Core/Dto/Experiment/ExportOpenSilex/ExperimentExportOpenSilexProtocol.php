<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

/**
 *
 */
class ExperimentExportOpenSilexProtocol
{
    /**
     * @var string|null
     */
    private ?string $openSilexUri = null;

    /**
     * @var ExperimentExportOpenSilexFactor[]
     */
    private array $factors = [];

    /**
     * @var ExperimentExportOpenSilexTreatment[]
     */
    private array $treatments = [];

    private ?string $iri = null;
    private ?string $aim = null;
    private ?string $name = null;

    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    public function setOpenSilexUri(?string $openSilexUri): ExperimentExportOpenSilexProtocol
    {
        $this->openSilexUri = $openSilexUri;
        return $this;
    }

    public function getFactors(): array
    {
        return $this->factors;
    }

    public function setFactors(array $factors): ExperimentExportOpenSilexProtocol
    {
        $this->factors = $factors;
        return $this;
    }

    public function getTreatments(): array
    {
        return $this->treatments;
    }

    public function setTreatments(array $treatments): ExperimentExportOpenSilexProtocol
    {
        $this->treatments = $treatments;
        return $this;
    }

    public function getIri(): ?string
    {
        return $this->iri;
    }

    public function setIri(?string $iri): ExperimentExportOpenSilexProtocol
    {
        $this->iri = $iri;
        return $this;
    }

    public function getAim(): ?string
    {
        return $this->aim;
    }

    public function setAim(?string $aim): ExperimentExportOpenSilexProtocol
    {
        $this->aim = $aim;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ExperimentExportOpenSilexProtocol
    {
        $this->name = $name;
        return $this;
    }
}
