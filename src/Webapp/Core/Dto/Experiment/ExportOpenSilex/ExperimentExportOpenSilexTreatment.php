<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

/**
 *
 */
class ExperimentExportOpenSilexTreatment
{
    /**
     * @var string[]
     */
    private array $modalities = [];
    private ?string $iri = null;

    public function getModalities(): array
    {
        return $this->modalities;
    }

    public function setModalities(array $modalities): ExperimentExportOpenSilexTreatment
    {
        $this->modalities = $modalities;
        return $this;
    }

    public function getIri(): ?string
    {
        return $this->iri;
    }

    public function setIri(?string $iri): ExperimentExportOpenSilexTreatment
    {
        $this->iri = $iri;
        return $this;
    }

}
