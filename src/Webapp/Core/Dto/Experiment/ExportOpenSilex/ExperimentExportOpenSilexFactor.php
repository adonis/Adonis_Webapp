<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

/**
 *
 */
class ExperimentExportOpenSilexFactor
{
    private ?int $id = null;
    /**
     * @var string|null
     */
    private ?string $openSilexUri = null;

    /**
     * @var string
     */
    private ?string $name = null;
    private ?string $iri = null;

    /**
     * @var bool
     */
    private ?bool $germplasm = null;

    /**
     * @var ExperimentExportOpenSilexModality[]
     */
    private array $modalities = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ExperimentExportOpenSilexFactor
    {
        $this->id = $id;
        return $this;
    }

    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    public function setOpenSilexUri(?string $openSilexUri): ExperimentExportOpenSilexFactor
    {
        $this->openSilexUri = $openSilexUri;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ExperimentExportOpenSilexFactor
    {
        $this->name = $name;
        return $this;
    }

    public function getIri(): ?string
    {
        return $this->iri;
    }

    public function setIri(?string $iri): ExperimentExportOpenSilexFactor
    {
        $this->iri = $iri;
        return $this;
    }

    public function getGermplasm(): ?bool
    {
        return $this->germplasm;
    }

    public function setGermplasm(?bool $germplasm): ExperimentExportOpenSilexFactor
    {
        $this->germplasm = $germplasm;
        return $this;
    }

    public function getModalities(): array
    {
        return $this->modalities;
    }

    public function setModalities(array $modalities): ExperimentExportOpenSilexFactor
    {
        $this->modalities = $modalities;
        return $this;
    }



}
