<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

use Webapp\Core\Entity\OpenSilexInstance;

class ExperimentExportOpenSilex
{
    private ?OpenSilexInstance $openSilexInstance = null;

    private ?string $name = null;

    private ?ExperimentExportOpenSilexProtocol $protocol = null;

    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): ExperimentExportOpenSilex
    {
        $this->openSilexInstance = $openSilexInstance;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ExperimentExportOpenSilex
    {
        $this->name = $name;
        return $this;
    }

    public function getProtocol(): ?ExperimentExportOpenSilexProtocol
    {
        return $this->protocol;
    }

    public function setProtocol(?ExperimentExportOpenSilexProtocol $protocol): ExperimentExportOpenSilex
    {
        $this->protocol = $protocol;
        return $this;
    }


}
