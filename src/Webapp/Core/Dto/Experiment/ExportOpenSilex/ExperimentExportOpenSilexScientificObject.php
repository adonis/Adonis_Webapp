<?php

namespace Webapp\Core\Dto\Experiment\ExportOpenSilex;

use DateTime;

/**
 *
 */
class ExperimentExportOpenSilexScientificObject
{
    private ?DateTime $appeared;
    private ?DateTime $disappeared;
    private string $number;
    private ?string $treatment;
    private string $parent;
    private string $iri;
    private ?string $geometry;

    public function getAppeared(): ?DateTime
    {
        return $this->appeared;
    }

    public function setAppeared(?DateTime $appeared): ExperimentExportOpenSilexScientificObject
    {
        $this->appeared = $appeared;
        return $this;
    }

    public function getDisappeared(): ?DateTime
    {
        return $this->disappeared;
    }

    public function setDisappeared(?DateTime $disappeared): ExperimentExportOpenSilexScientificObject
    {
        $this->disappeared = $disappeared;
        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): ExperimentExportOpenSilexScientificObject
    {
        $this->number = $number;
        return $this;
    }

    public function getTreatment(): ?string
    {
        return $this->treatment;
    }

    public function setTreatment(?string $treatment): ExperimentExportOpenSilexScientificObject
    {
        $this->treatment = $treatment;
        return $this;
    }

    public function getParent(): string
    {
        return $this->parent;
    }

    public function setParent(string $parent): ExperimentExportOpenSilexScientificObject
    {
        $this->parent = $parent;
        return $this;
    }

    public function getIri(): string
    {
        return $this->iri;
    }

    public function setIri(string $iri): ExperimentExportOpenSilexScientificObject
    {
        $this->iri = $iri;
        return $this;
    }

    public function getGeometry(): ?string
    {
        return $this->geometry;
    }

    public function setGeometry(?string $geometry): ExperimentExportOpenSilexScientificObject
    {
        $this->geometry = $geometry;
        return $this;
    }
}
