<?php

namespace Webapp\Core\Dto\Experiment;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Webapp\Core\Entity\Algorithm;
use Webapp\Core\Entity\AlgorithmParameter;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;

/**
 * Class ExperimentPostTransformer
 * @package Webapp\Core\Dto\Experiment
 */
class ExperimentPostTransformer implements DataTransformerInterface
{
    private IriConverterInterface $iriConverter;

    private ContainerBagInterface $params;

    public function __construct(IriConverterInterface $iriConverter, ContainerBagInterface $params)
    {
        $this->iriConverter = $iriConverter;
        $this->params = $params;
    }

    /**
     * @param ExperimentInputDto $object
     * @param string $to
     * @param array $context
     * @return object
     */
    public function transform($object, string $to, array $context = []): object
    {
        $experiment = (new Experiment())
            ->setName($object->getName())
            ->setIndividualUP($object->isIndividualUP())
            ->setPlatform(null)
            ->setSite($this->iriConverter->getItemFromIri($object->getSite()))
            ->setProtocol($this->iriConverter->getItemFromIri($object->getProtocol()))
            ->setComment($object->getComment());
        foreach ($object->getExperimentAttachments() as $experimentAttachment) {
            $experiment->addExperimentAttachment($experimentAttachment);
        }

        $unitPlotLength = 0;
        /** @var Treatment $treatment */
        foreach ($experiment->getProtocol()->getTreatments() as $treatment) {
            $unitPlotLength += $treatment->getRepetitions();
        }
        $useSubBlocs = isset($object->getAlgorithmConfig()['subBlockNumber']) && $object->getAlgorithmConfig()['subBlockNumber'] > 0;
        $nbrPUByParent = ceil($unitPlotLength / ((!$useSubBlocs ? 1 : $object->getAlgorithmConfig()['subBlockNumber']) * $object->getAlgorithmConfig()['blockNumber']));
        $unitPlotLength = $object->isIndividualUP() ? $object->getAlgorithmConfig()['individualByLine'] : 1;
        $unitPlotHeight = $object->isIndividualUP() ? ceil($object->getAlgorithmConfig()['individualsByPuNumber'] / $object->getAlgorithmConfig()['individualByLine']) : 1;
        $subBlockLength = $object->getAlgorithmConfig()['puByLine'] * $unitPlotLength;
        $subBlockHeight = ceil($nbrPUByParent / $object->getAlgorithmConfig()['puByLine']) * $unitPlotHeight;
        $blockLength = !$useSubBlocs ?
            $subBlockLength :
            $object->getAlgorithmConfig()['subBlockByLine'] * $subBlockLength;
        $blockHeight = !$useSubBlocs ?
            $subBlockHeight :
            ceil($object->getAlgorithmConfig()['subBlockNumber'] / $object->getAlgorithmConfig()['subBlockByLine']) * $subBlockHeight;

        $unitPlots = $this->placeWithoutAlgorithm($experiment, $object, $nbrPUByParent, $unitPlotLength, $unitPlotHeight, $subBlockLength, $subBlockHeight, $blockLength, $blockHeight, $useSubBlocs);

        if ($experiment->getProtocol()->getAlgorithm() !== null) {
            $this->correctUnitPlotTreatment($experiment, $object, $unitPlots, $useSubBlocs);
        }

        return $experiment;
    }

    /**
     * @param Experiment $experiment
     * @param ExperimentInputDto $object
     * @param $nbrPUByParent
     * @param $unitPlotLength
     * @param $unitPlotHeight
     * @param $subBlockLength
     * @param $subBlockHeight
     * @param $blockLength
     * @param $blockHeight
     * @return void
     */
    private function placeWithoutAlgorithm(Experiment $experiment, ExperimentInputDto $object, $nbrPUByParent, $unitPlotLength, $unitPlotHeight, $subBlockLength, $subBlockHeight, $blockLength, $blockHeight, $useSubBlocks): array
    {
        // Create Unit Plots
        $unitPlots = [];
        $unitPlotNumber = 1;
        /** @var Treatment $treatment */
        foreach ($experiment->getProtocol()->getTreatments() as $treatment) {
            foreach (range(1, $treatment->getRepetitions()) as $i) {
                $unitPlots[] = ($object->isIndividualUP() ? new UnitPlot() : new SurfacicUnitPlot())
                    ->setTreatment($treatment)
                    ->setNumber($unitPlotNumber);
                $unitPlotNumber++;

            }
        }

        $xBloc = 1;
        $yBloc = 1;
        $blocs = [];
        foreach (range(1, $object->getAlgorithmConfig()['blockNumber']) as $blockNumber) {
            $blockUnitPlots = null;

            // Create SubBlocks
            $subBlocks = null;
            if ($useSubBlocks) {
                $xSubBloc = $xBloc;
                $ySubBloc = $yBloc;
                $subBlocks = [];
                foreach (range(1, $object->getAlgorithmConfig()['subBlockNumber']) as $subBlockindex) {
                    $subBlockNumber = ($blockNumber - 1) * $object->getAlgorithmConfig()['subBlockNumber'] + $subBlockindex;
                    $unitPlotStartPoint = ($subBlockNumber - 1) * $nbrPUByParent;
                    $subBlockUnitPlots = array_slice($unitPlots, $unitPlotStartPoint, $nbrPUByParent);

                    $this->populatePu($xSubBloc, $ySubBloc, $subBlockUnitPlots, $object->getAlgorithmConfig(), $unitPlotLength, $unitPlotHeight, $object->isIndividualUP());

                    $subBlock = (new SubBlock())
                        ->setNumber($subBlockNumber);
                    if ($object->isIndividualUP()) {
                        $subBlock->setUnitPlots($subBlockUnitPlots ?? new ArrayCollection());
                    } else {
                        $subBlock->setSurfacicUnitPlots($subBlockUnitPlots ?? new ArrayCollection());
                    }
                    $subBlocks[] = $subBlock;

                    $xSubBloc += $subBlockLength;
                    if (($xSubBloc - $xBloc) / $subBlockLength >= $object->getAlgorithmConfig()['subBlockByLine']) {
                        $xSubBloc = $xBloc;
                        $ySubBloc += $subBlockHeight;
                    }
                }
            } else {
                $unitPlotStartPoint = ($blockNumber - 1) * $nbrPUByParent;
                $blockUnitPlots = array_slice($unitPlots, $unitPlotStartPoint, $nbrPUByParent);
                $this->populatePu($xBloc, $yBloc, $blockUnitPlots, $object->getAlgorithmConfig(), $unitPlotLength, $unitPlotHeight, $object->isIndividualUP());
            }

            // Create Block
            $block = (new Block())
                ->setSubBlocks($subBlocks ?? new ArrayCollection())
                ->setNumber($blockNumber);
            if ($object->isIndividualUP()) {
                $block->setUnitPlots($blockUnitPlots ?? new ArrayCollection());
            } else {
                $block->setSurfacicUnitPlots($blockUnitPlots ?? new ArrayCollection());
            }

            $blocs[] = $block;

            $xBloc += $blockLength;
            if (isset($object->getAlgorithmConfig()['blocsByLine']) && $xBloc / $blockLength > $object->getAlgorithmConfig()['blocsByLine']) {
                $xBloc = 1;
                $yBloc += $blockHeight;
            }
        }
        $experiment->setBlocks($blocs);
        return $unitPlots;
    }

    private function populatePu($xParent, $yParent, $unitPlots, $config, $unitPlotLength, $unitPlotHeight, bool $individualPu)
    {
        $xPU = $xParent;
        $yPU = $yParent;

        foreach ($unitPlots as $unitPlot) {
            // Create Individuals
            $individuals = [];
            $x = $xPU;
            $y = $yPU;
            if ($individualPu) {
                /** @var UnitPlot $unitPlot */
                foreach (range(1, $config['individualsByPuNumber']) as $individualIndex) {
                    $individuals[] = (new Individual())
                        ->setX($x)
                        ->setY($y)
                        ->setNumber(($unitPlot->getNumber() - 1) * $config['individualsByPuNumber'] + $individualIndex);
                    $x++;
                    if ($x - $xPU >= $config['individualByLine']) {
                        $x = $xPU;
                        $y++;
                    }
                }
                $unitPlot->setIndividuals($individuals);
            } else {
                /** @var SurfacicUnitPlot $unitPlot */
                $unitPlot->setX($x)
                    ->setY($y);
            }
            $xPU += $unitPlotLength;
            if (($xPU - $xParent) / $unitPlotLength >= $config['puByLine']) {
                $xPU = $xParent;
                $yPU += $unitPlotHeight;
            }
        }
    }

    /**
     * @param Experiment $experiment
     * @param ExperimentInputDto $object
     * @param UnitPlot[] | SurfacicUnitPlot[] $unitPlots
     * @return void
     * @throws InternalErrorException
     */
    private function correctUnitPlotTreatment(Experiment $experiment, ExperimentInputDto $object, array $unitPlots, bool $useSubBlocs): void
    {
        $outputFile = 'output.out';

        if ($experiment->getProtocol()->getAlgorithm()->getName() === Algorithm::SPECIAL_ALGORITHM_SPLIT_PLOT) {
            $this->executeSplitPlotAlgorithm($experiment, $object, $outputFile);
        } else {
            $this->executeImportedAlgorithm($experiment, $object, $outputFile);
        }

        $handle = fopen($outputFile, "r");
        if ($handle) {
            // Skip the 1st header line
            fgets($handle);

            $blocks = [];
            $subBlocks = [];
            foreach ($experiment->getBlocks() as $block) {
                $blocks[intval($block->getNumber())] = $block;
                $block->setUnitPlots(new ArrayCollection());
                $block->setSurfacicUnitPlots(new ArrayCollection());
                if ($useSubBlocs) {
                    foreach ($block->getSubBlocks() as $subBlock) {
                        $subBlocks[intval($subBlock->getNumber())] = $subBlock;
                        $subBlock->setUnitPlots(new ArrayCollection());
                        $subBlock->setSurfacicUnitPlots(new ArrayCollection());
                    }
                }
                $block->setSubBlocks(new ArrayCollection());
            }
            $unitPlotsReindex = [];
            foreach ($unitPlots as $unitPlot) {
                $unitPlotsReindex[intval($unitPlot->getNumber())] = $unitPlot;
            }
            $unitPlotNumber = 1;
            while (($line = fgets($handle)) !== false) {
                $lineTab = explode(';', $line);
                if (count($lineTab) !== 9) {
                    break;
                }
                $blocNumber = intval($lineTab[0]);
                if ($useSubBlocs) {
                    $subBlocNumber = intval($lineTab[1]) + $object->getAlgorithmConfig()['subBlockNumber'] * ($blocNumber - 1);
                }
                $treatmentIndex = intval($lineTab[3]);
                $unitPlot = $unitPlotsReindex[$unitPlotNumber++];

                $unitPlot->setTreatment($experiment->getProtocol()->getTreatments()->get($treatmentIndex - 1));
                if ($useSubBlocs) {
                    $blocks[$blocNumber]->addSubBlocks($subBlocks[$subBlocNumber]);
                    if ($experiment->isIndividualUP()) {
                        $subBlocks[$subBlocNumber]->addUnitPlots($unitPlot);
                    } else {
                        $subBlocks[$subBlocNumber]->addSurfacicUnitPlots($unitPlot);
                    }
                } else {
                    if ($experiment->isIndividualUP()) {
                        $blocks[$blocNumber]->addUnitPlots($unitPlot);
                    } else {
                        $blocks[$blocNumber]->addSurfacicUnitPlots($unitPlot);
                    }
                }

            }

            fclose($handle);
        } else {
            throw new InternalErrorException("Cannot find algorithm output file : $outputFile");
        }
    }

    /**
     * @param Experiment $experiment
     * @param ExperimentInputDto $object
     * @param string $outputFile
     */
    private function executeImportedAlgorithm(Experiment $experiment, ExperimentInputDto $object, string $outputFile)
    {
        $parameters = [];
        /** @var AlgorithmParameter[] $algorithmParameters */
        $algorithmParameters = $experiment->getProtocol()->getAlgorithm()->getAlgorithmParameters()->toArray();
        usort($algorithmParameters, function (AlgorithmParameter $a, AlgorithmParameter $b) {
            return $a->getOrder() - $b->getOrder();
        });
        foreach ($algorithmParameters as $parameter) {
            switch ($parameter->getSpecialParam()) {
                case AlgorithmParameter::SPECIAL_PARAM_NBR_TREATMENT:
                    $parameters[] = $experiment->getProtocol()->getTreatments()->count();
                    break;
                case AlgorithmParameter::SPECIAL_PARAM_CONSTANT_REPETITION:
                    $parameters[] = $experiment->getProtocol()->getTreatments()->first()->getRepetitions();
                    break;
                case AlgorithmParameter::SPECIAL_PARAM_NBR_FACTOR:
                    $parameters[] = $experiment->getProtocol()->getFactors()->count();
                    break;
                default:
                    $parameters[] = $object->getAlgorithmConfig()[$parameter->getName()];
            }
        }

        // Execute algorithm
        chdir($this->params->get('algorithm.dir'));
        $script = "Rscript " . $experiment->getProtocol()->getAlgorithm()->getScriptName() . " " . implode(" ", $parameters) . " " . $outputFile;
        exec($script);
    }

    /**
     * @param Experiment $experiment
     * @param ExperimentInputDto $object
     * @param string $outputFile
     */
    private function executeSplitPlotAlgorithm(Experiment $experiment, ExperimentInputDto $object, string $outputFile)
    {
        chdir($this->params->get('algorithm.dir'));
        $parameters = [];

        if ($object->getAlgorithmConfig()["extraParameters"]["independentFactors"]) {
            $comparator = function (Factor $a, Factor $b) use ($object) {
                $weight = function (Factor $factor) use ($object) {
                    switch ($factor->getName()) {
                        case $object->getAlgorithmConfig()["extraParameters"]["factor1"]:
                            return 1;
                        case $object->getAlgorithmConfig()["extraParameters"]["factor2"]:
                            return 2;
                        case $object->getAlgorithmConfig()["extraParameters"]["factor3"]:
                            return 3;
                        default:
                            return 0;
                    }
                };
                return $weight($a) - $weight($b);
            };
            $factors = $experiment->getProtocol()->getFactors()->toArray();
            usort($factors, $comparator);
            $modalityVector = [];
            /** @var Factor $factor */
            foreach ($factors as $factor) {
                $modalityVector[] = $factor->getModalities()->count();
            }
            $parameters[] = implode(',', $modalityVector);
        } else {
            /** @var Factor[] $factors */
            $factors = $experiment->getProtocol()->getFactors()->toArray();
            $isGroupPos1 = $object->getAlgorithmConfig()["extraParameters"]["factor1"] === implode(" - ", $object->getAlgorithmConfig()["extraParameters"]["groupFactor"]);
            usort($factors, function (Factor $a, Factor $b) use ($isGroupPos1, $object) {
                return $isGroupPos1 ?
                    ($a->getName() === $object->getAlgorithmConfig()["extraParameters"]["factor2"] ? 1 : -1) :
                    ($a->getName() === $object->getAlgorithmConfig()["extraParameters"]["factor1"] ? -1 : 1);
            });
            $modalityVector = [];
            if ($isGroupPos1) {
                $modalityVector[] = $factors[0]->getModalities()->count() * $factors[1]->getModalities()->count();
                $modalityVector[] = $factors[2]->getModalities()->count();
            } else {
                $modalityVector[] = $factors[0]->getModalities()->count();
                $modalityVector[] = $factors[1]->getModalities()->count() * $factors[2]->getModalities()->count();
            }
            $parameters[] = implode(',', $modalityVector);
        }

        $parameters[] = $object->getAlgorithmConfig()["blockNumber"];

        $MFTDataFile = "matMFT.dat";
        $info = "\"m1\";\"m2\";\"m3\";\"Trt\";\"ind\"\n";
        $fillInfo = function (array $factors, array $modalities = []) use ($experiment, &$fillInfo, &$info) {
            if (count($factors) === 0) {
                $ind = 1;
                foreach ($experiment->getProtocol()->getTreatments() as $treatment) {
                    $bool = true;
                    foreach ($modalities as $modality) {
                        if (!$treatment->getModalities()->contains($modality[1])) {
                            $bool = false;
                            break;
                        }
                    }
                    if ($bool) {
                        $m1 = $modalities[0][0];
                        $m2 = $modalities[1][0];
                        $m3 = isset($modalities[2]) ? $modalities[2][0] : 0;
                        $trt = $treatment->getShortName();
                        $info .= "$m1;$m2;$m3;$trt;$ind\n";
                    }
                    $ind++;
                }
            } else {
                /** @var Factor $factor */
                $factor = $factors[0];
                $modalityCnt = 1;
                foreach ($factor->getModalities() as $modality) {
                    $fillInfo(array_slice($factors, 1), [...$modalities, [$modalityCnt++, $modality]]);
                }
            }
        };
        $fillInfo($factors);
        file_put_contents($MFTDataFile, $info);
        $parameters[] = $MFTDataFile;

        // Execute algorithm
        $script = "Rscript " . $experiment->getProtocol()->getAlgorithm()->getScriptName() . " " . implode(" ", $parameters) . " " . $outputFile;
        exec($script);
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Experiment) {
            return false;
        }
        return Experiment::class === $to &&
            $context['operation_type'] === OperationType::COLLECTION &&
            $context['collection_operation_name'] === 'post' &&
            null !== ($context['input']['class'] ?? null);
    }
}
