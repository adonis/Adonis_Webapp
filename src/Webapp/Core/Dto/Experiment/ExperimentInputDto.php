<?php


namespace Webapp\Core\Dto\Experiment;

use Webapp\Core\Entity\Attachment\ExperimentAttachment;

/**
 * Class ExperimentInputDto
 * @package Webapp\Core\Dto\Experiment
 */
class ExperimentInputDto
{
    private string $name;
    private string $protocol;
    private bool $individualUP;
    private string $comment;
    private string $site;
    private $algorithmConfig;
    /**
     * @var array<ExperimentAttachment>
     */
    private $experimentAttachments;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ExperimentInputDto
     */
    public function setName(string $name): ExperimentInputDto
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getProtocol(): string
    {
        return $this->protocol;
    }

    /**
     * @param string $protocol
     * @return ExperimentInputDto
     */
    public function setProtocol(string $protocol): ExperimentInputDto
    {
        $this->protocol = $protocol;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIndividualUP(): bool
    {
        return $this->individualUP;
    }

    /**
     * @param bool $individualUP
     * @return ExperimentInputDto
     */
    public function setIndividualUP(bool $individualUP): ExperimentInputDto
    {
        $this->individualUP = $individualUP;
        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     * @return ExperimentInputDto
     */
    public function setComment(string $comment): ExperimentInputDto
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @param string $site
     * @return ExperimentInputDto
     */
    public function setSite(string $site): ExperimentInputDto
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAlgorithmConfig()
    {
        return $this->algorithmConfig;
    }

    /**
     * @param mixed $algorithmConfig
     * @return ExperimentInputDto
     */
    public function setAlgorithmConfig($algorithmConfig)
    {
        $this->algorithmConfig = $algorithmConfig;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperimentAttachments()
    {
        return $this->experimentAttachments;
    }

    /**
     * @param mixed $experimentAttachments
     * @return ExperimentInputDto
     */
    public function setExperimentAttachments($experimentAttachments)
    {
        $this->experimentAttachments = $experimentAttachments;
        return $this;
    }


}
