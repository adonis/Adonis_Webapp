<?php


namespace Webapp\Core\Dto\BusinessObject;

/**
 * Class BusinessObjectCsvOutputDto
 * @package Webapp\Core\Dto\BusinessObject
 */
class BusinessObjectCsvOutputDto
{
    private $x;
    private $y;
    private $id;
    private $up;
    private $dead;
    private $appeared;
    private $disapeared;
    private $treatment;
    private $block;
    private $subBlock;
    private $experiment;
    private $platform;
    private $shortTreatment;
    private $factor_1;
    private $factor_1_shortName;
    private $factor_1_id;
    private $factor_2;
    private $factor_2_shortName;
    private $factor_2_id;
    private $factor_3;
    private $factor_3_shortName;
    private $factor_3_id;
    private $ZHE;
    private $individualGeometry;
    private $upGeometry;
    private $blockGeometry;
    private $subBlockGeometry;
    private $experimentGeometry;
    private $ZHEGeometry;

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param mixed $x
     * @return BusinessObjectCsvOutputDto
     */
    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param mixed $y
     * @return BusinessObjectCsvOutputDto
     */
    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BusinessObjectCsvOutputDto
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUp()
    {
        return $this->up;
    }

    /**
     * @param mixed $up
     * @return BusinessObjectCsvOutputDto
     */
    public function setUp($up)
    {
        $this->up = $up;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDead()
    {
        return $this->dead;
    }

    /**
     * @param mixed $dead
     * @return BusinessObjectCsvOutputDto
     */
    public function setDead($dead)
    {
        $this->dead = $dead;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppeared()
    {
        return $this->appeared;
    }

    /**
     * @param mixed $appeared
     * @return BusinessObjectCsvOutputDto
     */
    public function setAppeared($appeared)
    {
        $this->appeared = $appeared;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisapeared()
    {
        return $this->disapeared;
    }

    /**
     * @param mixed $disapeared
     * @return BusinessObjectCsvOutputDto
     */
    public function setDisapeared($disapeared)
    {
        $this->disapeared = $disapeared;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTreatment()
    {
        return $this->treatment;
    }

    /**
     * @param mixed $treatment
     * @return BusinessObjectCsvOutputDto
     */
    public function setTreatment($treatment)
    {
        $this->treatment = $treatment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlock()
    {
        return $this->block;
    }

    /**
     * @param mixed $block
     * @return BusinessObjectCsvOutputDto
     */
    public function setBlock($block)
    {
        $this->block = $block;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubBlock()
    {
        return $this->subBlock;
    }

    /**
     * @param mixed $subBlock
     * @return BusinessObjectCsvOutputDto
     */
    public function setSubBlock($subBlock)
    {
        $this->subBlock = $subBlock;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperiment()
    {
        return $this->experiment;
    }

    /**
     * @param mixed $experiment
     * @return BusinessObjectCsvOutputDto
     */
    public function setExperiment($experiment)
    {
        $this->experiment = $experiment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     * @return BusinessObjectCsvOutputDto
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortTreatment()
    {
        return $this->shortTreatment;
    }

    /**
     * @param mixed $shortTreatment
     * @return BusinessObjectCsvOutputDto
     */
    public function setShortTreatment($shortTreatment)
    {
        $this->shortTreatment = $shortTreatment;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor1()
    {
        return $this->factor_1;
    }

    /**
     * @param mixed $factor_1
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor1($factor_1)
    {
        $this->factor_1 = $factor_1;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor1ShortName()
    {
        return $this->factor_1_shortName;
    }

    /**
     * @param mixed $factor_1_shortName
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor1ShortName($factor_1_shortName)
    {
        $this->factor_1_shortName = $factor_1_shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor1Id()
    {
        return $this->factor_1_id;
    }

    /**
     * @param mixed $factor_1_id
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor1Id($factor_1_id)
    {
        $this->factor_1_id = $factor_1_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor2()
    {
        return $this->factor_2;
    }

    /**
     * @param mixed $factor_2
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor2($factor_2)
    {
        $this->factor_2 = $factor_2;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor2ShortName()
    {
        return $this->factor_2_shortName;
    }

    /**
     * @param mixed $factor_2_shortName
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor2ShortName($factor_2_shortName)
    {
        $this->factor_2_shortName = $factor_2_shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor2Id()
    {
        return $this->factor_2_id;
    }

    /**
     * @param mixed $factor_2_id
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor2Id($factor_2_id)
    {
        $this->factor_2_id = $factor_2_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor3()
    {
        return $this->factor_3;
    }

    /**
     * @param mixed $factor_3
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor3($factor_3)
    {
        $this->factor_3 = $factor_3;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor3ShortName()
    {
        return $this->factor_3_shortName;
    }

    /**
     * @param mixed $factor_3_shortName
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor3ShortName($factor_3_shortName)
    {
        $this->factor_3_shortName = $factor_3_shortName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFactor3Id()
    {
        return $this->factor_3_id;
    }

    /**
     * @param mixed $factor_3_id
     * @return BusinessObjectCsvOutputDto
     */
    public function setFactor3Id($factor_3_id)
    {
        $this->factor_3_id = $factor_3_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZHE()
    {
        return $this->ZHE;
    }

    /**
     * @param mixed $ZHE
     * @return BusinessObjectCsvOutputDto
     */
    public function setZHE($ZHE)
    {
        $this->ZHE = $ZHE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIndividualGeometry()
    {
        return $this->individualGeometry;
    }

    /**
     * @param mixed $individualGeometry
     * @return BusinessObjectCsvOutputDto
     */
    public function setIndividualGeometry($individualGeometry)
    {
        $this->individualGeometry = $individualGeometry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpGeometry()
    {
        return $this->upGeometry;
    }

    /**
     * @param mixed $upGeometry
     * @return BusinessObjectCsvOutputDto
     */
    public function setUpGeometry($upGeometry)
    {
        $this->upGeometry = $upGeometry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlockGeometry()
    {
        return $this->blockGeometry;
    }

    /**
     * @param mixed $blockGeometry
     * @return BusinessObjectCsvOutputDto
     */
    public function setBlockGeometry($blockGeometry)
    {
        $this->blockGeometry = $blockGeometry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubBlockGeometry()
    {
        return $this->subBlockGeometry;
    }

    /**
     * @param mixed $subBlockGeometry
     * @return BusinessObjectCsvOutputDto
     */
    public function setSubBlockGeometry($subBlockGeometry)
    {
        $this->subBlockGeometry = $subBlockGeometry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExperimentGeometry()
    {
        return $this->experimentGeometry;
    }

    /**
     * @param mixed $experimentGeometry
     * @return BusinessObjectCsvOutputDto
     */
    public function setExperimentGeometry($experimentGeometry)
    {
        $this->experimentGeometry = $experimentGeometry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZHEGeometry()
    {
        return $this->ZHEGeometry;
    }

    /**
     * @param mixed $ZHEGeometry
     * @return BusinessObjectCsvOutputDto
     */
    public function setZHEGeometry($ZHEGeometry)
    {
        $this->ZHEGeometry = $ZHEGeometry;
        return $this;
    }

}
