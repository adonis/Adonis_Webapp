<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

use DateTime;

class MeasureExportOpenSilex
{
    private ?string $value;

    private ?DateTime $timestamp;

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): MeasureExportOpenSilex
    {
        $this->value = $value;
        return $this;
    }

    public function getTimestamp(): ?DateTime
    {
        return $this->timestamp;
    }

    public function setTimestamp(?DateTime $timestamp): MeasureExportOpenSilex
    {
        $this->timestamp = $timestamp;
        return $this;
    }


}
