<?php


namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;


use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use Webapp\Core\Entity\ProjectData;

class ProjectDataOpenSilexImportTransformer implements DataTransformerInterface
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param object $object
     * @param string $to
     * @param array $context
     * @return object
     */
    public function transform($object, string $to, array $context = []): object
    {
        /** @var ProjectDataExportOpenSilex $object */
        $this->validator->validate($object);

        /** @var ProjectData $projectData */
        $projectData = $context[AbstractItemNormalizer::OBJECT_TO_POPULATE];
        $projectData->setOpenSilexInstance($object->getOpenSilexInstance());

        return $projectData;
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ProjectData) {
            return false;
        }
        return ProjectData::class === $to &&
            $context['operation_type'] === OperationType::ITEM &&
            $context['item_operation_name'] === 'updateOpenSilexUri' &&
            null !== ($context['input']['class'] ?? null);
    }
}
