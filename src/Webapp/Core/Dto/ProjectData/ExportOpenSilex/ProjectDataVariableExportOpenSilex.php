<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

use Webapp\Core\Entity\OpenSilexInstance;

class ProjectDataVariableExportOpenSilex
{
    private ?OpenSilexInstance $openSilexInstance = null;
    private ?string $openSilexUri = null;
    private string $name = '';
    private ?string $unit = '';

    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): ProjectDataVariableExportOpenSilex
    {
        $this->openSilexInstance = $openSilexInstance;
        return $this;
    }

    public function getOpenSilexUri(): ?string
    {
        return $this->openSilexUri;
    }

    public function setOpenSilexUri(?string $openSilexUri): ProjectDataVariableExportOpenSilex
    {
        $this->openSilexUri = $openSilexUri;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): ProjectDataVariableExportOpenSilex
    {
        $this->name = $name;
        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): ProjectDataVariableExportOpenSilex
    {
        $this->unit = $unit;
        return $this;
    }


}
