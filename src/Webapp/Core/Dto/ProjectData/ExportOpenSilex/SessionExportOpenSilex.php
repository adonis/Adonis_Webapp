<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

use DateTime;

class SessionExportOpenSilex
{

    private ?string $user = null;

    private ?DateTime $debut = null;

    private ?DateTime $fin = null;

    private array $fields = [];

    public function getUser(): ?string
    {
        return $this->user;
    }

    public function setUser(?string $user): SessionExportOpenSilex
    {
        $this->user = $user;
        return $this;
    }

    public function getDebut(): ?DateTime
    {
        return $this->debut;
    }

    public function setDebut(?DateTime $debut): SessionExportOpenSilex
    {
        $this->debut = $debut;
        return $this;
    }

    public function getFin(): ?DateTime
    {
        return $this->fin;
    }

    public function setFin(?DateTime $fin): SessionExportOpenSilex
    {
        $this->fin = $fin;
        return $this;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    public function setFields(array $fields): SessionExportOpenSilex
    {
        $this->fields = $fields;
        return $this;
    }

}
