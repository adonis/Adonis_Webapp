<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

class FieldExportOpenSilex
{
    private array $measures = [];

    private ?string $variableOpenSilexIri = null;

    private ?string $targetIri = null;

    public function getMeasures(): array
    {
        return $this->measures;
    }

    public function setMeasures(array $measures): FieldExportOpenSilex
    {
        $this->measures = $measures;
        return $this;
    }

    public function getVariableOpenSilexIri(): ?string
    {
        return $this->variableOpenSilexIri;
    }

    public function setVariableOpenSilexIri(?string $variableOpenSilexIri): FieldExportOpenSilex
    {
        $this->variableOpenSilexIri = $variableOpenSilexIri;
        return $this;
    }

    public function getTargetIri(): ?string
    {
        return $this->targetIri;
    }

    public function setTargetIri(?string $targetIri): FieldExportOpenSilex
    {
        $this->targetIri = $targetIri;
        return $this;
    }

}
