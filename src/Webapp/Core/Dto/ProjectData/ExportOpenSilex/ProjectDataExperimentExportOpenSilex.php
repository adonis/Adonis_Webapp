<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

use Webapp\Core\Entity\OpenSilexInstance;

class ProjectDataExperimentExportOpenSilex
{
    private ?OpenSilexInstance $openSilexInstance = null;
    private string $protocolIri = '';
    private string $name = '';

    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): ProjectDataExperimentExportOpenSilex
    {
        $this->openSilexInstance = $openSilexInstance;
        return $this;
    }

    public function getProtocolIri(): string
    {
        return $this->protocolIri;
    }

    public function setProtocolIri(string $protocolIri): ProjectDataExperimentExportOpenSilex
    {
        $this->protocolIri = $protocolIri;
        return $this;
    }


    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): ProjectDataExperimentExportOpenSilex
    {
        $this->name = $name;
        return $this;
    }


}
