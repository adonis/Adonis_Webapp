<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

use DateTime;
use Webapp\Core\Entity\OpenSilexInstance;

class ProjectDataExportOpenSilex
{
    private ?OpenSilexInstance $openSilexInstance = null;

    private ?string $name = null;

    private ?array $sessions = [];

    private ?ProjectExportOpenSilex $project = null;

    private ?DateTime $end = null;

    private ?OperatorExportOpenSilex $operator = null;

    public function getOpenSilexInstance(): ?OpenSilexInstance
    {
        return $this->openSilexInstance;
    }

    public function setOpenSilexInstance(?OpenSilexInstance $openSilexInstance): ProjectDataExportOpenSilex
    {
        $this->openSilexInstance = $openSilexInstance;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): ProjectDataExportOpenSilex
    {
        $this->name = $name;
        return $this;
    }

    public function getSessions(): ?array
    {
        return $this->sessions;
    }

    public function setSessions(?array $sessions): ProjectDataExportOpenSilex
    {
        $this->sessions = $sessions;
        return $this;
    }

    public function getProject(): ?ProjectExportOpenSilex
    {
        return $this->project;
    }

    public function setProject(?ProjectExportOpenSilex $project): ProjectDataExportOpenSilex
    {
        $this->project = $project;
        return $this;
    }

    public function getEnd(): ?DateTime
    {
        return $this->end;
    }

    public function setEnd(?DateTime $end): ProjectDataExportOpenSilex
    {
        $this->end = $end;
        return $this;
    }

    public function getOperator(): ?OperatorExportOpenSilex
    {
        return $this->operator;
    }

    public function setOperator(?OperatorExportOpenSilex $operator): ProjectDataExportOpenSilex
    {
        $this->operator = $operator;
        return $this;
    }

}
