<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

class ProjectExportOpenSilex
{

    private array $experiments = [];

    private array $variables = [];

    public function getExperiments(): array
    {
        return $this->experiments;
    }

    public function setExperiments(array $experiments): ProjectExportOpenSilex
    {
        $this->experiments = $experiments;
        return $this;
    }

    public function getVariables(): array
    {
        return $this->variables;
    }

    public function setVariables(array $variables): ProjectExportOpenSilex
    {
        $this->variables = $variables;
        return $this;
    }


}
