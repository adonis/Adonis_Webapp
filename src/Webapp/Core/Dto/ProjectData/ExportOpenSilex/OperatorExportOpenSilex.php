<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

class OperatorExportOpenSilex
{

    private ?string $name = null;
    private ?string $surname = null;
    private ?string $email = null;
    private ?string $iri = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): OperatorExportOpenSilex
    {
        $this->name = $name;
        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): OperatorExportOpenSilex
    {
        $this->surname = $surname;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): OperatorExportOpenSilex
    {
        $this->email = $email;
        return $this;
    }

    public function getIri(): ?string
    {
        return $this->iri;
    }

    public function setIri(?string $iri): OperatorExportOpenSilex
    {
        $this->iri = $iri;
        return $this;
    }

}
