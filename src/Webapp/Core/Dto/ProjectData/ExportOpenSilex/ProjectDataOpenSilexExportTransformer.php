<?php

namespace Webapp\Core\Dto\ProjectData\ExportOpenSilex;

use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Webapp\Core\Entity\AbstractVariable;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Entity\ProjectData;
use Webapp\Core\Entity\Session;
use Webapp\Core\Entity\SimpleVariable;

class ProjectDataOpenSilexExportTransformer implements DataTransformerInterface
{
    private IriConverterInterface $iriConverter;
    private EntityManagerInterface $entityManager;

    public function __construct(IriConverterInterface $iriConverter, EntityManagerInterface $entityManager)
    {
        $this->iriConverter = $iriConverter;
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $object
     * @param string $to
     * @param array $context
     * @return ProjectDataExportOpenSilex
     */
    public function transform($object, string $to, array $context = []): ProjectDataExportOpenSilex
    {
        /** @var ProjectData $object */
        $experiments = $object->getProject()->getExperiments()->map(fn(Experiment $e) => (new ProjectDataExperimentExportOpenSilex())
            ->setOpenSilexInstance($e->getOpenSilexInstance())
            ->setName($e->getName()))->toArray();

        $variables = array_values(array_map(fn(AbstractVariable $e) => (new ProjectDataVariableExportOpenSilex())
            ->setOpenSilexUri($e->getOpenSilexUri())
            ->setOpenSilexInstance($e->getOpenSilexInstance())
            ->setUnit($e->getUnit())
            ->setName($e->getName()),
            $object->getVariables()));
        //->filter(fn(SimpleVariable $e) => $e->getOpenSilexUri())
        $sessions = $object->getSessions()->map(function (Session $session) use ($object, $variables) {
            $res = (new SessionExportOpenSilex())
                ->setUser($session->getUser()->getUsername())
                ->setDebut($session->getStartedAt())
                ->setFin($session->getEndedAt())
                ->setFields(
                    array_map(fn(FieldMeasure $fm) => (new FieldExportOpenSilex())
                        ->setTargetIri($this->iriConverter->getIriFromItem($fm->getTarget()))
                        ->setMeasures($fm->getMeasures()->map(fn(Measure $measure) => (new MeasureExportOpenSilex())
                            ->setValue($measure->getValue() ?? $measure->getState()->getCode())
                            ->setTimestamp($measure->getTimestamp())
                        )->toArray())
                        ->setVariableOpenSilexIri($fm->getVariable()->getOpenSilexUri()),
                        $this->entityManager->getRepository(FieldMeasure::class)->findBy([
                            'session' => $session->getId(),
                            'simpleVariable' => $object->getSimpleVariables()
                                ->filter(fn(SimpleVariable $e) => in_array($e->getName(), array_map(fn(ProjectDataVariableExportOpenSilex $v) => $v->getName(), $variables)))
                                ->map(fn(SimpleVariable $e) => $e->getId())->toArray(),
                        ]))
                );
            return $res;
        })->toArray();

        return (new ProjectDataExportOpenSilex())
            ->setName($object->getName())
            ->setOpenSilexInstance($object->getOpenSilexInstance())
            ->setSessions($sessions)
            ->setEnd($object->getEnd())
            ->setOperator((new OperatorExportOpenSilex())
                ->setIri($this->iriConverter->getIriFromItem($object->getUser()))
                ->setEmail($object->getUser()->getEmail())
                ->setSurname($object->getUser()->getSurname())
                ->setName($object->getUser()->getName())
            )
            ->setProject((new ProjectExportOpenSilex())
                ->setExperiments($experiments)
                ->setVariables($variables));
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ProjectDataExportOpenSilex) {
            return false;
        }
        return ProjectDataExportOpenSilex::class === $to &&
            $context['operation_type'] === OperationType::ITEM &&
            $context['item_operation_name'] === 'exportOpenSilex';
    }
}
