<?php


namespace Webapp\Core\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use Shared\Authentication\Entity\Site;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Webapp\Core\Entity\StateCode;
use Webapp\Core\Enumeration\SpreadingEnum;

class SiteCreateEventSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onSiteCreate', EventPriorities::PRE_WRITE],
        ];
    }

    public function onSiteCreate(ViewEvent $event): void
    {
        $site = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!$site instanceof Site || Request::METHOD_POST !== $method) {
            return;
        }
        $site->addStateCode(
            (new StateCode())
            ->setCode(-9)
            ->setMeaning('Code d\'état permanent : Individu Mort')
            ->setTitle('Mort')
            ->setSpreading(SpreadingEnum::INDIVIDUAL)
            ->setColor(null)
            ->setPermanent(true)
        );
        $site->addStateCode(
            (new StateCode())
            ->setCode(-6)
            ->setMeaning('Code d\'état permanent : Donnée manquante')
            ->setTitle('Donnée Manquante')
            ->setSpreading(null)
            ->setColor(null)
            ->setPermanent(true)
        );
    }
}
