<?php


namespace Webapp\Core\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Webapp\Core\Entity\AbstractVariable;

class VariableCreateEventSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onVariableCreate', EventPriorities::PRE_WRITE],
        ];
    }

    public function onVariableCreate(ViewEvent $event): void
    {
        $variable = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if (!($variable instanceof AbstractVariable) || Request::METHOD_POST !== $method || $variable->getProject() === null) {
            return;
        }
        $variable->setOrder(max(0, 0, ...array_map(function (AbstractVariable $item) {
                return $item->getOrder() ?? 0;
            }, $variable->getProject()->getVariables())) + 1);

    }
}
