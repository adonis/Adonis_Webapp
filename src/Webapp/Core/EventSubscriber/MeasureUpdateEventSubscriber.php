<?php


namespace Webapp\Core\EventSubscriber;


use ApiPlatform\Core\EventListener\EventPriorities;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\Annotation;
use Webapp\Core\Entity\Measure;
use Webapp\Core\Enumeration\AnnotationKindEnum;

class MeasureUpdateEventSubscriber implements EventSubscriberInterface
{
    private Security $security;
    private EntityManagerInterface $entityManager;

    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => ['onMeasureUpdate', EventPriorities::POST_DESERIALIZE],
        ];
    }

    public function onMeasureUpdate(RequestEvent $event): void
    {
        $measure = $event->getRequest()->attributes->get('data');
        $method = $event->getRequest()->getMethod();
        if (!$measure instanceof Measure || Request::METHOD_PATCH !== $method) {
            return;
        }
        if (!array_reduce($measure->getAnnotations()->toArray(), function ($acc, Annotation $annotation) {
            return $acc || ($annotation->getValue() && str_starts_with($annotation->getValue(), "Donnée brute"));
        }, false)) {
            /** @var Measure $oldMeasure */
            $oldMeasure = $event->getRequest()->attributes->get('previous_data');
            $measure->addAnnotation((new Annotation())
                ->setName("Modification de donnée " . $measure->getFormField()->getVariable()->getShortName())
                ->setSession($measure->getFormField()->getSession())
                ->setType(AnnotationKindEnum::TEXT)
                ->setTimestamp(new DateTime())
                ->setValue(sprintf("Donnée brute %s : %s Modifiée par %s %s",
                    $measure->getFormField()->getVariable()->getName(),
                    $oldMeasure->getValue() === '' ? $oldMeasure->getState()->getCode() : $oldMeasure->getValue(),
                    $this->security->getUser()->getSurname(),
                    $this->security->getUser()->getName(),
                )));
        }

    }
}
