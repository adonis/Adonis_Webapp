<?php


namespace Webapp\Core\EventSubscriber;


use ApiPlatform\Core\Api\IriConverterInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Events;
use Symfony\Contracts\Cache\CacheInterface;
use Webapp\Core\Entity\Block;
use Webapp\Core\Entity\Experiment;
use Webapp\Core\Entity\Factor;
use Webapp\Core\Entity\GraphicalTextZone;
use Webapp\Core\Entity\Individual;
use Webapp\Core\Entity\Modality;
use Webapp\Core\Entity\NorthIndicator;
use Webapp\Core\Entity\OezNature;
use Webapp\Core\Entity\OutExperimentationZone;
use Webapp\Core\Entity\Protocol;
use Webapp\Core\Entity\SubBlock;
use Webapp\Core\Entity\SurfacicUnitPlot;
use Webapp\Core\Entity\Treatment;
use Webapp\Core\Entity\UnitPlot;

class CacheInvalidationEventSubscriber implements EventSubscriber
{
    private IriConverterInterface $iriConverter;
    private CacheInterface $cache;

    public function __construct(IriConverterInterface $iriConverter, CacheInterface $platformFullViewCache)
    {
        $this->iriConverter = $iriConverter;
        $this->cache = $platformFullViewCache;
    }


    public function getSubscribedEvents()
    {
        return array(
            Events::onFlush,
        );
    }

    public function onFlush(OnFlushEventArgs $event): void
    {
        $em = $event->getEntityManager();
        $uow = $em->getUnitOfWork();
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            $this->invalidParentCache($entity);
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $this->invalidObjectCache($entity);
        }

        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            $this->invalidObjectCache($entity);
        }

    }

    private function invalidObjectCache($entity)
    {
        if($entity instanceof Experiment && $entity->getId() !== null){
            $this->cache->delete($entity->getId());
        }
        if (
            $entity instanceof Block ||
            $entity instanceof SubBlock ||
            $entity instanceof UnitPlot ||
            $entity instanceof SurfacicUnitPlot ||
            $entity instanceof Individual ||
            $entity instanceof Protocol ||
            $entity instanceof Factor ||
            $entity instanceof Modality ||
            $entity instanceof Treatment ||
            $entity instanceof OutExperimentationZone ||
            $entity instanceof OezNature
        ) {
            $this->invalidParentCache($entity);
        }

    }

    private function invalidParentCache(object $entity)
    {
        if ($entity instanceof Experiment) {
            $this->invalidObjectCache($entity->getPlatform());
        } elseif ($entity instanceof Block) {
            $this->invalidObjectCache($entity->getExperiment());
        } elseif ($entity instanceof SubBlock) {
            $this->invalidObjectCache($entity->getBlock());
        } elseif ($entity instanceof UnitPlot) {
            $this->invalidObjectCache($entity->getSubBlock());
            $this->invalidObjectCache($entity->getBlock());
        } elseif ($entity instanceof SurfacicUnitPlot) {
            $this->invalidObjectCache($entity->getSubBlock());
            $this->invalidObjectCache($entity->getBlock());
        } elseif ($entity instanceof Individual) {
            $this->invalidObjectCache($entity->getUnitPlot());
        } elseif ($entity instanceof Protocol) {
            $this->invalidObjectCache($entity->getExperiment());
        } elseif ($entity instanceof Factor) {
            $this->invalidObjectCache($entity->getProtocol());
        } elseif ($entity instanceof Modality) {
            $this->invalidObjectCache($entity->getTreatments());
            $this->invalidObjectCache($entity->getFactor());
        } elseif ($entity instanceof Treatment) {
            $this->invalidObjectCache($entity->getProtocol());
        } elseif ($entity instanceof OutExperimentationZone) {
            $this->invalidObjectCache($entity->getExperiment());
            $this->invalidObjectCache($entity->getBlock());
            $this->invalidObjectCache($entity->getSubBlock());
            $this->invalidObjectCache($entity->getUnitPlot());
        } elseif ($entity instanceof GraphicalTextZone) {
            $this->invalidObjectCache($entity->getPlatform());
        } elseif ($entity instanceof NorthIndicator) {
            $this->invalidObjectCache($entity->getPlatform());
        } elseif ($entity instanceof OezNature) {
            foreach ($entity->getOutExperimentationZones() as $outExperimentationZone){
                $this->invalidObjectCache($outExperimentationZone);
            }
        }
    }
}
