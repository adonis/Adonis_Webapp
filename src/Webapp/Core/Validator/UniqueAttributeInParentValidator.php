<?php

namespace Webapp\Core\Validator;

use Exception;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class UniqueAttributeInParentValidator extends ConstraintValidator
{

    private PropertyAccessorInterface $propertyAccessor;

    public function __construct(PropertyAccessorInterface $propertyAccessor)
    {
        $this->propertyAccessor = $propertyAccessor;
    }

    /**
     * @param mixed $value
     * @param UniqueAttributeInParent $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueAttributeInParent) {
            throw new UnexpectedTypeException($constraint, UniqueAttributeInParent::class);
        }
        // Ignore null and blank value
        if (null === $value || '' === $value) {
            return;
        }
        // Validator only works on strings and numerics values
        if (is_numeric($value)){
            $value = strval($value);
        }
        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }
        foreach ($constraint->parentsAttributes as $attribute){
            try {
                $collection = $this->propertyAccessor->getValue($this->context->getObject(), $attribute);
                foreach ($collection as $item){
                    if($item !== $this->context->getObject()){
                        if( strval($this->propertyAccessor->getValue($item, $this->context->getPropertyName())) === $value){
                            $this->context->buildViolation($constraint->message)
                                ->setParameter('{{ string }}', $this->context->getPropertyName())
                                ->addViolation();
                            return;
                        }
                    }
                }
            } catch (Exception $e){}
        }
    }
}
