<?php

namespace Webapp\Core\Validator\Project;

use Symfony\Component\Validator\Constraint;

/**
 * Class ProjectPostConstraint
 * @Annotation
 * @Target({"CLASS"})
 */
class ProjectPostConstraint extends Constraint
{
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
