<?php

namespace Webapp\Core\Validator\Experiment;

use Symfony\Component\Validator\Constraint;

/**
 * Class ExperimentPatchConstraint
 * @Annotation
 * @Target({"CLASS"})
 */
class ExperimentPatchConstraint extends Constraint
{
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
