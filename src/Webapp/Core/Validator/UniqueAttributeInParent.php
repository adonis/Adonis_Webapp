<?php


namespace Webapp\Core\Validator;


use Symfony\Component\Validator\Constraint;

/**
 * Class UniqueAttributeInParent
 * @package Webapp\Core\Validator
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class UniqueAttributeInParent extends Constraint
{
    public $message = "The {{ string }} must be unique in its parent";
    public array $parentsAttributes;
}
