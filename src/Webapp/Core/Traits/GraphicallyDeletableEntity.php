<?php

namespace Webapp\Core\Traits;

use Doctrine\ORM\Mapping as ORM;

trait GraphicallyDeletableEntity
{
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected ?\DateTime $graphicallyDeletedAt = null;

    /**
     * Set or clear the deleted at timestamp.
     *
     * @return $this
     */
    public function setGraphicallyDeletedAt(?\DateTime $deletedAt = null): self
    {
        $this->graphicallyDeletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get the deleted at timestamp value. Will return null if
     * the entity has not been soft deleted.
     */
    public function getGraphicallyDeletedAt(): ?\DateTime
    {
        return $this->graphicallyDeletedAt;
    }

    /**
     * Check if the entity has been soft deleted.
     */
    public function isGraphicallyDeleted(): bool
    {
        return null !== $this->graphicallyDeletedAt;
    }
}
