<?php

namespace Webapp\Core\Persister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Webapp\Core\Entity\FieldMeasure;
use Webapp\Core\Entity\ProjectData;

class ProjectDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof ProjectData && $context['item_operation_name'] === Request::METHOD_DELETE;
    }

    /**
     * @param ProjectData $data
     * @param array $context
     * @return object|void
     * @throws InternalErrorException
     */
    public function persist($data, array $context = [])
    {
        throw new InternalErrorException();
    }

    /**
     * @param ProjectData $data
     * @param array $context
     */
    public function remove($data, array $context = [])
    {
        $removeFieldMeasure = function (FieldMeasure $fieldMeasure) use (&$removeFieldMeasure){
            foreach ($fieldMeasure->getFieldGenerations() as $generation){
                foreach ($generation->getChildren() as $child){
                    $removeFieldMeasure($child);
                }
                $this->entityManager->remove($generation);
            }
            $this->entityManager->remove($fieldMeasure);
            $this->entityManager->flush();
        };

        foreach ($data->getSessions() as $session){
            foreach ($session->getFieldMeasures() as $fieldMeasure){
                $removeFieldMeasure($fieldMeasure);
            }
        }
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
