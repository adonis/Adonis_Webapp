<?php
/**
 * @author      Aurélien BERNARD - TRYDEA - 2021
 */

declare(strict_types=1);

namespace Webapp\Core\RightManagement;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Shared\Authentication\Entity\User;
use Shared\RightManagement\Entity\AbstractAdvancedRight;
use Symfony\Component\Security\Core\Security;
use Webapp\Core\Entity\RelUserMobileProject;
use Webapp\Core\Entity\UserGroup;

/**
 * Class RelUserMobileProjectSecurityExtension
 * @package Webapp\Core\RightManagement
 */
class RelUserMobileProjectSecurityExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private Security $security;
    private ObjectManager $entityManager;

    public function __construct(
        Security $security,
        ManagerRegistry $managerRegistry
    )
    {
        $this->security = $security;
        $this->entityManager = $managerRegistry->getManager();
    }

    public function applyToItem(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        array $identifiers,
        string $operationName = null,
        array $context = []
    )
    {
        if ($resourceClass === RelUserMobileProject::class) {
            /** @var $currentUser User */
            $currentUser = $this->security->getUser();
            switch ($operationName) {
                case 'get':
                    $this->handleGetSecurity($queryBuilder, $currentUser);
                    break;
                default:
                    break;
            }
        }
    }

    public function applyToCollection(
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $operationName = null
    )
    {
        if ($resourceClass === RelUserMobileProject::class) {
            /** @var $currentUser User */
            $currentUser = $this->security->getUser();
            switch ($operationName) {
                case 'get':
                    $this->handleGetSecurity($queryBuilder, $currentUser);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param User $currentUser
     */
    private function handleGetSecurity(
        QueryBuilder $queryBuilder,
        User $currentUser
    ): void
    {
        $mainAlias = $queryBuilder->getRootAliases()[0];

        /** @var QueryBuilder $userRightQueryBuilder */
        $userRightQueryBuilder = $this->entityManager->getRepository('Shared\RightManagement\Entity\AdvancedUserRight')->createQueryBuilder('u');
        $userRightQueryBuilder
            ->andWhere($userRightQueryBuilder->expr()->eq('wp.id', 'u.objectId'))
            ->andWhere($userRightQueryBuilder->expr()->eq('u.userId', ':current_user'))
            ->andWhere($userRightQueryBuilder->expr()->eq('u.right', ':right'))
            ->andWhere($userRightQueryBuilder->expr()->eq('u.classIdentifier', ':class_identifier'));

        /** @var QueryBuilder $groupRightQueryBuilder */
        $groupRightQueryBuilder = $this->entityManager->getRepository('Shared\RightManagement\Entity\AdvancedGroupRight')->createQueryBuilder('g');
        $groupRightQueryBuilder
            ->innerJoin(UserGroup::class, 'ug', Join::WITH, 'g.groupId = ug.id')
            ->andWhere($groupRightQueryBuilder->expr()->eq('wp.id', 'g.objectId'))
            ->andWhere($groupRightQueryBuilder->expr()->eq('g.right', ':right'))
            ->andWhere($groupRightQueryBuilder->expr()->eq('g.classIdentifier', ':class_identifier'))
            ->andwhere($groupRightQueryBuilder->expr()->isMemberOf(':current_user', 'ug.users'));

        // SQL filter applies, will reject all project the user can't see
        $queryBuilder->innerJoin("$mainAlias.webappProject", 'wp');
        // SQL filter applies, will reject all mobileStatus the user can't see
        $queryBuilder->innerJoin("$mainAlias.mobileStatus", 'ms');

        $queryBuilder
            ->andWhere($queryBuilder->expr()->orX(
                $queryBuilder->expr()->eq('ms.user', ':current_user'), // Owner of rel user to mobile project
                $queryBuilder->expr()->eq('wp.owner', ':current_user'), // Webapp project owner
                $queryBuilder->expr()->exists($userRightQueryBuilder->getDQL()), // Has edit right on project
                $queryBuilder->expr()->exists($groupRightQueryBuilder->getDQL()) // In a group that has edit right on project
            ));

        $queryBuilder->setParameter('current_user', $currentUser)
            ->setParameter('right', AbstractAdvancedRight::ADVANCED_RIGHT_EDIT)
            ->setParameter('class_identifier', 'webapp_project');
    }
}
