<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211220143110 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the algorithm link to the protocol';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.protocol ADD algorithm_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.protocol ADD CONSTRAINT FK_70129518BBEB6CF7 FOREIGN KEY (algorithm_id) REFERENCES webapp.algorithm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_70129518BBEB6CF7 ON webapp.protocol (algorithm_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.protocol DROP CONSTRAINT FK_70129518BBEB6CF7');
        $this->addSql('DROP INDEX IDX_70129518BBEB6CF7');
        $this->addSql('ALTER TABLE webapp.protocol DROP algorithm_id');
    }
}
