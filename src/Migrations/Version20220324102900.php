<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324102900 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Normalize database and add missing comment sections';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.algorithm_parameter RENAME COLUMN script_name TO "algorithm_order"');
        $this->addSql('ALTER TABLE webapp.block ADD comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment RENAME COLUMN individualup TO individual_up');
        $this->addSql('ALTER TABLE webapp.individual ADD height DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.individual ADD comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.modality DROP CONSTRAINT fk_88aba194ed38ec00');
        $this->addSql('DROP INDEX webapp.idx_88aba194ed38ec00');
        $this->addSql('ALTER TABLE webapp.modality RENAME COLUMN factor TO factor_id');
        $this->addSql('ALTER TABLE webapp.modality ADD CONSTRAINT FK_88ABA194BC88C1A3 FOREIGN KEY (factor_id) REFERENCES webapp.factor (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_88ABA194BC88C1A3 ON webapp.modality (factor_id)');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone RENAME COLUMN identifier TO comment');
        $this->addSql('ALTER TABLE webapp.platform RENAME COLUMN sitename TO site_name');
        $this->addSql('ALTER TABLE webapp.platform RENAME COLUMN placename TO place_name');
        $this->addSql('ALTER TABLE webapp.project_data ADD comment VARCHAR(255) DEFAULT NULL');

        $this->addSql('ALTER TABLE webapp.project_data ADD name VARCHAR(255) DEFAULT NULL');
        $this->addSql('UPDATE webapp.project_data SET name = p.name FROM webapp.project AS p WHERE p.id = project_id');
        $this->addSql('ALTER TABLE webapp.project_data ALTER name DROP DEFAULT ');
        $this->addSql('ALTER TABLE webapp.project_data ALTER name SET NOT NULL ');

        $this->addSql('ALTER TABLE webapp.session_data ADD comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.sub_block ADD comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD height DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD comment VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD comment VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP height');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP comment');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP comment');
        $this->addSql('ALTER TABLE webapp.project_data DROP comment');
        $this->addSql('ALTER TABLE webapp.project_data DROP name');
        $this->addSql('ALTER TABLE webapp.algorithm_parameter RENAME COLUMN "order" TO script_name');
        $this->addSql('ALTER TABLE webapp.factor RENAME COLUMN "order" TO factor_order');
        $this->addSql('ALTER TABLE webapp.platform ADD sitename VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE webapp.platform ADD placename VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE webapp.platform DROP site_name');
        $this->addSql('ALTER TABLE webapp.platform DROP place_name');
        $this->addSql('ALTER TABLE webapp.experiment RENAME COLUMN individual_up TO individualup');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone RENAME COLUMN comment TO identifier');
        $this->addSql('ALTER TABLE webapp.modality DROP CONSTRAINT FK_88ABA194BC88C1A3');
        $this->addSql('DROP INDEX IDX_88ABA194BC88C1A3');
        $this->addSql('ALTER TABLE webapp.modality RENAME COLUMN factor_id TO factor');
        $this->addSql('ALTER TABLE webapp.modality ADD CONSTRAINT fk_88aba194ed38ec00 FOREIGN KEY (factor) REFERENCES webapp.factor (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_88aba194ed38ec00 ON webapp.modality (factor)');
        $this->addSql('ALTER TABLE webapp.sub_block DROP comment');
        $this->addSql('ALTER TABLE webapp.block DROP comment');
        $this->addSql('ALTER TABLE webapp.session_data DROP comment');
        $this->addSql('ALTER TABLE webapp.individual DROP height');
        $this->addSql('ALTER TABLE webapp.individual DROP comment');
    }
}
