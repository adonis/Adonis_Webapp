<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210713121016 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the tests';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.test_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.test (id INT NOT NULL, variable_simple_id INT DEFAULT NULL, variable_semi_automatic_id INT DEFAULT NULL, variable_generator_id INT DEFAULT NULL, compared_variable_simple_id INT DEFAULT NULL, compared_variable_semi_automatic_id INT DEFAULT NULL, compared_variable_generator_id INT DEFAULT NULL, combined_variable1_simple_id INT DEFAULT NULL, combined_variable1_semi_automatic_id INT DEFAULT NULL, combined_variable1_generator_id INT DEFAULT NULL, combined_variable2_simple_id INT DEFAULT NULL, combined_variable2_semi_automatic_id INT DEFAULT NULL, combined_variable2_generator_id INT DEFAULT NULL, compared_variable1_simple_id INT DEFAULT NULL, compared_variable1_semi_automatic_id INT DEFAULT NULL, compared_variable1_generator_id INT DEFAULT NULL, compared_variable2_simple_id INT DEFAULT NULL, compared_variable2_semi_automatic_id INT DEFAULT NULL, compared_variable2_generator_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, auth_interval_min DOUBLE PRECISION DEFAULT NULL, prob_interval_min DOUBLE PRECISION DEFAULT NULL, prob_interval_max DOUBLE PRECISION DEFAULT NULL, auth_interval_max DOUBLE PRECISION DEFAULT NULL, min_growth DOUBLE PRECISION DEFAULT NULL, max_growth DOUBLE PRECISION DEFAULT NULL, combination_operation VARCHAR(255) DEFAULT NULL, min_limit VARCHAR(255) DEFAULT NULL, max_limit VARCHAR(255) DEFAULT NULL, compare_with_variable BOOLEAN DEFAULT NULL, comparison_operation VARCHAR(255) DEFAULT NULL, compared_value VARCHAR(255) DEFAULT NULL, assigned_value VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8504DD302D8C9D7C ON webapp.test (variable_simple_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30708976EF ON webapp.test (variable_semi_automatic_id)');
        $this->addSql('CREATE INDEX IDX_8504DD3019089810 ON webapp.test (variable_generator_id)');
        $this->addSql('CREATE INDEX IDX_8504DD302E95CE7E ON webapp.test (compared_variable_simple_id)');
        $this->addSql('CREATE INDEX IDX_8504DD3043E8AB3B ON webapp.test (compared_variable_semi_automatic_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30EF71EA2A ON webapp.test (compared_variable_generator_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30AB11E00D ON webapp.test (combined_variable1_simple_id)');
        $this->addSql('CREATE INDEX IDX_8504DD3029216A5 ON webapp.test (combined_variable1_semi_automatic_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30581472B3 ON webapp.test (combined_variable1_generator_id)');
        $this->addSql('CREATE INDEX IDX_8504DD3032F3860C ON webapp.test (combined_variable2_simple_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30A1C4900C ON webapp.test (combined_variable2_semi_automatic_id)');
        $this->addSql('CREATE INDEX IDX_8504DD3024755768 ON webapp.test (combined_variable2_generator_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30B0FAB979 ON webapp.test (compared_variable1_simple_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30F7766879 ON webapp.test (compared_variable1_semi_automatic_id)');
        $this->addSql('CREATE INDEX IDX_8504DD30574D6FC9 ON webapp.test (compared_variable1_generator_id)');
        $this->addSql('CREATE INDEX IDX_8504DD302918DF78 ON webapp.test (compared_variable2_simple_id)');
        $this->addSql('CREATE INDEX IDX_8504DD305420EED0 ON webapp.test (compared_variable2_semi_automatic_id)');
        $this->addSql('CREATE INDEX IDX_8504DD302B2C4A12 ON webapp.test (compared_variable2_generator_id)');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD302D8C9D7C FOREIGN KEY (variable_simple_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30708976EF FOREIGN KEY (variable_semi_automatic_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD3019089810 FOREIGN KEY (variable_generator_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD302E95CE7E FOREIGN KEY (compared_variable_simple_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD3043E8AB3B FOREIGN KEY (compared_variable_semi_automatic_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30EF71EA2A FOREIGN KEY (compared_variable_generator_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30AB11E00D FOREIGN KEY (combined_variable1_simple_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD3029216A5 FOREIGN KEY (combined_variable1_semi_automatic_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30581472B3 FOREIGN KEY (combined_variable1_generator_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD3032F3860C FOREIGN KEY (combined_variable2_simple_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30A1C4900C FOREIGN KEY (combined_variable2_semi_automatic_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD3024755768 FOREIGN KEY (combined_variable2_generator_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30B0FAB979 FOREIGN KEY (compared_variable1_simple_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30F7766879 FOREIGN KEY (compared_variable1_semi_automatic_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD30574D6FC9 FOREIGN KEY (compared_variable1_generator_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD302918DF78 FOREIGN KEY (compared_variable2_simple_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD305420EED0 FOREIGN KEY (compared_variable2_semi_automatic_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.test ADD CONSTRAINT FK_8504DD302B2C4A12 FOREIGN KEY (compared_variable2_generator_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.test_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.test');
    }
}
