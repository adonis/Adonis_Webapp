<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221024095701 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER SEQUENCE webapp.metadata_id_seq RENAME TO annotation_id_seq');
        $this->addSql('ALTER SEQUENCE webapp.required_metadata_id_seq RENAME TO required_annotation_id_seq');
        $this->addSql('ALTER SEQUENCE adonis.metadata_id_seq RENAME TO annotation_id_seq');
        $this->addSql('ALTER SEQUENCE adonis.required_metadata_id_seq RENAME TO required_annotation_id_seq');
        $this->addSql('ALTER TABLE webapp.metadata rename to annotation');
        $this->addSql('ALTER TABLE webapp.required_metadata rename to required_annotation');
        $this->addSql('ALTER TABLE adonis.metadata rename to annotation');
        $this->addSql('ALTER TABLE adonis.required_metadata rename to required_annotation');
        $this->addSql('ALTER TABLE adonis.project ALTER benchmark SET NOT NULL');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('ALTER TABLE adonis.project ALTER benchmark DROP NOT NULL');
        $this->addSql('ALTER TABLE webapp.annotation rename to metadata');
        $this->addSql('ALTER TABLE webapp.required_annotation rename to required_metadata');
        $this->addSql('ALTER TABLE adonis.annotation rename to metadata');
        $this->addSql('ALTER TABLE adonis.required_annotation rename to required_metadata');
        $this->addSql('ALTER SEQUENCE webapp.annotation_id_seq RENAME TO metadata_id_seq');
        $this->addSql('ALTER SEQUENCE webapp.required_annotation_id_seq RENAME TO required_metadata_id_seq');
        $this->addSql('ALTER SEQUENCE adonis.annotation_id_seq RENAME TO metadata_id_seq');
        $this->addSql('ALTER SEQUENCE adonis.required_annotation_id_seq RENAME TO required_metadata_id_seq');
    }
}
