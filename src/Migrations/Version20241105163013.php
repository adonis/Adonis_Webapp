<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20241105163013 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Set owner when missing for data entry projects.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE webapp.project ps SET owner_id = (SELECT pf.owner_id FROM webapp.platform pf WHERE ps.platform_id = pf.id) WHERE ps.owner_id IS NULL');
    }

    public function down(Schema $schema): void
    {
    }
}
