<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210526145558 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Disable the advanced right for the factor class';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.factor DROP CONSTRAINT fk_d1143ad67e3c61f9');
        $this->addSql('ALTER TABLE webapp.factor DROP owner_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.factor ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT fk_d1143ad67e3c61f9 FOREIGN KEY (owner_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d1143ad67e3c61f9 ON webapp.factor (owner_id)');
    }
}
