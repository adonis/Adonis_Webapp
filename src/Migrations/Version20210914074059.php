<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210914074059 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add a storage for the scales pics';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_scale_item ADD pic TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_scale_item ALTER text TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE webapp.variable_scale_item ALTER text DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_scale_item DROP pic');
        $this->addSql('ALTER TABLE webapp.variable_scale_item ALTER text TYPE TEXT');
        $this->addSql('ALTER TABLE webapp.variable_scale_item ALTER text DROP DEFAULT');
    }
}
