<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210623142309 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.value_list_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.value_list_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.value_list (id INT NOT NULL, site_id INT DEFAULT NULL, origin_site_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_71421B2FF6BD1646 ON webapp.value_list (site_id)');
        $this->addSql('CREATE INDEX IDX_71421B2F43CA032B ON webapp.value_list (origin_site_id)');
        $this->addSql('CREATE TABLE webapp.value_list_item (id INT NOT NULL, list_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C38D2C573DAE168B ON webapp.value_list_item (list_id)');
        $this->addSql('ALTER TABLE webapp.value_list ADD CONSTRAINT FK_71421B2FF6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.value_list ADD CONSTRAINT FK_71421B2F43CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.value_list_item ADD CONSTRAINT FK_C38D2C573DAE168B FOREIGN KEY (list_id) REFERENCES webapp.value_list (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.value_list_item DROP CONSTRAINT FK_C38D2C573DAE168B');
        $this->addSql('DROP SEQUENCE webapp.value_list_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.value_list_item_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.value_list');
        $this->addSql('DROP TABLE webapp.value_list_item');
    }
}
