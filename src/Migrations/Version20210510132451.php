<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210510132451 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the unaccent extension in order to search into users';
    }

    public function up(Schema $schema) : void
    {
        dump("Please enable the unaccent extension");
        dump("CREATE EXTENSION unaccent;");
    }

    public function down(Schema $schema) : void
    {
        dump("Please disable the unaccent extension");
        dump("DROP EXTENSION unaccent;");
    }
}
