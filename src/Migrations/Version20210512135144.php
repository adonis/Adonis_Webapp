<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512135144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add modalities and factor informations';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA webapp');
        $this->addSql('CREATE SEQUENCE webapp.factor_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.modality_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.factor (id INT NOT NULL, site_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D1143AD6F6BD1646 ON webapp.factor (site_id)');
        $this->addSql('CREATE INDEX IDX_D1143AD67E3C61F9 ON webapp.factor (owner_id)');
        $this->addSql('CREATE TABLE webapp.modality (id INT NOT NULL, factor INT NOT NULL, value VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, identifier VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_88ABA194ED38EC00 ON webapp.modality (factor)');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT FK_D1143AD6F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT FK_D1143AD67E3C61F9 FOREIGN KEY (owner_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.modality ADD CONSTRAINT FK_88ABA194ED38EC00 FOREIGN KEY (factor) REFERENCES webapp.factor (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.modality DROP CONSTRAINT FK_88ABA194ED38EC00');
        $this->addSql('DROP SEQUENCE webapp.factor_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.modality_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.factor');
        $this->addSql('DROP TABLE webapp.modality');
        $this->addSql('DROP SCHEMA webapp');
    }
}
