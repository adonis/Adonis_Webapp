<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210712084208 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Regroups webapp.rel_user_app_project and adonis.ado_user_project into webapp.rel_user_project_status';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.rel_user_app_project_id_seq');
        $this->addSql('ALTER TABLE adonis.ado_user_project SET SCHEMA webapp');
        $this->addSql('ALTER TABLE webapp.ado_user_project RENAME TO rel_user_project_status');
        $this->addSql('ALTER SEQUENCE adonis.ado_user_project_id_seq RENAME TO rel_user_project_status_id_seq');
        $this->addSql('ALTER SEQUENCE adonis.rel_user_project_status_id_seq SET SCHEMA webapp');
        $this->addSql('ALTER TABLE webapp.rel_user_project_status ADD COLUMN webapp_project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.rel_user_project_status RENAME COLUMN synchronizable TO syncable');
        $this->addSql('ALTER TABLE webapp.rel_user_project_status ADD CONSTRAINT FK_6C102AE5B5A42A41 FOREIGN KEY (webapp_project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE webapp.rel_user_app_project');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.rel_user_project_status DROP CONSTRAINT FK_6C102AE5B5A42A41');
        $this->addSql('ALTER TABLE webapp.rel_user_project_status DROP COLUMN webapp_project_id');
        $this->addSql('ALTER TABLE webapp.rel_user_project_status RENAME COLUMN syncable TO synchronizable');
        $this->addSql('ALTER TABLE webapp.rel_user_project_status RENAME TO ado_user_project');
        $this->addSql('ALTER TABLE webapp.ado_user_project SET SCHEMA adonis');
        $this->addSql('ALTER SEQUENCE webapp.rel_user_project_status_id_seq RENAME TO ado_user_project_id_seq');
        $this->addSql('ALTER SEQUENCE webapp.ado_user_project_id_seq SET SCHEMA adonis');
        $this->addSql('CREATE SEQUENCE webapp.rel_user_app_project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.rel_user_app_project (id INT NOT NULL, mobile_status_id INT DEFAULT NULL, webapp_project_id INT DEFAULT NULL, origin_site_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_c5c2e00a43ca032b ON webapp.rel_user_app_project (origin_site_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_c5c2e00ae7e94be2 ON webapp.rel_user_app_project (mobile_status_id)');
        $this->addSql('CREATE INDEX idx_c5c2e00ab5a42a41 ON webapp.rel_user_app_project (webapp_project_id)');
        $this->addSql('ALTER TABLE webapp.rel_user_app_project ADD CONSTRAINT fk_c5c2e00ae7e94be2 FOREIGN KEY (mobile_status_id) REFERENCES adonis.ado_user_project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_user_app_project ADD CONSTRAINT fk_c5c2e00ab5a42a41 FOREIGN KEY (webapp_project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_user_app_project ADD CONSTRAINT fk_c5c2e00a43ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
