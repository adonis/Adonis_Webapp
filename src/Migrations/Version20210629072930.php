<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210629072930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Communication protocol for device';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device ADD communication_protocol VARCHAR(255) NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE webapp.device ALTER communication_protocol DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.device ADD frame_length INT NOT NULL DEFAULT 1');
        $this->addSql('ALTER TABLE webapp.device ALTER frame_length DROP DEFAULT ');
        $this->addSql('ALTER TABLE webapp.device ADD frame_start VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD frame_end VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD frame_csv VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD baudrate INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD bit_format INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD flow_control VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD parity VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD stop_bit VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD remote_control BOOLEAN DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device DROP communication_protocol');
        $this->addSql('ALTER TABLE webapp.device DROP frame_length');
        $this->addSql('ALTER TABLE webapp.device DROP frame_start');
        $this->addSql('ALTER TABLE webapp.device DROP frame_end');
        $this->addSql('ALTER TABLE webapp.device DROP frame_csv');
        $this->addSql('ALTER TABLE webapp.device DROP baudrate');
        $this->addSql('ALTER TABLE webapp.device DROP bit_format');
        $this->addSql('ALTER TABLE webapp.device DROP flow_control');
        $this->addSql('ALTER TABLE webapp.device DROP parity');
        $this->addSql('ALTER TABLE webapp.device DROP stop_bit');
        $this->addSql('ALTER TABLE webapp.device DROP remote_control');
    }
}
