<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630064534 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.state_code_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.state_code (id INT NOT NULL, site_id INT DEFAULT NULL, origin_site_id INT DEFAULT NULL, code INT NOT NULL, title VARCHAR(255) NOT NULL, meaning VARCHAR(255) DEFAULT NULL, spreading VARCHAR(255) DEFAULT NULL, color VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7F9301B077153098 ON webapp.state_code (code)');
        $this->addSql('CREATE INDEX IDX_7F9301B0F6BD1646 ON webapp.state_code (site_id)');
        $this->addSql('CREATE INDEX IDX_7F9301B043CA032B ON webapp.state_code (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.state_code ADD CONSTRAINT FK_7F9301B0F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.state_code ADD CONSTRAINT FK_7F9301B043CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.state_code_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.state_code');
    }
}
