<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210628134506 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the device and semi automatic variables';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.device_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.variable_semi_automatic_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.device (id INT NOT NULL, site_id INT DEFAULT NULL, alias VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, manufacturer VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_35036058F6BD1646 ON webapp.device (site_id)');
        $this->addSql('CREATE TABLE webapp.variable_semi_automatic (id INT NOT NULL, device_id INT DEFAULT NULL, frame_start INT NOT NULL, frame_end INT NOT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, repetitions INT NOT NULL, unit VARCHAR(255) DEFAULT NULL, path_level VARCHAR(255) NOT NULL, comment TEXT DEFAULT NULL, print_order INT DEFAULT NULL, format VARCHAR(255) DEFAULT NULL, format_length INT DEFAULT NULL, default_true_value BOOLEAN DEFAULT NULL, identifier VARCHAR(255) DEFAULT NULL, mandatory BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E5B3AE9494A4C7D4 ON webapp.variable_semi_automatic (device_id)');
        $this->addSql('ALTER TABLE webapp.device ADD CONSTRAINT FK_35036058F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD CONSTRAINT FK_E5B3AE9494A4C7D4 FOREIGN KEY (device_id) REFERENCES webapp.device (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP CONSTRAINT FK_E5B3AE9494A4C7D4');
        $this->addSql('DROP SEQUENCE webapp.device_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.variable_semi_automatic_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.device');
        $this->addSql('DROP TABLE webapp.variable_semi_automatic');
    }
}
