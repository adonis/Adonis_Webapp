<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220715134206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Remove Spacialisation Projects';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX adonis.idx_ecb8464961220ea6');
        $this->addSql('ALTER TABLE adonis.project DROP discr');
        $this->addSql('UPDATE adonis.project set improvised = false WHERE improvised is null');
        $this->addSql('ALTER TABLE adonis.project ALTER improvised SET DEFAULT false');
        $this->addSql('ALTER TABLE adonis.project ALTER improvised SET NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_ECB8464961220EA6 ON adonis.project (creator_id)');
        $this->addSql('DROP INDEX shared.idx_2c695f87fbf32840');
        $this->addSql('ALTER TABLE shared.rel_user_project_status DROP discr');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_2C695F87FBF32840 ON shared.rel_user_project_status (response_id)');
        $this->addSql('ALTER TABLE webapp.file_request DROP type');
        $this->addSql('ALTER TABLE webapp.file_response DROP type');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.file_request ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE webapp.file_response ADD type VARCHAR(255) NOT NULL');
        $this->addSql('DROP INDEX shared.UNIQ_2C695F87FBF32840');
        $this->addSql('ALTER TABLE shared.rel_user_project_status ADD discr VARCHAR(255) NOT NULL');
        $this->addSql('CREATE INDEX idx_2c695f87fbf32840 ON shared.rel_user_project_status (response_id)');
        $this->addSql('DROP INDEX adonis.UNIQ_ECB8464961220EA6');
        $this->addSql('ALTER TABLE adonis.project ADD discr VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE adonis.project ALTER improvised DROP NOT NULL');
        $this->addSql('CREATE INDEX idx_ecb8464961220ea6 ON adonis.project (creator_id)');
    }
}
