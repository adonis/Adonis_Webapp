<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210716070822 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE shared.rel_individual_change_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE shared.rel_individual_change (id INT NOT NULL, request_individual_id INT DEFAULT NULL, response_individual_id INT DEFAULT NULL, status_data_entry_id INT NOT NULL, approved BOOLEAN DEFAULT \'false\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_72F7779826346CB2 ON shared.rel_individual_change (request_individual_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_72F77798EF5EC12A ON shared.rel_individual_change (response_individual_id)');
        $this->addSql('CREATE INDEX IDX_72F777985D20D10B ON shared.rel_individual_change (status_data_entry_id)');
        $this->addSql('ALTER TABLE shared.rel_individual_change ADD CONSTRAINT FK_72F7779826346CB2 FOREIGN KEY (request_individual_id) REFERENCES adonis.individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shared.rel_individual_change ADD CONSTRAINT FK_72F77798EF5EC12A FOREIGN KEY (response_individual_id) REFERENCES adonis.individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shared.rel_individual_change ADD CONSTRAINT FK_72F777985D20D10B FOREIGN KEY (status_data_entry_id) REFERENCES shared.rel_user_project_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE shared.rel_individual_change_id_seq CASCADE');
        $this->addSql('DROP TABLE shared.rel_individual_change');
    }
}
