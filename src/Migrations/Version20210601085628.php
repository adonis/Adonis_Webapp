<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210601085628 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the user group managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.user_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.user_group (id INT NOT NULL, site_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_13670773F6BD1646 ON webapp.user_group (site_id)');
        $this->addSql('CREATE TABLE webapp.rel_user_user_group (user_group_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(user_group_id, user_id))');
        $this->addSql('CREATE INDEX IDX_CE760F991ED93D47 ON webapp.rel_user_user_group (user_group_id)');
        $this->addSql('CREATE INDEX IDX_CE760F99A76ED395 ON webapp.rel_user_user_group (user_id)');
        $this->addSql('ALTER TABLE webapp.user_group ADD CONSTRAINT FK_13670773F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_user_user_group ADD CONSTRAINT FK_CE760F991ED93D47 FOREIGN KEY (user_group_id) REFERENCES webapp.user_group (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_user_user_group ADD CONSTRAINT FK_CE760F99A76ED395 FOREIGN KEY (user_id) REFERENCES adonis.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.rel_user_user_group DROP CONSTRAINT FK_CE760F991ED93D47');
        $this->addSql('DROP SEQUENCE webapp.user_group_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.user_group');
        $this->addSql('DROP TABLE webapp.rel_user_user_group');
    }
}
