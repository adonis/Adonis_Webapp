<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210603073043 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'change the order of PK cause of abstraction';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shared.advanced_group_right DROP CONSTRAINT advanced_group_right_pkey');
        $this->addSql('ALTER TABLE shared.advanced_group_right ADD PRIMARY KEY (class_identifier, object_id, group_id)');
        $this->addSql('ALTER TABLE shared.advanced_user_right DROP CONSTRAINT advanced_user_right_pkey');
        $this->addSql('ALTER TABLE shared.advanced_user_right ADD PRIMARY KEY (class_identifier, object_id, user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shared.advanced_group_right DROP CONSTRAINT advanced_group_right_pkey');
        $this->addSql('ALTER TABLE shared.advanced_group_right ADD PRIMARY KEY (group_id, class_identifier, object_id)');
        $this->addSql('ALTER TABLE shared.advanced_user_right DROP CONSTRAINT advanced_user_right_pkey');
        $this->addSql('ALTER TABLE shared.advanced_user_right ADD PRIMARY KEY (user_id, class_identifier, object_id)');
    }
}
