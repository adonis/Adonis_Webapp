<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210727071930 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the metadata managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.metadata_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.metadata (id INT NOT NULL, target_measure_id INT DEFAULT NULL, target_individual_id INT DEFAULT NULL, target_surfacic_unit_plot_id INT DEFAULT NULL, target_unit_plot_id INT DEFAULT NULL, target_sub_block_id INT DEFAULT NULL, target_block_id INT DEFAULT NULL, target_experiment_id INT DEFAULT NULL, session_id INT NOT NULL, name VARCHAR(255) DEFAULT NULL, type INT NOT NULL, value VARCHAR(255) DEFAULT NULL, image TEXT DEFAULT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, categories TEXT NOT NULL, keywords TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F7C61D406F4FA591 ON webapp.metadata (target_measure_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D40B2AB2FBC ON webapp.metadata (target_individual_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D4015B6DEE0 ON webapp.metadata (target_surfacic_unit_plot_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D40DC16D692 ON webapp.metadata (target_unit_plot_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D40B310BF04 ON webapp.metadata (target_sub_block_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D4033465265 ON webapp.metadata (target_block_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D4013787779 ON webapp.metadata (target_experiment_id)');
        $this->addSql('CREATE INDEX IDX_F7C61D40613FECDF ON webapp.metadata (session_id)');
        $this->addSql('COMMENT ON COLUMN webapp.metadata.categories IS \'(DC2Type:array)\'');
        $this->addSql('COMMENT ON COLUMN webapp.metadata.keywords IS \'(DC2Type:array)\'');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D406F4FA591 FOREIGN KEY (target_measure_id) REFERENCES webapp.measure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D40B2AB2FBC FOREIGN KEY (target_individual_id) REFERENCES webapp.individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D4015B6DEE0 FOREIGN KEY (target_surfacic_unit_plot_id) REFERENCES webapp.surfacic_unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D40DC16D692 FOREIGN KEY (target_unit_plot_id) REFERENCES webapp.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D40B310BF04 FOREIGN KEY (target_sub_block_id) REFERENCES webapp.sub_block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D4033465265 FOREIGN KEY (target_block_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D4013787779 FOREIGN KEY (target_experiment_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.metadata ADD CONSTRAINT FK_F7C61D40613FECDF FOREIGN KEY (session_id) REFERENCES webapp.session_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.metadata_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.metadata');
    }
}
