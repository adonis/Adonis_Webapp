<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220421143505 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Delete direct link between site and project';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.project DROP CONSTRAINT fk_403f8bd9f6bd1646');
        $this->addSql('DROP INDEX webapp.idx_403f8bd9f6bd1646');
        $this->addSql('ALTER TABLE webapp.project DROP site_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.project ADD site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.project ADD CONSTRAINT fk_403f8bd9f6bd1646 FOREIGN KEY (site_id) REFERENCES shared.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_403f8bd9f6bd1646 ON webapp.project (site_id)');
    }
}
