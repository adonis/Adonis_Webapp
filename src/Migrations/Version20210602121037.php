<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210602121037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Correct the name of the right column (reserved keyword)';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shared.advanced_group_right RENAME COLUMN "right" TO ad_right');
        $this->addSql('ALTER TABLE shared.advanced_user_right RENAME COLUMN "right" TO ad_right');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shared.advanced_group_right RENAME COLUMN ad_right TO "right"');
        $this->addSql('ALTER TABLE shared.advanced_user_right RENAME COLUMN ad_right TO "right"');
    }
}
