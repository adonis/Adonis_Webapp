<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210818080200 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add path level algorithm';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.path_base_level_algorithm_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.path_base_level_algorithm (id INT NOT NULL, path_base_id INT NOT NULL, path_level VARCHAR(255) NOT NULL, move VARCHAR(255) NOT NULL, start_point VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_127223DDAED1E968 ON webapp.path_base_level_algorithm (path_base_id)');
        $this->addSql('ALTER TABLE webapp.path_base_level_algorithm ADD CONSTRAINT FK_127223DDAED1E968 FOREIGN KEY (path_base_id) REFERENCES webapp.path_base (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_base ADD ask_when_entering BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql('ALTER TABLE webapp.path_base ALTER ask_when_entering DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.path_base_level_algorithm_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.path_base_level_algorithm');
        $this->addSql('ALTER TABLE webapp.path_base DROP ask_when_entering');
    }
}
