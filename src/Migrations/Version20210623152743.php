<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210623152743 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add parent information on Variable';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_generator DROP CONSTRAINT fk_e6d775f543ca032b');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD has_parent BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql('ALTER TABLE webapp.variable_generator ALTER has_parent DROP DEFAULT ');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP origin_site_id');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP CONSTRAINT fk_920e6b8343ca032b');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD has_parent BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql('ALTER TABLE webapp.variable_simple ALTER has_parent DROP DEFAULT ');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP origin_site_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_generator ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP has_parent');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT fk_e6d775f543ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e6d775f543ca032b ON webapp.variable_generator (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP has_parent');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT fk_920e6b8343ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_920e6b8343ca032b ON webapp.variable_simple (origin_site_id)');
    }
}
