<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210630081800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD has_parent BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql('ALTER TABLE webapp.device ALTER has_parent DROP DEFAULT ');
        $this->addSql('ALTER TABLE webapp.device ADD CONSTRAINT FK_35036058166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_35036058166D1F9C ON webapp.device (project_id)');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT FK_E6D775F5166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E6D775F5166D1F9C ON webapp.variable_generator (project_id)');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT FK_920E6B83166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_920E6B83166D1F9C ON webapp.variable_simple (project_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_generator DROP CONSTRAINT FK_E6D775F5166D1F9C');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP project_id');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP CONSTRAINT FK_920E6B83166D1F9C');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP project_id');
        $this->addSql('ALTER TABLE webapp.device DROP CONSTRAINT FK_35036058166D1F9C');
        $this->addSql('ALTER TABLE webapp.device DROP project_id');
        $this->addSql('ALTER TABLE webapp.device DROP has_parent');
    }
}
