<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220527091951 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.graphical_text_zone_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.graphical_text_zone (id INT NOT NULL, platform_id INT NOT NULL, text TEXT NOT NULL, color INT DEFAULT NULL, x INT NOT NULL, y INT NOT NULL, width INT NOT NULL, height INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A45136CAFFE6496F ON webapp.graphical_text_zone (platform_id)');
        $this->addSql('ALTER TABLE webapp.graphical_text_zone ADD CONSTRAINT FK_A45136CAFFE6496F FOREIGN KEY (platform_id) REFERENCES webapp.platform (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER INDEX webapp.uniq_52d6040bffe6496f RENAME TO UNIQ_16CCE659FFE6496F');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.graphical_text_zone_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.graphical_text_zone');
        $this->addSql('ALTER INDEX webapp.uniq_16cce659ffe6496f RENAME TO uniq_52d6040bffe6496f');
    }
}
