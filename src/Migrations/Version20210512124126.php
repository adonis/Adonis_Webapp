<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210512124126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the right managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA shared');
        $this->addSql('CREATE TABLE shared.advanced_group_right (group_id INT NOT NULL, class_identifier VARCHAR(255) NOT NULL, object_id INT NOT NULL, "right" VARCHAR(255) NOT NULL, PRIMARY KEY(group_id, class_identifier, object_id))');
        $this->addSql('CREATE TABLE shared.advanced_user_right (user_id INT NOT NULL, class_identifier VARCHAR(255) NOT NULL, object_id INT NOT NULL, "right" VARCHAR(255) NOT NULL, PRIMARY KEY(user_id, class_identifier, object_id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE shared.advanced_group_right');
        $this->addSql('DROP TABLE shared.advanced_user_right');
        $this->addSql('DROP SCHEMA shared');
    }
}
