<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210902075547 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow a value list to be connected with a variable';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.value_list ADD variable_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.value_list ADD CONSTRAINT FK_71421B2FF3037E8E FOREIGN KEY (variable_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_71421B2FF3037E8E ON webapp.value_list (variable_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.value_list DROP CONSTRAINT FK_71421B2FF3037E8E');
        $this->addSql('ALTER TABLE webapp.value_list DROP variable_id');
    }
}
