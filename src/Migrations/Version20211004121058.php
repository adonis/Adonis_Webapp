<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211004121058 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Manage required metadata';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.required_metadata_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.required_metadata (id INT NOT NULL, project_id INT DEFAULT NULL, type INT NOT NULL, level VARCHAR(255) NOT NULL, ask_when_entering BOOLEAN NOT NULL, comment VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D713FDF0166D1F9C ON webapp.required_metadata (project_id)');
        $this->addSql('ALTER TABLE webapp.required_metadata ADD CONSTRAINT FK_D713FDF0166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.required_metadata_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.required_metadata');
    }
}
