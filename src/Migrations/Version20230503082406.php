<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230503082406 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Update repetition number, it has to start from 1';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('update webapp.measure set repetition = repetition + 1 where form_field_id in (select id from webapp.field_measure where session_id in (select distinct fm.session_id from webapp.measure inner join webapp.field_measure fm on fm.id = measure.form_field_id where repetition = 0))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
