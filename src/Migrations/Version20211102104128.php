<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211102104128 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Update the width and height (stored with same range as Adonis Bureau';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE webapp.platform SET x_mesh = x_mesh / 2');
        $this->addSql('UPDATE webapp.platform SET y_mesh = y_mesh / 2');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
