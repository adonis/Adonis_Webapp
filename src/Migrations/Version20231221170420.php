<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231221170420 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.project_data ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.project_data ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.project_data ADD CONSTRAINT FK_912542C2A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_912542C2A4EEF451 ON webapp.project_data (open_silex_instance_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.project_data DROP CONSTRAINT FK_912542C2A4EEF451');
        $this->addSql('DROP INDEX IDX_912542C2A4EEF451');
        $this->addSql('ALTER TABLE webapp.project_data DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.project_data DROP open_silex_uri');
    }
}
