<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230707144521 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.nature_zhe DROP CONSTRAINT fk_ba0806b35c96afc5');
        $this->addSql('ALTER TABLE adonis.experiment_wrapper DROP CONSTRAINT fk_f403a1cb79160211');
        $this->addSql('ALTER TABLE adonis.variable DROP CONSTRAINT fk_bae91c9e27166d67');
        $this->addSql('DROP SEQUENCE adonis.experiment_library_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE adonis.experiment_wrapper_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE adonis.variable_library_id_seq CASCADE');
        $this->addSql('DROP TABLE adonis.experiment_wrapper');
        $this->addSql('DROP TABLE adonis.experiment_library');
        $this->addSql('DROP TABLE adonis.variable_library');
        $this->addSql('ALTER TABLE adonis.nature_zhe DROP device_wrapper_id');
        $this->addSql('ALTER TABLE shared.rel_user_project_status ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE adonis.variable DROP variable_library_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE adonis.experiment_library_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE adonis.experiment_wrapper_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE adonis.variable_library_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE adonis.experiment_wrapper (id INT NOT NULL, device_id INT NOT NULL, device_library_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_8398ca1479160211 ON adonis.experiment_wrapper (device_library_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_8398ca1494a4c7d4 ON adonis.experiment_wrapper (device_id)');
        $this->addSql('CREATE TABLE adonis.experiment_library (id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_ed4cd67aa76ed395 ON adonis.experiment_library (user_id)');
        $this->addSql('CREATE TABLE adonis.variable_library (id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX uniq_33354b6da76ed395 ON adonis.variable_library (user_id)');
        $this->addSql('ALTER TABLE adonis.experiment_wrapper ADD CONSTRAINT fk_f403a1cb79160211 FOREIGN KEY (device_library_id) REFERENCES adonis.experiment_library (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adonis.experiment_wrapper ADD CONSTRAINT fk_f403a1cb94a4c7d4 FOREIGN KEY (device_id) REFERENCES adonis.experiment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adonis.experiment_library ADD CONSTRAINT fk_9ad7bda5a76ed395 FOREIGN KEY (user_id) REFERENCES shared.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adonis.variable_library ADD CONSTRAINT fk_24807347a76ed395 FOREIGN KEY (user_id) REFERENCES shared.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adonis.nature_zhe ADD device_wrapper_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adonis.nature_zhe ADD CONSTRAINT fk_ba0806b35c96afc5 FOREIGN KEY (device_wrapper_id) REFERENCES adonis.experiment_wrapper (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d0c4c47b5c96afc5 ON adonis.nature_zhe (device_wrapper_id)');
        $this->addSql('ALTER TABLE shared.rel_user_project_status DROP deleted_at');
        $this->addSql('ALTER TABLE adonis.variable ADD variable_library_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE adonis.variable ADD CONSTRAINT fk_bae91c9e27166d67 FOREIGN KEY (variable_library_id) REFERENCES adonis.variable_library (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_843cba5027166d67 ON adonis.variable (variable_library_id)');
    }
}
