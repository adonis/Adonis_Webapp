<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109085116 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Change the color type to integer';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.state_code ADD color2 INT');
        $this->addSql('UPDATE webapp.state_code SET color2 = (\'x\' || lpad(substring(color, 2, 6), 8, \'0\'))::bit(32)::int');
        $this->addSql('ALTER TABLE webapp.state_code DROP color');
        $this->addSql('ALTER TABLE webapp.state_code ADD color INT');
        $this->addSql('UPDATE webapp.state_code SET color = color2');
        $this->addSql('ALTER TABLE webapp.state_code DROP color2');
        $this->addSql('ALTER TABLE webapp.state_code ALTER color DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.state_code ALTER color TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE webapp.state_code ALTER color DROP DEFAULT');
    }
}
