<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210726151430 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Remove on delete cascade constraints and only works with doctrine';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_generation DROP CONSTRAINT FK_2AA67A01F50D82F4');
        $this->addSql('ALTER TABLE webapp.field_generation ADD CONSTRAINT FK_2AA67A01F50D82F4 FOREIGN KEY (form_field_id) REFERENCES webapp.field_measure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4613FECDF');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D497794CFA');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4CE8F2B87');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D436E3D90');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D411C670A');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4703CF43D');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D425618B73');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D43FDAFB5C');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D444662655');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4C6F00F25');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4613FECDF FOREIGN KEY (session_id) REFERENCES webapp.session_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D497794CFA FOREIGN KEY (field_parent_id) REFERENCES webapp.field_generation (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4CE8F2B87 FOREIGN KEY (generator_variable_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D436E3D90 FOREIGN KEY (simple_variable_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D411C670A FOREIGN KEY (individual_target_id) REFERENCES webapp.individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4703CF43D FOREIGN KEY (unit_plot_target_id) REFERENCES webapp.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D425618B73 FOREIGN KEY (block_target_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D43FDAFB5C FOREIGN KEY (sub_block_target_id) REFERENCES webapp.sub_block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D444662655 FOREIGN KEY (experiment_target_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4C6F00F25 FOREIGN KEY (surfacic_unit_plot_target_id) REFERENCES webapp.surfacic_unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.file_request DROP CONSTRAINT fk_a0163420166d1f9c');
        $this->addSql('ALTER TABLE webapp.file_request ADD CONSTRAINT FK_65B64F1D166D1F9C FOREIGN KEY (project_id) REFERENCES adonis.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.file_response DROP CONSTRAINT fk_653e245d166d1f9c');
        $this->addSql('ALTER TABLE webapp.file_response ADD CONSTRAINT FK_3D93C837166D1F9C FOREIGN KEY (project_id) REFERENCES adonis.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.measure DROP CONSTRAINT FK_EF8B4212F50D82F4');
        $this->addSql('ALTER TABLE webapp.measure ADD CONSTRAINT FK_EF8B4212F50D82F4 FOREIGN KEY (form_field_id) REFERENCES webapp.field_measure (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.project_data DROP CONSTRAINT FK_912542C2166D1F9C');
        $this->addSql('ALTER TABLE webapp.project_data ADD CONSTRAINT FK_912542C2166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.session_data DROP CONSTRAINT FK_6EC58AF0A1EBAD59');
        $this->addSql('ALTER TABLE webapp.session_data ADD CONSTRAINT FK_6EC58AF0A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_generation DROP CONSTRAINT fk_2aa67a01f50d82f4');
        $this->addSql('ALTER TABLE webapp.field_generation ADD CONSTRAINT fk_2aa67a01f50d82f4 FOREIGN KEY (form_field_id) REFERENCES webapp.field_measure (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d4613fecdf');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d497794cfa');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d4ce8f2b87');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d436e3d90');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d411c670a');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d4703cf43d');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d4c6f00f25');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d425618b73');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d43fdafb5c');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT fk_dc9b70d444662655');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d4613fecdf FOREIGN KEY (session_id) REFERENCES webapp.session_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d497794cfa FOREIGN KEY (field_parent_id) REFERENCES webapp.field_generation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d4ce8f2b87 FOREIGN KEY (generator_variable_id) REFERENCES webapp.variable_generator (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d436e3d90 FOREIGN KEY (simple_variable_id) REFERENCES webapp.variable_simple (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d411c670a FOREIGN KEY (individual_target_id) REFERENCES webapp.individual (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d4703cf43d FOREIGN KEY (unit_plot_target_id) REFERENCES webapp.unit_plot (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d4c6f00f25 FOREIGN KEY (surfacic_unit_plot_target_id) REFERENCES webapp.surfacic_unit_plot (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d425618b73 FOREIGN KEY (block_target_id) REFERENCES webapp.block (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d43fdafb5c FOREIGN KEY (sub_block_target_id) REFERENCES webapp.sub_block (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT fk_dc9b70d444662655 FOREIGN KEY (experiment_target_id) REFERENCES webapp.experiment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.measure DROP CONSTRAINT fk_ef8b4212f50d82f4');
        $this->addSql('ALTER TABLE webapp.measure ADD CONSTRAINT fk_ef8b4212f50d82f4 FOREIGN KEY (form_field_id) REFERENCES webapp.field_measure (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.project_data DROP CONSTRAINT fk_912542c2166d1f9c');
        $this->addSql('ALTER TABLE webapp.project_data ADD CONSTRAINT fk_912542c2166d1f9c FOREIGN KEY (project_id) REFERENCES webapp.project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.file_request DROP CONSTRAINT FK_65B64F1D166D1F9C');
        $this->addSql('ALTER TABLE webapp.file_request ADD CONSTRAINT fk_a0163420166d1f9c FOREIGN KEY (project_id) REFERENCES adonis.project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.file_response DROP CONSTRAINT FK_3D93C837166D1F9C');
        $this->addSql('ALTER TABLE webapp.file_response ADD CONSTRAINT fk_653e245d166d1f9c FOREIGN KEY (project_id) REFERENCES adonis.project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.session_data DROP CONSTRAINT fk_6ec58af0a1ebad59');
        $this->addSql('ALTER TABLE webapp.session_data ADD CONSTRAINT fk_6ec58af0a1ebad59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
