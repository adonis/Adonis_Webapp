<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211123132310 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_user_workflow DROP CONSTRAINT fk_ee7c9621166d1f9c');
        $this->addSql('DROP INDEX webapp.idx_ee7c9621166d1f9c');
        $this->addSql('ALTER TABLE webapp.path_user_workflow RENAME COLUMN project_id TO path_base_id');
        $this->addSql('UPDATE webapp.path_user_workflow SET path_base_id = (select id from webapp.path_base where project_id = path_base_id)');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ADD CONSTRAINT FK_EE7C9621AED1E968 FOREIGN KEY (path_base_id) REFERENCES webapp.path_base (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_EE7C9621AED1E968 ON webapp.path_user_workflow (path_base_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_user_workflow DROP CONSTRAINT FK_EE7C9621AED1E968');
        $this->addSql('DROP INDEX webapp.IDX_EE7C9621AED1E968');
        $this->addSql('ALTER TABLE webapp.path_user_workflow RENAME COLUMN path_base_id TO project_id');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ADD CONSTRAINT fk_ee7c9621166d1f9c FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_ee7c9621166d1f9c ON webapp.path_user_workflow (project_id)');
    }
}
