<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210722093354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add unit parcel to changes';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shared.rel_individual_change ADD request_unit_parcel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shared.rel_individual_change ADD response_unit_parcel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE shared.rel_individual_change ADD CONSTRAINT FK_72F7779837C16E3 FOREIGN KEY (request_unit_parcel_id) REFERENCES adonis.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shared.rel_individual_change ADD CONSTRAINT FK_72F77798FD616738 FOREIGN KEY (response_unit_parcel_id) REFERENCES adonis.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_72F7779837C16E3 ON shared.rel_individual_change (request_unit_parcel_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_72F77798FD616738 ON shared.rel_individual_change (response_unit_parcel_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE shared.rel_individual_change DROP CONSTRAINT FK_72F7779837C16E3');
        $this->addSql('ALTER TABLE shared.rel_individual_change DROP CONSTRAINT FK_72F77798FD616738');
        $this->addSql('DROP INDEX UNIQ_72F7779837C16E3');
        $this->addSql('DROP INDEX UNIQ_72F77798FD616738');
        $this->addSql('ALTER TABLE shared.rel_individual_change DROP request_unit_parcel_id');
        $this->addSql('ALTER TABLE shared.rel_individual_change DROP response_unit_parcel_id');
    }
}
