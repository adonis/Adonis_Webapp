<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210420145333 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add role between user and site';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.rel_user_site DROP CONSTRAINT FK_9C804012A76ED395');
        $this->addSql('ALTER TABLE adonis.rel_user_site DROP CONSTRAINT FK_9C804012F6BD1646');
        $this->addSql('ALTER TABLE adonis.rel_user_site ADD role VARCHAR(255) NOT NULL DEFAULT \'admin\'');
        $this->addSql('ALTER TABLE adonis.rel_user_site ALTER role DROP DEFAULT');
        $this->addSql('ALTER TABLE adonis.rel_user_site ADD CONSTRAINT FK_9C804012A76ED395 FOREIGN KEY (user_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adonis.rel_user_site ADD CONSTRAINT FK_9C804012F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.rel_user_site DROP CONSTRAINT fk_9c804012a76ed395');
        $this->addSql('ALTER TABLE adonis.rel_user_site DROP CONSTRAINT fk_9c804012f6bd1646');
        $this->addSql('ALTER TABLE adonis.rel_user_site DROP role');
        $this->addSql('ALTER TABLE adonis.rel_user_site ADD CONSTRAINT fk_9c804012a76ed395 FOREIGN KEY (user_id) REFERENCES adonis.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE adonis.rel_user_site ADD CONSTRAINT fk_9c804012f6bd1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
