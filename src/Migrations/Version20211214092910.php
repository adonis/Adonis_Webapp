<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211214092910 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'remove useless unique constraint';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX shared.uniq_72f77798fd616738');
        $this->addSql('DROP INDEX shared.uniq_72f77798ef5ec12a');
        $this->addSql('CREATE INDEX IDX_72F77798EF5EC12A ON shared.rel_individual_change (response_individual_id)');
        $this->addSql('CREATE INDEX IDX_72F77798FD616738 ON shared.rel_individual_change (response_unit_parcel_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX shared.IDX_72F77798EF5EC12A');
        $this->addSql('DROP INDEX shared.IDX_72F77798FD616738');
        $this->addSql('CREATE UNIQUE INDEX uniq_72f77798fd616738 ON shared.rel_individual_change (response_unit_parcel_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_72f77798ef5ec12a ON shared.rel_individual_change (response_individual_id)');
    }
}
