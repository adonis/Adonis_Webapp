<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211112132951 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add a copy of the variable on the data entries';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device ADD project_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD CONSTRAINT FK_35036058A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_35036058A1EBAD59 ON webapp.device (project_data_id)');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD project_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT FK_E6D775F5A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E6D775F5A1EBAD59 ON webapp.variable_generator (project_data_id)');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD project_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT FK_920E6B83A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_920E6B83A1EBAD59 ON webapp.variable_simple (project_data_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device DROP CONSTRAINT FK_35036058A1EBAD59');
        $this->addSql('ALTER TABLE webapp.device DROP project_data_id');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP CONSTRAINT FK_E6D775F5A1EBAD59');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP project_data_id');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP CONSTRAINT FK_920E6B83A1EBAD59');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP project_data_id');
    }
}
