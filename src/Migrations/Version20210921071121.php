<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210921071121 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the graphical config';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.graphical_configuration_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.graphical_configuration (id INT NOT NULL, site_id INT NOT NULL, individual_color INT DEFAULT NULL, unit_plot_color INT DEFAULT NULL, block_color INT DEFAULT NULL, sub_block_color INT DEFAULT NULL, experiment_color INT DEFAULT NULL, platform_color INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3F899257F6BD1646 ON webapp.graphical_configuration (site_id)');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD CONSTRAINT FK_3F899257F6BD1646 FOREIGN KEY (site_id) REFERENCES shared.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.graphical_configuration_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.graphical_configuration');
    }
}
