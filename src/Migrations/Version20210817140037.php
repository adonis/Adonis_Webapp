<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210817140037 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow to store nodes and leafs in same table';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_filter_node ALTER operator DROP NOT NULL');
        $this->addSql('ALTER TABLE webapp.path_filter_node ALTER value DROP NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_filter_node ALTER operator SET NOT NULL');
        $this->addSql('ALTER TABLE webapp.path_filter_node ALTER value SET NOT NULL');
    }
}
