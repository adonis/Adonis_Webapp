<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220808145250 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Manage mobile Unit plot apparition/disparition date';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.unit_plot ADD apparition_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT now()');
        $this->addSql('ALTER TABLE adonis.unit_plot ALTER apparition_date DROP DEFAULT');
        $this->addSql('ALTER TABLE adonis.unit_plot ADD demise_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.unit_plot DROP apparition_date');
        $this->addSql('ALTER TABLE adonis.unit_plot DROP demise_date');
    }
}
