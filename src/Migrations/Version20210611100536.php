<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210611100536 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the experiments';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.experiment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.experiment (id INT NOT NULL, site_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, origin_site_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, individualUP BOOLEAN NOT NULL, comment VARCHAR(255) DEFAULT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8F0AE05CF6BD1646 ON webapp.experiment (site_id)');
        $this->addSql('CREATE INDEX IDX_8F0AE05C7E3C61F9 ON webapp.experiment (owner_id)');
        $this->addSql('CREATE INDEX IDX_8F0AE05C43CA032B ON webapp.experiment (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.experiment ADD CONSTRAINT FK_8F0AE05CF6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.experiment ADD CONSTRAINT FK_8F0AE05C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.experiment ADD CONSTRAINT FK_8F0AE05C43CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.protocol ADD experiment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.protocol ADD CONSTRAINT FK_70129518FF444C8 FOREIGN KEY (experiment_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_70129518FF444C8 ON webapp.protocol (experiment_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.protocol DROP CONSTRAINT FK_70129518FF444C8');
        $this->addSql('DROP SEQUENCE webapp.experiment_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.experiment');
        $this->addSql('ALTER TABLE webapp.protocol DROP experiment_id');
    }
}
