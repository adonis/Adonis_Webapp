<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210915120255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Timestamps for variables';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_generator ADD created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE webapp.variable_generator ALTER created DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD last_modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE webapp.variable_generator ALTER last_modified DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ALTER created DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD last_modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ALTER last_modified DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE webapp.variable_simple ALTER created DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD last_modified TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addSql('ALTER TABLE webapp.variable_simple ALTER last_modified DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_generator DROP created');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP last_modified');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP created');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP last_modified');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP created');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP last_modified');
    }
}
