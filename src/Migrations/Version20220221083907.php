<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220221083907 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow to parse dataEntry return file';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.parsing_job ADD status_data_entry_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.parsing_job ADD CONSTRAINT FK_8011B1E95D20D10B FOREIGN KEY (status_data_entry_id) REFERENCES shared.rel_user_project_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8011B1E95D20D10B ON webapp.parsing_job (status_data_entry_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.parsing_job DROP CONSTRAINT FK_8011B1E95D20D10B');
        $this->addSql('DROP INDEX IDX_8011B1E95D20D10B');
        $this->addSql('ALTER TABLE webapp.parsing_job DROP status_data_entry_id');
    }
}
