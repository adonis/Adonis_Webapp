<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210721084659 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow the field measur to target a surfacic unit plot';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure ADD surfacic_unit_plot_target_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4C6F00F25 FOREIGN KEY (surfacic_unit_plot_target_id) REFERENCES webapp.surfacic_unit_plot (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DC9B70D4C6F00F25 ON webapp.field_measure (surfacic_unit_plot_target_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4C6F00F25');
        $this->addSql('ALTER TABLE webapp.field_measure DROP surfacic_unit_plot_target_id');
    }
}
