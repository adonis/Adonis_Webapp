<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210621100030 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the field to disable or not the filters';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.experiment ADD has_parent BOOLEAN NOT NULL DEFAULT false');
        $this->addSql('ALTER TABLE webapp.experiment ALTER has_parent DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.platform ADD has_parent BOOLEAN NOT NULL DEFAULT false');
        $this->addSql('ALTER TABLE webapp.platform ALTER has_parent DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.protocol ADD has_parent BOOLEAN NOT NULL DEFAULT false');
        $this->addSql('ALTER TABLE webapp.protocol ALTER has_parent DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.protocol DROP has_parent');
        $this->addSql('ALTER TABLE webapp.experiment DROP has_parent');
        $this->addSql('ALTER TABLE webapp.platform DROP has_parent');
    }
}
