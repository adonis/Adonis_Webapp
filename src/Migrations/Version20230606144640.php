<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230606144640 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.clone_site ADD admin_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.clone_site ADD CONSTRAINT FK_875788FF642B8210 FOREIGN KEY (admin_id) REFERENCES shared.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_875788FF642B8210 ON webapp.clone_site (admin_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.clone_site DROP CONSTRAINT FK_875788FF642B8210');
        $this->addSql('DROP INDEX IDX_875788FF642B8210');
        $this->addSql('ALTER TABLE webapp.clone_site DROP admin_id');
    }
}
