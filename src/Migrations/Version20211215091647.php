<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211215091647 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow an other kind of soft delete from graphical view';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.block ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.individual ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.modality ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.note ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.platform ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.sub_block ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.treatment ADD deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD graphically_deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.treatment DROP deleted_at');
        $this->addSql('ALTER TABLE webapp.note DROP deleted_at');
        $this->addSql('ALTER TABLE webapp.platform DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.factor DROP deleted_at');
        $this->addSql('ALTER TABLE webapp.experiment DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.sub_block DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.modality DROP deleted_at');
        $this->addSql('ALTER TABLE webapp.block DROP graphically_deleted_at');
        $this->addSql('ALTER TABLE webapp.individual DROP graphically_deleted_at');
    }
}
