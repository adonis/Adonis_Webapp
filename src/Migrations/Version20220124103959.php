<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220124103959 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.variable_connection_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.variable_connection (id INT NOT NULL, project_simple_variable_id INT DEFAULT NULL, project_generator_variable_id INT DEFAULT NULL, project_semi_automatic_variable_id INT DEFAULT NULL, data_entry_simple_variable_id INT DEFAULT NULL, data_entry_generator_variable_id INT DEFAULT NULL, data_entry_semi_automatic_variable_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B4097DC7B53CC3F1 ON webapp.variable_connection (project_simple_variable_id)');
        $this->addSql('CREATE INDEX IDX_B4097DC788C076CD ON webapp.variable_connection (project_generator_variable_id)');
        $this->addSql('CREATE INDEX IDX_B4097DC77BD250D3 ON webapp.variable_connection (project_semi_automatic_variable_id)');
        $this->addSql('CREATE INDEX IDX_B4097DC7C537CEF7 ON webapp.variable_connection (data_entry_simple_variable_id)');
        $this->addSql('CREATE INDEX IDX_B4097DC7AE31ADCA ON webapp.variable_connection (data_entry_generator_variable_id)');
        $this->addSql('CREATE INDEX IDX_B4097DC74F7F2898 ON webapp.variable_connection (data_entry_semi_automatic_variable_id)');
        $this->addSql('ALTER TABLE webapp.variable_connection ADD CONSTRAINT FK_B4097DC7B53CC3F1 FOREIGN KEY (project_simple_variable_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_connection ADD CONSTRAINT FK_B4097DC788C076CD FOREIGN KEY (project_generator_variable_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_connection ADD CONSTRAINT FK_B4097DC77BD250D3 FOREIGN KEY (project_semi_automatic_variable_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_connection ADD CONSTRAINT FK_B4097DC7C537CEF7 FOREIGN KEY (data_entry_simple_variable_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_connection ADD CONSTRAINT FK_B4097DC7AE31ADCA FOREIGN KEY (data_entry_generator_variable_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_connection ADD CONSTRAINT FK_B4097DC74F7F2898 FOREIGN KEY (data_entry_semi_automatic_variable_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.variable_connection_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.variable_connection');
    }
}
