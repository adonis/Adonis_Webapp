<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210615071355 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the platform managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.platform_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.platform (id INT NOT NULL, site_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, siteName VARCHAR(255) NOT NULL, placeName VARCHAR(255) NOT NULL, comment VARCHAR(255) DEFAULT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8180F99FF6BD1646 ON webapp.platform (site_id)');
        $this->addSql('CREATE INDEX IDX_8180F99F7E3C61F9 ON webapp.platform (owner_id)');
        $this->addSql('ALTER TABLE webapp.platform ADD CONSTRAINT FK_8180F99FF6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.platform ADD CONSTRAINT FK_8180F99F7E3C61F9 FOREIGN KEY (owner_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.experiment ADD platform_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD CONSTRAINT FK_8F0AE05CFFE6496F FOREIGN KEY (platform_id) REFERENCES webapp.platform (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8F0AE05CFFE6496F ON webapp.experiment (platform_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.experiment DROP CONSTRAINT FK_8F0AE05CFFE6496F');
        $this->addSql('DROP SEQUENCE webapp.platform_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.platform');
        $this->addSql('ALTER TABLE webapp.experiment DROP platform_id');
    }
}
