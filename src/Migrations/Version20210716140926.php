<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Adds measures on webapp side
 */
final class Version20210716140926 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adds measures on webapp side';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.field_generation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.field_measure_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.measure_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.project_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.session_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.field_generation (id INT NOT NULL, form_field_id INT NOT NULL, index INT NOT NULL, prefix VARCHAR(255) NOT NULL, numeral_increment BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2AA67A01F50D82F4 ON webapp.field_generation (form_field_id)');
        $this->addSql('CREATE TABLE webapp.field_measure (id INT NOT NULL, session_id INT DEFAULT NULL, field_parent_id INT DEFAULT NULL, generator_variable_id INT DEFAULT NULL, simple_variable_id INT DEFAULT NULL, target_class VARCHAR(255) NOT NULL, target_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DC9B70D4613FECDF ON webapp.field_measure (session_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D497794CFA ON webapp.field_measure (field_parent_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D4CE8F2B87 ON webapp.field_measure (generator_variable_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D436E3D90 ON webapp.field_measure (simple_variable_id)');
        $this->addSql('CREATE TABLE webapp.measure (id INT NOT NULL, form_field_id INT NOT NULL, state_id INT DEFAULT NULL, value VARCHAR(255) DEFAULT NULL, repetition INT NOT NULL, timestamp TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EF8B4212F50D82F4 ON webapp.measure (form_field_id)');
        $this->addSql('CREATE INDEX IDX_EF8B42125D83CC1 ON webapp.measure (state_id)');
        $this->addSql('CREATE TABLE webapp.project_data (id INT NOT NULL, project_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_912542C2166D1F9C ON webapp.project_data (project_id)');
        $this->addSql('CREATE TABLE webapp.session_data (id INT NOT NULL, project_data_id INT NOT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ended_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6EC58AF0A1EBAD59 ON webapp.session_data (project_data_id)');
        $this->addSql('ALTER TABLE webapp.field_generation ADD CONSTRAINT FK_2AA67A01F50D82F4 FOREIGN KEY (form_field_id) REFERENCES webapp.field_measure (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4613FECDF FOREIGN KEY (session_id) REFERENCES webapp.session_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D497794CFA FOREIGN KEY (field_parent_id) REFERENCES webapp.field_generation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4CE8F2B87 FOREIGN KEY (generator_variable_id) REFERENCES webapp.variable_generator (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D436E3D90 FOREIGN KEY (simple_variable_id) REFERENCES webapp.variable_simple (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.measure ADD CONSTRAINT FK_EF8B4212F50D82F4 FOREIGN KEY (form_field_id) REFERENCES webapp.field_measure (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.measure ADD CONSTRAINT FK_EF8B42125D83CC1 FOREIGN KEY (state_id) REFERENCES webapp.state_code (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.project_data ADD CONSTRAINT FK_912542C2166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.session_data ADD CONSTRAINT FK_6EC58AF0A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D497794CFA');
        $this->addSql('ALTER TABLE webapp.field_generation DROP CONSTRAINT FK_2AA67A01F50D82F4');
        $this->addSql('ALTER TABLE webapp.measure DROP CONSTRAINT FK_EF8B4212F50D82F4');
        $this->addSql('ALTER TABLE webapp.session_data DROP CONSTRAINT FK_6EC58AF0A1EBAD59');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4613FECDF');
        $this->addSql('DROP SEQUENCE webapp.field_generation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.field_measure_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.measure_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.project_data_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.session_data_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.field_generation');
        $this->addSql('DROP TABLE webapp.field_measure');
        $this->addSql('DROP TABLE webapp.measure');
        $this->addSql('DROP TABLE webapp.project_data');
        $this->addSql('DROP TABLE webapp.session_data');
    }
}
