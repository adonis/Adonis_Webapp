<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210922121522 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Store the graphic labels';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD experiment_labels TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD bloc_labels TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD sub_bloc_labels TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD unit_plot_labels TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD surfacic_unit_plot_labels TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD individual_labels TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD active_breadcrumb BOOLEAN NOT NULL DEFAULT TRUE');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ALTER active_breadcrumb DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN webapp.graphical_configuration.experiment_labels IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN webapp.graphical_configuration.bloc_labels IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN webapp.graphical_configuration.sub_bloc_labels IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN webapp.graphical_configuration.unit_plot_labels IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN webapp.graphical_configuration.surfacic_unit_plot_labels IS \'(DC2Type:simple_array)\'');
        $this->addSql('COMMENT ON COLUMN webapp.graphical_configuration.individual_labels IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP experiment_labels');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP bloc_labels');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP sub_bloc_labels');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP unit_plot_labels');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP surfacic_unit_plot_labels');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP individual_labels');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP active_breadcrumb');
    }
}
