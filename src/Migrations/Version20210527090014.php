<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210527090014 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Update the origin on factor and protocols';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.factor ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT FK_D1143AD643CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D1143AD643CA032B ON webapp.factor (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.protocol ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.protocol ADD CONSTRAINT FK_7012951843CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7012951843CA032B ON webapp.protocol (origin_site_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.protocol DROP CONSTRAINT FK_7012951843CA032B');
        $this->addSql('ALTER TABLE webapp.protocol DROP origin_site_id');
        $this->addSql('ALTER TABLE webapp.factor DROP CONSTRAINT FK_D1143AD643CA032B');
        $this->addSql('ALTER TABLE webapp.factor DROP origin_site_id');
    }
}
