<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210702082938 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add the link between project and state code';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.factor DROP CONSTRAINT fk_d1143ad643ca032b');
        $this->addSql('ALTER TABLE webapp.factor DROP origin_site_id');
        $this->addSql('ALTER TABLE webapp.state_code ADD project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.state_code ADD has_parent BOOLEAN NOT NULL DEFAULT false');
        $this->addSql('ALTER TABLE webapp.state_code ALTER has_parent DROP DEFAULT ');
        $this->addSql('ALTER TABLE webapp.state_code ADD CONSTRAINT FK_7F9301B0166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_7F9301B0166D1F9C ON webapp.state_code (project_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.state_code DROP CONSTRAINT FK_7F9301B0166D1F9C');
        $this->addSql('ALTER TABLE webapp.state_code DROP project_id');
        $this->addSql('ALTER TABLE webapp.state_code DROP has_parent');
        $this->addSql('ALTER TABLE webapp.factor ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT fk_d1143ad643ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d1143ad643ca032b ON webapp.factor (origin_site_id)');
    }
}
