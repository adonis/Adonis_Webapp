<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210728081217 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create the note entity';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.note_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.note (id INT NOT NULL, creator_id INT NOT NULL, individual_target_id INT DEFAULT NULL, unit_plot_target_id INT DEFAULT NULL, surfacic_unit_plot_target_id INT DEFAULT NULL, block_target_id INT DEFAULT NULL, sub_block_target_id INT DEFAULT NULL, experiment_target_id INT DEFAULT NULL, text TEXT DEFAULT NULL, deleted BOOLEAN NOT NULL, creation_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_92C6592861220EA6 ON webapp.note (creator_id)');
        $this->addSql('CREATE INDEX IDX_92C6592811C670A ON webapp.note (individual_target_id)');
        $this->addSql('CREATE INDEX IDX_92C65928703CF43D ON webapp.note (unit_plot_target_id)');
        $this->addSql('CREATE INDEX IDX_92C65928C6F00F25 ON webapp.note (surfacic_unit_plot_target_id)');
        $this->addSql('CREATE INDEX IDX_92C6592825618B73 ON webapp.note (block_target_id)');
        $this->addSql('CREATE INDEX IDX_92C659283FDAFB5C ON webapp.note (sub_block_target_id)');
        $this->addSql('CREATE INDEX IDX_92C6592844662655 ON webapp.note (experiment_target_id)');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C6592861220EA6 FOREIGN KEY (creator_id) REFERENCES shared.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C6592811C670A FOREIGN KEY (individual_target_id) REFERENCES webapp.individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C65928703CF43D FOREIGN KEY (unit_plot_target_id) REFERENCES webapp.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C65928C6F00F25 FOREIGN KEY (surfacic_unit_plot_target_id) REFERENCES webapp.surfacic_unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C6592825618B73 FOREIGN KEY (block_target_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C659283FDAFB5C FOREIGN KEY (sub_block_target_id) REFERENCES webapp.sub_block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.note ADD CONSTRAINT FK_92C6592844662655 FOREIGN KEY (experiment_target_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.note_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.note');
    }
}
