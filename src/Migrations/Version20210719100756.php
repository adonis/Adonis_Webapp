<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210719100756 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure ADD individual_target_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD unit_plot_target_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD block_target_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD sub_block_target_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD experiment_target_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure DROP target_level');
        $this->addSql('ALTER TABLE webapp.field_measure DROP target_name');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D411C670A FOREIGN KEY (individual_target_id) REFERENCES webapp.individual (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D4703CF43D FOREIGN KEY (unit_plot_target_id) REFERENCES webapp.unit_plot (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D425618B73 FOREIGN KEY (block_target_id) REFERENCES webapp.block (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D43FDAFB5C FOREIGN KEY (sub_block_target_id) REFERENCES webapp.sub_block (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D444662655 FOREIGN KEY (experiment_target_id) REFERENCES webapp.experiment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DC9B70D411C670A ON webapp.field_measure (individual_target_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D4703CF43D ON webapp.field_measure (unit_plot_target_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D425618B73 ON webapp.field_measure (block_target_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D43FDAFB5C ON webapp.field_measure (sub_block_target_id)');
        $this->addSql('CREATE INDEX IDX_DC9B70D444662655 ON webapp.field_measure (experiment_target_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D411C670A');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D4703CF43D');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D425618B73');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D43FDAFB5C');
        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D444662655');
        $this->addSql('DROP INDEX IDX_DC9B70D411C670A');
        $this->addSql('DROP INDEX IDX_DC9B70D4703CF43D');
        $this->addSql('DROP INDEX IDX_DC9B70D425618B73');
        $this->addSql('DROP INDEX IDX_DC9B70D43FDAFB5C');
        $this->addSql('DROP INDEX IDX_DC9B70D444662655');
        $this->addSql('ALTER TABLE webapp.field_measure ADD target_level VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD target_name INT NOT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure DROP individual_target_id');
        $this->addSql('ALTER TABLE webapp.field_measure DROP unit_plot_target_id');
        $this->addSql('ALTER TABLE webapp.field_measure DROP block_target_id');
        $this->addSql('ALTER TABLE webapp.field_measure DROP sub_block_target_id');
        $this->addSql('ALTER TABLE webapp.field_measure DROP experiment_target_id');
    }
}
