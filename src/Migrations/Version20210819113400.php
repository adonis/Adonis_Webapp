<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210819113400 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'remove hasParent management';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device DROP has_parent');
        $this->addSql('ALTER TABLE webapp.experiment DROP has_parent');
        $this->addSql('ALTER TABLE webapp.platform DROP has_parent');
        $this->addSql('ALTER TABLE webapp.project DROP has_parent');
        $this->addSql('ALTER TABLE webapp.protocol DROP has_parent');
        $this->addSql('ALTER TABLE webapp.state_code DROP has_parent');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP has_parent');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP has_parent');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.protocol ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.platform ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.project ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD has_parent BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE webapp.state_code ADD has_parent BOOLEAN NOT NULL');
    }
}
