<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211122153500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Store workflows as array';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_user_workflow ALTER workflow DROP NOT NULL');
        $this->addSql('UPDATE webapp.path_user_workflow SET workflow = null');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ALTER workflow TYPE TEXT');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ALTER workflow DROP DEFAULT');
        $this->addSql('COMMENT ON COLUMN webapp.path_user_workflow.workflow IS \'(DC2Type:simple_array)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_user_workflow ALTER workflow TYPE TEXT');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ALTER workflow DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ALTER workflow SET NOT NULL');
        $this->addSql('COMMENT ON COLUMN webapp.path_user_workflow.workflow IS NULL');
    }
}
