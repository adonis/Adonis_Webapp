<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231201095804 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.block ADD geometry TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD geometry TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.individual ADD geometry TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD geometry TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.sub_block ADD geometry TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD geometry TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD geometry TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.sub_block DROP geometry');
        $this->addSql('ALTER TABLE webapp.block DROP geometry');
        $this->addSql('ALTER TABLE webapp.experiment DROP geometry');
        $this->addSql('ALTER TABLE webapp.individual DROP geometry');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone DROP geometry');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP geometry');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP geometry');
    }
}
