<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210629152025 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create the project base';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.project (id INT NOT NULL, platform_id INT DEFAULT NULL, site_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, comment VARCHAR(255) DEFAULT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, has_parent BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_403F8BD9FFE6496F ON webapp.project (platform_id)');
        $this->addSql('CREATE INDEX IDX_403F8BD9F6BD1646 ON webapp.project (site_id)');
        $this->addSql('CREATE INDEX IDX_403F8BD97E3C61F9 ON webapp.project (owner_id)');
        $this->addSql('CREATE TABLE webapp.rel_project_experiment (project_id INT NOT NULL, experiment_id INT NOT NULL, PRIMARY KEY(project_id, experiment_id))');
        $this->addSql('CREATE INDEX IDX_51E33DD0166D1F9C ON webapp.rel_project_experiment (project_id)');
        $this->addSql('CREATE INDEX IDX_51E33DD0FF444C8 ON webapp.rel_project_experiment (experiment_id)');
        $this->addSql('ALTER TABLE webapp.project ADD CONSTRAINT FK_403F8BD9FFE6496F FOREIGN KEY (platform_id) REFERENCES webapp.platform (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.project ADD CONSTRAINT FK_403F8BD9F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.project ADD CONSTRAINT FK_403F8BD97E3C61F9 FOREIGN KEY (owner_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_project_experiment ADD CONSTRAINT FK_51E33DD0166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_project_experiment ADD CONSTRAINT FK_51E33DD0FF444C8 FOREIGN KEY (experiment_id) REFERENCES webapp.experiment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.rel_project_experiment DROP CONSTRAINT FK_51E33DD0166D1F9C');
        $this->addSql('DROP SEQUENCE webapp.project_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.project');
        $this->addSql('DROP TABLE webapp.rel_project_experiment');
    }
}
