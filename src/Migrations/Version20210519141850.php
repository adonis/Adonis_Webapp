<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210519141850 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add protocol managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.protocol_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.treatment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.protocol (id INT NOT NULL, site_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, aim VARCHAR(255) NOT NULL, comment VARCHAR(255) DEFAULT NULL, created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_70129518F6BD1646 ON webapp.protocol (site_id)');
        $this->addSql('CREATE INDEX IDX_701295187E3C61F9 ON webapp.protocol (owner_id)');
        $this->addSql('CREATE TABLE webapp.treatment (id INT NOT NULL, protocol_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, repetitions INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F4BF7BF5CCD59258 ON webapp.treatment (protocol_id)');
        $this->addSql('CREATE TABLE webapp.rel_treatment_modality (treatment_id INT NOT NULL, modality_id INT NOT NULL, PRIMARY KEY(treatment_id, modality_id))');
        $this->addSql('CREATE INDEX IDX_F8F9DEC9471C0366 ON webapp.rel_treatment_modality (treatment_id)');
        $this->addSql('CREATE INDEX IDX_F8F9DEC92D6D889B ON webapp.rel_treatment_modality (modality_id)');
        $this->addSql('ALTER TABLE webapp.protocol ADD CONSTRAINT FK_70129518F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.protocol ADD CONSTRAINT FK_701295187E3C61F9 FOREIGN KEY (owner_id) REFERENCES adonis.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.treatment ADD CONSTRAINT FK_F4BF7BF5CCD59258 FOREIGN KEY (protocol_id) REFERENCES webapp.protocol (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_treatment_modality ADD CONSTRAINT FK_F8F9DEC9471C0366 FOREIGN KEY (treatment_id) REFERENCES webapp.treatment (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_treatment_modality ADD CONSTRAINT FK_F8F9DEC92D6D889B FOREIGN KEY (modality_id) REFERENCES webapp.modality (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.factor ADD protocol_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT FK_D1143AD6CCD59258 FOREIGN KEY (protocol_id) REFERENCES webapp.protocol (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D1143AD6CCD59258 ON webapp.factor (protocol_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.factor DROP CONSTRAINT FK_D1143AD6CCD59258');
        $this->addSql('ALTER TABLE webapp.treatment DROP CONSTRAINT FK_F4BF7BF5CCD59258');
        $this->addSql('ALTER TABLE webapp.rel_treatment_modality DROP CONSTRAINT FK_F8F9DEC9471C0366');
        $this->addSql('DROP SEQUENCE webapp.protocol_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.treatment_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.protocol');
        $this->addSql('DROP TABLE webapp.treatment');
        $this->addSql('DROP TABLE webapp.rel_treatment_modality');
        $this->addSql('ALTER TABLE webapp.factor DROP protocol_id');
    }
}
