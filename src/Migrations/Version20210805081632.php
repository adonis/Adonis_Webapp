<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210805081632 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.path_base_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.path_filter_node_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.path_user_workflow_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.path_base (id INT NOT NULL, project_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C16630F2166D1F9C ON webapp.path_base (project_id)');
        $this->addSql('CREATE TABLE webapp.path_filter_node (id INT NOT NULL, path_base_id INT DEFAULT NULL, parent_filter_id INT DEFAULT NULL, type INT NOT NULL, text VARCHAR(255) NOT NULL, operator VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_530ED705AED1E968 ON webapp.path_filter_node (path_base_id)');
        $this->addSql('CREATE INDEX IDX_530ED70522DE9DB0 ON webapp.path_filter_node (parent_filter_id)');
        $this->addSql('CREATE TABLE webapp.path_user_workflow (id INT NOT NULL, user_id INT DEFAULT NULL, project_id INT DEFAULT NULL, workflow VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EE7C9621A76ED395 ON webapp.path_user_workflow (user_id)');
        $this->addSql('CREATE INDEX IDX_EE7C9621166D1F9C ON webapp.path_user_workflow (project_id)');
        $this->addSql('ALTER TABLE webapp.path_base ADD CONSTRAINT FK_C16630F2166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD CONSTRAINT FK_530ED705AED1E968 FOREIGN KEY (path_base_id) REFERENCES webapp.path_base (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD CONSTRAINT FK_530ED70522DE9DB0 FOREIGN KEY (parent_filter_id) REFERENCES webapp.path_filter_node (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ADD CONSTRAINT FK_EE7C9621A76ED395 FOREIGN KEY (user_id) REFERENCES shared.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_user_workflow ADD CONSTRAINT FK_EE7C9621166D1F9C FOREIGN KEY (project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_filter_node DROP CONSTRAINT FK_530ED705AED1E968');
        $this->addSql('ALTER TABLE webapp.path_filter_node DROP CONSTRAINT FK_530ED70522DE9DB0');
        $this->addSql('DROP SEQUENCE webapp.path_base_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.path_filter_node_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.path_user_workflow_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.path_base');
        $this->addSql('DROP TABLE webapp.path_filter_node');
        $this->addSql('DROP TABLE webapp.path_user_workflow');
    }
}
