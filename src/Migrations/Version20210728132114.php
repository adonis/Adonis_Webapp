<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210728132114 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add semi automatic value managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure ADD semi_automatic_variable_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.field_measure ADD CONSTRAINT FK_DC9B70D48D13E53A FOREIGN KEY (semi_automatic_variable_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_DC9B70D48D13E53A ON webapp.field_measure (semi_automatic_variable_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.field_measure DROP CONSTRAINT FK_DC9B70D48D13E53A');
        $this->addSql('DROP INDEX IDX_DC9B70D48D13E53A');
        $this->addSql('ALTER TABLE webapp.field_measure DROP semi_automatic_variable_id');
    }
}
