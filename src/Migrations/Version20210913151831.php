<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210913151831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow users to join pics with variable scale';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.variable_scale_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.variable_scale_item (id INT NOT NULL, scale_id INT DEFAULT NULL, value INT NOT NULL, text TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8A965F68F73142C2 ON webapp.variable_scale_item (scale_id)');
        $this->addSql('ALTER TABLE webapp.variable_scale_item ADD CONSTRAINT FK_8A965F68F73142C2 FOREIGN KEY (scale_id) REFERENCES webapp.scale (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.variable_scale_item_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.variable_scale_item');
    }
}
