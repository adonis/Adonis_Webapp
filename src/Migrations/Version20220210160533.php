<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220210160533 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow to save the link between a filter and a connectedVariable';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_filter_node ADD generator_variable_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD simple_variable_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD semi_automatic_variable_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD CONSTRAINT FK_530ED705CE8F2B87 FOREIGN KEY (generator_variable_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD CONSTRAINT FK_530ED70536E3D90 FOREIGN KEY (simple_variable_id) REFERENCES webapp.variable_simple (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.path_filter_node ADD CONSTRAINT FK_530ED7058D13E53A FOREIGN KEY (semi_automatic_variable_id) REFERENCES webapp.variable_semi_automatic (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_530ED705CE8F2B87 ON webapp.path_filter_node (generator_variable_id)');
        $this->addSql('CREATE INDEX IDX_530ED70536E3D90 ON webapp.path_filter_node (simple_variable_id)');
        $this->addSql('CREATE INDEX IDX_530ED7058D13E53A ON webapp.path_filter_node (semi_automatic_variable_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.path_filter_node DROP CONSTRAINT FK_530ED705CE8F2B87');
        $this->addSql('ALTER TABLE webapp.path_filter_node DROP CONSTRAINT FK_530ED70536E3D90');
        $this->addSql('ALTER TABLE webapp.path_filter_node DROP CONSTRAINT FK_530ED7058D13E53A');
        $this->addSql('DROP INDEX IDX_530ED705CE8F2B87');
        $this->addSql('DROP INDEX IDX_530ED70536E3D90');
        $this->addSql('DROP INDEX IDX_530ED7058D13E53A');
        $this->addSql('ALTER TABLE webapp.path_filter_node DROP generator_variable_id');
        $this->addSql('ALTER TABLE webapp.path_filter_node DROP simple_variable_id');
        $this->addSql('ALTER TABLE webapp.path_filter_node DROP semi_automatic_variable_id');
    }
}
