<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210722095655 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX shared.uniq_72f7779826346cb2');
        $this->addSql('DROP INDEX shared.uniq_72f7779837c16e3');
        $this->addSql('CREATE INDEX IDX_72F7779826346CB2 ON shared.rel_individual_change (request_individual_id)');
        $this->addSql('CREATE INDEX IDX_72F7779837C16E3 ON shared.rel_individual_change (request_unit_parcel_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX shared.IDX_72F7779826346CB2');
        $this->addSql('DROP INDEX shared.IDX_72F7779837C16E3');
        $this->addSql('CREATE UNIQUE INDEX uniq_72f7779826346cb2 ON shared.rel_individual_change (request_individual_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_72f7779837c16e3 ON shared.rel_individual_change (request_unit_parcel_id)');
    }
}
