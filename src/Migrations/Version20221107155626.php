<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221107155626 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.clone_site_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.clone_site (id INT NOT NULL, origin_site_id INT NOT NULL, result_id INT DEFAULT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, clone_libraries BOOLEAN NOT NULL, clone_users BOOLEAN NOT NULL, clone_platforms BOOLEAN NOT NULL, clone_projects BOOLEAN NOT NULL, clone_entries BOOLEAN NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_875788FF43CA032B ON webapp.clone_site (origin_site_id)');
        $this->addSql('CREATE INDEX IDX_875788FF7A7B643 ON webapp.clone_site (result_id)');
        $this->addSql('CREATE INDEX IDX_875788FFA76ED395 ON webapp.clone_site (user_id)');
        $this->addSql('ALTER TABLE webapp.clone_site ADD CONSTRAINT FK_875788FF43CA032B FOREIGN KEY (origin_site_id) REFERENCES shared.ado_site (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.clone_site ADD CONSTRAINT FK_875788FF7A7B643 FOREIGN KEY (result_id) REFERENCES shared.ado_site (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.clone_site ADD CONSTRAINT FK_875788FFA76ED395 FOREIGN KEY (user_id) REFERENCES shared.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.clone_site_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.clone_site');
    }
}
