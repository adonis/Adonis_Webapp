<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230830080719 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.annotation ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.annotation ADD CONSTRAINT FK_B221861CA4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B221861CA4EEF451 ON webapp.annotation (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.block ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.block ADD CONSTRAINT FK_AC299006A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_AC299006A4EEF451 ON webapp.block (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.device ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD CONSTRAINT FK_35036058A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_35036058A4EEF451 ON webapp.device (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.experiment ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD CONSTRAINT FK_8F0AE05CA4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_8F0AE05CA4EEF451 ON webapp.experiment (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.factor ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD germplasm BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql('ALTER TABLE webapp.factor ADD CONSTRAINT FK_D1143AD6A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D1143AD6A4EEF451 ON webapp.factor (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.individual ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.individual ADD CONSTRAINT FK_1BF644F9A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_1BF644F9A4EEF451 ON webapp.individual (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.measure ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.measure ADD CONSTRAINT FK_EF8B4212A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_EF8B4212A4EEF451 ON webapp.measure (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.modality ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.modality ADD CONSTRAINT FK_88ABA194A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_88ABA194A4EEF451 ON webapp.modality (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD CONSTRAINT FK_C3EBDF93A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_C3EBDF93A4EEF451 ON webapp.out_experimentation_zone (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.sub_block ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.sub_block ADD CONSTRAINT FK_3E1B45FA4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_3E1B45FA4EEF451 ON webapp.sub_block (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD CONSTRAINT FK_67AF1848A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_67AF1848A4EEF451 ON webapp.surfacic_unit_plot (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD CONSTRAINT FK_5EC32917A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_5EC32917A4EEF451 ON webapp.unit_plot (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT FK_E6D775F5A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E6D775F5A4EEF451 ON webapp.variable_generator (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD CONSTRAINT FK_E5B3AE94A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E5B3AE94A4EEF451 ON webapp.variable_semi_automatic (open_silex_instance_id)');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD open_silex_instance_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT FK_920E6B83A4EEF451 FOREIGN KEY (open_silex_instance_id) REFERENCES webapp.opensilex_instance (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_920E6B83A4EEF451 ON webapp.variable_simple (open_silex_instance_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.block DROP CONSTRAINT FK_AC299006A4EEF451');
        $this->addSql('DROP INDEX IDX_AC299006A4EEF451');
        $this->addSql('ALTER TABLE webapp.block DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.device DROP CONSTRAINT FK_35036058A4EEF451');
        $this->addSql('DROP INDEX IDX_35036058A4EEF451');
        $this->addSql('ALTER TABLE webapp.device DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.experiment DROP CONSTRAINT FK_8F0AE05CA4EEF451');
        $this->addSql('DROP INDEX IDX_8F0AE05CA4EEF451');
        $this->addSql('ALTER TABLE webapp.experiment DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.individual DROP CONSTRAINT FK_1BF644F9A4EEF451');
        $this->addSql('DROP INDEX IDX_1BF644F9A4EEF451');
        $this->addSql('ALTER TABLE webapp.individual DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.measure DROP CONSTRAINT FK_EF8B4212A4EEF451');
        $this->addSql('DROP INDEX IDX_EF8B4212A4EEF451');
        $this->addSql('ALTER TABLE webapp.measure DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.sub_block DROP CONSTRAINT FK_3E1B45FA4EEF451');
        $this->addSql('DROP INDEX IDX_3E1B45FA4EEF451');
        $this->addSql('ALTER TABLE webapp.sub_block DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP CONSTRAINT FK_67AF1848A4EEF451');
        $this->addSql('DROP INDEX IDX_67AF1848A4EEF451');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP CONSTRAINT FK_5EC32917A4EEF451');
        $this->addSql('DROP INDEX IDX_5EC32917A4EEF451');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP CONSTRAINT FK_E6D775F5A4EEF451');
        $this->addSql('DROP INDEX IDX_E6D775F5A4EEF451');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP CONSTRAINT FK_E5B3AE94A4EEF451');
        $this->addSql('DROP INDEX IDX_E5B3AE94A4EEF451');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.annotation DROP CONSTRAINT FK_B221861CA4EEF451');
        $this->addSql('DROP INDEX IDX_B221861CA4EEF451');
        $this->addSql('ALTER TABLE webapp.annotation DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.factor DROP CONSTRAINT FK_D1143AD6A4EEF451');
        $this->addSql('DROP INDEX IDX_D1143AD6A4EEF451');
        $this->addSql('ALTER TABLE webapp.factor DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.factor DROP germplasm');
        $this->addSql('ALTER TABLE webapp.modality DROP CONSTRAINT FK_88ABA194A4EEF451');
        $this->addSql('DROP INDEX IDX_88ABA194A4EEF451');
        $this->addSql('ALTER TABLE webapp.modality DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone DROP CONSTRAINT FK_C3EBDF93A4EEF451');
        $this->addSql('DROP INDEX IDX_C3EBDF93A4EEF451');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone DROP open_silex_instance_id');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP CONSTRAINT FK_920E6B83A4EEF451');
        $this->addSql('DROP INDEX IDX_920E6B83A4EEF451');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP open_silex_instance_id');
    }
}
