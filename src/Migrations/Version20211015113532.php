<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211015113532 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Manage out experimentation zones & Add a way to store the color of a particular businessObject';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.out_experimentation_zone_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.out_experimentation_zone (id INT NOT NULL, nature_id INT NOT NULL, unit_plot_id INT DEFAULT NULL, sub_block_id INT DEFAULT NULL, block_id INT DEFAULT NULL, experiment_id INT DEFAULT NULL, number VARCHAR(255) NOT NULL, x INT NOT NULL, y INT NOT NULL, identifier VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C3EBDF933BCB2E4B ON webapp.out_experimentation_zone (nature_id)');
        $this->addSql('CREATE INDEX IDX_C3EBDF933C4405C3 ON webapp.out_experimentation_zone (unit_plot_id)');
        $this->addSql('CREATE INDEX IDX_C3EBDF9353426C55 ON webapp.out_experimentation_zone (sub_block_id)');
        $this->addSql('CREATE INDEX IDX_C3EBDF93E9ED820C ON webapp.out_experimentation_zone (block_id)');
        $this->addSql('CREATE INDEX IDX_C3EBDF93FF444C8 ON webapp.out_experimentation_zone (experiment_id)');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD CONSTRAINT FK_C3EBDF933BCB2E4B FOREIGN KEY (nature_id) REFERENCES webapp.oez_nature (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD CONSTRAINT FK_C3EBDF933C4405C3 FOREIGN KEY (unit_plot_id) REFERENCES webapp.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD CONSTRAINT FK_C3EBDF9353426C55 FOREIGN KEY (sub_block_id) REFERENCES webapp.sub_block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD CONSTRAINT FK_C3EBDF93E9ED820C FOREIGN KEY (block_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD CONSTRAINT FK_C3EBDF93FF444C8 FOREIGN KEY (experiment_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.block ADD color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.individual ADD color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.platform ADD color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.sub_block ADD color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD color INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.out_experimentation_zone_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.out_experimentation_zone');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP color');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP color');
        $this->addSql('ALTER TABLE webapp.platform DROP color');
        $this->addSql('ALTER TABLE webapp.experiment DROP color');
        $this->addSql('ALTER TABLE webapp.sub_block DROP color');
        $this->addSql('ALTER TABLE webapp.block DROP color');
        $this->addSql('ALTER TABLE webapp.individual DROP color');
    }
}
