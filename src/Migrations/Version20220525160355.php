<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220525160355 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Manage the north indicator';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.north_indicator_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.north_indicator (id INT NOT NULL, platform_id INT NOT NULL, orientation INT NOT NULL, x INT NOT NULL, y INT NOT NULL, width INT NOT NULL, height INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_52D6040BFFE6496F ON webapp.north_indicator (platform_id)');
        $this->addSql('ALTER TABLE webapp.north_indicator ADD CONSTRAINT FK_52D6040BFFE6496F FOREIGN KEY (platform_id) REFERENCES webapp.platform (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.north_indicator_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.north_indicator');
    }
}
