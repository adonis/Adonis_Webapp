<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211005094245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'manage oezNature';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.oez_nature_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.oez_nature (id INT NOT NULL, site_id INT DEFAULT NULL, nature VARCHAR(255) NOT NULL, color INT NOT NULL, texture INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A1E07C8CF6BD1646 ON webapp.oez_nature (site_id)');
        $this->addSql('ALTER TABLE webapp.oez_nature ADD CONSTRAINT FK_A1E07C8CF6BD1646 FOREIGN KEY (site_id) REFERENCES shared.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.oez_nature_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.oez_nature');
    }
}
