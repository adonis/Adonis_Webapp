<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211021092123 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add graphical origin and grid size';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.platform ADD x_mesh INT DEFAULT 100');
        $this->addSql('ALTER TABLE webapp.platform ALTER x_mesh DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.platform ADD y_mesh INT DEFAULT 100');
        $this->addSql('ALTER TABLE webapp.platform ALTER y_mesh DROP DEFAULT');
        $this->addSql('ALTER TABLE webapp.platform ADD origin INT DEFAULT 0');
        $this->addSql('ALTER TABLE webapp.platform ALTER origin DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.platform DROP x_mesh');
        $this->addSql('ALTER TABLE webapp.platform DROP y_mesh');
        $this->addSql('ALTER TABLE webapp.platform DROP origin');
    }
}
