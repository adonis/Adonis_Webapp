<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220620145705 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow to track session experimenter';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.session_data ADD user_id INT DEFAULT NULL');
        $this->addSql('UPDATE webapp.session_data SET user_id = (SELECT user_id FROM webapp.project_data where project_data_id = webapp.project_data.id)');
        $this->addSql('ALTER TABLE webapp.session_data ADD CONSTRAINT FK_6EC58AF0A76ED395 FOREIGN KEY (user_id) REFERENCES shared.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6EC58AF0A76ED395 ON webapp.session_data (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.session_data DROP CONSTRAINT FK_6EC58AF0A76ED395');
        $this->addSql('DROP INDEX IDX_6EC58AF0A76ED395');
        $this->addSql('ALTER TABLE webapp.session_data DROP user_id');
    }
}
