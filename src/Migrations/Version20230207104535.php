<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230207104535 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Change la taille max de l\'objectif d\'un protocole';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER INDEX adonis.idx_76509c9321113fc RENAME TO IDX_42BA81C0321113FC');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d406f4fa591 RENAME TO IDX_B221861C6F4FA591');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d40b2ab2fbc RENAME TO IDX_B221861CB2AB2FBC');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d4015b6dee0 RENAME TO IDX_B221861C15B6DEE0');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d40dc16d692 RENAME TO IDX_B221861CDC16D692');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d40b310bf04 RENAME TO IDX_B221861CB310BF04');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d4033465265 RENAME TO IDX_B221861C33465265');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d4013787779 RENAME TO IDX_B221861C13787779');
        $this->addSql('ALTER INDEX webapp.idx_f7c61d40613fecdf RENAME TO IDX_B221861C613FECDF');
        $this->addSql('ALTER INDEX webapp.idx_d713fdf0166d1f9c RENAME TO IDX_F6EEEA09166D1F9C');
        $this->addSql('ALTER INDEX adonis.idx_86c5a4dfd616280 RENAME TO IDX_F6456AE2D616280');
        $this->addSql('ALTER TABLE webapp.protocol ALTER aim TYPE TEXT');
        $this->addSql('ALTER TABLE webapp.protocol ALTER aim DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER INDEX webapp.idx_f6eeea09166d1f9c RENAME TO idx_d713fdf0166d1f9c');
        $this->addSql('ALTER TABLE webapp.protocol ALTER aim TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE webapp.protocol ALTER aim DROP DEFAULT');
        $this->addSql('ALTER INDEX adonis.idx_42ba81c0321113fc RENAME TO idx_76509c9321113fc');
        $this->addSql('ALTER INDEX adonis.idx_f6456ae2d616280 RENAME TO idx_86c5a4dfd616280');
        $this->addSql('ALTER INDEX webapp.idx_b221861c613fecdf RENAME TO idx_f7c61d40613fecdf');
        $this->addSql('ALTER INDEX webapp.idx_b221861cdc16d692 RENAME TO idx_f7c61d40dc16d692');
        $this->addSql('ALTER INDEX webapp.idx_b221861cb2ab2fbc RENAME TO idx_f7c61d40b2ab2fbc');
        $this->addSql('ALTER INDEX webapp.idx_b221861c13787779 RENAME TO idx_f7c61d4013787779');
        $this->addSql('ALTER INDEX webapp.idx_b221861c6f4fa591 RENAME TO idx_f7c61d406f4fa591');
        $this->addSql('ALTER INDEX webapp.idx_b221861c33465265 RENAME TO idx_f7c61d4033465265');
        $this->addSql('ALTER INDEX webapp.idx_b221861cb310bf04 RENAME TO idx_f7c61d40b310bf04');
        $this->addSql('ALTER INDEX webapp.idx_b221861c15b6dee0 RENAME TO idx_f7c61d4015b6dee0');
    }
}
