<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230718143814 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.annotation ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.block ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.factor ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.individual ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.measure ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.modality ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.sub_block ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD open_silex_uri VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.factor DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.experiment DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.device DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.block DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.measure DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.out_experimentation_zone DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.modality DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.individual DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.sub_block DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.variable_generator DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP open_silex_uri');
        $this->addSql('ALTER TABLE webapp.annotation DROP open_silex_uri');
    }
}
