<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210720160656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add a change report for Webapp';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.change_report_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.change_report (id INT NOT NULL, involved_individual_id INT DEFAULT NULL, involved_surfacic_unit_plot_id INT DEFAULT NULL, project_data_id INT NOT NULL, aproved BOOLEAN NOT NULL, change_type VARCHAR(255) NOT NULL, change_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7EA6D645F4A37176 ON webapp.change_report (involved_individual_id)');
        $this->addSql('CREATE INDEX IDX_7EA6D645B6DA3871 ON webapp.change_report (involved_surfacic_unit_plot_id)');
        $this->addSql('CREATE INDEX IDX_7EA6D645A1EBAD59 ON webapp.change_report (project_data_id)');
        $this->addSql('ALTER TABLE webapp.change_report ADD CONSTRAINT FK_7EA6D645F4A37176 FOREIGN KEY (involved_individual_id) REFERENCES webapp.individual (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.change_report ADD CONSTRAINT FK_7EA6D645B6DA3871 FOREIGN KEY (involved_surfacic_unit_plot_id) REFERENCES webapp.surfacic_unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.change_report ADD CONSTRAINT FK_7EA6D645A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.change_report_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.change_report');
    }
}
