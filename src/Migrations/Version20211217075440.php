<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211217075440 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow the admin to store Algorithms';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.algorithm_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.algorithm_condition_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.algorithm_parameter_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.algorithm (id INT NOT NULL, name VARCHAR(255) NOT NULL, script_name VARCHAR(255) NOT NULL, with_sub_block BOOLEAN NOT NULL, with_repartition BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE webapp.algorithm_condition (id INT NOT NULL, algorithm_id INT DEFAULT NULL, type VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_6D870C1FBBEB6CF7 ON webapp.algorithm_condition (algorithm_id)');
        $this->addSql('CREATE TABLE webapp.algorithm_parameter (id INT NOT NULL, algorithm_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, script_name INT NOT NULL, special_param VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FAC6154CBBEB6CF7 ON webapp.algorithm_parameter (algorithm_id)');
        $this->addSql('ALTER TABLE webapp.algorithm_condition ADD CONSTRAINT FK_6D870C1FBBEB6CF7 FOREIGN KEY (algorithm_id) REFERENCES webapp.algorithm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.algorithm_parameter ADD CONSTRAINT FK_FAC6154CBBEB6CF7 FOREIGN KEY (algorithm_id) REFERENCES webapp.algorithm (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.algorithm_condition DROP CONSTRAINT FK_6D870C1FBBEB6CF7');
        $this->addSql('ALTER TABLE webapp.algorithm_parameter DROP CONSTRAINT FK_FAC6154CBBEB6CF7');
        $this->addSql('DROP SEQUENCE webapp.algorithm_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.algorithm_condition_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.algorithm_parameter_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.algorithm');
        $this->addSql('DROP TABLE webapp.algorithm_condition');
        $this->addSql('DROP TABLE webapp.algorithm_parameter');
    }
}
