<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220126145243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'correct the semi automatic variable link to previous value';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device DROP CONSTRAINT fk_35036058a1ebad59');
        $this->addSql('DROP INDEX webapp.idx_35036058a1ebad59');
        $this->addSql('ALTER TABLE webapp.device DROP project_data_id');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD project_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic ADD CONSTRAINT FK_E5B3AE94A1EBAD59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E5B3AE94A1EBAD59 ON webapp.variable_semi_automatic (project_data_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.device ADD project_data_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.device ADD CONSTRAINT fk_35036058a1ebad59 FOREIGN KEY (project_data_id) REFERENCES webapp.project_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_35036058a1ebad59 ON webapp.device (project_data_id)');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP CONSTRAINT FK_E5B3AE94A1EBAD59');
        $this->addSql('DROP INDEX webapp.IDX_E5B3AE94A1EBAD59');
        $this->addSql('ALTER TABLE webapp.variable_semi_automatic DROP project_data_id');
    }
}
