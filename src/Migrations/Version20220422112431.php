<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220422112431 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Manage experiments attachments';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.attachment_experiment_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shared.file_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.attachment_experiment (id INT NOT NULL, experiment_id INT DEFAULT NULL, file_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_37C7D7ABFF444C8 ON webapp.attachment_experiment (experiment_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_37C7D7AB93CB796C ON webapp.attachment_experiment (file_id)');
        $this->addSql('CREATE TABLE shared.file (id INT NOT NULL, owner_id INT DEFAULT NULL, file_name VARCHAR(255) NOT NULL, original_file_name VARCHAR(255) NOT NULL, size INT NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F2DB94F77E3C61F9 ON shared.file (owner_id)');
        $this->addSql('ALTER TABLE webapp.attachment_experiment ADD CONSTRAINT FK_37C7D7ABFF444C8 FOREIGN KEY (experiment_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.attachment_experiment ADD CONSTRAINT FK_37C7D7AB93CB796C FOREIGN KEY (file_id) REFERENCES shared.file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE shared.file ADD CONSTRAINT FK_F2DB94F77E3C61F9 FOREIGN KEY (owner_id) REFERENCES shared.ado_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.attachment_experiment DROP CONSTRAINT FK_37C7D7AB93CB796C');
        $this->addSql('DROP SEQUENCE webapp.attachment_experiment_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shared.file_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.attachment_experiment');
        $this->addSql('DROP TABLE shared.file');
    }
}
