<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210426121825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add new user informations';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.ado_user ADD name VARCHAR(255) NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE adonis.ado_user ALTER name  DROP DEFAULT');
        $this->addSql('ALTER TABLE adonis.ado_user ADD surname VARCHAR(255) NOT NULL DEFAULT \'\'');
        $this->addSql('ALTER TABLE adonis.ado_user ALTER surname  DROP DEFAULT');
        $this->addSql('ALTER TABLE adonis.ado_user ADD active BOOLEAN NOT NULL DEFAULT TRUE');
        $this->addSql('ALTER TABLE adonis.ado_user ALTER active DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE adonis.ado_user DROP name');
        $this->addSql('ALTER TABLE adonis.ado_user DROP surname');
        $this->addSql('ALTER TABLE adonis.ado_user DROP active');
    }
}
