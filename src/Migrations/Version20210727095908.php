<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210727095908 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.file_response DROP CONSTRAINT FK_3D93C837166D1F9C');
        $this->addSql('ALTER TABLE webapp.file_response ADD CONSTRAINT FK_3D93C837166D1F9C FOREIGN KEY (project_id) REFERENCES adonis.project (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.file_response DROP CONSTRAINT fk_3d93c837166d1f9c');
        $this->addSql('ALTER TABLE webapp.file_response ADD CONSTRAINT fk_3d93c837166d1f9c FOREIGN KEY (project_id) REFERENCES adonis.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
