<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210622150946 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add variable managment';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.variable_generator_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.variable_simple_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.variable_generator (id INT NOT NULL, origin_site_id INT DEFAULT NULL, site_id INT DEFAULT NULL, generator_variable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, repetitions INT NOT NULL, unit VARCHAR(255) DEFAULT NULL, path_level VARCHAR(255) NOT NULL, comment TEXT DEFAULT NULL, print_order INT DEFAULT NULL, format VARCHAR(255) DEFAULT NULL, format_length INT DEFAULT NULL, default_true_value BOOLEAN DEFAULT NULL, type VARCHAR(255) NOT NULL, identifier VARCHAR(255) DEFAULT NULL, mandatory BOOLEAN NOT NULL, generated_prefix VARCHAR(255) NOT NULL, numeral_increment BOOLEAN NOT NULL, path_way_horizontal BOOLEAN NOT NULL, direct_counting BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E6D775F543CA032B ON webapp.variable_generator (origin_site_id)');
        $this->addSql('CREATE INDEX IDX_E6D775F5F6BD1646 ON webapp.variable_generator (site_id)');
        $this->addSql('CREATE INDEX IDX_E6D775F5CE8F2B87 ON webapp.variable_generator (generator_variable_id)');
        $this->addSql('CREATE TABLE webapp.variable_simple (id INT NOT NULL, origin_site_id INT DEFAULT NULL, site_id INT DEFAULT NULL, generator_variable_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(255) NOT NULL, repetitions INT NOT NULL, unit VARCHAR(255) DEFAULT NULL, path_level VARCHAR(255) NOT NULL, comment TEXT DEFAULT NULL, print_order INT DEFAULT NULL, format VARCHAR(255) DEFAULT NULL, format_length INT DEFAULT NULL, default_true_value BOOLEAN DEFAULT NULL, type VARCHAR(255) NOT NULL, identifier VARCHAR(255) DEFAULT NULL, mandatory BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_920E6B8343CA032B ON webapp.variable_simple (origin_site_id)');
        $this->addSql('CREATE INDEX IDX_920E6B83F6BD1646 ON webapp.variable_simple (site_id)');
        $this->addSql('CREATE INDEX IDX_920E6B83CE8F2B87 ON webapp.variable_simple (generator_variable_id)');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT FK_E6D775F543CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT FK_E6D775F5F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_generator ADD CONSTRAINT FK_E6D775F5CE8F2B87 FOREIGN KEY (generator_variable_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT FK_920E6B8343CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT FK_920E6B83F6BD1646 FOREIGN KEY (site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.variable_simple ADD CONSTRAINT FK_920E6B83CE8F2B87 FOREIGN KEY (generator_variable_id) REFERENCES webapp.variable_generator (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.variable_generator DROP CONSTRAINT FK_E6D775F5CE8F2B87');
        $this->addSql('ALTER TABLE webapp.variable_simple DROP CONSTRAINT FK_920E6B83CE8F2B87');
        $this->addSql('DROP SEQUENCE webapp.variable_generator_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.variable_simple_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.variable_generator');
        $this->addSql('DROP TABLE webapp.variable_simple');
    }
}
