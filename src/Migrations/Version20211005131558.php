<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211005131558 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Store 2 additionnal colors';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD selection_rectangle_color INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.graphical_configuration ADD selected_item_color INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP selection_rectangle_color');
        $this->addSql('ALTER TABLE webapp.graphical_configuration DROP selected_item_color');
    }
}
