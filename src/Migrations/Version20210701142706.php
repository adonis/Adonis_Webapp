<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210701142706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.rel_user_app_project_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.rel_user_app_project (id INT NOT NULL, mobile_status_id INT DEFAULT NULL, webapp_project_id INT DEFAULT NULL, origin_site_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C5C2E00AE7E94BE2 ON webapp.rel_user_app_project (mobile_status_id)');
        $this->addSql('CREATE INDEX IDX_C5C2E00AB5A42A41 ON webapp.rel_user_app_project (webapp_project_id)');
        $this->addSql('CREATE INDEX IDX_C5C2E00A43CA032B ON webapp.rel_user_app_project (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.rel_user_app_project ADD CONSTRAINT FK_C5C2E00AE7E94BE2 FOREIGN KEY (mobile_status_id) REFERENCES adonis.ado_user_project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_user_app_project ADD CONSTRAINT FK_C5C2E00AB5A42A41 FOREIGN KEY (webapp_project_id) REFERENCES webapp.project (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.rel_user_app_project ADD CONSTRAINT FK_C5C2E00A43CA032B FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.rel_user_app_project_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.rel_user_app_project');
    }
}
