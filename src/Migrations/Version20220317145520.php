<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220317145520 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Update old projectData user link';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('update webapp.project_data d set user_id = (select owner_id from webapp.project where d.project_id = id) where user_id is null');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
