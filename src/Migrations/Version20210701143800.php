<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210701143800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Drop Has Origin site';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.experiment DROP CONSTRAINT fk_8f0ae05c43ca032b');
        $this->addSql('ALTER TABLE webapp.experiment DROP origin_site_id');
        $this->addSql('ALTER TABLE webapp.protocol DROP CONSTRAINT fk_7012951843ca032b');
        $this->addSql('ALTER TABLE webapp.protocol DROP origin_site_id');
        $this->addSql('ALTER TABLE webapp.state_code DROP CONSTRAINT fk_7f9301b043ca032b');
        $this->addSql('ALTER TABLE webapp.state_code DROP origin_site_id');
        $this->addSql('ALTER TABLE webapp.value_list DROP CONSTRAINT fk_71421b2f43ca032b');
        $this->addSql('ALTER TABLE webapp.value_list DROP origin_site_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.protocol ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.protocol ADD CONSTRAINT fk_7012951843ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7012951843ca032b ON webapp.protocol (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.experiment ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.experiment ADD CONSTRAINT fk_8f0ae05c43ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_8f0ae05c43ca032b ON webapp.experiment (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.state_code ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.state_code ADD CONSTRAINT fk_7f9301b043ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_7f9301b043ca032b ON webapp.state_code (origin_site_id)');
        $this->addSql('ALTER TABLE webapp.value_list ADD origin_site_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE webapp.value_list ADD CONSTRAINT fk_71421b2f43ca032b FOREIGN KEY (origin_site_id) REFERENCES adonis.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_71421b2f43ca032b ON webapp.value_list (origin_site_id)');
    }
}
