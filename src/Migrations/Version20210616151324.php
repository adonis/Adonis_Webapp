<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210616151324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Allow to create experiments children';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.block_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.individual_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.sub_block_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.surfacic_unit_plot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.unit_plot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.block (id INT NOT NULL, experiment_id INT DEFAULT NULL, number VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AC299006FF444C8 ON webapp.block (experiment_id)');
        $this->addSql('CREATE TABLE webapp.individual (id INT NOT NULL, unit_plot_id INT NOT NULL, number VARCHAR(255) NOT NULL, x INT NOT NULL, y INT NOT NULL, dead BOOLEAN NOT NULL, appeared TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, disappeared TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, identifier VARCHAR(255) DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1BF644F93C4405C3 ON webapp.individual (unit_plot_id)');
        $this->addSql('CREATE TABLE webapp.sub_block (id INT NOT NULL, block_id INT DEFAULT NULL, number VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3E1B45FE9ED820C ON webapp.sub_block (block_id)');
        $this->addSql('CREATE TABLE webapp.surfacic_unit_plot (id INT NOT NULL, treatment_id INT NOT NULL, block_id INT DEFAULT NULL, sub_block_id INT DEFAULT NULL, number VARCHAR(255) NOT NULL, x INT NOT NULL, y INT NOT NULL, dead BOOLEAN NOT NULL, appeared TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, disappeared TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, identifier VARCHAR(255) DEFAULT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_67AF1848471C0366 ON webapp.surfacic_unit_plot (treatment_id)');
        $this->addSql('CREATE INDEX IDX_67AF1848E9ED820C ON webapp.surfacic_unit_plot (block_id)');
        $this->addSql('CREATE INDEX IDX_67AF184853426C55 ON webapp.surfacic_unit_plot (sub_block_id)');
        $this->addSql('CREATE TABLE webapp.unit_plot (id INT NOT NULL, treatment_id INT NOT NULL, block_id INT DEFAULT NULL, sub_block_id INT DEFAULT NULL, number VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5EC32917471C0366 ON webapp.unit_plot (treatment_id)');
        $this->addSql('CREATE INDEX IDX_5EC32917E9ED820C ON webapp.unit_plot (block_id)');
        $this->addSql('CREATE INDEX IDX_5EC3291753426C55 ON webapp.unit_plot (sub_block_id)');
        $this->addSql('ALTER TABLE webapp.block ADD CONSTRAINT FK_AC299006FF444C8 FOREIGN KEY (experiment_id) REFERENCES webapp.experiment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.individual ADD CONSTRAINT FK_1BF644F93C4405C3 FOREIGN KEY (unit_plot_id) REFERENCES webapp.unit_plot (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.sub_block ADD CONSTRAINT FK_3E1B45FE9ED820C FOREIGN KEY (block_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD CONSTRAINT FK_67AF1848471C0366 FOREIGN KEY (treatment_id) REFERENCES webapp.treatment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD CONSTRAINT FK_67AF1848E9ED820C FOREIGN KEY (block_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot ADD CONSTRAINT FK_67AF184853426C55 FOREIGN KEY (sub_block_id) REFERENCES webapp.sub_block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD CONSTRAINT FK_5EC32917471C0366 FOREIGN KEY (treatment_id) REFERENCES webapp.treatment (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD CONSTRAINT FK_5EC32917E9ED820C FOREIGN KEY (block_id) REFERENCES webapp.block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.unit_plot ADD CONSTRAINT FK_5EC3291753426C55 FOREIGN KEY (sub_block_id) REFERENCES webapp.sub_block (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE webapp.sub_block DROP CONSTRAINT FK_3E1B45FE9ED820C');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP CONSTRAINT FK_67AF1848E9ED820C');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP CONSTRAINT FK_5EC32917E9ED820C');
        $this->addSql('ALTER TABLE webapp.surfacic_unit_plot DROP CONSTRAINT FK_67AF184853426C55');
        $this->addSql('ALTER TABLE webapp.unit_plot DROP CONSTRAINT FK_5EC3291753426C55');
        $this->addSql('ALTER TABLE webapp.individual DROP CONSTRAINT FK_1BF644F93C4405C3');
        $this->addSql('DROP SEQUENCE webapp.block_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.individual_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.sub_block_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.surfacic_unit_plot_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.unit_plot_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.block');
        $this->addSql('DROP TABLE webapp.individual');
        $this->addSql('DROP TABLE webapp.sub_block');
        $this->addSql('DROP TABLE webapp.surfacic_unit_plot');
        $this->addSql('DROP TABLE webapp.unit_plot');
    }
}
