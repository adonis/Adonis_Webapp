<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220502073511 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Manage attachments for platform and protocol objects';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.attachment_platform_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE webapp.attachment_protocol_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.attachment_platform (id INT NOT NULL, platform_id INT DEFAULT NULL, file_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_798C419AFFE6496F ON webapp.attachment_platform (platform_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_798C419A93CB796C ON webapp.attachment_platform (file_id)');
        $this->addSql('CREATE TABLE webapp.attachment_protocol (id INT NOT NULL, protocol_id INT DEFAULT NULL, file_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_881E2D1DCCD59258 ON webapp.attachment_protocol (protocol_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_881E2D1D93CB796C ON webapp.attachment_protocol (file_id)');
        $this->addSql('ALTER TABLE webapp.attachment_platform ADD CONSTRAINT FK_798C419AFFE6496F FOREIGN KEY (platform_id) REFERENCES webapp.platform (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.attachment_platform ADD CONSTRAINT FK_798C419A93CB796C FOREIGN KEY (file_id) REFERENCES shared.file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.attachment_protocol ADD CONSTRAINT FK_881E2D1DCCD59258 FOREIGN KEY (protocol_id) REFERENCES webapp.protocol (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.attachment_protocol ADD CONSTRAINT FK_881E2D1D93CB796C FOREIGN KEY (file_id) REFERENCES shared.file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.attachment_platform_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE webapp.attachment_protocol_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.attachment_platform');
        $this->addSql('DROP TABLE webapp.attachment_protocol');
    }
}
