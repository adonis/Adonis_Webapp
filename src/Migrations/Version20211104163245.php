<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211104163245 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'add a parsing job entity to initiate XML/CSV object parsing';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE webapp.parsing_job_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE webapp.parsing_job (id INT NOT NULL, site_id INT NOT NULL, user_id INT NOT NULL, object_type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, file_path VARCHAR(255) DEFAULT NULL, uniq_directory_name TEXT DEFAULT NULL, upload_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8011B1E9F6BD1646 ON webapp.parsing_job (site_id)');
        $this->addSql('CREATE INDEX IDX_8011B1E9A76ED395 ON webapp.parsing_job (user_id)');
        $this->addSql('ALTER TABLE webapp.parsing_job ADD CONSTRAINT FK_8011B1E9F6BD1646 FOREIGN KEY (site_id) REFERENCES shared.ado_site (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.parsing_job ADD CONSTRAINT FK_8011B1E9A76ED395 FOREIGN KEY (user_id) REFERENCES shared.ado_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE webapp.experiment ALTER state SET NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE webapp.parsing_job_id_seq CASCADE');
        $this->addSql('DROP TABLE webapp.parsing_job');
        $this->addSql('ALTER TABLE webapp.experiment ALTER state DROP NOT NULL');
    }
}
