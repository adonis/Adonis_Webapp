<?php

declare(strict_types=1);

/*
 * @author TRYDEA - 2024
 */

namespace Front;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Serves frontend code
 * Handle some "frontend" related routes.
 */
class FrontController extends AbstractController
{
    /**
     * Point d'accès à l'application mobile.
     *
     * @Route("/", name="root")
     */
    public function rootAction(): Response
    {
        return $this->redirectToRoute('appMobile');
    }

    /**
     * Point d'accès à l'application mobile.
     *
     * @Route("/app/", name="appMobile")
     */
    public function appAction(): Response
    {
        return $this->render('app.html.twig');
    }

    /**
     * Point d'accès à l'application webapp.
     *
     * @Route("/webapp/", name="webapp")
     */
    public function webappAction(): Response
    {
        return $this->render('webapp.html.twig');
    }

    /**
     * @Route("/phpinfo", name="phpinfo")
     */
    public function phpinfoAction(): Response
    {
        return new Response('<html lang="en"><body>'.phpinfo().'</body></html>');
    }

    /**
     * @Route("/version", name="version")
     */
    public function versionAction(): JsonResponse
    {
        return new JsonResponse(['version' => '2.6.8']);
    }
}
