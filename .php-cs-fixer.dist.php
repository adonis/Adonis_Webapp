<?php

/*
 * @author TRYDEA - 2024
 */

$enteteFichier = '@author TRYDEA - 2024';

$finder = (new PhpCsFixer\Finder())
    ->in(__DIR__)
    ->exclude('config')
    ->exclude('var')
    ->exclude('reports')
    ->exclude('public/bundles')
    ->exclude('public/build')
    ->notPath('bin/console')
    ->notPath('public/index.php')
    ->notPath('.php-cs-fixer.dist.php');

return (new PhpCsFixer\Config())
    ->setRiskyAllowed(true)
    ->setRules([
        '@PHP74Migration' => true,
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'general_phpdoc_annotation_remove' => ['annotations' => ['author', 'version', 'category', 'package']],
        'header_comment' => ['header' => $enteteFichier, 'separate' => 'both'],
        'mb_str_functions' => true,
        'no_php4_constructor' => true,
        'no_superfluous_phpdoc_tags' => [
            'allow_mixed' => true,
            'remove_inheritdoc' => true,
            'allow_unused_params' => true,
        ],
        'no_unreachable_default_argument_value' => true,
        'no_useless_else' => true,
        'no_useless_return' => true,
        'php_unit_strict' => true,
        'strict_comparison' => true,
        'strict_param' => true,
    ])
    ->setFinder($finder);
