let Encore = require('@symfony/webpack-encore');
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');
const path = require('path');

const MODE_DEV = 'development';
const VERSION = require("./package.json").version
const TEST_VERSION_SUFFIX = '-test';
const PREPROD_VERSION_SUFFIX = 'preprod';

console.log('-----');
console.log('i [ado]: VERSION: ', VERSION);
console.log('-----');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || MODE_DEV);
}

function setCommonConfig(Encore) {
    Encore
        .addAliases({
            '@adonis': path.resolve(__dirname, 'assets'),
        })
        .configureDevServerOptions(option => {
            option.public = 'https://localhost:8080/'
        })
        .splitEntryChunks()
        .enableSingleRuntimeChunk()

        .cleanupOutputBeforeBuild()
        .enableBuildNotifications()
        .enableSourceMaps(!Encore.isProduction())
        .enableVersioning(Encore.isProduction())

        .configureBabelPresetEnv((config) => {
            config.useBuiltIns = 'usage';
            config.corejs = 3;
        })
        .enableTypeScriptLoader(function (tsConfig) {
            tsConfig.transpileOnly = true
            tsConfig.appendTsSuffixTo = [/\.vue$/]
        })
        .enableForkedTypeScriptTypesChecking((config) => {
            config.typescript = {
                extensions: {
                    vue: true
                }
            }
        })
        .enableVueLoader(() => {
        }, {runtimeCompilerBuild: true})
        .addPlugin(new VuetifyLoaderPlugin())

        .enableSassLoader(options => {
            options.implementation = require('sass');
        })

        .configureDefinePlugin((options) => {
            options['process.env'].VERSION = JSON.stringify(VERSION);

        })
    ;
}

// ----- APP APPLICATION ------------------------------------------------------------------------------------
Encore
    .setOutputPath('public/app')
    .setPublicPath('/app')
    .addEntry('app', './assets/app/app.ts')
;

setCommonConfig(Encore);

Encore
    .addPlugin(new WebpackPwaManifest({
        filename: "manifest.webmanifest",
        inject: false,
        fingerprints: false,
        start_url: '/app',
        name: 'Adonis Terrain',
        orientation: undefined,
        short_name:
            VERSION.includes(TEST_VERSION_SUFFIX) ?
                'Adonis TEST' :
                VERSION.includes(PREPROD_VERSION_SUFFIX) ?
                    'Adonis PreProd' :
                    'Adonis',
        description: 'Portage de Adonis Terrain sur navigateur',
        background_color: '#ffffff',
        crossorigin: 'use-credentials',
        type: "privileged",
        permissions: {
            "audio-capture": {
                description: "Audio capture"
            },
            "speech-recognition": {
                description: "Speech recognition"
            }
        },
        icons: [
            {
                src:
                    VERSION.includes(TEST_VERSION_SUFFIX) ?
                        './assets/shared/images/adonis-icon-test.png' :
                        VERSION.includes(PREPROD_VERSION_SUFFIX) ?
                            './assets/shared/images/adonis-icon-pprod.png' :
                            './assets/shared/images/adonis-icon.png',
                sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
            },
        ]
    }))
    .addPlugin(new WorkboxPlugin.GenerateSW({
        clientsClaim: true,
        skipWaiting: true,
        maximumFileSizeToCacheInBytes: 200000000,
        runtimeCaching: [{
            urlPattern: new RegExp('/app'),
            handler: 'StaleWhileRevalidate',
        }]
    }));

const app = Encore.getWebpackConfig();
app.name = 'app';

// ----- RESET
Encore.reset();

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

// ----- RESET
Encore.reset();

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

// ----- CLIENT APPLICATION ---------------------------------------------------------------------------------
Encore
    .setOutputPath('public/webapp')
    .setPublicPath('/webapp')
    .addEntry('webapp', './assets/webapp/webapp.ts');

setCommonConfig(Encore);

const webapp = Encore.getWebpackConfig();
webapp.name = 'webapp';
webapp.node = {
    fs: 'empty'
}

// ----- EXPORT
module.exports = [
    app,
    webapp
];
