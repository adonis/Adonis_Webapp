# Adonis Mobile

Adonis Mobile is a Single Page Application allowing the user to enforce data entry projects from ZIP file generated
from Adonis Bureau.

This version of Adonis Terrain aim to replace Adonis Terrain WAP & CFU1. Running on Chrome browser,
this application can also be installed on Android devices just like native applications. Offline,
connected operator can still enforce project stored locally in the app database.

# Adonis Webapp

Adonis Webapp is a web application allowing its users to define a set of experiments to study. The purpose of this app
is to follow this experiment's evolution by define variables on them.

Unlikely Adonis Bureau, this app uses a shared database for all users and for all sites. It can so allow the user to
share their site and work on the same projects on different devices with different users.

### Production

Adonis Mobile can be installed using two different authentication mode : LDAP or DEFAULT.
By default, the administrator has the ability to create, modify or renew user password through a specific admin page.

To proceed with production installation, follow the dedicated procedure:

* [Install a production environment](doc/DOC--PROD-INSTALL.md)
* [Update a production environment](doc/DOC--PROD-UPDATE.md)

### Development

Complete guide to install an get a development instance up and running available:

* [Install a development environment](doc/DOC--DEV-INSTALL.md)
* [Understand application structure for development purpose](doc/DOC--DEVELOPMENT.md)

# Licence

CeCILL-2.1

Copyright 2023 INRAE

## Authors :

* [Antoine JACQUE](mailto:ajacque@trydea.fr) - dev and specifications
* [Aurélien BERNARD](mailto:abernard@trydea.fr) - dev and specifications
* [Audrey PALAIN-SAINT-AGATHE](mailto:audrey.palain-saint-agathe@inrae.fr) - specifications
* [Vincent DUMAS](mailto:vincent.dumas@inrae.fr) - specifications
* [Fabien SURAULT](mailto:fabien.surault@inrae.fr) - specifications
* [Laurent FALCHETTO](mailto:laurent.falchetto@inrae.fr) - specifications
* [Guillaume BODINEAU](mailto:pascal.bertin@inrae.fr) - specifications
* [Frédéric BERNIER](mailto:frederic.bernier@inrae.fr) - specifications
* [David ALLETRU](mailto:david.alletru@inrae.fr) - specifications
* [Julien PARMENTIER](mailto:julien.parmentier@inrae.fr) - specifications
* [David LANOUE](mailto:david.lanoue@inrae.fr) - specifications



adonis@inrae.fr

## CeCILL-2.1

This software is a computer program used to capture experimental data in the domain of vegetal (field crops, row crops, natural areas, crops under cover, etc.).

This software is governed by the [CeCILL-2.1](https://forgemia.inra.fr/adonis/Adonis_Webapp/-/blob/99c18ccaf1a92302e2f308077a2e465cda11fb2a/LICENSE) license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the [CeCILL-2.1](https://forgemia.inra.fr/adonis/Adonis_Webapp/-/blob/99c18ccaf1a92302e2f308077a2e465cda11fb2a/LICENSE)
license as circulated by CEA, CNRS and INRIA at the following URL
[https://www.cecill.info](https://www.cecill.info)

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the [CeCILL-2.1](https://forgemia.inra.fr/adonis/Adonis_Webapp/-/blob/99c18ccaf1a92302e2f308077a2e465cda11fb2a/LICENSE)  license and that you accept its terms.

