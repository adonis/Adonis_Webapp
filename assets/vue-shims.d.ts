declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'vuetify/lib' {
  import 'vuetify/types/lib'
}

declare type VForm = Vue & {
  validate: () => boolean,
  reset: () => void,
  resetValidation: () => void,
}

declare module 'jimp/es'
