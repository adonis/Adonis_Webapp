import User from '@adonis/shared/models/user'

export class ConnectionState {
  userToken: string = null
  refreshToken: string = null
  user: User = null
  roles: string[] = []
}