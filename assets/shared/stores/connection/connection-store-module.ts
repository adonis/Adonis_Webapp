import { ROLE_ADMIN } from '@adonis/shared/constants'
import { BASE_URL } from '@adonis/shared/env'
import { LoginDto } from '@adonis/shared/models/login-dto'
import User from '@adonis/shared/models/user'
import { ConnectionState } from '@adonis/shared/stores/connection/connection-state'
import ConnectionStore from '@adonis/shared/stores/connection/connection-store'
import axios, { AxiosError, AxiosResponse } from 'axios'

export const STORAGE_KEY = 'Adonis::ConnectionStore'

/**
 * Store CONNECTION. Save authentication token for API calls. Contains refreshToken to performs automatic reconnection.
 */
export default {

  namespaced: true,

  state: new ConnectionState(),

  getters: {

    authenticationHeader( currentState: ConnectionState ): string {
      return (null !== currentState.userToken)
          ? 'Bearer ' + currentState.userToken
          : null
    },

    roles( currentState: ConnectionState ): string[] {
      return (null !== currentState.user)
          ? currentState.roles
          : []
    },

    isAdmin( currentState: ConnectionState ): boolean {
      return (null !== currentState.user)
          ? currentState.roles.includes( ROLE_ADMIN )
          : false
    },

    user( currentState: ConnectionState ): User {
      return ( null !== currentState.user )
             ? currentState.user
             : null
    },
  },

  actions: {

    updateTokens( connectionStore: ConnectionStore, tokens: LoginDto ): Promise<any> {
      connectionStore.commit( 'setTokens', tokens )
      return Promise.resolve()
    },

    updateUser( connectionStore: ConnectionStore, user: User ): Promise<any> {
      connectionStore.commit( 'setUser', {
        user,
      } )
      return Promise.resolve()
    },

    refresh( connectionStore: ConnectionStore ): Promise<boolean> {
      return axios.create( { baseURL: BASE_URL } )
          .post( 'authentication/refresh', { refresh_token: connectionStore.state.refreshToken } )
          .then( ( response: AxiosResponse ) => {
            connectionStore.commit( 'setTokens', {
              token: response.data.token,
              refresh_token: response.data.refresh_token,
            } )
            return true
          } )
          .catch( ( error: AxiosError ) => {
            connectionStore.commit( 'clear' )
            console.log( error )
            console.log( error.response )
            return false
          } )
    },

    disconnect( data: ConnectionStore ): Promise<void> {
      return new Promise( ( resolve: ( ...args: any ) => void ) => {
        data.commit( 'clear' )
        resolve()
      } )
    },
  },

  mutations: {

    clear( currentState: ConnectionState ): void {
      currentState.roles = []
      currentState.userToken = null
      currentState.refreshToken = null
      currentState.user = null
      // Update local storage to remove all connection info.
      window.localStorage.removeItem( STORAGE_KEY )
    },

    setTokens( currentState: ConnectionState, payload: {
      token: string,
      refresh_token: string,
    } ): void {
      currentState.userToken = payload.token
      currentState.refreshToken = payload.refresh_token
      // Update local storage with new tokens.
      window.localStorage.setItem( STORAGE_KEY, JSON.stringify( currentState ) )
    },

    setUser( currentState: ConnectionState, payload: {
      user: User,
      roles: string[],
    } ): void {
      currentState.user = payload.user
      currentState.roles = payload.roles
      // Update local storage with user.
      window.localStorage.setItem( STORAGE_KEY, JSON.stringify( currentState ) )
    },
  },
}
