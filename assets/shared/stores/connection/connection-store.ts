import { ConnectionState } from '@adonis/shared/stores/connection/connection-state'
import { ActionContext, Commit, Dispatch } from 'vuex'

export default class ConnectionStore implements ActionContext<ConnectionState, any> {
  commit: Commit
  dispatch: Dispatch
  getters: any
  rootGetters: any
  rootState: any
  state: ConnectionState

}
