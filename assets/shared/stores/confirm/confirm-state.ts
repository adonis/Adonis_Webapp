import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'

export default class ConfirmState {
  confirmData: AsyncConfirmPopup
}
