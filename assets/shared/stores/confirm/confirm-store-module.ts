import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import ConfirmState from '@adonis/shared/stores/confirm/confirm-state'
import ConfirmStore from '@adonis/shared/stores/confirm/confirm-store'

const state: ConfirmState = new ConfirmState()
state.confirmData = null

export default {

  namespaced: true,

  state,

  getters: {

    getConfirm( currentState: ConfirmState ): AsyncConfirmPopup {
      return currentState.confirmData
    },
  },

  actions: {

    askConfirm( store: ConfirmStore, confirm: AsyncConfirmPopup ): void {
      if (!store.state.confirmData) {
        store.commit( 'setConfirm', confirm )
      }
    },

    setLockButton( store: ConfirmStore, buttonState: { index: number, disable: boolean } ): void {
      if (!!store.state.confirmData) {
        store.commit( 'setLock', buttonState )
      }
    },

    reinitialize( store: ConfirmStore ): void {
      if (!!store.state.confirmData) {
        store.commit( 'setConfirm', null )
      }
    },
  },

  mutations: {

    setConfirm( currentState: ConfirmState, confirm: AsyncConfirmPopup ): void {
      currentState.confirmData = confirm
    },

    setLock( currentState: ConfirmState, buttonState: { index: number, disable: boolean } ): void {
      currentState.confirmData.actions[buttonState.index].disabled = buttonState.disable
    },
  },
}
