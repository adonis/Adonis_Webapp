import ConfirmState from '@adonis/shared/stores/confirm/confirm-state'

export default class ConfirmStore {
  commit: ( ...args: any ) => void
  state: ConfirmState
}
