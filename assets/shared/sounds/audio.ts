export enum SoundEnum {
  i1555 = 'i1555',
  i1684 = 'i1684',
  i1796 = 'i1796',
  i1827 = 'i1827',
  i2278 = 'i2278',
}

export const availableSounds: any[] = [
  {
    label: 'dataEntry.parameter.cardOthers.sounds.i1',
    sound: SoundEnum.i1555,
  },
  {
    label: 'dataEntry.parameter.cardOthers.sounds.i2',
    sound: SoundEnum.i1684,
  },
  {
    label: 'dataEntry.parameter.cardOthers.sounds.i3',
    sound: SoundEnum.i1796,
  },
  {
    label: 'dataEntry.parameter.cardOthers.sounds.i4',
    sound: SoundEnum.i1827,
  },
  {
    label: 'dataEntry.parameter.cardOthers.sounds.i5',
    sound: SoundEnum.i2278,
  },
]
