// @ts-ignore
import library from '@adonis/shared/images/object-icon/Bibliotheque.gif'
// @ts-ignore
import block from '@adonis/shared/images/object-icon/Bloc.gif'
// @ts-ignore
import path from '@adonis/shared/images/object-icon/Cheminement.gif'
// @ts-ignore
import pathLibrary from '@adonis/shared/images/object-icon/Cheminements.gif'
// @ts-ignore
import userPath from '@adonis/shared/images/object-icon/CheminementUtilisateur.gif'
// @ts-ignore
import stateCode from '@adonis/shared/images/object-icon/CodeEtat.gif'
// @ts-ignore
import stateCodeLibrary from '@adonis/shared/images/object-icon/CodesEtats.gif'
// @ts-ignore
import experiment from '@adonis/shared/images/object-icon/Dispositif.gif'
// @ts-ignore
import disabledExperiment from '@adonis/shared/images/object-icon/DispositifDesactives.gif'
// @ts-ignore
import experimentLibrary from '@adonis/shared/images/object-icon/Dispositifs.gif'
// @ts-ignore
import disabledExperimentLibrary from '@adonis/shared/images/object-icon/DispositifsDesactives.gif'
// @ts-ignore
import validExperiment from '@adonis/shared/images/object-icon/DispositifValide.gif'
// @ts-ignore
import lockedExperiment from '@adonis/shared/images/object-icon/DispositifVerrouille.gif'
// @ts-ignore
import experimenter from '@adonis/shared/images/object-icon/Experimentateur.gif'
// @ts-ignore
import factor from '@adonis/shared/images/object-icon/Facteur.gif'
// @ts-ignore
import factorLibrary from '@adonis/shared/images/object-icon/Facteurs.gif'
// @ts-ignore
import group from '@adonis/shared/images/object-icon/Groupe.gif'
// @ts-ignore
import groupLibrary from '@adonis/shared/images/object-icon/Groupes.gif'
// @ts-ignore
import individual from '@adonis/shared/images/object-icon/Individu.gif'
// @ts-ignore
import valueList from '@adonis/shared/images/object-icon/ListeValeur.gif'
// @ts-ignore
import valueListLibrary from '@adonis/shared/images/object-icon/ListeValeurs.gif'
// @ts-ignore
import device from '@adonis/shared/images/object-icon/Materiel.gif'
// @ts-ignore
import materials from '@adonis/shared/images/object-icon/Materiels.gif'
// @ts-ignore
import annotation from '@adonis/shared/images/object-icon/Metadonnee.gif'
// @ts-ignore
import annotationLibrary from '@adonis/shared/images/object-icon/Metadonnees.gif'
// @ts-ignore
import modality from '@adonis/shared/images/object-icon/Modalite.gif'
// @ts-ignore
import oezKindLibrary from '@adonis/shared/images/object-icon/NaturesZHE.gif'
// @ts-ignore
import oezKind from '@adonis/shared/images/object-icon/NatureZHE.gif'
// @ts-ignore
import disabledObjectLibrary from '@adonis/shared/images/object-icon/ObjetsDesactives.gif'
// @ts-ignore
import param from '@adonis/shared/images/object-icon/Parametre.png'
// @ts-ignore
import unitPlot from '@adonis/shared/images/object-icon/Parcelle.gif'
// @ts-ignore
import platform from '@adonis/shared/images/object-icon/Plateforme.gif'
// @ts-ignore
import disabledPlatform from '@adonis/shared/images/object-icon/PlateformeDesactives.gif'
// @ts-ignore
import platformLibrary from '@adonis/shared/images/object-icon/Plateformes.gif'
// @ts-ignore
import disabledPlatformLibrary from '@adonis/shared/images/object-icon/PlateformesDesactivees.gif'
// @ts-ignore
import disabledProject from '@adonis/shared/images/object-icon/ProjetDeSaisieDesactive.gif'
// @ts-ignore
import project from '@adonis/shared/images/object-icon/ProjetSaisie.gif'
// @ts-ignore
import projectTransfer from '@adonis/shared/images/object-icon/ProjetSaisieTransferer.gif'
// @ts-ignore
import disabledProjectLibrary from '@adonis/shared/images/object-icon/ProjetsDeSaisieDesactives.gif'
// @ts-ignore
import projectLibrary from '@adonis/shared/images/object-icon/ProjetsDeSaisies.gif'
// @ts-ignore
import protocol from '@adonis/shared/images/object-icon/Protocole.gif'
// @ts-ignore
import disabledProtocol from '@adonis/shared/images/object-icon/ProtocoleDesactive.gif'
// @ts-ignore
import protocolLibrary from '@adonis/shared/images/object-icon/Protocoles.gif'
// @ts-ignore
import disabledProtocolLibrary from '@adonis/shared/images/object-icon/ProtocolesDesactives.gif'
// @ts-ignore
import dataEntry from '@adonis/shared/images/object-icon/Saisie.gif'
// @ts-ignore
import session from '@adonis/shared/images/object-icon/Session.gif'
// @ts-ignore
import site from '@adonis/shared/images/object-icon/Site.gif'
// @ts-ignore
import siteLibrary from '@adonis/shared/images/object-icon/Sites.gif'
// @ts-ignore
import subBlock from '@adonis/shared/images/object-icon/SousBloc.gif'
// @ts-ignore
import test from '@adonis/shared/images/object-icon/Test.gif'
// @ts-ignore
import treatment from '@adonis/shared/images/object-icon/Traitement.gif'
// @ts-ignore
import treatmentLibrary from '@adonis/shared/images/object-icon/Traitements.gif'
// @ts-ignore
import variable from '@adonis/shared/images/object-icon/Variable.gif'
// @ts-ignore
import generatorVariable from '@adonis/shared/images/object-icon/VariableGeneratrice.gif'
// @ts-ignore
import variableLibrary from '@adonis/shared/images/object-icon/Variables.gif'
// @ts-ignore
import semiAutomaticVariable from '@adonis/shared/images/object-icon/VariableSemiAuto.gif'
// @ts-ignore
import outExperimentationZone from '@adonis/shared/images/object-icon/ZoneHorsExperimentation.gif'

enum IconEnum {
  LIBRARY = library,
  BLOCK = block,
  PATH = path,
  STATE_CODE = stateCode,
  STATE_CODE_LIBRARY = stateCodeLibrary,
  EXPERIMENT = experiment,
  VALID_EXPERIMENT = validExperiment,
  LOCKED_EXPERIMENT = lockedExperiment,
  EXPERIMENT_LIBRARY = experimentLibrary,
  DISABLED_EXPERIMENT = disabledExperiment,
  DISABLED_EXPERIMENT_LIBRARY = disabledExperimentLibrary,
  FACTOR = factor,
  FACTOR_LIBRARY = factorLibrary,
  INDIVIDUAL = individual,
  ANNOTATION_LIBRARY = annotationLibrary,
  MODALITY = modality,
  UNIT_PLOT = unitPlot,
  PLATFORM = platform,
  PLATFORM_LIBRARY = platformLibrary,
  DISABLED_PLATFORM = disabledPlatform,
  DISABLED_PLATFORM_LIBRARY = disabledPlatformLibrary,
  PROJECT = project,
  PROJECT_TRANSFER = projectTransfer,
  PROJECT_LIBRARY = projectLibrary,
  DISABLED_PROJECT = disabledProject,
  DISABLED_PROJECT_LIBRARY = disabledProjectLibrary,
  PROTOCOL = protocol,
  PROTOCOL_LIBRARY = protocolLibrary,
  DISABLED_PROTOCOL = disabledProtocol,
  DISABLED_PROTOCOL_LIBRARY = disabledProtocolLibrary,
  SESSION = session,
  SUB_BLOCK = subBlock,
  TREATMENT = treatment,
  TREATMENT_LIBRARY = treatmentLibrary,
  VARIABLE = variable,
  VARIABLE_LIBRARY = variableLibrary,
  PATH_LIBRARY = pathLibrary,
  USER_PATH = userPath,
  EXPERIMENTER = experimenter,
  GROUP = group,
  GROUP_LIBRARY = groupLibrary,
  VALUE_LIST = valueList,
  VALUE_LIST_LIBRARY = valueListLibrary,
  ANNOTATION = annotation,
  OEZ_KIND_LIBRARY = oezKindLibrary,
  OEZ_KIND = oezKind,
  DISABLED_OBJECT_LIBRARY = disabledObjectLibrary,
  DATA_ENTRY = dataEntry,
  SITE = site,
  SITE_LIBRARY = siteLibrary,
  TEST = test,
  GENERATOR_VARIABLE = generatorVariable,
  SEMI_AUTOMATIC_VARIABLE = semiAutomaticVariable,
  DEVICE = device,
  DEVICE_LIBRARY = materials,
  PARAMETER = param,
}

export default IconEnum
