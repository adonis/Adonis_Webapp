enum VariableFormatEnum {
  FREE = 'free',
  CHARACTER_NUMBER = 'characterNumber',
  JJMMYYYY = 'jjmmyyyy',
  QUANTIEM = 'quantieme',
}

export default VariableFormatEnum
