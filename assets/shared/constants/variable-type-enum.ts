enum VariableTypeEnum {
  REAL = 'real',
  ALPHANUMERIC = 'alphanumeric',
  BOOLEAN = 'boolean',
  INTEGER = 'integer',
  DATE = 'date',
  HOUR = 'time',
}

export default VariableTypeEnum
