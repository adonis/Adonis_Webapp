enum ObjectActionEnum {

  CREATED = 'created',
  MODIFIED = 'modified',
  DELETED = 'deleted',
  FETCHED = 'fetched',
}

export default ObjectActionEnum
