enum PathLevelEnum {
  NONE = 'none',
  OEZ = 'oez',
  INDIVIDUAL = 'individual',
  UNIT_PLOT = 'unitPlot',
  SURFACE_UNIT_PLOT = 'surfacicUnitPlot',
  BLOCK = 'block',
  SUB_BLOCK = 'subBlock',
  EXPERIMENT = 'experiment',
  PLATFORM = 'platform',
}

export default PathLevelEnum
