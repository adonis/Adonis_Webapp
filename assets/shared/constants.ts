export const TYPE_DANGER = 'error'
export const TYPE_PRIMARY = 'primary'
export const TYPE_SECONDARY = 'secondary'
export const TYPE_INFO = 'info'

export const API_COMMUNICATION_FORMAT = 'application/ld+json'
export const SERVER_VERSION_URL = 'version'

export const ROLE_ADMIN = 'ROLE_ADMIN'
