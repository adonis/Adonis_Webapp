import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import en from 'vuetify/src/locale/en'
import fr from 'vuetify/src/locale/fr'
import { Iconfont } from 'vuetify/types/services/icons'

Vue.use( Vuetify )

const CONFIG = {
  lang: {
    current: 'fr',
    locales: { fr, en },
  },
  icons: {
    iconfont: 'md' as Iconfont,
  },
  theme: {
    themes: {
      light: {
        primary: '#678815',
        secondary: '#483C3C',
        tertiary: '#e7d4c5',
        error: '#e10000',
        success: '#678815',
        info: '#2196f3',
      },
    },
  },
}

export default new Vuetify( CONFIG )
