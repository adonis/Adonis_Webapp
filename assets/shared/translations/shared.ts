export default {
  en: {
    languages: {
      en: 'English',
      fr: 'Français',
    },
    connection: {
      login: 'Login',
      password: 'Password',
      connect: 'Log in',
      errors: {
        loginRequired: 'Login is required',
        loginInvalid: 'Login must be valid (Test)',
        passwordRequired: 'Password is required',
        passwordInvalid: 'Password must be valid (test)',
        401: 'Wrong credentials',
      },
    },
    disconnection: {
      confirm: {
        title: 'Logout',
        message1: 'You are about to logout',
        continueBtnLabel: 'Continue',
        cancelBtnLabel: 'Cancel',
      },
    },
    confirmDialog: {
      btnOpenText: 'Confirm',
      titleText: 'Confirm',
      cancelBtnLabel: 'Cancel',
      continueBtnLabel: 'Continue',
    },
    errors: {
      any: '',
    },
    alert: {
      failure: {
        accessDenied: 'Access denied',
        created: 'An error occurred during creation',
        modified: 'An error occurred during modification',
        deleted: 'An error occurred during deletion',
        fetched: 'An error occurred during fetch',
      },
    },
    adonisSelect: {
      noData: 'No data available',
    },
    newVersion: {
      title: 'Version {version}',
      message1: 'A new version {version} is available. The application will be updated.',
      continueBtnLabel: 'Continue',
    },
  },
  fr: {
    languages: {
      en: 'English',
      fr: 'Français',
    },
    connection: {
      login: 'Identifiant',
      password: 'Mot de passe',
      connect: 'Se connecter',
      errors: {
        loginRequired: 'Un identifiant est requis',
        loginInvalid: 'L\'identifiant doit être valide (Test)',
        passwordRequired: 'Un mot de passe est requis',
        passwordInvalid: 'Le mot de passe doit être valide (test)',
        401: 'Les informations transmises n\'ont pas permis de vous identifier',
      },
    },
    disconnection: {
      confirm: {
        title: 'Déconnexion',
        message1: 'Vous êtes sur le point d\'être déconnecté',
        continueBtnLabel: 'Continuer',
        cancelBtnLabel: 'Annuler',
      },
    },
    confirmDialog: {
      btnOpenText: 'Confirmer',
      titleText: 'Confirmer',
      cancelBtnLabel: 'Annuler',
      continueBtnLabel: 'Continuer',
    },
    errors: {
      any: '',
    },
    alert: {
      failure: {
        accessDenied: 'Accès non autorisé',
        created: 'Une erreur est survenue durant la création',
        modified: 'Une erreur est survenue durant la modification',
        deleted: 'Une erreur est survenue durant la suppression',
        fetched: 'Une erreur est survenue durant la récupération des données',
      },
    },
    adonisSelect: {
      noData: 'Aucun résultat',
    },
    newVersion: {
      title: 'Version {version}',
      message1: 'Une nouvelle version {version} est disponible et va être installée.',
      continueBtnLabel: 'Continuer',
    },
  },
}
