export const BASE_URL = '/'
export const API_URL = '/api'
export const APP_URL = '/app'
export const WEBAPP_URL = '/webapp'
export const VERSION = process.env.VERSION
