import ApiEntity from '@adonis/shared/models/api-entity'
import Site from '@adonis/shared/models/site'
import User from '@adonis/shared/models/user'
import SiteRoleEnum from '@adonis/webapp/constants/site-role-enum'

export default class RelUserSite implements ApiEntity {

    private _site: Promise<Site>
    private _user: Promise<User>

    constructor(
        public iri: string,
        private _siteIri: string,
        private siteCallback: () => Promise<Site>,
        private _userIri: string,
        private userCallback: () => Promise<User>,
        public role: SiteRoleEnum,
    ) {

    }

    get siteIri(): string {
        return this._siteIri
    }

    get site() {
        return this._site = this._site || this.siteCallback()
    }

    get userIri(): string {
        return this._userIri
    }

    get user() {
        return this._user = this._user || this.userCallback()
    }
}
