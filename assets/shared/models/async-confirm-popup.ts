import { ConfirmAction } from '@adonis/shared/models/confirm-action'

/**
 * Class AsyncConfirmPopup: store data for popup confirm before action.
 */
export default class AsyncConfirmPopup {

  constructor(
      public title: string,
      public messages: string[] = [],
      public actions: ConfirmAction[] = [],
      public portalName: string = null,
  ) {
  }
}
