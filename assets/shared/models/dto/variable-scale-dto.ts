import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export interface VariableScaleValueDto {
  text: string,
  value: number,
  pic: string,
}

export type VariableScaleDto = AbstractDtoObject & {
  name?: string,
  minValue?: number,
  maxValue?: number,
  open?: boolean,
  variable?: string,
  values?: VariableScaleValueDto[],
}
