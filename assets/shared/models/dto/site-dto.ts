import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'

export type SiteDto = AbstractDtoObject & {
    label?: string,
    userRoles?: string[] | RelUserSiteDto[],
    platforms?: string[],
    projects?: string[],
}
