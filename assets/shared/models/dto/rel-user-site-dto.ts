import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import SiteRoleEnum from '@adonis/webapp/constants/site-role-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type RelUserSiteDto = AbstractDtoObject & HasSiteDto & {
    user?: string,
    role?: SiteRoleEnum,
}
