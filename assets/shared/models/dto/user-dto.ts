import RoleEnum from '@adonis/shared/constants/role-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type UserDto = AbstractDtoObject & {

    username?: string,
    name?: string,
    surname?: string,
    email?: string,
    roles?: RoleEnum[],
    siteRoles?: string[],
    active?: boolean,
    avatar?: string,
    renewPassword?: true,
}
