import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ValueListDto } from '@adonis/shared/models/dto/value-list-dto'
import { VariableScaleDto } from '@adonis/shared/models/dto/variable-scale-dto'

export type SimpleVariableDto = AbstractDtoObject & {
  name?: string,
  shortName?: string,
  repetitions?: number,
  unit?: string,
  pathLevel?: PathLevelEnum,
  comment?: string,
  order?: number,
  format?: VariableFormatEnum,
  formatLength?: number,
  defaultTrueValue?: boolean,
  type?: VariableTypeEnum,
  mandatory?: boolean,
  identifier?: string,
  created?: Date,
  project?: string,
  scale?: string | VariableScaleDto,
  valueList?: string | ValueListDto,
  site?: string,
}
