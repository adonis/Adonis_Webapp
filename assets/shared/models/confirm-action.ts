/**
 * Class ConfirmAction: store data about a single action inside AsyncConfirmPopup.
 */
export class ConfirmAction {

  constructor(
      public label: string,
      public type = 'secondary',
      public action: ( ...args: any ) => any = () => {
      },
      public disabled = false,
  ) {
  }
}