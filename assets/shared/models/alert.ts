/**
 * Alert class.
 */
export default class Alert {
  constructor(
      public open = false,
      public type = 'error',
      public text = '-',
      public duration = 5,
  ) {
  }
}
