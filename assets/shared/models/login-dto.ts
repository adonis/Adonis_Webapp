import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type LoginDto = AbstractDtoObject & {
  token: string,
  refresh_token: string,
}
