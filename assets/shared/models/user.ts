import RoleEnum from '@adonis/shared/constants/role-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import RelUserSite from '@adonis/shared/models/rel-user-site'

export default class User implements ApiEntity {

    private _siteRoles: Promise<RelUserSite>[]

    constructor(
        public iri: string,
        public id: number,
        public username: string,
        public name: string,
        public surname: string,
        public email: string,
        private siteRolesIris: string[],
        private siteRolesCallback: () => Promise<RelUserSite>[],
        public active: boolean,
        public roles: RoleEnum[],
        public avatar: string,
    ) {
    }

    get siteRoles(): Promise<RelUserSite>[] {
        return this._siteRoles = this._siteRoles || this.siteRolesCallback()
    }

    get siteRolesIriTab(): string[] {
        return this.siteRolesIris
    }
}
