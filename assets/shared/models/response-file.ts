export default class ResponseFile {
  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public status: string,
      public contentUrl: string,
      public uploadDate: Date,
  ) {
  }
}
