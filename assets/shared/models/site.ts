import ApiEntity from '@adonis/shared/models/api-entity'
import RelUserSite from '@adonis/shared/models/rel-user-site'

export default class Site implements ApiEntity {

    private _userRoles: Promise<RelUserSite>[]

    constructor(
        public iri: string,
        public id: number,
        public label: string,
        private _userRolesIris: string[],
        private userRolesCallback: () => Promise<RelUserSite>[],
        private _platformIris: string[],
    ) {

    }

    get userRoles(): Promise<RelUserSite>[] {
        return this._userRoles = this._userRoles || this.userRolesCallback()
    }

    get userRolesIriTab(): string[] {
        return this._userRolesIris
    }

    get platformIris(): string[] {
        return this._platformIris
    }
}
