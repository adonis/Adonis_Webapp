import {RelUserSiteDto} from '@adonis/shared/models/dto/rel-user-site-dto'
import {SiteDto} from '@adonis/shared/models/dto/site-dto'
import {UserDto} from '@adonis/shared/models/dto/user-dto'
import RelUserSite from '@adonis/shared/models/rel-user-site'
import AbstractTransformer from '@adonis/shared/services/transformers/abstract-transformer'
import SiteTransformer from '@adonis/shared/services/transformers/site-transformer'
import UserTransformer from '@adonis/shared/services/transformers/user-transformer'
import RoleUserSiteFormInterface from '@adonis/webapp/form-interfaces/role-user-site-form-interface'

export default class RelUserSiteTransformer extends AbstractTransformer<RelUserSiteDto, RelUserSite> {

  private _siteTransformer: SiteTransformer
  private _userTransformer: UserTransformer

  public dtoToObject(from: RelUserSiteDto): RelUserSite {
    return new RelUserSite(
      from['@id'],
      from.site,
      () => this.httpService.get<SiteDto>(from.site)
        .then(response => this._siteTransformer.dtoToObject(response.data)),
      from.user,
      () => this.httpService.get<UserDto>(from.user)
        .then(response => this._userTransformer.dtoToObject(response.data)),
      from.role,
    )
  }

  public objectToFormInterface(from: RelUserSite): Promise<RoleUserSiteFormInterface> {
    return undefined
  }

  public formInterfaceToDto(from: RoleUserSiteFormInterface): RelUserSiteDto {
    return {
      site: from.site.iri,
      user: from.user.iri,
      role: from.role,
    }
  }

  public formInterfaceToUpdateDto(from: RoleUserSiteFormInterface): RelUserSiteDto {
    return {
      role: from.role,
    }
  }

  public set siteTransformer(value: SiteTransformer) {
    this._siteTransformer = value
  }

  public set userTransformer(value: UserTransformer) {
    this._userTransformer = value
  }
}
