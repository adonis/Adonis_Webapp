import ApiEntity from '@adonis/shared/models/api-entity'
import {AbstractDtoObject} from '@adonis/shared/models/dto-types'
import HttpService from '@adonis/shared/services/http-service'

export default abstract class AbstractTransformer<S extends AbstractDtoObject, T extends ApiEntity> {

  constructor(protected httpService: HttpService) {
  }

  public abstract dtoToObject(from: S): T
}
