import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import {RelUserSiteDto} from '@adonis/shared/models/dto/rel-user-site-dto'
import {UserDto} from '@adonis/shared/models/dto/user-dto'
import User from '@adonis/shared/models/user'
import AbstractTransformer from '@adonis/shared/services/transformers/abstract-transformer'
import RelUserSiteTransformer from '@adonis/shared/services/transformers/rel-user-site-transformer'
import UserFormInterface from '@adonis/webapp/form-interfaces/user-form-interface'

export default class UserTransformer extends AbstractTransformer<UserDto, User> {

  private _relUserSiteTransformer: RelUserSiteTransformer

  public dtoToObject(from: UserDto): User {
    return new User(
      from['@id'],
      from.id,
      from.username,
      from.name,
      from.surname,
      from.email,
      from.siteRoles,
      () => {
        const promise = this.httpService.getAll<RelUserSiteDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.ROLE_USER_SITE),
          {pagination: false, user: from['@id']})
        return from.siteRoles.map((uri, index) => {
          return promise
            .then(response => this._relUserSiteTransformer.dtoToObject(response.data['hydra:member'][index]))
        })
      },
      from.active,
      from.roles,
      from.avatar,
    )
  }

  public objectToFormInterface(from: User): Promise<UserFormInterface> {
    return undefined
  }

  public formInterfaceToDto(from: UserFormInterface): UserDto {
    return {
      name: from.name,
      email: from.email,
      username: from.login,
      surname: from.surname,
    }
  }

  public set relUserSiteTransformer(value: RelUserSiteTransformer) {
    this._relUserSiteTransformer = value
  }
}
