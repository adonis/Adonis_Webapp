import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import {RelUserSiteDto} from '@adonis/shared/models/dto/rel-user-site-dto'
import {SiteDto} from '@adonis/shared/models/dto/site-dto'
import Site from '@adonis/shared/models/site'
import AbstractTransformer from '@adonis/shared/services/transformers/abstract-transformer'
import RelUserSiteTransformer from '@adonis/shared/services/transformers/rel-user-site-transformer'
import SiteFormInterface from '@adonis/webapp/form-interfaces/site-form-interface'

export default class SiteTransformer extends AbstractTransformer<SiteDto, Site> {

  private _relUserSiteTransformer: RelUserSiteTransformer

  public dtoToObject(from: SiteDto): Site {
    return new Site(
      from['@id'],
      from.id,
      from.label,
      from.userRoles as string[],
      () => {
        const promise = this.httpService.getAll<RelUserSiteDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.ROLE_USER_SITE),
          {pagination: false, site: from['@id']})
        return (from.userRoles as string[]).map((uri, index) => {
          return promise
            .then(response => this._relUserSiteTransformer.dtoToObject(response.data['hydra:member'][index]))
        })
      },
      from.platforms,
    )
  }

  public objectToFormInterface(from: Site): Promise<SiteFormInterface> {
    return undefined
  }

  public formInterfaceToDto(from: SiteFormInterface): SiteDto {
    return {
      label: from.name,
    }
  }

  public set relUserSiteTransformer(value: RelUserSiteTransformer) {
    this._relUserSiteTransformer = value
  }
}
