import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { AxiosError } from 'axios'

export default interface NotificationServiceInterface {

    objectSuccess(object: ObjectTypeEnum, action: ObjectActionEnum): void

    defaultObjectFailure(action: ObjectActionEnum, reason?: AxiosError): void

    accessDenied(): void
}
