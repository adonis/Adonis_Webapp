import HttpService from '@adonis/shared/services/http-service'
import RelUserSiteTransformer from '@adonis/shared/services/transformers/rel-user-site-transformer'
import SiteTransformer from '@adonis/shared/services/transformers/site-transformer'
import UserTransformer from '@adonis/shared/services/transformers/user-transformer'

export default class TransformerService {

    public siteTransformer: SiteTransformer
    public userTransformer: UserTransformer
    public relUserSiteTransformer: RelUserSiteTransformer

    constructor(
        private httpService: HttpService,
    ) {
        this.siteTransformer = new SiteTransformer(httpService)
        this.userTransformer = new UserTransformer(httpService)
        this.relUserSiteTransformer = new RelUserSiteTransformer(httpService)

        this.siteTransformer.relUserSiteTransformer = this.relUserSiteTransformer
        this.userTransformer.relUserSiteTransformer = this.relUserSiteTransformer
        this.relUserSiteTransformer.siteTransformer = this.siteTransformer
        this.relUserSiteTransformer.userTransformer = this.userTransformer
    }

}
