import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { ConfirmAction } from '@adonis/shared/models/confirm-action'

export default class ConnectionService {

  static askConfirmBeforeLogout( vue: Vue, logountAction: () => void ): void {
    const confirmRequest = new AsyncConfirmPopup(
        vue.$t( 'shared.disconnection.confirm.title' ).toString(),
        [
          vue.$t( 'shared.disconnection.confirm.message1' ).toString(),
        ],
        [
          new ConfirmAction(
              vue.$t( 'shared.disconnection.confirm.continueBtnLabel' ).toString(),
              'error',
              logountAction,
          ),
          new ConfirmAction(
              vue.$t( 'shared.disconnection.confirm.cancelBtnLabel' ).toString(),
          ),
        ],
    )
    vue.$store.dispatch( 'confirm/askConfirm', confirmRequest ).then()
  }
}
