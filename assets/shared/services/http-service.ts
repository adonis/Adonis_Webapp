import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { BASE_URL } from '@adonis/shared/env'
import Credentials from '@adonis/shared/models/credentials'
import { AbstractDtoCollection, AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { LoginDto } from '@adonis/shared/models/login-dto'
import NotificationServiceInterface from '@adonis/shared/services/notification-service-interface'
import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import contentDisposition from 'content-disposition'
import objectHash from 'object-hash'
import { Store } from 'vuex'

export default abstract class HttpService {

    private axiosInstance: AxiosInstance

    private requests: Map<string, Promise<any>>

    protected constructor(
        private store: Store<any>,
        private notifier: NotificationServiceInterface,
    ) {
        const qs = require('qs')
        this.axiosInstance = axios.create({
            baseURL: BASE_URL,
            paramsSerializer: params => qs.stringify(params),
        })
        this.requests = new Map()
    }

    public post<S extends AbstractDtoObject>(iri: string, subject: any): Promise<AxiosResponse<S>> {

        const attempt = () => this.axiosInstance.post<S>(iri, subject, this.getConfig())
        return attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.CREATED)
                return Promise.reject(reason)
            })
    }

    public patch<S extends AbstractDtoObject>(iri: string, changes: any, params = {}): Promise<AxiosResponse<S>> {

        const config = this.getConfig()
        config.headers['Content-Type'] = 'application/merge-patch+json'
        const attempt = () => this.axiosInstance.patch<S>(iri, changes, {...config, params})
        return attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.MODIFIED)
                return Promise.reject(reason)
            })
    }

    public put<S extends AbstractDtoObject>(iri: string, changes: any): Promise<AxiosResponse<S>> {

        const config = this.getConfig()
        const attempt = () => this.axiosInstance.put<S>(iri, changes, config)
        return attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.MODIFIED)
                return Promise.reject(reason)
            })
    }

    public get<S extends AbstractDtoObject>(iri: string, params = {}): Promise<AxiosResponse<S>> {
        const hash = iri + objectHash(params)
        if (this.requests.has(hash)) {
            return this.requests.get(hash)
        }
        const attempt = () => this.axiosInstance.get<S>(iri, {...this.getConfig(), params})
        this.requests.set(hash, attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.FETCHED)
                return Promise.reject(reason)
            })
            .finally(() => this.requests.delete(hash)))
        return this.requests.get(hash)
    }

    public getFile(url: string, type: string, navigationId?: string): Promise<any> {

        const explorer = this.store.getters['navigation/getExplorerItem'](navigationId)
        if (!!explorer) {
            explorer.loading = true
        }

        const baseConfig = this.getConfig()
        const config: AxiosRequestConfig = {
            ...baseConfig,
            headers: {...baseConfig.headers, 'Accept': type},
            responseType: 'blob',
        }
        const attempt = () => this.axiosInstance.get(url, config)
            .then(response => {
                const xmlZipFile = new Blob([response.data], {type})
                const filename = contentDisposition.parse(response.headers['content-disposition']).parameters.filename
                if (navigator.msSaveBlob) {
                    window.navigator.msSaveBlob(xmlZipFile, filename)
                } else {
                    const link = document.createElement('a')
                    link.href = window.URL.createObjectURL(xmlZipFile)
                    link.setAttribute('download', filename)
                    document.body.appendChild(link)
                    link.click()
                    document.body.removeChild(link)
                }
                if (!!explorer) {
                    explorer.loading = false
                }
            })
        return attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.FETCHED)
                if (!!explorer) {
                    explorer.loading = false
                }
                return Promise.reject(reason)
            })
    }

    public getAll<S extends AbstractDtoObject>(iri: string, params = {}): Promise<AxiosResponse<AbstractDtoCollection<S>>> {

        const attempt = () => this.axiosInstance.get<AbstractDtoCollection<S>>(iri, {...this.getConfig(), params})
        return attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.FETCHED)
                return Promise.reject(reason)
            })
    }

    public delete(iri: string, params = {}): Promise<AxiosResponse> {

        const attempt = () => this.axiosInstance.delete(iri, {...this.getConfig(), params})
        return attempt()
            .catch((reason: AxiosError) => this.retry(reason, attempt))
            .catch((reason: AxiosError) => {
                this.notifier.defaultObjectFailure(ObjectActionEnum.DELETED, reason)
                return Promise.reject(reason)
            })
    }

    public connect(credentials: Credentials): Promise<AxiosResponse<LoginDto>> {

        return this.axiosInstance.post<LoginDto>('authentication/login', credentials)
    }

    public refresh(refreshToken: string): Promise<AxiosResponse<LoginDto>> {

        return this.axiosInstance.post<LoginDto>('authentication/refresh', {refresh_token: refreshToken})
    }

    private getConfig(): AxiosRequestConfig {

        const headers = {'Authorization': this.store.getters['connection/authenticationHeader']}
        return {headers}
    }

    private retry<S>(error: AxiosError, attempt: () => Promise<S>): Promise<S> {

        if (401 === error.response.status) {
            return this.store.dispatch('connection/refresh')
                .then(() => attempt())
                .catch(() => Promise.reject(error))
        }
        return Promise.reject(error)
    }

    public getEndpointFromType(type: ObjectTypeEnum): string {
        switch (type) {
            case ObjectTypeEnum.USER:
                return 'api/users'
            case ObjectTypeEnum.SITE:
                return 'api/sites'
            case ObjectTypeEnum.ROLE_USER_SITE:
                return 'api/rel_user_sites'
            case ObjectTypeEnum.FACTOR:
                return 'api/factors'
            case ObjectTypeEnum.MODALITY:
                return 'api/modalities'
            case ObjectTypeEnum.PROTOCOL:
                return 'api/protocols'
            case ObjectTypeEnum.TREATMENT:
                return 'api/treatments'
            case ObjectTypeEnum.USER_GROUP:
                return 'api/user_groups'
            case ObjectTypeEnum.ADVANCED_RIGHT_USER:
                return 'api/advanced_user_rights'
            case ObjectTypeEnum.ADVANCED_RIGHT_GROUP:
                return 'api/advanced_group_rights'
            case ObjectTypeEnum.EXPERIMENT:
                return 'api/experiments'
            case ObjectTypeEnum.PLATFORM:
                return 'api/platforms'
            case ObjectTypeEnum.SIMPLE_VARIABLE:
                return 'api/simple_variables'
            case ObjectTypeEnum.VALUE_LIST:
                return 'api/value_lists'
            case ObjectTypeEnum.GENERATOR_VARIABLE:
                return 'api/generator_variables'
            case ObjectTypeEnum.STATE_CODE:
                return 'api/state_codes'
            case ObjectTypeEnum.DEVICE:
                return 'api/devices'
            case ObjectTypeEnum.DATA_ENTRY_PROJECT:
                return 'api/projects'
            case ObjectTypeEnum.STATUS_DATA_ENTRY:
                return '/api/status_data_entries'
            case ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE:
                return '/api/semi_automatic_variables'
            case ObjectTypeEnum.TEST:
                return '/api/tests'
            case ObjectTypeEnum.PATH_BASE:
                return 'api/path_bases'
            case ObjectTypeEnum.USER_PATH:
                return 'api/path_user_workflows'
            case ObjectTypeEnum.VARIABLE_SCALE:
                return 'api/variable_scales'
            case ObjectTypeEnum.BLOCK:
                return 'api/blocks'
            case ObjectTypeEnum.SUB_BLOCK:
                return 'api/sub_blocks'
            case ObjectTypeEnum.SURFACIC_UNIT_PLOT:
                return 'api/surfacic_unit_plots'
            case ObjectTypeEnum.UNIT_PLOT:
                return 'api/unit_plots'
            case ObjectTypeEnum.INDIVIDUAL:
                return 'api/individuals'
            case ObjectTypeEnum.OEZ:
                return 'api/out_experimentation_zones'
            case ObjectTypeEnum.GRAPHICAL_CONFIGURATION:
                return 'api/graphical_configurations'
            case ObjectTypeEnum.EDIT:
                return 'api/edits'
            case ObjectTypeEnum.DELETE:
                return 'api/deletes'
            case ObjectTypeEnum.CREATE:
                return 'api/creates'
            case ObjectTypeEnum.COPY:
                return 'api/copies'
            case ObjectTypeEnum.RESTORE:
                return 'api/restores'
            case ObjectTypeEnum.PLACE_EXPERIMENT:
                return 'api/place_experiments'
            case ObjectTypeEnum.REQUIRED_ANNOTATION:
                return 'api/required_annotations'
            case ObjectTypeEnum.OEZ_NATURE:
                return 'api/oez_natures'
            case ObjectTypeEnum.NOTE:
                return 'api/notes'
            case ObjectTypeEnum.PARSING_JOB:
                return 'api/parsing_jobs'
            case ObjectTypeEnum.ALGORITHM:
                return 'api/algorithms'
            case ObjectTypeEnum.DATA_ENTRY:
                return 'api/project_datas'
            case ObjectTypeEnum.VARIABLE_CONNECTION:
                return 'api/variable_connections'
            case ObjectTypeEnum.REQUEST_FILE:
                return 'api/request_files'
            case ObjectTypeEnum.RESPONSE_FILE:
                return 'api/response_files'
            case ObjectTypeEnum.DATA_ENTRY_FUSION:
                return 'api/data_entry_fusions'
            case ObjectTypeEnum.CLONE_PROJECT:
                return 'api/clone_projects'
            case ObjectTypeEnum.FIELD_MEASURE:
                return 'api/field_measures'
            case ObjectTypeEnum.CLONE_SITE:
                return 'api/clone_sites'
            case ObjectTypeEnum.FILE:
                return 'api/files'
            case ObjectTypeEnum.EXPERIMENT_ATTACHMENT:
                return 'api/experiment_attachments'
            case ObjectTypeEnum.PROTOCOL_ATTACHMENT:
                return 'api/protocol_attachments'
            case ObjectTypeEnum.PLATFORM_ATTACHMENT:
                return 'api/platform_attachments'
            case ObjectTypeEnum.ANNOTATION:
                return 'api/annotations'
            case ObjectTypeEnum.NORTH_INDICATOR:
                return 'api/north_indicators'
            case ObjectTypeEnum.GRAPHICAL_TEXT_ZONE:
                return 'api/graphical_text_zones'
            case ObjectTypeEnum.PARAMETERS:
                return 'api/parameters'
            case ObjectTypeEnum.ENV:
                return 'api/envs'
            case ObjectTypeEnum.CLONE_VARIABLE:
                return 'api/clone_variables'
            case ObjectTypeEnum.SESSION:
                return 'api/sessions'
            case ObjectTypeEnum.CLONE_EXPERIMENT:
                return 'api/clone_experiments'
            case ObjectTypeEnum.IDENTIFICATION_CODE_UPDATE:
                return 'api/identification_code_updates'
            case ObjectTypeEnum.OPEN_SILEX_INSTANCE:
                return 'api/open_silex_instances'
            case ObjectTypeEnum.DATA_VIEW_ITEM:
                return 'api/data_view_items'
            case ObjectTypeEnum.DATA_VIEW_BUSINESS_OBJECT:
                return 'api/data_view_business_objects'
        }
        return undefined
    }
}
