import ApiEntity from '@adonis/shared/models/api-entity'
import DeviceCommunicationProtocolEnum from '@adonis/webapp/constants/device-communication-protocol-enum'
import Rs232BitFormat from '@adonis/webapp/constants/rs232-bit-format'
import Rs232FlowControl from '@adonis/webapp/constants/rs232-flow-control'
import Rs232Parity from '@adonis/webapp/constants/rs232-parity'
import Rs232StopBit from '@adonis/webapp/constants/rs232-stop-bit'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'

export default class Device implements ApiEntity, PropertyViewable {

  private _managedVariables: Promise<SemiAutomaticVariable>[]

  constructor(
      public iri: string,
      public id: number,
      public alias: string,
      public name: string,
      public manufacturer: string,
      public type: string,
      private _managedVariablesIriTab: string[],
      private managedVariablesCallback: () => Promise<SemiAutomaticVariable>[],
      public communicationProtocol: DeviceCommunicationProtocolEnum,
      public frameLength: number,
      public frameStart: string,
      public frameEnd: string,
      public frameCsv: string,
      public baudrate: number,
      public bitFormat: Rs232BitFormat,
      public flowControl: Rs232FlowControl,
      public parity: Rs232Parity,
      public stopBit: Rs232StopBit,
      public remoteControl: boolean,
      public openSilexUri: string,
  ) {

  }

  get managedVariables(): Promise<SemiAutomaticVariable>[] {
    return this._managedVariables = this._managedVariables || this.managedVariablesCallback()
  }

  get managedVariablesIriTab(): string[] {
    return this._managedVariablesIriTab
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all([
      ...['alias', 'name', 'manufacturer', 'type'].map( ( prop: keyof Device ) => ({
        propertyValue: this[prop],
        propertyName: 'navigation.propertyView.device.' + prop,
      }) ),
      {
        propertyName: 'navigation.propertyView.device.managedVariablesNumber',
        propertyValue: this._managedVariablesIriTab.length,
      },
      {
        propertyName: 'navigation.propertyView.device.communicationProtocol',
        propertyValue: 'enums.device.communications',
        propertyValueArguments: { communication: this.communicationProtocol },
      },
      ...['frameLength', 'frameStart', 'frameEnd', 'frameCsv'].map( ( prop: keyof Device ) => ({
        propertyValue: this[prop],
        propertyName: 'navigation.propertyView.device.' + prop,
      }) ),
      ...(
          this.communicationProtocol === DeviceCommunicationProtocolEnum.RS232 ?
              [
                ...['baudrate', 'bitFormat', 'stopBit', 'remoteControl' ].map( ( prop: keyof Device ) => ({
                  propertyValue: this[prop],
                  propertyName: 'navigation.propertyView.device.' + prop,
                }) ),
                {
                  propertyName: 'navigation.propertyView.device.flowControl',
                  propertyValue: 'enums.device.rs232.flowControls',
                  propertyValueArguments: { flowControl: this.flowControl },
                },
                {
                  propertyName: 'navigation.propertyView.device.parity',
                  propertyValue: 'enums.device.rs232.parities',
                  propertyValueArguments: { parity: this.parity },
                },
              ] : []
      ),
      {
        propertyValue: this.openSilexUri,
        propertyName: 'navigation.propertyView.device.openSilexUri',
      },
    ])
  }
}
