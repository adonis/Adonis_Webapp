import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import AdvancedRightEnum from '@adonis/webapp/constants/advanced-right-enum'

export default class AdvancedGroupRight {

  constructor(
      public iri: string,
      public groupId: number,
      public objectId: number,
      public classIdentifier: AdvancedRightClassIdentifierEnum,
      public right: AdvancedRightEnum,
  ) {
  }
}
