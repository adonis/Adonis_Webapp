import ApiEntity from '@adonis/shared/models/api-entity'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Project from '@adonis/webapp/models/project'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import Session from '@adonis/webapp/models/session'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default class ProjectData implements ApiEntity, PropertyViewable {

  private _sessions: Promise<Session>[]
  private _project: Promise<Project>
  private _simpleVariables: Promise<SimpleVariable>[]
  private _generatorVariables: Promise<GeneratorVariable>[]
  private _semiAutomaticVariables: Promise<SemiAutomaticVariable>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public start: Date,
      public end: Date,
      public userName: string,
      public comment: string,
      public fusion: boolean,
      private _sessionIriTab: string[],
      private sessionsCallback: () => Promise<Session>[],
      private _projectIri: string,
      private projectCallback: () => Promise<Project>,
      private _simpleVariableIriTab: string[],
      private simpleVariablesCallback: () => Promise<SimpleVariable>[],
      private _generatorVariableIriTab: string[],
      private generatorVariablesCallback: () => Promise<GeneratorVariable>[],
      private _semiAutomaticVariablesIriTab: string[],
      private semiAutomaticVariablesCallback: () => Promise<SemiAutomaticVariable>[],
  ) {
  }

  get sessions(): Promise<Session>[] {
    return this._sessions = this._sessions || this.sessionsCallback()
  }

  get project(): Promise<Project> {
    return this._project = this._project || this.projectCallback()
  }

  get simpleVariables(): Promise<SimpleVariable>[] {
    return this._simpleVariables = this._simpleVariables || this.simpleVariablesCallback()
  }

  get simpleVariablesIri(): string[] {
    return this._simpleVariableIriTab
  }

  get generatorVariables(): Promise<GeneratorVariable>[] {
    return this._generatorVariables = this._generatorVariables || this.generatorVariablesCallback()
  }

  get generatorVariablesIri(): string[] {
    return this._generatorVariableIriTab
  }

  get semiAutomaticVariables(): Promise<SemiAutomaticVariable>[] {
    return this._semiAutomaticVariables = this._semiAutomaticVariables || this.semiAutomaticVariablesCallback()
  }

  get semiAutomaticVariablesIri(): string[] {
    return this._semiAutomaticVariablesIriTab
  }

  get variables(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>[] {
    return [...this.semiAutomaticVariables, ...this.generatorVariables, ...this.simpleVariables]
  }

  get variablesIri(): string[] {
    return [...this.semiAutomaticVariablesIri, ...this.generatorVariablesIri, ...this.simpleVariablesIri]
  }

  get sessionsIri(): string[] {
    return this._sessionIriTab
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.projectData.name',
        patchDtoProperty: 'name',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.projectData.comment',
        patchDtoProperty: 'comment',
      },
      {
        propertyValue: this.userName,
        propertyName: 'navigation.propertyView.projectData.userName',
      },
      {
        propertyValue: this.start,
        propertyName: 'navigation.propertyView.projectData.start',
      },
      {
        propertyValue: this.end,
        propertyName: 'navigation.propertyView.projectData.end',
      },
    ] )
  }
}
