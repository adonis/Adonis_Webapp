import ApiEntity from '@adonis/shared/models/api-entity'

export default class NorthIndicator implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public orientation: number,
      public x: number,
      public y: number,
      public width: number,
      public height: number,
  ) {
  }
}
