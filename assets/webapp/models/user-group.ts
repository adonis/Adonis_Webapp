import Site from '@adonis/shared/models/site'
import User from '@adonis/shared/models/user'

export default class UserGroup {

    private _users: Promise<User>[]
    private _site: Promise<Site>

    constructor(
        public iri: string,
        public name: string,
        private _usersIris: string[],
        private usersCallback: () => Promise<User>[],
        private _siteIri: string,
        private siteCallback: () => Promise<Site>,
    ) {
    }

    get users(): Promise<User>[] {
        return this._users = this._users || this.usersCallback()
    }

    get usersIriTab(): string[] {
        return this._usersIris
    }

    get site(): Promise<Site> {
        return this._site = this._site || this.siteCallback()
    }

    get siteIri(): string {
        return this._siteIri
    }
}
