import ApiEntity from '@adonis/shared/models/api-entity'

export default class VariableScale implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public minValue: number,
      public maxValue: number,
      public open: boolean,
      public values: {
        text: string,
        value: number,
        pic: string,
      }[],
  ) {

  }

}
