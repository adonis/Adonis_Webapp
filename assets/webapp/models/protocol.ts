import ApiEntity from '@adonis/shared/models/api-entity'
import User from '@adonis/shared/models/user'
import Algorithm from '@adonis/webapp/models/algorithm'
import Attachment from '@adonis/webapp/models/attachment'
import Factor from '@adonis/webapp/models/factor'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Treatment from '@adonis/webapp/models/treatment'

export default class Protocol implements ApiEntity, PropertyViewable {

    private _factors: Promise<Factor>[]
    private _treatments: Promise<Treatment>[]
    private _owner: Promise<User>
    private _algorithm: Promise<Algorithm>
    private _attachments: Promise<Attachment>[]

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        public aim: string,
        private _factorIris: string[],
        private factorCallback: () => Promise<Factor>[],
        private _treatmentIris: string[],
        private treatmentCallback: () => Promise<Treatment>[],
        public comment: string,
        public created: Date,
        private _ownerIri: string,
        private ownerCallback: () => Promise<User>,
        private _algorithmIri: string,
        public algorithmCallback: () => Promise<Algorithm>,
        private _attachmentsIris: string[],
        private attachmentsCallback: () => Promise<Attachment>[],
    ) {

    }

    get factors(): Promise<Factor>[] {
        return this._factors = this._factors || this.factorCallback()
    }

    get factorIriTab(): string[] {
        return this._factorIris
    }

    get treatments(): Promise<Treatment>[] {
        return this._treatments = this._treatments || this.treatmentCallback()
    }

    get treatmentIriTab(): string[] {
        return this._treatmentIris
    }

    get owner(): Promise<User> {
        return this._owner = this._owner || this.ownerCallback()
    }

    get ownerIri(): string {
        return this._ownerIri
    }

    get algorithm(): Promise<Algorithm> {
        return this._algorithm = this._algorithm || this.algorithmCallback()
    }

    get attachments(): Promise<Attachment>[] {
        return this._attachments = this._attachments || this.attachmentsCallback()
    }

    get attachmentsIris(): string[] {
        return this._attachmentsIris
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.name,
                propertyName: 'navigation.propertyView.protocol.name',
                patchDtoProperty: 'name',
            },
            {
                propertyValue: this.aim,
                propertyName: 'navigation.propertyView.protocol.aim',
                patchDtoProperty: 'aim',
            },
            {
                propertyValue: this.comment,
                propertyName: 'navigation.propertyView.protocol.comment',
                patchDtoProperty: 'comment',
            },
            this.owner.then(owner => ({
                propertyName: 'navigation.propertyView.protocol.owner',
                propertyValue: owner.username,
            })),
            this.algorithm.then(algorithm => ({
                propertyName: 'navigation.propertyView.protocol.algorithm',
                propertyValue: algorithm?.name ?? 'Sans tirage',
            })),
            Promise.all(this.attachments.map(promise => promise.then(att => att.file.then(file => ({
                name: file.originalFileName,
                id: file.id,
                attachmentIri: att.iri,
            }))))).then(files => files.reduce((acc, item) => ({
                ...acc,
                propertyValueArguments: {count: acc.propertyValueArguments.count + 1},
                nameLinkTab: [...acc.nameLinkTab, item],
            }), {
                propertyValue: 'navigation.propertyView.experiment.attachmentCount',
                propertyValueArguments: {count: 0},
                propertyName: 'navigation.propertyView.experiment.attachments',
                nameLinkTab: [],
            })),
        ])
    }

}
