import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import HasNote from '@adonis/webapp/models/interfaces/has-note'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Note from '@adonis/webapp/models/note'
import { v4 } from 'uuid'

export class Individual implements ApiEntity, HasPathLevel, HasNote, PropertyViewable {

  private _notes: Promise<Note>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public x: number,
      public y: number,
      public dead: boolean,
      public appeared: Date,
      public disappeared: Date,
      public identifier: string,
      public latitude: number,
      public longitude: number,
      public height: number,
      private _noteIris: string[],
      private notesCallback: () => Promise<Note>[],
      public color: number,
      public comment: string,
      public openSilexUri: string,
      public geometry: string,
  ) {
  }

  get pathLevel(): PathLevelEnum {
    return PathLevelEnum.INDIVIDUAL
  }

  get children(): null {
    return null
  }

  get notes(): Promise<Note>[] {
    return this._notes = this._notes || this.notesCallback()
  }

  get noteIris(): string[] {
    return this._noteIris
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.individual.name',
      },
      {
        propertyValue: this.x,
        propertyName: 'navigation.propertyView.individual.x',
      },
      {
        propertyValue: this.y,
        propertyName: 'navigation.propertyView.individual.y',
      },
      {
        propertyValue: this.identifier,
        propertyName: 'navigation.propertyView.individual.identifier',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.individual.comment',
        patchDtoProperty: 'comment',
      },
      {
        propertyValue: this.dead,
        propertyName: 'navigation.propertyView.individual.dead',
      },
      {
        propertyValue: this.appeared,
        propertyName: 'navigation.propertyView.individual.appeared',
      },
      {
        propertyValue: this.disappeared,
        propertyName: 'navigation.propertyView.individual.disappeared',
      },
      {
        propertyValue: this.openSilexUri,
        propertyName: 'navigation.propertyView.individual.openSilexUri',
      },
      {
          propertyValue: this.geometry,
          propertyName: 'navigation.propertyView.unitPlot.geometry',
          patchDtoProperty: 'geometry',
      },
    ] )
  }

  clone(): Individual {
    return new Individual(
        v4(),
        null,
        this.name,
        this.x,
        this.y,
        this.dead,
        this.appeared,
        this.disappeared,
        this.identifier,
        this.latitude,
        this.longitude,
        this.height,
        [],
        () => [],
        this.color,
        this.comment,
        null,
        this.geometry,
    )
  }

}
