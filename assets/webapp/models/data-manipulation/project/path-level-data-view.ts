import IconEnum from '@adonis/shared/constants/icon-name-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

/**
 * Display data of PathLevelItemInterface.
 */
export interface PathLevelDataView {

  id: number
  iri: string
  name: string
  icon: IconEnum
  pathLevel: PathLevelEnum
  children: PathLevelDataView[]
  x?: number
  y?: number
  treatmentName?: string
  treatmentShortName?: string
}
