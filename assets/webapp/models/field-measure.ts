import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import BlockDataView from '@adonis/webapp/models/data-view/block-data-view'
import ExperimentDataView from '@adonis/webapp/models/data-view/experiment-data-view'
import { IndividualDataView } from '@adonis/webapp/models/data-view/individual-data-view'
import PlatformDataView from '@adonis/webapp/models/data-view/platform-data-view'
import SubBlockDataView from '@adonis/webapp/models/data-view/sub-block-data-view'
import { SurfacicUnitPlotDataView } from '@adonis/webapp/models/data-view/surfacic-unit-plot-data-view'
import UnitPlotDataView from '@adonis/webapp/models/data-view/unit-plot-data-view'

export default class FieldMeasure implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public measures: {
        iri: string,
        value: string,
        repetition: number,
        timestamp: Date,
        state: number,
          stateColor: number,
      }[],
      public fieldGenerations: {
        index: number,
        prefix: string,
        numeralIncrement: boolean,
        children: FieldMeasure[],
      }[],
      public target: BlockDataView | ExperimentDataView | IndividualDataView | SubBlockDataView | SurfacicUnitPlotDataView | UnitPlotDataView | PlatformDataView,
      public targetIri: string,
      public targetType: PathLevelEnum,
      private _variableIri: string,
      public variable: {
          format: VariableFormatEnum,
      },
  ) {
  }

  get variableIri(): string {
    return this._variableIri
  }
}
