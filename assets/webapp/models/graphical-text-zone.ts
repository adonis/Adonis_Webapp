import ApiEntity from '@adonis/shared/models/api-entity'

export default class GraphicalTextZone implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public text: string,
      public color: number,
      public x: number,
      public y: number,
      public width: number,
      public height: number,
      public size: number,
  ) {
  }
}
