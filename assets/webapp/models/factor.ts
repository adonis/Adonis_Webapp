import ApiEntity from '@adonis/shared/models/api-entity'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Modality from '@adonis/webapp/models/modality'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export default class Factor implements ApiEntity, PropertyViewable {

    private _modalities: Promise<Modality>[]

    private _openSilexInstance: Promise<OpenSilexInstance>

    constructor(
        public iri: string,
        public id: number,
        public label: string,
        private _modalityIris: string[],
        private modalityCallback: () => Promise<Modality>[],
        public order: number,
        public openSilexUri: string,
        private _openSilexInstanceIri: string,
        private openSilexInstanceCallback: () => Promise<OpenSilexInstance>,
        public germplasm: boolean,
    ) {

    }

    get modalities(): Promise<Modality>[] {
        return this._modalities = this._modalities || this.modalityCallback()
    }

    get modalitiesIriTab(): string[] {
        return this._modalityIris
    }

    get openSilexInstance(): Promise<OpenSilexInstance> {
        return this._openSilexInstance = this._openSilexInstance || this.openSilexInstanceCallback()
    }

    get openSilexInstanceIri(): string {
        return this._openSilexInstanceIri
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.label,
                propertyName: 'navigation.propertyView.factor.label',
            },
            {
                propertyValue: this.germplasm,
                propertyName: 'navigation.propertyView.factor.germplasm',
            },
            {
                propertyValue: this.openSilexUri,
                propertyName: 'navigation.propertyView.factor.openSilexUri',
            },
        ])
    }

}
