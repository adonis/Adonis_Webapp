import ApiEntity from '@adonis/shared/models/api-entity'

export default class File implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public fileName?: string,
      public originalFileName?: string,
      public size?: number,
      public date?: string,
  ) {

  }
}
