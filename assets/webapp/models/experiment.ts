import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import User from '@adonis/shared/models/user'
import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import Attachment from '@adonis/webapp/models/attachment'
import Block from '@adonis/webapp/models/block'
import HasNote from '@adonis/webapp/models/interfaces/has-note'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Note from '@adonis/webapp/models/note'
import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import Protocol from '@adonis/webapp/models/protocol'

export default class Experiment implements ApiEntity, HasPathLevel, HasNote, PropertyViewable {

    private _protocol: Promise<Protocol>
    private _owner: Promise<User>
    private _blocks: Promise<Block>[]
    private _outExperimentationZones: Promise<OutExperimentationZone>[]
    private _notes: Promise<Note>[]
    private _attachments: Promise<Attachment>[]

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        private _protocolIri: string,
        private protocolCallback: () => Promise<Protocol>,
        public comment: string,
        public created: Date,
        public individualUP: boolean,
        private _ownerIri: string,
        private ownerCallback: () => Promise<User>,
        private _blockIris: string[],
        private blocksCallback: () => Promise<Block>[],
        private _outExperimentationZonesIri: string[],
        private outExperimentationZonesCallback: () => Promise<OutExperimentationZone>[],
        private _noteIris: string[],
        private notesCallback: () => Promise<Note>[],
        public color: number,
        public state: ExperimentStateEnum,
        private _attachmentsIris: string[],
        private attachmentsCallback: () => Promise<Attachment>[],
        public openSilexUri: string,
        public geometry: string,
    ) {

    }

    get protocol(): Promise<Protocol> {
        return this._protocol = this._protocol || this.protocolCallback()
    }

    get protocolIri(): string {
        return this._protocolIri
    }

    get owner(): Promise<User> {
        return this._owner = this._owner || this.ownerCallback()
    }

    get ownerIri(): string {
        return this._ownerIri
    }

    get pathLevel(): PathLevelEnum {
        return PathLevelEnum.EXPERIMENT
    }

    get blocks(): Promise<Block>[] {
        return this._blocks = this._blocks || this.blocksCallback()
    }

    get blockIris(): string[] {
        return this._blockIris
    }

    get outExperimentationZones(): Promise<OutExperimentationZone>[] {
        return this._outExperimentationZones = this._outExperimentationZones || this.outExperimentationZonesCallback()
    }

    get outExperimentationZonesIris(): string[] {
        return this._outExperimentationZonesIri
    }

    get notes(): Promise<Note>[] {
        return this._notes = this._notes || this.notesCallback()
    }

    get noteIris(): string[] {
        return this._noteIris
    }

    get attachments(): Promise<Attachment>[] {
        return this._attachments = this._attachments || this.attachmentsCallback()
    }

    get attachmentsIris(): string[] {
        return this._attachmentsIris
    }

    get children(): Promise<Block>[] {
        return this.blocks
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.name,
                propertyName: 'navigation.propertyView.experiment.name',
                patchDtoProperty: 'name',
            },
            this.owner.then(owner => ({
                propertyName: 'navigation.propertyView.experiment.owner',
                propertyValue: owner.username,
            })),
            {
                propertyValue: this.created,
                propertyName: 'navigation.propertyView.experiment.created',
            },
            {
                propertyValue: 'enums.levels',
                propertyName: 'navigation.propertyView.experiment.individualUP',
                propertyValueArguments: {level: this.individualUP ? PathLevelEnum.UNIT_PLOT : PathLevelEnum.SURFACE_UNIT_PLOT},
            },
            {
                propertyValue: this.comment,
                propertyName: 'navigation.propertyView.experiment.comment',
                patchDtoProperty: 'comment',
            },
            {
                propertyName: 'navigation.propertyView.experiment.state',
                propertyValue: 'enums.experimentState',
                propertyValueArguments: {state: this.state},
            },
            Promise.all(this.attachments.map(promise => promise.then(att => att.file.then(file => ({
                name: file.originalFileName,
                id: file.id,
                attachmentIri: att.iri,
            }))))).then(files => files.reduce((acc, item) => ({
                ...acc,
                propertyValueArguments: {count: acc.propertyValueArguments.count + 1},
                nameLinkTab: [...acc.nameLinkTab, item],
            }), {
                propertyValue: 'navigation.propertyView.experiment.attachmentCount',
                propertyValueArguments: {count: 0},
                propertyName: 'navigation.propertyView.experiment.attachments',
                nameLinkTab: [],
            })),
            {
                propertyValue: this.openSilexUri,
                propertyName: 'navigation.propertyView.experiment.openSilexUri',
            },
            {
                propertyValue: this.geometry,
                propertyName: 'navigation.propertyView.experiment.geometry',
                patchDtoProperty: 'geometry',
            },
        ])
    }

}
