import ApiEntity from '@adonis/shared/models/api-entity'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import BlockDataView from '@adonis/webapp/models/data-view/block-data-view'
import ExperimentDataView from '@adonis/webapp/models/data-view/experiment-data-view'
import { IndividualDataView } from '@adonis/webapp/models/data-view/individual-data-view'
import PlatformDataView from '@adonis/webapp/models/data-view/platform-data-view'
import SubBlockDataView from '@adonis/webapp/models/data-view/sub-block-data-view'
import { SurfacicUnitPlotDataView } from '@adonis/webapp/models/data-view/surfacic-unit-plot-data-view'
import UnitPlotDataView from '@adonis/webapp/models/data-view/unit-plot-data-view'

export default class Annotation implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public categories: string[],
      public image: string,
      public keywords: string[],
      public name: string,
      public target: BlockDataView | ExperimentDataView | IndividualDataView | SubBlockDataView | SurfacicUnitPlotDataView | UnitPlotDataView | PlatformDataView | string,
      public targetType: PathLevelEnum,
      public timestamp: Date,
      public type: number,
      public value: string,
  ) {

  }

}
