import ApiEntity from '@adonis/shared/models/api-entity'
import MobileSession from '@adonis/webapp/models/mobile-session'

export default class MobileDataEntry implements ApiEntity {
  private _sessions: Promise<MobileSession>[]

  constructor(
      public iri: string,
      public id: number,
      private _sessionIris: string[],
      private sessionCallback: () => Promise<MobileSession>[],
  ) {

  }

  get sessions(): Promise<MobileSession>[] {
    return this._sessions = this._sessions || this.sessionCallback()
  }

  get sessionIris(): string[] {
    return this._sessionIris
  }
}
