import ApiEntity from '@adonis/shared/models/api-entity'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import PossibleMoveEnum from '@adonis/webapp/constants/possible-move-enum'
import PossibleStartPointEnum from '@adonis/webapp/constants/possible-start-point-enum'

export default class PathLevelAlgorithm implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public pathLevel: PathLevelEnum,
      public move: PossibleMoveEnum,
      public startPoint: PossibleStartPointEnum,
      public orderedLevelIris: string[],
  ) {

  }

}
