import ApiEntity from '@adonis/shared/models/api-entity'
import TextureEnum from '@adonis/webapp/constants/texture-enum'

export default class OezNature implements ApiEntity {

  constructor(
      public iri: string,
      public nature: string,
      public color: number,
      public texture: TextureEnum,
  ) {

  }

}
