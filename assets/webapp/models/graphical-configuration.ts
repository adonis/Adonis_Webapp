import ApiEntity from '@adonis/shared/models/api-entity'
import {
  BlockLabelsEnum,
  ExperimentLabelsEnum,
  IndividualLabelsEnum,
  SubBlockLabelsEnum,
  SurfacicUnitPlotLabelsEnum,
  UnitPlotLabelsEnum,
} from '@adonis/webapp/constants/graphical-labels-enums'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import * as PIXI from 'pixi.js'

export default class GraphicalConfiguration implements ApiEntity {

  public static defaultDeadItemColor = 0xff0000
  public static defaultInactiveItemColor = 0x9c9c9c
  public static defaultSelectionRectangleColor = 0x000000
  public static defaultInvalidSelectionRectangleColor = 0xff0000
  public static defaultLoadingBackgroundColor = 0x000000

  private static individualScaleThreshold = 0.15
  private static unitPlotScaleThreshold = 0.13
  private static subBlockScaleThreshold = 0.11
  private static blockScaleThreshold = 0.10

  private static defaultIndividualColor = 0xffff00
  private static defaultUnitPlotColor = 0x33cc00
  private static defaultBlockColor = 0x3399cc
  private static defaultSubBlockColor = 0x33ccff
  private static defaultExperimentColor = 0x99ff99
  private static defaultPlatformColor = 0xffffff
  private static defaultSelectedItemColor = 0x000000

  private static minZoomW = 1000
  private static maxZoomW = 5000
  private static minZoomH = 1000
  private static maxZoomH = 5000
  private static textureH = 128
  private static textureW = 128

  private static individualOffset = 25
  private static unitPlotOffset = 20
  private static subBlockOffset = 15
  private static blockOffset = 10
  private static experimentOffset = 5

  constructor(
      public iri: string,
      public individualColor?: number,
      public unitPlotColor?: number,
      public blockColor?: number,
      public subBlockColor?: number,
      public experimentColor?: number,
      public platformColor?: number,
      public selectedItemColor?: number,
      public activeBreadcrumb?: boolean,
      public experimentLabels?: ExperimentLabelsEnum[],
      public blocLabels?: BlockLabelsEnum[],
      public subBlocLabels?: SubBlockLabelsEnum[],
      public unitPlotLabels?: UnitPlotLabelsEnum[],
      public surfacicUnitPlotLabels?: SurfacicUnitPlotLabelsEnum[],
      public individualLabels?: IndividualLabelsEnum[],
  ) {
    this.individualColor = individualColor ?? GraphicalConfiguration.defaultIndividualColor
    this.unitPlotColor = unitPlotColor ?? GraphicalConfiguration.defaultUnitPlotColor
    this.blockColor = blockColor ?? GraphicalConfiguration.defaultBlockColor
    this.subBlockColor = subBlockColor ?? GraphicalConfiguration.defaultSubBlockColor
    this.experimentColor = experimentColor ?? GraphicalConfiguration.defaultExperimentColor
    this.platformColor = platformColor ?? GraphicalConfiguration.defaultPlatformColor
    this.selectedItemColor = selectedItemColor ?? GraphicalConfiguration.defaultSelectedItemColor
    this.activeBreadcrumb = activeBreadcrumb ?? true
    this.experimentLabels = experimentLabels ?? [ExperimentLabelsEnum.NAME]
    this.blocLabels = blocLabels ?? [BlockLabelsEnum.NUMBER]
    this.subBlocLabels = subBlocLabels ?? [SubBlockLabelsEnum.NUMBER]
    this.unitPlotLabels = unitPlotLabels ?? [UnitPlotLabelsEnum.NUMBER]
    this.surfacicUnitPlotLabels = surfacicUnitPlotLabels ?? [SurfacicUnitPlotLabelsEnum.NUMBER]
    this.individualLabels = individualLabels ?? [IndividualLabelsEnum.NUMBER]
  }

  private static normalize( p: number[] ) {
    const length = Math.sqrt( p[0] * p[0] + p[1] * p[1] )
    p[0] /= length
    p[1] /= length
    return p
  }

  private static getDirectorVector( index: number, points: PIXI.Point[] ): number[] {
    let res = new Array( 2 )

    const nextPoint = (index + points.length + 1) % points.length
    const currentPoint = (index + points.length) % points.length

    res[0] = points[nextPoint].x - points[currentPoint].x
    res[1] = points[nextPoint].y - points[currentPoint].y

    res = GraphicalConfiguration.normalize( res )

    return res
  }

  private static crossProductZ( a: number[], b: number[] ): number {
    return a[0] * b[1] - a[1] * b[0]
  }

  public getShrinkedPolygon( innerMargin: number, points: PIXI.Point[] ): PIXI.Point[] {

    const res: PIXI.Point[] = []
    if (points.length === 0) {
      return res
    }

    innerMargin = this.getOrientation( points ) * innerMargin

    points.forEach( ( point, i ) => {

      const v = GraphicalConfiguration.getDirectorVector( i, points )
      const u = GraphicalConfiguration.getDirectorVector( i - 1, points )

      const sinAlpha = -GraphicalConfiguration.crossProductZ( v, u )

      if (Math.abs( sinAlpha ) > 1.0e-12) {
        const k = innerMargin / sinAlpha
        const dx = k * v[0] - k * u[0]
        const dy = k * v[1] - k * u[1]
        const x = point.x
        const y = point.y

        const p = new PIXI.Point( x + dx, y + dy )

        res.push( p )
      }
    } )
    return res
  }

  private getOrientation( points: PIXI.Point[] ): number {
    // find a points on the bounding edge
    let indexMini = 0
    points.forEach( ( point, i ) => {
      if (point.x < points[indexMini].x) {
        indexMini = i
      } else if (point.x === points[indexMini].x) {
        if (point.y < points[indexMini].y) {
          indexMini = i
        }
      }
    } )

    const a = GraphicalConfiguration.getDirectorVector( indexMini, points )
    const b = GraphicalConfiguration.getDirectorVector( indexMini - 1, points )

    return (GraphicalConfiguration.crossProductZ( a, b ) < 0) ? 1 : -1
  }

  getOffsetForType( objectType: PathLevelEnum ) {
    switch (objectType) {
      case PathLevelEnum.OEZ:
        return GraphicalConfiguration.individualOffset
      case PathLevelEnum.INDIVIDUAL:
        return GraphicalConfiguration.individualOffset
      case PathLevelEnum.UNIT_PLOT:
        return GraphicalConfiguration.unitPlotOffset
      case PathLevelEnum.SURFACE_UNIT_PLOT:
        return GraphicalConfiguration.unitPlotOffset
      case PathLevelEnum.BLOCK:
        return GraphicalConfiguration.blockOffset
      case PathLevelEnum.SUB_BLOCK:
        return GraphicalConfiguration.subBlockOffset
      case PathLevelEnum.EXPERIMENT:
        return GraphicalConfiguration.experimentOffset
      case PathLevelEnum.PLATFORM:
        return 0
    }
  }

  getColorForType( objectType: PathLevelEnum ) {
    switch (objectType) {
      case PathLevelEnum.OEZ:
        return this.platformColor
      case PathLevelEnum.INDIVIDUAL:
        return this.individualColor
      case PathLevelEnum.UNIT_PLOT:
        return this.unitPlotColor
      case PathLevelEnum.SURFACE_UNIT_PLOT:
        return this.unitPlotColor
      case PathLevelEnum.BLOCK:
        return this.blockColor
      case PathLevelEnum.SUB_BLOCK:
        return this.subBlockColor
      case PathLevelEnum.EXPERIMENT:
        return this.experimentColor
      case PathLevelEnum.PLATFORM:
        return this.platformColor
    }
  }

  getBreadcrumbText( objectType: PathLevelEnum, object: any ) {
    const propertyCallback = ( item: string ) => object[item]
    let res: string[] = []
    switch (objectType) {
      case PathLevelEnum.INDIVIDUAL:
        res = this.individualLabels
        break
      case PathLevelEnum.UNIT_PLOT:
        res = this.unitPlotLabels
        break
      case PathLevelEnum.SURFACE_UNIT_PLOT:
        res = this.surfacicUnitPlotLabels
        break
      case PathLevelEnum.BLOCK:
        res = this.blocLabels
        break
      case PathLevelEnum.SUB_BLOCK:
        res = this.subBlocLabels
        break
      case PathLevelEnum.EXPERIMENT:
        res = this.experimentLabels
        break
    }
    return res.map( propertyCallback )
        .join( '; ' )
  }

  getPathLevelForScale( scale: number, isIndividualUnitPlot: boolean ) {
    if (scale > GraphicalConfiguration.individualScaleThreshold) {
      return isIndividualUnitPlot ? PathLevelEnum.INDIVIDUAL : PathLevelEnum.SURFACE_UNIT_PLOT
    } else if (scale > GraphicalConfiguration.unitPlotScaleThreshold) {
      return PathLevelEnum.UNIT_PLOT
    } else if (scale > GraphicalConfiguration.subBlockScaleThreshold) {
      return PathLevelEnum.SUB_BLOCK
    } else if (scale > GraphicalConfiguration.blockScaleThreshold) {
      return PathLevelEnum.BLOCK
    } else {
      return PathLevelEnum.EXPERIMENT
    }
  }

  get textureW(): number {
    return GraphicalConfiguration.textureW
  }

  get textureH(): number {
    return GraphicalConfiguration.textureH
  }

  get minZoomW(): number {
    return GraphicalConfiguration.minZoomW
  }

  get maxZoomW(): number {
    return GraphicalConfiguration.maxZoomW
  }

  get minZoomH(): number {
    return GraphicalConfiguration.minZoomH
  }

  get maxZoomH(): number {
    return GraphicalConfiguration.maxZoomH
  }
}
