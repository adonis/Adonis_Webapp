import LeafPart from '@adonis/webapp/models/graphics/business-objects/leaf-part'

export default class PositionTreeNode {

  private static xInfyInf = parseInt( '00', 3 )
  private static xInfyEq = parseInt( '01', 3 )
  private static xInfySup = parseInt( '02', 3 )
  private static xEqyInf = parseInt( '10', 3 )
  // private static xEqyEq = parseInt('11', 3)
  private static xEqySup = parseInt( '12', 3 )
  private static xSupyInf = parseInt( '20', 3 )
  private static xSupyEq = parseInt( '21', 3 )
  private static xSupySup = parseInt( '22', 3 )

  sons: PositionTreeNode[]

  constructor(
      public xPos: number,
      public yPos: number,
      public part: LeafPart,
  ) {
    this.sons = []
  }

  getNode( x: number, y: number ): PositionTreeNode {
    if (this.xPos === x && this.yPos === y) {
      return !!this.part ? this : null
    } else {
      const tabIndex = this.getTabIndex( x, y )
      return this.sons[tabIndex]?.getNode( x, y )
    }
  }

  addPart( x: number, y: number, part: LeafPart ) {
    const tabIndex = this.getTabIndex( x, y )
    if (!!this.sons[tabIndex]) {
      this.sons[tabIndex].addPart( x, y, part )
    } else if (tabIndex === parseInt( '11', 3 )) {
      // Do not throw error if the part exists because of batch move
      this.part = part
    } else {
      this.sons[tabIndex] = new PositionTreeNode( x, y, part )
    }
  }

  deletePart( x: number, y: number ): LeafPart {
    if (this.xPos === x && this.yPos === y) {
      const res = this.part
      this.part = undefined
      return res
    }
    const tabIndex = this.getTabIndex( x, y )
    if (!!this.sons[tabIndex]) {
      return this.sons[tabIndex].deletePart( x, y )
    }
    return null
  }

  getNodesInRect( x: number[], y: number[] ): PositionTreeNode[] {
    const res = []
    const validBranches: PositionTreeNode[] = []
    if (
        this.xPos < x[1] &&
        this.xPos > x[0] &&
        this.yPos < y[1] &&
        this.yPos > y[0] &&
        !!this.part
    ) {
      res.push( this )
    }
    if (this.xPos >= x[0]) {
      if (this.yPos >= y[0]) {
        [
          PositionTreeNode.xInfyInf, PositionTreeNode.xInfyEq, PositionTreeNode.xEqyInf,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
      if (this.yPos <= y[1]) {
        [
          PositionTreeNode.xInfyEq, PositionTreeNode.xInfySup, PositionTreeNode.xEqySup,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
    }
    if (this.xPos <= x[1]) {
      if (this.yPos >= y[0]) {
        [
          PositionTreeNode.xSupyInf, PositionTreeNode.xSupyEq, PositionTreeNode.xEqyInf,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
      if (this.yPos <= y[1]) {
        [
          PositionTreeNode.xSupyEq, PositionTreeNode.xSupySup, PositionTreeNode.xEqySup,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
    }
    validBranches.forEach( branch => {
      if (!!branch) {
        res.push( ...branch.getNodesInRect( x, y ) )
      }
    } )
    return res
  }

  hasNodeInRect( x: number[], y: number[] ): boolean {
    const validBranches: PositionTreeNode[] = []
    if (
        this.xPos < x[1] &&
        this.xPos > x[0] &&
        this.yPos < y[1] &&
        this.yPos > y[0] &&
        !!this.part
    ) {
      return true
    }
    if (this.xPos >= x[0]) {
      if (this.yPos >= y[0]) {
        [
          PositionTreeNode.xInfyInf, PositionTreeNode.xInfyEq, PositionTreeNode.xEqyInf,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
      if (this.yPos <= y[1]) {
        [
          PositionTreeNode.xInfyEq, PositionTreeNode.xInfySup, PositionTreeNode.xEqySup,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
    }
    if (this.xPos <= x[1]) {
      if (this.yPos >= y[0]) {
        [
          PositionTreeNode.xSupyInf, PositionTreeNode.xSupyEq, PositionTreeNode.xEqyInf,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
      if (this.yPos <= y[1]) {
        [
          PositionTreeNode.xSupyEq, PositionTreeNode.xSupySup, PositionTreeNode.xEqySup,
        ].forEach( item => validBranches[item] = this.sons[item] )
      }
    }
    return validBranches.reduce( (acc, branch) => acc || !!branch && branch.hasNodeInRect( x, y ), false )
  }

  private getTabIndex( x: number, y: number ) {
    let xComparator: string
    let yComparator: string
    if (x < this.xPos) {
      xComparator = '0'
    } else if (x === this.xPos) {
      xComparator = '1'
    } else {
      xComparator = '2'
    }
    if (y < this.yPos) {
      yComparator = '0'
    } else if (y === this.yPos) {
      yComparator = '1'
    } else {
      yComparator = '2'
    }
    return parseInt( xComparator + yComparator, 3 )
  }

}
