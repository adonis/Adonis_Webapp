import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'

export default class GraphicalContext {

  constructor(
      public cellW: number,
      public cellH: number,
      public minCellX: number,
      public minCellY: number,
      public maxCellX: number,
      public maxCellY: number,
      public mode: GraphicalEditorModeEnum,
      public isCopying: boolean,
  ) {
  }

  get worldW() {
    return this.cellW * this.boudaryX
  }

  get worldH() {
    return this.cellH * this.boundaryY
  }

  get boudaryX(): number {
    return 50 + 2 * this.maxCellX
  }

  get boundaryY(): number {
    return 50 + 2 * this.maxCellY
  }
}
