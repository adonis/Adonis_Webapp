import * as PIXI from 'pixi.js'

export default interface IMaskMovedObserver {
  updateMoveEndState( visibleBound: PIXI.Rectangle ): void
}
