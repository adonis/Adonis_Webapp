export default interface IParentObserver {
    updateChildState(): void
}
