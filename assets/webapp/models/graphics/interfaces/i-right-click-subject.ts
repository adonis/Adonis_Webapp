import IRightClickObserver from '@adonis/webapp/models/graphics/interfaces/i-right-click-observer'

export default interface IRightClickSubject {
  subscribeRightClick( observer: IRightClickObserver ): void

  unsubscribeRightClick( observer: IRightClickObserver ): void

  notifyRightClickState( x: number, y: number, showObject: boolean ): void
}
