import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export default interface IRightClickObserver {
  updateRightClickState(
      originX: number,
      originY: number,
      objectToShow: any,
      objectPathLevel: PathLevelEnum,
  ): void
}
