import IParentObserver from './i-parent-observer'

export default interface IParentSubject {
  subscribeParent( observer: IParentObserver ): void

  unsubscribeParent( observer: IParentObserver ): void

  notifyParentRedraw(): void
}
