import IHoverObserver from '@adonis/webapp/models/graphics/interfaces/i-hover-observer'

export default interface IHoverSubject {
  subscribeHover( observer: IHoverObserver ): void

  unsubscribeHover( observer: IHoverObserver ): void

  notifyHoverState( x: number, y: number, isHovering: boolean  ): void
}
