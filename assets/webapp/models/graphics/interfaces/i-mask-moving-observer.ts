import * as PIXI from 'pixi.js'

export default interface IMaskMovingObserver {
  updateMovingState( visibleBound: PIXI.Rectangle ): void
}
