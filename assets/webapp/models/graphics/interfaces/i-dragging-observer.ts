export default interface IDraggingObserver {
  updateDraggingState( isDragging: boolean ): void
}
