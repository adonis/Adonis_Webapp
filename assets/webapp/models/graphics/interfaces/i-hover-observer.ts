import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export default interface IHoverObserver {
  updateHoveringState(
      originX: number,
      originY: number,
      objectToShow: any,
      objectPathLevel: PathLevelEnum,
  ): void
}
