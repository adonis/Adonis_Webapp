import IMaskMovedObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moved-observer'
import * as PIXI from 'pixi.js'
import IMaskMovingObserver from './i-mask-moving-observer'

export default interface IMaskSubject {
  subscribeMovingMask( observer: IMaskMovingObserver ): void

  unsubscribeMovingMask( observer: IMaskMovingObserver ): void

  notifyMovingState( visibleBound: PIXI.Rectangle, shouldActivate: boolean ): void

  subscribeMovedMask( observer: IMaskMovedObserver ): void

  unsubscribeMovedMask( observer: IMaskMovedObserver ): void

  notifyMoveEndState( visibleBound: PIXI.Rectangle, shouldActivate: boolean ): void
}
