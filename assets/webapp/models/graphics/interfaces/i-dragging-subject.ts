import IDraggingObserver from './i-dragging-observer'

export default interface IDraggingSubject {

  subscribeDrag( observer: IDraggingObserver ): void

  unsubscribeDrag( observer: IDraggingObserver ): void

  notifyDraggingState( isDragging: boolean ): void

}
