import ApiEntity from '@adonis/shared/models/api-entity'
import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import NodePart from '@adonis/webapp/models/graphics/business-objects/node-part'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IDraggingObserver from '@adonis/webapp/models/graphics/interfaces/i-dragging-observer'
import IDraggingSubject from '@adonis/webapp/models/graphics/interfaces/i-dragging-subject'
import IHoverObserver from '@adonis/webapp/models/graphics/interfaces/i-hover-observer'
import IHoverSubject from '@adonis/webapp/models/graphics/interfaces/i-hover-subject'
import IMaskMovedObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moved-observer'
import IParentObserver from '@adonis/webapp/models/graphics/interfaces/i-parent-observer'
import IParentSubject from '@adonis/webapp/models/graphics/interfaces/i-parent-subject'
import { Individual } from '@adonis/webapp/models/individual'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import * as PIXI from 'pixi.js'
import { Geom } from 'polygon-clipping'

export default abstract class AbstractPart<R extends ApiEntity & HasPathLevel> implements IDraggingSubject, IMaskMovedObserver, IParentSubject, IHoverSubject {

  private observers: IDraggingObserver[] = []
  private parentObservers: IParentObserver[] = []
  private hoverObservers: IHoverObserver[] = []
  public subject: R
  public color?: number
  public objectType: PathLevelEnum
  private _drawingObject: PIXI.Graphics
  protected _text: PIXI.Text
  protected _texture?: PIXI.Sprite
  protected _container: PIXI.Container
  protected _selected = false
  protected graphicalConfiguration: GraphicalConfiguration
  protected graphicalContext: GraphicalContext
  public extraBreadcrumbLabels: any
  public parentPart: NodePart
  protected _graphicalOrigin: GraphicalOriginEnum
  protected _showedText: string
  protected _inViewport = true
  protected _inFilterList = true

  protected draggingRelativeOrigin: PIXI.Point
  protected draggingOrigin: PIXI.Point
  public dragging: boolean
  protected startSelect: boolean
  private _selectCallback: ( ctrlPressed: boolean ) => void = () => {
  }
  protected _moveCallback: ( dx: number, dy: number ) => void = () => {
  }
  private _dragCallback: ( x: number, y: number, newX: number, newY: number ) => void = () => {
  }
  protected _dropCallback: () => void = () => {
  }
  private _rightClickCallback: ( x: number, y: number ) => void = () => {
  }

  public origin: PIXI.Point

  public points: PIXI.Point[][]

  public polygon: Geom

  /**
   * Set the parent view of this graphic
   * @param container
   */
  renderIn( container: PIXI.Container ) {
    container.addChild( this.container )
  }

  removeFromView( container: PIXI.Container ) {
    container.removeChild( this.container )
    this.parentPart.childrenParts = this.parentPart.childrenParts.filter( part => part !== this )
  }

  protected onStartHovering( event: PIXI.InteractionEvent ) {
    const position = event.data.getLocalPosition( this._drawingObject.parent )
    this.notifyHoverState( position.x, position.y, true )
  }

  protected onStopHovering( event: PIXI.InteractionEvent ) {
    const position = event.data.getLocalPosition( this._drawingObject.parent )
    this.notifyHoverState( position.x, position.y, false )
  }

  protected onRightClick( event: PIXI.InteractionEvent ) {
    const position = event.data.getLocalPosition( this._drawingObject.parent )
    if (!this._selected && this.graphicalContext.mode === GraphicalEditorModeEnum.EDIT) {
      this._selectCallback( false )
    }
    this._rightClickCallback( position.x, position.y )
  }

  subscribeDrag( observer: IDraggingObserver ) {
    this.observers.push( observer )
  }

  unsubscribeDrag( observer: IDraggingObserver ) {
    this.observers = this.observers.filter( ( element ) => {
      return observer !== element
    } )
  }

  notifyDraggingState( isDraging: boolean ) {
    this.observers.forEach( observer => {
      observer.updateDraggingState( isDraging )
    } )
  }

  notifyParentRedraw(): void {
    this.parentObservers.forEach( observer => {
      observer.updateChildState()
    } )
  }

  subscribeParent( observer: IParentObserver ): void {
    this.parentObservers.push( observer )
  }

  unsubscribeParent( observer: IParentObserver ): void {
    this.parentObservers = this.parentObservers.filter( ( element ) => {
      return observer !== element
    } )
  }

  notifyHoverState( x: number, y: number, isHovering: boolean ): void {
    this.hoverObservers.forEach( observer => {
      observer.updateHoveringState( x, y, isHovering ? { ...this.extraBreadcrumbLabels, ...this.subject } : null, this.objectType )
    } )
  }

  subscribeHover( observer: IHoverObserver ): void {
    this.hoverObservers.push( observer )
  }

  unsubscribeHover( observer: IHoverObserver ): void {
    this.hoverObservers = this.hoverObservers.filter( ( element ) => {
      return observer !== element
    } )
  }

  protected onSelect( ctrlPressed: boolean ) {
    this._selectCallback( ctrlPressed )
  }

  /**
   * Move the graphical object to the new location and redraw it
   * @param x new x position on the graphic
   * @param y new y position on the graphic
   * @param alpha transparency from 0 to 1
   * @protected
   */
  public moveTo( x = this.origin.x, y = this.origin.y, alpha = 0.8 ) {
    const xMove = x - this.origin.x
    const yMove = y - this.origin.y
    this.points.forEach( poly => {
      poly.forEach( point => {
        point.x += xMove
        point.y += yMove
      } )
    } )
    this.redraw()
  }

  public redraw() {
    this.drawingObject.clear()
    this.drawingObject.lineStyle( this._selected ? 5 : 2, this._selected ? this.graphicalConfiguration.selectedItemColor : undefined )
    this.points.forEach( poly => {
      this.drawingObject.beginFill( this.color !== undefined && this.color !== null ? this.color : this.graphicalConfiguration.getColorForType( this.objectType ), 0.8 )
      this.drawingObject.drawPolygon( poly )
      this.drawingObject.endFill()
    } )
    this._text.position.set( this.center.x, this.center.y )
    this._texture?.position.set( this.center.x, this.center.y )
  }

  setDraggingFrom( x: number, y: number ) {
    this._selected = true
    this.draggingRelativeOrigin = new PIXI.Point(
        this.origin.x - x,
        this.origin.y - y,
    )
    this.draggingOrigin = new PIXI.Point( this.origin.x, this.origin.y )
    this._drawingObject.alpha = 0.5
    this.dragging = true
    this.notifyDraggingState( this.dragging )
  }

  protected onDragStart( event: PIXI.InteractionEvent ) {
    this.startSelect = true // Needed to check if the event is a simple click or drag & drop
    if (this.graphicalContext.mode === GraphicalEditorModeEnum.EDIT && this._selected && !this.graphicalContext.isCopying) {
      this.draggingRelativeOrigin = new PIXI.Point(
          this.origin.x - event.data.getLocalPosition( this._drawingObject.parent ).x,
          this.origin.y - event.data.getLocalPosition( this._drawingObject.parent ).y,
      )
      this.draggingOrigin = new PIXI.Point( this.origin.x, this.origin.y )
      this._drawingObject.alpha = 0.5
      this.dragging = true
      this._drawingObject.on( 'pointerupoutside', evt => this.onDragEnd( evt ) )
      this.notifyDraggingState( this.dragging )
    }
  }

  protected onDragCancel() {
    this.startSelect = false
    if (this.dragging && !this.graphicalContext.isCopying) {
      this._drawingObject.alpha = 1
      this.dragging = false
      this.paint()
      this.notifyDraggingState( this.dragging )
    }
  }

  protected onDragMove( event: PIXI.InteractionEvent ) {
    this.startSelect = false
    if (this.dragging) {
      const newPosition = event.data.getLocalPosition( this._drawingObject.parent )
      this._dragCallback(
          newPosition.x + this.draggingRelativeOrigin.x + 10,
          newPosition.y + this.draggingRelativeOrigin.y + 10,
          this.getDragDX( newPosition.x ),
          this.getDragDY( newPosition.y ),
      )
    }
  }

  protected onDragEnd( event: PIXI.InteractionEvent ) {
    this._drawingObject.removeAllListeners( 'pointerupoutside' )
    if (this.startSelect) {
      this.onSelect( event.data.originalEvent.ctrlKey )
    }
    if (this.dragging) {
      const newPosition = event.data.getLocalPosition( this.drawingObject.parent )
      const dx = this.getDragDX( newPosition.x )
      const dy = this.getDragDY( newPosition.y )
      const canMove = (this.graphicalContext.mode === GraphicalEditorModeEnum.PLACE_EXPERIMENT || dx !== 0 || dy !== 0) && this.canMove( dx, dy )
      if ((this.graphicalContext.mode !== GraphicalEditorModeEnum.PLACE_EXPERIMENT && !this.graphicalContext.isCopying) || canMove) {
        this.dragging = false
        this._drawingObject.alpha = 1
        this.notifyDraggingState( this.dragging )
        if (canMove) {
          this._moveCallback( dx, dy )
        } else {
          this._dropCallback()
        }
      }
    }
  }

  public changeShowedPathLevel( pathLevel: PathLevelEnum ) {
    this._text.renderable = pathLevel === this.objectType || this.objectType === PathLevelEnum.OEZ
  }

  /**
   * Calculate the delta between old and new X position given the new X position
   * @param newXPosition
   * @protected
   */
  protected abstract getDragDX( newXPosition: number ): number

  /**
   * Calculate the delta between old and new Y position given the new Y position
   * @param newYPosition
   * @protected
   */
  protected abstract getDragDY( newYPosition: number ): number

  /**
   * Redraw the object on the graphics with current subject and children informations
   * @public
   */
  public abstract paint(): void

  public abstract resetColor(): void

  /**
   * Called when visible area change (move or zoom)
   * @param visibleBound
   */
  abstract updateMoveEndState( visibleBound: PIXI.Rectangle ): void

  /**
   * Change the position of the subject (or its children) and redraw
   * @param dx
   * @param dy
   */
  abstract moveSubject( dx: number, dy: number ): void

  abstract canMove( dx: number, dy: number ): boolean

  abstract get gridX(): number

  abstract get gridY(): number

  set selected( selected: boolean ) {
    if (selected !== this._selected) {
      this._selected = selected
      this.paint()
    }
  }

  set selectCallback( value: ( ctrlKey: boolean ) => void ) {
    this._selectCallback = value
  }

  set moveCallback( value: ( dx: number, dy: number ) => void ) {
    this._moveCallback = value
  }

  set dragCallback( value: ( x: number, y: number, dx: number, dy: number ) => void ) {
    this._dragCallback = value
  }

  set dropCallback( value: () => void ) {
    this._dropCallback = value
  }

  set rightClickCallback( value: ( x: number, y: number ) => void ) {
    this._rightClickCallback = value
  }

  get drawingObject(): PIXI.Graphics {
    return this._drawingObject
  }

  set drawingObject( value: PIXI.Graphics ) {
    this._drawingObject = value
    this._drawingObject.on( 'rightdown', event => this.onRightClick( event ) )
    this._drawingObject.on( 'mousedown', event => this.onDragStart( event ) )
    this._drawingObject.on( 'mouseup', event => this.onDragEnd( event ) )
    this._drawingObject.on( 'pointercancel', () => this.onDragCancel() )
    this._drawingObject.on( 'pointermove', event => this.onDragMove( event ) )
    this._drawingObject.on( 'pointerover', event => this.onStartHovering( event ) )
    this._drawingObject.on( 'pointerout', event => this.onStopHovering( event ) )
  }

  get container(): PIXI.Container {
    return this._container
  }

  set graphicalOrigin( value: GraphicalOriginEnum ) {
    this._graphicalOrigin = value
    switch (this._graphicalOrigin) {
      case GraphicalOriginEnum.TOP_LEFT:
        this._text.scale.set( 1, 1 )
        break
      case GraphicalOriginEnum.TOP_RIGHT:
        this._text.scale.set( -1, 1 )
        break
      case GraphicalOriginEnum.BOTTOM_RIGHT:
        this._text.scale.set( -1, -1 )
        break
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this._text.scale.set( 1, -1 )
        break
    }
  }

  get topLeftCorner() {
    return this.points.reduce( ( acc, items ) => {
      const localMin = items.reduce(
          ( acc2, item ) => ((acc2.x + acc2.y) < (item.x + item.y)) ? acc2 : item, items[0],
      )
      return ((acc.x + acc.y) < (localMin.x + localMin.y)) ? acc : localMin
    }, this.points[0][0] )
  }

  get center(): PIXI.Point {
    return new PIXI.Point(
        this.points.reduce(
            ( acc, points ) => acc + points.reduce(
                ( accc, point ) => accc + point.x,
                0 ) / points.length,
            0 ) / this.points.length,
        this.points.reduce(
            ( acc, points ) => acc + points.reduce(
                ( accc, point ) => accc + point.y,
                0 ) / points.length,
            0 ) / this.points.length,
    )
  }

  set textSize( value: number ) {
    this._text.style.fontSize = value
  }

  set showedText( showedText: string ) {
    this._showedText = showedText
    this.updateText()
  }

  protected abstract set visible( visible: boolean );

  set inViewport( value: boolean ) {
    this._inViewport = value
    this.visible = this._inFilterList && value
  }

  set inFilterList( value: boolean ) {
    this._inFilterList = value
    this.visible = this._inViewport && value
  }

  public updateText() {
    this._text.text = this.graphicalConfiguration.getBreadcrumbText( this.subject.pathLevel, { ...this.extraBreadcrumbLabels, ...this.subject } )

    if ((this.subject instanceof Individual || this.subject instanceof SurfacicUnitPlot) && this.subject.dead) {
      this._text.text = 'M'
    }
    if (this.graphicalContext.mode === GraphicalEditorModeEnum.VIEW_VARIABLE) {
      this._text.text = this._showedText
    }
  }

  prepareForExport() {
    this.container.renderable = this.container.visible = true
  }
}
