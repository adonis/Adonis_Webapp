import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import Block from '@adonis/webapp/models/block'
import Experiment from '@adonis/webapp/models/experiment'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import AbstractPart from '@adonis/webapp/models/graphics/business-objects/abstract-part'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IParentObserver from '@adonis/webapp/models/graphics/interfaces/i-parent-observer'
import Platform from '@adonis/webapp/models/platform'
import SubBlock from '@adonis/webapp/models/sub-block'
import UnitPlot from '@adonis/webapp/models/unit-plot'
import { min } from 'mathjs'
import * as PIXI from 'pixi.js'
import * as polygonClipping from 'polygon-clipping'
import LeafPart from './leaf-part'

export default class NodePart extends AbstractPart<UnitPlot | SubBlock | Block | Experiment | Platform> implements IParentObserver {
  private _childrenParts: AbstractPart<any>[]

  constructor(
      subject: UnitPlot | SubBlock | Block | Experiment | Platform,
      protected graphicalConfiguration: GraphicalConfiguration,
      public objectType: PathLevelEnum,
      protected graphicalContext: GraphicalContext,
      public extraBreadcrumbLabels: any = {},
  ) {
    super()
    this.subject = subject
    this.resetColor()
    this.drawingObject = new PIXI.Graphics()
    this._text = new PIXI.Text( '' )
    this.updateText()
    this._text.anchor.set( 0.5, 0.5 )
    this._container = new PIXI.Container()
    this.container.addChild( this.drawingObject, this._text )
  }

  public paint() {
    this.drawingObject.clear()
    // @ts-ignore
    if (this._childrenParts.filter( item => item.polygon.length > 0 ).length > 0) {
      // @ts-ignore
      this.polygon = polygonClipping.union( ...this._childrenParts.filter( item => item.polygon.length > 0 ).map( item => item.polygon ) )
      this.points = []
      this.polygon.forEach( poly => {
        poly[0].pop() // needed because last point equals 1st and cause shrink to delete them
        this.points.push( this.graphicalConfiguration.getShrinkedPolygon(
            this.graphicalConfiguration.getOffsetForType( this.objectType ),
            poly[0].map( pair => new PIXI.Point( pair[0], pair[1] ) ).reverse(),
        ) )
      } )
      this.origin = this.points[0][0]
      this.moveTo()
    }
  }

  protected getDragDX( newXPosition: number ): number {
    return Math.floor( (newXPosition + this.draggingRelativeOrigin.x - this.draggingOrigin.x + this.graphicalConfiguration.getOffsetForType( this.objectType )) / this.graphicalContext.cellW )
  }

  protected getDragDY( newYPosition: number ): number {
    return Math.floor( (newYPosition + this.draggingRelativeOrigin.y - this.draggingOrigin.y + this.graphicalConfiguration.getOffsetForType( this.objectType )) / this.graphicalContext.cellH )
  }

  moveSubject( dx: number, dy: number ) {
    this._childrenParts.forEach( ( child: LeafPart | NodePart ) => {
      child.moveSubject( dx, dy )
      this.paint()
    } )
  }

  canMove( dx: number, dy: number ): boolean {
    return this._childrenParts.every( ( child: LeafPart | NodePart ) => child.canMove( dx, dy ) )
  }

  updateMoveEndState( visibleBound: PIXI.Rectangle ): void {
    this.inViewport = this.graphicalContext.mode === GraphicalEditorModeEnum.PLACE_EXPERIMENT || this.container.parentLayer.renderable && !!this.polygon &&
        polygonClipping.intersection( this.polygon, [[
          [
            (this._graphicalOrigin === GraphicalOriginEnum.TOP_RIGHT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldW - visibleBound.x : visibleBound.x,
            (this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_LEFT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldH - visibleBound.y : visibleBound.y,
          ],
          [
            (this._graphicalOrigin === GraphicalOriginEnum.TOP_RIGHT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldW - visibleBound.x : visibleBound.x,
            (this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_LEFT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldH - (visibleBound.y + visibleBound.height) : visibleBound.y + visibleBound.height,
          ],
          [
            (this._graphicalOrigin === GraphicalOriginEnum.TOP_RIGHT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldW - (visibleBound.x + visibleBound.width) : visibleBound.x + visibleBound.width,
            (this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_LEFT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldH - (visibleBound.y + visibleBound.height) : visibleBound.y + visibleBound.height,
          ],
          [
            (this._graphicalOrigin === GraphicalOriginEnum.TOP_RIGHT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldW - (visibleBound.x + visibleBound.width) : visibleBound.x + visibleBound.width,
            (this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_LEFT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.worldH - visibleBound.y : visibleBound.y,
          ]],
        ] ).length > 0
  }

  updateChildState(): void {
    this.paint()
    this.notifyParentRedraw()
  }

  addChildPart( ...parts: AbstractPart<any>[] ) {
    const partsToAdd = parts.filter( part => this._childrenParts.every( existingPart => existingPart !== part ) )
    partsToAdd.forEach( part => {
      this._childrenParts.push( part )
      part.parentPart = this
      part.renderIn( this.container )
    } )
    if (partsToAdd.length > 0) {
      this.paint()
    }
  }

  set childrenParts( value: AbstractPart<any>[] ) {
    if (!!this._childrenParts) {
      this._childrenParts.forEach( ( part: LeafPart | NodePart ): void => part.parentPart = null )
    }
    this._childrenParts = value.filter( part => !!part.polygon )
    this._childrenParts.forEach( ( part: LeafPart | NodePart ) => part.parentPart = this )
    this.container.removeChildren()
    this.container.addChild( this.drawingObject, this._text, ...this._childrenParts.map( part => part.container ) )
    this.paint()
  }

  get childrenParts(): AbstractPart<any>[] {
    return this._childrenParts
  }

  protected set visible( visible: boolean ) {
    if (!!this.polygon) {
      this.container.interactiveChildren =
          this.drawingObject.interactive =
              this.container.visible =
                  this.container.renderable = visible
    }
  }

  resetColor(): void {
    this.color = this.subject.color
  }

  get gridX(): number {
    return min( this._childrenParts.map( child => child.gridX ) )
  }

  get gridY(): number {
    return min( this._childrenParts.map( child => child.gridY ) )
  }
}
