import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import TextureEnum from '@adonis/webapp/constants/texture-enum'
import TextureImgEnum from '@adonis/webapp/constants/texture-img-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import AbstractPart from '@adonis/webapp/models/graphics/business-objects/abstract-part'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import PositionTreeNode from '@adonis/webapp/models/graphics/position-tree/position-tree-node'
import { Individual } from '@adonis/webapp/models/individual'
import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import * as PIXI from 'pixi.js'

export default class LeafPart extends AbstractPart<Individual | SurfacicUnitPlot | OutExperimentationZone> {
  positionTree: PositionTreeNode
  selectedLeafsIri: string[]

  constructor(
      subject: Individual | SurfacicUnitPlot | OutExperimentationZone,
      protected graphicalConfiguration: GraphicalConfiguration,
      public objectType: PathLevelEnum,
      protected graphicalContext: GraphicalContext,
      public extraBreadcrumbLabels: any = {},
  ) {
    super()
    this.subject = subject
    this.resetColor()
    this.drawingObject = new PIXI.Graphics()
    this.drawingObject.interactive = false
    const offsetForType = this.graphicalConfiguration.getOffsetForType( this.subject.pathLevel )
    this.points = [[
      new PIXI.Point( this.graphicalContext.cellW * this.subject.x + offsetForType, this.graphicalContext.cellH * this.subject.y + offsetForType ),
      new PIXI.Point( this.graphicalContext.cellW * (this.subject.x + 1) - offsetForType, this.graphicalContext.cellH * this.subject.y + offsetForType ),
      new PIXI.Point( this.graphicalContext.cellW * (this.subject.x + 1) - offsetForType, this.graphicalContext.cellH * (this.subject.y + 1) - offsetForType ),
      new PIXI.Point( this.graphicalContext.cellW * this.subject.x + offsetForType, this.graphicalContext.cellH * (this.subject.y + 1) - offsetForType ),
    ]]
    this._text = new PIXI.Text( '' )
    this._text.anchor.set( 0.5, 0.5 )
    this.dragging = false
    this.startSelect = false
    this._container = new PIXI.Container()
    this.container.addChild( this.drawingObject, this._text )
    if (subject instanceof Individual || subject instanceof SurfacicUnitPlot) {
      this.updateText()
    } else {
      if (subject.texture !== null && subject.texture !== undefined) {
        // @ts-ignore
        this._texture = PIXI.Sprite.from( LeafPart.textureSwitch( subject.texture ) )
        this._texture.anchor.set( 0.5, 0.5 )
        this._texture.width = graphicalContext.cellW / 2
        this._texture.height = graphicalContext.cellH / 2
        this._container.addChild( this._texture )
      }

    }
    this.paint()
  }

  public paint() {
    this.polygon = [[
      [this.graphicalContext.cellW * this.subject.x, this.graphicalContext.cellH * this.subject.y],
      [this.graphicalContext.cellW * (this.subject.x + 1), this.graphicalContext.cellH * this.subject.y],
      [this.graphicalContext.cellW * (this.subject.x + 1), this.graphicalContext.cellH * (this.subject.y + 1)],
      [this.graphicalContext.cellW * this.subject.x, this.graphicalContext.cellH * (this.subject.y + 1)],
    ]]
    this.origin = this.points[0][0]
    const offsetForType = this.graphicalConfiguration.getOffsetForType( this.objectType )
    this.moveTo( this.graphicalContext.cellW * this.subject.x + offsetForType, this.graphicalContext.cellH * this.subject.y + offsetForType, 1 )

  }

  protected getDragDX( newXPosition: number ): number {
    return Math.floor( newXPosition / this.graphicalContext.cellW ) - this.subject.x
  }

  protected getDragDY( newYPosition: number ): number {
    return Math.floor( newYPosition / this.graphicalContext.cellH ) - this.subject.y
  }

  updateMoveEndState( visibleBound: PIXI.Rectangle ): void {
    this.inViewport = this.container.parentLayer.renderable &&
        visibleBound.contains(
            (this._graphicalOrigin === GraphicalOriginEnum.TOP_RIGHT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.cellW + this.graphicalContext.worldW - this.origin.x :
                this.origin.x,
            (this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_LEFT || this._graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
                this.graphicalContext.cellH + this.graphicalContext.worldH - this.origin.y :
                this.origin.y,
        )
  }

  moveSubject( dx: number, dy: number ) {
    if (dx !== 0 || dy !== 0) {
      this.positionTree.deletePart( this.subject.x, this.subject.y )
      this.positionTree.addPart( this.subject.x += dx, this.subject.y += dy, this )
    }
    this.paint()
    this.notifyParentRedraw()
  }

  canMove( dx: number, dy: number ): boolean {
    const posX = this.subject.x + dx
    const posY = this.subject.y + dy
    const leaf = this.positionTree.getNode( posX, posY )?.part
    return posX > 0 && posY > 0 && (!leaf || this.selectedLeafsIri.some( item => item === leaf.subject.iri ))
  }

  set selected( selected: boolean ) {
    if (selected !== this._selected) {
      this._selected = selected
      this.paint()
    }
  }

  set dead( dead: boolean ) {
    if (!(this.subject instanceof OutExperimentationZone)) {
      this.subject.dead = dead
      this.updateText()
      this.resetColor()
      this.moveTo()
    }
  }

  protected set visible( visible: boolean ) {
    this.drawingObject.interactive = this.container.visible = this.container.renderable = visible
  }

  private static textureSwitch( texture: TextureEnum ): TextureImgEnum {
    switch (texture) {
      case TextureEnum.TEXTURE1:
        return TextureImgEnum.TEXTURE1
      case TextureEnum.TEXTURE2:
        return TextureImgEnum.TEXTURE2
      case TextureEnum.TEXTURE3:
        return TextureImgEnum.TEXTURE3
      case TextureEnum.TEXTURE4:
        return TextureImgEnum.TEXTURE4
      case TextureEnum.TEXTURE5:
        return TextureImgEnum.TEXTURE5
      case TextureEnum.TEXTURE6:
        return TextureImgEnum.TEXTURE6
      case TextureEnum.TEXTURE7:
        return TextureImgEnum.TEXTURE7
      case TextureEnum.TEXTURE8:
        return TextureImgEnum.TEXTURE8
      case TextureEnum.TEXTURE9:
        return TextureImgEnum.TEXTURE9
    }
  }

  resetColor(): void {
    this.color = ((this.subject instanceof Individual || this.subject instanceof SurfacicUnitPlot) && this.subject.dead) ?
        GraphicalConfiguration.defaultDeadItemColor : this.subject.color
  }

  get gridX(): number {
    return this.subject.x
  }

  get gridY(): number {
    return this.subject.y
  }
}
