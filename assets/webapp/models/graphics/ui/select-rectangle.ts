import * as PIXI from 'pixi.js'

export default class SelectRectangle extends PIXI.Graphics {

  private startX: number
  private startY: number

  constructor() {
    super()
  }

  private draw( x1: number, x2: number, y1: number, y2: number, color: number ) {
    this.clear()
    this.lineStyle( 4, color, 1)
    this.beginFill(color, 0.3)
    this.drawRect( x1, y1, x2 - x1, y2 - y1 )
    this.endFill()
  }

  public setStartPoint( x: number, y: number ) {
    this.startX = x
    this.startY = y
  }

  public setOppositeCorner( x: number, y: number, color: number ) {
    this.draw( this.startX, x, this.startY, y, color )
  }

}
