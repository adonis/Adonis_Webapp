import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IDraggingObserver from '@adonis/webapp/models/graphics/interfaces/i-dragging-observer'
import IMaskMovedObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moved-observer'
import IMaskMovingObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moving-observer'
import IMaskSubject from '@adonis/webapp/models/graphics/interfaces/i-mask-subject'
import IRightClickObserver from '@adonis/webapp/models/graphics/interfaces/i-right-click-observer'
import IRightClickSubject from '@adonis/webapp/models/graphics/interfaces/i-right-click-subject'
import { IViewportOptions, Viewport } from 'pixi-viewport'
import * as PIXI from 'pixi.js'

export default class DragableViewport extends Viewport implements IDraggingObserver, IMaskSubject, IRightClickSubject {

  private movingObservers: IMaskMovingObserver[] = []
  private movedObservers: IMaskMovedObserver[] = []
  private rightClickObservers: IRightClickObserver[] = []

  private _origin: GraphicalOriginEnum

  private draggingObject = false

  private selecting: boolean
  private dragStartX: number
  private dragStartY: number

  private _dragStartCallback: ( x: number, y: number ) => void = () => {
  }

  private _dragMoveCallback: ( x: number, y: number, y1: number, y2: number ) => void = () => {
  }

  private _dragEndCallback: ( x: number, y: number, y1: number, y2: number ) => void = () => {
  }

  private _zoomedEndCallback: ( scale: number ) => void = () => {
  }

  constructor(
      private graphicalConfiguration: GraphicalConfiguration,
      private graphicalContext: GraphicalContext,
      screenWidth: number,
      screenHeight: number,
      options?: IViewportOptions,
  ) {
    super( {
      ...options,
      worldWidth: graphicalContext.worldW,
      worldHeight: graphicalContext.worldH,
      screenWidth,
      screenHeight,
      disableOnContextMenu: true,
    } )
    this.on( 'zoomed', () => this.onZoomEnd() )
    this.on( 'moved', () => this.onMove() )
    this.on( 'moved-end', () => this.onMoveEnd() )
    this.on( 'pointerdown', event => this.onDragStart( event ) )
    this.on( 'pointermove', event => this.onDragMove( event ) )
    this.on( 'pointerup', event => this.onDragEnd( event ) )
    this.drag( {
      mouseButtons: 'right',
    } )
        .wheel( {
          smooth: 10,
        } )
        .clampZoom( {
          minWidth: graphicalConfiguration.minZoomW,
          minHeight: graphicalConfiguration.minZoomH,
        } )
  }

  updateBoundaries() {
    this.worldWidth = this.graphicalContext.worldW + this.graphicalContext.cellW
    this.worldHeight = this.graphicalContext.worldH +  this.graphicalContext.cellH
    this.bounce( {
      bounceBox: new PIXI.Rectangle(
          0,
          0,
          this.worldWidth + 10,
          this.worldHeight + 10,
      ),
      underflow: this._origin === GraphicalOriginEnum.BOTTOM_RIGHT ? 'bottom-right' :
          this._origin === GraphicalOriginEnum.BOTTOM_LEFT ? 'bottom-left' :
              this._origin === GraphicalOriginEnum.TOP_RIGHT ? 'top-right' : 'top-left',
    } )
  }

  updateDraggingState( isDraging: boolean ): void {
    this.draggingObject = isDraging
    // Useless since dragging is done by right click
    /*this.drag( {
      pressDrag: !isDraging,
    } )*/
  }

  subscribeMovingMask( observer: IMaskMovingObserver ) {
    this.movingObservers.push( observer )
  }

  unsubscribeMovingMask( observer: IMaskMovingObserver ) {
    this.movingObservers = this.movingObservers.filter( ( element ) => {
      return observer !== element
    } )
  }

  subscribeMovedMask( observer: IMaskMovedObserver ) {
    this.movedObservers.push( observer )
  }

  unsubscribeMovedMask( observer: IMaskMovedObserver ) {
    this.movedObservers = this.movedObservers.filter( ( element ) => {
      return observer !== element
    } )
  }

  resetMaskObservers() {
    this.movingObservers = []
    this.movedObservers = []
  }

  notifyMoveEndState() {
    this.movedObservers.forEach( observer => {
      observer.updateMoveEndState( this.getVisibleBounds() )
    } )
  }

  notifyMovingState() {
    this.movingObservers.forEach( observer => {
      observer.updateMovingState( this.getVisibleBounds() )
    } )
  }

  notifyRightClickState( x: number, y: number, showObject: boolean ): void {
    this.rightClickObservers.forEach( observer => {
      observer.updateRightClickState( x, y, showObject, null )
    } )
  }

  subscribeRightClick( observer: IRightClickObserver ): void {
    this.rightClickObservers.push( observer )
  }

  unsubscribeRightClick( observer: IRightClickObserver ): void {
    this.rightClickObservers = this.rightClickObservers.filter( ( element ) => {
      return observer !== element
    } )
  }

  onMoveEnd() {
    this.notifyMoveEndState()
    this.notifyRightClickState( 0, 0, null )
  }

  onMove() {
    this.notifyMovingState()
    this.notifyRightClickState( 0, 0, null )
  }

  onDragStart( event: PIXI.InteractionEvent ) {
    if (!this.graphicalContext.isCopying && !this.draggingObject && event.data.button === 0) {
      this.selecting = true
      const pointerPos = event.data.getLocalPosition( this )
      this.dragStartX = pointerPos.x
      this.dragStartY = pointerPos.y
      this._dragStartCallback(
          (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldWidth - pointerPos.x : pointerPos.x,
          (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldHeight - pointerPos.y : pointerPos.y,
      )
    }
  }

  onDragMove( event: PIXI.InteractionEvent ) {
    if (this.selecting) {
      const pointerPos = event.data.getLocalPosition( this )
      this._dragMoveCallback(
          (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldWidth - this.dragStartX : this.dragStartX,
          (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldWidth - pointerPos.x : pointerPos.x,
          (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldHeight - this.dragStartY : this.dragStartY,
          (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldHeight - pointerPos.y : pointerPos.y,
      )
    }
  }

  onDragEnd( event: PIXI.InteractionEvent ) {
    if (this.selecting) {
      this.selecting = false
      const pointerPos = event.data.getLocalPosition( this )
      this._dragEndCallback(
          (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldWidth - this.dragStartX : this.dragStartX,
          (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldWidth - pointerPos.x : pointerPos.x,
          (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldHeight - this.dragStartY : this.dragStartY,
          (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
              this.worldHeight - pointerPos.y : pointerPos.y,
      )
      this.notifyRightClickState( 0, 0, null )
    }
  }

  onZoomEnd() {
    this._zoomedEndCallback( this.scaled )
  }

  set dragStartCallback( value: ( x: number, y: number ) => void ) {
    this._dragStartCallback = value
  }

  set dragMoveCallback( value: ( x: number, y: number, y1: number, y2: number ) => void ) {
    this._dragMoveCallback = value
  }

  set dragEndCallback( value: ( x: number, y: number, y1: number, y2: number ) => void ) {
    this._dragEndCallback = value
  }

  set zoomedEndCallback( value: ( scale: number ) => void ) {
    this._zoomedEndCallback = value
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
  }
}
