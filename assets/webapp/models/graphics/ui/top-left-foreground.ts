// @ts-ignore
import topLeft from '@adonis/shared/images/graphics/topLeft.svg'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IMaskMovingObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moving-observer'
import * as PIXI from 'pixi.js'

export default class TopLeftForeground extends PIXI.Container implements IMaskMovingObserver {

  private topShift = 0
  private leftShift = 0
  private _origin: GraphicalOriginEnum
  private sprite: PIXI.Sprite

  constructor( private graphicalContext: GraphicalContext ) {
    super()
    this.sprite = PIXI.Sprite.from( topLeft )
    this.sprite.anchor.set( 0.5, 0.5 )
    this.addChild( this.sprite )
    this.paint()
  }

  private paint() {
    this.y = this.topShift
    this.x = this.leftShift
  }

  updateBoundaries() {
    switch (this._origin) {
      case GraphicalOriginEnum.TOP_RIGHT:
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this.sprite.width = this.graphicalContext.cellH / 2
        this.sprite.height = this.graphicalContext.cellW / 2
        this.sprite.x = this.sprite.height / 2
        this.sprite.y = this.sprite.width / 2
        break
      default:
        this.sprite.width = this.graphicalContext.cellW / 2
        this.sprite.height = this.graphicalContext.cellH / 2
        this.sprite.x = this.sprite.width / 2
        this.sprite.y = this.sprite.height / 2
    }

  }

  updateMovingState( visibleBound: PIXI.Rectangle ): void {
    this.topShift = (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
        visibleBound.y + visibleBound.height - (this.graphicalContext.cellH / 2) : visibleBound.y
    this.leftShift = (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
        visibleBound.x + visibleBound.width - (this.graphicalContext.cellW / 2) : visibleBound.x
    this.paint()
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
    switch (this._origin) {
      case GraphicalOriginEnum.TOP_RIGHT:
        this.sprite.rotation = Math.PI / 2
        break
      case GraphicalOriginEnum.BOTTOM_RIGHT:
        this.sprite.rotation = Math.PI
        break
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this.sprite.rotation = -Math.PI / 2
        break
      default:
        this.sprite.rotation = 0
        break
    }
  }
}
