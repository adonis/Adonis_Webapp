// @ts-ignore
import northIndicatorImg from '@adonis/shared/images/graphics/northIndicator.png'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import MovableGraphicObject from '@adonis/webapp/models/graphics/ui/movable-graphic-object'
import '@pixi/graphics-extras'
import * as PIXI from 'pixi.js'

export default class NorthIndicatorGraphic extends MovableGraphicObject {

  private sprite: PIXI.Sprite
  private _graphicalOrigin: GraphicalOriginEnum

  constructor(
      graphicalContext: GraphicalContext,
      xGrid: number,
      yGrid: number,
      width: number,
      height: number,
      orientation: number ) {
    super(graphicalContext, xGrid, yGrid)
    this.sprite = PIXI.Sprite.from( northIndicatorImg )
    this.sprite.position.set( graphicalContext.cellW / 2, graphicalContext.cellH / 2 )
    this.sprite.anchor.set( 0.5, 0.5 )
    this.sprite.width = width
    this.sprite.height = height
    this.sprite.angle = orientation
    this.addChild( this.sprite )
  }

  set graphicalOrigin( value: GraphicalOriginEnum ) {
    this._graphicalOrigin = value
    switch (this._graphicalOrigin) {
      case GraphicalOriginEnum.TOP_LEFT:
        this.scale.set( 1, 1 )
        break
      case GraphicalOriginEnum.TOP_RIGHT:
        this.scale.set( -1, 1 )
        break
      case GraphicalOriginEnum.BOTTOM_RIGHT:
        this.scale.set( -1, -1 )
        break
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this.scale.set( 1, -1 )
        break
    }
  }


}
