import ApiEntity from '@adonis/shared/models/api-entity'
import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IRightClickObserver from '@adonis/webapp/models/graphics/interfaces/i-right-click-observer'
import { Individual } from '@adonis/webapp/models/individual'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import * as PIXI from 'pixi.js'

export default class ContextMenu extends PIXI.Container implements IRightClickObserver {

  public originX: number
  public originY: number
  // If null, the context will not be shown
  public objectsToShow: HasPathLevel[]
  private _origin: GraphicalOriginEnum

  private static CONTEXT_MIN_WIDTH = 200
  private static CONTEXT_MIN_HEIGH = 40

  constructor(
      private graphicalConfiguration: GraphicalConfiguration,
      private graphicalContext: GraphicalContext,
      private translateCallback: ( key: string ) => string,
      private deleteCallback: () => void,
      private editColorCallback: () => void,
      private declareDeadCallback: ( objectToDelete: ApiEntity ) => void,
      private replantCallback: ( objectToDelete: ApiEntity ) => void,
      private copyCallback: () => void,
      private pasteCallback: () => void,
  ) {
    super()
    this.paint()
  }

  private paint() {
    this.removeChildren()
    if (!!this.objectsToShow && this.graphicalContext.mode === GraphicalEditorModeEnum.EDIT) {
      const style = new PIXI.TextStyle( {
        fontSize: 40,
      } )

      const background = new PIXI.Graphics()
      this.addChild( background )

      let height = 0
      let width = 0
      const graphicsTab: {
        graphics: PIXI.Graphics,
        startY: number,
        textHeight: number,
        action: () => void,
      }[] = []
      this.contextMenuItems.forEach( item => {
        const text = new PIXI.Text( this.translateCallback( item.text ), style )
        text.y = height
        text.x = 0
        const graphics = new PIXI.Graphics()
        graphics.interactive = true
        this.addChild( graphics )
        this.addChild( text )
        graphicsTab.push( {
          graphics,
          startY: height,
          textHeight: text.height,
          action: item.action,
        } )
        height += text.height
        width = text.width > width ? text.width : width
      } )

      graphicsTab.forEach( item => {
        const draw = ( color: number ) => {
          item.graphics.beginFill( color )
          item.graphics.drawRect( 0, item.startY, width, item.textHeight )
          item.graphics.endFill()
        }
        draw( 0xececec )
        item.graphics.on( 'pointerover', () => draw( 0xdcdcdc ) )
        item.graphics.on( 'pointerout', () => draw( 0xececec ) )
        item.graphics.on( 'click', () => item.action() )
      } )

      height = height > 0 ? height : ContextMenu.CONTEXT_MIN_HEIGH
      width = width > 0 ? width : ContextMenu.CONTEXT_MIN_WIDTH

      background.lineStyle( { width: 2 } )
      background.drawRect( 0, 0, width, height )
    }
  }

  updateRightClickState(
      originX: number,
      originY: number,
      objectsToShow: any[],
  ): void {
    this.objectsToShow = objectsToShow
    this.paint()
    this.x = (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ? originX - this.width : originX
    this.y = (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ? originY - this.height : originY
  }

  get contextMenuItems(): {
    text: string,
    action: () => void,
  }[] {
    const options = [
      {
        text: 'commons.graphics.contextMenu.delete',
        action: () => this.deleteCallback(),
      },
      {
        text: 'commons.graphics.contextMenu.changeColor',
        action: () => this.editColorCallback(),
      },
    ]
    switch (this.objectsToShow[0].pathLevel) {
      case PathLevelEnum.SURFACE_UNIT_PLOT:
      case PathLevelEnum.INDIVIDUAL:
        if (this.objectsToShow.length === 1) {
          options.push( (this.objectsToShow[0] as (Individual | SurfacicUnitPlot)).dead ?
              {
                text: 'commons.graphics.contextMenu.replant',
                action: () => this.replantCallback( this.objectsToShow[0] ),
              } :
              {
                text: 'commons.graphics.contextMenu.declareDead',
                action: () => this.declareDeadCallback( this.objectsToShow[0] ),
              },
          )
        }
      case PathLevelEnum.UNIT_PLOT:
      case PathLevelEnum.BLOCK:
      case PathLevelEnum.SUB_BLOCK:
      case PathLevelEnum.EXPERIMENT:
      case PathLevelEnum.PLATFORM:
      default:
        if (this.objectsToShow.every( item => item.pathLevel === this.objectsToShow[0].pathLevel )) {
          options.push( {
            text: 'commons.graphics.contextMenu.copy',
            action: () => this.copyCallback(),
          } )
        }
        if (this.objectsToShow.length === 1) {
          options.push( {
            text: 'commons.graphics.contextMenu.paste',
            action: () => this.pasteCallback(),
          } )
        }
        return options
    }
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
    switch (this._origin) {
      case GraphicalOriginEnum.TOP_LEFT:
        this.scale.set( 1, 1 )
        break
      case GraphicalOriginEnum.TOP_RIGHT:
        this.scale.set( -1, 1 )
        break
      case GraphicalOriginEnum.BOTTOM_RIGHT:
        this.scale.set( -1, -1 )
        break
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this.scale.set( 1, -1 )
        break
    }
  }
}
