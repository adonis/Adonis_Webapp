// @ts-ignore
import star from '@adonis/shared/images/graphics/favorite1.png'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import '@pixi/graphics-extras'
import * as PIXI from 'pixi.js'

export default class PathStartIndicator extends PIXI.Container {

  private sprite: PIXI.Sprite

  constructor( graphicalContext: GraphicalContext, x: number, y: number ) {
    super()
    this.sprite = PIXI.Sprite.from( star )
    this.sprite.position.set( x, y )
    this.sprite.anchor.set( 0.5, 0.5 )
    this.sprite.width = graphicalContext.cellW / 2
    this.sprite.height = graphicalContext.cellH / 2
    this.addChild( this.sprite )
  }

}
