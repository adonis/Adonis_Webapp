// @ts-ignore
import barBackground from '@adonis/shared/images/graphics/barBackground.svg'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IMaskMovingObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moving-observer'
import * as PIXI from 'pixi.js'

export default class BarBackground extends PIXI.Container implements IMaskMovingObserver {

    private topShift = 0
    private leftShift = 0
    private visibleWidth = 0
    private visibleHeight = 0
    private topSprite: PIXI.Sprite
    private bottomSprite: PIXI.Sprite
    private leftSprite: PIXI.Sprite
    private rightSprite: PIXI.Sprite
    private _origin: GraphicalOriginEnum

    constructor(private graphicalContext: GraphicalContext) {
        super()

        this.leftSprite = PIXI.Sprite.from(barBackground)
        this.addChild(this.leftSprite)
        this.topSprite = PIXI.Sprite.from(barBackground)
        this.addChild(this.topSprite)

        this.rightSprite = PIXI.Sprite.from(barBackground)
        this.addChild(this.rightSprite)
        this.bottomSprite = PIXI.Sprite.from(barBackground)
        this.addChild(this.bottomSprite)

        this.paint()
    }

    private paint() {
        this.y = this.topShift
        this.x = this.leftShift
        this.bottomSprite.width = this.topSprite.width = this.visibleWidth
        this.rightSprite.height = this.leftSprite.height = this.visibleHeight
    }

    updateBoundaries() {
        this.rightSprite.width = this.leftSprite.width = this.graphicalContext.cellW / 2
        this.bottomSprite.height = this.topSprite.height = this.graphicalContext.cellH / 2
        this.visibleWidth = this.graphicalContext.worldW
        this.visibleHeight = this.graphicalContext.worldH
    }

    updateMovingState( visibleBound: PIXI.Rectangle ): void {
        this.topShift = visibleBound.y
        this.leftShift = visibleBound.x
        this.leftSprite.x = (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
            visibleBound.width - (this.graphicalContext.cellW / 2)  : 0
        this.rightSprite.x = !(this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
            visibleBound.width - (this.graphicalContext.cellW / 2)  : 0
        this.topSprite.y = (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
            visibleBound.height - (this.graphicalContext.cellH / 2)  : 0
        this.bottomSprite.y = !(this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
            visibleBound.height - (this.graphicalContext.cellH / 2)  : 0
        this.visibleWidth = visibleBound.width
        this.visibleHeight = visibleBound.height
        this.paint()
    }

    set origin( value: GraphicalOriginEnum ) {
        this._origin = value
    }
}
