import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IDraggingObserver from '@adonis/webapp/models/graphics/interfaces/i-dragging-observer'
import IDraggingSubject from '@adonis/webapp/models/graphics/interfaces/i-dragging-subject'
import '@pixi/graphics-extras'
import * as PIXI from 'pixi.js'

export default abstract class MovableGraphicObject extends PIXI.Container implements IDraggingSubject {

  private observers: IDraggingObserver[] = []
  private _selectCallback: () => void
  private dragStartPosition: { x: number, y: number }
  private _dragEndCallback: ( dx: number, dy: number ) => void
  private doubleClickIndicator: boolean
  private _doubleClickCallback: () => void

  protected constructor(
      protected graphicalContext: GraphicalContext,
      protected xGrid: number,
      protected yGrid: number,
  ) {
    super()
    this.position.set( this.xGrid * this.graphicalContext.cellW, this.yGrid * this.graphicalContext.cellH )
    this.interactive = true
    this.on( 'mousedown', event => this.onDragStart( event ) )
  }

  set selectCallback( value: () => void ) {
    this._selectCallback = value
    this.on( 'pointerdown', this._selectCallback )
  }

  set dragEndCallback( value: ( dx: number, dy: number ) => void ) {
    this._dragEndCallback = value
  }

  set doubleClickCallback( value: () => void ) {
    this._doubleClickCallback = value
  }

  protected onDragStart( event: PIXI.InteractionEvent ) {
    if (this.graphicalContext.mode === GraphicalEditorModeEnum.EDIT && !this.graphicalContext.isCopying) {
      if (this.doubleClickIndicator) {
        this._doubleClickCallback()
      }
      this.doubleClickIndicator = true
      setTimeout( () => {
        this.doubleClickIndicator = false
      }, 500 )

      this.dragStartPosition = {
        x: event.data.getLocalPosition( this.parent ).x,
        y: event.data.getLocalPosition( this.parent ).y,
      }
      this.alpha = 0.5
      this.on( 'pointermove', e => this.onDragMove( e ) )
      this.on( 'mouseup', e => this.onDragEnd( e ) )
      this.on( 'pointerupoutside', e => this.onDragEnd( e ) )
      this.notifyDraggingState( true )
    }
  }

  protected onDragMove( event: PIXI.InteractionEvent ) {
    this.position.set(
        this.xGrid * this.graphicalContext.cellW + event.data.getLocalPosition( this.parent ).x - this.dragStartPosition.x,
        this.yGrid * this.graphicalContext.cellH + event.data.getLocalPosition( this.parent ).y - this.dragStartPosition.y,
    )
  }

  protected onDragEnd( event: PIXI.InteractionEvent ) {
    this.alpha = 1
    const dx = Math.floor( (event.data.getLocalPosition( this.parent ).x - this.dragStartPosition.x) / this.graphicalContext.cellW )
    const dy = Math.floor( (event.data.getLocalPosition( this.parent ).y - this.dragStartPosition.y) / this.graphicalContext.cellH )

    this.removeAllListeners( 'pointermove' )
    this.removeAllListeners( 'mouseup' )
    this.removeAllListeners( 'pointerupoutside' )
    this.notifyDraggingState( false )
    if (dx !== 0 || dy !== 0) {
      this._dragEndCallback( dx, dy )
    } else {
      this.position.set( this.xGrid * this.graphicalContext.cellW, this.yGrid * this.graphicalContext.cellH )
    }
  }

  subscribeDrag( observer: IDraggingObserver ) {
    this.observers.push( observer )
  }

  unsubscribeDrag( observer: IDraggingObserver ) {
    this.observers = this.observers.filter( ( element ) => {
      return observer !== element
    } )
  }

  notifyDraggingState( isDraging: boolean ) {
    this.observers.forEach( observer => {
      observer.updateDraggingState( isDraging )
    } )
  }

}
