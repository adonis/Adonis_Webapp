import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import MovableGraphicObject from '@adonis/webapp/models/graphics/ui/movable-graphic-object'
import '@pixi/graphics-extras'
import { max } from 'mathjs'
import * as PIXI from 'pixi.js'

export default class TextZoneGraphic extends MovableGraphicObject {

  private graphics: PIXI.Graphics
  private text: PIXI.Text
  private _graphicalOrigin: GraphicalOriginEnum

  constructor(
      graphicalContext: GraphicalContext,
      xGrid: number,
      yGrid: number,
      width: number,
      height: number,
      text: string,
      size: number,
      color?: number,
  ) {
    super(graphicalContext, xGrid, yGrid)
    this.graphics = new PIXI.Graphics()
    this.graphics.lineStyle( 4, 0, 1 )
    this.graphics.beginFill( color ?? 0xffffff, 0.95 )
    this.graphics.drawRoundedRect( 0, 0, width, height, 10 )

    this.text = new PIXI.Text( text, new PIXI.TextStyle( {
      fontFamily: 'Arial',
      fontSize: size,
      wordWrap: true,
      wordWrapWidth: max( width - 10, 100 ),
      lineJoin: 'round',
    } ) )
    this.text.anchor.set( 0.5, 0.5 )
    this.text.position.set( width / 2, height / 2 )

    this.addChild( this.graphics, this.text )
  }

  set graphicalOrigin( value: GraphicalOriginEnum ) {
    this._graphicalOrigin = value
    switch (this._graphicalOrigin) {
      case GraphicalOriginEnum.TOP_LEFT:
        this.text.scale.set( 1, 1 )
        break
      case GraphicalOriginEnum.TOP_RIGHT:
        this.text.scale.set( -1, 1 )
        break
      case GraphicalOriginEnum.BOTTOM_RIGHT:
        this.text.scale.set( -1, -1 )
        break
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this.text.scale.set( 1, -1 )
        break
    }
  }

}
