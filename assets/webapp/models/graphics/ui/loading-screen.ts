// @ts-ignore
import barBackground from '@adonis/shared/images/graphics/barBackground.svg'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import * as PIXI from 'pixi.js'

export default class LoadingScreen extends PIXI.Container {

  private _loading: boolean
  private graphics: PIXI.Graphics
  private loadingBar: PIXI.Graphics
  private text: PIXI.Text
  private _theoreticalWaiting: number = null
  private _requestStartsAt: number = null

  constructor(
      private _screenWidth: number,
      private _screenHeight: number,
      private ticker: PIXI.Ticker,
  ) {
    super()
    this._loading = false
    this.graphics = new PIXI.Graphics()
    this.loadingBar = new PIXI.Graphics()

    const style = new PIXI.TextStyle( {
      fontSize: 40,
      fill: ['#ffffff'],
    } )
    this.text = new PIXI.Text( '', style )
    this.text.anchor.set( 0.5, 0.5 )
    this.text.x = _screenWidth / 2
    this.text.y = _screenHeight / 2 - 30

    let totalElapsed = 0
    this.ticker.add( elapsed => {
      this.visible = this._loading
      this.loadingBar.visible = !!this._theoreticalWaiting
      if (!!this._theoreticalWaiting) {
        this.text.x = _screenWidth / 2
        this.text.y = _screenHeight / 2 - 30
        const percentage = Math.min( 99, Math.floor( (1 - (this._theoreticalWaiting - (performance.now() - this._requestStartsAt)) / this._theoreticalWaiting) * 100 ) )
        this.loadingBar.clear()
        this.loadingBar.lineStyle( 2, 0xffffff )
        this.loadingBar.drawRect( this._screenWidth / 2 - 100, this._screenHeight / 2 + 10, 200, 20 )
        this.loadingBar.beginFill( 0xffffff, 0.8 )
        this.loadingBar.drawRect( this._screenWidth / 2 - 100, this._screenHeight / 2 + 10, percentage * 2, 20 )
        this.loadingBar.endFill()
        if (totalElapsed === 0) {
          this.text.text = percentage + '%'
        }
        totalElapsed += elapsed
        if (totalElapsed > 10) {
          totalElapsed = 0
        }
      } else {
        this.text.text = ''
      }
    } )
    this.addChild( this.graphics )
    this.addChild( this.loadingBar )
    this.addChild( this.text )
    this.paint()
  }

  private paint() {
    this.graphics.clear()
    this.graphics.beginFill( GraphicalConfiguration.defaultLoadingBackgroundColor, 0.5 )
    this.graphics.drawRect( 0, 0, this._screenWidth, this._screenHeight )
    this.graphics.endFill()
  }

  set loading( value: boolean ) {
    this._loading = value
  }

  set screenWidth( value: number ) {
    this._screenWidth = value
    this.paint()
  }

  set screenHeight( value: number ) {
    this._screenHeight = value
    this.paint()
  }

  set theoreticalWaiting( value: number ) {
    this._theoreticalWaiting = value
  }

  set requestStartsAt( value: number ) {
    this._requestStartsAt = value
  }
}
