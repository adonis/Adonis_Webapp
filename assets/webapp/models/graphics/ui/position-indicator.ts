// @ts-ignore
import positionIndicatorImg from '@adonis/shared/images/graphics/positionIndicator.png'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import '@pixi/graphics-extras'
import * as PIXI from 'pixi.js'

export default class PositionIndicator extends PIXI.Container {

  private sprite: PIXI.Sprite

  constructor(
      graphicalContext: GraphicalContext,
  ) {
    super()
    this.sprite = PIXI.Sprite.from( positionIndicatorImg )
    this.sprite.position.set( graphicalContext.cellW / 2, graphicalContext.cellH / 2 )
    this.sprite.anchor.set( 0.5, 0.5 )
    this.sprite.width = graphicalContext.cellW / 5
    this.sprite.height = graphicalContext.cellH / 5
    this.addChild( this.sprite )
    this.visible = false
  }
}
