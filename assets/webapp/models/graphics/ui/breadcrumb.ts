import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IHoverObserver from '@adonis/webapp/models/graphics/interfaces/i-hover-observer'
import * as PIXI from 'pixi.js'

export default class Breadcrumb extends PIXI.Graphics implements IHoverObserver {

  public originX: number
  public originY: number
  public objectToShow: any
  public objectPathLevel: PathLevelEnum
  private timer: any
  private _origin: GraphicalOriginEnum
  private text: PIXI.Text

  constructor(
      private graphicalConfiguration: GraphicalConfiguration,
      private graphicalContext: GraphicalContext,
  ) {
    super()
    const style = new PIXI.TextStyle( {
      fontSize: 40,
    } )
    this.text = new PIXI.Text( '', style )
    this.text.anchor.set( 0.5 )
    this.paint()
  }

  private paint() {
    this.clear()
    this.removeChildren()
    if (!!this.objectToShow && !!this.objectPathLevel) {
      this.text.text = this.graphicalConfiguration.getBreadcrumbText( this.objectPathLevel, this.objectToShow )
      this.text.y = this.originY + this.graphicalContext.cellH
      this.text.x = this.originX
      this.addChild( this.text )
      const points = [
        new PIXI.Point( this.text.x - 0.5 * this.text.width, this.text.y - 0.5 * this.text.height ),
        new PIXI.Point( this.text.x + 0.5 * this.text.width, this.text.y - 0.5 * this.text.height ),
        new PIXI.Point( this.text.x + 0.5 * this.text.width, this.text.y + 0.5 * this.text.height ),
        new PIXI.Point( this.text.x - 0.5 * this.text.width, this.text.y + 0.5 * this.text.height ),
      ]
      this.lineStyle( 2 )
      this.beginFill( 0xffffe1, 1 )
      this.drawPolygon( points )
      this.endFill()
    }
  }

  updateHoveringState(
      originX: number,
      originY: number,
      objectToShow: any,
      objectPathLevel: PathLevelEnum,
  ): void {
    this.originX = originX
    this.originY = originY
    this.objectToShow = objectToShow
    this.objectPathLevel = objectPathLevel
    if (!!this.objectToShow) {
      this.timer = setTimeout( () => this.paint(), 1000 )
    } else {
      this.paint()
      clearTimeout( this.timer )
    }
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
    switch (this._origin) {
      case GraphicalOriginEnum.TOP_LEFT:
        this.text.scale.set( 1, 1 )
        break
      case GraphicalOriginEnum.TOP_RIGHT:
        this.text.scale.set( -1, 1 )
        break
      case GraphicalOriginEnum.BOTTOM_RIGHT:
        this.text.scale.set( -1, -1 )
        break
      case GraphicalOriginEnum.BOTTOM_LEFT:
        this.text.scale.set( 1, -1 )
        break
    }
  }
}
