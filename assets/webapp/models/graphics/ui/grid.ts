// @ts-ignore
import rect from '@adonis/shared/images/graphics/rect.svg'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IMaskMovingObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moving-observer'
import * as PIXI from 'pixi.js'

export default class Grid extends PIXI.TilingSprite implements IMaskMovingObserver {

  private _origin: GraphicalOriginEnum

  constructor( private graphicalConfiguration: GraphicalConfiguration,
               private graphicalContext: GraphicalContext ) {
    super(
        PIXI.Texture.from( rect ),
        0,
        0,
    )
  }

  updateMovingState( visibleBound: PIXI.Rectangle ): void {
    this.width = ((this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
        this.graphicalContext.worldW - visibleBound.x : visibleBound.width + visibleBound.x) + 50
    this.height = ((this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
        this.graphicalContext.worldH - visibleBound.y : visibleBound.height + visibleBound.y) + 50
  }

  updateBoundaries() {
    this.tileScale.x = this.graphicalContext.cellW / this.graphicalConfiguration.textureW
    this.tileScale.y = this.graphicalContext.cellH / this.graphicalConfiguration.textureH
    this.x = this.graphicalContext.cellW
    this.y = this.graphicalContext.cellH
  }

  prepareForExport() {
    this.width = (this.graphicalContext.maxCellX - this.graphicalContext.minCellX + 3) * this.graphicalContext.cellW
    this.height = (this.graphicalContext.maxCellY - this.graphicalContext.minCellY + 3) * this.graphicalContext.cellH
    this.x = (this.graphicalContext.minCellX - 1) * this.graphicalContext.cellW
    this.y = (this.graphicalContext.minCellY - 1) * this.graphicalContext.cellH
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
  }
}
