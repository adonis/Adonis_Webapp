// @ts-ignore
import rect from '@adonis/shared/images/graphics/topBar.svg'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IMaskMovingObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moving-observer'
import * as PIXI from 'pixi.js'

export default class TopBar extends PIXI.Container implements IMaskMovingObserver {

  private topShift = 0
  private background: PIXI.TilingSprite
  private _origin: GraphicalOriginEnum

  constructor( private graphicalConfiguration: GraphicalConfiguration,
               private graphicalContext: GraphicalContext,
               private bottom = false ) {
    super()
    this.background = PIXI.TilingSprite.from(
        rect,
        {
          width: graphicalContext.worldW,
          height: graphicalContext.cellH / 2,
        },
    )
    this.addChild( this.background )
    this.writeLabels()
    this.paint()
  }

  private writeLabels() {
    const style = new PIXI.TextStyle( {
      fontSize: 40,
    } );
    (new Array( this.graphicalContext.boudaryX ).fill( 0 )).map( ( item, index, array ) => {
      if (index > 0) {
        const text = new PIXI.Text( `${
            (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ? array.length - index : index
        }`, style )
        text.anchor.set( 0.5 )
        text.x = (index + 0.5) * this.graphicalContext.cellW
        text.y = this.graphicalContext.cellH / 4
        this.addChild( text )
      }
    } )
  }

  private paint() {
    this.y = this.topShift
  }

  updateBoundaries() {
    this.background.width = this.graphicalContext.worldW
    this.background.height = this.graphicalContext.cellH / 2
    this.background.tileScale.x = this.graphicalContext.cellW / this.graphicalConfiguration.textureW
    this.background.tileScale.y = this.graphicalContext.cellH / this.graphicalConfiguration.textureH
    this.removeChildren()
    this.addChild( this.background )
    this.writeLabels()
  }

  updateMovingState( visibleBound: PIXI.Rectangle ): void {
    const originShift = this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT
    this.topShift = (originShift || this.bottom) && !(originShift && this.bottom) ?
        visibleBound.y + visibleBound.height - (this.graphicalContext.cellH / 2) : visibleBound.y
    this.paint()
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
    this.updateBoundaries()
  }

  cloneForExport(): TopBar {
    const res = new TopBar( this.graphicalConfiguration, this.graphicalContext, this.bottom )
    res.origin = this._origin
    const originShift = (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT)
    res.y = (originShift || this.bottom) && !(originShift && this.bottom) ?
        (this.graphicalContext.maxCellY + 1.5) * this.graphicalContext.cellH :
        (this.graphicalContext.minCellY - 1) * this.graphicalContext.cellH
    if ((this._origin === GraphicalOriginEnum.BOTTOM_RIGHT || this._origin === GraphicalOriginEnum.TOP_RIGHT)) {
      res.scale.x = -1
      res.x = -res.width + this.graphicalContext.cellW
      res.background.x = this.graphicalContext.cellW
    }
    if ((this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT)) {
      res.scale.y = -1
      res.y -= res.height
    }
    return res
  }
}
