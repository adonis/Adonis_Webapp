// @ts-ignore
import rect from '@adonis/shared/images/graphics/leftBar.svg'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import IMaskMovingObserver from '@adonis/webapp/models/graphics/interfaces/i-mask-moving-observer'
import * as PIXI from 'pixi.js'

export default class LeftBar extends PIXI.Container implements IMaskMovingObserver {

  private leftShift = 0
  private background: PIXI.TilingSprite
  private _origin: GraphicalOriginEnum

  constructor( private graphicalConfiguration: GraphicalConfiguration,
               private graphicalContext: GraphicalContext,
               private right = false ) {
    super()
    this.background = PIXI.TilingSprite.from(
        rect,
        {
          width: graphicalContext.cellW / 2,
          height: graphicalContext.worldH,
        },
    )
    this.addChild( this.background )
    this.writeLabels()
    this.paint()
  }

  private paint() {
    this.x = this.leftShift
  }

  private writeLabels() {
    const style = new PIXI.TextStyle( {
      fontSize: 40,
    } );
    (new Array( this.graphicalContext.boundaryY ).fill( 0 )).forEach( ( item, index, array ) => {
      if (index > 0) {
        const text = new PIXI.Text( `${
            (this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT) ? array.length - index : index
        }`, style )
        text.anchor.set( 0.5 )
        text.x = this.graphicalContext.cellW / 4
        text.y = (index + 0.5) * this.graphicalContext.cellH
        this.addChild( text )
      }

    } )
  }

  updateBoundaries() {
    this.background.width = this.graphicalContext.cellW / 2
    this.background.height = this.graphicalContext.worldH
    this.background.tileScale.x = this.graphicalContext.cellW / this.graphicalConfiguration.textureW
    this.background.tileScale.y = this.graphicalContext.cellH / this.graphicalConfiguration.textureH
    this.removeChildren()
    this.addChild( this.background )
    this.writeLabels()
  }

  updateMovingState( visibleBound: PIXI.Rectangle ): void {
    const originShift = (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT)
    this.leftShift = (originShift || this.right) && !(originShift && this.right) ?
        visibleBound.x + visibleBound.width - (this.graphicalContext.cellW / 2) : visibleBound.x
    this.paint()
  }

  cloneForExport(): LeftBar {
    const res = new LeftBar( this.graphicalConfiguration, this.graphicalContext, this.right )
    res.origin = this._origin
    const originShift = (this._origin === GraphicalOriginEnum.TOP_RIGHT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT)
    res.x = (originShift || this.right) && !(originShift && this.right) ?
        (this.graphicalContext.maxCellX + 1.5) * this.graphicalContext.cellW :
        (this.graphicalContext.minCellX - 1) * this.graphicalContext.cellW
    if ((this._origin === GraphicalOriginEnum.BOTTOM_LEFT || this._origin === GraphicalOriginEnum.BOTTOM_RIGHT)) {
      res.scale.y = -1
      res.y = -res.height + this.graphicalContext.cellH
      res.background.y = this.graphicalContext.cellH
    }
    if ((this._origin === GraphicalOriginEnum.BOTTOM_RIGHT || this._origin === GraphicalOriginEnum.TOP_RIGHT)) {
      res.scale.x = -1
      res.x -= res.width
    }
    return res
  }

  set origin( value: GraphicalOriginEnum ) {
    this._origin = value
    this.updateBoundaries()
  }
}
