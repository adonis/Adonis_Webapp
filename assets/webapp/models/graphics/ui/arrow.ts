import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import AbstractPart from '@adonis/webapp/models/graphics/business-objects/abstract-part'
import GraphicalContext from '@adonis/webapp/models/graphics/graphical-context'
import '@pixi/graphics-extras'
import * as PIXI from 'pixi.js'

export default class Arrow extends PIXI.Container {

    private graphics: PIXI.Graphics

    selected = false

    constructor(private graphicalContext: GraphicalContext, private graphicalConfiguration: GraphicalConfiguration, public start: AbstractPart<any>, public end: AbstractPart<any>) {
        super()
        this.graphics = new PIXI.Graphics()
        this.interactive = true
        this.addChild(this.graphics)
        this.paint()
    }

    private paint() {
        this.graphics.clear()
        if (!!this.end) {
            this.graphics.lineStyle(4, this.selected ? this.graphicalConfiguration.selectedItemColor : 0x000000, 1)
            const startX = this.start.center.x
            const startY = this.start.center.y
            const endX = this.end.center.x
            const endY = this.end.center.y

            const getIntersect = (acc1: { x: number, y: number }, poly: PIXI.Point[]) => acc1 !== null ? acc1 : poly.reduce(
                (acc: { x: number, y: number }, point, index, tab) => acc !== null ? acc : this.calculateIntersection(
                    point,
                    tab[(index + 1) % tab.length],
                    {x: startX, y: startY},
                    {x: endX, y: endY},
                ), null)
            const startIntersect = this.start.points.reduce(
                getIntersect,
                null) ?? {x: startX, y: startY}
            const endIntersect = this.end.points.reduce(
                getIntersect,
                null) ?? {x: endX, y: endY}
            this.graphics.moveTo(startIntersect.x, startIntersect.y)
            this.graphics.lineTo(endIntersect.x, endIntersect.y)
            this.graphics.beginFill()
            this.graphics.drawRegularPolygon(
                endIntersect.x,
                endIntersect.y,
                10,
                3,
                ((endX - startX < 0 ? -1 : 1) * Math.PI) / 2 + Math.atan((endY - startY) / (endX - startX)),
            )
            this.graphics.endFill()
        }
    }

    public select() {
        this.selected = true
        this.paint()
    }

    public unselect() {
        if (this.selected) {
            this.selected = false
            this.paint()
        }
    }

    private calculateIntersection(
        p1: { x: number, y: number },
        p2: { x: number, y: number },
        p3: { x: number, y: number },
        p4: { x: number, y: number },
    ): { x: number, y: number } {

        const denom = ((p4.y - p3.y) * (p2.x - p1.x)) - ((p4.x - p3.x) * (p2.y - p1.y))
        const numeA = ((p4.x - p3.x) * (p1.y - p3.y)) - ((p4.y - p3.y) * (p1.x - p3.x))
        const numeB = ((p2.x - p1.x) * (p1.y - p3.y)) - ((p2.y - p1.y) * (p1.x - p3.x))

        if (denom === 0) {
            return null
        }

        const uA = numeA / denom
        const uB = numeB / denom

        if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
            return {
                x: p1.x + (uA * (p2.x - p1.x)),
                y: p1.y + (uA * (p2.y - p1.y)),
            }
        }
        return null
    }
}
