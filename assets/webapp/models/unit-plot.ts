import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import { Individual } from '@adonis/webapp/models/individual'
import HasNote from '@adonis/webapp/models/interfaces/has-note'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Note from '@adonis/webapp/models/note'
import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import Treatment from '@adonis/webapp/models/treatment'
import { v4 } from 'uuid'

export default class UnitPlot implements ApiEntity, HasPathLevel, HasNote, PropertyViewable {

  private _individuals: Promise<Individual>[]
  private _outExperimentationZones: Promise<OutExperimentationZone>[]
  private _treatment: Promise<Treatment>
  private _notes: Promise<Note>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      private _individualIris: string[],
      private individualCallback: () => Promise<Individual>[],
      private _outExperimentationZonesIri: string[],
      private outExperimentationZonesCallback: () => Promise<OutExperimentationZone>[],
      private _treatmentIri: string,
      private treatmentCallback: () => Promise<Treatment>,
      private _noteIris: string[],
      private notesCallback: () => Promise<Note>[],
      public color: number,
      public comment: string,
      public openSilexUri: string,
      public geometry: string,
  ) {
  }

  get pathLevel(): PathLevelEnum {
    return PathLevelEnum.UNIT_PLOT
  }

  get individuals(): Promise<Individual>[] {
    return this._individuals = this._individuals || this.individualCallback()
  }

  get individualIris(): string[] {
    return this._individualIris
  }

  get outExperimentationZones(): Promise<OutExperimentationZone>[] {
    return this._outExperimentationZones = this._outExperimentationZones || this.outExperimentationZonesCallback()
  }

  get outExperimentationZonesIris(): string[] {
    return this._outExperimentationZonesIri
  }

  get treatmentIri(): string {
    return this._treatmentIri
  }

  get treatment(): Promise<Treatment> {
    return this._treatment = this._treatment || this.treatmentCallback()
  }

  get children(): Promise<Individual>[] {
    return this.individuals
  }

  get notes(): Promise<Note>[] {
    return this._notes = this._notes || this.notesCallback()
  }

  get noteIris(): string[] {
    return this._noteIris
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.unitPlot.name',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.unitPlot.comment',
        patchDtoProperty: 'comment',
      },
      this.treatment.then( treatment => ({
        propertyValue: treatment.name,
        propertyName: 'navigation.propertyView.unitPlot.treatmentName',
      }) ),
      this.treatment.then( treatment => ({
        propertyValue: treatment.shortName,
        propertyName: 'navigation.propertyView.unitPlot.treatmentShortName',
      }) ),
      {
        propertyValue: this._individualIris.length,
        propertyName: 'navigation.propertyView.unitPlot.individualNumber',
      },
        {
            propertyValue: this.geometry,
            propertyName: 'navigation.propertyView.unitPlot.geometry',
            patchDtoProperty: 'geometry',
        },
    ] )
  }

  clone(): UnitPlot {
    return new UnitPlot(
        v4(),
        null,
        this.name,
        this._individualIris,
        () => this.individualCallback().map(promise => promise.then(item => item.clone())),
        this._outExperimentationZonesIri,
        () => this.outExperimentationZonesCallback().map(promise => promise.then(item => item.clone())),
        this._treatmentIri,
        this.treatmentCallback,
        [],
        () => [],
        this.color,
        this.comment,
        null,
        this.geometry,
    )
  }
}
