import ApiEntity from '@adonis/shared/models/api-entity'
import MobileDataEntry from '@adonis/webapp/models/mobile-data-entry'

export default class MobileResponse implements ApiEntity {
  private _dataEntry: Promise<MobileDataEntry>

  constructor(
      public iri: string,
      public id: number,
      private _dataEntryIri: string,
      private dataEntryCallback: () => Promise<MobileDataEntry>,
  ) {

  }

  get dataEntry(): Promise<MobileDataEntry> {
    return this._dataEntry = this._dataEntry || this.dataEntryCallback()
  }

  get dataEntryIri(): string {
    return this._dataEntryIri
  }
}
