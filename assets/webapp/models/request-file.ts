import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'
import ParsingErrorEnum from '@adonis/webapp/constants/parsing-error-enum'

export default class RequestFile {
  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public status: JobStatusEnum,
      public contentUrl: string,
      public uploadDate: Date,
      public linedError: ParsingErrorEnum[][],
      public disabled: boolean,
      public deleted: boolean,
  ) {
  }
}
