import ApiEntity from '@adonis/shared/models/api-entity'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import VariableConnection from '@adonis/webapp/models/variable-connection'

export default class GeneratorVariable implements ApiEntity, PropertyViewable {

  private _generatedSimpleVariables: Promise<SimpleVariable>[]

  private _generatedGeneratorVariables: Promise<GeneratorVariable>[]

  private _connectedvariables: Promise<VariableConnection>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public shortName: string,
      public repetitions: number,
      public unit: string,
      public pathLevel: PathLevelEnum,
      public comment: string,
      public order: number,
      public format: VariableFormatEnum,
      public formatLength: number,
      public mandatory: boolean,
      public identifier: string,
      public created: Date,
      public generatedPrefix: string,
      public numeralIncrement: boolean,
      public pathWayHorizontal: boolean,
      public directCounting: boolean,
      private _generatedSimpleVariablesIriTab: string[],
      private generatedSimpleVariablesCallback: () => Promise<SimpleVariable>[],
      public _generatedGeneratorVariablesIriTab: string[],
      private generatedGeneratorVariablesCallback: () => Promise<GeneratorVariable>[],
      private _projectIri: string,
      private _connectedVariableIris: string[],
      private connectedvariableCallback: () => Promise<VariableConnection>[],
      public openSilexUri: string,
  ) {

  }

  get generatedSimpleVariables(): Promise<SimpleVariable>[] {
    return this._generatedSimpleVariables = this._generatedSimpleVariables || this.generatedSimpleVariablesCallback()
  }

  get generatedSimpleVariablesIriTab(): string[] {
    return this._generatedSimpleVariablesIriTab
  }

  get generatedGeneratorVariables(): Promise<GeneratorVariable>[] {
    return this._generatedGeneratorVariables = this._generatedGeneratorVariables || this.generatedGeneratorVariablesCallback()
  }

  get generatedGeneratorVariablesIriTab(): string[] {
    return this._generatedGeneratorVariablesIriTab
  }

  get projectIri() {
    return this._projectIri
  }

  get connectedVariableIris(): string[] {
    return this._connectedVariableIris
  }

  get connectedvariables(): Promise<VariableConnection>[] {
    return this._connectedvariables = this._connectedvariables || this.connectedvariableCallback()
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all([
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.variable.name',
        patchDtoProperty: 'name',
      },
      {
        propertyValue: this.shortName,
        propertyName: 'navigation.propertyView.variable.shortName',
        patchDtoProperty: 'shortName',
      },
      {
        propertyValue: this.created,
        propertyName: 'navigation.propertyView.variable.created',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.variable.comment',
        patchDtoProperty: 'comment',
      },
      {
        propertyValue: this.identifier,
        propertyName: 'navigation.propertyView.variable.identifier',
        patchDtoProperty: 'identifier',
      },
      {
        propertyValue: this.unit,
        propertyName: 'navigation.propertyView.variable.unit',
      },
      {
        propertyValue: this.repetitions,
        propertyName: 'navigation.propertyView.variable.repetitions',
      },
      {
        propertyName: 'navigation.propertyView.variable.pathLevel',
        propertyValue: 'enums.levels',
        propertyValueArguments: { level: this.pathLevel },
        link: false,
      },
      {
        propertyValue: this.mandatory,
        propertyName: 'navigation.propertyView.variable.mandatory',
      },
      {
        propertyValue: this.openSilexUri,
        propertyName: 'navigation.propertyView.variable.openSilexUri',
      },
    ])
  }
}
