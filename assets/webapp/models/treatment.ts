import ApiEntity from '@adonis/shared/models/api-entity'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Modality from '@adonis/webapp/models/modality'

export default class Treatment implements ApiEntity, PropertyViewable {

  private _modalities: Promise<Modality>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public shortName: string,
      public repetitions: number,
      private _modalityIris: string[],
      private modalityCallback: () => Promise<Modality>[],
  ) {

  }

  get modalities(): Promise<Modality>[] {
    return this._modalities = this._modalities || this.modalityCallback()
  }

  get modalitiesIriTab(): string[] {
    return this._modalityIris
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.treatment.name',
        patchDtoProperty: 'value',
      },
      {
        propertyValue: this.shortName,
        propertyName: 'navigation.propertyView.treatment.shortName',
        patchDtoProperty: 'shortName',
      },
      {
        propertyValue: this.repetitions,
        propertyName: 'navigation.propertyView.treatment.repetitions',
      },
      Promise.all( this.modalities ).then( modalities => ({
        propertyValue: modalities.map( modality => modality.value ).reduce( ( acc, item ) => acc + ', ' + item ),
        propertyName: 'navigation.propertyView.treatment.modalities',
      }) ),
    ] )
  }

}
