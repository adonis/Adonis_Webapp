import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import Note from '@adonis/webapp/models/note'

export default interface HasNote extends HasPathLevel {
  notes: Promise<Note>[]
  noteIris: string[]
  children: Promise<HasNote>[] | null
}
