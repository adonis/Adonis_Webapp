
export default interface PropertyLine {
    propertyName: string,
    propertyNameArguments?: any,
    propertyValue: any,
    propertyValueArguments?: any,
    patchDtoProperty?: string
    nameLinkTab?: {
        name: string,
        id: number,
        attachmentIri: string,
    }[]
}
