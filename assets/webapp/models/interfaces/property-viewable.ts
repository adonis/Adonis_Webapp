import PropertyLine from '@adonis/webapp/models/interfaces/property-line'

export default interface PropertyViewable {
  propertyView: Promise<PropertyLine[]>
}
