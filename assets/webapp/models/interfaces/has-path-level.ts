import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export default interface HasPathLevel {
  iri: string
  id: number
  name: string
  pathLevel: PathLevelEnum
  children: Promise<HasPathLevel>[] | null
}
