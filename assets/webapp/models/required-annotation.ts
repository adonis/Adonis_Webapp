import ApiEntity from '@adonis/shared/models/api-entity'
import AnnotationKindEnum from '@adonis/webapp/constants/annotation-kind-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export default class RequiredAnnotation implements ApiEntity {

  constructor(
      public iri: string,
      public level: PathLevelEnum,
      public askWhenEntering: boolean,
      public type: AnnotationKindEnum,
      public comment: string,
  ) {

  }

}
