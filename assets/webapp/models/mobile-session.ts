import ApiEntity from '@adonis/shared/models/api-entity'

export default class MobileSession implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public startedAt: Date,
      public endedAt: Date,
  ) {
  }

}
