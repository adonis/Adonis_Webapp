import ApiEntity from '@adonis/shared/models/api-entity'
import SpreadingEnum from '@adonis/webapp/constants/spreading-enum'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'

export default class StateCode implements ApiEntity, PropertyViewable {

  constructor(
      public iri: string,
      public id: number,
      public code: number,
      public title: string,
      public meaning: string,
      public spreading: SpreadingEnum,
      public color: number,
      public permanent: boolean,
  ) {
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all([
      {
        propertyValue: this.title,
        propertyName: 'navigation.propertyView.stateCode.title',
      },
      {
        propertyValue: this.code,
        propertyName: 'navigation.propertyView.stateCode.code',
      },
      {
        propertyValue: this.meaning,
        propertyName: 'navigation.propertyView.stateCode.meaning',
        patchDtoProperty: 'meaning',
      },
      {
        propertyName: 'navigation.propertyView.stateCode.spreading',
        propertyValue: 'enums.spreading',
        propertyValueArguments: { level: this.spreading },
      },
    ])
  }

}
