import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'

export default class ResponseFile {
  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public status: JobStatusEnum,
      public contentUrl: string,
      public uploadDate: Date,
  ) {
  }
}
