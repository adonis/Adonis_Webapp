import ApiEntity from '@adonis/shared/models/api-entity'

export default class Env implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public ldapOn: boolean,
  ) {

  }

}
