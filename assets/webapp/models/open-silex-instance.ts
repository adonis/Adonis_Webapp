import Site from '@adonis/shared/models/site'
import User from '@adonis/shared/models/user'

export default class OpenSilexInstance {
    private _site: Promise<Site>

    constructor(
        public iri: string,
        public url: string,
        private _siteIri: string,
        private siteCallback: () => Promise<Site>,
    ) {
    }

    get site(): Promise<Site> {
        return this._site = this._site || this.siteCallback()
    }

    get siteIri(): string {
        return this._siteIri
    }
}
