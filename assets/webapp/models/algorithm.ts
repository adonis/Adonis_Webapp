import ApiEntity from '@adonis/shared/models/api-entity'
import AlgorithmConditionEnum from '@adonis/webapp/constants/algorithm-condition-enum'
import AlgorithmParameterEnum from '@adonis/webapp/constants/algorithm-parameter-enum'

export default class Algorithm implements ApiEntity {

  constructor(
      public iri: string,
      public name: string,
      public algorithmParameters: {
        name: string;
        order: number;
        specialParam?: AlgorithmParameterEnum;
      }[],
      public algorithmConditions: {
        type: AlgorithmConditionEnum,
      }[],
      public withSubBlock: boolean,
      public withRepartition: boolean,
  ) {

  }

}
