import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import HasNote from '@adonis/webapp/models/interfaces/has-note'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Note from '@adonis/webapp/models/note'
import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import UnitPlot from '@adonis/webapp/models/unit-plot'
import { v4 } from 'uuid'

export default class SubBlock implements ApiEntity, HasPathLevel, HasNote, PropertyViewable {

  private _unitPlots: Promise<UnitPlot>[]
  private _outExperimentationZones: Promise<OutExperimentationZone>[]
  private _surfacicUnitPlots: Promise<SurfacicUnitPlot>[]
  private _notes: Promise<Note>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      private _unitPlotIris: string[],
      private unitPlotCallback: () => Promise<UnitPlot>[],
      private _surfacicUnitPlotIris: string[],
      private surfacicUnitPlotCallback: () => Promise<SurfacicUnitPlot>[],
      private _outExperimentationZonesIri: string[],
      private outExperimentationZonesCallback: () => Promise<OutExperimentationZone>[],
      private _noteIris: string[],
      private notesCallback: () => Promise<Note>[],
      public color: number,
      public comment: string,
      public openSilexUri: string,
      public geometry: string,
  ) {
  }

  get pathLevel(): PathLevelEnum {
    return PathLevelEnum.SUB_BLOCK
  }

  get unitPlots(): Promise<UnitPlot>[] {
    return this._unitPlots = this._unitPlots || this.unitPlotCallback()
  }

  get surfacicUnitPlots(): Promise<SurfacicUnitPlot>[] {
    return this._surfacicUnitPlots = this._surfacicUnitPlots || this.surfacicUnitPlotCallback()
  }

  get outExperimentationZones(): Promise<OutExperimentationZone>[] {
    return this._outExperimentationZones = this._outExperimentationZones || this.outExperimentationZonesCallback()
  }

  get outExperimentationZonesIris(): string[] {
    return this._outExperimentationZonesIri
  }

  get children(): Promise<UnitPlot>[] | Promise<SurfacicUnitPlot>[] {
    return this._unitPlotIris.length > 0 ? this.unitPlots : this.surfacicUnitPlots
  }

  get unitPlotIris(): string[] {
    return this._unitPlotIris
  }

  get surfacicUnitPlotIris(): string[] {
    return this._surfacicUnitPlotIris
  }

  get notes(): Promise<Note>[] {
    return this._notes = this._notes || this.notesCallback()
  }

  get noteIris(): string[] {
    return this._noteIris
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.subBlock.name',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.subBlock.comment',
        patchDtoProperty: 'comment',
      },
      {
        propertyValue: this.openSilexUri,
        propertyName: 'navigation.propertyView.subBlock.openSilexUri',
      },
        {
            propertyValue: this.geometry,
            propertyName: 'navigation.propertyView.subBlock.geometry',
            patchDtoProperty: 'geometry',
        },
    ] )
  }

  clone() {
    return new SubBlock(
        v4(),
        null,
        this.name,
        this._unitPlotIris,
        () => this.unitPlotCallback().map( promise => promise.then( item => item.clone() ) ),
        this._surfacicUnitPlotIris,
        () => this.surfacicUnitPlotCallback().map( promise => promise.then( item => item.clone() ) ),
        this._outExperimentationZonesIri,
        () => this.outExperimentationZonesCallback().map( promise => promise.then( item => item.clone() ) ),
        [],
        () => [],
        this.color,
        this.comment,
        null,
        this.geometry,
    )
  }
}
