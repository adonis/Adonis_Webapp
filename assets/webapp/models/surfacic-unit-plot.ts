import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import HasNote from '@adonis/webapp/models/interfaces/has-note'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Note from '@adonis/webapp/models/note'
import Treatment from '@adonis/webapp/models/treatment'
import { v4 } from 'uuid'

export class SurfacicUnitPlot implements ApiEntity, HasPathLevel, HasNote, PropertyViewable {

  private _treatment: Promise<Treatment>
  private _notes: Promise<Note>[]

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public x: number,
      public y: number,
      public dead: boolean,
      public appeared: Date,
      public disappeared: Date,
      public identifier: string,
      public latitude: number,
      public longitude: number,
      public height: number,
      private _treatmentIri: string,
      private treatmentCallback: () => Promise<Treatment>,
      private _noteIris: string[],
      private notesCallback: () => Promise<Note>[],
      public color: number,
      public comment: string,
      public openSilexUri: string,
      public geometry: string,
  ) {
  }

  get pathLevel(): PathLevelEnum {
    return PathLevelEnum.SURFACE_UNIT_PLOT
  }

  get children(): null {
    return null
  }

  get treatmentIri(): string {
    return this._treatmentIri
  }

  get treatment(): Promise<Treatment> {
    return this._treatment = this._treatment || this.treatmentCallback()
  }

  get notes(): Promise<Note>[] {
    return this._notes = this._notes || this.notesCallback()
  }

  get noteIris(): string[] {
    return this._noteIris
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.name',
      },
      {
        propertyValue: this.x,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.x',
      },
      {
        propertyValue: this.y,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.y',
      },
      this.treatment.then( treatment => ({
        propertyValue: treatment.name,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.treatmentName',
      }) ),
      this.treatment.then( treatment => ({
        propertyValue: treatment.shortName,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.treatmentShortName',
      }) ),
      {
        propertyValue: this.identifier,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.identifier',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.comment',
        patchDtoProperty: 'comment',
      },
      {
        propertyValue: this.dead,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.dead',
      },
      {
        propertyValue: this.appeared,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.appeared',
      },
      {
        propertyValue: this.disappeared,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.disappeared',
      },
      {
        propertyValue: this.openSilexUri,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.openSilexUri',
      },
        {
            propertyValue: this.geometry,
            propertyName: 'navigation.propertyView.surfacicUnitPlot.geometry',
            patchDtoProperty: 'geometry',
        },
      /*{
        propertyValue: this.latitude,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.latitude',
      },
      {
        propertyValue: this.longitude,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.longitude',
      },
      {
        propertyValue: this.height,
        propertyName: 'navigation.propertyView.surfacicUnitPlot.height',
      },*/
    ] )
  }

  clone() {
    return new SurfacicUnitPlot(
        v4(),
        null,
        this.name,
        this.x,
        this.y,
        this.dead,
        this.appeared,
        this.disappeared,
        this.identifier,
        this.latitude,
        this.longitude,
        this.height,
        this._treatmentIri,
        this.treatmentCallback,
        [],
        () => [],
        this.color,
        this.comment,
        null,
        this.geometry,
    )
  }
}
