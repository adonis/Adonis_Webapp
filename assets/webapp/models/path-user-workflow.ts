import ApiEntity from '@adonis/shared/models/api-entity'
import User from '@adonis/shared/models/user'
import PathBase from '@adonis/webapp/models/path-base'

export default class PathUserWorkflow implements ApiEntity {

    private _user: Promise<User>
    private _pathBase: Promise<PathBase>

    constructor(
        public iri: string,
        public id: number,
        public workflow: string[],
        private _userIri: string,
        private _userCallback: () => Promise<User>,
        private _pathBaseIri: string,
        private _pathBaseCallback: () => Promise<PathBase>,
    ) {
    }

    get userIri(): string {
        return this._userIri
    }

    get user(): Promise<User> {
        return this._user = this._user || this._userCallback()
    }

    get pathBase(): Promise<PathBase> {
        return this._pathBase = this._pathBase || this._pathBaseCallback()
    }
}
