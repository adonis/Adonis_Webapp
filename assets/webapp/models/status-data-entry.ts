import ApiEntity from '@adonis/shared/models/api-entity'
import User from '@adonis/shared/models/user'
import IndividualChangeFormInterface from '@adonis/webapp/form-interfaces/individual-change-form-interface'
import MobileResponse from '@adonis/webapp/models/mobile-response'
import Project from '@adonis/webapp/models/project'

export default class StatusDataEntry implements ApiEntity {

    private _webappProject: Promise<Project>
    private _response: Promise<MobileResponse>
    private _user: Promise<User>

    constructor(
        public iri: string,
        public id: number,
        private _userIri: string,
        private userCallback: () => Promise<User>,
        private _webappProjectIri: string,
        private webappProjectCallback: () => Promise<Project>,
        private _projectOwnerIri: string,
        public projectName: string,
        public mobileProjectName: string,
        public status: string,
        public syncable: boolean,
        private _responseIri: string,
        private responseCallback: () => Promise<MobileResponse>,
        public changes: IndividualChangeFormInterface[],
    ) {
    }

    get webappProject(): Promise<Project> {
        return this._webappProject = this._webappProject || this.webappProjectCallback()
    }

    get userIri(): string {
        return this._userIri
    }

    get user(): Promise<User> {
        return this._user = this._user || this.userCallback()
    }

    get projectIri(): string {
        return this._webappProjectIri
    }

    get projectOwnerIri(): string {
        return this._projectOwnerIri
    }

    get response(): Promise<MobileResponse> {
        return this._response = this._response || this.responseCallback()
    }

    get responseIri(): string {
        return this._responseIri
    }
}
