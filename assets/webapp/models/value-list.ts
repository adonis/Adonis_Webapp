import ApiEntity from '@adonis/shared/models/api-entity'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'

export default class ValueList implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public values: {
        name: string,
      }[],
  ) {
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.valueList.name',
        patchDtoProperty: 'name',
      },
      {
        propertyName: 'navigation.propertyView.valueList.values',
        propertyValue: this.values.map(item => item.name).join(', '),
      },
    ] )
  }
}
