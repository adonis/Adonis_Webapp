import ApiEntity from '@adonis/shared/models/api-entity'
import CombinationOperationEnum from '@adonis/webapp/constants/combination-operation-enum'
import ComparisonOperationEnum from '@adonis/webapp/constants/comparison-operation-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default class Test implements ApiEntity {

  private _variable: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>
  private _comparedVariable: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>
  private _combinedVariable1: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>
  private _combinedVariable2: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>
  private _comparedVariable1: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>
  private _comparedVariable2: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>

  constructor(
      public iri: string,
      public id: number,
      public type: TestTypeEnum,
      private _variableIri: string,
      private variableCallback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      public authIntervalMin: number,
      public probIntervalMin: number,
      public probIntervalMax: number,
      public authIntervalMax: number,
      private _comparedVariableIri: string,
      private comparedVariableCallback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      public minGrowth: number,
      public maxGrowth: number,
      private _combinedVariable1Iri: string,
      private combinedVariable1Callback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      public combinationOperation: CombinationOperationEnum,
      private _combinedVariable2Iri: string,
      private combinedVariable2Callback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      public minLimit: number,
      public maxLimit: number,
      public compareWithVariable: boolean,
      private _comparedVariable1Iri: string,
      private comparedVariable1Callback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      public comparisonOperation: ComparisonOperationEnum,
      private _comparedVariable2Iri: string,
      private comparedVariable2Callback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      public comparedValue: number,
      public assignedValue: string,
  ) {

  }

  get variable(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._variable = this._variable || this.variableCallback()
  }

  get comparedVariable(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._comparedVariable = this._comparedVariable || this.comparedVariableCallback()
  }

  get combinedVariable1(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._combinedVariable1 = this._combinedVariable1 || this.combinedVariable1Callback()
  }

  get combinedVariable2(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._combinedVariable2 = this._combinedVariable2 || this.combinedVariable2Callback()
  }

  get comparedVariable1(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._comparedVariable1 = this._comparedVariable1 || this.comparedVariable1Callback()
  }

  get comparedVariable2(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._comparedVariable2 = this._comparedVariable2 || this.comparedVariable2Callback()
  }

  get variableIri(): string {
    return this._variableIri
  }

  get comparedVariableIri(): string {
    return this._comparedVariableIri
  }

  get combinedVariable1Iri(): string {
    return this._combinedVariable1Iri
  }

  get combinedVariable2Iri(): string {
    return this._combinedVariable2Iri
  }

  get comparedVariable1Iri(): string {
    return this._comparedVariable1Iri
  }

  get comparedVariable2Iri(): string {
    return this._comparedVariable2Iri
  }
}
