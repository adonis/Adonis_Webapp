import ApiEntity from '@adonis/shared/models/api-entity'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default class VariableConnection implements ApiEntity {

  /*private _project: Promise<Project>*/
  private _projectVariable: Promise<(SimpleVariable | SemiAutomaticVariable | GeneratorVariable)>
  private _dataEntryVariable: Promise<(SimpleVariable | SemiAutomaticVariable | GeneratorVariable)>

  constructor(
      public iri: string,
      /*private _projectIri: string,
      private projectCallback: () => Promise<Project>,*/
      private _projectVariableIri: string,
      private projectVariableCallback: () => Promise<(SimpleVariable | SemiAutomaticVariable | GeneratorVariable)>,
      private _dataEntryVariableIri: string,
      private dataEntryVariableCallback: () => Promise<(SimpleVariable | SemiAutomaticVariable | GeneratorVariable)>,
  ) {
  }

  /*get project(): Promise<Project> {
    return this._project = this._project || this.projectCallback()
  }*/

  get projectVariable(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._projectVariable = this._projectVariable || this.projectVariableCallback()
  }

  get dataEntryVariable(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._dataEntryVariable = this._dataEntryVariable || this.dataEntryVariableCallback()
  }
}
