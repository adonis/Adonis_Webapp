import ApiEntity from '@adonis/shared/models/api-entity'
import PathFilterNode from '@adonis/webapp/models/path-filter-node'
import PathLevelAlgorithm from '@adonis/webapp/models/path-level-algorithm'
import PathUserWorkflow from '@adonis/webapp/models/path-user-workflow'
import Project from '@adonis/webapp/models/project'

export default class PathBase implements ApiEntity {

  private _userPaths: Promise<PathUserWorkflow>[]
  private _project: Promise<Project>

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public filters: PathFilterNode[],
      public askWhenEntering: boolean,
      public levels: PathLevelAlgorithm[],
      public selectedIris: string[],
      public orderedIris: string[],
      private _userPathIris: string[],
      private userPathCallback: () => Promise<PathUserWorkflow>[],
      private _projectIri: string,
      private projectCallback: () => Promise<Project>,
  ) {

  }

  get userPaths(): Promise<PathUserWorkflow>[] {
    return this._userPaths = this._userPaths || this.userPathCallback()
  }

  get project(): Promise<Project> {
    return this._project = this._project || this.projectCallback()
  }
}
