import ApiEntity from '@adonis/shared/models/api-entity'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import TextureEnum from '@adonis/webapp/constants/texture-enum'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import OezNature from '@adonis/webapp/models/oez-nature'
import { v4 } from 'uuid'

export class OutExperimentationZone implements ApiEntity, HasPathLevel, PropertyViewable {

  private _nature: Promise<OezNature>

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public x: number,
      public y: number,
      public comment: string,
      private _natureIri: string,
      private natureCallback: () => Promise<OezNature>,
      public color: number,
      public texture: TextureEnum,
      public openSilexUri: string,
  ) {
  }

  get pathLevel(): PathLevelEnum {
    return PathLevelEnum.OEZ
  }

  get nature(): Promise<OezNature> {
    return this._nature = this._nature || this.natureCallback()
  }

  get children(): null {
    return null
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.name,
        propertyName: 'navigation.propertyView.outExperimentationZone.name',
        patchDtoProperty: 'name',
      },
      {
        propertyValue: this.x,
        propertyName: 'navigation.propertyView.outExperimentationZone.x',
      },
      {
        propertyValue: this.y,
        propertyName: 'navigation.propertyView.outExperimentationZone.y',
      },
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.outExperimentationZone.comment',
        patchDtoProperty: 'comment',
      },
      this.nature.then( nature => ({
        propertyValue: nature.nature,
        propertyName: 'navigation.propertyView.outExperimentationZone.nature',
      }) ),
      {
        propertyValue: this.openSilexUri,
        propertyName: 'navigation.propertyView.outExperimentationZone.openSilexUri',
      },
    ] )
  }

  clone() {
    return new OutExperimentationZone(
        v4(),
        null,
        this.name,
        this.x,
        this.y,
        this.comment,
        this._natureIri,
        this.natureCallback,
        this.color,
        this.texture,
        null,
    )
  }
}
