import ApiEntity from '@adonis/shared/models/api-entity'
import FilterComparatorsEnum from '@adonis/webapp/constants/filter-comparators-enum'
import FilterKeyEnum from '@adonis/webapp/constants/filters/filter-key-enum'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default class PathFilterNode implements ApiEntity {

  private _variable: Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>

  constructor(
      public iri: string,
      public id: number,
      public type: FilterKeyEnum,
      public text: string,
      public branches: PathFilterNode[],
      public operator: FilterComparatorsEnum,
      public value: string,
      private variableCallback: () => Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable>,
      private _variableIri: string,
  ) {

  }

  get variable(): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
    return this._variable = this._variable || this.variableCallback()
  }

  get variableIri(): string {
    return this._variableIri
  }
}
