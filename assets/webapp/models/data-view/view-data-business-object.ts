import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import FieldMeasure from '@adonis/webapp/models/field-measure'

export class ViewDataBusinessObject implements ApiEntity {

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        public x: number,
        public y: number,
        public dead: boolean,
        public appeared: Date,
        public disappeared: Date,
        public identifier: string,
        public projectData: string,
        public session: string,
        public treatment: TreatmentDataView,
        public target: string,
        public targetType: PathLevelEnum,
        public platform: string,
        public experiment: string,
        public block: string,
        public subBlock: string,
        public unitPlot: string,
        public surfacicUnitPlot: string,
        public individual: string,
    ) {
    }
}
