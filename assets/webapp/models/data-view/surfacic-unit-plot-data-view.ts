import ApiEntity from '@adonis/shared/models/api-entity'
import BlockDataView from '@adonis/webapp/models/data-view/block-data-view'
import SubBlockDataView from '@adonis/webapp/models/data-view/sub-block-data-view'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'

export class SurfacicUnitPlotDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public x: number,
      public y: number,
      public dead: boolean,
      public appeared: Date,
      public disappeared: Date,
      public identifier: string,
      public block: BlockDataView,
      public subBlock: SubBlockDataView,
      public treatment: TreatmentDataView,
  ) {
  }
}
