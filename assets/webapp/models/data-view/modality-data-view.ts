import ApiEntity from '@adonis/shared/models/api-entity'
import FactorDataView from '@adonis/webapp/models/data-view/factor-data-view'

export default class ModalityDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public value: string,
      public shortName: string,
      public factor: FactorDataView,
  ) {

  }

}