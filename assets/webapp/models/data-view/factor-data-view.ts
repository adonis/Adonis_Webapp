import ApiEntity from '@adonis/shared/models/api-entity'

export default class FactorDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public order: number,
  ) {

  }

}
