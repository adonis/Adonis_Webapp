import ApiEntity from '@adonis/shared/models/api-entity'
import PlatformDataView from '@adonis/webapp/models/data-view/platform-data-view'

export default class ExperimentDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public platform: PlatformDataView,
  ) {
  }
}
