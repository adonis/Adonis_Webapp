import ApiEntity from '@adonis/shared/models/api-entity'
import ExperimentDataView from '@adonis/webapp/models/data-view/experiment-data-view'

export default class BlockDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public experiment: ExperimentDataView,
  ) {
  }
}
