import ApiEntity from '@adonis/shared/models/api-entity'
import ModalityDataView from '@adonis/webapp/models/data-view/modality-data-view'

export default class TreatmentDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public shortName: string,
      public modalities: ModalityDataView[],
  ) {

  }

}