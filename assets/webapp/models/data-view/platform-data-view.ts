import ApiEntity from '@adonis/shared/models/api-entity'

export default class PlatformDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
  ) {
  }
}
