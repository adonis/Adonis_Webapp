import ApiEntity from '@adonis/shared/models/api-entity'
import BlockDataView from '@adonis/webapp/models/data-view/block-data-view'

export default class SubBlockDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public block: BlockDataView,
  ) {
  }
}
