import ApiEntity from '@adonis/shared/models/api-entity'
import UnitPlotDataView from '@adonis/webapp/models/data-view/unit-plot-data-view'

export class IndividualDataView implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public name: string,
      public x: number,
      public y: number,
      public dead: boolean,
      public appeared: Date,
      public disappeared: Date,
      public identifier: string,
      public unitPlot: UnitPlotDataView,
  ) {
  }
}
