import ApiEntity from '@adonis/shared/models/api-entity'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import FieldMeasure from '@adonis/webapp/models/field-measure'

export class ViewDataItem implements ApiEntity {

    constructor(
        public iri: string,
        public id: number,
        public code: number,
        public fieldMeasure: FieldMeasure,
        public projectData: string,
        public session: string,
        public repetition: number,
        public value: string,
        public timestamp: Date,
        public username: string,
        public target: string,
        public variable: string,
    ) {
    }
}
