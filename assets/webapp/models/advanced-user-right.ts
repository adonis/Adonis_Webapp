import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import AdvancedRightEnum from '@adonis/webapp/constants/advanced-right-enum'

export default class AdvancedUserRight {

  constructor(
      public iri: string,
      public userId: number,
      public objectId: number,
      public classIdentifier: AdvancedRightClassIdentifierEnum,
      public right: AdvancedRightEnum,
  ) {
  }
}
