import ApiEntity from '@adonis/shared/models/api-entity'
import Factor from '@adonis/webapp/models/factor'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export default class Modality implements ApiEntity, PropertyViewable {

    private _factor: Promise<Factor>

    private _openSilexInstance: Promise<OpenSilexInstance>

    constructor(
        public iri: string,
        public id: number,
        public value: string,
        public shortName: string,
        public identifier: string,
        private _factorIri: string,
        private factorCallback: () => Promise<Factor>,
        public openSilexUri: string,
        private _openSilexInstanceIri: string,
        private openSilexInstanceCallback: () => Promise<OpenSilexInstance>,
    ) {
    }

    get factor(): Promise<Factor> {
        return this._factor = this._factor || this.factorCallback()
    }

    get factorIri(): string {
        return this._factorIri
    }

    get openSilexInstance(): Promise<OpenSilexInstance> {
        return this._openSilexInstance = this._openSilexInstance || this.openSilexInstanceCallback()
    }

    get openSilexInstanceIri(): string {
        return this._openSilexInstanceIri
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.value,
                propertyName: 'navigation.propertyView.modality.value',
                patchDtoProperty: 'value',
            },
            {
                propertyValue: this.shortName,
                propertyName: 'navigation.propertyView.modality.shortName',
                patchDtoProperty: 'shortName',
            },
            {
                propertyValue: this.identifier,
                propertyName: 'navigation.propertyView.modality.identifier',
                patchDtoProperty: 'identifier',
            },
            {
                propertyValue: this.openSilexUri,
                propertyName: 'navigation.propertyView.modality.openSilexUri',
            },
        ])
    }

}
