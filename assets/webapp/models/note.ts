import ApiEntity from '@adonis/shared/models/api-entity'

export default class Note implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public text: string,
      public creationDate: Date,
      public creatorLogin: string,
      public creatorAvatar: string,
      public deleted: boolean,
  ) {
  }
}
