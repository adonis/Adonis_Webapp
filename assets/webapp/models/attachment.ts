import ApiEntity from '@adonis/shared/models/api-entity'
import File from '@adonis/webapp/models/file'

export default class Attachment implements ApiEntity {

  private _file: Promise<File>

  constructor(
      public iri: string,
      private _fileIri: string,
      private fileCallback: () => Promise<File>,
  ) {

  }

  get file(): Promise<File> {
    return this._file = this._file || this.fileCallback()
  }

  get fileIri(): string {
    return this._fileIri
  }

}
