import ApiEntity from '@adonis/shared/models/api-entity'

export default class Parameters implements ApiEntity {

  constructor(
      public iri: string,
      public id: number,
      public daysBeforeFileDelete: number,
  ) {

  }
}
