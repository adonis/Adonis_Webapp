import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import HasNote from '@adonis/webapp/models/interfaces/has-note'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import Note from '@adonis/webapp/models/note'
import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import SubBlock from '@adonis/webapp/models/sub-block'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import UnitPlot from '@adonis/webapp/models/unit-plot'
import { v4 } from 'uuid'

export default class Block implements ApiEntity, HasPathLevel, HasNote, PropertyViewable {

    private _unitPlots: Promise<UnitPlot>[]
    private _surfacicUnitPlots: Promise<SurfacicUnitPlot>[]
    private _subBlocks: Promise<SubBlock>[]
    private _outExperimentationZones: Promise<OutExperimentationZone>[]
    private _notes: Promise<Note>[]

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        private _unitPlotIris: string[],
        private unitPlotCallback: () => Promise<UnitPlot>[],
        private _surfacicUnitPlotIris: string[],
        private surfacicUnitPlotCallback: () => Promise<SurfacicUnitPlot>[],
        private _subBlockIris: string[],
        private subBlockCallback: () => Promise<SubBlock>[],
        private _outExperimentationZonesIri: string[],
        private outExperimentationZonesCallback: () => Promise<OutExperimentationZone>[],
        private _noteIris: string[],
        private notesCallback: () => Promise<Note>[],
        public color: number,
        public comment: string,
        public openSilexUri: string,
        public geometry: string,
    ) {
    }

    get pathLevel(): PathLevelEnum {
        return PathLevelEnum.BLOCK
    }

    get unitPlots(): Promise<UnitPlot>[] {
        return this._unitPlots = this._unitPlots || this.unitPlotCallback()
    }

    get surfacicUnitPlots(): Promise<SurfacicUnitPlot>[] {
        return this._surfacicUnitPlots = this._surfacicUnitPlots || this.surfacicUnitPlotCallback()
    }

    get subBlocks(): Promise<SubBlock>[] {
        return this._subBlocks = this._subBlocks || this.subBlockCallback()
    }

    get outExperimentationZones(): Promise<OutExperimentationZone>[] {
        return this._outExperimentationZones = this._outExperimentationZones || this.outExperimentationZonesCallback()
    }

    get outExperimentationZonesIris(): string[] {
        return this._outExperimentationZonesIri
    }

    get children(): Promise<UnitPlot>[] | Promise<SurfacicUnitPlot>[] | Promise<SubBlock>[] {
        return this._subBlockIris.length > 0 ? this.subBlocks : this._unitPlotIris.length > 0 ? this.unitPlots : this.surfacicUnitPlots
    }

    get unitPlotIris(): string[] {
        return this._unitPlotIris
    }

    get surfacicUnitPlotIris(): string[] {
        return this._surfacicUnitPlotIris
    }

    get subBlockIris(): string[] {
        return this._subBlockIris
    }

    get notes(): Promise<Note>[] {
        return this._notes = this._notes || this.notesCallback()
    }

    get noteIris(): string[] {
        return this._noteIris
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.name,
                propertyName: 'navigation.propertyView.block.name',
            },
            {
                propertyValue: this.comment,
                propertyName: 'navigation.propertyView.block.comment',
                patchDtoProperty: 'comment',
            },
            {
                propertyValue: this.openSilexUri,
                propertyName: 'navigation.propertyView.block.openSilexUri',
            },
            {
                propertyValue: this.geometry,
                propertyName: 'navigation.propertyView.block.geometry',
                patchDtoProperty: 'geometry',
            },
        ])
    }

    clone() {
        return new Block(
            v4(),
            null,
            this.name,
            this._unitPlotIris,
            () => this.unitPlotCallback().map(promise => promise.then(item => item.clone())),
            this._surfacicUnitPlotIris,
            () => this.surfacicUnitPlotCallback().map(promise => promise.then(item => item.clone())),
            this._subBlockIris,
            () => this.subBlockCallback().map(promise => promise.then(item => item.clone())),
            this._outExperimentationZonesIri,
            () => this.outExperimentationZonesCallback().map(promise => promise.then(item => item.clone())),
            [],
            () => [],
            this.color,
            this.comment,
            null,
            this.geometry,
        )
    }
}
