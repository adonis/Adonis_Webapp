import ApiEntity from '@adonis/shared/models/api-entity'
import User from '@adonis/shared/models/user'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import Attachment from '@adonis/webapp/models/attachment'
import Experiment from '@adonis/webapp/models/experiment'
import GraphicalTextZone from '@adonis/webapp/models/graphical-text-zone'
import HasPathLevel from '@adonis/webapp/models/interfaces/has-path-level'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import NorthIndicator from '@adonis/webapp/models/north-indicator'
import Project from '@adonis/webapp/models/project'

export default class Platform implements ApiEntity, PropertyViewable, HasPathLevel {

    private _experiments: Promise<Experiment>[]
    private _projects: Promise<Project>[]
    private _owner: Promise<User>
    private _attachments: Promise<Attachment>[]
    private _northIndicator: Promise<NorthIndicator>
    private _graphicalTextZones: Promise<GraphicalTextZone>[]

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        public siteName: string,
        public placeName: string,
        private _experimentIriTab: string[],
        private experimentsCallback: () => Promise<Experiment>[],
        private _projectIriTab: string[],
        private projectsCallback: () => Promise<Project>[],
        public comment: string,
        public created: string,
        private _ownerIri: string,
        private ownerCallback: () => Promise<User>,
        public color: number,
        public xMesh: number,
        public yMesh: number,
        public origin: GraphicalOriginEnum,
        private _attachmentsIris: string[],
        private attachmentsCallback: () => Promise<Attachment>[],
        private _northIndicatorIri: string,
        private northIndicatorCallback: () => Promise<NorthIndicator>,
        private _graphicalTextZoneIris: string[],
        private graphicalTextZonesCallback: () => Promise<GraphicalTextZone>[],
    ) {

    }

    get experiments(): Promise<Experiment>[] {
        return this._experiments = this._experiments || this.experimentsCallback()
    }

    get experimentIris(): string[] {
        return this._experimentIriTab
    }

    get projects(): Promise<Project>[] {
        return this._projects = this._projects || this.projectsCallback()
    }

    get projectIris(): string[] {
        return this._projectIriTab
    }

    get owner(): Promise<User> {
        return this._owner = this._owner || this.ownerCallback()
    }

    get ownerIri(): string {
        return this._ownerIri
    }

    get attachments(): Promise<Attachment>[] {
        return this._attachments = this._attachments || this.attachmentsCallback()
    }

    get attachmentsIris(): string[] {
        return this._attachmentsIris
    }

    get northIndicator(): Promise<NorthIndicator> {
        return this._northIndicator = this._northIndicator || this.northIndicatorCallback()
    }

    get northIndicatorIri(): string {
        return this._northIndicatorIri
    }

    get graphicalTextZones(): Promise<GraphicalTextZone>[] {
        return this._graphicalTextZones = this._graphicalTextZones || this.graphicalTextZonesCallback()
    }

    get graphicalTextZoneIris(): string[] {
        return this._graphicalTextZoneIris
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.name,
                propertyName: 'navigation.propertyView.platform.name',
                patchDtoProperty: 'name',
            },
            {
                propertyValue: this.siteName,
                propertyName: 'navigation.propertyView.platform.siteName',
                patchDtoProperty: 'siteName',
            },
            {
                propertyValue: this.placeName,
                propertyName: 'navigation.propertyView.platform.placeName',
                patchDtoProperty: 'placeName',
            },
            this.owner.then(owner => ({
                propertyName: 'navigation.propertyView.platform.owner',
                propertyValue: owner.username,
            })),
            {
                propertyValue: this.created,
                propertyName: 'navigation.propertyView.platform.created',
            },
            {
                propertyValue: this.comment,
                propertyName: 'navigation.propertyView.platform.comment',
                patchDtoProperty: 'comment',
            },
            Promise.all(this.attachments.map(promise => promise.then(att => att.file.then(file => ({
                name: file.originalFileName,
                id: file.id,
                attachmentIri: att.iri,
            }))))).then(files => files.reduce((acc, item) => ({
                ...acc,
                propertyValueArguments: {count: acc.propertyValueArguments.count + 1},
                nameLinkTab: [...acc.nameLinkTab, item],
            }), {
                propertyValue: 'navigation.propertyView.experiment.attachmentCount',
                propertyValueArguments: {count: 0},
                propertyName: 'navigation.propertyView.experiment.attachments',
                nameLinkTab: [],
            })),
        ])
    }

    get children(): Promise<Experiment>[] {
        return this.experiments
    }

    get pathLevel(): PathLevelEnum {
        return PathLevelEnum.PLATFORM
    }

}
