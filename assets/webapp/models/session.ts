import ApiEntity from '@adonis/shared/models/api-entity'
import Annotation from '@adonis/webapp/models/annotation'
import FieldMeasure from '@adonis/webapp/models/field-measure'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'

export default class Session implements ApiEntity, PropertyViewable {

  private _fields: Promise<FieldMeasure>[]
  private _annotations: Promise<Annotation>[]

  constructor(
      public iri: string,
      public id: number,
      public startedAt: Date,
      public endedAt: Date,
      public comment: string,
      private fieldIriTab: string[],
      private fieldsCallback: () => Promise<FieldMeasure>[],
      private annotationIriTab: string[],
      private annotationsCallback: () => Promise<Annotation>[],
      public userName: string,
  ) {
  }

  get fields(): Promise<FieldMeasure>[] {
    return this._fields = this._fields || this.fieldsCallback()
  }

  get annotations(): Promise<Annotation>[] {
    return this._annotations = this._annotations || this.annotationsCallback()
  }

  get propertyView(): Promise<PropertyLine[]> {
    return Promise.all( [
      {
        propertyValue: this.comment,
        propertyName: 'navigation.propertyView.session.comment',
        patchDtoProperty: 'comment',
      },
      {
        propertyValue: this.userName,
        propertyName: 'navigation.propertyView.session.userName',
      },
      {
        propertyValue: this.startedAt,
        propertyName: 'navigation.propertyView.session.startedAt',
      },
      {
        propertyValue: this.endedAt,
        propertyName: 'navigation.propertyView.session.endedAt',
      },
    ] )
  }
}
