import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'
import ValueList from '@adonis/webapp/models/value-list'
import VariableConnection from '@adonis/webapp/models/variable-connection'
import VariableScale from '@adonis/webapp/models/variable-scale'

export default class SimpleVariable implements ApiEntity, PropertyViewable {

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        public shortName: string,
        public repetitions: number,
        public unit: string,
        public pathLevel: PathLevelEnum,
        public comment: string,
        public order: number,
        public format: VariableFormatEnum,
        public formatLength: number,
        public defaultTrueValue: boolean,
        public type: VariableTypeEnum,
        public mandatory: boolean,
        public identifier: string,
        public created: Date,
        private _projectIri: string,
        private _scaleIri: string,
        private scaleCallback: () => Promise<VariableScale>,
        private _valueListIri: string,
        private valueListCallback: () => Promise<ValueList>,
        private _connectedVariableIris: string[],
        private connectedvariableCallback: () => Promise<VariableConnection>[],
        public openSilexUri: string,
        private _openSilexInstanceIri: string,
        private openSilexInstanceCallback: () => Promise<OpenSilexInstance>,
    ) {

    }

    private _variableScale: Promise<VariableScale>

    get variableScale(): Promise<VariableScale> {
        return this._variableScale = this._variableScale || this.scaleCallback()
    }

    private _valueList: Promise<ValueList>

    get valueList(): Promise<ValueList> {
        return this._valueList = this._valueList || this.valueListCallback()
    }

    private _connectedvariables: Promise<VariableConnection>[]

    get connectedvariables(): Promise<VariableConnection>[] {
        return this._connectedvariables = this._connectedvariables || this.connectedvariableCallback()
    }

    private _openSilexInstance: Promise<OpenSilexInstance>

    get openSilexInstance(): Promise<OpenSilexInstance> {
        return this._openSilexInstance = this._openSilexInstance || this.openSilexInstanceCallback()
    }

    get projectIri() {
        return this._projectIri
    }

    get scaleIri(): string {
        return this._scaleIri
    }

    get valueListIri(): string {
        return this._valueListIri
    }

    get connectedVariableIris(): string[] {
        return this._connectedVariableIris
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.name,
                propertyName: 'navigation.propertyView.variable.name',
                patchDtoProperty: 'name',
            },
            {
                propertyValue: this.shortName,
                propertyName: 'navigation.propertyView.variable.shortName',
                patchDtoProperty: 'shortName',
            },
            {
                propertyValue: this.created,
                propertyName: 'navigation.propertyView.variable.created',
            },
            {
                propertyValue: this.comment,
                propertyName: 'navigation.propertyView.variable.comment',
                patchDtoProperty: 'comment',
            },
            {
                propertyValue: this.identifier,
                propertyName: 'navigation.propertyView.variable.identifier',
                patchDtoProperty: 'identifier',
            },
            {
                propertyName: 'navigation.propertyView.variable.type',
                propertyValue: 'enums.variable.types',
                propertyValueArguments: {type: this.type},
            },
            {
                propertyValue: this.unit,
                propertyName: 'navigation.propertyView.variable.unit',
            },
            {
                propertyValue: this.repetitions,
                propertyName: 'navigation.propertyView.variable.repetitions',
            },
            {
                propertyName: 'navigation.propertyView.variable.pathLevel',
                propertyValue: 'enums.levels',
                propertyValueArguments: {level: this.pathLevel},
                link: false,
            },
            {
                propertyValue: this.mandatory,
                propertyName: 'navigation.propertyView.variable.mandatory',
            },
            {
                propertyValue: this.openSilexUri,
                propertyName: 'navigation.propertyView.variable.openSilexUri',
            },
        ])
    }
}
