import ApiEntity from '@adonis/shared/models/api-entity'
import User from '@adonis/shared/models/user'
import Device from '@adonis/webapp/models/device'
import Experiment from '@adonis/webapp/models/experiment'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import PropertyLine from '@adonis/webapp/models/interfaces/property-line'
import PropertyViewable from '@adonis/webapp/models/interfaces/property-viewable'
import PathBase from '@adonis/webapp/models/path-base'
import Platform from '@adonis/webapp/models/platform'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import StateCode from '@adonis/webapp/models/state-code'
import StatusDataEntry from '@adonis/webapp/models/status-data-entry'

export default class Project implements ApiEntity, PropertyViewable {

    private _experiments: Promise<Experiment>[]
    private _simpleVariables: Promise<SimpleVariable>[]
    private _generatorVariables: Promise<GeneratorVariable>[]
    private _devices: Promise<Device>[]
    private _owner: Promise<User>
    private _platform: Promise<Platform>
    private _pathBase: Promise<PathBase>
    private _stateCodes: Promise<StateCode>[]
    private _statusDataEntries: Promise<StatusDataEntry>[]

    constructor(
        public iri: string,
        public id: number,
        public name: string,
        private _platformIri: string,
        private platformCallback: () => Promise<Platform>,
        private _experimentIriTab: string[],
        private experimentsCallback: () => Promise<Experiment>[],
        private _simpleVariableIriTab: string[],
        private simpleVariablesCallback: () => Promise<SimpleVariable>[],
        private _generatorVariableIriTab: string[],
        private generatorVariablesCallback: () => Promise<GeneratorVariable>[],
        private _deviceIriTab: string[],
        private devicesCallback: () => Promise<Device>[],
        public comment: string,
        public created: Date,
        private _ownerIri: string,
        private ownerCallback: () => Promise<User>,
        private _pathBaseIri: string,
        private pathBaseCallback: () => Promise<PathBase>,
        private _projectDataIri: string[],
        private stateCodesCallback: () => Promise<StateCode>[],
        private _stateCodesIriTab: string[],
        private statusDataEntriesCallback: () => Promise<StatusDataEntry>[],
        private _statusDataEntryIriTab: string[],
    ) {

    }

    get experiments(): Promise<Experiment>[] {
        return this._experiments = this._experiments || this.experimentsCallback()
    }

    get experimentsIri(): string[] {
        return this._experimentIriTab
    }

    get simpleVariables(): Promise<SimpleVariable>[] {
        return this._simpleVariables = this._simpleVariables || this.simpleVariablesCallback()
    }

    get simpleVariablesIri(): string[] {
        return this._simpleVariableIriTab
    }

    get generatorVariables(): Promise<GeneratorVariable>[] {
        return this._generatorVariables = this._generatorVariables || this.generatorVariablesCallback()
    }

    get generatorVariablesIri(): string[] {
        return this._generatorVariableIriTab
    }

    get devices(): Promise<Device>[] {
        return this._devices = this._devices || this.devicesCallback()
    }

    get devicesIri(): string[] {
        return this._deviceIriTab
    }

    get owner(): Promise<User> {
        return this._owner = this._owner || this.ownerCallback()
    }

    get ownerIri(): string {
        return this._ownerIri
    }

    get platform(): Promise<Platform> {
        return this._platform = this._platform || this.platformCallback()
    }

    get platformIri(): string {
        return this._platformIri
    }

    get pathBaseIri(): string {
        return this._pathBaseIri
    }

    get pathBase(): Promise<PathBase> {
        return this._pathBase = this._pathBase || this.pathBaseCallback()
    }

    get projectDataIri(): string[] {
        return this._projectDataIri
    }

    get stateCodes(): Promise<StateCode>[] {
        return this._stateCodes = this._stateCodes || this.stateCodesCallback()
    }

    get stateCodesIriTab(): string[] {
        return this._stateCodesIriTab
    }

    get statusDataEntries(): Promise<StatusDataEntry>[] {
        return this._statusDataEntries = this._statusDataEntries || this.statusDataEntriesCallback()
    }

    get statusDataEntryIriTab(): string[] {
        return this._statusDataEntryIriTab
    }

    get propertyView(): Promise<PropertyLine[]> {
        return Promise.all([
            {
                propertyValue: this.name,
                propertyName: 'navigation.propertyView.project.name',
                patchDtoProperty: 'name',
            },
            {
                propertyValue: this.comment,
                propertyName: 'navigation.propertyView.project.comment',
                patchDtoProperty: 'comment',
            },
            this.owner.then(owner => ({
                propertyName: 'navigation.propertyView.project.owner',
                propertyValue: owner.username,
            })),
            {
                propertyValue: this.created,
                propertyName: 'navigation.propertyView.project.created',
            },
        ])
    }
}
