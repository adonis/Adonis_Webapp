enum AlertTypeEnum {

  SUCCESS = 'primary',
  FAILURE = 'error',

  WARNING = 'warning',
}

export default AlertTypeEnum
