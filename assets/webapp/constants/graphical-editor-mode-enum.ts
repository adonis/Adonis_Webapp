enum GraphicalEditorModeEnum {
  CONSULT = 'consult',
  EDIT = 'edit',
  PLACE_EXPERIMENT = 'placeExperiment',
  CREATE_PATH = 'createPath',
  VIEW_PATH = 'viewPath',
  VIEW_VARIABLE = 'viewVariable',
}

export default GraphicalEditorModeEnum
