enum ComparisonOperationEnum {
  SUP = '>',
  SUP_EQUAL = '>=',
  EQUAL = '=',
  INF_EQUAL = '<=',
  INF = '<',
}

export default ComparisonOperationEnum
