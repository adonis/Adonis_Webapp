enum AdvancedRightClassIdentifierEnum {

  PROTOCOL = 'webapp_protocol',
  EXPERIMENT = 'webapp_experiment',
  PLATFORM = 'webapp_platform',
  PROJECT = 'webapp_project',
}

export default AdvancedRightClassIdentifierEnum
