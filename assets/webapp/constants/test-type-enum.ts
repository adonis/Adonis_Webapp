enum TestTypeEnum {
  INTERVAL = 'interval',
  GROWTH = 'growth',
  COMBINATION = 'combination',
  PRECONDITIONED = 'preconditioned',
}

export default TestTypeEnum
