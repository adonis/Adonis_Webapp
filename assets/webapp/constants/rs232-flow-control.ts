enum Rs232FlowControl {
  NONE = 'none',
  XONXOFF = 'xonxoff',
  RTSCTS = 'rtscts',
}

export default Rs232FlowControl
