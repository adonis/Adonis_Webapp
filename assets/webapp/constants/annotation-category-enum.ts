enum AnnotationCategoryEnum {

  EVENT = 'event',
  OBSERVATION = 'observation',
  MEASURE = 'measure',
  DIFFICULTY = 'difficulty',
}

export default AnnotationCategoryEnum
