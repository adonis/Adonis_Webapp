enum ModuleEnum {
  DESIGN = 'design',
  PROJECT = 'project',
  TRANSFER = 'transfer',
  DATA = 'data',
  ADMIN = 'admin',
}

export default ModuleEnum
