// @ts-ignore
import texture1 from '@adonis/shared/images/textures/texture1.png'
// @ts-ignore
import texture2 from '@adonis/shared/images/textures/texture2.png'
// @ts-ignore
import texture3 from '@adonis/shared/images/textures/texture3.png'
// @ts-ignore
import texture4 from '@adonis/shared/images/textures/texture4.png'
// @ts-ignore
import texture5 from '@adonis/shared/images/textures/texture5.png'
// @ts-ignore
import texture6 from '@adonis/shared/images/textures/texture6.png'
// @ts-ignore
import texture7 from '@adonis/shared/images/textures/texture7.png'
// @ts-ignore
import texture8 from '@adonis/shared/images/textures/texture8.png'
// @ts-ignore
import texture9 from '@adonis/shared/images/textures/texture9.png'

enum TextureEnum {
  TEXTURE1 = texture1,
  TEXTURE2 = texture2,
  TEXTURE3 = texture3,
  TEXTURE4 = texture4,
  TEXTURE5 = texture5,
  TEXTURE6 = texture6,
  TEXTURE7 = texture7,
  TEXTURE8 = texture8,
  TEXTURE9 = texture9,
}

export default TextureEnum
