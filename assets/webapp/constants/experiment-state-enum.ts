enum ExperimentStateEnum {
  CREATED,
  VALIDATED,
  LOCKED,
  NON_UNLOCKABLE,
}

export default ExperimentStateEnum
