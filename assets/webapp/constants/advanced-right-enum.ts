enum AdvancedRightEnum {
  NONE = 'none',
  EDIT = 'edit',
  VIEW = 'view',
}

export default AdvancedRightEnum
