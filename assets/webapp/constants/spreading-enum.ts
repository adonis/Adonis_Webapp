enum SpreadingEnum {
  INDIVIDUAL = 'individual',
  UNIT_PLOT = 'unit_plot',
}

export default SpreadingEnum
