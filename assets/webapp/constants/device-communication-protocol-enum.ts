enum DeviceCommunicationProtocolEnum {
  NONE = '',
  RS232 = 'rs232',
  USB = 'usb',
  BLUETOOTH = 'bluetooth',
}

export default DeviceCommunicationProtocolEnum
