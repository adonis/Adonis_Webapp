import FilterComparatorsEnum from '@adonis/webapp/constants/filter-comparators-enum'
import FilterKeyEnum from '@adonis/webapp/constants/filters/filter-key-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import FilterTreeLeaf from '@adonis/webapp/form-interfaces/filters/filter-tree-leaf'
import FilterTreeNode from '@adonis/webapp/form-interfaces/filters/filter-tree-node'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import { v4 } from 'uuid'

export const FILTER_TREE_NODE_FACTORY = {
  constructFilterLeaf( filterKey: FilterKeyEnum, comparedValue: string, operator: FilterComparatorsEnum, text: string, variable?: SimpleVariable | SemiAutomaticVariable | GeneratorVariable ): FilterTreeLeaf {
    const res: FilterTreeLeaf = {
      id: v4(),
      type: filterKey,
      text,
      filter: {
        level: undefined,
        operator,
        value: comparedValue,
        function: undefined,
        variable,
      },
    }
    switch (filterKey) {
      case FilterKeyEnum.INDIVIDUAL_X_FILTER:
        res.filter.level = PathLevelEnum.INDIVIDUAL
        res.filter.function = this.generateNumericFunction( 'x' )
        break
      case FilterKeyEnum.INDIVIDUAL_Y_FILTER:
        res.filter.level = PathLevelEnum.INDIVIDUAL
        res.filter.function = this.generateNumericFunction( 'y' )
        break
      case FilterKeyEnum.UNIT_PLOT_NAME_FILTER:
        res.filter.level = PathLevelEnum.UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'name' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_X_FILTER:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateNumericFunction( 'x' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_Y_FILTER:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateNumericFunction( 'y' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_NAME_FILTER:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'name' )
        break
      case FilterKeyEnum.INDIVIDUAL_APPARITION_DATE_FILTER:
        res.filter.level = PathLevelEnum.INDIVIDUAL
        res.filter.function = this.generateDateFunction( 'appeared' )
        break
      case FilterKeyEnum.INDIVIDUAL_DISPARITION_DATE_FILTER:
        res.filter.level = PathLevelEnum.INDIVIDUAL
        res.filter.function = this.generateDateFunction( 'disappeared' )
        break
      case FilterKeyEnum.INDIVIDUAL_NUMBER_FILTER:
        res.filter.level = PathLevelEnum.INDIVIDUAL
        res.filter.function = this.generateNumericFunction( 'name', true)
        break
      case FilterKeyEnum.UNIT_PLOT_TREATMENT_FILTER:
        res.filter.level = PathLevelEnum.UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'treatmentName' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_TREATMENT_FILTER:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'treatmentName' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_DISPARITION_DATE_FILTER:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateDateFunction( 'disappeared' )
        break
      case FilterKeyEnum.EXPERIMENT_NAME_FILTER:
        res.filter.level = PathLevelEnum.EXPERIMENT
        res.filter.function = this.generateStringFunction( 'name' )
        break
      case FilterKeyEnum.BLOCK_NAME_FILTER:
        res.filter.level = PathLevelEnum.BLOCK
        res.filter.function = this.generateStringFunction( 'name' )
        break
      case FilterKeyEnum.SUB_BLOCK_NAME_FILTER:
        res.filter.level = PathLevelEnum.SUB_BLOCK
        res.filter.function = this.generateStringFunction( 'name' )
        break
      case FilterKeyEnum.UNIT_PLOT_SHORT_TREATMENT_FILTER:
        res.filter.level = PathLevelEnum.UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'treatmentShortName' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_SHORT_TREATMENT_FILTER:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'treatmentShortName' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_MODALITY_1:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'factor1' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_MODALITY_2:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'factor2' )
        break
      case FilterKeyEnum.SURFACIC_UNIT_PLOT_MODALITY_3:
        res.filter.level = PathLevelEnum.SURFACE_UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'factor3' )
        break
      case FilterKeyEnum.UNIT_PLOT_MODALITY_1:
        res.filter.level = PathLevelEnum.UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'factor1' )
        break
      case FilterKeyEnum.UNIT_PLOT_MODALITY_2:
        res.filter.level = PathLevelEnum.UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'factor2' )
        break
      case FilterKeyEnum.UNIT_PLOT_MODALITY_3:
        res.filter.level = PathLevelEnum.UNIT_PLOT
        res.filter.function = this.generateStringFunction( 'factor3' )
        break
      default:
        res.filter.level = variable.pathLevel
        res.filter.function = ((variable as SimpleVariable)?.type === VariableTypeEnum.INTEGER || (variable as SimpleVariable)?.type === VariableTypeEnum.REAL) ?
            this.generateNumericFunction( variable.iri, true ) :
            this.generateStringFunction( variable.iri )
    }
    return res
  },

  constructFilterNode( filterKey: FilterKeyEnum, text: string ): FilterTreeNode {
    return {
      id: v4(),
      type: filterKey,
      text,
      branches: [],
      maxChildren: filterKey === FilterKeyEnum.NOT_FILTER ? 1 : -1,
    }
  },

  generateStringFunction( stringParam: keyof {} ) {
    return ( object: any, comparator: FilterComparatorsEnum, value: string ) => {
      switch (comparator) {
        case FilterComparatorsEnum.EQUAL:
          return object[stringParam] === value
        case FilterComparatorsEnum.NOT_EQUAL:
          return object[stringParam] !== value
        case FilterComparatorsEnum.INCLUDE:
          return object[stringParam].includes( value )
      }
    }
  },

  generateDateFunction( dateParam: keyof {} ) {
    return ( object: any, comparator: FilterComparatorsEnum, value: string ) => {
      const dateValue = new Date( value )
      const dateAppeared = new Date( object[dateParam] )
      dateAppeared.setHours( 0, 0, 0, 0 )
      dateValue.setHours( 0, 0, 0, 0 )
      switch (comparator) {
        case FilterComparatorsEnum.EQUAL:
          return dateAppeared.getTime() === dateValue.getTime()
        case FilterComparatorsEnum.INF_EQUAL:
          return dateAppeared < dateValue || dateAppeared.getTime() === dateValue.getTime()
        case FilterComparatorsEnum.INF:
          return dateAppeared < dateValue
        case FilterComparatorsEnum.SUP:
          return dateAppeared > dateValue
        case FilterComparatorsEnum.SUP_EQUAL:
          return dateAppeared > dateValue || dateAppeared.getTime() === dateValue.getTime()
        case FilterComparatorsEnum.NOT_EQUAL:
          return dateAppeared.getTime() !== dateValue.getTime()
        case FilterComparatorsEnum.EMPTY:
          return object[dateParam] === undefined
      }
    }
  },

  generateNumericFunction( numericParam: keyof {}, parseValue = false ) {
    return ( object: any, comparator: FilterComparatorsEnum, value: string ) => {
      const numericValue = Number.parseInt( value, undefined )
      const individualValue = parseValue ? Number.parseInt( object[numericParam], undefined ) : object[numericParam]
      switch (comparator) {
        case FilterComparatorsEnum.EQUAL:
          return individualValue === numericValue
        case FilterComparatorsEnum.INF_EQUAL:
          return individualValue <= numericValue
        case FilterComparatorsEnum.INF:
          return individualValue < numericValue
        case FilterComparatorsEnum.SUP:
          return individualValue > numericValue
        case FilterComparatorsEnum.SUP_EQUAL:
          return individualValue >= numericValue
        case FilterComparatorsEnum.NOT_EQUAL:
          return individualValue !== numericValue
      }
    }
  },

  isFilterTreeNode( node: FilterTreeNode | FilterTreeLeaf ): node is FilterTreeNode {
    return node.type === FilterKeyEnum.NOT_FILTER ||
        node.type === FilterKeyEnum.AND_FILTER ||
        node.type === FilterKeyEnum.OR_FILTER
  },
}
