enum AlgorithmConditionEnum {
  CONDITION_MIN_2_FACTORS = 'min2Factors',
  CONDITION_CONSTANT_REPETITION_NUMBER = 'constantRepNumber',
  CONDITION_REPETITION_EQUALS_TREATMENTS = 'repEqTreatments',
}

export default AlgorithmConditionEnum
