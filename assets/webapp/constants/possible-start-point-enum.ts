enum PossibleStartPointEnum {
  BOT_LFT_DTOP = 'bottom left top',
  BOT_LFT_DRGT = 'bottom left right',
  BOT_RGT_DTOP = 'bottom right top',
  BOT_RGT_DLFT = 'bottom right left',
  TOP_LFT_DBOT = 'top left bottom',
  TOP_LFT_DRGT = 'top left right',
  TOP_RGT_DBOT = 'top right bottom',
  TOP_RGT_DLFT = 'top right left',
}

export default PossibleStartPointEnum
