enum PossibleMoveEnum {
  LIBRE = 'libre',
  GRAPHIQUE = 'graphique',
  ORDONNANCEUR = 'ordonnanceur',
  ALLER_SIMPLE = 'allerSimple',
  ALLER_RETOUR = 'allerRetour',
  SERPENTIN = 'serpentin',
  DEMI_SERPENTIN = 'demiSerpentin',
}

export default PossibleMoveEnum
