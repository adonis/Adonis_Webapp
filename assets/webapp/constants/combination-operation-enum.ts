enum CombinationOperationEnum {
  ADIDITION = '+',
  SUBSTRACTION = '-',
  MULTIPLICATION = '*',
  DIVISION = '/',
}

export default CombinationOperationEnum
