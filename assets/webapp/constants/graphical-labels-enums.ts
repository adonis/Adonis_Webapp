// Direct object attributes must match the enum value
export enum ExperimentLabelsEnum {

  NAME = 'name',
  STATE = 'state',
  BLOCK_NUMBER = 'blockNumber',
}

export enum BlockLabelsEnum {

  NUMBER = 'name',
  EXPERIMENT_NAME = 'experimentName',
  UP_NUMBER = 'unitPlotNumber',
}

export enum SubBlockLabelsEnum {

  NUMBER = 'name',
  BLOC_NAME = 'blocName',
  UP_NUMBER = 'unitPlotNumber',
}

export enum UnitPlotLabelsEnum {

  NUMBER = 'name',
  BLOC_NAME = 'blocName',
  SUB_BLOC_NAME = 'subBlocName',
  TREATMENT_L = 'treatmentName',
  TREATMENT_S = 'treatmentShortName',
  FACTOR_1 = 'factor1',
  FACTOR_2 = 'factor2',
  FACTOR_3 = 'factor3',
  INDIVIDUAL_NUMBER = 'individualNumber',
}

export enum SurfacicUnitPlotLabelsEnum {

  NUMBER = 'name',
  BLOC_NAME = 'blocName',
  SUB_BLOC_NAME = 'subBlocName',
  TREATMENT_L = 'treatmentName',
  TREATMENT_S = 'treatmentShortName',
  FACTOR_1 = 'factor1',
  FACTOR_2 = 'factor2',
  FACTOR_3 = 'factor3',
  X = 'x',
  Y = 'y',
  ID = 'identifier',
}

export enum IndividualLabelsEnum {

  NUMBER = 'name',
  X = 'x',
  Y = 'y',
  ID = 'identifier',
  UP_NUMBER = 'unitPlotNumber',
  BLOC_NAME = 'blocName',
  SUB_BLOC_NAME = 'subBlocName',
  TREATMENT_L = 'treatmentName',
  TREATMENT_S = 'treatmentShortName',
}
