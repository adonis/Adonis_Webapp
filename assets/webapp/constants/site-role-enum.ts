enum SiteRoleEnum {

  SITE_ADMIN = 'admin',
  SITE_MANAGER = 'manager',
  SITE_EXPERT = 'expert',
}

export default SiteRoleEnum
