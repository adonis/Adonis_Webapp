enum FilterComparatorsEnum {
  INCLUDE = 'Include',
  EQUAL = '=',
  INF_EQUAL = '<=',
  INF = '<',
  SUP = '>',
  SUP_EQUAL = '>=',
  NOT_EQUAL = '!=',
  EMPTY = 'Empty',
}

export default FilterComparatorsEnum
