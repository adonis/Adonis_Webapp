enum AlgorithmParameterEnum {
  SPECIAL_PARAM_NBR_TREATMENT = 'treatmentNumber',
  SPECIAL_PARAM_CONSTANT_REPETITION = 'repetitionNumber',
  SPECIAL_PARAM_NBR_FACTOR = 'factorNumber',
}

export default AlgorithmParameterEnum
