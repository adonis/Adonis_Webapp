enum FileTypeEnum {
  TYPE_EXPERIMENT = 'experiment',
  TYPE_EXPERIMENT_CSV = 'experiment_csv',
  TYPE_DATA_ENTRY_CSV = 'data_entry_csv',
  TYPE_PROTOCOL_CSV = 'protocol_csv',
  TYPE_PATH_BASE_CSV = 'path_base_csv',
  TYPE_PLATFORM = 'platform',
  TYPE_PROJECT_DATA_ZIP = 'project_data_zip',
  TYPE_VARIABLE_COLLECTION = 'variable_collection',
}

export default FileTypeEnum
