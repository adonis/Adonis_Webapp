enum PathLevelIconEnum {
  INDIVIDUAL = 'android',
  UNIT_PARCEL = 'folder',
  BLOC = 'folder',
  SUB_BLOC = 'folder',
  DEVICE = 'folder',
}

export default PathLevelIconEnum
