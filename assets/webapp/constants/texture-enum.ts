enum TextureEnum {
  TEXTURE1,
  TEXTURE2,
  TEXTURE3,
  TEXTURE4,
  TEXTURE5,
  TEXTURE6,
  TEXTURE7,
  TEXTURE8,
  TEXTURE9,
}

export default TextureEnum
