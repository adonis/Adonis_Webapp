enum Rs232StopBit {
  ONE = '1',
  ONE_HALF = '1.5',
  TWO = '2',
}

export default Rs232StopBit
