enum GraphicalOriginEnum {
  TOP_LEFT,
  TOP_RIGHT,
  BOTTOM_RIGHT,
  BOTTOM_LEFT,
}

export default GraphicalOriginEnum
