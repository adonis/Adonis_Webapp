enum Rs232BitFormat {
  EIGHT = 8,
  SEVEN = 7,
  SIX = 6,
  FIVE = 5,
}

export default Rs232BitFormat
