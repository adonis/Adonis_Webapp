enum ChangeTypeEnum {

  DEAD = 'dead',
  REPLANTED = 'replanted',

}

export default ChangeTypeEnum
