enum Rs232Parity {
  NONE = 'none',
  EVEN = 'even',
  ODD = 'odd',
  MARK = 'mark',
  SPACE = 'space',
}

export default Rs232Parity
