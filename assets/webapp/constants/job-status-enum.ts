enum JobStatusEnum {
  FILE_STATUS_ERROR = 'error',
  FILE_STATUS_RUNNING = 'running',
  FILE_STATUS_SUCCESS = 'success',
  FILE_STATUS_PENDING = 'pending',
}

export default JobStatusEnum
