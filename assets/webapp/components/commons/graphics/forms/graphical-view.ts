import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import Jimp from 'jimp/es'
import * as PIXI from 'pixi.js'

export function imagine(
  tilingSprite: any,
  graphicsMap: any,
  topBarContainer: any,
  leftBarContainer: any,
  rightBarContainer: any,
  bottomBarContainer: any,
  world: any,
  app: any,
  pictureScale: any,
  graphicalContext: any,
  graphicalOrigin: any,
  callbacks: {
    updateGraphicalOrigin: () => void,
    updateGraphicalBoundaries: () => void,
    center: () => void,
  },
): Promise<any> {

  tilingSprite.prepareForExport()
  graphicsMap.forEach((item: any): void => {
    item.prepareForExport()
  })

  const tmpBar = [
    topBarContainer.cloneForExport(),
    leftBarContainer.cloneForExport(),
    rightBarContainer.cloneForExport(),
    bottomBarContainer.cloneForExport(),
  ]
  world.addChild(...tmpBar)
  app.render()

  const scale = pictureScale

  const totalWidth = (graphicalContext.maxCellX - graphicalContext.minCellX + 3) * graphicalContext.cellW
  const totalHeight = (graphicalContext.maxCellY - graphicalContext.minCellY + 3) * graphicalContext.cellH

  let promise: Promise<any> = Promise.resolve(new Jimp(totalWidth * scale, totalHeight * scale))

  const gl = document.createElement('canvas').getContext('webgl')
  const tilesSize = gl.getParameter(gl.MAX_TEXTURE_SIZE)

  world.position.x -= (graphicalOrigin === GraphicalOriginEnum.TOP_RIGHT || graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT) ?
    graphicalContext.worldW - (graphicalContext.minCellX - 2) * graphicalContext.cellW - totalWidth :
    (graphicalContext.minCellX - 1) * graphicalContext.cellW

  const startingYpos = world.position.y
  for (let x = 0; x < totalWidth; x += tilesSize) {
    const requiresYInverse = graphicalOrigin === GraphicalOriginEnum.BOTTOM_LEFT || graphicalOrigin === GraphicalOriginEnum.BOTTOM_RIGHT
    const yTranslation = requiresYInverse
      ? graphicalContext.worldH - (graphicalContext.minCellY - 2) * graphicalContext.cellH - totalHeight
      : (graphicalContext.minCellY - 1) * graphicalContext.cellH
    world.position.y = startingYpos - yTranslation
    for (let y = 0; y < totalHeight; y += tilesSize) {
      const renderTexture = new PIXI.RenderTexture(
        new PIXI.BaseRenderTexture({
          width: x + tilesSize < totalWidth ? tilesSize : totalWidth - x,
          height: y + tilesSize < totalHeight ? tilesSize : totalHeight - y,
        }))
      app.renderer.render(world, {renderTexture})
      const sprite = PIXI.Sprite.from(renderTexture)
      sprite.scale.set(scale, scale)
      const image = app.renderer.plugins.extract.base64(sprite)

      promise = promise.then((baseImage: any) => {
        const buffer = Buffer.from(image.substr(image.indexOf(',')), 'base64')
        return Jimp.read(buffer)
          .then((img: any) => {
            baseImage.composite(img, x * scale, y * scale)
            return baseImage
          })
      })

      world.y -= tilesSize
    }
    world.x -= tilesSize
  }

  world.removeChild(...tmpBar)
  callbacks.updateGraphicalOrigin()
  callbacks.updateGraphicalBoundaries()
  callbacks.center()

  return promise.then(baseImage => baseImage.getBase64Async('image/png'))
}
