import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import DesignService from '@adonis/webapp/services/data-manipulation/design-service'
import ProjectService from '@adonis/webapp/services/data-manipulation/project-service'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import { Store } from 'vuex'

/**
 * Service to handle drag-n-drop hover ExplorerItems.
 */
export default class DragNDropService {

  allowedMatching: Map<ExplorerTypeEnum, ExplorerTypeEnum[]>

  constructor(
      private store: Store<any>,
      private designService: DesignService,
      private projectService: ProjectService,
  ) {
    this.allowedMatching = new Map()
    this.allowedMatching.set( ExplorerTypeEnum.PLATFORM, [ExplorerTypeEnum.UNLINKED_EXPERIMENT] )
    this.allowedMatching.set( ExplorerTypeEnum.VARIABLE_ALPHANUMERIC, [ExplorerTypeEnum.VALUE_LIST] )
    this.allowedMatching.set( ExplorerTypeEnum.PROJECT_VARIABLE_LIBRARY, [
      ExplorerTypeEnum.SIMPLE_VARIABLE,
      ExplorerTypeEnum.GENERATOR_VARIABLE,
      ExplorerTypeEnum.DEVICE,
      ExplorerTypeEnum.SEMI_AUTOMATIC_VARIABLE,
    ] )
    this.allowedMatching.set( ExplorerTypeEnum.PROJECT_STATE_CODE_LIBRARY, [ExplorerTypeEnum.STATE_CODE] )
    this.allowedMatching.set( ExplorerTypeEnum.PROJECT_PATH_LIBRARY, [ExplorerTypeEnum.PATH_BASE] )
  }

  can( sourceTypes: ExplorerTypeEnum[], targetTypes: ExplorerTypeEnum[] ): boolean {
    return sourceTypes.reduce( ( accSource, sourceType ) => accSource ||
            targetTypes.reduce( ( accTarget, targetType ) => accTarget ||
                    (this.allowedMatching.get( targetType )
                        ?.some( allowedType => allowedType === sourceType ) ?? false),
                false ),
        false )
  }

  do( item: ExplorerItem, target: ExplorerItem ): void {
    let targetName = target.name
    item.types.forEach( sourceType => target.types.forEach( targetType => {
      switch (targetType) {
        case ExplorerTypeEnum.PLATFORM:
          switch (sourceType) {
            case ExplorerTypeEnum.UNLINKED_EXPERIMENT:
              this.designService.linkExperimentToPlatform( item.name, targetName )
              break
          }
          break
        case ExplorerTypeEnum.VARIABLE_ALPHANUMERIC:
          switch (sourceType) {
            case ExplorerTypeEnum.VALUE_LIST:
              this.projectService.linkValueListToVariable( item.name, targetName )
              break
          }
          break
        case ExplorerTypeEnum.PROJECT_VARIABLE_LIBRARY:
          targetName = target.name.replace( /:variable/g, '' )
          switch (sourceType) {
            case ExplorerTypeEnum.SIMPLE_VARIABLE:
              this.projectService.addSimpleVariableToDataEntryProject( item.name, targetName )
              break
            case ExplorerTypeEnum.DEVICE:
              this.projectService.addDeviceToDataEntryProject( item.name, targetName )
              break
            case ExplorerTypeEnum.SEMI_AUTOMATIC_VARIABLE:
              this.projectService.addSemiAutomaticVariableToDataEntryProject( item.name, targetName )
              break
            case ExplorerTypeEnum.GENERATOR_VARIABLE:
              this.projectService.addGeneratorVariableToDataEntryProject( item.name, targetName )
              break
          }
          break
        case ExplorerTypeEnum.PROJECT_STATE_CODE_LIBRARY:
          targetName = target.name.replace( /:stateCode/g, '' )
          switch (sourceType) {
            case ExplorerTypeEnum.STATE_CODE:
              this.projectService.addStateCodeToDataEntryProject( item.name, targetName )
              break
          }
          break
        case ExplorerTypeEnum.PROJECT_PATH_LIBRARY:
          targetName = target.name.replace( /:path/g, '' )
          switch (sourceType) {
            case ExplorerTypeEnum.PATH_BASE:
              this.projectService.copyPathBase( item.name, targetName )
              break
          }
          break
      }
    }))
  }
}
