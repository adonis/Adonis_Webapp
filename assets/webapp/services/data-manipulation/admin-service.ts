import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'
import { SiteDto } from '@adonis/shared/models/dto/site-dto'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import RelUserSite from '@adonis/shared/models/rel-user-site'
import Site from '@adonis/shared/models/site'
import User from '@adonis/shared/models/user'
import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import AdvancedRightEnum from '@adonis/webapp/constants/advanced-right-enum'
import SiteRoleEnum from '@adonis/webapp/constants/site-role-enum'
import GraphicalConfigurationFormInterface from '@adonis/webapp/form-interfaces/graphical-configuration-form-interface'
import OpenSilexInstanceFormInterface from '@adonis/webapp/form-interfaces/open-silex-instance-form-interface'
import ParametersFormInterface from '@adonis/webapp/form-interfaces/parameters-form-interface'
import RoleUserSiteFormInterface from '@adonis/webapp/form-interfaces/role-user-site-form-interface'
import SiteDuplicationFormInterface from '@adonis/webapp/form-interfaces/site-duplication-form-interface'
import SiteFormInterface from '@adonis/webapp/form-interfaces/site-form-interface'
import SiteFormWithAdminInterface from '@adonis/webapp/form-interfaces/site-form-with-admin-interface'
import UserFormInterface from '@adonis/webapp/form-interfaces/user-form-interface'
import UserGroupFormInterface from '@adonis/webapp/form-interfaces/user-group-form-interface'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'
import UserGroup from '@adonis/webapp/models/user-group'
import AbstractService from '@adonis/webapp/services/data-manipulation/abstract-service'
import {
    OpenSilexInstanceExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { UserGroupExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/user-group-explorer-get-dto'
import { AdvancedGroupRightDto } from '@adonis/webapp/services/http/entities/simple-dto/advanced-group-right-dto'
import { AdvancedUserRightDto } from '@adonis/webapp/services/http/entities/simple-dto/advanced-user-right-dto'
import { GraphicalConfigurationDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-configuration-dto'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import { UserGroupDto } from '@adonis/webapp/services/http/entities/simple-dto/user-group-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import AdminExplorerProvider, {
    OPEN_SILEX_INSTANCE_EXPLORER_NAME,
    USER_GROUPS_EXPLORER_NAME
} from '@adonis/webapp/services/providers/explorer-providers/admin-explorer-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import { AxiosError, AxiosResponse } from 'axios'
import VueI18n from 'vue-i18n'
import { Store } from 'vuex'

export default class AdminService extends AbstractService {

    constructor(
        protected store: Store<any>,
        protected notifier: NotificationService,
        protected httpService: WebappHttpService,
        private transformerService: TransformerService,
        protected explorerProvider: AdminExplorerProvider,
        protected i18n: VueI18n,
    ) {
        super()
    }

    createUser(user: UserFormInterface): Promise<User> {
        const userDto = this.transformerService.userTransformer.formInterfaceToCreateDto(user)
        return this.httpService.post<UserDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.USER), userDto)
            .then(value => this.transformerService.userTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.CREATED)
                return value
            })
    }

    updateUser(userIri: string, userForm: UserFormInterface): Promise<User> {
        const userDto = this.transformerService.userTransformer.formInterfaceToCreateDto(userForm)
        return this.httpService.patch<UserDto>(userIri, userDto)
            .then(value => this.transformerService.userTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    updateUserAvatar(userIri: string, icon: string): Promise<User> {
        return this.httpService.patch<UserDto>(userIri, {avatar: icon})
            .then(value => this.transformerService.userTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    changePassword(userIri: string, password: string, currentPassword: string): Promise<User> {
        return this.httpService.patch<UserDto>(userIri, {password, currentPassword})
            .then(value => this.transformerService.userTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.MODIFIED)
                return value
            })
            .catch((error: AxiosError) => {
                if (error.response.status === 403) {
                    this.notifier.wrongPassword()
                }
                return Promise.reject(error)
            })
    }

    toggleUserActive(user: User): Promise<User> {
        const userDto: UserDto = {
            active: !user.active,
        }
        return this.httpService.patch<UserDto>(user.iri, userDto)
            .then(value => this.transformerService.userTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    renewUserPassword(user: User): Promise<User> {
        const userDto: UserDto = {
            renewPassword: true,
        }
        return this.httpService.patch<UserDto>(user.iri, userDto)
            .then(value => this.transformerService.userTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    deleteUser(user: User): void {
        // TODO action sur le serveur...
        this.notifier.objectSuccess(ObjectTypeEnum.USER, ObjectActionEnum.DELETED)
    }

    createSite(siteForm: SiteFormWithAdminInterface): Promise<Site> {
        const siteDto = this.transformerService.siteTransformer.formInterfaceToCreateDto(siteForm)
        siteDto.userRoles = [
            {user: siteForm.admin.iri, role: SiteRoleEnum.SITE_ADMIN},
        ]
        return this.httpService.post<SiteDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.SITE), siteDto)
            .then(value => this.transformerService.siteTransformer.dtoToObject(value.data))
            .then(site => Promise.all(site.userRoles.map(promise => promise.then(relUserSite => {
                    if (relUserSite.userIri === this.store.getters['connection/user'].iri) {
                        this.store.dispatch('navigation/add2roleSite', {roleSite: relUserSite})
                            .then()
                    }
                })))
                    .then(() => site),
            )
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.SITE, ObjectActionEnum.CREATED)
                return value
            })
    }

    cloneSite(siteForm: SiteDuplicationFormInterface): Promise<any> {
        const siteDto = {
            originSite: this.store.getters['navigation/getActiveRoleSite'].siteIri,
            ...siteForm,
        }
        return this.httpService.post<AbstractDtoObject & {
            result: string
        }>(this.httpService.getEndpointFromType(ObjectTypeEnum.CLONE_SITE), siteDto)
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.SITE, ObjectActionEnum.CREATED)
            })
    }

    updateSite(site: Site, siteForm: SiteFormInterface): Promise<Site> {
        const siteDto = this.transformerService.siteTransformer.formInterfaceToUpdateDto(siteForm)
        return this.httpService.patch<SiteDto>(site.iri, siteDto)
            .then(value => this.transformerService.siteTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.SITE, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    createRoleUserSite(role: RoleUserSiteFormInterface): Promise<RelUserSite> {
        const relUserSiteDto = this.transformerService.relUserSiteTransformer.formInterfaceToCreateDto(role)
        return this.httpService.post<RelUserSiteDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.ROLE_USER_SITE), relUserSiteDto)
            .then(value => this.transformerService.relUserSiteTransformer.dtoToObject(value.data))
            .then(roleSite => {
                if (role.user.iri === this.store.getters['connection/user'].iri) {
                    this.store.dispatch('navigation/add2roleSite', {roleSite})
                        .then()
                }
                return roleSite
            })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.ROLE_USER_SITE, ObjectActionEnum.CREATED)
                return value
            })
    }

    updateRelUserSite(role: RelUserSite, roleForm: RoleUserSiteFormInterface): Promise<RelUserSite> {
        const relUserSiteDto = this.transformerService.relUserSiteTransformer.formInterfaceToUpdateDto(roleForm)
        return this.httpService.patch<RelUserSiteDto>(role.iri, relUserSiteDto)
            .then(value => this.transformerService.relUserSiteTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.ROLE_USER_SITE, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    deleteRelUserSite(role: RelUserSite): Promise<void> {
        return this.httpService.delete(role.iri)
            .then(() => this.httpService.delete(role.iri)) // Double delete needed for soft delete
            .then(value => this.transformerService.relUserSiteTransformer.dtoToObject(value.data))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.ROLE_USER_SITE, ObjectActionEnum.DELETED))
    }

    deleteSite(site: Site, notify = true): Promise<void> {
        return this.httpService.delete(site.iri)
            .then(() => notify ? this.notifier.objectSuccess(ObjectTypeEnum.SITE, ObjectActionEnum.DELETED) : null)
    }

    createUserGroup(groupFormInterface: UserGroupFormInterface): Promise<UserGroup> {
        const groupDto = this.transformerService.userGroupTransformer.formInterfaceToCreateDto(groupFormInterface)
        groupDto.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
        return this.httpService.post<UserGroupDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.USER_GROUP), groupDto)
            .then(value => this.transformerService.userGroupTransformer.dtoToObject(value.data))
            .then(value => {
                this.httpService.get<UserGroupExplorerGetDto>(value.iri, {groups: ['admin_explorer_view']})
                    .then(response => this.store.dispatch('navigation/add2explorer', {
                        item: this.explorerProvider.constructUserGroupExplorerItem(response.data),
                        attachment: USER_GROUPS_EXPLORER_NAME,
                    }))
                return value
            })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER_GROUP, ObjectActionEnum.CREATED)
                return value
            })
    }

    deleteUserGroup(userGroupIri: string, notify = true) {
        return this.confirmDelete(() => this.httpService.delete(userGroupIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: userGroupIri}))
            .then(() => notify ? this.notifier.objectSuccess(ObjectTypeEnum.USER_GROUP, ObjectActionEnum.DELETED) : null))
    }

    updateUserGroup(groupFormInterface: UserGroupFormInterface, userGroupIri: string): Promise<UserGroup> {
        const groupDto = this.transformerService.userGroupTransformer.formInterfaceToUpdateDto(groupFormInterface)
        return this.httpService.patch<UserGroupDto>(userGroupIri, groupDto)
            .then(value => this.transformerService.userGroupTransformer.dtoToObject(value.data))
            .then(value => {
                this.httpService.get<UserGroupExplorerGetDto>(value.iri, {groups: ['admin_explorer_view']})
                    .then(response => this.store.dispatch('navigation/updateExplorer', {
                        item: this.explorerProvider.constructUserGroupExplorerItem(response.data),
                    }))
                return value
            })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.USER_GROUP, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    updateAdvancedUserRight(objectId: number,
                            classIdentifier: AdvancedRightClassIdentifierEnum,
                            rightMap: Map<number, AdvancedRightEnum>): Promise<any> {
        return this.httpService.delete(this.httpService.getEndpointFromType(ObjectTypeEnum.ADVANCED_RIGHT_USER), {
            classIdentifier,
            objectId,
        })
            .then(() => {
                const promises: Promise<any>[] = []
                rightMap.forEach((val, key) => {
                    const userRight: AdvancedUserRightDto = {
                        right: val,
                        userId: key,
                        objectId,
                        classIdentifier,
                    }
                    promises.push(this.httpService.post<AdvancedUserRightDto>(
                        this.httpService.getEndpointFromType(ObjectTypeEnum.ADVANCED_RIGHT_USER),
                        userRight,
                    ))
                })
                return Promise.all(promises)
            })
            .catch(reason => {
                this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                return reason
            })
    }

    updateAdvancedGroupRight(objectId: number,
                             classIdentifier: AdvancedRightClassIdentifierEnum,
                             rightMap: Map<number, AdvancedRightEnum>): Promise<any> {
        return this.httpService.delete(this.httpService.getEndpointFromType(ObjectTypeEnum.ADVANCED_RIGHT_GROUP), {
            classIdentifier,
            objectId,
        })
            .then(() => {
                const promises: Promise<any>[] = []
                rightMap.forEach((val, key) => {
                    const groupRight: AdvancedGroupRightDto = {
                        right: val,
                        groupId: key,
                        objectId,
                        classIdentifier,
                    }
                    promises.push(this.httpService.post<AdvancedGroupRightDto>(
                        this.httpService.getEndpointFromType(ObjectTypeEnum.ADVANCED_RIGHT_GROUP),
                        groupRight,
                    ))
                })
                return Promise.all(promises)
            })
            .catch(reason => {
                this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                return reason
            })
    }

    updateGraphicalConfiguration(config: GraphicalConfigurationFormInterface): Promise<any> {
        return this.httpService.getAll<GraphicalConfigurationDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.GRAPHICAL_CONFIGURATION), {site: this.store.getters['navigation/getActiveRoleSite'].siteIri})
            .then(result => {
                let promise: Promise<AxiosResponse<GraphicalConfigurationDto>>
                const updateDto = this.transformerService.graphicalConfigurationTransformer.formInterfaceToDto(config)
                if (result.data['hydra:totalItems'] === 1) {
                    promise = this.httpService.patch<GraphicalConfigurationDto>(result.data['hydra:member'][0]['@id'], updateDto)
                } else {
                    updateDto.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
                    promise = this.httpService.post<GraphicalConfigurationDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.GRAPHICAL_CONFIGURATION), updateDto)
                }
                promise
                    .then(response => this.transformerService.graphicalConfigurationTransformer.dtoToObject(response.data))
                    .then(value => {
                        this.notifier.objectSuccess(ObjectTypeEnum.GRAPHICAL_CONFIGURATION, ObjectActionEnum.MODIFIED)
                        return value
                    })
            })

    }

    deleteDisabledObject(objectIri: string, objextType: ObjectTypeEnum) {
        return this.confirmDelete(() => this.httpService.delete(objectIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: objectIri}))
            .then(() => this.notifier.objectSuccess(objextType, ObjectActionEnum.DELETED)))
    }

    restoreDisabledObject(objectIri: string, objextType: ObjectTypeEnum) {
        return this.httpService.patch(objectIri + '/restore', {})
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: objectIri}))
            .then(() => this.notifier.objectSuccess(objextType, ObjectActionEnum.MODIFIED))
    }

    updateParameters(parameters: ParametersFormInterface) {
        const parameterDto = this.transformerService.parametersTransformer.formInterfaceToUpdateDto(parameters)
        return this.httpService.patch(this.httpService.getEndpointFromType(ObjectTypeEnum.PARAMETERS) + '/1', parameterDto)
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.PARAMETERS, ObjectActionEnum.MODIFIED))
    }

    createOpenSilexInterface(openSilexInstanceFormInterface: OpenSilexInstanceFormInterface): Promise<OpenSilexInstance> {
        const openSilexInstanceDto = this.transformerService.openSilexInstanceTransformer.formInterfaceToCreateDto(openSilexInstanceFormInterface)
        openSilexInstanceDto.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
        return this.httpService.post<OpenSilexInstanceDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.OPEN_SILEX_INSTANCE), openSilexInstanceDto)
            .then(value => this.transformerService.openSilexInstanceTransformer.dtoToObject(value.data))
            .then(value => {
                this.httpService.get<OpenSilexInstanceExplorerGetDto>(value.iri, {groups: ['admin_explorer_view']})
                    .then(response => this.store.dispatch('navigation/add2explorer', {
                        item: this.explorerProvider.constructOpenSilexInstanceExplorerItem(response.data),
                        attachment: OPEN_SILEX_INSTANCE_EXPLORER_NAME,
                    }))
                return value
            })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.OPEN_SILEX_INSTANCE, ObjectActionEnum.CREATED)
                return value
            })
    }

    deleteOpenSilexInstance(openSilexInstanceIri: string, notify = true) {
        return this.confirmDelete(() => this.httpService.delete(openSilexInstanceIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: openSilexInstanceIri}))
            .then(() => notify ? this.notifier.objectSuccess(ObjectTypeEnum.OPEN_SILEX_INSTANCE, ObjectActionEnum.DELETED) : null))
    }

    updateOpenSilexInstance(openSilexInstanceFormInterface: OpenSilexInstanceFormInterface, openSilexInstanceIri: string): Promise<OpenSilexInstance> {
        const openSilexInstanceDto = this.transformerService.openSilexInstanceTransformer.formInterfaceToUpdateDto(openSilexInstanceFormInterface)
        return this.httpService.patch<OpenSilexInstanceDto>(openSilexInstanceIri, openSilexInstanceDto)
            .then(value => this.transformerService.openSilexInstanceTransformer.dtoToObject(value.data))
            .then(value => {
                this.httpService.get<OpenSilexInstanceExplorerGetDto>(value.iri, {groups: ['admin_explorer_view']})
                    .then(response => this.store.dispatch('navigation/updateExplorer', {
                        item: this.explorerProvider.constructOpenSilexInstanceExplorerItem(response.data),
                    }))
                return value
            })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.OPEN_SILEX_INSTANCE, ObjectActionEnum.MODIFIED)
                return value
            })
    }
}
