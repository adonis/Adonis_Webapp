import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { ConfirmAction } from '@adonis/shared/models/confirm-action'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import FileTypeEnum from '@adonis/webapp/constants/file-type-enum'
import CloneExperimentFormInterface from '@adonis/webapp/form-interfaces/clone-experiment-form-interface'
import ExperimentCsvBindingFormInterface from '@adonis/webapp/form-interfaces/experiment-csv-binding-form-interface'
import ExperimentFormInterface from '@adonis/webapp/form-interfaces/experiment-form-interface'
import FactorFormInterface from '@adonis/webapp/form-interfaces/factor-form-interface'
import IdentificationCodeFormInterface from '@adonis/webapp/form-interfaces/identification-code-form-interface'
import NorthIndicatorFormInterface from '@adonis/webapp/form-interfaces/north-indicator-form-interface'
import OezNatureFormInterface from '@adonis/webapp/form-interfaces/oez-nature-form-interface'
import PlatformFormInterface from '@adonis/webapp/form-interfaces/platform-form-interface'
import ProtocolCsvBindingFormInterface from '@adonis/webapp/form-interfaces/protocol-csv-binding-form-interface'
import ProtocolFormInterface from '@adonis/webapp/form-interfaces/protocol-form-interface'
import Experiment from '@adonis/webapp/models/experiment'
import Factor from '@adonis/webapp/models/factor'
import GraphicalTextZone from '@adonis/webapp/models/graphical-text-zone'
import NorthIndicator from '@adonis/webapp/models/north-indicator'
import OezNature from '@adonis/webapp/models/oez-nature'
import Platform from '@adonis/webapp/models/platform'
import Protocol from '@adonis/webapp/models/protocol'
import AbstractService from '@adonis/webapp/services/data-manipulation/abstract-service'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import { FactorExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/factor-explorer-get-dto'
import { OezNatureExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/oez-nature-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'
import { BlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/block-parent-view-get-dto'
import { IndividualParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/individual-parent-view-get-dto'
import { SubBlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/sub-block-parent-view-get-dto'
import {
    SurfacicUnitPlotParentViewGetDto,
} from '@adonis/webapp/services/http/entities/parent-view-get-dto/surfacic-unit-plot-parent-view-get-dto'
import { UnitPlotParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/unit-plot-parent-view-get-dto'
import { CloneExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/clone-experiment-dto'
import { CopyDto } from '@adonis/webapp/services/http/entities/simple-dto/copy-dto'
import { CreateDto } from '@adonis/webapp/services/http/entities/simple-dto/create-dto'
import { DeleteDto } from '@adonis/webapp/services/http/entities/simple-dto/delete-dto'
import { EditDto } from '@adonis/webapp/services/http/entities/simple-dto/edit-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { GraphicalTextZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-text-zone-dto'
import { NorthIndicatorDto } from '@adonis/webapp/services/http/entities/simple-dto/north-indicator-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { OezNatureDto } from '@adonis/webapp/services/http/entities/simple-dto/oez-nature-dto'
import { ParsingJobDto } from '@adonis/webapp/services/http/entities/simple-dto/parsing-job-dto'
import { PlaceExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/place-experiment-dto'
import { PlatformDto } from '@adonis/webapp/services/http/entities/simple-dto/platform-dto'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'
import { ExperimentExportDto } from '@adonis/webapp/services/http/open-silex-entities/experiment-export/experiment-export-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import DesignExplorerProvider, {
    DESIGN_LIST_OF_PLATFORM,
    EXPERIMENT_LIBRARY_EXPLORER_NAME,
    FACTOR_LIBRARY_EXPLORER_NAME,
    OEZ_NATURE_LIBRARY_EXPLORER_NAME,
    PROTOCOL_LIBRARY_EXPLORER_NAME,
} from '@adonis/webapp/services/providers/explorer-providers/design-explorer-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import VueI18n from 'vue-i18n'
import { Store } from 'vuex'

export default class DesignService extends AbstractService {

    constructor(
        protected store: Store<any>,
        protected notifier: NotificationService,
        protected httpService: WebappHttpService,
        private transformerService: TransformerService,
        protected explorerProvider: DesignExplorerProvider,
        protected i18n: VueI18n,
    ) {
        super()
    }

    createOezNature(oezNature: OezNatureFormInterface): Promise<OezNature> {
        const oezNatureDto: OezNatureDto = this.transformerService.oezNatureTransformer.formInterfaceToCreateDto(oezNature)
        oezNatureDto.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
        return this.httpService.post<OezNatureDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.OEZ_NATURE), oezNatureDto)
            .then(value => this.transformerService.oezNatureTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.OEZ_NATURE, ObjectActionEnum.CREATED)
                this.httpService.get<OezNatureExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                    .then(response => this.store.dispatch('navigation/add2explorer', {
                        item: this.explorerProvider.constructOezNatureExplorerItem(response.data),
                        attachment: OEZ_NATURE_LIBRARY_EXPLORER_NAME,
                    }))
                return value
            })
    }

    updateOezNature(oezNature: OezNatureFormInterface, oezNatureIri: string): Promise<OezNature> {
        const oezNatureDto: OezNatureDto = this.transformerService.oezNatureTransformer.formInterfaceToUpdateDto(oezNature)
        return this.httpService.patch<OezNatureDto>(oezNatureIri, oezNatureDto)
            .then(value => this.transformerService.oezNatureTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.OEZ_NATURE, ObjectActionEnum.CREATED)
                this.httpService.get<OezNatureExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                    .then(response => this.store.dispatch('navigation/updateExplorer', {
                        item: this.explorerProvider.constructOezNatureExplorerItem(response.data),
                    }))
                return value
            })
    }

    deleteOezNature(oezNatureIri: string) {
        return this.confirmDelete(() => this.httpService.delete(oezNatureIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: oezNatureIri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.OEZ_NATURE, ObjectActionEnum.DELETED)))
    }

    createFactor(factor: FactorFormInterface, addToLib = true, notify = true): Promise<Factor> {
        const factorDto = this.transformerService.factorTransformer.formInterfaceToCreateDto(factor)
        factorDto.site = addToLib ? this.store.getters['navigation/getActiveRoleSite'].siteIri : undefined
        return this.httpService.post<FactorDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.FACTOR), factorDto)
            .then(value => this.transformerService.factorTransformer.dtoToObject(value.data))
            .then(value => {
                if (notify) {
                    this.notifier.objectSuccess(ObjectTypeEnum.FACTOR, ObjectActionEnum.CREATED)
                }
                return value
            })
            .then(value => {
                if (addToLib) {
                    this.httpService.get<FactorExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                        .then(response => this.store.dispatch('navigation/add2explorer', {
                            item: this.explorerProvider.constructFactorExplorerItem(response.data),
                            attachment: FACTOR_LIBRARY_EXPLORER_NAME,
                        }))
                }
                return value

            })
    }

    updateFactor(factorIri: string, factorFormInterface: FactorFormInterface): Promise<Factor> {
        const factorDto = this.transformerService.factorTransformer.formInterfaceToUpdateDto(factorFormInterface)
        return this.httpService.patch<FactorDto>(factorIri, factorDto)
            .then(value => this.transformerService.factorTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.FACTOR, ObjectActionEnum.MODIFIED)
                return value
            })
            .then(value => {
                this.httpService.get<FactorExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                    .then(response => this.store.dispatch('navigation/updateExplorer', {
                        item: this.explorerProvider.constructFactorExplorerItem(response.data),
                    }))
                return value
            })
    }

    deleteFactor(factorIri: string) {
        return this.confirmDelete(() => this.httpService.delete(factorIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: factorIri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.FACTOR, ObjectActionEnum.DELETED)))
    }

    createProtocol(protocol: ProtocolFormInterface, addToLib = true): Promise<Protocol> {
        const protocolDto: ProtocolDto = this.transformerService.protocolTransformer.formInterfaceToCreateDto(protocol)
        protocolDto.site = addToLib ? this.store.getters['navigation/getActiveRoleSite'].siteIri : undefined
        return this.httpService.post<ProtocolDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PROTOCOL), protocolDto)
            .then(value => this.transformerService.protocolTransformer.dtoToObject(value.data))
            .then(value => {
                if (addToLib) {
                    this.notifier.objectSuccess(ObjectTypeEnum.PROTOCOL, ObjectActionEnum.CREATED)
                    this.httpService.get<ProtocolExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                        .then(response => this.store.dispatch('navigation/add2explorer', {
                            item: this.explorerProvider.constructProtocolExplorerItem(response.data),
                            attachment: PROTOCOL_LIBRARY_EXPLORER_NAME,
                        }))
                }
                return value
            })
    }

    updateProtocol(protocol: ProtocolFormInterface, protocolToModify: Protocol): Promise<any> {
        const protocolDto: ProtocolDto = this.transformerService.protocolTransformer.formInterfaceToCreateDto(protocol)
        protocolDto.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
        return this.httpService.put<ProtocolDto>(protocolToModify.iri, protocolDto)
            .then(value => this.transformerService.protocolTransformer.dtoToObject(value.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.PROTOCOL, ObjectActionEnum.CREATED)
                return value
            })
            .then(value => {
                this.httpService.get<ProtocolExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                    .then(response => this.store.dispatch('navigation/updateExplorer', {
                        item: this.explorerProvider.constructProtocolExplorerItem(response.data),
                    }))
                return value
            })

    }

    deleteProtocol(protocolIri: string) {
        return this.confirmDelete(() => this.httpService.delete(protocolIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: protocolIri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.PROTOCOL, ObjectActionEnum.DELETED)))
    }

    createExperiment(experimentFormInterface: ExperimentFormInterface, addToLib = true): Promise<Experiment> {
        return this.createProtocol(experimentFormInterface.protocol, false)
            .then(protocol => {
                const experimentPost = this.transformerService.experimentTransformer.formInterfaceToCreateDto(experimentFormInterface)
                experimentPost.protocol = protocol.iri
                experimentPost.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
                return this.httpService.post<ExperimentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT), experimentPost)
                    .then(response => this.transformerService.experimentTransformer.dtoToObject(response.data))
                    .then(value => {
                        this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.CREATED)
                        return value
                    })
                    .then(experiment => {
                        if (addToLib) {
                            this.httpService.get<ExperimentExplorerGetDto>(experiment.iri, {groups: ['design_explorer_view']})
                                .then(response => {
                                    const item = this.explorerProvider.constructExperimentExplorerItem(response.data, false)
                                    item.types = [ExplorerTypeEnum.UNLINKED_EXPERIMENT]
                                    return this.store.dispatch('navigation/add2explorer', {
                                        item,
                                        attachment: EXPERIMENT_LIBRARY_EXPLORER_NAME,
                                    })
                                })
                        }
                        return experiment
                    })
                    .catch(error => {
                        this.httpService.delete(protocol.iri)
                        throw error
                    })
            })
    }

    updateExperiment(experimentFormInterface: ExperimentFormInterface, experimentIri: string): Promise<Experiment> {
        const experimentDto = this.transformerService.experimentTransformer.formInterfaceToUpdateDto(experimentFormInterface)
        return this.httpService.patch<ExperimentDto>(experimentIri, experimentDto)
            .then(response => this.transformerService.experimentTransformer.dtoToObject(response.data))
            .then(value => {
                this.httpService.get<ExperimentExplorerGetDto>(experimentIri, {groups: ['design_explorer_view']})
                    .then(response => this.store.dispatch('navigation/updateExplorer', {
                        item: this.explorerProvider.constructExperimentExplorerItem(response.data, true),
                    }))
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    deleteExperiment(experimentIri: string) {
        return this.confirmDelete(() => this.httpService.delete(experimentIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: experimentIri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.DELETED)))
    }

    linkExperimentToPlatform(experimentIri: string, platformIri: string): Promise<Experiment> {
        const patch: ExperimentDto = {
            platform: platformIri,
        }

        return this.httpService.get<PlatformDto>(platformIri)
            .then(platformResponse => {
                if (platformResponse.data.experiments.length === 0) {
                    return this.httpService.patch<ExperimentDto>(experimentIri, patch)
                        .then(response => this.transformerService.experimentTransformer.dtoToObject(response.data))
                        .then(value => {
                            this.store.dispatch('navigation/removeExplorer', {itemName: value.iri})
                                .then(() =>
                                    this.httpService.get<PlatformExplorerGetDto>(platformIri, {groups: ['design_explorer_view']})
                                        .then(response => this.store.dispatch('navigation/updateExplorer', {
                                            item: this.explorerProvider.constructPlatformExplorerItem(response.data),
                                        })),
                                )
                            return value
                        })
                        .then(value => {
                            this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                            return value
                        })
                        .catch(reason => {
                            this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                            return reason
                        })
                } else {
                    this.notifier.objectFailure('notifications.alerts.failure.useLinkMenu')
                }
            })
    }

    unlinkExperimentFromPlatform(experimentIri: string): Promise<Experiment> {
        const patch: ExperimentDto = {
            site: this.store.getters['navigation/getActiveRoleSite'].siteIri,
        }
        return this.httpService.patch<ExperimentDto>(experimentIri, patch)
            .then(response => this.transformerService.experimentTransformer.dtoToObject(response.data))
            .then(value => {
                this.store.dispatch('navigation/removeExplorer', {itemName: value.iri})
                    .then(() =>
                        this.httpService.get<ExperimentExplorerGetDto>(value.iri, {groups: ['design_explorer_view']})
                            .then(response => {
                                const item = this.explorerProvider.constructExperimentExplorerItem(response.data, false)
                                item.types = [ExplorerTypeEnum.UNLINKED_EXPERIMENT]
                                return this.store.dispatch('navigation/add2explorer', {
                                    item,
                                    attachment: EXPERIMENT_LIBRARY_EXPLORER_NAME,
                                })
                            }),
                    )
                return value
            })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value
            })
            .catch(reason => {
                this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                return reason
            })
    }

    createPlatform(platform: PlatformFormInterface): Promise<Platform> {
        const platformPost = this.transformerService.platformTransformer.formInterfaceToCreateDto(platform)
        platformPost.site = this.store.getters['navigation/getActiveRoleSite'].siteIri
        return this.httpService.post<PlatformDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM), platformPost)
            .then(response => this.transformerService.platformTransformer.dtoToObject(response.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.PLATFORM, ObjectActionEnum.CREATED)
                return value
            })
            .then(experiment => {
                this.httpService.get<PlatformExplorerGetDto>(experiment.iri, {groups: ['design_explorer_view']})
                    .then(response => this.store.dispatch('navigation/add2explorer', {
                        item: this.explorerProvider.constructPlatformExplorerItem(response.data),
                        attachment: DESIGN_LIST_OF_PLATFORM,
                    }))
                return experiment
            })
    }

    updatePlatform(platform: PlatformFormInterface, platformIri: string): Promise<Platform> {
        const platformDto = this.transformerService.platformTransformer.formInterfaceToUpdateDto(platform)
        return this.httpService.patch<PlatformDto>(platformIri, platformDto)
            .then(response => this.transformerService.platformTransformer.dtoToObject(response.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.PLATFORM, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    deletePlatform(platformIri: string): Promise<any> {
        return this.confirmDelete(() => this.httpService.delete(platformIri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: platformIri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.PLATFORM, ObjectActionEnum.DELETED)))
    }

    constructParentExplorerItems(iri: string): Promise<any> {
        const blockCallback = (item: BlockParentViewGetDto) => {
            return this.store.dispatch('navigation/add2explorer', {
                item: this.explorerProvider.constructBlockExplorerItem(item),
                attachment: item.experiment['@id'],
            }).catch(() => undefined)
        }
        const subBlockCallback = (item: SubBlockParentViewGetDto) => {
            return blockCallback(item.block)
                .finally(() => this.store.dispatch('navigation/add2explorer', {
                    item: this.explorerProvider.constructSubBlockExplorerItem(item),
                    attachment: item.block['@id'],
                }).catch(() => undefined))
        }
        const unitPlotCallback = (item: UnitPlotParentViewGetDto) => {
            return (!!item.subBlock ? subBlockCallback(item.subBlock) : blockCallback(item.block))
                .finally(() => this.store.dispatch('navigation/add2explorer', {
                    item: this.explorerProvider.constructUnitPlotExplorerItem(item),
                    attachment: (!!item.subBlock ? item.subBlock : item.block)['@id'],
                }).catch(() => undefined))
        }
        const surfacicUnitPlotCallback = (item: SurfacicUnitPlotParentViewGetDto) => {
            return (!!item.subBlock ? subBlockCallback(item.subBlock) : blockCallback(item.block))
                .finally(() => this.store.dispatch('navigation/add2explorer', {
                    item: this.explorerProvider.constructSurfacicUnitPlotExplorerItem(item),
                    attachment: (!!item.subBlock ? item.subBlock : item.block)['@id'],
                }).catch(() => undefined))
        }
        const individualCallback = (item: IndividualParentViewGetDto) => {
            return unitPlotCallback(item.unitPlot)
                .finally(() => this.store.dispatch('navigation/add2explorer', {
                    item: this.explorerProvider.constructIndividualExplorerItem(item),
                    attachment: item.unitPlot['@id'],
                }).catch(() => undefined))
        }
        if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.BLOCK))) {
            return this.httpService.get<BlockParentViewGetDto>(iri, {groups: ['design_explorer_view', 'parent_view']})
                .then(response => blockCallback(response.data))
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SUB_BLOCK))) {
            return this.httpService.get<SubBlockParentViewGetDto>(iri, {groups: ['design_explorer_view', 'parent_view']})
                .then(response => subBlockCallback(response.data))
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.UNIT_PLOT))) {
            return this.httpService.get<UnitPlotParentViewGetDto>(iri, {groups: ['design_explorer_view', 'parent_view']})
                .then(response => unitPlotCallback(response.data))
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SURFACIC_UNIT_PLOT))) {
            return this.httpService.get<SurfacicUnitPlotParentViewGetDto>(iri, {groups: ['design_explorer_view', 'parent_view']})
                .then(response => surfacicUnitPlotCallback(response.data))
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.INDIVIDUAL))) {
            return this.httpService.get<IndividualParentViewGetDto>(iri, {groups: ['design_explorer_view', 'parent_view']})
                .then(response => individualCallback(response.data))
        }
    }

    private sortBuisnessObjectIriTab(dirtyIriTab: string[]) {
        return dirtyIriTab
            .reduce((acc: any, iri) => {
                if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM))) {
                    return {...acc, [PathLevelEnum.PLATFORM]: [...acc[PathLevelEnum.PLATFORM], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT))) {
                    return {...acc, [PathLevelEnum.EXPERIMENT]: [...acc[PathLevelEnum.EXPERIMENT], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.BLOCK))) {
                    return {...acc, [PathLevelEnum.BLOCK]: [...acc[PathLevelEnum.BLOCK], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SUB_BLOCK))) {
                    return {...acc, [PathLevelEnum.SUB_BLOCK]: [...acc[PathLevelEnum.SUB_BLOCK], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.UNIT_PLOT))) {
                    return {...acc, [PathLevelEnum.UNIT_PLOT]: [...acc[PathLevelEnum.UNIT_PLOT], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SURFACIC_UNIT_PLOT))) {
                    return {...acc, [PathLevelEnum.SURFACE_UNIT_PLOT]: [...acc[PathLevelEnum.SURFACE_UNIT_PLOT], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.INDIVIDUAL))) {
                    return {...acc, [PathLevelEnum.INDIVIDUAL]: [...acc[PathLevelEnum.INDIVIDUAL], iri]}
                } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.OEZ))) {
                    return {...acc, [PathLevelEnum.OEZ]: [...acc[PathLevelEnum.OEZ], iri]}
                } else {
                    return acc // If the iri don't match any buisness object
                }
            }, {
                [PathLevelEnum.PLATFORM]: [],
                [PathLevelEnum.EXPERIMENT]: [],
                [PathLevelEnum.BLOCK]: [],
                [PathLevelEnum.SUB_BLOCK]: [],
                [PathLevelEnum.UNIT_PLOT]: [],
                [PathLevelEnum.SURFACE_UNIT_PLOT]: [],
                [PathLevelEnum.INDIVIDUAL]: [],
                [PathLevelEnum.OEZ]: [],
            })
    }

    public getPathLevelFromIri(iri: string) {

        if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM))) {
            return PathLevelEnum.PLATFORM
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT))) {
            return PathLevelEnum.EXPERIMENT
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.BLOCK))) {
            return PathLevelEnum.BLOCK
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SUB_BLOCK))) {
            return PathLevelEnum.SUB_BLOCK
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.UNIT_PLOT))) {
            return PathLevelEnum.UNIT_PLOT
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SURFACIC_UNIT_PLOT))) {
            return PathLevelEnum.SURFACE_UNIT_PLOT
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.INDIVIDUAL))) {
            return PathLevelEnum.INDIVIDUAL
        } else {
            return null
        }
    }

    moveBusinessObjects(dirtyIriTab: string[], dx: number, dy: number): Promise<EditDto> {
        const res = this.sortBuisnessObjectIriTab(dirtyIriTab)
        return this.httpService.patch<EditDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.EDIT), {objectIris: res, dx, dy})
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
            .catch(reason => {
                if (reason.response.data['hydra:description'] === 'Locked experiment') {
                    this.notifier.objectFailure('notifications.alerts.failure.experimentStatusIncompatible')
                } else if (reason.response.status === 500) {
                    this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                }
                return reason
            })
    }

    copyBusinessObjects(dirtyIriTab: string[], dx: number, dy: number, newParentIri: string): Promise<CopyDto> {
        const res = this.sortBuisnessObjectIriTab(dirtyIriTab)
        return this.httpService.patch<CopyDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.COPY), {
            objectIris: res,
            dx,
            dy,
            parent: newParentIri
        })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
            .catch(reason => {
                if (reason.response.data['hydra:description'] === 'Locked experiment') {
                    this.notifier.objectFailure('notifications.alerts.failure.experimentStatusIncompatible')
                } else if (reason.response.status === 400) {
                    this.notifier.objectFailure('notifications.alerts.failure.illegalCopy')
                }
                return reason
            })
    }

    placeExperiment(experimentIri: string, dx: number, dy: number, platformIri: string): Promise<PlaceExperimentDto> {
        return this.httpService.patch<PlaceExperimentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PLACE_EXPERIMENT), {
            experiment: experimentIri,
            dx,
            dy,
            platform: platformIri,
        })
            .then(value => {
                this.store.dispatch('navigation/removeExplorer', {itemName: experimentIri})
                    .then(() => {
                        if (!!platformIri) {
                            this.httpService.get<PlatformExplorerGetDto>(platformIri, {groups: ['design_explorer_view']})
                                .then(response => this.store.dispatch('navigation/updateExplorer', {
                                    item: this.explorerProvider.constructPlatformExplorerItem(response.data),
                                }))
                        }
                    })
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
    }

    deleteBusinessObjects(dirtyIriTab: string[]): Promise<DeleteDto> {
        const res = this.sortBuisnessObjectIriTab(dirtyIriTab)
        return this.httpService.patch<EditDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DELETE), {objectIris: res})
            .then(value => {
                dirtyIriTab.forEach(iri => {
                    try {
                        this.store.dispatch('navigation/removeExplorer', {itemName: iri})
                    } finally {
                    }
                })
                this.notifier.objectSuccess(ObjectTypeEnum.EDIT, ObjectActionEnum.DELETED)
                return value.data
            })
            .catch(reason => {
                if (reason.response.data['hydra:description'] === 'Locked experiment') {
                    this.notifier.objectFailure('notifications.alerts.failure.experimentStatusIncompatible')
                } else if (reason.response.status === 500) {
                    this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                }
                return reason
            })
    }

    restoreBusinessObjects(dirtyIriTab: string[]): Promise<DeleteDto> {
        const res = this.sortBuisnessObjectIriTab(dirtyIriTab)
        return this.httpService.patch<EditDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.RESTORE), {objectIris: res})
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EDIT, ObjectActionEnum.CREATED)
                return value.data
            })
            .catch(reason => {
                if (reason.response.data['hydra:description'] === 'Locked experiment') {
                    this.notifier.objectFailure('notifications.alerts.failure.experimentStatusIncompatible')
                } else if (reason.response.status === 500) {
                    this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                }
                return reason
            })
    }

    createBusinessObjects(x1: number, x2: number, y1: number, y2: number, pathLevel: PathLevelEnum, parentIri: string, treatmentIri: string, useSubBlock: boolean, natureIri?: string): Promise<CreateDto> {
        return this.httpService.patch<CreateDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.CREATE), {
            parent: parentIri,
            treatment: treatmentIri,
            nature: natureIri,
            useSubBlock,
            x1,
            x2,
            y1,
            y2,
            pathLevel,
        })
            .then(value => {
                this.httpService.get<any>(parentIri, {groups: ['design_explorer_view']})
                    .then(parentDto => {
                        let explorerItem: ExplorerItem
                        switch (this.getPathLevelFromIri(parentIri)) {
                            case PathLevelEnum.EXPERIMENT:
                                explorerItem = this.explorerProvider.constructExperimentExplorerItem(parentDto.data, false)
                                break
                            case PathLevelEnum.BLOCK:
                                explorerItem = this.explorerProvider.constructBlockExplorerItem(parentDto.data)
                                break
                            case PathLevelEnum.SUB_BLOCK:
                                explorerItem = this.explorerProvider.constructSubBlockExplorerItem(parentDto.data)
                                break
                            case PathLevelEnum.UNIT_PLOT:
                                explorerItem = this.explorerProvider.constructUnitPlotExplorerItem(parentDto.data)
                                break
                        }
                        explorerItem.collapsed = false
                    })
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
            .catch(reason => {
                if (reason.response.data['hydra:description'] === 'Locked experiment') {
                    this.notifier.objectFailure('notifications.alerts.failure.experimentStatusIncompatible')
                } else if (reason.response.status === 500) {
                    this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                }
                return reason
            })
    }

    updateNote(noteIri: string, note: { text: string }) {
        this.httpService.patch<NoteDto>(noteIri, note)
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.NOTE, ObjectActionEnum.MODIFIED)
            })
    }

    deleteNote(noteIri: string) {
        this.httpService.patch<NoteDto>(noteIri, {deleted: true})
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.NOTE, ObjectActionEnum.DELETED)
            })
    }

    changeBusinessObjectColor(dirtyIriTab: string[], newColor: number): Promise<any> {
        const res = this.sortBuisnessObjectIriTab(dirtyIriTab)
        return this.httpService.patch<EditDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.EDIT), {
            objectIris: res,
            color: newColor,
        })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
            .catch(reason => {
                if (reason.response.data['hydra:description'] === 'Locked experiment') {
                    this.notifier.objectFailure('notifications.alerts.failure.experimentStatusIncompatible')
                } else if (reason.response.status === 500) {
                    this.notifier.objectFailure('notifications.alerts.failure.insufficientRight')
                }
                return Promise.reject(reason)
            })
    }

    changeBusinessObjectDeadStatus(objectIri: string, dead: boolean): Promise<any> {
        return this.httpService.patch<any>(objectIri, {
            dead,
        })
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
    }

    importExperimentXml() {
        this.importFile(FileTypeEnum.TYPE_EXPERIMENT, IconEnum.EXPERIMENT, EXPERIMENT_LIBRARY_EXPLORER_NAME, (job: ParsingJobDto) => {
            this.httpService.get<ExperimentExplorerGetDto>(job.objectIri, {groups: ['design_explorer_view']})
                .then(experiment => {
                    const item = this.explorerProvider.constructExperimentExplorerItem(experiment.data, false)
                    item.types = [ExplorerTypeEnum.UNLINKED_EXPERIMENT]
                    return this.store.dispatch('navigation/add2explorer', {
                        item,
                        attachment: EXPERIMENT_LIBRARY_EXPLORER_NAME,
                    })
                })
        })
    }

    importPlatformXml() {
        this.importFile(FileTypeEnum.TYPE_PLATFORM, IconEnum.PLATFORM, DESIGN_LIST_OF_PLATFORM, (job: ParsingJobDto) => {
            this.httpService.get<PlatformExplorerGetDto>(job.objectIri, {groups: ['design_explorer_view']})
                .then(platform => {
                    const item = this.explorerProvider.constructPlatformExplorerItem(platform.data)
                    return this.store.dispatch('navigation/add2explorer', {
                        item,
                        attachment: DESIGN_LIST_OF_PLATFORM,
                    })
                })
        })
    }

    importExperimentCsv(csvBindings: ExperimentCsvBindingFormInterface): Promise<any> {
        return this.importFile(FileTypeEnum.TYPE_EXPERIMENT_CSV, IconEnum.EXPERIMENT, EXPERIMENT_LIBRARY_EXPLORER_NAME, (job: ParsingJobDto) => {
            this.httpService.get<ExperimentExplorerGetDto>(job.objectIri, {groups: ['design_explorer_view']})
                .then(experiment => {
                    const item = this.explorerProvider.constructExperimentExplorerItem(experiment.data, false)
                    item.types = [ExplorerTypeEnum.UNLINKED_EXPERIMENT]
                    return this.store.dispatch('navigation/add2explorer', {
                        item,
                        attachment: EXPERIMENT_LIBRARY_EXPLORER_NAME,
                    })
                })
        }, csvBindings)
    }

    importProtocolCsv(csvBindings: ProtocolCsvBindingFormInterface): Promise<any> {
        return this.importFile(FileTypeEnum.TYPE_PROTOCOL_CSV, IconEnum.PROTOCOL, PROTOCOL_LIBRARY_EXPLORER_NAME, (job: ParsingJobDto) => {
            this.httpService.get<ProtocolExplorerGetDto>(job.objectIri, {groups: ['design_explorer_view']})
                .then(protocol => {
                    const item = this.explorerProvider.constructProtocolExplorerItem(protocol.data, false)
                    return this.store.dispatch('navigation/add2explorer', {
                        item,
                        attachment: PROTOCOL_LIBRARY_EXPLORER_NAME,
                    })
                })
        }, csvBindings)
    }

    updateIdentificationCode(identificationInterface: IdentificationCodeFormInterface, experimentIri: string): Promise<any> {
        const formData = new FormData()
        formData.append('importFromFile', identificationInterface.importFromFile ? 'true' : 'false')
        formData.append('file', identificationInterface.file)
        formData.append('csvSeparator', identificationInterface.separator)
        formData.append('xColumn', identificationInterface.xColumn)
        formData.append('yColumn', identificationInterface.yColumn)
        formData.append('codeColumn', identificationInterface.codeColumn)
        formData.append('experiment', experimentIri)
        return this.httpService.post<any>(this.httpService.getEndpointFromType(ObjectTypeEnum.IDENTIFICATION_CODE_UPDATE), formData)
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.MODIFIED)
                return value.data
            })
    }

    exportToCsv(objectIri: string, objectname: string) {
        return this.httpService.get<any>(objectIri + '/exportCsv')
            .then(result => {

                const lines = result.data.split(/\r\n|\n/)
                const headers = lines[0].split(';')
                    .map((title: string) => this.i18n.t('files.csv.' + title))
                lines[0] = headers.join(';')

                const blob = new Blob([lines.join('\r\n')], {type: 'text/csv'})
                const elem = window.document.createElement('a')
                elem.href = window.URL.createObjectURL(blob)
                elem.download = objectname + '.csv'
                document.body.appendChild(elem)
                elem.click()
                document.body.removeChild(elem)
            })
    }

    exportExperimentXml(experimentId: number, experimentIri: string): Promise<any> {
        return this.httpService.getFile('/api/download/experiment/' + experimentId, 'application/xml', experimentIri)
    }

    exportPlatformXml(platformId: number, platformIri: string): Promise<any> {
        const confirmAction: AsyncConfirmPopup = new AsyncConfirmPopup(
            'navigation.menus.platform.xmlExportConfirmPopup.title',
            ['navigation.menus.platform.xmlExportConfirmPopup.message'],
            [
                new ConfirmAction(
                    'navigation.menus.platform.xmlExportConfirmPopup.confirm',
                    'primary',
                    () => this.httpService.getFile('/api/download/platformWithData/' + platformId, 'application/xml', platformIri),
                ),
                new ConfirmAction(
                    'navigation.menus.platform.xmlExportConfirmPopup.decline',
                    'secondary',
                    () => this.httpService.getFile('/api/download/platform/' + platformId, 'application/xml', platformIri),
                ),
            ],
        )
        return this.store.dispatch('confirm/askConfirm', confirmAction)
    }

    swapTreatments(unitPlotIri1: string, treatmentIri1: string, unitPlotIri2: string, treatmentIri2: string) {
        return Promise.all([
            this.httpService.patch<any>(unitPlotIri1, {treatment: treatmentIri2}, {groups: ['design_explorer_view']}),
            this.httpService.patch<any>(unitPlotIri2, {treatment: treatmentIri1}, {groups: ['design_explorer_view']}),
        ])
            .then(results => {
                results.forEach(result => this.store.dispatch('navigation/updateExplorer', {
                    item: !!result.data.x ?
                        this.explorerProvider.constructSurfacicUnitPlotExplorerItem(result.data) :
                        this.explorerProvider.constructUnitPlotExplorerItem(result.data),
                }))
                this.notifier.objectSuccess(ObjectTypeEnum.UNIT_PLOT, ObjectActionEnum.MODIFIED)
            })
    }

    createNorthIndicator(platformIri: string, northIndicatorFormInterface: NorthIndicatorFormInterface): Promise<NorthIndicator> {
        const northIndicatorDto = this.transformerService.northIndicatorTransformer.formInterfaceToCreateDto(northIndicatorFormInterface)
        northIndicatorDto.platform = platformIri
        return this.httpService.post<NorthIndicatorDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.NORTH_INDICATOR), northIndicatorDto)
            .then(response => this.transformerService.northIndicatorTransformer.dtoToObject(response.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.NORTH_INDICATOR, ObjectActionEnum.CREATED)
                return value
            })
    }

    updateNorthIndicator(northIndicatorIri: string, northIndicatorFormInterface: NorthIndicatorFormInterface) {
        const northIndicatorDto = this.transformerService.northIndicatorTransformer.formInterfaceToCreateDto(northIndicatorFormInterface)
        return this.httpService.patch <NorthIndicatorDto>(northIndicatorIri, northIndicatorDto)
            .then(response => this.transformerService.northIndicatorTransformer.dtoToObject(response.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.NORTH_INDICATOR, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    deleteNorthIndicator(northIndicatorIri: string) {
        return this.httpService.delete(northIndicatorIri)
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.NORTH_INDICATOR, ObjectActionEnum.DELETED)
            })
    }

    createTextZone(platformIri: string, textZoneInfos: {
        x: number,
        y: number,
        width: number,
        height: number
    }): Promise<GraphicalTextZone> {
        const textZoneDto: GraphicalTextZoneDto = {
            text: 'Description',
            x: textZoneInfos.x,
            y: textZoneInfos.y,
            width: Math.floor(textZoneInfos.width),
            height: Math.floor(textZoneInfos.height),
            size: 26,
            platform: platformIri,
        }
        return this.httpService.post<GraphicalTextZoneDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.GRAPHICAL_TEXT_ZONE), textZoneDto)
            .then(response => this.transformerService.graphicalTextZoneTransformer.dtoToObject(response.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.GRAPHICAL_TEXT_ZONE, ObjectActionEnum.CREATED)
                return value
            })
    }

    updateTextZone(textZoneIri: string, textZoneDto: GraphicalTextZoneDto): Promise<GraphicalTextZone> {
        return this.httpService.patch <GraphicalTextZoneDto>(textZoneIri, textZoneDto)
            .then(response => this.transformerService.graphicalTextZoneTransformer.dtoToObject(response.data))
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.GRAPHICAL_TEXT_ZONE, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    deleteTextZone(textZoneIri: string) {
        return this.httpService.delete(textZoneIri)
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.GRAPHICAL_TEXT_ZONE, ObjectActionEnum.DELETED)
            })
    }

    duplicateExperiment(cloneInterface: CloneExperimentFormInterface): Promise<any> {
        const cloneDto = this.transformerService.cloneExperimentTransformer.formInterfaceToCreateDto(cloneInterface)
        return this.httpService.post<CloneExperimentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.CLONE_EXPERIMENT), cloneDto)
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.CREATED)
                this.httpService.get<ExperimentExplorerGetDto>(value.data.result, {groups: ['design_explorer_view']})
                    .then(response => {
                        const projectExplorer = this.explorerProvider.constructExperimentExplorerItem(response.data, false)
                        this.store.dispatch('navigation/add2explorer', {
                            item: projectExplorer,
                            attachment: EXPERIMENT_LIBRARY_EXPLORER_NAME,
                        })
                    })
            })
    }

    updateOpenSilexUris(experimentIri: string, experimentDto: ExperimentExportDto): Promise<any> {
        return this.httpService.patch<ExperimentDto>(experimentIri + '/opensilexUris', experimentDto)
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.CREATED)
            })
    }
}
