import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { VERSION } from '@adonis/shared/env'
import Site from '@adonis/shared/models/site'
import ChangeTypeEnum from '@adonis/webapp/constants/change-type-enum'
import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import PossibleMoveEnum from '@adonis/webapp/constants/possible-move-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import AbstractService from '@adonis/webapp/services/data-manipulation/abstract-service'
import { DataEntrySynthesisReportDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/data-entry-synthesis-report-dto'
import { ModificationReportDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/modification-report-dto'
import { PlatformSynthesisReportDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/platform-synthesis-report-dto'
import { ProjectSynthesisReportDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/project-synthesis-report-dto'
import { ProtocolSynthesisReportDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/protocol-synthesis-report-dto'
import { VariableRDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/variable-r-dto'
import { VariableSynthesisReportDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/variable-synthesis-report-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import { AxiosResponse } from 'axios'
import Chart from 'chart.js'
import jsPDF from 'jspdf'
import 'jspdf-autotable'
import autoTable from 'jspdf-autotable'
import { max } from 'mathjs'
import moment from 'moment'
import VueI18n from 'vue-i18n'
import { Store } from 'vuex'

// tslint:disable:max-line-length
// tslint:disable:newline-per-chained-call

export default class PrintService extends AbstractService {

    protected PDF_X_MARGIN = 10
    protected PDF_Y_MARGIN = 15
    protected PDF_LINE_HEIGHT = 5
    protected PDF_FONT_SIZE = 10

    constructor(
        protected store: Store<any>,
        protected notifier: NotificationService,
        protected httpService: WebappHttpService,
        protected i18n: VueI18n,
    ) {
        super()
    }

    protected writePdfFooter(doc: jsPDF, siteName: string) {
        doc.setFontSize(10);
        (new Array(doc.getNumberOfPages()).fill(0)).forEach((item, index) => doc.line(this.PDF_X_MARGIN, doc.internal.pageSize.height - 15, doc.internal.pageSize.width - this.PDF_X_MARGIN, doc.internal.pageSize.height - 15)
            .setPage(index + 1)
            .text('V:' + VERSION, this.PDF_X_MARGIN, doc.internal.pageSize.height - 10)
            .text(this.i18n.t('files.pdf.footer.site') + siteName, doc.internal.pageSize.width - this.PDF_X_MARGIN, doc.internal.pageSize.height - 10, {align: 'right'})
            .text(this.i18n.t('files.pdf.footer.printDate') + moment(new Date()).format('YYYY-MM-DD'), this.PDF_X_MARGIN, doc.internal.pageSize.height - 5)
            .text(doc.getCurrentPageInfo().pageNumber + '/' + doc.getNumberOfPages(), doc.internal.pageSize.width - this.PDF_X_MARGIN, doc.internal.pageSize.height - 5, {align: 'right'}),
        )
    }

    protected writeTitle(doc: jsPDF, text: string, verticalPointer: number, center = false) {
        verticalPointer = this.getPDFNextLinePos(doc, verticalPointer)
        doc
            .setFontSize(15)
            .text(text, center ? (doc.internal.pageSize.getWidth() / 2) : this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer), {align: center ? 'center' : 'left'})
            .setFontSize(this.PDF_FONT_SIZE)
        if (!center) {
            doc.line(
                this.PDF_X_MARGIN,
                verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                doc.internal.pageSize.width - this.PDF_X_MARGIN,
                verticalPointer)
        }
        return verticalPointer
    }

    protected getPDFNextLinePos(doc: jsPDF, startingPosition: number, skipLines = 1): number {
        if (doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN > startingPosition + this.PDF_LINE_HEIGHT * skipLines) {
            return startingPosition + this.PDF_LINE_HEIGHT * skipLines
        } else {
            doc.addPage()
            return this.PDF_Y_MARGIN
        }
    }

    generateModificationReport(dataEntryIri: string) {
        this.httpService.get<ModificationReportDto>(dataEntryIri, {groups: ['change_report']})
            .then(report => {
                if (report.data.changeReports.length === 0) {
                    this.notifier.objectFailure('Aucune modification sur cette saisie')
                    return
                }
                const pdfName = this.i18n.t('files.pdf.modificationReport.title')
                const doc = new jsPDF()
                const pageWidth = doc.internal.pageSize.getWidth()
                let verticalPointer = this.PDF_Y_MARGIN
                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.modificationReport.subTitle').toString(), verticalPointer, true)
                doc.text(
                    this.i18n.t('files.pdf.modificationReport.experiments') + report.data.project.experiments.map(experiment => experiment.name)
                        .reduce((acc, name) => acc + ', ' + name),
                    this.PDF_X_MARGIN,
                    verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 4),
                    {align: 'left'},
                )
                    .text(this.i18n.t('files.pdf.modificationReport.project') + report.data.project.name, pageWidth / 2, verticalPointer, {align: 'center'})
                    .text(this.i18n.t('files.pdf.modificationReport.date') + moment(new Date(report.data.start)).format('YYYY-MM-DD'), pageWidth - this.PDF_X_MARGIN, verticalPointer, {align: 'right'})
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2),
                    body: report.data.changeReports.map(change => {
                        const target = change.involvedIndividual ?? change.involvedSurfacicUnitPlot
                        const targetUnitPlot = change.involvedSurfacicUnitPlot ?? change.involvedIndividual.unitPlot
                        return [
                            change.changeType === ChangeTypeEnum.DEAD ? this.i18n.t('files.pdf.modificationReport.death') : this.i18n.t('files.pdf.modificationReport.replanted'),
                            target.x,
                            target.y,
                            targetUnitPlot.number,
                            (targetUnitPlot.block ?? targetUnitPlot.subBlock.block).number,
                            targetUnitPlot.treatment.name,
                            (change.changeType === ChangeTypeEnum.DEAD ?
                                this.i18n.t('files.pdf.modificationReport.alive') + ' (' + moment(change.lastChangeDate).format('YYYY-MM-DD') + ')' :
                                this.i18n.t('files.pdf.modificationReport.dead') + ' (' + moment(change.lastChangeDate).format('YYYY-MM-DD') + ')'),
                            (change.changeType === ChangeTypeEnum.DEAD ?
                                this.i18n.t('files.pdf.modificationReport.dead') + ' (' + moment(change.changeDate).format('YYYY-MM-DD') + ')' :
                                this.i18n.t('files.pdf.modificationReport.alive') + ' (' + moment(change.changeDate).format('YYYY-MM-DD') + ')'),
                            change.aproved ? this.i18n.t('files.pdf.modificationReport.approved') : this.i18n.t('files.pdf.modificationReport.rejected'),
                        ]
                    }),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.modificationReport.modificationKind'),
                        this.i18n.t('files.pdf.modificationReport.x'),
                        this.i18n.t('files.pdf.modificationReport.y'),
                        this.i18n.t('files.pdf.modificationReport.up'),
                        this.i18n.t('files.pdf.modificationReport.block'),
                        this.i18n.t('files.pdf.modificationReport.treatment'),
                        this.i18n.t('files.pdf.modificationReport.before'),
                        this.i18n.t('files.pdf.modificationReport.after'),
                        this.i18n.t('files.pdf.modificationReport.status'),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })
                this.writePdfFooter(doc, report.data.project.platform.site.label)
                const pdf = doc.output('bloburl')
                window.open(pdf.toString())
            })
    }

    generateDataEntrySynthesisReport(dataEntryIri: string, platformImg: string) {
        this.httpService.get<DataEntrySynthesisReportDto>(dataEntryIri, {groups: ['data_entry_synthesis']})
            .then(report => {
                const pdfName = this.i18n.t('files.pdf.dataEntrySynthesis.title')
                const doc = new jsPDF()
                const pageWidth = doc.internal.pageSize.getWidth()
                let verticalPointer = this.PDF_Y_MARGIN
                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.dataEntrySynthesis.subTitle').toString(), verticalPointer, true)
                const minutesSpent = report.data.sessions.map(session => moment(session.endedAt).diff(moment(session.startedAt), 'minutes'))
                    .reduce((acc, min) => acc + min)
                doc.text(
                    this.i18n.t('files.pdf.dataEntrySynthesis.start') + moment(new Date(report.data.start)).format('YYYY-MM-DD'), this.PDF_X_MARGIN + 20,
                    verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 4),
                    {align: 'left'},
                )
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.end') + moment(new Date(report.data.end)).format('YYYY-MM-DD'), pageWidth / 2 + 10, verticalPointer, {align: 'center'})
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.time').toString() + Math.floor(minutesSpent / 60) + 'h' + (minutesSpent % 60), pageWidth - this.PDF_X_MARGIN, verticalPointer, {align: 'right'})
                // Takes the longest text to align props
                let infoAlignMargin = doc.getTextWidth(this.i18n.t('files.pdf.dataEntrySynthesis.comment').toString()) + this.PDF_X_MARGIN
                doc
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.dataEntry').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2))
                    .text(report.data.name, infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.project').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.project.name, infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.platform').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.project.platform.name, infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.experimenter').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.user.surname + ' ' + report.data.user.name + ' (' + report.data.user.username + ')', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.experiment').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2))
                    .text(report.data.project.experiments.map(experiment => experiment.name)
                            .reduce((acc, name) => acc + ', ' + name),
                        infoAlignMargin,
                        verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.comment').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.comment ?? '', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.sessions').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.sessions.length + ' session(s):', infoAlignMargin, verticalPointer)
                report.data.sessions.forEach(session => doc.text(moment(new Date(session.startedAt)).format('YYYY-MM-DD HH:mm'), infoAlignMargin, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer)))
                doc
                    .setFont(undefined, 'bold')
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.path').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer), {})
                    .setFont(undefined, 'normal')

                const imageProperties = doc.getImageProperties(platformImg)
                let imageWidth = doc.internal.pageSize.width - 2 * this.PDF_X_MARGIN
                let imageHeight = imageWidth * imageProperties.height / imageProperties.width
                if (imageHeight + verticalPointer > doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN) {
                    imageHeight = doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN - verticalPointer
                    imageWidth = imageHeight * imageProperties.width / imageProperties.height
                }
                doc.addImage(platformImg, 'png',
                    this.PDF_X_MARGIN,
                    verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    imageWidth,
                    imageHeight,
                )
                verticalPointer = this.getPDFNextLinePos(doc, verticalPointer + imageHeight)

                doc
                    .setFont(undefined, 'bold')
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.variables').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer), {})
                    .setFont(undefined, 'normal')
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.variables.map(variable => [variable.shortName, variable.name]),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.dataEntrySynthesis.shortName'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.name'),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })
                const variableMap = new Map<string, {
                    count: number,
                    expected: number,
                }>()
                const stateCodeKeyTab: number[] = []
                const stateCodeMap = new Map<number, {
                    label: string,
                    individualNumber: number,
                    blockNumber: number,
                    experimentNumber: number,
                }>()
                const annotationMap = new Map<PathLevelEnum, { count: number }>()
                const objectSet = new Set<string>()
                const effectivelySeenObjectSet = new Set<string>()
                const missingDataObjectSet = new Set<string>()
                report.data.sessions.forEach(session => {
                    session.annotations.forEach(annotation => {
                        // Count object related annotations
                        if (!annotationMap.has(annotation.targetType)) {
                            annotationMap.set(annotation.targetType, {count: 0})
                        }
                        annotationMap.get(annotation.targetType).count += 1
                    })
                    session.fieldMeasures.forEach(formField => {
                        if (!variableMap.has(formField.variable['@id'])) {
                            variableMap.set(formField.variable['@id'], {count: 0, expected: 0})
                        }
                        const elem = variableMap.get(formField.variable['@id'])
                        objectSet.add(formField.target)
                        formField.measures.forEach(measure => {
                            elem.expected++

                            // Count variable related Annotations
                            if (!annotationMap.has(formField.targetType)) {
                                annotationMap.set(formField.targetType, {count: 0})
                            }
                            annotationMap.get(formField.targetType).count += measure.annotations.length

                            if (!measure.state) {
                                elem.count++
                                effectivelySeenObjectSet.add(formField.target)
                            } else {
                                if (!stateCodeMap.has(measure.state.code)) {
                                    stateCodeKeyTab.push(measure.state.code)
                                    stateCodeMap.set(measure.state.code, {
                                        label: measure.state.title,
                                        blockNumber: 0,
                                        experimentNumber: 0,
                                        individualNumber: 0,
                                    })
                                }
                                switch (formField.targetType) {
                                    case PathLevelEnum.BLOCK:
                                        stateCodeMap.get(measure.state.code).blockNumber++
                                        break
                                    case PathLevelEnum.EXPERIMENT:
                                        stateCodeMap.get(measure.state.code).experimentNumber++
                                        break
                                    case PathLevelEnum.INDIVIDUAL:
                                    case PathLevelEnum.SURFACE_UNIT_PLOT:
                                        stateCodeMap.get(measure.state.code).individualNumber++
                                        break
                                }
                                if (!!measure.state.permanent && measure.state.title === 'Donnée Manquante') {
                                    missingDataObjectSet.add(formField.target)
                                } else {
                                    effectivelySeenObjectSet.add(formField.target)
                                }
                            }
                        })
                    })
                })
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.variables.map(variable => [
                        variable.name,
                        this.i18n.t('enums.variable.types', {type: ((variable instanceof GeneratorVariable) ? VariableTypeEnum.INTEGER : variable.type)}).toString(),
                        this.i18n.t('enums.levels', {level: variable.pathLevel}).toString(),
                        variable.unit,
                        variable.repetitions,
                        variable['@type'] === 'SimpleVariable' ? this.i18n.t('files.pdf.dataEntrySynthesis.indepVar') :
                            variable['@type'] === 'GeneratorVariable' ? this.i18n.t('files.pdf.dataEntrySynthesis.genVar') :
                                this.i18n.t('files.pdf.dataEntrySynthesis.semiAuto'),
                        variable.generatorVariable?.shortName ?? '',
                        variableMap.get(variable['@id']).count + ' valeur(s) saisies / ' + variableMap.get(variable['@id']).expected + ' valeurs à saisir',
                    ]),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.dataEntrySynthesis.variable'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.type'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.level'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.unit'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.repetition'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.kind'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.genVar'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.state'),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })
                doc.setFont(undefined, 'bold')
                infoAlignMargin = doc.getTextWidth(this.i18n.t('files.pdf.dataEntrySynthesis.missingDataNumber').toString()) + this.PDF_X_MARGIN
                doc
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.concernedObjects').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2))
                    .text(objectSet.size + '', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.declaredDead').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.changeReports.reduce((acc, change) => acc + (change.changeType === ChangeTypeEnum.DEAD && change.aproved ? 1 : 0), 0) + '', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.measuredObjects').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(effectivelySeenObjectSet.size + '', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.nonMeasuredObjects').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(objectSet.size - effectivelySeenObjectSet.size + '', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.missingDataNumber').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(missingDataObjectSet.size + '', infoAlignMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.forcedTests').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.sessions.map(session => session.annotations.filter(annotation => annotation.categories.includes('Test')).length)
                        .reduce((acc, item) => acc + item) + '', infoAlignMargin, verticalPointer)
                    .setFont(undefined, 'normal')
                doc
                    .setFont(undefined, 'bold')
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.measuresByState').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2), {})
                    .setFont(undefined, 'normal')
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: stateCodeKeyTab.map(code => [
                        code,
                        stateCodeMap.get(code).label,
                        stateCodeMap.get(code).individualNumber,
                        stateCodeMap.get(code).blockNumber,
                        stateCodeMap.get(code).experimentNumber,
                    ]),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.dataEntrySynthesis.value'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.label'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.individuals'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.blocks'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.experiments'),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })
                doc
                    .setFont(undefined, 'bold')
                    .text(this.i18n.t('files.pdf.dataEntrySynthesis.annotation').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2), {})
                    .setFont(undefined, 'normal')
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: [
                        [this.i18n.t('enums.levels', {level: PathLevelEnum.INDIVIDUAL}), report.data.project.requiredAnnotations.filter(item => item.level === PathLevelEnum.INDIVIDUAL).length, annotationMap.get(PathLevelEnum.INDIVIDUAL)?.count ?? '0'],
                        [this.i18n.t('enums.levels', {level: PathLevelEnum.SURFACE_UNIT_PLOT}), report.data.project.requiredAnnotations.filter(item => item.level === PathLevelEnum.SURFACE_UNIT_PLOT).length, annotationMap.get(PathLevelEnum.SURFACE_UNIT_PLOT)?.count ?? '0'],
                        [this.i18n.t('enums.levels', {level: PathLevelEnum.UNIT_PLOT}), report.data.project.requiredAnnotations.filter(item => item.level === PathLevelEnum.UNIT_PLOT).length, annotationMap.get(PathLevelEnum.UNIT_PLOT)?.count ?? '0'],
                        [this.i18n.t('enums.levels', {level: PathLevelEnum.SUB_BLOCK}), report.data.project.requiredAnnotations.filter(item => item.level === PathLevelEnum.SUB_BLOCK).length, annotationMap.get(PathLevelEnum.SUB_BLOCK)?.count ?? '0'],
                        [this.i18n.t('enums.levels', {level: PathLevelEnum.BLOCK}), report.data.project.requiredAnnotations.filter(item => item.level === PathLevelEnum.BLOCK).length, annotationMap.get(PathLevelEnum.BLOCK)?.count ?? '0'],
                        [this.i18n.t('enums.levels', {level: PathLevelEnum.EXPERIMENT}), report.data.project.requiredAnnotations.filter(item => item.level === PathLevelEnum.EXPERIMENT).length, annotationMap.get(PathLevelEnum.EXPERIMENT)?.count ?? '0'],
                    ],
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.dataEntrySynthesis.level'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.requiredAnnotation'),
                        this.i18n.t('files.pdf.dataEntrySynthesis.annotationCount'),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })
                this.writePdfFooter(doc, report.data.project.platform.site.label)
                const pdf = doc.output('bloburl')
                window.open(pdf.toString())
            })
    }

    generatePlatformSynthesisReport(platformIri: string, platformImg: string) {
        this.httpService.get<PlatformSynthesisReportDto>(platformIri, {groups: ['platform_synthesis']})
            .then(report => {
                const pdfName = this.i18n.t('files.pdf.platformSynthesis.title')
                const doc = new jsPDF()
                const pageWidth = doc.internal.pageSize.getWidth()
                let verticalPointer = this.PDF_Y_MARGIN
                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.platformSynthesis.platform') + report.data.name, verticalPointer, true)
                doc.line(
                    this.PDF_X_MARGIN,
                    verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    doc.internal.pageSize.width - this.PDF_X_MARGIN,
                    verticalPointer)
                const infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.platformSynthesis.comment').toString()) + this.PDF_X_MARGIN
                const infoAlignRMargin = doc.getTextWidth(moment(new Date(report.data.created)).format(' YYYY-MM-DD HH:mm')) + this.PDF_X_MARGIN
                doc
                    .text(this.i18n.t('files.pdf.platformSynthesis.platform').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2))
                    .text(report.data.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.platformSynthesis.place').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.placeName, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.platformSynthesis.creation').toString(), doc.internal.pageSize.width - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(moment(new Date(report.data.created)).format('YYYY-MM-DD HH:mm'), doc.internal.pageSize.width - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.platformSynthesis.site').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.siteName, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.platformSynthesis.creator').toString(), doc.internal.pageSize.width - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(report.data.owner.surname, doc.internal.pageSize.width - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.platformSynthesis.comment').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.comment ?? '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.platformSynthesis.experiments').toString(), verticalPointer)

                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.experiments.map(item => [
                        item.name,
                        item.individualUP ? this.i18n.t('files.pdf.platformSynthesis.individualPlot') :
                            this.i18n.t('files.pdf.platformSynthesis.surfacicPlot'),
                        item.owner.surname,
                        moment(new Date(item.created)).format('YYYY-MM-DD'),
                        item.protocol.algorithm?.name ?? 'Sans Tirage',
                        item.protocol.name,
                        item.state >= ExperimentStateEnum.LOCKED ? this.i18n.t('enums.experimentState', {state: ExperimentStateEnum.LOCKED}) :
                            this.i18n.t('enums.experimentState', {state: ExperimentStateEnum.VALIDATED}),
                        '' + item.projects
                            .reduce((acc, project) =>
                                    acc + project.projectDatas.reduce((acc2, data) =>
                                            acc2 + data.variables.length,
                                        0),
                                0),
                    ]),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.platformSynthesis.experiment'),
                        this.i18n.t('files.pdf.platformSynthesis.plotType'),
                        this.i18n.t('files.pdf.platformSynthesis.creator'),
                        this.i18n.t('files.pdf.platformSynthesis.date'),
                        this.i18n.t('files.pdf.platformSynthesis.algorithm'),
                        this.i18n.t('files.pdf.platformSynthesis.protocol'),
                        this.i18n.t('files.pdf.platformSynthesis.state'),
                        this.i18n.t('files.pdf.platformSynthesis.variableCount'),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })

                doc.addPage()
                verticalPointer = this.PDF_Y_MARGIN
                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.platformSynthesis.graphical').toString(), verticalPointer)
                const imageProperties = doc.getImageProperties(platformImg)
                let imageWidth = doc.internal.pageSize.width - 2 * this.PDF_X_MARGIN
                let imageHeight = imageWidth * imageProperties.height / imageProperties.width
                if (imageHeight + verticalPointer > doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN) {
                    imageHeight = doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN - verticalPointer
                    imageWidth = imageHeight * imageProperties.width / imageProperties.height
                }
                doc.addImage(platformImg, 'png',
                    this.PDF_X_MARGIN,
                    verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    imageWidth,
                    imageHeight,
                )

                this.writePdfFooter(doc, report.data.site.label)
                const pdf = doc.output('bloburl')
                window.open(pdf.toString())
            })
    }

    generateProtocolSynthesisReport(protocolIri: string) {
        this.httpService.get<ProtocolSynthesisReportDto>(protocolIri, {groups: ['protocol_synthesis']})
            .then(report => {
                if (!report.data.site) {
                    return this.store.getters['navigation/getActiveRoleSite'].site.then((site: Site) => {
                        report.data.site = site
                        return report
                    })
                }
                return report
            })
            .then((report: AxiosResponse<ProtocolSynthesisReportDto>) => {
                this.httpService.getAll<ExperimentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT), {'protocol.name': report.data.name})
                    .then(experimentResponse => {
                        const pdfName = this.i18n.t('files.pdf.protocolSynthesis.title')
                        const doc = new jsPDF()
                        const pageWidth = doc.internal.pageSize.getWidth()
                        let verticalPointer = this.PDF_Y_MARGIN

                        verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.protocolSynthesis.protocol').toString(), verticalPointer)

                        let infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.protocolSynthesis.protocolExperiments').toString()) + this.PDF_X_MARGIN
                        const infoAlignRMargin = doc.getTextWidth(report.data.owner.name + ' ' + report.data.owner.surname) + this.PDF_X_MARGIN
                        doc
                            .text(this.i18n.t('files.pdf.protocolSynthesis.protocol').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(report.data.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                            .text(this.i18n.t('files.pdf.protocolSynthesis.creation').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(moment(new Date(report.data.created)).format('YYYY-MM-DD HH:mm'), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                            .text(this.i18n.t('files.pdf.protocolSynthesis.owner').toString(), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                            .text(report.data.owner.name + ' ' + report.data.owner.surname, pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                            .text(this.i18n.t('files.pdf.protocolSynthesis.aim').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(report.data.aim, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                            .text(this.i18n.t('files.pdf.protocolSynthesis.comment').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(report.data.comment ?? '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                            .text(this.i18n.t('files.pdf.protocolSynthesis.algorithm').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(report.data.algorithm?.name ?? 'Sans Tirage', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                            .text(this.i18n.t('files.pdf.protocolSynthesis.protocolExperiments').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(experimentResponse.data['hydra:member'].map(exp => exp.name).join(', '), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                        verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.protocolSynthesis.details').toString(), verticalPointer)

                        infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.protocolSynthesis.treatmentNumber').toString()) + this.PDF_X_MARGIN
                        doc
                            .text(this.i18n.t('files.pdf.protocolSynthesis.factorNumber').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(report.data.factors.length + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                        report.data.factors.sort((a, b) => a.order - b.order)
                            .forEach((factor, index) => doc
                                .text(this.i18n.tc('files.pdf.protocolSynthesis.factor', index + 1).toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                                .text(this.i18n.t('files.pdf.protocolSynthesis.factorModalities', {
                                    factorName: factor.name,
                                    count: factor.modalities.length
                                }).toString(), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer),
                            )
                        doc
                            .text(this.i18n.t('files.pdf.protocolSynthesis.treatmentNumber').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .text(report.data.treatments.length + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                        verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2)

                        report.data.factors.sort((a, b) => a.order - b.order)
                            .forEach((factor, index) => {
                                infoAlignLMargin = doc.getTextWidth(this.i18n.tc('files.pdf.protocolSynthesis.factor', index + 1).toString()) + this.PDF_X_MARGIN
                                doc
                                    .setFont(undefined, 'bold')
                                    .text(this.i18n.tc('files.pdf.protocolSynthesis.factor', index + 1).toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                                    .setFont(undefined, 'normal')
                                    .text(factor.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                                factor.modalities.forEach(modality => doc
                                    .text(this.i18n.t('files.pdf.protocolSynthesis.modality', {
                                        modalityValue: modality.value,
                                        modalityShortName: modality.shortName
                                    }).toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer)),
                                )
                            })

                        verticalPointer = this.getPDFNextLinePos(doc, verticalPointer, 2)

                        doc
                            .setFont(undefined, 'bold')
                            .text(this.i18n.t('files.pdf.protocolSynthesis.treatments').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                            .setFont(undefined, 'normal')

                        autoTable(doc, {
                            startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                            body: report.data.treatments.map(treatment => [
                                ...treatment.modalities.map(modality => modality.value),
                                treatment.name,
                                treatment.shortName,
                                treatment.repetitions,
                            ]),
                            margin: {
                                vertical: this.PDF_Y_MARGIN,
                                horizontal: this.PDF_X_MARGIN,
                            },
                            head: [[
                                ...report.data.factors.map((item, index) => this.i18n.tc('files.pdf.protocolSynthesis.factorTab', index + 1).toString()),
                                this.i18n.t('files.pdf.protocolSynthesis.treatmentLong').toString(),
                                this.i18n.t('files.pdf.protocolSynthesis.treatmentShort').toString(),
                                this.i18n.t('files.pdf.protocolSynthesis.repetitions').toString(),
                            ]],
                            didDrawPage: item => {
                                verticalPointer = item.cursor.y
                            },
                        })

                        this.writePdfFooter(doc, report.data.site.label)
                        const pdf = doc.output('bloburl')
                        window.open(pdf.toString())
                    })
            })
    }

    generateProjectSynthesisReport(projectIri: string, platformImage?: string) {
        this.httpService.get<ProjectSynthesisReportDto>(projectIri, {groups: ['project_synthesis']})
            .then(report => {
                const pdfName = this.i18n.t('files.pdf.projectSynthesis.title').toString()
                const doc = new jsPDF()
                const pageWidth = doc.internal.pageSize.getWidth()
                let verticalPointer = this.PDF_Y_MARGIN

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.projectTitle').toString(), verticalPointer, true)

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.details').toString(), verticalPointer)

                const infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.projectSynthesis.dataEntriesDone').toString()) + this.PDF_X_MARGIN
                const infoAlignRMargin = max([
                    doc.getTextWidth(moment(new Date(report.data.created)).format(' YYYY-MM-DD HH:mm')),
                    doc.getTextWidth(report.data.owner.surname + ' ' + report.data.owner.name + ' (' + report.data.owner.username + ')'),
                ]) + this.PDF_X_MARGIN

                doc
                    .text(this.i18n.t('files.pdf.projectSynthesis.project').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.projectSynthesis.type').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.pathBase?.userPaths.length > 1 ? 'Multi opérateurs' : 'Mono opérateur', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.projectSynthesis.creation').toString(), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(moment(new Date(report.data.created)).format('YYYY-MM-DD HH:mm'), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.projectSynthesis.platform').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.platform.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.projectSynthesis.owner').toString(), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(report.data.owner.name + ' ' + report.data.owner.surname, pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.projectSynthesis.experiments').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.experiments.map(exp => exp.name).join(', '), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.projectSynthesis.dataEntriesDone').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.projectDatas.length + ' (' + report.data.projectDatas.map(data => moment(new Date(data.start)).format('YYYY-MM-DD')).join(', ') + ')', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.paths').toString(), verticalPointer)
                const imageProperties = doc.getImageProperties(platformImage)
                let imageWidth = doc.internal.pageSize.width - 2 * this.PDF_X_MARGIN
                let imageHeight = imageWidth * imageProperties.height / imageProperties.width
                if (imageHeight + verticalPointer > doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN) {
                    imageHeight = doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN - verticalPointer
                    imageWidth = imageHeight * imageProperties.width / imageProperties.height
                }
                doc.addImage(platformImage, 'png',
                    this.PDF_X_MARGIN,
                    verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    imageWidth,
                    imageHeight,
                )
                verticalPointer = this.getPDFNextLinePos(doc, verticalPointer + imageHeight)

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.variablesAndAttributes').toString(), verticalPointer)

                const rec = (variables: VariableRDto[]): VariableRDto[] => {
                    return [...variables.sort((a, b) => (a.order ?? a.id) - (b.order ?? b.id))
                        .reduce((acc, item) => [...acc, item, ...(!!item.generatedVariables ? rec(item.generatedVariables) : [])], [])]
                }
                const flatVars = rec(report.data.variables)

                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: flatVars.map(variable => [variable.shortName, variable.name]),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.projectSynthesis.shortName').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.longName').toString(),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: flatVars.map(variable => [
                        variable.shortName,
                        this.i18n.t('enums.variable.types', {type: variable.type ?? VariableTypeEnum.INTEGER}),
                        this.i18n.t('enums.levels', {level: variable.pathLevel}),
                        variable.unit,
                        variable.repetitions,
                        variable['@type'] === 'SimpleVariable' ? 'Indépendante' : variable['@type'] === 'GeneratorVariable' ? 'Génératrice' : 'Semi-Automatique',
                        flatVars.find(item => !!item.generatedVariables?.some(gen => gen['@id'] === variable['@id']))?.shortName ?? '',
                        variable.scale?.name ?? ' - ',
                        variable.connectedVariables.length > 0 ? 'Oui' : 'Non',
                    ]),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.projectSynthesis.variable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.variableType').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.level').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.unit').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.repetition').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.nature').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.generator').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.scale').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.precharged').toString(),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.rangeTest').toString(), verticalPointer)

                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.variables
                        .reduce((acc, variable) => [...acc, ...variable.tests.filter(test => test.type === TestTypeEnum.INTERVAL)
                            .map(item => [
                                variable.shortName,
                                item.authIntervalMin,
                                item.probIntervalMin,
                                item.probIntervalMax,
                                item.authIntervalMax,
                            ])], []),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.projectSynthesis.variable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.mandatoryMin').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.min').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.max').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.mandatoryMax').toString(),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.growTest').toString(), verticalPointer)
                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.variables
                        .reduce((acc, variable) => [...acc, ...variable.tests.filter(test => test.type === TestTypeEnum.GROWTH)
                            .map(item => [
                                variable.shortName,
                                item.comparedVariable.shortName,
                                'Ecart = [' + item.minGrowth + ',' + item.maxGrowth + ']',
                            ])], []),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.projectSynthesis.variable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.sourceVariable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.rule').toString(),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.combinationTest').toString(), verticalPointer)

                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.variables
                        .reduce((acc, variable) => [...acc, ...variable.tests.filter(test => test.type === TestTypeEnum.COMBINATION)
                            .map(item => [
                                variable.shortName,
                                '(' + item.combinedVariable1.shortName + ' ' + item.combinationOperation + ' ' + item.combinedVariable2.shortName + ') contenu dans : [' +
                                item.minLimit + ',' + item.maxLimit + ']',
                            ])], []),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.projectSynthesis.variable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.rule').toString(),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.projectSynthesis.preconditionnalCalcul').toString(), verticalPointer)

                autoTable(doc, {
                    startY: verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                    body: report.data.variables
                        .reduce((acc, variable) => [...acc, ...variable.tests.filter(test => test.type === TestTypeEnum.PRECONDITIONED)
                            .map(item => [
                                variable.name,
                                item.comparedVariable1.shortName,
                                'Si (' + item.comparedVariable1.shortName + ' ' + item.comparisonOperation + ' ' + (item.compareWithVariable ? item.comparedVariable2.shortName : item.comparedValue) + ') affecter ' + item.assignedValue,
                            ])], []),
                    margin: {
                        vertical: this.PDF_Y_MARGIN,
                        horizontal: this.PDF_X_MARGIN,
                    },
                    head: [[
                        this.i18n.t('files.pdf.projectSynthesis.variable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.sourceVariable').toString(),
                        this.i18n.t('files.pdf.projectSynthesis.rule').toString(),
                    ]],
                    didDrawPage: item => {
                        verticalPointer = item.cursor.y
                    },
                })

                this.writePdfFooter(doc, report.data.platform.site.label)
                const pdf = doc.output('bloburl')
                window.open(pdf.toString())
            })
    }

    generateVariableSynthesisReport(variableIri: string) {
        this.httpService.get<VariableSynthesisReportDto>(variableIri, {groups: ['variable_synthesis']})
            .then(report => {
                const pdfName = this.i18n.t('files.pdf.variableSynthesis.title').toString()
                const doc = new jsPDF()
                const pageWidth = doc.internal.pageSize.getWidth()
                let verticalPointer = this.PDF_Y_MARGIN

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.variableSynthesis.variableTitle').toString() + report.data.name, verticalPointer, true)

                let infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.variableSynthesis.shortName').toString()) + this.PDF_X_MARGIN

                doc
                    .text(this.i18n.t('files.pdf.variableSynthesis.longName').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.shortName').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.shortName, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.variableSynthesis.projectFeature').toString(), verticalPointer)

                let infoAlignRMargin = doc.getTextWidth(moment(new Date(report.data.projectData.end)).format(' YYYY-MM-DD HH:mm')) + this.PDF_X_MARGIN

                infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.variableSynthesis.projectOwner').toString()) + this.PDF_X_MARGIN

                const effectivelySeenObjectSet = new Set()
                const objectSet = new Set()

                let min: number = null
                let max: number = null
                const isVariableNumeric = !report.data.type || [VariableTypeEnum.REAL, VariableTypeEnum.INTEGER].includes(report.data.type)

                const aggregateFunc = (acc: { count: number, mean: number, m2: number }, measure: number) => {
                    acc.count += 1
                    const delta = measure - acc.mean
                    acc.mean += delta / acc.count
                    const delta2 = measure - acc.mean
                    acc.m2 += delta * delta2
                    return acc
                }
                let aggregate = {count: 0, mean: 0, m2: 0}

                const diffValueSet = new Set<number>()
                const values: number[] = []
                report.data.projectData.sessions
                    .forEach(sessions => sessions.fieldMeasures
                        .forEach(fieldMeasure => {
                            objectSet.add(fieldMeasure.target)
                            fieldMeasure.measures
                                .forEach(measure => {
                                    if (!measure.state) {
                                        effectivelySeenObjectSet.add(fieldMeasure.target)
                                        if (isVariableNumeric) {
                                            const numerivValue = parseFloat(measure.value)
                                            values.push(numerivValue)
                                            diffValueSet.add(numerivValue)
                                            min = Math.min(min ?? Number.MAX_VALUE, numerivValue)
                                            max = Math.max(max ?? Number.MIN_VALUE, numerivValue)
                                            aggregate = aggregateFunc(aggregate, numerivValue)
                                        }
                                    } else {
                                        if (measure.state.title !== 'Donnée Manquante') {
                                            effectivelySeenObjectSet.add(fieldMeasure.target)
                                        }
                                    }
                                })
                        }))
                const mean = Math.round(aggregate.mean * 100) / 100
                const deviance = Math.round(Math.sqrt(aggregate.m2 / (aggregate.count - 1)) * 100) / 100

                doc
                    .text(this.i18n.t('files.pdf.variableSynthesis.projectOwner').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.platform').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.projectData.project.platform.name, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.start').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(moment(new Date(report.data.projectData.start)).format('YYYY-MM-DD HH:mm'), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.end').toString(), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(moment(new Date(report.data.projectData.end)).format(' YYYY-MM-DD HH:mm'), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.type').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.projectData.project.pathBase?.userPaths.length > 1 ? 'Multi opérateurs' : 'Mono opérateur', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.type').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.projectData.user.name + ' ' + report.data.projectData.user.surname, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.objects').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(effectivelySeenObjectSet.size + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.objectsSkipped').toString(), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(objectSet.size - effectivelySeenObjectSet.size + '', pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.projectOwner').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.projectData.user.name + ' ' + report.data.projectData.user.surname, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.path').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text((!!report.data.projectData.project.pathBase ? 'Oui ' : 'Non '), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                report.data.projectData.project.pathBase?.pathLevelAlgorithms.forEach(item => {
                    if (item.move !== PossibleMoveEnum.LIBRE) {
                        doc.text(this.i18n.t('enums.levels', {level: item.pathLevel}) +
                            ' ' +
                            item.move +
                            ' ' +
                            this.i18n.t('enums.path.startPoints', {startPoint: item.startPoint}), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    }
                })

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.variableSynthesis.variableFeature').toString(), verticalPointer)

                infoAlignRMargin = doc.getTextWidth(report.data?.unit ?? '  ') + this.PDF_X_MARGIN
                infoAlignLMargin = doc.getTextWidth('Commentaire de saisie : ') + this.PDF_X_MARGIN

                doc
                    .text(this.i18n.t('files.pdf.variableSynthesis.variableType').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(!!report.data.type ? this.i18n.t('enums.variable.types', {type: report.data.type}).toString() : 'Génératrice', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.unit').toString(), pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer, {align: 'right'})
                    .text(report.data?.unit ?? '  ', pageWidth - this.PDF_X_MARGIN - infoAlignRMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.repetition').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.repetitions + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.comment').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.comment, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.level').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(this.i18n.t('enums.levels', {level: report.data.pathLevel}).toString(), this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.test').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.tests.map(test => this.i18n.t('enums.testType', {type: test.type})).join(', ') + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                    .text(this.i18n.t('files.pdf.variableSynthesis.stateCodes').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))

                report.data.projectData.project.stateCodes.forEach(item =>
                    doc.text(item.code + ' ' + item.title, this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer)),
                )
                doc
                    .text(this.i18n.t('files.pdf.variableSynthesis.dataEntryComment').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(report.data.projectData.comment ?? '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                verticalPointer = this.writeTitle(doc, this.i18n.t('files.pdf.variableSynthesis.data').toString(), verticalPointer)

                infoAlignLMargin = doc.getTextWidth(this.i18n.t('files.pdf.variableSynthesis.assessedObjects').toString()) + this.PDF_X_MARGIN

                doc
                    .text(this.i18n.t('files.pdf.variableSynthesis.assessedObjects').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                    .text(effectivelySeenObjectSet.size + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                if (isVariableNumeric) {
                    doc
                        .text(this.i18n.t('files.pdf.variableSynthesis.min').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                        .text(min + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                        .text(this.i18n.t('files.pdf.variableSynthesis.max').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                        .text(max + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                        .text(this.i18n.t('files.pdf.variableSynthesis.mean').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                        .text(mean + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)
                        .text(this.i18n.t('files.pdf.variableSynthesis.deviance').toString(), this.PDF_X_MARGIN, verticalPointer = this.getPDFNextLinePos(doc, verticalPointer))
                        .text(deviance + '', this.PDF_X_MARGIN + infoAlignLMargin, verticalPointer)

                    doc.addPage()
                    verticalPointer = this.PDF_Y_MARGIN

                    const maxBarCount = 8
                    const setSize = Math.ceil((max - min) / Math.min(diffValueSet.size, maxBarCount))
                    const minFloor = Math.floor(min)
                    let labels: any[]
                    let data: number[]
                    if (diffValueSet.size <= maxBarCount) {
                        labels = Array.from(diffValueSet)
                        labels.sort((a, b) => a - b)
                        data = values.reduce((acc, value) => {
                            acc[labels.indexOf(value)]++
                            return acc
                        }, (new Array(diffValueSet.size)).fill(0))
                    } else {
                        labels = (new Array(maxBarCount)).fill(0).map((item, index) =>
                            '[' + (index * setSize + minFloor) + '; ' + ((index + 1) * setSize + minFloor) + (index === maxBarCount - 1 ? ']' : '['),
                        )
                        data = values.reduce((acc, value) => {
                            acc[Math.floor((value - min) / setSize) > acc.length ? acc.length - 1 : Math.floor((value - min) / setSize)]++
                            return acc
                        }, (new Array(maxBarCount)).fill(0))
                    }

                    const canvas = document.createElement('canvas')
                    canvas.width = 1000
                    canvas.height = 1000 * doc.internal.pageSize.height / doc.internal.pageSize.width
                    const ctx = canvas.getContext('2d')
                    const myChart = (new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels,
                            datasets: [{
                                label: 'Répartition des résultats',
                                data,
                                borderWidth: 1,
                            }],
                        },
                        options: {
                            responsive: false,
                            animation: {
                                duration: 0,
                            },
                            hover: {
                                animationDuration: 0,
                            },
                            responsiveAnimationDuration: 0,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true,
                                    },
                                }],
                            },
                        },
                    })).toBase64Image()
                    const imageProperties = doc.getImageProperties(myChart)
                    let imageWidth = doc.internal.pageSize.width - 2 * this.PDF_X_MARGIN
                    let imageHeight = imageWidth * imageProperties.height / imageProperties.width
                    if (imageHeight + verticalPointer > doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN) {
                        imageHeight = doc.internal.pageSize.height - 2 * this.PDF_Y_MARGIN - verticalPointer
                        imageWidth = imageHeight * imageProperties.width / imageProperties.height
                    }
                    doc.addImage(myChart, 'png',
                        this.PDF_X_MARGIN,
                        verticalPointer = this.getPDFNextLinePos(doc, verticalPointer),
                        imageWidth,
                        imageHeight,
                    )
                }

                this.writePdfFooter(doc, report.data.projectData.project.platform.site.label)
                const pdf = doc.output('bloburl')
                window.open(pdf.toString())
            })
    }
}
