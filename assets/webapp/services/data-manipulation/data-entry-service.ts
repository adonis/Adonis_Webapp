import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import FileTypeEnum from '@adonis/webapp/constants/file-type-enum'
import DataEntryCsvBindingFormInterface from '@adonis/webapp/form-interfaces/data-entry-csv-binding-form-interface'
import DataEntryFusionFormInterface from '@adonis/webapp/form-interfaces/data-entry-fusion-form-interface'
import Project from '@adonis/webapp/models/project'
import StatusDataEntry from '@adonis/webapp/models/status-data-entry'
import AbstractService from '@adonis/webapp/services/data-manipulation/abstract-service'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProjectDataExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-data-explorer-get-dto'
import { DataEntryFusionDto } from '@adonis/webapp/services/http/entities/simple-dto/data-entry-fusion-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'
import { ParsingJobDto } from '@adonis/webapp/services/http/entities/simple-dto/parsing-job-dto'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import { ExperimentExportDto } from '@adonis/webapp/services/http/open-silex-entities/experiment-export/experiment-export-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import DataExplorerProvider, {
    DATA_ENTRY_LIBRARY_EXPLORER_NAME
} from '@adonis/webapp/services/providers/explorer-providers/data-explorer-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import 'jspdf-autotable'
import VueI18n from 'vue-i18n'
import { Store } from 'vuex'

export default class DataEntryService extends AbstractService {

    constructor(
        protected store: Store<any>,
        protected notifier: NotificationService,
        protected httpService: WebappHttpService,
        private transformerService: TransformerService,
        protected explorerProvider: DataExplorerProvider,
        protected i18n: VueI18n,
    ) {
        super()
    }

    createDataEntry(statusDataEntry: StatusDataEntry, changes: { sessions?: string[], approvedChanges?: string[] }) {

        return this.httpService.patch<ProjectDataDto>(`${statusDataEntry.iri}/return-data`, changes)
            .then(response => {
                return response.data === null ? null : this.transformerService.statusDataEntryTransformer.dtoToObject(response.data)
            })
            .then(dataEntry => {
                if (dataEntry !== null) {
                    statusDataEntry.webappProject.then(project => {

                        project.platform.then(platform => {

                            const source = this.store.getters['navigation/getExplorerItem'](platform.iri)
                            let promise
                            if (!source) {
                                promise = this.httpService.get<PlatformExplorerGetDto>(platform.iri, {groups: ['admin_explorer_view']})
                                    .then(platformResponse =>
                                        this.store.dispatch('navigation/add2explorer', {
                                            item: this.explorerProvider.constructPlatformExplorerItem(platformResponse.data),
                                            attachment: DATA_ENTRY_LIBRARY_EXPLORER_NAME,
                                        }),
                                    )
                            } else {
                                promise = Promise.resolve()
                            }

                            promise.then(() => {

                                this.httpService.get<ProjectDataExplorerGetDto>(dataEntry.iri, {groups: ['data_explorer_view']})
                                    .then(dataEntryResponse => {

                                        const projectDataExplorer = this.explorerProvider.constructDataEntryExplorerItem(dataEntryResponse.data, platform.iri)
                                        this.store.dispatch('navigation/add2explorer', {
                                            item: projectDataExplorer,
                                            attachment: platform.iri
                                        })
                                            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY, ObjectActionEnum.CREATED))
                                    })
                            })
                        })
                    })
                } else {
                    this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY, ObjectActionEnum.CREATED)
                }
            })
    }

    updateMeasure(measureIri: string, newValue: any): Promise<AbstractDtoObject & { annotations: string[] }> {
        const measureDto = {
            value: newValue,
            state: null as string,
        }
        return this.httpService.patch<AbstractDtoObject & { annotations: string[] }>(measureIri, measureDto)
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.MEASURE, ObjectActionEnum.MODIFIED)
                return value.data
            })
    }

    deleteDataEntry(dataEntryUri: string) {
        return this.confirmDelete(() => this.httpService.delete(dataEntryUri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: dataEntryUri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY, ObjectActionEnum.DELETED)))
    }

    deleteSession(sessionUri: string) {
        return this.confirmDelete(() => this.httpService.delete(sessionUri)
            .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: sessionUri}))
            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.SESSION, ObjectActionEnum.DELETED)))
    }

    fusionDataEntries(fusionInterface: DataEntryFusionFormInterface): Promise<FieldMeasureDto[][]> {
        return this.httpService.post<DataEntryFusionDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DATA_ENTRY_FUSION),
            this.transformerService.dataEntryFusionTransformer.formInterfaceToCreateDto(fusionInterface))
            .then(response => {
                if (!!response.data.result) {
                    return this.httpService.get<ProjectDataExplorerGetDto>(response.data.result, {groups: ['data_explorer_view']})
                        .then(dataEntryResponse => {
                            const projectDataExplorer = this.explorerProvider.constructDataEntryExplorerItem(dataEntryResponse.data, response.data.platform)
                            return this.store.dispatch('navigation/add2explorer', {
                                item: projectDataExplorer,
                                attachment: response.data.platform,
                            })
                                .then(() => this.notifier.objectSuccess(ObjectTypeEnum.FUSION_DATA_ENTRY, ObjectActionEnum.CREATED))
                                .then(() => [])
                        })
                } else {
                    return response.data.conflicts
                }
            })
    }

    updateValueList(valueListIri: string) {
        return this.httpService.patch<any>(valueListIri + '/update', {})
            .then(value => {
                this.notifier.objectSuccess(ObjectTypeEnum.VALUE_LIST, ObjectActionEnum.MODIFIED)
                return value
            })
    }

    importDataEntryCsv(csvBindings: DataEntryCsvBindingFormInterface, project: Project): Promise<any> {
        csvBindings.variables = csvBindings.variables.filter(item => item.value !== null)
        return this.importFile(FileTypeEnum.TYPE_DATA_ENTRY_CSV, IconEnum.DATA_ENTRY, DATA_ENTRY_LIBRARY_EXPLORER_NAME, (job: ParsingJobDto) => {
            const source = this.store.getters['navigation/getExplorerItem'](project.platformIri)
            const promise = !source ? this.httpService.get<PlatformExplorerGetDto>(project.platformIri, {groups: ['admin_explorer_view']})
                .then(platformResponse =>
                    this.store.dispatch('navigation/add2explorer', {
                        item: this.explorerProvider.constructPlatformExplorerItem(platformResponse.data),
                        attachment: DATA_ENTRY_LIBRARY_EXPLORER_NAME,
                    }),
                ) : Promise.resolve()
            promise.then(() => {
                this.httpService.get<ProjectDataExplorerGetDto>(job.objectIri, {groups: ['data_explorer_view']})
                    .then(dataEntryResponse => {

                        const projectDataExplorer = this.explorerProvider.constructDataEntryExplorerItem(dataEntryResponse.data, project.platformIri)
                        this.store.dispatch('navigation/add2explorer', {item: projectDataExplorer, attachment: project.platformIri})
                            .then(() => this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY, ObjectActionEnum.CREATED))
                    })
            })
        }, csvBindings)
    }

    updateOpenSilexUris(dataEntryIri: string, experimentDto: ExperimentExportDto): Promise<any> {
        return this.httpService.patch<ExperimentDto>(dataEntryIri + '/opensilexUris', experimentDto)
            .then(() => {
                this.notifier.objectSuccess(ObjectTypeEnum.EXPERIMENT, ObjectActionEnum.CREATED)
            })
    }

}
