import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import FileTypeEnum from '@adonis/webapp/constants/file-type-enum'
import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'
import RequestFile from '@adonis/webapp/models/request-file'
import AbstractService from '@adonis/webapp/services/data-manipulation/abstract-service'
import {ParsingJobDto} from '@adonis/webapp/services/http/entities/simple-dto/parsing-job-dto'
import {RequestFileDto} from '@adonis/webapp/services/http/entities/simple-dto/request-file-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import TransferExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/transfer-explorer-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import VueI18n from 'vue-i18n'
import {Store} from 'vuex'
import axios, {AxiosResponse} from "axios"

export default class TransferService extends AbstractService {

  constructor(
    protected store: Store<any>,
    protected notifier: NotificationService,
    protected httpService: WebappHttpService,
    private transformerService: TransformerService,
    protected explorerProvider: TransferExplorerProvider,
    protected i18n: VueI18n,
  ) {
    super()
  }

  toggleProjectSyncability(syncable: boolean, relUserProjectIri: string): Promise<any> {

    return this.httpService.patch(relUserProjectIri, {syncable})
  }

  downloadResponseFile(responseFileUrl: string): Promise<any> {

    return this.httpService.getFile(responseFileUrl, 'application/zip')
  }

  downloadProjectFile(relUserProjectId: number): Promise<any> {

    return this.httpService.getFile('/api/download/project/' + relUserProjectId, 'application/zip')
  }

  importProjectDataZipFile(relUserProjectIri: string) {
    const fileSelector = document.createElement('input')
    fileSelector.setAttribute('type', 'file')
    fileSelector.setAttribute('accept', '.zip,application/zip')
    fileSelector.click()
    fileSelector.onchange = () => {
      const formData = new FormData()
      formData.append('file', fileSelector.files[0])
      formData.append('site', this.store.getters['navigation/getActiveRoleSite'].siteIri)
      formData.append('statusDataEntry', relUserProjectIri)
      formData.append('objectType', FileTypeEnum.TYPE_PROJECT_DATA_ZIP)
      return this.httpService.post<ParsingJobDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PARSING_JOB), formData)
        .then(response => {
          const tmp = setInterval(() => {
            this.httpService.get<ParsingJobDto>(response.data['@id'])
              .then(job => {
                switch (job.data.status) {
                  case JobStatusEnum.FILE_STATUS_ERROR:
                    this.notifier.defaultObjectFailure(ObjectActionEnum.FETCHED)
                    break
                  case JobStatusEnum.FILE_STATUS_SUCCESS:
                    // TODO
                    break
                  default:
                    return
                }
                clearInterval(tmp)
              })
          }, 4000)
        })

    }

  }

  deleteStatusDataEntryProject(responseFileIri: string): Promise<any> {

    return this.httpService.delete(responseFileIri)
  }

  uploadRequestFile(file: any): Promise<RequestFile> {
    const formData = new FormData()
    formData.append('file', file)
    return this.httpService.post<RequestFileDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.REQUEST_FILE), formData)
      .then(response => this.transformerService.requestFileTransformer.dtoToObject(response.data))
  }

  toggleRequestFileDisableState(requestFileIri: string, newState: boolean): Promise<RequestFile> {
    return this.httpService.patch<RequestFileDto>(requestFileIri, {
      disabled: newState,
    }).then(response => this.transformerService.requestFileTransformer.dtoToObject(response.data))
  }

  deleteRequestFile(requestFileIri: string): Promise<any> {
    return this.httpService.delete(requestFileIri)
  }

  deleteResponseFile(responseFileIri: string): Promise<any> {
    return this.httpService.delete(responseFileIri)
  }

  /**
   * Check if user is allowed to transfer return data.
   *
   * @param statusDataEntryIri
   */
  isAllowed(statusDataEntryIri: string): Promise<boolean> {
    const headers = {'Authorization': this.store.getters['connection/authenticationHeader']}

    return axios
      .get<Boolean, AxiosResponse<Boolean>>(statusDataEntryIri + '/return-data-permission', {headers, responseType: "json"})
      .then(value => value.data.valueOf())
  }
}
