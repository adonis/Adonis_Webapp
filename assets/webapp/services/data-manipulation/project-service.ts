import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import FileTypeEnum from '@adonis/webapp/constants/file-type-enum'
import CloneProjectFormInterface from '@adonis/webapp/form-interfaces/clone-project-form-interface'
import DeviceFormInterface from '@adonis/webapp/form-interfaces/device-form-interface'
import GeneratorInfoFormInterface from '@adonis/webapp/form-interfaces/generator-info-form-interface'
import ManagedVariableExtraInfoFormInterface from '@adonis/webapp/form-interfaces/managed-variable-extra-info-form-interface'
import PathBaseCsvBindingFormInterface from '@adonis/webapp/form-interfaces/path-base-csv-binding-form-interface'
import PathBaseFormInterface from '@adonis/webapp/form-interfaces/path-base-form-interface'
import PathBaseMovementFormInterface from '@adonis/webapp/form-interfaces/path-base-movement-form-interface'
import ProjectFormInterface from '@adonis/webapp/form-interfaces/project-form-interface'
import RequiredAnnotationFormInterface from '@adonis/webapp/form-interfaces/required-annotation-form-interface'
import StateCodeFormInterface from '@adonis/webapp/form-interfaces/state-code-form-interface'
import TestFormInterface from '@adonis/webapp/form-interfaces/test-form-interface'
import UserPathFormInterface from '@adonis/webapp/form-interfaces/user-path-form-interface'
import ValueListFormInterface from '@adonis/webapp/form-interfaces/value-list-form-interface'
import VariableConnectionFormInterface from '@adonis/webapp/form-interfaces/variable-connection-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import VariableScaleFormInterface from '@adonis/webapp/form-interfaces/variable-scale-form-interface'
import Device from '@adonis/webapp/models/device'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import PathBase from '@adonis/webapp/models/path-base'
import PathUserWorkflow from '@adonis/webapp/models/path-user-workflow'
import Project from '@adonis/webapp/models/project'
import RequiredAnnotation from '@adonis/webapp/models/required-annotation'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import StateCode from '@adonis/webapp/models/state-code'
import Test from '@adonis/webapp/models/test'
import ValueList from '@adonis/webapp/models/value-list'
import VariableConnection from '@adonis/webapp/models/variable-connection'
import VariableScale from '@adonis/webapp/models/variable-scale'
import AbstractService from '@adonis/webapp/services/data-manipulation/abstract-service'
import {ConnectedVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/connected-variable-explorer-get-dto'
import {DeviceExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/device-explorer-get-dto'
import {GeneratorVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import {PathBaseExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/path-base-explorer-get-dto'
import {PlatformExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import {ProjectExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import {
  RequiredAnnotationExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/required-annotation-explorer-get-dto'
import {SimpleVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import {SiteProjectExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/site-project-explorer-get-dto'
import {StateCodeExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/state-code-explorer-get-dto'
import {TestExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/test-explorer-get-dto'
import {ValueListExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'
import {CloneProjectDto} from '@adonis/webapp/services/http/entities/simple-dto/clone-project-dto'
import {CloneVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/clone-variable-dto'
import {DeviceDto} from '@adonis/webapp/services/http/entities/simple-dto/device-dto'
import {GeneratorVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import {ParsingJobDto} from '@adonis/webapp/services/http/entities/simple-dto/parsing-job-dto'
import {PathBaseDto} from '@adonis/webapp/services/http/entities/simple-dto/path-base-dto'
import {ProjectDto} from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import {RequiredAnnotationDto} from '@adonis/webapp/services/http/entities/simple-dto/required-annotation-dto'
import {SemiAutomaticVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import {SimpleVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import {StateCodeDto} from '@adonis/webapp/services/http/entities/simple-dto/state-code-dto'
import {TestDto} from '@adonis/webapp/services/http/entities/simple-dto/test-dto'
import {UserPathDto} from '@adonis/webapp/services/http/entities/simple-dto/user-path-dto'
import {ValueListDto} from '@adonis/webapp/services/http/entities/simple-dto/value-list-dto'
import {VariableConnectionDto} from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import {VariableScaleDto} from '@adonis/webapp/services/http/entities/simple-dto/variable-scale-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import ProjectExplorerProvider, {
  DATA_ENTRY_PROJECT_LIBRARY_EXPLORER_NAME,
  DEVICE_LIBRARY_EXPLORER_NAME,
  GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME,
  SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
  SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME,
  STATE_CODE_LIBRARY_EXPLORER_NAME,
  VALUE_LIST_LIBRARY_EXPLORER_NAME,
  VARIABLE_LIBRARY_EXPLORER_NAME,
} from '@adonis/webapp/services/providers/explorer-providers/project-explorer-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import VueI18n from 'vue-i18n'
import {Store} from 'vuex'
import Site from "@adonis/shared/models/site"

export default class ProjectService extends AbstractService {

  constructor(
    protected store: Store<any>,
    protected notifier: NotificationService,
    protected httpService: WebappHttpService,
    private transformerService: TransformerService,
    protected explorerProvider: ProjectExplorerProvider,
    protected i18n: VueI18n,
  ) {
    super()
  }

  createStateCode(stateCode: StateCodeFormInterface, projectIri?: string) {

    const stateCodeDto = this.transformerService.stateCodeTransformer.formInterfaceToCreateDto(stateCode)
    stateCodeDto.site = !projectIri ? this.store.getters['navigation/getActiveRoleSite'].siteIri : undefined
    stateCodeDto.project = projectIri

    return this.httpService.post<StateCodeDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.STATE_CODE), stateCodeDto)
      .then(value => this.transformerService.stateCodeTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.STATE_CODE, ObjectActionEnum.CREATED)
        this.httpService.get<StateCodeExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/add2explorer', {
            item: this.explorerProvider.constructStateCodeExplorerItem(response.data),
            attachment: !projectIri ? STATE_CODE_LIBRARY_EXPLORER_NAME : `${projectIri}:stateCode`,
          }))
        return value
      })
  }

  updateStateCode(stateCodeIri: string, stateCode: StateCodeFormInterface): Promise<StateCode> {

    const stateCodeDto = this.transformerService.stateCodeTransformer.formInterfaceToUpdateDto(stateCode)
    return this.httpService.patch<StateCodeDto>(stateCodeIri, stateCodeDto)
      .then(value => this.transformerService.stateCodeTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.STATE_CODE, ObjectActionEnum.MODIFIED)
        this.httpService.get<StateCodeExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructStateCodeExplorerItem(response.data),
          }))
        return value
      })
  }

  deleteStateCode(stateCodeIri: string, notify = true) {

    return this.confirmDelete(() => this.httpService.delete(stateCodeIri)
      .then(() => {
        if (notify) {
          this.notifier.objectSuccess(ObjectTypeEnum.STATE_CODE, ObjectActionEnum.DELETED)
        }
        this.store.dispatch('navigation/removeExplorer', {itemName: stateCodeIri})
          .then()
      }))
  }

  addStateCodeToDataEntryProject(stateCodeIri: string, projectIri: string): Promise<any> {
    return this.httpService.get<StateCodeDto>(stateCodeIri)
      .then(response => this.transformerService.stateCodeTransformer.dtoToObject(response.data))
      .then(stateCode => this.transformerService.stateCodeTransformer.objectToFormInterface(stateCode))
      .then(stateCodeInterface => this.createStateCode(stateCodeInterface, projectIri))
  }

  orderVariable(variableIri: string, newOrder: number): Promise<any> {

    const variableDto = {
      order: newOrder,
    }
    return this.httpService.patch<SimpleVariableDto>(variableIri, variableDto)
      .then(() => this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.MODIFIED))
  }

  createSimpleVariable(variable: VariableFormInterface, projectIri?: string, siteIri = this.store.getters['navigation/getActiveRoleSite'].siteIri): Promise<SimpleVariable> {

    const variableDto = this.transformerService.simpleVariableTransformer.formInterfaceToCreateDto(variable)
    variableDto.site = !projectIri ? siteIri : undefined
    variableDto.project = projectIri
    return this.httpService.post<SimpleVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE), variableDto)
      .then(value => this.transformerService.simpleVariableTransformer.dtoToObject(value.data))
      .then(value => {
        if (!!siteIri || !!projectIri) {
          this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.CREATED)
          this.httpService.get<SimpleVariableExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
            .then(response => this.store.dispatch('navigation/add2explorer', {
              item: this.explorerProvider.constructSimpleVariableExplorerItem(response.data, !!projectIri),
              attachment: !projectIri ? SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME : `${projectIri}:variable`,
            }))
        }
        return value
      })
  }

  updateSimpleVariable(variableIri: string, variable: VariableFormInterface): Promise<SimpleVariable> {

    const variableDto = this.transformerService.simpleVariableTransformer.formInterfaceToUpdateDto(variable)
    return this.httpService.patch<SimpleVariableDto>(variableIri, variableDto)
      .then(value => this.transformerService.simpleVariableTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.MODIFIED)
        this.httpService.get<SimpleVariableExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructSimpleVariableExplorerItem(response.data, !!value.projectIri),
          }))
        return value

      })
  }

  updateSemiAutomaticVariable(variableIri: string,
                              variable: VariableFormInterface & ManagedVariableExtraInfoFormInterface,
  ): Promise<SemiAutomaticVariable> {

    const variableDto = this.transformerService.semiAutomaticVariableTransformer.formInterfaceToUpdateDto(variable)
    return this.httpService.patch<SemiAutomaticVariableDto>(variableIri, variableDto)
      .then(value => this.transformerService.semiAutomaticVariableTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE, ObjectActionEnum.MODIFIED)
        return value
      })
  }

  deleteVariable(variableIri: string) {

    return this.confirmDelete(() => this.httpService.delete(variableIri)
      .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: variableIri}))
      .then(() => this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.DELETED)),
    )
  }

  createConnectedVariable(
    variableConnection: VariableConnectionFormInterface,
    variableIri: string,
  ): Promise<VariableConnection> {
    const variableConnectionDto = this.transformerService.variableConnectionTransformer.formInterfaceToCreateDto(variableConnection)
    return this.httpService.post<VariableConnectionDto>(
      this.httpService.getEndpointFromType(ObjectTypeEnum.VARIABLE_CONNECTION),
      variableConnectionDto,
    )
      .then(value => this.transformerService.variableConnectionTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.VARIABLE_CONNECTION, ObjectActionEnum.CREATED)
        this.httpService.get<ConnectedVariableExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/add2explorer', {
            item: this.explorerProvider.constructConnectedVariableExplorerItem(response.data),
            attachment: variableIri,
          }))
        return value
      })
  }

  deleteConnectedVariable(connectedVariableIri: string): Promise<any> {

    return this.httpService.delete(connectedVariableIri)
      .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: connectedVariableIri}))
      .then(() => this.notifier.objectSuccess(ObjectTypeEnum.VARIABLE_CONNECTION, ObjectActionEnum.DELETED))
  }

  createGeneratorVariable(generatorVariableInterface: GeneratorInfoFormInterface & VariableFormInterface,
                          projectIri?: string,
                          siteIri = this.store.getters['navigation/getActiveRoleSite'].siteIri): Promise<GeneratorVariable> {

    const variableDto = this.transformerService.generatorVariableTransformer.formInterfaceToCreateDto(generatorVariableInterface)
    variableDto.site = !projectIri ? siteIri : undefined
    variableDto.project = projectIri
    return this.httpService.post<GeneratorVariableDto>(
      this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE), variableDto)
      .then(value => this.transformerService.generatorVariableTransformer.dtoToObject(value.data))
      .then(value => {
        if (!!projectIri || !!siteIri) {
          this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.CREATED)
          this.httpService.get<GeneratorVariableExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
            .then(response => this.store.dispatch('navigation/add2explorer', {
              item: this.explorerProvider.constructGeneratorVariableExplorerItem(response.data, !!projectIri),
              attachment: !projectIri ? GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME : `${projectIri}:variable`,
            }))
        }
        return value
      })
  }

  updateGeneratorVariable(variableIri: string,
                          generatorVariableInterface: GeneratorInfoFormInterface & VariableFormInterface): Promise<GeneratorVariable> {

    const variableDto = this.transformerService.generatorVariableTransformer.formInterfaceToUpdateDto((generatorVariableInterface))
    return this.httpService.patch<GeneratorVariableDto>(variableIri, variableDto)
      .then(value => this.transformerService.generatorVariableTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.GENERATOR_VARIABLE, ObjectActionEnum.MODIFIED)
        return value
      })
      .then(value => {
        this.httpService.get<GeneratorVariableExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructGeneratorVariableExplorerItem(response.data, !!value.projectIri),
          }))
        return value
      })
  }

  deleteGeneratorVariable(variableIri: string) {

    return this.confirmDelete(() => this.httpService.delete(variableIri)
      .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: variableIri}))
      .then(() => this.notifier.objectSuccess(ObjectTypeEnum.GENERATOR_VARIABLE, ObjectActionEnum.DELETED)))
  }

  createVariableScale(variableScale: VariableScaleFormInterface, variableIri: string): Promise<VariableScale> {
    const variableScaleDto = this.transformerService.variableScaleTransformer.formInterfaceToCreateDto(variableScale)
    variableScaleDto.variable = variableIri
    return this.httpService.post<VariableScaleDto>(
      this.httpService.getEndpointFromType(ObjectTypeEnum.VARIABLE_SCALE),
      variableScaleDto,
    )
      .then(value => this.transformerService.variableScaleTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.VARIABLE_SCALE, ObjectActionEnum.CREATED)
        this.httpService.get<SimpleVariableExplorerGetDto>(variableIri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructSimpleVariableExplorerItem(response.data),
          }))
        return value
      })
  }

  updateVariableScale(variableScale: VariableScaleFormInterface, variableScaleIri: string, variableIri: string): Promise<VariableScale> {
    const variableScaleDto = this.transformerService.variableScaleTransformer.formInterfaceToCreateDto(variableScale)
    return this.httpService.patch<VariableScaleDto>(variableScaleIri, variableScaleDto)
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.VARIABLE_SCALE, ObjectActionEnum.CREATED)
        this.httpService.get<SimpleVariableExplorerGetDto>(variableScaleIri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructVariableScaleExplorerItem(response.data, variableIri),
          }))
        return this.transformerService.variableScaleTransformer.dtoToObject(value.data)
      })
  }

  deleteVariableScale(variableScaleIri: string) {

    return this.confirmDelete(() => this.httpService.delete(variableScaleIri)
      .then(() => this.store.dispatch('navigation/removeExplorer', {itemName: variableScaleIri}))
      .then(() => this.notifier.objectSuccess(ObjectTypeEnum.VARIABLE_SCALE, ObjectActionEnum.DELETED)))
  }

  createValueList(valueList: ValueListFormInterface, variableIri: string = null): Promise<ValueList> {

    // TODO: Gérer la création d'une liste de valeur directement fils d'une variable
    const valueListDto = this.transformerService.valueListTransformer.formInterfaceToCreateDto(valueList)
    valueListDto.site = !variableIri ? this.store.getters['navigation/getActiveRoleSite'].siteIri : undefined
    return this.httpService.post<ValueListDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.VALUE_LIST), valueListDto)
      .then(value => this.transformerService.valueListTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.VALUE_LIST, ObjectActionEnum.CREATED)
        if (!variableIri) {
          this.httpService.get<ValueListExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
            .then(response => this.store.dispatch('navigation/add2explorer', {
              item: this.explorerProvider.constructValueListExplorerItem(response.data),
              attachment: VALUE_LIST_LIBRARY_EXPLORER_NAME,
            }))
        }
        return value
      })
  }

  updateValueList(valueListIri: string, valueList: ValueListFormInterface): Promise<ValueList> {

    const valueListDto = this.transformerService.valueListTransformer.formInterfaceToUpdateDto(valueList)
    return this.httpService.patch<ValueListDto>(valueListIri, valueListDto)
      .then(value => this.transformerService.valueListTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.VALUE_LIST, ObjectActionEnum.MODIFIED)
        this.httpService.get<ValueListExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructValueListExplorerItem(response.data),
          }))
        return value
      })
  }

  deleteValueList(valueListIri: string, notify = true) {

    return this.confirmDelete(() => this.httpService.delete(valueListIri)
      .then(() => {
        if (notify) {
          this.notifier.objectSuccess(ObjectTypeEnum.VALUE_LIST, ObjectActionEnum.DELETED)
        }
        this.store.dispatch('navigation/removeExplorer', {itemName: valueListIri})
          .then()
      }))
  }

  linkValueListToVariable(valueListIri: string, variableIri: string): Promise<ValueList> {

    return this.httpService.get<ValueListDto>(valueListIri)
      .then(valueListDto => this.httpService.post<ValueListDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.VALUE_LIST), {
        name: valueListDto.data.name,
        values: valueListDto.data.values,
        variable: variableIri,
      }))
      .then(value => this.transformerService.valueListTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.MODIFIED)
        this.httpService.get<SimpleVariableExplorerGetDto>(variableIri, {groups: ['project_explorer_view']})
          .then(response => {
            this.store.dispatch('navigation/updateExplorer', {
              item: this.explorerProvider.constructSimpleVariableExplorerItem(response.data),
            })
          })
        return value
      })
  }

  createDevice(device: DeviceFormInterface, projectIri?: string): Promise<Device> {

    const deviceDto = this.transformerService.deviceTransformer.formInterfaceToCreateDto(device)
    deviceDto.site = !projectIri ? this.store.getters['navigation/getActiveRoleSite'].siteIri : undefined
    deviceDto.project = projectIri
    return this.httpService.post<DeviceDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DEVICE), deviceDto)
      .then(value => this.transformerService.deviceTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.DEVICE, ObjectActionEnum.CREATED)
        if (!projectIri) {
          this.httpService.get<DeviceExplorerGetDto>(value.iri, {
            groups: ['project_explorer_view'],
            site: this.store.getters['navigation/getActiveRoleSite'].siteIri,
          })
            .then(response => {
              response.data.managedVariables.forEach(variable => this.store.dispatch('navigation/add2explorer', {
                item: this.explorerProvider.constructSemiAutomaticVariableExplorerItem(variable, response.data.alias),
                attachment: SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
              }))
              return response
            })
            .then(response => this.store.dispatch('navigation/add2explorer', {
              item: this.explorerProvider.constructDeviceExplorerItem(response.data),
              attachment: DEVICE_LIBRARY_EXPLORER_NAME,
            }))
        } else {
          this.refreshProjectExplorer(projectIri)
            .then()
        }
        return value
      })
  }

  updateDevice(deviceIri: string, device: DeviceFormInterface, projectIri?: string): Promise<Device> {

    const deviceDto = this.transformerService.deviceTransformer.formInterfaceToUpdateDto(device)
    return this.httpService.patch<DeviceDto>(deviceIri, deviceDto)
      .then(value => {
        return this.transformerService.deviceTransformer.dtoToObject(value.data)
      })
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.DEVICE, ObjectActionEnum.MODIFIED)
        if (!projectIri) {
          this.httpService.getAll<DeviceExplorerGetDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DEVICE), {
            groups: ['project_explorer_view'],
            site: this.store.getters['navigation/getActiveRoleSite'].siteIri,
          })
            .then(response => {
              this.store.dispatch('navigation/removeExplorerChildren', {
                itemName: SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
              })
                .then(() => response.data['hydra:member'].forEach(deviceExplorer => {
                  deviceExplorer.managedVariables.forEach(variable => this.store.dispatch('navigation/add2explorer', {
                    item: this.explorerProvider.constructSemiAutomaticVariableExplorerItem(variable, deviceExplorer.alias),
                    attachment: SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
                  }))
                }))
              return response
            })
          this.httpService.get<DeviceExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
            .then(response => this.store.dispatch('navigation/updateExplorer', {
              item: this.explorerProvider.constructDeviceExplorerItem(response.data),
            }))
        } else {
          this.refreshProjectExplorer(projectIri)
            .then()
        }
        return value
      })
  }

  deleteDevice(deviceIri: string, notify = true) {

    return this.confirmDelete(() => this.httpService.delete(deviceIri)
      .then(() => {
        if (notify) {
          this.notifier.objectSuccess(ObjectTypeEnum.DEVICE, ObjectActionEnum.DELETED)
        }

        // TODO: Supprimer dans l'explorer des variables
        this.store.dispatch('navigation/removeExplorer', {itemName: deviceIri})
          .then()
      }))
  }

  createDataEntryProject(dataEntryProject: ProjectFormInterface): Promise<Project> {

    const projectDto = this.transformerService.projectTransformer.formInterfaceToCreateDto(dataEntryProject)
    return this.httpService.post<ProjectDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DATA_ENTRY_PROJECT), projectDto)
      .then(value => this.transformerService.projectTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY_PROJECT, ObjectActionEnum.CREATED)
        this.refreshProjectExplorer(value.iri)
        return value
      })
  }

  transferProjectToMobile(projectIri: string, userIris: string[]): Promise<Project> {

    return this.httpService.patch<ProjectDto>(`${projectIri}/transfer`, {users: userIris})
      .then(value => this.transformerService.projectTransformer.dtoToObject(value.data))
  }

  addSimpleVariableToDataEntryProject(simpleVariableIri: string, projectIri: string): Promise<any> {
    const cloneDto: CloneVariableDto = {
      project: projectIri,
      variable: simpleVariableIri,
    }
    return this.httpService.post<CloneVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.CLONE_VARIABLE), cloneDto)
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.SIMPLE_VARIABLE, ObjectActionEnum.CREATED)
        this.httpService.get<SimpleVariableExplorerGetDto>(value.data.result, {groups: ['project_explorer_view']})
          .then(response => {
            this.store.dispatch('navigation/add2explorer', {
              item: this.explorerProvider.constructSimpleVariableExplorerItem(response.data, true),
              attachment: `${projectIri}:variable`,
            })
          })
      })
  }

  addGeneratorVariableToDataEntryProject(generatorVariableIri: string, projectIri: string): Promise<any> {
    const cloneDto: CloneVariableDto = {
      project: projectIri,
      variable: generatorVariableIri,
    }
    return this.httpService.post<CloneVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.CLONE_VARIABLE), cloneDto)
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.GENERATOR_VARIABLE, ObjectActionEnum.CREATED)
        this.httpService.get<GeneratorVariableExplorerGetDto>(value.data.result, {groups: ['project_explorer_view']})
          .then(response => {
            this.store.dispatch('navigation/add2explorer', {
              item: this.explorerProvider.constructGeneratorVariableExplorerItem(response.data, true),
              attachment: `${projectIri}:variable`,
            })
          })
      })
  }

  addSemiAutomaticVariableToDataEntryProject(semiAutomaticVariableIri: string, projectIri: string): Promise<any> {

    return this.httpService.getAll<DeviceDto>(
      this.httpService.getEndpointFromType(ObjectTypeEnum.DEVICE),
      {managedVariables: semiAutomaticVariableIri},
    )
      .then(response => {

        if (response.data['hydra:totalItems'] !== 1) {
          throw new Error('A variable must be linked to only one device')
        }
        const deviceDto = response.data['hydra:member'][0]
        deviceDto.managedVariables = deviceDto.managedVariables.filter(value => value['@id'] === semiAutomaticVariableIri)
        return this.httpService.getAll<DeviceDto>(
          this.httpService.getEndpointFromType(ObjectTypeEnum.DEVICE),
          {project: projectIri},
        )
          .then(responseProject => {
            const existingDevices = responseProject.data['hydra:member']
              .filter(projectDeviceDto => projectDeviceDto.alias === deviceDto.alias)
            if (existingDevices.length === 1) {
              const variableDto = deviceDto.managedVariables[0]
              variableDto.device = existingDevices[0]['@id']
              this.httpService.post<SemiAutomaticVariableDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE),
                variableDto,
              )
                .then(() => this.refreshProjectExplorer(projectIri))
            } else {
              return this.transformerService.deviceTransformer
                .objectToFormInterface(this.transformerService.deviceTransformer.dtoToObject(deviceDto))
                .then(formInterface => this.createDevice(formInterface, projectIri))
            }
          })
      })
  }

  addDeviceToDataEntryProject(deviceIri: string, projectIri: string): Promise<any> {

    return this.httpService.get<DeviceDto>(deviceIri)
      .then(response => this.transformerService.deviceTransformer.dtoToObject(response.data))
      .then(device => this.transformerService.deviceTransformer.objectToFormInterface(device))
      .then(deviceInterface => this.createDevice(deviceInterface, projectIri))

  }

  deleteDataEntryProject(projectIri: string) {

    return this.confirmDelete(() => this.httpService.delete(projectIri)
      .then(() => {
        this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY_PROJECT, ObjectActionEnum.DELETED)
        return this.store.dispatch('navigation/removeExplorer', {itemName: projectIri})
      }))
  }

  createRequiredAnnotation(requiredAnnotation: RequiredAnnotationFormInterface, projectIri: string): Promise<RequiredAnnotation> {

    const requiredAnnotationDto = this.transformerService.requiredAnnotationTransformer.formInterfaceToCreateDto(requiredAnnotation)
    requiredAnnotationDto.project = projectIri
    return this.httpService.post<RequiredAnnotationDto>(
      this.httpService.getEndpointFromType(ObjectTypeEnum.REQUIRED_ANNOTATION),
      requiredAnnotationDto,
    )
      .then(value => this.transformerService.requiredAnnotationTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.REQUIRED_ANNOTATION, ObjectActionEnum.CREATED)
        this.httpService.get<RequiredAnnotationExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/add2explorer', {
            item: this.explorerProvider.constructRequiredAnnotationExplorerItem(response.data),
            attachment: `${projectIri}:annotation`,
          }))
        return value
      })
  }

  updateRequiredAnnotation(requiredAnnotationInterface: RequiredAnnotationFormInterface, requiredAnnotationIri: string)
    : Promise<RequiredAnnotation> {
    const requiredAnnotationDto = this.transformerService.requiredAnnotationTransformer.formInterfaceToUpdateDto(requiredAnnotationInterface)
    return this.httpService.patch<RequiredAnnotationDto>(requiredAnnotationIri, requiredAnnotationDto)
      .then(value => this.transformerService.requiredAnnotationTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.REQUIRED_ANNOTATION, ObjectActionEnum.MODIFIED)
        this.httpService.get<RequiredAnnotationExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructRequiredAnnotationExplorerItem(response.data),
          }))
        return value
      })
  }

  deleteRequiredAnnotation(requiredAnnotationIri: string) {
    return this.confirmDelete(() => this.httpService.delete(requiredAnnotationIri)
      .then(() => {
        this.notifier.objectSuccess(ObjectTypeEnum.REQUIRED_ANNOTATION, ObjectActionEnum.DELETED)
        this.store.dispatch('navigation/removeExplorer', {itemName: requiredAnnotationIri})
      }))
  }

  createPath(pathBase: { base: PathBaseFormInterface, movement: PathBaseMovementFormInterface },
             projectIri: string, platformIri: string): Promise<PathBase> {

    const pathBaseDto = this.transformerService.pathBaseTransformer.formInterfaceToCreateDto(pathBase)
    pathBaseDto.project = projectIri
    return this.httpService.post<PathBaseDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PATH_BASE), pathBaseDto)
      .then(value => this.transformerService.pathBaseTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.PATH_BASE, ObjectActionEnum.CREATED)
        this.httpService.get<PathBaseExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => {
            const pathBaseExplorer = this.explorerProvider.constructPathBaseExplorerItem(response.data, projectIri, platformIri)
            return this.store.dispatch('navigation/add2explorer', {item: pathBaseExplorer, attachment: `${projectIri}:path`})
          })
        return value
      })
  }

  /**
   * Update base path for a data entries project.
   */
  public updatePath(
    pathBase: { base: PathBaseFormInterface, movement: PathBaseMovementFormInterface },
    pathBaseIri: string,
    projectIri: string,
  ) {
    const pathBaseDto = this.transformerService.pathBaseTransformer.formInterfaceToUpdateDto(pathBase)

    return this.httpService.patch<PathBaseDto>(pathBaseIri, pathBaseDto)
      .then(value => this.transformerService.pathBaseTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.PATH_BASE, ObjectActionEnum.MODIFIED)
        this.refreshProjectExplorer(projectIri)

        return value
      })
  }

  deletePath(pathBaseIri: string, projectIri: string) {
    return this.confirmDelete(() => this.httpService.delete(pathBaseIri)
      .then(() => {
        this.notifier.objectSuccess(ObjectTypeEnum.PATH_BASE, ObjectActionEnum.DELETED)
        this.refreshProjectExplorer(projectIri)
      }))
  }

  createUserPath(userPathFormData: UserPathFormInterface, projectIri: string, pathBaseIri: string): Promise<PathUserWorkflow> {

    const pathUserDto = this.transformerService.pathUserWorkflowTransformer.formInterfaceToUpdateDto(userPathFormData)
    pathUserDto.pathBase = pathBaseIri
    return this.httpService.post<UserPathDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.USER_PATH), pathUserDto)
      .then(userPathDtoData => this.transformerService.pathUserWorkflowTransformer.dtoToObject(userPathDtoData.data))
      .then(userPath => {
        this.notifier.objectSuccess(ObjectTypeEnum.USER_PATH, ObjectActionEnum.CREATED)
        this.refreshProjectExplorer(projectIri)
        return userPath
      })
  }

  updateUserPath(userPathFormData: UserPathFormInterface, userPathIri: string): Promise<PathUserWorkflow> {

    const userPathDto = this.transformerService.pathUserWorkflowTransformer.formInterfaceToUpdateDto(userPathFormData)
    return this.httpService.patch<UserPathDto>(userPathIri, userPathDto)
      .then(userPathDtoData => this.transformerService.pathUserWorkflowTransformer.dtoToObject(userPathDtoData.data))
      .then(userPath => {
        this.notifier.objectSuccess(ObjectTypeEnum.USER_PATH, ObjectActionEnum.MODIFIED)
        return userPath
      })
  }

  public deleteUserPath(userPathIri: string, projectIri: string, showConfirm: boolean = true) {
    const confirm = () => this.httpService.delete(userPathIri)
      .then(() => {
        this.notifier.objectSuccess(ObjectTypeEnum.USER_PATH, ObjectActionEnum.DELETED)
        this.refreshProjectExplorer(projectIri)
      })

    return showConfirm ? this.confirmDelete(confirm) : confirm()
  }

  refreshProjectExplorer(projectIri: string) {

    return this.httpService.getAll<PlatformExplorerGetDto>(
      this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM),
      {projects: projectIri, groups: ['project_explorer_view']},
    )
      .then(response => {
        const platformExplorer = this.explorerProvider.constructPlatformExplorerItem(response.data['hydra:member'][0])
        platformExplorer.collapsed = false
        platformExplorer.children.find(item => item.name === projectIri).collapsed = false
        if (!this.store.getters['navigation/getExplorerItem'](response.data['hydra:member'][0]['@id'])) {
          this.store.dispatch('navigation/add2explorer', {
            item: platformExplorer,
            attachment: DATA_ENTRY_PROJECT_LIBRARY_EXPLORER_NAME,
          })
        } else {
          this.store.dispatch('navigation/updateExplorer', {item: platformExplorer})
        }
      })
  }

  createTest(test: TestFormInterface): Promise<Test> {

    const testDto = this.transformerService.testTransformer.formInterfaceToCreateDto(test)
    return this.httpService.post<TestDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.TEST), testDto)
      .then(value => this.transformerService.testTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.TEST, ObjectActionEnum.CREATED)
        this.httpService.get<TestExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/add2explorer', {
            item: this.explorerProvider.constructTestExplorerItem(response.data),
            attachment: test.variable.iri,
          }))
        return value
      })
  }

  updateTest(testIri: string, test: TestFormInterface): Promise<Test> {

    const testDto = this.transformerService.testTransformer.formInterfaceToUpdateDto(test)
    return this.httpService.patch<TestDto>(testIri, testDto)
      .then(value => this.transformerService.testTransformer.dtoToObject(value.data))
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.TEST, ObjectActionEnum.MODIFIED)
        this.httpService.get<TestExplorerGetDto>(value.iri, {groups: ['project_explorer_view']})
          .then(response => this.store.dispatch('navigation/updateExplorer', {
            item: this.explorerProvider.constructTestExplorerItem(response.data),
          }))
        return value
      })
  }

  deleteTest(testIri: string, notify = true) {

    return this.confirmDelete(() => this.httpService.delete(testIri)
      .then(() => {
        if (notify) {
          this.notifier.objectSuccess(ObjectTypeEnum.TEST, ObjectActionEnum.DELETED)
        }
        this.store.dispatch('navigation/removeExplorer', {itemName: testIri})
          .then()
      }))
  }

  duplicateProject(cloneInterface: CloneProjectFormInterface): Promise<any> {

    const cloneDto = this.transformerService.cloneProjectTransformer.formInterfaceToCreateDto(cloneInterface)
    return this.httpService.post<CloneProjectDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.CLONE_PROJECT), cloneDto)
      .then(value => {
        this.notifier.objectSuccess(ObjectTypeEnum.DATA_ENTRY_PROJECT, ObjectActionEnum.CREATED)
        this.httpService.get<ProjectExplorerGetDto>(value.data.result, {groups: ['project_explorer_view']})
          .then(response => {
            const projectExplorer = this.explorerProvider.constructProjectExplorerItem(response.data, value.data.platform)
            this.store.dispatch('navigation/add2explorer', {item: projectExplorer, attachment: value.data.platform})
          })
      })
  }

  copyPathBase(pathBaseIri: string, projectIri: string) {
    this.httpService.get<ProjectDto>(projectIri).then(project => {
      if (!!project.data.pathBase) {
        this.notifier.objectFailure('Un cheminement est existant')
        return
      }
      this.httpService.get<PathBaseDto>(pathBaseIri)
        .then(pathBase => this.transformerService.pathBaseTransformer.dtoToObject(pathBase.data))
        .then(pathBase => {
          pathBase.project.then(pathBaseProject => {
            if (!pathBaseProject.platformIri || pathBaseProject.platformIri !== project.data.platform) {
              this.notifier.objectFailure('Le projet n\'est pas issu de la même plateforme')
              return
            }
            this.transformerService.pathBaseTransformer.objectToFormInterface(pathBase)
              .then(pathBaseInterface => this.createPath(pathBaseInterface, projectIri, project.data.platform)
                .then(newPathBase => pathBase.userPaths.map(userPathPromise => userPathPromise.then(userPath =>
                  this.transformerService.pathUserWorkflowTransformer.objectToFormInterface(userPath)
                    .then(item => this.createUserPath(item, projectIri, newPathBase.iri))))))
          })
        })

    })
  }

  importVariableXml() {
    this.importFile(FileTypeEnum.TYPE_VARIABLE_COLLECTION, IconEnum.VARIABLE, VARIABLE_LIBRARY_EXPLORER_NAME, () => {
      this.httpService.get<SiteProjectExplorerGetDto>(this.store.getters['navigation/getActiveRoleSite'].siteIri, {groups: ['project_explorer_view']})
        .then(value => {
          value.data.simpleVariables.forEach(variable => {
            if (!this.store.getters['navigation/getExplorerItem'](variable['@id'])) {
              const variableExplorer = this.explorerProvider.constructSimpleVariableExplorerItem(variable)
              variableExplorer.collapsed = true
              this.store.dispatch('navigation/add2explorer', {
                item: variableExplorer,
                attachment: SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME,
              })
            }
          })
          value.data.generatorVariables.forEach(variable => {
            if (!this.store.getters['navigation/getExplorerItem'](variable['@id'])) {
              const variableExplorer = this.explorerProvider.constructGeneratorVariableExplorerItem(variable)
              variableExplorer.collapsed = true
              this.store.dispatch('navigation/add2explorer', {
                item: variableExplorer,
                attachment: GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME,
              })
            }
          })
          value.data.devices.forEach(device => {
            if (!this.store.getters['navigation/getExplorerItem'](device['@id'])) {
              device.managedVariables.forEach(variable => this.store.dispatch('navigation/add2explorer', {
                item: this.explorerProvider.constructSemiAutomaticVariableExplorerItem(variable, device.alias),
                attachment: SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
              }))
              const variableExplorer = this.explorerProvider.constructDeviceExplorerItem(device)
              variableExplorer.collapsed = true
              this.store.dispatch('navigation/add2explorer', {
                item: variableExplorer,
                attachment: DEVICE_LIBRARY_EXPLORER_NAME,
              })
            }
          })
        })
    })
  }

  importPathBaseCsv(csvBindings: PathBaseCsvBindingFormInterface, projectIri: string, platformIri: string): Promise<any> {
    return this.importFile(FileTypeEnum.TYPE_PATH_BASE_CSV, IconEnum.PATH, projectIri, (job: ParsingJobDto) => {
      this.httpService.get<PathBaseExplorerGetDto>(job.objectIri, {groups: ['project_explorer_view']})
        .then(experiment => {
          const item = this.explorerProvider.constructPathBaseExplorerItem(experiment.data, projectIri, platformIri)
          return this.store.dispatch('navigation/add2explorer', {
            item,
            attachment: projectIri + ':path',
          })
        })
    }, csvBindings)
  }

  /**
   * Export variables from a site.
   *
   * `datasToExport` can be :
   *  * 'variables' : export all variables from the library,
   *  * 'ind_variables' : export all simple variables from the library,
   *  * 'auto_variables' : export all semiautomatic variables from the library,
   *  * 'gen_variables' : export all generator variables from the library,
   *  * project IRI : export all project's variables,
   *  * variable IRI : export variable.
   *
   * @param datasToExport variables to export
   */
  public exportVariables(datasToExport: string[]): Promise<any> {
    return this.store.getters['navigation/getActiveRoleSite'].site.then(
      (site: Site) => {
        const siteId = site.id
        const searchParams = new URLSearchParams()
        datasToExport.forEach((data) => {
          searchParams.append('datas[]', data)
        })

        return this.httpService.getFile('/api/download/variables/' + siteId + '?' + searchParams.toString(), 'application/xml')
      },
    )
  }
}
