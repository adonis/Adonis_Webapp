import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { ConfirmAction } from '@adonis/shared/models/confirm-action'
import FileTypeEnum from '@adonis/webapp/constants/file-type-enum'
import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'
import { FileDto } from '@adonis/webapp/services/http/entities/simple-dto/file-dto'
import { ParsingJobDto } from '@adonis/webapp/services/http/entities/simple-dto/parsing-job-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import VueI18n from 'vue-i18n'
import { Store } from 'vuex'

export default abstract class AbstractService {

    protected store: Store<any>
    protected notifier: NotificationService
    protected httpService: WebappHttpService
    protected explorerProvider: AbstractModuleExplorerProvider
    protected i18n: VueI18n

    protected importFile(type: FileTypeEnum, tmpIcon: IconEnum, attachment: string, successCallback: (job: ParsingJobDto) => void, csvBindings?: any) {

        const callback = (data: FormData) => {
            data.append('site', this.store.getters['navigation/getActiveRoleSite'].siteIri)
            data.append('objectType', type)
            return this.httpService.post<ParsingJobDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PARSING_JOB), data)
                .then(response => {
                    const tmpExplorerItem = this.explorerProvider.constructAwaitingExplorerItem(tmpIcon)
                    this.store.dispatch('navigation/add2explorer', {
                        item: tmpExplorerItem,
                        attachment,
                    })
                    const tmp = setInterval(() => {
                        this.httpService.get<ParsingJobDto>(response.data['@id'])
                            .then(job => {
                                switch (job.data.status) {
                                    case JobStatusEnum.FILE_STATUS_ERROR:
                                        !!job.data.errors ?
                                            this.notifier.parsingError(job.data.errors) :
                                            this.notifier.defaultObjectFailure(ObjectActionEnum.FETCHED)
                                        break
                                    case JobStatusEnum.FILE_STATUS_SUCCESS:
                                        if (job.data.errors?.length > 0) {
                                            this.notifier.parsingWarnings(job.data.errors)
                                        }
                                        successCallback(job.data)
                                        break
                                    default:
                                        return
                                }
                                this.store.dispatch('navigation/removeExplorer', {
                                    itemName: tmpExplorerItem.name,
                                })
                                clearInterval(tmp)
                            })
                    }, 4000)
                })
        }
        const formData = new FormData()
        switch (type) {
            case FileTypeEnum.TYPE_EXPERIMENT_CSV:
            case FileTypeEnum.TYPE_PROTOCOL_CSV:
            case FileTypeEnum.TYPE_DATA_ENTRY_CSV:
            case FileTypeEnum.TYPE_PATH_BASE_CSV:
                formData.append('file', csvBindings.file)
                formData.append('csvBindings', JSON.stringify({...csvBindings, file: undefined}))
                return callback(formData)
            case FileTypeEnum.TYPE_EXPERIMENT:
            case FileTypeEnum.TYPE_PLATFORM:
            case FileTypeEnum.TYPE_VARIABLE_COLLECTION:
                const fileSelector = document.createElement('input')
                fileSelector.setAttribute('type', 'file')
                fileSelector.setAttribute('accept', '.xml,application/xml,text/xml,.zip,application/zip')
                fileSelector.click()
                fileSelector.onchange = () => {
                    formData.append('file', fileSelector.files[0])
                    callback(formData)
                }
        }
    }

    protected confirmDelete(confirmCallback: () => any): Promise<any> {
        return new Promise((resolve, reject) => {
            const confirmAction: AsyncConfirmPopup = new AsyncConfirmPopup(
                'commons.deletePopup.title',
                ['commons.deletePopup.message'],
                [
                    new ConfirmAction(
                        'commons.confirm',
                        'error',
                        () => {
                            confirmCallback()
                            resolve()
                        },
                    ),
                    new ConfirmAction(
                        'commons.cancel',
                        'secondary',
                        reject,
                    ),
                ],
            )
            this.store.dispatch('confirm/askConfirm', confirmAction)
        })
    }

    uploadFiles(files: File[]): Promise<string[]> {
        return Promise.all(files.map(file => {
            const formData = new FormData()
            formData.append('file', file)
            return this.httpService.post<FileDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.FILE), formData)
                .then(response => response.data['@id'])
        }))
    }

    attachFileToObject(fileIri: string, objectIri: string, objectType: ObjectTypeEnum): Promise<any> {
        const dto: any = {
            file: fileIri,
        }
        let uri: string
        switch (objectType) {
            case ObjectTypeEnum.PLATFORM:
                dto.platform = objectIri
                uri = this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM_ATTACHMENT)
                break
            case ObjectTypeEnum.EXPERIMENT:
                dto.experiment = objectIri
                uri = this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT_ATTACHMENT)
                break
            case ObjectTypeEnum.PROTOCOL:
                dto.protocol = objectIri
                uri = this.httpService.getEndpointFromType(ObjectTypeEnum.PROTOCOL_ATTACHMENT)
                break
        }
        return this.httpService.post<any>(uri, dto)
    }

    deleteAttachedFile(iri: string): Promise<any> {
        return this.confirmDelete(() => this.httpService.delete(iri))
    }
}
