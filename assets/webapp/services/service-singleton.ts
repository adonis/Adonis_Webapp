import i18n from '@adonis/webapp/i18n/vue-i18n'
import AdminService from '@adonis/webapp/services/data-manipulation/admin-service'
import DataEntryService from '@adonis/webapp/services/data-manipulation/data-entry-service'
import DesignService from '@adonis/webapp/services/data-manipulation/design-service'
import PrintService from '@adonis/webapp/services/data-manipulation/print-service'
import ProjectService from '@adonis/webapp/services/data-manipulation/project-service'
import TransferService from '@adonis/webapp/services/data-manipulation/transfer-service'
import DragNDropService from '@adonis/webapp/services/drag-n-drop/drag-n-drop-service'
import OpenSilexHttpService from '@adonis/webapp/services/http/open-silex-http-service'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import ExplorerProvider from '@adonis/webapp/services/providers/explorer-provider'
import AdvancedGroupRightProvider from '@adonis/webapp/services/providers/http-providers/advanced-group-right-provider'
import AdvancedUserRightProvider from '@adonis/webapp/services/providers/http-providers/advanced-user-right-provider'
import AlgorithmProvider from '@adonis/webapp/services/providers/http-providers/algorithm-provider'
import AnnotationProvider from '@adonis/webapp/services/providers/http-providers/annotation-provider'
import BlockProvider from '@adonis/webapp/services/providers/http-providers/block-provider'
import BusinessObjectProvider from '@adonis/webapp/services/providers/http-providers/business-object-provider'
import DeviceProvider from '@adonis/webapp/services/providers/http-providers/device-provider'
import EnvProvider from '@adonis/webapp/services/providers/http-providers/env-provider'
import ExperimentProvider from '@adonis/webapp/services/providers/http-providers/experiment-provider'
import FactorProvider from '@adonis/webapp/services/providers/http-providers/factor-provider'
import FieldMeasureProvider from '@adonis/webapp/services/providers/http-providers/field-measure-provider'
import GeneratorVariableProvider from '@adonis/webapp/services/providers/http-providers/generator-variable-provider'
import GraphicalConfigurationProvider from '@adonis/webapp/services/providers/http-providers/graphical-configuration-provider'
import IndividualProvider from '@adonis/webapp/services/providers/http-providers/individual-provider'
import ModalityProvider from '@adonis/webapp/services/providers/http-providers/modality-provider'
import NorthIndicatorProvider from '@adonis/webapp/services/providers/http-providers/north-indicator-provider'
import OezNatureProvider from '@adonis/webapp/services/providers/http-providers/oez-nature-provider'
import OpenSilexInstanceProvider from '@adonis/webapp/services/providers/http-providers/open-silex-instance-provider'
import OutExperimentationZoneProvider from '@adonis/webapp/services/providers/http-providers/out-experimentation-zone-provider'
import ParametersProvider from '@adonis/webapp/services/providers/http-providers/parameters-provider'
import PathBaseProvider from '@adonis/webapp/services/providers/http-providers/path-base-provider'
import PathUserWorkflowProvider from '@adonis/webapp/services/providers/http-providers/path-user-workflow-provider'
import PlatformProvider from '@adonis/webapp/services/providers/http-providers/platform-provider'
import ProjectDataProvider from '@adonis/webapp/services/providers/http-providers/project-data-provider'
import ProjectProvider from '@adonis/webapp/services/providers/http-providers/project-provider'
import ProtocolProvider from '@adonis/webapp/services/providers/http-providers/protocol-provider'
import RequestFileProvider from '@adonis/webapp/services/providers/http-providers/request-file-provider'
import RequiredAnnotationProvider from '@adonis/webapp/services/providers/http-providers/required-annotation-provider'
import ResponseFileProvider from '@adonis/webapp/services/providers/http-providers/response-file-provider'
import RoleUserSiteProvider from '@adonis/webapp/services/providers/http-providers/role-user-site-provider'
import SemiAutomaticVariableProvider from '@adonis/webapp/services/providers/http-providers/semi-automatic-variable-provider'
import SessionProvider from '@adonis/webapp/services/providers/http-providers/session-provider'
import SimpleVariableProvider from '@adonis/webapp/services/providers/http-providers/simple-variable-provider'
import SiteProvider from '@adonis/webapp/services/providers/http-providers/site-provider'
import StateCodeProvider from '@adonis/webapp/services/providers/http-providers/state-code-provider'
import StatusDataEntryProvider from '@adonis/webapp/services/providers/http-providers/status-data-entry-provider'
import SubBlockProvider from '@adonis/webapp/services/providers/http-providers/sub-block-provider'
import SurfacicUnitPlotProvider from '@adonis/webapp/services/providers/http-providers/surfacic-unit-plot-provider'
import TestProvider from '@adonis/webapp/services/providers/http-providers/test-provider'
import TreatmentProvider from '@adonis/webapp/services/providers/http-providers/treatment-provider'
import UnitPlotProvider from '@adonis/webapp/services/providers/http-providers/unit-plot-provider'
import UserGroupProvider from '@adonis/webapp/services/providers/http-providers/user-group-provider'
import UserProvider from '@adonis/webapp/services/providers/http-providers/user-provider'
import ValueListProvider from '@adonis/webapp/services/providers/http-providers/value-list-provider'
import VariableConnectionProvider from '@adonis/webapp/services/providers/http-providers/variable-connection-provider'
import VariableProvider from '@adonis/webapp/services/providers/http-providers/variable-provider'
import VariableScaleProvider from '@adonis/webapp/services/providers/http-providers/variable-scale-provider'
import ViewDataBusinessObjectProvider from '@adonis/webapp/services/providers/http-providers/view-data-business-object-provider'
import ViewDataItemProvider from '@adonis/webapp/services/providers/http-providers/view-data-item-provider'
import ModuleBySiteProvider from '@adonis/webapp/services/providers/module-by-site.provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import store from '../stores/vuex'

class Chorus {

  notificationService = new NotificationService( store )

  httpService: WebappHttpService = new WebappHttpService( store, this.notificationService )

  transformerService = new TransformerService( this.httpService )

  roleUserSiteProvider = new RoleUserSiteProvider( this.httpService, this.transformerService )

  explorerProvider = new ExplorerProvider( this.httpService, this.transformerService, store )

  platformProvider = new PlatformProvider( this.httpService, this.transformerService )

  experimentProvider = new ExperimentProvider( this.httpService, this.transformerService )

  blockProvider = new BlockProvider( this.httpService, this.transformerService )

  subBlockProvider = new SubBlockProvider( this.httpService, this.transformerService )

  unitPlotProvider = new UnitPlotProvider( this.httpService, this.transformerService )

  surfacicUnitPlotProvider = new SurfacicUnitPlotProvider( this.httpService, this.transformerService )

  individualProvider = new IndividualProvider( this.httpService, this.transformerService )

  outExperimentationZoneProvider = new OutExperimentationZoneProvider( this.httpService, this.transformerService )

  businessObjectProvider = new BusinessObjectProvider( this.httpService, this.transformerService, this.platformProvider,
      this.experimentProvider )

  userProvider = new UserProvider( this.httpService, this.transformerService )

  siteProvider = new SiteProvider( this.httpService, this.transformerService )

  simpleVariableProvider = new SimpleVariableProvider( this.httpService, this.transformerService )

  generatorVariableProvider: GeneratorVariableProvider = new GeneratorVariableProvider( this.httpService, this.transformerService )

  semiAutomaticVariableProvider: SemiAutomaticVariableProvider = new SemiAutomaticVariableProvider( this.httpService,
      this.transformerService )

  deviceProvider = new DeviceProvider( this.httpService, this.transformerService )

  variableProvider: VariableProvider = new VariableProvider( this.httpService, this.transformerService,
      this.simpleVariableProvider, this.generatorVariableProvider, this.deviceProvider, this.semiAutomaticVariableProvider )

  factorProvider = new FactorProvider( this.httpService, this.transformerService )

  protocolProvider = new ProtocolProvider( this.httpService, this.transformerService )

  treatmentProvider = new TreatmentProvider( this.httpService, this.transformerService )

  modalityProvider = new ModalityProvider( this.httpService, this.transformerService )

  userGroupProvider = new UserGroupProvider( this.httpService, this.transformerService )

  advancedUserRightProvider = new AdvancedUserRightProvider( this.httpService, this.transformerService )

  advancedGroupRightProvider = new AdvancedGroupRightProvider( this.httpService, this.transformerService )

  designService = new DesignService( store, this.notificationService, this.httpService,
      this.transformerService, this.explorerProvider._designExplorerProvider, i18n )

  projectService = new ProjectService( store, this.notificationService, this.httpService,
      this.transformerService, this.explorerProvider._projectExplorerProvider, i18n )

  dataEntryService = new DataEntryService( store, this.notificationService, this.httpService,
      this.transformerService, this.explorerProvider._dataExplorerProvider, i18n )

  adminService = new AdminService( store, this.notificationService, this.httpService,
      this.transformerService, this.explorerProvider._adminExplorerProvider, i18n )

  transferService = new TransferService( store, this.notificationService, this.httpService,
      this.transformerService, this.explorerProvider._transferExplorerProvider, i18n )

  printService = new PrintService( store, this.notificationService, this.httpService, i18n )

  dragNDropService = new DragNDropService( store, this.designService, this.projectService )

  moduleProvider = new ModuleBySiteProvider()

  valueListProvider = new ValueListProvider( this.httpService, this.transformerService )

  stateCodeProvider = new StateCodeProvider( this.httpService, this.transformerService )

  projectProvider = new ProjectProvider( this.httpService, this.transformerService )

  statusDataEntryProvider = new StatusDataEntryProvider( this.httpService, this.transformerService )

  projectDataProvider = new ProjectDataProvider( this.httpService, this.transformerService )

  sessionProvider = new SessionProvider( this.httpService, this.transformerService )

  pathBaseProvider = new PathBaseProvider( this.httpService, this.transformerService )

  userPathProvider = new PathUserWorkflowProvider( this.httpService, this.transformerService )

  variableScaleProvider = new VariableScaleProvider( this.httpService, this.transformerService )

  graphicalConfigurationProvider = new GraphicalConfigurationProvider( this.httpService, this.transformerService )

  requiredAnnotationProvider = new RequiredAnnotationProvider( this.httpService, this.transformerService )

  oezNatureProvider = new OezNatureProvider( this.httpService, this.transformerService )

  algorithmProvider = new AlgorithmProvider( this.httpService, this.transformerService )

  variableConnectionProvider = new VariableConnectionProvider( this.httpService, this.transformerService )

  requestFileProvider = new RequestFileProvider( this.httpService, this.transformerService )

  responseFileProvider = new ResponseFileProvider( this.httpService, this.transformerService )

  fieldMeasureProvider = new FieldMeasureProvider( this.httpService, this.transformerService )

  annotationProvider = new AnnotationProvider( this.httpService, this.transformerService )

  northIndicatorProvider = new NorthIndicatorProvider( this.httpService, this.transformerService )

  parametersProvider = new ParametersProvider( this.httpService, this.transformerService )

  testProvider = new TestProvider( this.httpService, this.transformerService )

  envProvider = new EnvProvider( this.httpService, this.transformerService )

  openSilexInstanceProvider = new OpenSilexInstanceProvider( this.httpService, this.transformerService )

  openSilexHttpService = new OpenSilexHttpService(store, this.notificationService)

  viewDataItemProvider = new ViewDataItemProvider( this.httpService, this.transformerService )

  viewDataBusinessObjectProvider = new ViewDataBusinessObjectProvider( this.httpService, this.transformerService )
}

export default new Chorus()
