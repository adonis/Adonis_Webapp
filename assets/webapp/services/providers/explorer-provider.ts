import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ModuleEnum from '@adonis/webapp/constants/module-enum'
import {
  ROUTE_EDITOR_PARAMETER,
  ROUTE_EDITOR_SITE,
  ROUTE_EDITOR_SITE_LIST,
  ROUTE_EDITOR_SUPER_ADMIN_USER,
  ROUTE_EDITOR_USER_LIST,
} from '@adonis/webapp/router/routes-names'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import AdminExplorerProvider, {
  PARAMETERS_EXPLORER_NAME,
  SITES_EXPLORER_NAME,
  USERS_EXPLORER_NAME,
} from '@adonis/webapp/services/providers/explorer-providers/admin-explorer-provider'
import DataExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/data-explorer-provider'
import DesignExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/design-explorer-provider'
import ProjectExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/project-explorer-provider'
import TransferExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/transfer-explorer-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import Module from '@adonis/webapp/stores/navigation/models/module.model'
import VueRouter from 'vue-router'
import { Store } from 'vuex'

export default class ExplorerProvider {

  _designExplorerProvider: DesignExplorerProvider
  _projectExplorerProvider: ProjectExplorerProvider
  _dataExplorerProvider: DataExplorerProvider
  _transferExplorerProvider: TransferExplorerProvider
  _adminExplorerProvider: AdminExplorerProvider

  constructor(
      httpService: WebappHttpService,
      transformerService: TransformerService,
      store: Store<any>,
  ) {
    this._designExplorerProvider = new DesignExplorerProvider( httpService, transformerService, store )
    this._projectExplorerProvider = new ProjectExplorerProvider( httpService, transformerService, store )
    this._transferExplorerProvider = new TransferExplorerProvider( httpService, transformerService, store )
    this._dataExplorerProvider = new DataExplorerProvider( httpService, transformerService, store )
    this._adminExplorerProvider = new AdminExplorerProvider( httpService, transformerService, store )
  }

  getExplorersForModule( module: Module ): ExplorerItem[] {
    if (!module) {
      return []
    }
    return this.getProviderForModule( module )
        .getBaseExplorers()
  }

  initObjectExplorerForModule( module: Module ): Promise<any> {
    return this.getProviderForModule( module )
        .initObjectExplorers()
  }

  private getProviderForModule( module: Module ): AbstractModuleExplorerProvider {
    switch (module.name) {
      case ModuleEnum.DESIGN:
        return this._designExplorerProvider
      case ModuleEnum.PROJECT:
        return this._projectExplorerProvider
      case ModuleEnum.TRANSFER:
        return this._transferExplorerProvider
      case ModuleEnum.DATA:
        return this._dataExplorerProvider
      case ModuleEnum.ADMIN:
        return this._adminExplorerProvider
    }
  }

  getSuperAdminExplorers(): ExplorerItem[] {
    return [
      new ExplorerItem( {
        name: SITES_EXPLORER_NAME,
        label: 'navigation.explorer.sites.label',
        icon: IconEnum.SITE_LIBRARY,
        menu: [
          {
            title: 'navigation.menus.site.create',
            action: ( router: VueRouter ) => router.push( { name: ROUTE_EDITOR_SITE } ),
          },
          {
            title: 'navigation.menus.site.list',
            action: ( router: VueRouter ) => router.push( { name: ROUTE_EDITOR_SITE_LIST } ),
          },
        ],
        redirectRouteName: ROUTE_EDITOR_SITE_LIST,
      } ),
      new ExplorerItem( {
        name: USERS_EXPLORER_NAME,
        label: 'navigation.explorer.users.label',
        icon: IconEnum.GROUP,
        menu: [
          {
            title: 'navigation.menus.user.create',
            action: ( router: VueRouter ) => router.push( { name: ROUTE_EDITOR_SUPER_ADMIN_USER } ),
          },
          {
            title: 'navigation.menus.user.list',
            action: ( router: VueRouter ) => router.push( { name: ROUTE_EDITOR_USER_LIST } ),
          },
        ],
        redirectRouteName: ROUTE_EDITOR_USER_LIST,
      } ),
      new ExplorerItem( {
        name: PARAMETERS_EXPLORER_NAME,
        label: 'navigation.explorer.parameters.label',
        icon: IconEnum.PARAMETER,
        menu: [
          {
            title: 'navigation.menus.parameter.update',
            action: ( router: VueRouter ) => router.push( { name: ROUTE_EDITOR_PARAMETER } ),
          },
        ],
        redirectRouteName: ROUTE_EDITOR_PARAMETER,
      } ),
    ]
  }
}
