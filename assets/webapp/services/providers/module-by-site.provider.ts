import RelUserSite from '@adonis/shared/models/rel-user-site'
import ModuleEnum from '@adonis/webapp/constants/module-enum'
import SiteRoleEnum from '@adonis/webapp/constants/site-role-enum'
import { ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE, ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS } from '@adonis/webapp/router/routes-names'
import Module from '@adonis/webapp/stores/navigation/models/module.model'

export default class ModuleBySiteProvider {

    getModules(site: RelUserSite): Module[] {
        if (!site) {
            return []
        }
        const design = new Module({
            name: ModuleEnum.DESIGN,
            label: 'modules.design.label',
            selectable: true,
        })
        const project = new Module({
            name: ModuleEnum.PROJECT,
            label: 'modules.project.label',
            selectable: true,
        })
        const transfer = new Module({
            name: ModuleEnum.TRANSFER,
            label: 'modules.transfer.label',
            selectable: true,
            defaultRedirect: ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS,
        })
        const data = new Module({
            name: ModuleEnum.DATA,
            label: 'modules.data.label',
            selectable: true,
        })
        const admin = new Module({
            name: ModuleEnum.ADMIN,
            label: 'modules.admin.label',
            selectable: false,
            defaultRedirect: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE,
        })
        switch (site.role) {
            case SiteRoleEnum.SITE_ADMIN:
                return [
                    design,
                    project,
                    transfer,
                    data,
                    admin,
                ]
            case SiteRoleEnum.SITE_MANAGER:
                return [
                    design,
                    project,
                    transfer,
                    data,
                ]
            case SiteRoleEnum.SITE_EXPERT:
                return [
                    transfer,
                ]
            default:
                return []
        }
    }

    getDefaultModule(site: RelUserSite): Module {
        if (!site) {
            return null
        }
        return !!site
            ? this.getModules(site)[0]
            : null
    }
}
