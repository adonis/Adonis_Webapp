import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import StateCodeFormInterface from '@adonis/webapp/form-interfaces/state-code-form-interface'
import StateCode from '@adonis/webapp/models/state-code'
import { StateCodeDto } from '@adonis/webapp/services/http/entities/simple-dto/state-code-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class StateCodeProvider extends AbstractHttpProvider<StateCodeDto, StateCode> {

    doesNameExist(code: number, options: { siteIri: string, stateCodeIri?: string }): Promise<boolean> {
        return this.getAll({
            code,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(stateCode => stateCode.code === code && stateCode.iri !== options.stateCodeIri))
    }

    transformer(group?: string): AbstractTransformer<StateCodeDto, StateCode, StateCodeFormInterface> {

        return this.transformerService.stateCodeTransformer
    }

    get objectType(): ObjectTypeEnum {

        return ObjectTypeEnum.STATE_CODE
    }
}
