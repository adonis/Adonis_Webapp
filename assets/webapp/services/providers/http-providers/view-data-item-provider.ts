import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { ViewDataItem } from '@adonis/webapp/models/data-view/view-data-item'
import ProjectData from '@adonis/webapp/models/project-data'
import { ViewDataItemDto } from '@adonis/webapp/services/http/entities/data-view-dto/view-data-item-dto'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ViewDataItemProvider extends AbstractHttpProvider<ViewDataItemDto, ViewDataItem> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.DATA_VIEW_ITEM
    }

    transformer(group: string | undefined): AbstractTransformer<ViewDataItemDto, ViewDataItem, any> {
        switch (group) {
            default:
                return this.transformerService.viewDataItemTransformer
        }

    }

}
