import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import User from '@adonis/shared/models/user'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class UserProvider extends AbstractHttpProvider<UserDto, User> {

    transformer(group?: string): AbstractTransformer<UserDto, User, any> {
        return this.transformerService.userTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.USER
    }

    doesNameExist(username: string, options: { userIri?: string }): Promise<boolean> {
        return this.getAll({
            search_names: username,
            pagination: false,
            deleted: true,
        })
            .then(value => value.result.some(user => user.username === username && user.iri !== options.userIri))
    }
}
