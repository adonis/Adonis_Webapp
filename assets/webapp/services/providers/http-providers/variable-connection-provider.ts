import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VariableConnectionFormInterface from '@adonis/webapp/form-interfaces/variable-connection-form-interface'
import VariableConnection from '@adonis/webapp/models/variable-connection'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class VariableConnectionProvider extends AbstractHttpProvider<VariableConnectionDto, VariableConnection> {

    transformer(group?: string): AbstractTransformer<VariableConnectionDto, VariableConnection, VariableConnectionFormInterface> {
        switch (group) {
            case 'connected_variables':
                return this.transformerService.connectedVariablesVariableConnectionTransformer
            default:
                return this.transformerService.variableConnectionTransformer
        }
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.VARIABLE_CONNECTION
    }
}
