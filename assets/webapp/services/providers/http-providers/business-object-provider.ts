import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Block from '@adonis/webapp/models/block'
import Experiment from '@adonis/webapp/models/experiment'
import { Individual } from '@adonis/webapp/models/individual'
import Platform from '@adonis/webapp/models/platform'
import SubBlock from '@adonis/webapp/models/sub-block'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import UnitPlot from '@adonis/webapp/models/unit-plot'
import { BlockDto } from '@adonis/webapp/services/http/entities/simple-dto/block-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { IndividualDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-dto'
import { PlatformDto } from '@adonis/webapp/services/http/entities/simple-dto/platform-dto'
import { SubBlockDto } from '@adonis/webapp/services/http/entities/simple-dto/sub-block-dto'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import ExperimentProvider from '@adonis/webapp/services/providers/http-providers/experiment-provider'
import PlatformProvider from '@adonis/webapp/services/providers/http-providers/platform-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'

export default class BusinessObjectProvider extends AbstractHttpProvider<PlatformDto | ExperimentDto | BlockDto | SubBlockDto |
    UnitPlotDto | SurfacicUnitPlotDto | IndividualDto, Platform | Experiment | Block | SubBlock | UnitPlot | SurfacicUnitPlot |
    Individual> {

    constructor(
        httpService: WebappHttpService,
        transformerService: TransformerService,
        private platformProvider: PlatformProvider,
        private experimentProvider: ExperimentProvider,
    ) {
        super(httpService, transformerService)
    }

    getAll(params: any = {}): Promise<{ result: (Platform | Experiment)[], totalItems: number }> {
        return Promise.all([
            this.platformProvider.getAll(params),
            this.experimentProvider.getAll(params),
        ])
            .then(result => result.reduce((prev, current) => ({
                result: [...prev.result, ...current.result],
                totalItems: prev.totalItems + current.totalItems,
            }), {result: [], totalItems: 0}))
    }

    getByIri(iri: string, params?: any): Promise<Platform | Experiment> {
        if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM))) {
            return this.platformProvider.getByIri(iri, params)
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT))) {
            return this.experimentProvider.getByIri(iri, params)
        }
    }

    get objectType(): ObjectTypeEnum {
        return undefined
    }

    transformer(group?: string): AbstractTransformer<PlatformDto | ExperimentDto, Platform | Experiment, any> {
        return undefined
    }
}
