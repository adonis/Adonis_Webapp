import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import PathBaseFormInterface from '@adonis/webapp/form-interfaces/path-base-form-interface'
import PathBaseMovementFormInterface from '@adonis/webapp/form-interfaces/path-base-movement-form-interface'
import PathBase from '@adonis/webapp/models/path-base'
import { PathBaseDto } from '@adonis/webapp/services/http/entities/simple-dto/path-base-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class PathBaseProvider extends AbstractHttpProvider<PathBaseDto, PathBase> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.PATH_BASE
    }

    transformer(group?: string): AbstractTransformer<PathBaseDto, PathBase, {
        base: PathBaseFormInterface,
        movement: PathBaseMovementFormInterface
    }> {
        return this.transformerService.pathBaseTransformer
    }

}
