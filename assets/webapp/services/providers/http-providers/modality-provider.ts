import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Modality from '@adonis/webapp/models/modality'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ModalityProvider extends AbstractHttpProvider<ModalityDto, Modality> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.MODALITY
    }

    transformer(group?: string): AbstractTransformer<ModalityDto, Modality, any> {
        return this.transformerService.modalityTransformer
    }
}
