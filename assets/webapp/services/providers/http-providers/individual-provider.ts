import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { Individual } from '@adonis/webapp/models/individual'
import { IndividualDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class IndividualProvider extends AbstractHttpProvider<IndividualDto, Individual> {

    doesNameExist(individualName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: individualName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(individual => individual.name === individualName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.INDIVIDUAL
    }

    transformer(group?: string): AbstractTransformer<IndividualDto, Individual, any> {
        return this.transformerService.individualTransformer
    }
}
