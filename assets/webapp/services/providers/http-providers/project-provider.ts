import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ProjectFormInterface from '@adonis/webapp/form-interfaces/project-form-interface'
import Project from '@adonis/webapp/models/project'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ProjectProvider extends AbstractHttpProvider<ProjectDto, Project> {

    doesNameExist(projectName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: projectName,
            pagination: false,
            deleted: true,
            'platform.site': options.siteIri,
        })
            .then(value => value.result.some(project => project.name === projectName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.DATA_ENTRY_PROJECT
    }

    transformer(group?: string): AbstractTransformer<ProjectDto, Project, ProjectFormInterface> {
        return this.transformerService.projectTransformer
    }

}
