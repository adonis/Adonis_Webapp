import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import UserPathFormInterface from '@adonis/webapp/form-interfaces/user-path-form-interface'
import PathUserWorkflow from '@adonis/webapp/models/path-user-workflow'
import { UserPathDto } from '@adonis/webapp/services/http/entities/simple-dto/user-path-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class PathUserWorkflowProvider extends AbstractHttpProvider<UserPathDto, PathUserWorkflow> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.USER_PATH
    }

    transformer(group?: string): AbstractTransformer<UserPathDto, PathUserWorkflow, UserPathFormInterface> {
        return this.transformerService.pathUserWorkflowTransformer
    }

}
