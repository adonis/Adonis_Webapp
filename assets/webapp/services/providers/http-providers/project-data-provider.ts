import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ProjectData from '@adonis/webapp/models/project-data'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ProjectDataProvider extends AbstractHttpProvider<ProjectDataDto, ProjectData> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.DATA_ENTRY
    }

    transformer(group: string | undefined): AbstractTransformer<ProjectDataDto, ProjectData, any> {
        switch (group) {
            case 'webapp_data_view':
                return this.transformerService.projectDataDataViewTransformer
            case 'webapp_data_path':
                return this.transformerService.projectDataDataPathTransformer
            case 'webapp_data_fusion':
                return this.transformerService.projectDataDataFusionTransformer
            default:
                return this.transformerService.projectDataTransformer
        }

    }

}
