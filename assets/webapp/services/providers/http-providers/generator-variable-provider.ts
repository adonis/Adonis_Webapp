import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import GeneratorInfoFormInterface from '@adonis/webapp/form-interfaces/generator-info-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class GeneratorVariableProvider extends AbstractHttpProvider<GeneratorVariableDto, GeneratorVariable> {

    transformer(group?: string): AbstractTransformer<GeneratorVariableDto, GeneratorVariable, GeneratorInfoFormInterface & VariableFormInterface> {
        switch (group) {
            case 'connected_variables':
                return this.transformerService.connectedVariablesGeneratorVariableTransformer
            default:
                return this.transformerService.generatorVariableTransformer
        }
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.GENERATOR_VARIABLE
    }
}
