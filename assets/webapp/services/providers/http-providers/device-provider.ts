import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Device from '@adonis/webapp/models/device'
import { DeviceDto } from '@adonis/webapp/services/http/entities/simple-dto/device-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class DeviceProvider extends AbstractHttpProvider<DeviceDto, Device> {

    doesNameExist(deviceName: string, options: { siteIri?: string, projectIri?: string, deviceIri?: string }): Promise<boolean> {
        return this.getAll({
            alias: deviceName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
            project: options.projectIri,
        })
            .then(value => value.result.some(device => device.alias === deviceName && device.iri !== options.deviceIri))
    }

    transformer(group?: string): AbstractTransformer<DeviceDto, Device, any> {
        switch (group) {
            case 'connected_variables':
                return this.transformerService.connectedVariableDeviceTransformer
            default:
                return this.transformerService.deviceTransformer
        }
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.DEVICE
    }
}
