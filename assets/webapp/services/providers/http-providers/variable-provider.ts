import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import DeviceProvider from '@adonis/webapp/services/providers/http-providers/device-provider'
import GeneratorVariableProvider from '@adonis/webapp/services/providers/http-providers/generator-variable-provider'
import SemiAutomaticVariableProvider from '@adonis/webapp/services/providers/http-providers/semi-automatic-variable-provider'
import SimpleVariableProvider from '@adonis/webapp/services/providers/http-providers/simple-variable-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'

export default class VariableProvider extends AbstractHttpProvider<SimpleVariableDto | SemiAutomaticVariableDto | GeneratorVariableDto,
    SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {

    constructor(
        httpService: WebappHttpService,
        transformerService: TransformerService,
        private simpleVariableProvider: SimpleVariableProvider,
        private generatorVariableProvider: GeneratorVariableProvider,
        private deviceProvider: DeviceProvider,
        private semiAutomaticVariableProvider: SemiAutomaticVariableProvider,
    ) {
        super(httpService, transformerService)
    }

    doesNameExist(variableName: string, options: { siteIri?: string, projectIri?: string, variableIri?: string }): Promise<boolean> {
        return this.getAll({
            name: variableName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
            project: options.projectIri,
        })
            .then(value => value.result.some(variable => variable.name === variableName && variable.iri !== options.variableIri))
    }

    getAll(params: any = {}): Promise<{ result: (SimpleVariable | SemiAutomaticVariable | GeneratorVariable)[], totalItems: number }> {
        return Promise.all([
            this.simpleVariableProvider.getAll(params),
            this.generatorVariableProvider.getAll(params),
            this.deviceProvider.getAll(params)
                .then(async response => ({
                    totalItems: response.result.reduce((acc, device) => acc + device.managedVariablesIriTab.length, 0),
                    result: await Promise.all(response.result
                        .map(device => device.managedVariables)
                        .reduce((acc, item) => [...acc, ...item], [])),
                })),
        ])
            .then(result => result.reduce((prev, current) => ({
                result: [...prev.result, ...current.result],
                totalItems: prev.totalItems + current.totalItems,
            }), {result: [], totalItems: 0}))
    }

    getByIri(iri: string): Promise<SimpleVariable | SemiAutomaticVariable | GeneratorVariable> {
        if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE))) {
            return this.simpleVariableProvider.getByIri(iri)
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE))) {
            return this.generatorVariableProvider.getByIri(iri)
        } else if (iri.match(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE))) {
            return this.semiAutomaticVariableProvider.getByIri(iri)
        }
    }

    transformer(group?: string): any {
        return undefined
    }

    get objectType(): ObjectTypeEnum {
        return undefined
    }
}
