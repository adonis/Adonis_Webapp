import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class SimpleVariableProvider extends AbstractHttpProvider<SimpleVariableDto, SimpleVariable> {

    transformer(group?: string): AbstractTransformer<SimpleVariableDto, SimpleVariable, VariableFormInterface> {
        switch (group) {
            case 'connected_variables':
                return this.transformerService.connectedVariablesSimpleVariableTransformer
            default:
                return this.transformerService.simpleVariableTransformer
        }
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.SIMPLE_VARIABLE
    }
}
