import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class SurfacicUnitPlotProvider extends AbstractHttpProvider<SurfacicUnitPlotDto, SurfacicUnitPlot> {

    doesNameExist(unitPlotName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: unitPlotName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(up => up.name === unitPlotName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.SURFACIC_UNIT_PLOT
    }

    transformer(group?: string): AbstractTransformer<SurfacicUnitPlotDto, SurfacicUnitPlot, any> {
        switch (group) {
            case 'platform_full_view':
                return this.transformerService.surfacicUnitPlotFullTransformer
            default:
                return this.transformerService.surfacicUnitPlotTransformer
        }
    }
}
