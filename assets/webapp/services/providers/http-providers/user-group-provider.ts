import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import UserGroup from '@adonis/webapp/models/user-group'
import { UserGroupDto } from '@adonis/webapp/services/http/entities/simple-dto/user-group-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class UserGroupProvider extends AbstractHttpProvider<UserGroupDto, UserGroup> {

    doesNameExist(groupName: string, options: { siteIri: string, userGroupIri: string }): Promise<boolean> {
        return this.getAll({
            label: groupName,
            pagination: false,
            site: options.siteIri,
        })
            .then(value => value.result.some(group => group.name === groupName && group.iri !== options.userGroupIri))
    }

    transformer(group?: string): AbstractTransformer<UserGroupDto, UserGroup, any> {
        return this.transformerService.userGroupTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.USER_GROUP
    }
}
