import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import PlatformFormInterface from '@adonis/webapp/form-interfaces/platform-form-interface'
import Platform from '@adonis/webapp/models/platform'
import { PlatformDto } from '@adonis/webapp/services/http/entities/simple-dto/platform-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class PlatformProvider extends AbstractHttpProvider<PlatformDto, Platform> {

    doesNameExist(platformName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: platformName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(platform => platform.name === platformName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.PLATFORM
    }

    transformer(group: string | undefined): AbstractTransformer<PlatformDto, Platform, PlatformFormInterface> {
        switch (group) {
            case 'platform_full_view':
                return this.transformerService.platformFullTransformer
            default:
                return this.transformerService.platformTransformer
        }
    }
}
