import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import TestFormInterface from '@adonis/webapp/form-interfaces/test-form-interface'
import Test from '@adonis/webapp/models/test'
import { TestDto } from '@adonis/webapp/services/http/entities/simple-dto/test-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class TestProvider extends AbstractHttpProvider<TestDto, Test> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.TEST
    }

    transformer(group?: string): AbstractTransformer<TestDto, Test, TestFormInterface> {
        return this.transformerService.testTransformer
    }
}
