import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Treatment from '@adonis/webapp/models/treatment'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class TreatmentProvider extends AbstractHttpProvider<TreatmentDto, Treatment> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.TREATMENT
    }

    transformer(group?: string): AbstractTransformer<TreatmentDto, Treatment, any> {
        return this.transformerService.treatmentTransformer
    }
}
