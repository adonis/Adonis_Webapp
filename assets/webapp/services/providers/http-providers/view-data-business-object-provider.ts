import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { ViewDataBusinessObject } from '@adonis/webapp/models/data-view/view-data-business-object'
import { ViewDataItem } from '@adonis/webapp/models/data-view/view-data-item'
import ProjectData from '@adonis/webapp/models/project-data'
import { ViewDataBusinessObjectDto } from '@adonis/webapp/services/http/entities/data-view-dto/view-data-business-object-dto'
import { ViewDataItemDto } from '@adonis/webapp/services/http/entities/data-view-dto/view-data-item-dto'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ViewDataBusinessObjectProvider extends AbstractHttpProvider<ViewDataBusinessObjectDto, ViewDataBusinessObject> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.DATA_VIEW_BUSINESS_OBJECT
    }

    transformer(group: string | undefined): AbstractTransformer<ViewDataBusinessObjectDto, ViewDataBusinessObject, any> {
        switch (group) {
            default:
                return this.transformerService.viewDataBusinessObjectTransformer
        }

    }

}
