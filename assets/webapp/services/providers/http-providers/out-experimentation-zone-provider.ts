import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class OutExperimentationZoneProvider extends AbstractHttpProvider<OutExperimentationZoneDto, OutExperimentationZone> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.OEZ
    }

    transformer(group?: string): AbstractTransformer<OutExperimentationZoneDto, OutExperimentationZone, any> {
        return this.transformerService.outExperimentationZoneTransformer
    }
}
