import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'
import RelUserSite from '@adonis/shared/models/rel-user-site'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class RoleUserSiteProvider extends AbstractHttpProvider<RelUserSiteDto, RelUserSite> {

    transformer(group?: string): AbstractTransformer<RelUserSiteDto, RelUserSite, any> {
        return this.transformerService.relUserSiteTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.ROLE_USER_SITE
    }
}
