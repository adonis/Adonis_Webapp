import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Annotation from '@adonis/webapp/models/annotation'
import { AnnotationDto } from '@adonis/webapp/services/http/entities/simple-dto/annotation-dto'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'

export default class AnnotationProvider extends AbstractHttpProvider<AnnotationDto, Annotation> {

    constructor(
        httpService: WebappHttpService,
        transformerService: TransformerService,
    ) {
        super(httpService, transformerService)
    }

    transformer(group?: string): any {
        return this.transformerService.annotationTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.ANNOTATION
    }
}
