import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import OezNatureFormInterface from '@adonis/webapp/form-interfaces/oez-nature-form-interface'
import OezNature from '@adonis/webapp/models/oez-nature'
import { OezNatureDto } from '@adonis/webapp/services/http/entities/simple-dto/oez-nature-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class OezNatureProvider extends AbstractHttpProvider<OezNatureDto, OezNature> {

    doesNameExist(oezNatureName: string, options: { siteIri: string, oezNatureIri?: string }): Promise<boolean> {
        return this.getAll({
            nature: oezNatureName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(oezNature => oezNature.nature === oezNatureName && oezNature.iri !== options.oezNatureIri))
    }

    transformer(group?: string): AbstractTransformer<OezNatureDto, OezNature, OezNatureFormInterface> {
        return this.transformerService.oezNatureTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.OEZ_NATURE
    }
}
