import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import AdvancedUserRight from '@adonis/webapp/models/advanced-user-right'
import { AdvancedUserRightDto } from '@adonis/webapp/services/http/entities/simple-dto/advanced-user-right-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AdvancedUserRightProvider extends AbstractHttpProvider<AdvancedUserRightDto, AdvancedUserRight> {
    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.ADVANCED_RIGHT_USER
    }

    transformer(group?: string): AbstractTransformer<AdvancedUserRightDto, AdvancedUserRight, any> {
        return this.transformerService.advancedUserRightTransformer
    }

}
