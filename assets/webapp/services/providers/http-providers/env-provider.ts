import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Env from '@adonis/webapp/models/env'
import Modality from '@adonis/webapp/models/modality'
import { EnvDto } from '@adonis/webapp/services/http/entities/simple-dto/env-dto'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class EnvProvider extends AbstractHttpProvider<EnvDto, Env> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.ENV
    }

    transformer(group?: string): AbstractTransformer<EnvDto, Env, any> {
        return this.transformerService.envTransformer
    }
}
