import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import UnitPlot from '@adonis/webapp/models/unit-plot'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class UnitPlotProvider extends AbstractHttpProvider<UnitPlotDto, UnitPlot> {

    doesNameExist(unitPlotName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: unitPlotName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(up => up.name === unitPlotName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.UNIT_PLOT
    }

    transformer(group?: string): AbstractTransformer<UnitPlotDto, UnitPlot, any> {
        switch (group) {
            case 'platform_full_view':
                return this.transformerService.unitPlotFullTransformer
            default:
                return this.transformerService.unitPlotTransformer
        }
    }
}
