import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import NorthIndicatorFormInterface from '@adonis/webapp/form-interfaces/north-indicator-form-interface'
import NorthIndicator from '@adonis/webapp/models/north-indicator'
import { NorthIndicatorDto } from '@adonis/webapp/services/http/entities/simple-dto/north-indicator-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class NorthIndicatorProvider extends AbstractHttpProvider<NorthIndicatorDto, NorthIndicator> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.NORTH_INDICATOR
    }

    transformer(group?: string): AbstractTransformer<NorthIndicatorDto, NorthIndicator, NorthIndicatorFormInterface> {
        return this.transformerService.northIndicatorTransformer
    }
}
