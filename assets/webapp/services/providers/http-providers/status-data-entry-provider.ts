import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import StatusDataEntry from '@adonis/webapp/models/status-data-entry'
import { StatusDataEntryDto } from '@adonis/webapp/services/http/entities/simple-dto/status-data-entry-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class StatusDataEntryProvider extends AbstractHttpProvider<StatusDataEntryDto, StatusDataEntry> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.STATUS_DATA_ENTRY
    }

    transformer(group: string | undefined): AbstractTransformer<StatusDataEntryDto, StatusDataEntry, any> {
        return this.transformerService.statusDataEntryTransformer
    }

}
