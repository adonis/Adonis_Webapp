import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import RequestFile from '@adonis/webapp/models/request-file'
import { RequestFileDto } from '@adonis/webapp/services/http/entities/simple-dto/request-file-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class RequestFileProvider extends AbstractHttpProvider<RequestFileDto, RequestFile> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.REQUEST_FILE
    }

    transformer(group?: string): AbstractTransformer<RequestFileDto, RequestFile, any> {
        return this.transformerService.requestFileTransformer
    }
}
