import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Protocol from '@adonis/webapp/models/protocol'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ProtocolProvider extends AbstractHttpProvider<ProtocolDto, Protocol> {

    doesNameExist(protocolName: string, options: { siteIri: string, protocolIri?: string }): Promise<boolean> {
        return this.getAll({
            name: protocolName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(protocol => protocol.name === protocolName && protocol.iri !== options.protocolIri))
    }

    transformer(group?: string): AbstractTransformer<ProtocolDto, Protocol, any> {
        switch (group) {
            case 'platform_full_view':
            case 'protocol_full_view':
                return this.transformerService.protocolFullTransformer
            default:
                return this.transformerService.protocolTransformer
        }
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.PROTOCOL
    }

}
