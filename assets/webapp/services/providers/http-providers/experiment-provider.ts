import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Experiment from '@adonis/webapp/models/experiment'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ExperimentProvider extends AbstractHttpProvider<ExperimentDto, Experiment> {

    doesNameExist(experimentName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: experimentName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(experiment => experiment.name === experimentName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.EXPERIMENT
    }

    transformer(group?: string): AbstractTransformer<ExperimentDto, Experiment, any> {
        switch (group) {
            case 'platform_full_view':
                return this.transformerService.experimentFullTransformer
            default:
                return this.transformerService.experimentTransformer
        }
    }
}
