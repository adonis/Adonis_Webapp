import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import GraphicalConfigurationFormInterface from '@adonis/webapp/form-interfaces/graphical-configuration-form-interface'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import { GraphicalConfigurationDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-configuration-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class GraphicalConfigurationProvider extends AbstractHttpProvider<GraphicalConfigurationDto, GraphicalConfiguration> {

    transformer(group?: string): AbstractTransformer<GraphicalConfigurationDto, GraphicalConfiguration, GraphicalConfigurationFormInterface> {
        return this.transformerService.graphicalConfigurationTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.GRAPHICAL_CONFIGURATION
    }
}
