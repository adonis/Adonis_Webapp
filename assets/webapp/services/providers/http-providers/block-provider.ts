import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Block from '@adonis/webapp/models/block'
import { BlockDto } from '@adonis/webapp/services/http/entities/simple-dto/block-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class BlockProvider extends AbstractHttpProvider<BlockDto, Block> {

    doesNameExist(blockName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: blockName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(block => block.name === blockName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.BLOCK
    }

    transformer(group?: string): AbstractTransformer<BlockDto, Block, any> {
        switch (group) {
            case 'platform_full_view':
                return this.transformerService.blockFullTransformer
            default:
                return this.transformerService.blockTransformer
        }
    }
}
