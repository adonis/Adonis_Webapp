import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import FieldMeasure from '@adonis/webapp/models/field-measure'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class FieldMeasureProvider extends AbstractHttpProvider<FieldMeasureDto, FieldMeasure> {

    transformer(group?: string): AbstractTransformer<FieldMeasureDto, FieldMeasure, any> {
        return this.transformerService.fieldMeasureTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.FIELD_MEASURE
    }
}
