import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ResponseFile from '@adonis/webapp/models/response-file'
import { ResponseFileDto } from '@adonis/webapp/services/http/entities/simple-dto/response-file-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ResponseFileProvider extends AbstractHttpProvider<ResponseFileDto, ResponseFile> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.RESPONSE_FILE
    }

    transformer(group?: string): AbstractTransformer<ResponseFileDto, ResponseFile, any> {
        return this.transformerService.responseFileTransformer
    }
}
