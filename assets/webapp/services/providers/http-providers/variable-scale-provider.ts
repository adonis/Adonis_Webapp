import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VariableScaleFormInterface from '@adonis/webapp/form-interfaces/variable-scale-form-interface'
import VariableScale from '@adonis/webapp/models/variable-scale'
import { VariableScaleDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-scale-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class VariableScaleProvider extends AbstractHttpProvider<VariableScaleDto, VariableScale> {

    transformer(group?: string): AbstractTransformer<VariableScaleDto, VariableScale, VariableScaleFormInterface> {
        return this.transformerService.variableScaleTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.VARIABLE_SCALE
    }
}
