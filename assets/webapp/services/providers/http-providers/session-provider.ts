import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import SessionFormInterface from '@adonis/webapp/form-interfaces/session-form-interface'
import Session from '@adonis/webapp/models/session'
import { SessionDto } from '@adonis/webapp/services/http/entities/simple-dto/session-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class SessionProvider extends AbstractHttpProvider<SessionDto, Session> {

    get objectType(): ObjectTypeEnum {

        return ObjectTypeEnum.SESSION
    }

    transformer(group: string | undefined): AbstractTransformer<SessionDto, Session, SessionFormInterface> {

        return this.transformerService.sessionTransformer
    }
}
