import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Parameters from '@adonis/webapp/models/parameters'
import { ParametersDto } from '@adonis/webapp/services/http/entities/simple-dto/parameters-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ParametersProvider extends AbstractHttpProvider<ParametersDto, Parameters> {

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.PARAMETERS
    }

    transformer(group?: string): AbstractTransformer<ParametersDto, Parameters, any> {
        return this.transformerService.parametersTransformer
    }

    getSingleton(): Promise<Parameters> {
        return this.getByIri(this.httpService.getEndpointFromType(this.objectType) + '/1')
    }
}
