import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import OpenSilexInstanceFormInterface from '@adonis/webapp/form-interfaces/open-silex-instance-form-interface'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'
import UserGroup from '@adonis/webapp/models/user-group'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import { UserGroupDto } from '@adonis/webapp/services/http/entities/simple-dto/user-group-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class OpenSilexInstanceProvider extends AbstractHttpProvider<OpenSilexInstanceDto, OpenSilexInstance> {

    transformer(group?: string): AbstractTransformer<OpenSilexInstanceDto, OpenSilexInstance, OpenSilexInstanceFormInterface> {
        return this.transformerService.openSilexInstanceTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.OPEN_SILEX_INSTANCE
    }
}
