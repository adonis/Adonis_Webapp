import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ManagedVariableExtraInfoFormInterface from '@adonis/webapp/form-interfaces/managed-variable-extra-info-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class SemiAutomaticVariableProvider extends AbstractHttpProvider<SemiAutomaticVariableDto, SemiAutomaticVariable> {

    transformer(group?: string): AbstractTransformer<SemiAutomaticVariableDto, SemiAutomaticVariable, (VariableFormInterface & ManagedVariableExtraInfoFormInterface)> {
        switch (group) {
            case 'connected_variables':
                return this.transformerService.connectedVariablesSemiAutomaticVariableTransformer
            default:
                return this.transformerService.semiAutomaticVariableTransformer
        }
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE
    }
}
