import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import AdvancedGroupRight from '@adonis/webapp/models/advanced-group-right'
import { AdvancedGroupRightDto } from '@adonis/webapp/services/http/entities/simple-dto/advanced-group-right-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AdvancedGroupRightProvider extends AbstractHttpProvider<AdvancedGroupRightDto, AdvancedGroupRight> {
    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.ADVANCED_RIGHT_GROUP
    }

    transformer(group?: string): AbstractTransformer<AdvancedGroupRightDto, AdvancedGroupRight, any> {
        return this.transformerService.advancedGroupRightTransformer
    }

}
