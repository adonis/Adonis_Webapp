import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { SiteDto } from '@adonis/shared/models/dto/site-dto'
import Site from '@adonis/shared/models/site'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class SiteProvider extends AbstractHttpProvider<SiteDto, Site> {

    doesNameExist(siteName: string, options: { siteIri?: string }): Promise<boolean> {
        return this.getAll({
            label: siteName,
            pagination: false,
            deleted: true,
        })
            .then(value => value.result.some(site => site.label === siteName && site.iri !== options.siteIri))
    }

    transformer(group?: string): AbstractTransformer<SiteDto, Site, any> {
        return this.transformerService.siteTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.SITE
    }
}
