import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ApiEntity from '@adonis/shared/models/api-entity'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'

export default abstract class AbstractHttpProvider<D extends AbstractDtoObject, O extends ApiEntity> {

    constructor(
        protected httpService: WebappHttpService,
        protected transformerService: TransformerService,
    ) {
    }

    getAll(params: any = {}): Promise<{ result: O[], totalItems: number }> {
        return this.httpService.getAll<D>(this.httpService.getEndpointFromType(this.objectType), params)
            .then(value => {
                return {
                    result: value.data['hydra:member'].map(siteDto => this.transformer(params.groups?.[0])
                        .dtoToObject(siteDto)),
                    totalItems: value.data['hydra:totalItems'],
                }
            })
    }

    getByIri(iri: string, params: any = {}): Promise<O> {
        return this.httpService.get<D>(iri, params)
            .then(response => this.transformer(params.groups?.[0])
                .dtoToObject(response.data))
    }

    doesNameExist(name: string | number, options: {}): Promise<boolean> {
        return Promise.resolve(true)
    }

    abstract transformer(group?: string): AbstractTransformer<D, O, any>

    abstract get objectType(): ObjectTypeEnum
}
