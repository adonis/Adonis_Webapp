import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Algorithm from '@adonis/webapp/models/algorithm'
import { AlgorithmDto } from '@adonis/webapp/services/http/entities/simple-dto/algorithm-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AlgorithmProvider extends AbstractHttpProvider<AlgorithmDto, Algorithm> {

    transformer(group?: string): AbstractTransformer<AlgorithmDto, Algorithm, any> {
        return this.transformerService.algorithmTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.ALGORITHM
    }
}
