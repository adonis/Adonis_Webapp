import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import Factor from '@adonis/webapp/models/factor'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class FactorProvider extends AbstractHttpProvider<FactorDto, Factor> {

    doesNameExist(factorName: string, options: { siteIri: string, factorIri?: string }): Promise<boolean> {
        return this.getAll({
            name: factorName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(factor => factor.label === factorName && factor.iri !== options.factorIri))
    }

    transformer(group?: string): AbstractTransformer<FactorDto, Factor, any> {
        return this.transformerService.factorTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.FACTOR
    }
}
