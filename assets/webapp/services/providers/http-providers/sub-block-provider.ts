import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import SubBlock from '@adonis/webapp/models/sub-block'
import { SubBlockDto } from '@adonis/webapp/services/http/entities/simple-dto/sub-block-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class SubBlockProvider extends AbstractHttpProvider<SubBlockDto, SubBlock> {

    doesNameExist(subBlockName: string, options: { siteIri: string }): Promise<boolean> {
        return this.getAll({
            name: subBlockName,
            pagination: false,
            deleted: true,
            site: options.siteIri,
        })
            .then(value => value.result.some(block => block.name === subBlockName))
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.SUB_BLOCK
    }

    transformer(group?: string): AbstractTransformer<SubBlockDto, SubBlock, any> {
        switch (group) {
            case 'platform_full_view':
                return this.transformerService.subBlockFullTransformer
            default:
                return this.transformerService.subBlockTransformer
        }
    }
}
