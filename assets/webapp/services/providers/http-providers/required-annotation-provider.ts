import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import RequiredAnnotationFormInterface from '@adonis/webapp/form-interfaces/required-annotation-form-interface'
import RequiredAnnotation from '@adonis/webapp/models/required-annotation'
import { RequiredAnnotationDto } from '@adonis/webapp/services/http/entities/simple-dto/required-annotation-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class RequiredAnnotationProvider extends AbstractHttpProvider<RequiredAnnotationDto, RequiredAnnotation> {

    transformer(group?: string): AbstractTransformer<RequiredAnnotationDto, RequiredAnnotation, RequiredAnnotationFormInterface> {
        return this.transformerService.requiredAnnotationTransformer
    }

    get objectType(): ObjectTypeEnum {
        return ObjectTypeEnum.REQUIRED_ANNOTATION
    }
}
