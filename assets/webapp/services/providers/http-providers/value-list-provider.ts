import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ValueListFormInterface from '@adonis/webapp/form-interfaces/value-list-form-interface'
import ValueList from '@adonis/webapp/models/value-list'
import { ValueListDto } from '@adonis/webapp/services/http/entities/simple-dto/value-list-dto'
import AbstractHttpProvider from '@adonis/webapp/services/providers/http-providers/abstract-http-provider'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ValueListProvider extends AbstractHttpProvider<ValueListDto, ValueList> {

    transformer(group?: string): AbstractTransformer<ValueListDto, ValueList, ValueListFormInterface> {

        return this.transformerService.valueListTransformer
    }

    get objectType(): ObjectTypeEnum {

        return ObjectTypeEnum.VALUE_LIST
    }
}
