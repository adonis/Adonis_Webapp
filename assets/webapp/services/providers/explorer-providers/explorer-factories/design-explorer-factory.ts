import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import {
    ROUTE_EDITOR_ADVANCED_RIGHTS,
    ROUTE_EDITOR_DUPLICATE_EXPERIMENT,
    ROUTE_EDITOR_EXPERIMENT_EXPORT,
    ROUTE_EDITOR_EXPERIMENT_IDENTIFICATION_CODE,
    ROUTE_EDITOR_EXPERIMENT_STATE,
    ROUTE_EDITOR_FACTOR,
    ROUTE_EDITOR_GERMPLASM_FETCH,
    ROUTE_EDITOR_GRAPHIC,
    ROUTE_EDITOR_LINK_EXPERIMENT,
    ROUTE_EDITOR_OEZ_NATURE,
    ROUTE_EDITOR_PLATFORM_GRAPHICAL_INFO,
    ROUTE_EDITOR_PROTOCOL,
    ROUTE_NOTE_PAD,
    ROUTE_PDF_GENERATOR,
    ROUTE_SITE,
} from '@adonis/webapp/router/routes-names'
import { BlockExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/block-explorer-get-dto'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import { FactorExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/factor-explorer-get-dto'
import { IndividualExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/individual-explorer-get-dto'
import { ModalityExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/modality-explorer-get-dto'
import { OezNatureExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/oez-nature-explorer-get-dto'
import {
    OutExperimentationZoneExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/out-experimentation-zone-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'
import { SubBlockExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/sub-block-explorer-get-dto'
import { SurfacicUnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/surfacic-unit-plot-explorer-get-dto'
import { TreatmentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/treatment-explorer-get-dto'
import { UnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/unit-plot-explorer-get-dto'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import VueRouter from 'vue-router'

export default class DesignExplorerFactory {

    createOezNatureExplorerItem(oezNature: OezNatureExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.OEZ_KIND,
            name: oezNature['@id'],
            iri: oezNature['@id'],
            label: oezNature.nature,
            menu: [
                {
                    title: 'navigation.menus.oezNature.edit',
                    action: router => router.push({name: ROUTE_EDITOR_OEZ_NATURE, query: {oezNatureIri: oezNature['@id']}}),
                },
                {
                    title: 'navigation.menus.oezNature.delete',
                    action: () => CHORUS.designService.deleteOezNature(oezNature['@id']),
                },
            ],
        })
    }

    createFactorExplorerItem(factor: FactorExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.FACTOR,
            name: factor['@id'],
            iri: factor['@id'],
            label: factor.name,
            order: factor.order,
            objectType: ObjectTypeEnum.FACTOR,
            menu: [
                {
                    title: 'navigation.menus.factor.edit',
                    action: (router) => router.push({
                        name: factor.germplasm ?
                            ROUTE_EDITOR_GERMPLASM_FETCH :
                            ROUTE_EDITOR_FACTOR,
                        query: {
                            factorIri: factor['@id'],
                        },
                    }),
                },
                {
                    title: 'navigation.menus.factor.delete',
                    action: () => CHORUS.designService.deleteFactor(factor['@id']),
                },
            ],
        })
    }

    createModalityExplorerItem(modality: ModalityExplorerGetDto, context?: { namePrefix?: string }): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.MODALITY,
            name: (context?.namePrefix ?? '') + modality['@id'],
            iri: modality['@id'],
            label: `${modality.value}${!!modality.shortName ? ' (' + modality.shortName + ')' : ''}`,
            objectType: ObjectTypeEnum.MODALITY,
        })
    }

    createProtocolExplorerItem(protocol: ProtocolExplorerGetDto, context?: { linkedToExperiment?: boolean }): ExplorerItem {

        const explorer = new ExplorerItem({
            icon: IconEnum.PROTOCOL,
            name: protocol['@id'],
            iri: protocol['@id'],
            label: protocol.name,
            menu: [],
            objectType: ObjectTypeEnum.PROTOCOL,
        })
        if (!context?.linkedToExperiment) {
            explorer.menu = [
                {
                    title: 'navigation.menus.protocol.delete',
                    action: () => CHORUS.designService.deleteProtocol(protocol['@id']),
                },
                {
                    title: 'navigation.menus.protocol.edit',
                    action: (router) => router.push({name: ROUTE_EDITOR_PROTOCOL, query: {protocolIri: protocol['@id']}}),
                },
                {
                    title: 'navigation.menus.common.manageRight',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_ADVANCED_RIGHTS, query: {
                            objectId: protocol['@id'].match(
                                new RegExp(`${CHORUS.httpService.getEndpointFromType( ObjectTypeEnum.PROTOCOL )}/([\\d]+)$`),
                            )[1],
                            objectType: AdvancedRightClassIdentifierEnum.PROTOCOL,
                            owner: protocol.owner,
                        },
                    }),
                },
            ]
        }
        explorer.menu.push({
            title: 'navigation.menus.protocol.synthesis',
            action: () => CHORUS.printService.generateProtocolSynthesisReport(protocol['@id']),
        })
        return explorer
    }

    createProtocolTreatmentsExplorerItem(protocol: ProtocolExplorerGetDto, context: {
        collapsedCallback: () => Promise<void>
    }): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.TREATMENT_LIBRARY,
            name: protocol['@id'] + 'treatments',
            iri: protocol['@id'],
            label: 'navigation.explorer.library.protocols.treatments',
            collapsedCallback: context.collapsedCallback,
        })
    }

    createProtocolFactorExplorerItem(protocol: ProtocolExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.FACTOR_LIBRARY,
            name: protocol['@id'] + 'factors',
            iri: protocol['@id'],
            label: 'navigation.explorer.library.protocols.factors',
        })
    }

    createTreatmentExplorerItem(treatment: TreatmentExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.TREATMENT,
            name: treatment['@id'],
            iri: treatment['@id'],
            label: treatment.name,
            objectType: ObjectTypeEnum.TREATMENT,
        })
    }

    createExperimentExplorerItem(experiment: ExperimentExplorerGetDto, context?: {
        linkedToPlatform?: boolean,
        hasOpenSilexInstance?: boolean,
    }): ExplorerItem {

        const explorer = new ExplorerItem({
            icon: experiment.state === ExperimentStateEnum.CREATED ? IconEnum.EXPERIMENT : experiment.state === ExperimentStateEnum.VALIDATED ? IconEnum.VALID_EXPERIMENT : IconEnum.LOCKED_EXPERIMENT,
            name: experiment['@id'],
            iri: experiment['@id'],
            label: experiment.name,
            objectType: ObjectTypeEnum.EXPERIMENT,
            menu: [
                {
                    title: 'navigation.menus.experiment.note',
                    action: (router) => router.push({
                        name: ROUTE_NOTE_PAD,
                        query: {objectId: experiment['@id'], objectType: ObjectTypeEnum.EXPERIMENT, edit: 'edit'},
                    }),
                },
                {
                    title: 'navigation.menus.experiment.csvExport',
                    action: () => CHORUS.designService.exportToCsv(experiment['@id'], experiment.name),
                },
                {
                    title: 'navigation.menus.common.visualize',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_GRAPHIC,
                        query: {objectIri: experiment['@id'], mode: GraphicalEditorModeEnum.CONSULT},
                    }),
                },
                {
                    title: 'navigation.menus.experiment.xmlExport',
                    action: () => CHORUS.designService.exportExperimentXml(experiment.id, experiment['@id']),
                },
                ...context.linkedToPlatform && experiment.state > ExperimentStateEnum.CREATED && context.hasOpenSilexInstance ? [{
                    title: 'navigation.menus.experiment.openSilexExport',
                    action: (router: VueRouter) => router.push({
                        name: ROUTE_EDITOR_EXPERIMENT_EXPORT,
                        query: {experimentIri: experiment['@id']},
                    }),
                }] : [],
                {
                    title: 'navigation.menus.experiment.duplicate',
                    action: router => router.push({
                        name: ROUTE_EDITOR_DUPLICATE_EXPERIMENT,
                        query: {experimentIri: experiment['@id']},
                    }),
                },
            ],
        })
        if (!!context?.linkedToPlatform) {
            if (experiment.state === ExperimentStateEnum.CREATED) {
                explorer.menu.unshift({
                    title: 'navigation.menus.experiment.unlink',
                    action: () => CHORUS.designService.unlinkExperimentFromPlatform(experiment['@id']),
                })
            }
            explorer.menu.unshift(
                {
                    title: 'navigation.menus.experiment.state',
                    action: router => router.push({
                        name: ROUTE_EDITOR_EXPERIMENT_STATE,
                        query: {experimentIri: experiment['@id']},
                    }),
                },
            )
            explorer.menu.push(
                {
                    title: 'navigation.menus.experiment.identificationCodes',
                    action: router => router.push({
                        name: ROUTE_EDITOR_EXPERIMENT_IDENTIFICATION_CODE,
                        query: {experimentIri: experiment['@id']},
                    }),
                },
                {
                    title: 'navigation.menus.experiment.exportNotes',
                    action: () => CHORUS.designService.exportToCsv(experiment['@id'] + '/notes', experiment.name),
                },
            )
        } else {
            explorer.menu.unshift(
                {
                    title: 'navigation.menus.common.edit',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_GRAPHIC,
                        query: {objectIri: experiment['@id'], mode: GraphicalEditorModeEnum.EDIT},
                    }),
                },
                {
                    title: 'navigation.menus.experiment.delete',
                    action: (router) => {
                        CHORUS.designService.deleteExperiment(experiment['@id']).then(() => {
                            if (router.currentRoute.name === ROUTE_EDITOR_GRAPHIC && router.currentRoute.query.objectIri === experiment['@id']) {
                                router.push({name: ROUTE_SITE})
                            }
                        })
                    },
                },
                {
                    title: 'navigation.menus.common.manageRight',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_ADVANCED_RIGHTS, query: {
                            objectId: experiment['@id'].match(
                                new RegExp(`${CHORUS.httpService.getEndpointFromType( ObjectTypeEnum.EXPERIMENT )}/([\\d]+)$`),
                            )[1],
                            objectType: AdvancedRightClassIdentifierEnum.EXPERIMENT,
                            owner: experiment.owner,
                        },
                    }),
                })
        }
        return explorer
    }

    createPlatformExplorerItem(platform: PlatformExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.PLATFORM,
            name: platform['@id'],
            iri: platform['@id'],
            label: platform.name,
            types: [ExplorerTypeEnum.PLATFORM],
            objectType: ObjectTypeEnum.PLATFORM,
            menu: [
                ...(platform.projects.length === 0 ? [{
                    title: 'navigation.menus.platform.delete',
                    action: (router: VueRouter) => {
                        CHORUS.designService.deletePlatform(platform['@id']).then(() => {
                            if (router.currentRoute.name === ROUTE_EDITOR_GRAPHIC && router.currentRoute.query.objectIri === platform['@id']) {
                                router.push({name: ROUTE_SITE})
                            }
                        })
                    },
                }] : []),
                {
                    title: 'navigation.menus.common.visualize',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_GRAPHIC,
                        query: {objectIri: platform['@id'], mode: GraphicalEditorModeEnum.CONSULT},
                    }),
                },
                {
                    title: 'navigation.menus.common.edit',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_GRAPHIC,
                        query: {objectIri: platform['@id'], mode: GraphicalEditorModeEnum.EDIT},
                    }),
                },
                {
                    title: 'navigation.menus.platform.linkDevice',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_LINK_EXPERIMENT,
                        query: {platformIri: platform['@id']},
                    }),
                },
                {
                    title: 'navigation.menus.common.manageRight',
                    action: (router) => router.push({
                        name: ROUTE_EDITOR_ADVANCED_RIGHTS, query: {
                            objectId: platform['@id'].match(
                                new RegExp(`${CHORUS.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM)}/([\\d]+)$`),
                            )[1],
                            objectType: AdvancedRightClassIdentifierEnum.PLATFORM,
                            owner: platform.owner,
                        },
                    }),
                },
                {
                    title: 'navigation.menus.platform.originMeshSize',
                    action: (router: VueRouter) => router.push({
                        name: ROUTE_EDITOR_PLATFORM_GRAPHICAL_INFO,
                        query: {platformIri: platform['@id']},
                    }),
                },
                {
                    title: 'navigation.menus.platform.csvExport',
                    action: () => CHORUS.designService.exportToCsv(platform['@id'], platform.name),
                },
                {
                    title: 'navigation.menus.platform.xmlExport',
                    action: () => CHORUS.designService.exportPlatformXml(platform.id, platform['@id']),
                },
                {
                    title: 'navigation.menus.platform.synthesis',
                    action: (router) => router.push({
                        name: ROUTE_PDF_GENERATOR,
                        query: {
                            pdfType: ObjectTypeEnum.PLATFORM,
                            objectIri: platform['@id'],
                        },
                    }),
                },
            ],
        })
    }

    createBlockExplorerItem(block: BlockExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.BLOCK,
            name: block['@id'],
            iri: block['@id'],
            label: block.number,
            order: parseInt(block.number, 10),
            objectType: ObjectTypeEnum.BLOCK,
        })
    }

    createSubBlockExplorerItem(subBlock: SubBlockExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.SUB_BLOCK,
            name: subBlock['@id'],
            iri: subBlock['@id'],
            label: subBlock.number,
            order: parseInt(subBlock.number, 10),
            objectType: ObjectTypeEnum.SUB_BLOCK,
        })
    }

    createUnitPlotExplorerItem(unitPlot: UnitPlotExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.UNIT_PLOT,
            name: unitPlot['@id'],
            iri: unitPlot['@id'],
            label: unitPlot.number + ' ' + unitPlot.treatment.name,
            order: parseInt(unitPlot.number, 10),
            objectType: ObjectTypeEnum.UNIT_PLOT,
        })
    }

    createSurfacicUnitPlotExplorerItem(unitPlot: SurfacicUnitPlotExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.UNIT_PLOT,
            name: unitPlot['@id'],
            iri: unitPlot['@id'],
            label: unitPlot.number + ` ${unitPlot.treatment.name} (${unitPlot.x}, ${unitPlot.y})`,
            order: parseInt(unitPlot.number, 10),
            objectType: ObjectTypeEnum.SURFACIC_UNIT_PLOT,
        })
    }

    createIndividualExplorerItem(individual: IndividualExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.INDIVIDUAL,
            name: individual['@id'],
            iri: individual['@id'],
            label: individual.number + ` (${individual.x}, ${individual.y})`,
            order: parseInt(individual.number, 10),
            objectType: ObjectTypeEnum.INDIVIDUAL,
        })
    }

    createOezExplorerItem(oez: OutExperimentationZoneExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.OEZ_KIND, // TODO changer l'icône
            name: oez['@id'],
            iri: oez['@id'],
            label: oez.nature.nature + ` (${oez.x}, ${oez.y})`,
            objectType: ObjectTypeEnum.OEZ,
        })
    }
}
