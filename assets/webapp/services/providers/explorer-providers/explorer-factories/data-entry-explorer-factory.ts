import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import {
  ROUTE_EDITOR_DATA_ENTRY_TABLE,
  ROUTE_EDITOR_EXPORT_PROJECT_DATAS,
  ROUTE_EDITOR_FUSION_PROJECT_DATAS,
  ROUTE_EDITOR_GRAPHIC,
  ROUTE_PDF_GENERATOR,
} from '@adonis/webapp/router/routes-names'
import {DeviceExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/device-explorer-get-dto'
import {GeneratorVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import {PlatformExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import {ProjectDataExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/project-data-explorer-get-dto'
import {
  SemiAutomaticVariableExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/semi-automatic-variable-explorer-get-dto'
import {SessionExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/session-explorer-get-dto'
import {SimpleVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import {ValueListExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import store from '@adonis/webapp/stores/vuex'
import moment from 'moment'
import VueRouter from 'vue-router'

/**
 * Explorer Factory of DataEntry Module.
 */
export default class DataEntryExplorerFactory {

  createDataEntryExplorerItem(dataEntry: ProjectDataExplorerGetDto, context: {
    hasOpenSilexInstance?: boolean,
  }): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.DATA_ENTRY,
      name: dataEntry['@id'],
      iri: dataEntry['@id'],
      label: `${dataEntry.name} ${moment(dataEntry.end).format('YYYY-MM-DD')}`,
      objectType: ObjectTypeEnum.DATA_ENTRY,
      menu: [
        {
          title: 'navigation.menus.dataEntry.delete',
          action: () => {
            CHORUS.dataEntryService.deleteDataEntry(dataEntry['@id'])
          },
        },
        {
          title: 'navigation.menus.dataEntry.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_DATA_ENTRY_TABLE,
            query: {
              objectType: ObjectTypeEnum.DATA_ENTRY,
              objectIri: store.getters['navigation/getSelectedExplorerItems'].every((item: ExplorerItem) => item.objectType === ObjectTypeEnum.DATA_ENTRY)
                ? store.getters['navigation/getSelectedExplorerItems'].map((item: ExplorerItem) => item.name)
                : [dataEntry['@id']],
            },
          }),
        },
        {
          title: 'navigation.menus.dataEntry.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_DATA_ENTRY_TABLE,
            query: {
              objectType: ObjectTypeEnum.DATA_ENTRY,
              objectIri: store.getters['navigation/getSelectedExplorerItems'].every((item: ExplorerItem) => item.objectType === ObjectTypeEnum.DATA_ENTRY)
                ? store.getters['navigation/getSelectedExplorerItems'].map((item: ExplorerItem) => item.name)
                : [dataEntry['@id']],
              edit: 'Y',
            },
          }),
        },
        {
          title: 'navigation.menus.dataEntry.synthesis',
          action: (router) => router.push({
            name: ROUTE_PDF_GENERATOR,
            query: {
              pdfType: ObjectTypeEnum.DATA_ENTRY,
              objectIri: dataEntry['@id'],
            },
          }),
        },
        {
          title: 'navigation.menus.dataEntry.modificationReport',
          action: () => CHORUS.printService.generateModificationReport(dataEntry['@id']),
        },
        {
          title: 'navigation.menus.dataEntry.fusion',
          action: (router) => router.push({
            name: ROUTE_EDITOR_FUSION_PROJECT_DATAS,
            query: {
              projectIri: dataEntry.project,
            },
          }),
        },
        ...context.hasOpenSilexInstance ? [
          {
            title: 'navigation.menus.dataEntry.export',
            action: (router: VueRouter) => router.push({
              name: ROUTE_EDITOR_EXPORT_PROJECT_DATAS,
              query: {
                dataEntryIri: dataEntry['@id'],
              },
            }),
          },
        ] : [],
      ],
    })
  }

  createSessionExplorerItem(session: SessionExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.SESSION,
      name: session['@id'],
      iri: session['@id'],
      label: 'Session ' + moment(session.endedAt)
        .format('YYYY-MM-DD'),
      objectType: ObjectTypeEnum.SESSION,
      menu: [
        {
          title: 'navigation.menus.dataEntry.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_DATA_ENTRY_TABLE,
            query: {
              objectType: ObjectTypeEnum.SESSION,
              objectIri: [session['@id']],
            },
          }),
        },
        {
          title: 'navigation.menus.dataEntry.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_DATA_ENTRY_TABLE,
            query: {
              objectType: ObjectTypeEnum.SESSION,
              objectIri: [session['@id']],
              edit: 'Y',
            },
          }),
        },
      ],
    })
  }

  createPlatformExplorerItem(platform: PlatformExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.PLATFORM,
      name: platform['@id'],
      iri: platform['@id'],
      label: platform.name,
      objectType: ObjectTypeEnum.PLATFORM,
    })
  }

  createVariableExplorerItem(variable: SimpleVariableExplorerGetDto, context: { platformIri: string }): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.VARIABLE,
      name: variable['@id'],
      iri: variable['@id'],
      label: variable.name,
      objectType: ObjectTypeEnum.SIMPLE_VARIABLE,
      menu: [
        {
          title: 'navigation.menus.common.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GRAPHIC,
            query: {objectIri: context.platformIri, mode: GraphicalEditorModeEnum.VIEW_VARIABLE, variableIri: variable['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.synthesis',
          action: () => CHORUS.printService.generateVariableSynthesisReport(variable['@id']),
        },
      ],
    })
  }

  createValueListExplorerItem(valueList: ValueListExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.VALUE_LIST,
      name: valueList['@id'],
      iri: valueList['@id'],
      label: valueList.name,
      objectType: ObjectTypeEnum.VALUE_LIST,
      menu: [
        {
          title: 'navigation.menus.valueList.updateList',
          action: () => CHORUS.dataEntryService.updateValueList(valueList['@id']),
        },
      ],
    })
  }

  createSemiAutomaticVariableExplorerItem(variable: SemiAutomaticVariableExplorerGetDto,
                                          context: { explorerSuffix: string; platformIri: string })
    : ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.SEMI_AUTOMATIC_VARIABLE,
      name: variable['@id'] + (context.explorerSuffix ?? ''),
      iri: variable['@id'],
      label: variable.name + ` (${variable.device?.alias})`,
      objectType: ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE,
      menu: [
        {
          title: 'navigation.menus.common.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GRAPHIC,
            query: {objectIri: context.platformIri, mode: GraphicalEditorModeEnum.VIEW_VARIABLE, variableIri: variable['@id']},
          }),
        },
      ],
    })
  }

  createDeviceExplorerItem(device: DeviceExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.DEVICE,
      name: device['@id'],
      iri: device['@id'],
      label: device.alias,
      objectType: ObjectTypeEnum.DEVICE,
    })
  }

  createGeneratorVariableExplorerItem(generatorVariable: GeneratorVariableExplorerGetDto, context: {
    platformIri: string
  }): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.GENERATOR_VARIABLE,
      name: generatorVariable['@id'],
      iri: generatorVariable['@id'],
      label: generatorVariable.name,
      objectType: ObjectTypeEnum.GENERATOR_VARIABLE,
      menu: [
        {
          title: 'navigation.menus.common.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GRAPHIC,
            query: {
              objectIri: context.platformIri,
              mode: GraphicalEditorModeEnum.VIEW_VARIABLE,
              variableIri: generatorVariable['@id'],
            },
          }),
        },
      ],
    })
  }
}
