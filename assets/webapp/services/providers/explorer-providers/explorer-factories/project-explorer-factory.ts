import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import {APP_URL} from '@adonis/shared/env'
import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import {
  ROUTE_EDITOR_ADVANCED_RIGHTS,
  ROUTE_EDITOR_DEVICE,
  ROUTE_EDITOR_DUPLICATE_PROJECT,
  ROUTE_EDITOR_GENERATOR_VARIABLE,
  ROUTE_EDITOR_GRAPHIC,
  ROUTE_EDITOR_IMPORT_VARIABLE_OPENSILEX,
  ROUTE_EDITOR_LINK_VALUE_LIST,
  ROUTE_EDITOR_LINKED_VARIABLE,
  ROUTE_EDITOR_MODULE_PROJECT_LINK_STATE_CODE_TO_PROJECT,
  ROUTE_EDITOR_MODULE_PROJECT_LINK_VARIABLE_TO_PROJECT,
  ROUTE_EDITOR_MODULE_PROJECT_ORDER_VARIABLE,
  ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
  ROUTE_EDITOR_MODULE_PROJECT_TRANSFER_TO_MOBILE,
  ROUTE_EDITOR_PATH_BASE_CSV_IMPORT,
  ROUTE_EDITOR_PATH_CONFIGURATION,
  ROUTE_EDITOR_REQUIRED_ANNOTATION,
  ROUTE_EDITOR_STATE_CODE,
  ROUTE_EDITOR_USER_PATH,
  ROUTE_EDITOR_VALUE_LIST,
  ROUTE_EDITOR_VARIABLE,
  ROUTE_EDITOR_VARIABLE_SCALE,
  ROUTE_EDITOR_VARIABLE_SEMI_AUTOMATIC,
  ROUTE_PDF_GENERATOR,
} from '@adonis/webapp/router/routes-names'
import {ConnectedVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/connected-variable-explorer-get-dto'
import {DeviceExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/device-explorer-get-dto'
import {ExperimentExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import {GeneratorVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import {PathBaseExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/path-base-explorer-get-dto'
import {PathUserWorkflowExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/path-user-workflow-explorer-get-dto'
import {ProjectExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import {
  RequiredAnnotationExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/required-annotation-explorer-get-dto'
import {
  SemiAutomaticVariableExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/semi-automatic-variable-explorer-get-dto'
import {SimpleVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import {StateCodeExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/state-code-explorer-get-dto'
import {TestExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/test-explorer-get-dto'
import {ValueListExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'
import {VariableScaleExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/variable-scale-explorer-get-dto'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import VueRouter from 'vue-router'
import store from "@adonis/webapp/stores/vuex"
import {
  GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME,
  SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
  SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME,
  VARIABLE_LIBRARY_EXPLORER_NAME,
} from "@adonis/webapp/services/providers/explorer-providers/project-explorer-provider"

export default class ProjectExplorerFactory {

  public createVariablesExportMenu() {
    return {
      title: 'navigation.menus.variable.export',
      action: () => {
        const VARIABLES_MENUS_NAMES = [
          VARIABLE_LIBRARY_EXPLORER_NAME,
          SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME,
          GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME,
          SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
        ]
        const VARIABLES_TYPES = [
          ObjectTypeEnum.GENERATOR_VARIABLE.toString(),
          ObjectTypeEnum.SIMPLE_VARIABLE.toString(),
          ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE.toString(),
        ]

        const toExport: string[] = store.getters['navigation/getSelectedExplorerItems'].map((item: ExplorerItem) => {
          let data: string | null = null
          if (VARIABLES_MENUS_NAMES.indexOf(item.name) > -1) {
            data = item.name
          } else if (item.name.startsWith('/api/projects/')) {
            data = item.name.replace(':variable', '')
          } else if (item.iri && VARIABLES_TYPES.indexOf(item.objectType) > -1) {
            data = item.iri
          }

          return data
        }).filter((value: string | null) => value !== null)

        CHORUS.projectService.exportVariables(toExport).then()
      },
    }
  }

  createGeneratorVariableExplorerItem(generatorVariable: GeneratorVariableExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.GENERATOR_VARIABLE,
      name: generatorVariable['@id'],
      iri: generatorVariable['@id'],
      label: generatorVariable.name,
      types: [ExplorerTypeEnum.GENERATOR_VARIABLE],
      objectType: ObjectTypeEnum.GENERATOR_VARIABLE,
      menu: [
        {
          title: 'navigation.menus.variable.delete',
          action: () => CHORUS.projectService.deleteGeneratorVariable(generatorVariable['@id']),
        },
        {
          title: 'navigation.menus.variable.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GENERATOR_VARIABLE,
            query: {variableIri: generatorVariable['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.orderVariables',
          action: (router) => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_ORDER_VARIABLE,
            query: {generatorVariableIri: generatorVariable['@id']},
          }),
        },
        this.createVariablesExportMenu(),
      ],
    })
  }

  createVariableExplorerItem(variable: SimpleVariableExplorerGetDto,
                             context: { withTest: boolean, explorerType?: ExplorerTypeEnum })
    : ExplorerItem {

    const types = [ExplorerTypeEnum.SIMPLE_VARIABLE]
    const menu = [
      {
        title: 'navigation.menus.variable.delete',
        action: () => CHORUS.projectService.deleteVariable(variable['@id']),
      },
      {
        title: 'navigation.menus.variable.edit',
        action: (router: VueRouter) => router.push({
          name: ROUTE_EDITOR_VARIABLE,
          query: {variableIri: variable['@id']},
        }),
      },
      this.createVariablesExportMenu(),
    ]
    if (variable.type === VariableTypeEnum.ALPHANUMERIC) {
      types.push(ExplorerTypeEnum.VARIABLE_ALPHANUMERIC)
      menu.push({
        title: 'navigation.menus.variable.linkValueList',
        action: (router) => router.push({
          name: ROUTE_EDITOR_LINK_VALUE_LIST,
          query: {variableIri: variable['@id']},
        }),
      })
    }
    if (variable.type === VariableTypeEnum.INTEGER || variable.type === VariableTypeEnum.REAL) {
      menu.push({
        title: 'navigation.menus.variable.addScale',
        action: (router) => router.push({
          name: ROUTE_EDITOR_VARIABLE_SCALE,
          query: {variableIri: variable['@id']},
        }),
      })
    }
    if (!!context.withTest) {
      menu.push({
        title: 'navigation.menus.test.create',
        action: (router) => router.push({
          name: ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
          query: {variableIri: variable['@id']},
        }),
      })
    }
    return new ExplorerItem({
      icon: IconEnum.VARIABLE,
      name: variable['@id'],
      iri: variable['@id'],
      label: variable.name,
      types: !!context?.explorerType ? [context.explorerType] : types,
      objectType: ObjectTypeEnum.SIMPLE_VARIABLE,
      menu,
    })
  }

  createVariableScaleExplorerItem(variableScale: VariableScaleExplorerGetDto, context: { variableIri: string }): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.VARIABLE, // TODO ajouter le bon icône
      name: variableScale['@id'],
      iri: variableScale['@id'],
      label: variableScale.name,
      menu: [
        {
          title: 'navigation.menus.variableScale.delete',
          action: () => CHORUS.projectService.deleteVariableScale(variableScale['@id']),
        },
        {
          title: 'navigation.menus.variableScale.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_VARIABLE_SCALE,
            query: {variableIri: context.variableIri},
          }),
        },
      ],
    })
  }

  createStateCodeExplorerItem(stateCode: StateCodeExplorerGetDto, context?: { explorerType?: ExplorerTypeEnum }): ExplorerItem {

    const item = new ExplorerItem({
      icon: IconEnum.STATE_CODE,
      name: stateCode['@id'],
      iri: stateCode['@id'],
      label: `${stateCode.title} (${stateCode.code})`,
      types: [ExplorerTypeEnum.STATE_CODE],
      objectType: ObjectTypeEnum.STATE_CODE,
      menu: [
        {
          title: 'navigation.menus.stateCode.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_STATE_CODE,
            query: {stateCodeIri: stateCode['@id']},
          }),
        },
      ],
    })
    if (!stateCode.permanent) {
      item.menu.push({
        title: 'navigation.menus.stateCode.delete',
        action: () => CHORUS.projectService.deleteStateCode(stateCode['@id']),
      })
    }
    if (!!context?.explorerType) {
      item.types = [context.explorerType]
    }
    return item
  }

  createValueListExplorerItem(valueList: ValueListExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.VALUE_LIST,
      name: valueList['@id'],
      iri: valueList['@id'],
      label: valueList.name,
      types: [ExplorerTypeEnum.VALUE_LIST],
      objectType: ObjectTypeEnum.VALUE_LIST,
      menu: [
        {
          title: 'navigation.menus.valueList.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_VALUE_LIST,
            query: {valueListIri: valueList['@id']},
          }),
        },
        {
          title: 'navigation.menus.valueList.delete',
          action: () => CHORUS.projectService.deleteValueList(valueList['@id']),
        },
      ],
    })
  }

  createDeviceExplorerItem(device: DeviceExplorerGetDto): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.DEVICE,
      name: device['@id'],
      iri: device['@id'],
      label: device.alias,
      types: [ExplorerTypeEnum.DEVICE],
      objectType: ObjectTypeEnum.DEVICE,
      menu: [
        {
          title: 'navigation.menus.device.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_DEVICE,
            query: {deviceIri: device['@id']},
          }),
        },
        {
          title: 'navigation.menus.device.delete',
          action: () => CHORUS.projectService.deleteDevice(device['@id']),
        },
      ],
    })
  }

  createSemiAutomaticVariableExplorerItem(variable: SemiAutomaticVariableExplorerGetDto,
                                          context: { deviceName: string, explorerSuffix?: string, noMenu?: boolean })
    : ExplorerItem {

    const explorer = new ExplorerItem({
      icon: IconEnum.SEMI_AUTOMATIC_VARIABLE,
      name: variable['@id'] + (context.explorerSuffix ?? ''),
      iri: variable['@id'],
      label: variable.name + ` (${context.deviceName})`,
      types: [ExplorerTypeEnum.SEMI_AUTOMATIC_VARIABLE],
      objectType: ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE,
      menu: [],
    })
    if (!context.noMenu) {
      explorer.menu = [
        {
          title: 'navigation.menus.variable.edit',
          action: router => router.push({
            name: ROUTE_EDITOR_VARIABLE_SEMI_AUTOMATIC,
            query: {variableIri: variable['@id']},
          }),
        },
        this.createVariablesExportMenu(),
      ]
    }
    return explorer
  }

  createProjectBaseExplorerItem(project: ProjectExplorerGetDto): ExplorerItem {

    const explorer = new ExplorerItem({
      icon: !!project.transferDate ? IconEnum.PROJECT_TRANSFER : IconEnum.PROJECT,
      name: project['@id'],
      iri: project['@id'],
      label: project.name,
      objectType: ObjectTypeEnum.DATA_ENTRY_PROJECT,
      menu: [
        {
          title: 'navigation.menus.dataEntryProject.transfer',
          action: router => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_TRANSFER_TO_MOBILE,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.dataEntryProject.benchmark',
          action: () => CHORUS.projectService.transferProjectToMobile(project['@id'], [])
            .then(webappProject => {
              CHORUS.statusDataEntryProvider.getAll({
                'webappProject': webappProject.iri,
                'request.benchmark': true,
                pagination: false,
                groups: ['status_project_webapp_view', 'id_read'],
              })
                .then(statusList => {
                  const status = statusList.result.find(r => r.projectIri === webappProject.iri)
                  if (status) {
                    window.open(`${APP_URL}#/benchmark?target=${status.iri}`, '_blank')
                      .focus()
                  }
                })
            }),
        },
        {
          title: 'navigation.menus.common.manageRight',
          action: (router) => router.push({
            name: ROUTE_EDITOR_ADVANCED_RIGHTS, query: {
              objectId: project['@id'].match(
                new RegExp(`${CHORUS.httpService.getEndpointFromType(ObjectTypeEnum.DATA_ENTRY_PROJECT)}/([\\d]+)$`),
              )[1],
              objectType: AdvancedRightClassIdentifierEnum.PROJECT,
              owner: project.owner,
            },
          }),
        },
        {
          title: 'navigation.menus.dataEntryProject.duplicate',
          action: router => router.push({
            name: ROUTE_EDITOR_DUPLICATE_PROJECT,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.dataEntryProject.synthesis',
          action: (router) => router.push({
            name: ROUTE_PDF_GENERATOR,
            query: {
              pdfType: ObjectTypeEnum.DATA_ENTRY_PROJECT,
              objectIri: project['@id'],
            },
          }),
        },
      ],
    })
    if (!project.transferDate && project.projectDatas.length === 0) {
      explorer.menu.unshift(
        {
          title: 'navigation.menus.dataEntryProject.delete',
          action: () => CHORUS.projectService.deleteDataEntryProject(project['@id']),
        },
      )
    }
    return explorer
  }

  createProjectPathExplorerItem(project: ProjectExplorerGetDto, context: { withAction: boolean } = {withAction: true}) {

    const explorer = new ExplorerItem({
      icon: IconEnum.PATH_LIBRARY,
      name: `${project['@id']}:path`,
      label: 'navigation.explorer.dataEntryProject.path',
      menu: [],
      types: [ExplorerTypeEnum.PROJECT_PATH_LIBRARY],
    })
    if (context.withAction) {
      explorer.menu.push(
        {
          title: 'navigation.menus.path.configurePathFilter',
          action: (router) => router.push({
            name: ROUTE_EDITOR_PATH_CONFIGURATION,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.path.import',
          action: router => router.push({
            name: ROUTE_EDITOR_PATH_BASE_CSV_IMPORT,
            query: {projectIri: project['@id']},
          }),
        },
      )
    }
    return explorer
  }

  createProjectVariablesExplorerItem(project: ProjectExplorerGetDto) {

    return new ExplorerItem({
      icon: IconEnum.VARIABLE_LIBRARY,
      name: `${project['@id']}:variable`,
      label: 'navigation.explorer.dataEntryProject.variable',
      types: [ExplorerTypeEnum.PROJECT_VARIABLE_LIBRARY],
      menu: [
        {
          title: 'navigation.menus.variable.createIndependent',
          action: (router) => router.push({
            name: ROUTE_EDITOR_VARIABLE,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.createGenerator',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GENERATOR_VARIABLE,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.createAutomatic',
          action: (router) => router.push({
            name: ROUTE_EDITOR_DEVICE,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.linkFromLibrary',
          action: (router) => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_LINK_VARIABLE_TO_PROJECT,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.orderVariables',
          action: (router) => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_ORDER_VARIABLE,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.linkToPrevious',
          action: (router) => router.push({
            name: ROUTE_EDITOR_LINKED_VARIABLE,
            query: {projectIri: project['@id']},
          }),
        },
        {
          title: 'navigation.menus.variable.importOpenSilex',
          action: (router) => router.push({
            name: ROUTE_EDITOR_IMPORT_VARIABLE_OPENSILEX,
            query: {projectIri: project['@id']},
          }),
        },
        this.createVariablesExportMenu(),
      ],
    })
  }

  createProjectStateCodeExplorerItem(project: ProjectExplorerGetDto) {

    return new ExplorerItem({
      icon: IconEnum.STATE_CODE_LIBRARY,
      name: `${project['@id']}:stateCode`,
      label: 'navigation.explorer.dataEntryProject.stateCode',
      types: [ExplorerTypeEnum.PROJECT_STATE_CODE_LIBRARY],
      menu: [
        {
          title: 'navigation.menus.stateCode.linkFromLibrary',
          action: (router) => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_LINK_STATE_CODE_TO_PROJECT,
            query: {projectIri: project['@id']},
          }),
        },
      ],
    })
  }

  createProjectAnnotationExplorerItem(project: ProjectExplorerGetDto) {

    return new ExplorerItem({
      icon: IconEnum.ANNOTATION_LIBRARY,
      name: `${project['@id']}:annotation`,
      label: 'navigation.explorer.dataEntryProject.annotation',
      menu: [
        {
          title: 'navigation.menus.requiredAnnotation.create',
          action: (router) => router.push({
            name: ROUTE_EDITOR_REQUIRED_ANNOTATION,
            query: {projectIri: project['@id']},
          }),
        },
      ],
    })
  }

  createProjectExperimentExplorerItem(project: ProjectExplorerGetDto) {

    return new ExplorerItem({
      icon: IconEnum.EXPERIMENT_LIBRARY,
      name: `${project['@id']}:experiment`,
      label: 'navigation.explorer.dataEntryProject.experiments',
    })
  }

  createExperimentExplorerItem(experiment: ExperimentExplorerGetDto, context: { projectIri: string }): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.EXPERIMENT,
      name: context.projectIri + experiment['@id'],
      iri: experiment['@id'],
      label: experiment.name,
      objectType: ObjectTypeEnum.EXPERIMENT,
    })
  }

  createPlatformExplorerItem(platform: { iri: string, name: string }): ExplorerItem {

    return new ExplorerItem({
      icon: IconEnum.PLATFORM,
      name: platform.iri,
      iri: platform.iri,
      label: platform.name,
      types: [ExplorerTypeEnum.PLATFORM],
      objectType: ObjectTypeEnum.PLATFORM,
    })
  }

  createRequiredAnnotationExplorerItem(requiredAnnotation: RequiredAnnotationExplorerGetDto) {

    return new ExplorerItem({
      icon: IconEnum.ANNOTATION,
      name: requiredAnnotation['@id'],
      iri: requiredAnnotation['@id'],
      labelTab: ['enums.levels', ' [', 'enums.annotation.kinds', ']'],
      labelOption: [{level: requiredAnnotation.level}, null, {kind: requiredAnnotation.type}, null],
      menu: [
        {
          title: 'navigation.menus.requiredAnnotation.delete',
          action: () => CHORUS.projectService.deleteRequiredAnnotation(requiredAnnotation['@id']),
        },
        {
          title: 'navigation.menus.requiredAnnotation.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_REQUIRED_ANNOTATION,
            query: {requiredAnnotationIri: requiredAnnotation['@id']},
          }),
        },
      ],
    })
  }

  createPathExplorerItem(path: PathBaseExplorerGetDto, context: { projectIri: string, platformIri: string }) {

    return new ExplorerItem({
      icon: IconEnum.PATH,
      name: path['@id'],
      iri: path['@id'],
      label: path.name,
      types: [ExplorerTypeEnum.PATH_BASE],
      menu: [
        {
          title: 'navigation.menus.path.createUserPath',
          action: (router) => router.push({name: ROUTE_EDITOR_USER_PATH, query: {projectIri: context.projectIri}}),
        },
        {
          title: 'navigation.menus.path.edit',
          action: (router) => router.push({name: ROUTE_EDITOR_PATH_CONFIGURATION, query: {projectIri: context.projectIri}}),
        },
        {
          title: 'navigation.menus.path.delete',
          action: () => CHORUS.projectService.deletePath(path['@id'], context.projectIri),
        },
        {
          title: 'navigation.menus.common.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GRAPHIC,
            query: {objectIri: context.platformIri, mode: GraphicalEditorModeEnum.VIEW_PATH, pathBaseIri: path['@id']},
          }),
        },
      ],
    })
  }

  createUserPathExplorerItem(userPath: PathUserWorkflowExplorerGetDto,
                             context: { pathBaseIri: string, projectIri: string, platformIri: string }) {

    return new ExplorerItem({
      icon: IconEnum.USER_PATH,
      name: userPath['@id'],
      iri: userPath['@id'],
      label: userPath.username,
      menu: [
        {
          title: 'navigation.menus.userPath.duplicate',
          action: (router) => router.push({
            name: ROUTE_EDITOR_USER_PATH,
            query: {
              pathBaseIri: context.pathBaseIri,
              projectIri: context.projectIri,
              userPathIri: userPath['@id'],
              duplicate: 'true',
            },
          }),
        },
        {
          title: 'navigation.menus.userPath.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_USER_PATH,
            query: {pathBaseIri: context.pathBaseIri, projectIri: context.projectIri, userPathIri: userPath['@id']},
          }),
        },
        {
          title: 'navigation.menus.common.visualize',
          action: (router) => router.push({
            name: ROUTE_EDITOR_GRAPHIC,
            query: {objectIri: context.platformIri, mode: GraphicalEditorModeEnum.VIEW_PATH, userPathIri: userPath['@id']},
          }),
        },
        {
          title: 'navigation.menus.userPath.delete',
          action: () => CHORUS.projectService.deleteUserPath(userPath['@id'], context.projectIri),
        },
      ],
    })
  }

  createLinkedVariableExplorerItem(connectedVariableDto: ConnectedVariableExplorerGetDto) {

    return new ExplorerItem({
      icon: !!connectedVariableDto.dataEntrySimpleVariable ? IconEnum.VARIABLE :
        !!connectedVariableDto.dataEntryGeneratorVariable ? IconEnum.GENERATOR_VARIABLE : IconEnum.SEMI_AUTOMATIC_VARIABLE,
      name: connectedVariableDto['@id'],
      iri: connectedVariableDto['@id'],
      label: (
        connectedVariableDto.dataEntrySimpleVariable?.name ??
        connectedVariableDto.dataEntryGeneratorVariable?.name ??
        connectedVariableDto.dataEntrySemiAutomaticVariable?.name
      ) + ('C'),
      menu: [
        {
          title: 'navigation.menus.connectedVariable.delete',
          action: () => CHORUS.projectService.deleteConnectedVariable(connectedVariableDto['@id']),
        },
      ],
    })
  }

  createTestExplorerItem(test: TestExplorerGetDto, context: { explorerName: string }) {

    return new ExplorerItem({
      icon: IconEnum.TEST,
      name: test['@id'],
      iri: test['@id'],
      label: context.explorerName,
      menu: [
        {
          title: 'navigation.menus.test.edit',
          action: (router) => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
            query: {testIri: test['@id']},
          }),
        },
        {
          title: 'navigation.menus.test.delete',
          action: () => CHORUS.projectService.deleteTest(test['@id']),
        },
      ],
    })
  }
}
