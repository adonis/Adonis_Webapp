import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS } from '@adonis/webapp/router/routes-names'
import { ProjectExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'

export default class TransferExplorerFactory {

    createRunningProjectExplorerItem(project: ProjectExplorerGetDto): ExplorerItem {

        return new ExplorerItem({
            icon: IconEnum.PROJECT_TRANSFER,
            name: project['@id'],
            iri: project['@id'],
            label: project.name,
            objectType: ObjectTypeEnum.DATA_ENTRY_PROJECT,
            menu: [
                {
                    title: 'navigation.menus.transfer.statusProject',
                    action: router => router.push({
                        name: ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS,
                        query: {projectIri: project['@id']},
                    }),
                },
            ],
        })
    }
}
