import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { ROUTE_EDITOR_OPEN_SILEX_INSTANCE, ROUTE_EDITOR_USER_GROUP } from '@adonis/webapp/router/routes-names'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import {
    OpenSilexInstanceExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProjectExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'
import { UserGroupExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/user-group-explorer-get-dto'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'

export default class AdminExplorerFactory {

    createUserGroupExplorerItem(userGroup: UserGroupExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.GROUP,
            name: userGroup['@id'],
            iri: userGroup['@id'],
            label: userGroup.name,
            menu: [
                {
                    title: 'navigation.menus.userGroups.edit',
                    action: (router) => router.push({name: ROUTE_EDITOR_USER_GROUP, query: {userGroupIri: userGroup['@id']}}),
                },
                {
                    title: 'navigation.menus.userGroups.delete',
                    action: () => CHORUS.adminService.deleteUserGroup(userGroup['@id']),
                },
            ],
        })
    }

    createOpenSilexInstanceExplorerItem(openSilexInstance: OpenSilexInstanceExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.PARAMETER,
            name: openSilexInstance['@id'],
            iri: openSilexInstance['@id'],
            label: openSilexInstance.url,
            menu: [
                {
                    title: 'navigation.menus.openSilexInstance.edit',
                    action: (router) => router.push({name: ROUTE_EDITOR_OPEN_SILEX_INSTANCE, query: {openSilexInstanceIri: openSilexInstance['@id']}}),
                },
                {
                    title: 'navigation.menus.openSilexInstance.delete',
                    action: () => CHORUS.adminService.deleteOpenSilexInstance(openSilexInstance['@id']),
                },
            ],
        })
    }

    createExperimentExplorerItem(experiment: ExperimentExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.EXPERIMENT,
            name: experiment['@id'],
            iri: experiment['@id'],
            label: experiment.name,
            objectType: ObjectTypeEnum.EXPERIMENT,
            deleted: true,
            menu: [
                {
                    title: 'navigation.menus.admin.disabledObject.delete',
                    action: () => CHORUS.adminService.deleteDisabledObject(experiment['@id'], ObjectTypeEnum.EXPERIMENT),
                },
                {
                    title: 'navigation.menus.admin.disabledObject.restore',
                    action: () => CHORUS.adminService.restoreDisabledObject(experiment['@id'], ObjectTypeEnum.EXPERIMENT),
                },
            ],
        })

    }

    createPlatformExplorerItem(platform: PlatformExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.PLATFORM,
            name: platform['@id'],
            iri: platform['@id'],
            label: platform.name,
            objectType: ObjectTypeEnum.PLATFORM,
            deleted: true,
            menu: [
                {
                    title: 'navigation.menus.admin.disabledObject.delete',
                    action: () => CHORUS.adminService.deleteDisabledObject(platform['@id'], ObjectTypeEnum.PLATFORM),
                },
                {
                    title: 'navigation.menus.admin.disabledObject.restore',
                    action: () => CHORUS.adminService.restoreDisabledObject(platform['@id'], ObjectTypeEnum.PLATFORM),
                },
            ],
        })

    }

    createProjectExplorerItem(project: ProjectExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.PROJECT,
            name: project['@id'],
            iri: project['@id'],
            label: project.name,
            objectType: ObjectTypeEnum.DATA_ENTRY_PROJECT,
            deleted: true,
            menu: [
                {
                    title: 'navigation.menus.admin.disabledObject.delete',
                    action: () => CHORUS.adminService.deleteDisabledObject(project['@id'], ObjectTypeEnum.DATA_ENTRY_PROJECT),
                },
                {
                    title: 'navigation.menus.admin.disabledObject.restore',
                    action: () => CHORUS.adminService.restoreDisabledObject(project['@id'], ObjectTypeEnum.DATA_ENTRY_PROJECT),
                },
            ],
        })
    }

    createProtocolExplorerItem(protocol: ProtocolExplorerGetDto): ExplorerItem {
        return new ExplorerItem({
            icon: IconEnum.PROTOCOL,
            name: protocol['@id'],
            iri: protocol['@id'],
            label: protocol.name,
            objectType: ObjectTypeEnum.PROTOCOL,
            deleted: true,
            menu: [
                {
                    title: 'navigation.menus.admin.disabledObject.delete',
                    action: () => CHORUS.adminService.deleteDisabledObject(protocol['@id'], ObjectTypeEnum.PROTOCOL),
                },
                {
                    title: 'navigation.menus.admin.disabledObject.restore',
                    action: () => CHORUS.adminService.restoreDisabledObject(protocol['@id'], ObjectTypeEnum.PROTOCOL),
                },
            ],
        })
    }
}
