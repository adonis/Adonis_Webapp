import IconEnum from '@adonis/shared/constants/icon-name-enum'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import TransformerService from '@adonis/webapp/services/transformers/transformer-service'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import { v4 } from 'uuid'
import { Store } from 'vuex'

export default abstract class AbstractModuleExplorerProvider {

  constructor(
      protected httpService: WebappHttpService,
      protected transformerService: TransformerService,
      protected store: Store<any>,
  ) {
  }

  abstract getBaseExplorers(): ExplorerItem[]

  abstract initObjectExplorers(): Promise<any>

  constructAwaitingExplorerItem( icon: IconEnum ) {
    return new ExplorerItem( {
      name: v4(),
      label: '...',
      icon,
      menu: [],
      loading: true,
    } )
  }
}
