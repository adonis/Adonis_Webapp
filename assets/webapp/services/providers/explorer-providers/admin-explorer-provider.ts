import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import {
    ROUTE_EDITOR_MODULE_ADMIN_SITE_CREATE,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_DUPLICATE,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_UPDATE, ROUTE_EDITOR_OPEN_SILEX_INSTANCE,
    ROUTE_EDITOR_USER_GROUP,
} from '@adonis/webapp/router/routes-names'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import {
    OpenSilexInstanceExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProjectExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'
import { SiteAdminExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/site-admin-explorer-get-dto'
import { UserGroupExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/user-group-explorer-get-dto'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import AdminExplorerFactory from '@adonis/webapp/services/providers/explorer-providers/explorer-factories/admin-explorer-factory'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'

export const SITES_EXPLORER_NAME = 'sites'
export const USERS_EXPLORER_NAME = 'users'
export const USER_GROUPS_EXPLORER_NAME = 'user-group'
export const PARAMETERS_EXPLORER_NAME = 'parameters'
export const OPEN_SILEX_INSTANCE_EXPLORER_NAME = 'open-silex-instance'
export const DISABLED_EXPERIMENTS_EXPLORER_NAME = 'disabled-experiments'
export const DISABLED_PLATFORMS_EXPLORER_NAME = 'disabled-platforms'
export const DISABLED_PROJECTS_EXPLORER_NAME = 'disabled-projects'
export const DISABLED_PROTOCOLS_EXPLORER_NAME = 'disabled-protocols'

export default class AdminExplorerProvider extends AbstractModuleExplorerProvider {

    private adminExplorerFactory = new AdminExplorerFactory()

    getBaseExplorers(): ExplorerItem[] {

        return [
            new ExplorerItem({
                name: SITES_EXPLORER_NAME,
                label: 'navigation.explorer.site.label',
                icon: IconEnum.SITE,
                menu: [
                    {
                        title: 'navigation.menus.site.manage',
                        action: (router) => router.push({name: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE}),
                    },
                    {
                        title: 'navigation.menus.site.update',
                        action: (router) => router.push({
                            name: ROUTE_EDITOR_MODULE_ADMIN_SITE_UPDATE,
                            query: {siteIri: this.store.getters['navigation/getActiveRoleSite'].siteIri},
                        }),
                    },
                    {
                        title: 'navigation.menus.site.create',
                        action: (router) => router.push({name: ROUTE_EDITOR_MODULE_ADMIN_SITE_CREATE}),
                    },
                    {
                        title: 'navigation.menus.site.duplicate',
                        action: (router) => router.push({name: ROUTE_EDITOR_MODULE_ADMIN_SITE_DUPLICATE}),
                    },
                ],
            }),
            new ExplorerItem({
                name: USER_GROUPS_EXPLORER_NAME,
                label: 'navigation.explorer.userGroups.label',
                icon: IconEnum.GROUP_LIBRARY,
                menu: [
                    {
                        title: 'navigation.menus.userGroups.create',
                        action: (router) => router.push({name: ROUTE_EDITOR_USER_GROUP}),
                    },
                ],
            }),
            new ExplorerItem({
                name: OPEN_SILEX_INSTANCE_EXPLORER_NAME,
                label: 'navigation.explorer.openSilexInstance.label',
                icon: IconEnum.PARAMETER,
                menu: [
                    {
                        title: 'navigation.menus.openSilexInstance.create',
                        action: (router) => router.push({name: ROUTE_EDITOR_OPEN_SILEX_INSTANCE}),
                    },
                ],
            }),
            new ExplorerItem({
                name: 'disabled-objects',
                label: 'navigation.explorer.disabledObjects.label',
                icon: IconEnum.DISABLED_OBJECT_LIBRARY,
            })
                .addChild(new ExplorerItem({
                    name: DISABLED_PLATFORMS_EXPLORER_NAME,
                    label: 'navigation.explorer.disabledObjects.platforms',
                    icon: IconEnum.DISABLED_PLATFORM_LIBRARY,
                }))
                .addChild(new ExplorerItem({
                    name: DISABLED_EXPERIMENTS_EXPLORER_NAME,
                    label: 'navigation.explorer.disabledObjects.experiments',
                    icon: IconEnum.DISABLED_EXPERIMENT_LIBRARY,
                }))
                .addChild(new ExplorerItem({
                    name: DISABLED_PROJECTS_EXPLORER_NAME,
                    label: 'navigation.explorer.disabledObjects.projects',
                    icon: IconEnum.DISABLED_PROJECT_LIBRARY,
                }))
                .addChild(new ExplorerItem({
                    name: DISABLED_PROTOCOLS_EXPLORER_NAME,
                    label: 'navigation.explorer.disabledObjects.protocols',
                    icon: IconEnum.DISABLED_PROTOCOL_LIBRARY,
                })),
        ]
    }

    initObjectExplorers(): Promise<any> {

        return Promise.all([
            this.httpService.get<SiteAdminExplorerGetDto>(
                this.store.getters['navigation/getActiveRoleSite'].siteIri,
                {groups: ['admin_explorer_view']})
                .then(value => {
                        value.data.userGroups.forEach(userGroup => this.store.dispatch('navigation/add2explorer', {
                            item: this.constructUserGroupExplorerItem(userGroup),
                            attachment: USER_GROUPS_EXPLORER_NAME,
                        }))
                        value.data.openSilexInstances.forEach(openSilexInstance => this.store.dispatch('navigation/add2explorer', {
                            item: this.constructOpenSilexInstanceExplorerItem(openSilexInstance),
                            attachment: OPEN_SILEX_INSTANCE_EXPLORER_NAME,
                        }))
                    },
                ),
            this.httpService.getAll<ExperimentExplorerGetDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT),
                {
                    site: this.store.getters['navigation/getActiveRoleSite'].siteIri,
                    'exists[deletedAt]': true,
                    deleted: true,
                    groups: ['admin_explorer_view'],
                },
            )
                .then(value => value.data['hydra:member'].forEach(experiment =>
                    this.store.dispatch('navigation/add2explorer', {
                        item: this.constructDeletedExperimentExplorerItem(experiment),
                        attachment: DISABLED_EXPERIMENTS_EXPLORER_NAME,
                    }),
                )),
            this.httpService.getAll<PlatformExplorerGetDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM),
                {
                    site: this.store.getters['navigation/getActiveRoleSite'].siteIri,
                    'exists[deletedAt]': true,
                    deleted: true,
                    groups: ['admin_explorer_view'],
                },
            )
                .then(value => value.data['hydra:member'].forEach(platform =>
                    this.store.dispatch('navigation/add2explorer', {
                        item: this.constructDeletedPlatformExplorerItem(platform),
                        attachment: DISABLED_PLATFORMS_EXPLORER_NAME,
                    }),
                )),
            this.httpService.getAll<ProjectExplorerGetDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.DATA_ENTRY_PROJECT),
                {
                    'platform.site': this.store.getters['navigation/getActiveRoleSite'].siteIri,
                    'exists[deletedAt]': true,
                    deleted: true,
                    groups: ['admin_explorer_view'],
                },
            )
                .then(value => value.data['hydra:member'].forEach(project =>
                    this.store.dispatch('navigation/add2explorer', {
                        item: this.constructDeletedProjectExplorerItem(project),
                        attachment: DISABLED_PROJECTS_EXPLORER_NAME,
                    }),
                )),
            this.httpService.getAll<ProtocolExplorerGetDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.PROTOCOL),
                {
                    site: this.store.getters['navigation/getActiveRoleSite'].siteIri,
                    'exists[deletedAt]': true,
                    deleted: true,
                    groups: ['admin_explorer_view'],
                },
            )
                .then(value => value.data['hydra:member'].forEach(protocol =>
                    this.store.dispatch('navigation/add2explorer', {
                        item: this.constructDeletedProtocolExplorerItem(protocol),
                        attachment: DISABLED_PROTOCOLS_EXPLORER_NAME,
                    }),
                )),
        ])
    }

    constructUserGroupExplorerItem(userGroup: UserGroupExplorerGetDto) {

        return this.adminExplorerFactory.createUserGroupExplorerItem(userGroup)
    }

    constructOpenSilexInstanceExplorerItem(openSilexInstance: OpenSilexInstanceExplorerGetDto) {

        return this.adminExplorerFactory.createOpenSilexInstanceExplorerItem(openSilexInstance)
    }

    constructDeletedExperimentExplorerItem(experiment: ExperimentExplorerGetDto) {

        return this.adminExplorerFactory.createExperimentExplorerItem(experiment)
    }

    constructDeletedPlatformExplorerItem(platform: PlatformExplorerGetDto) {

        return this.adminExplorerFactory.createPlatformExplorerItem(platform)
    }

    constructDeletedProjectExplorerItem(project: ProjectExplorerGetDto) {

        return this.adminExplorerFactory.createProjectExplorerItem(project)
    }

    constructDeletedProtocolExplorerItem(protocol: ProtocolExplorerGetDto) {

        return this.adminExplorerFactory.createProtocolExplorerItem(protocol)
    }
}
