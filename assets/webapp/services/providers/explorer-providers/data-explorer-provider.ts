import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import { ROUTE_EDITOR_IMPORT_IMPROVISED_PROJECT, ROUTE_EDITOR_IMPORT_PROJECT_DATAS } from '@adonis/webapp/router/routes-names'
import { GeneratorVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProjectDataExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-data-explorer-get-dto'
import {
    SemiAutomaticVariableExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/semi-automatic-variable-explorer-get-dto'
import { SessionExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/session-explorer-get-dto'
import { SimpleVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import { SiteDataEntryExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/site-data-entry-explorer-get-dto'
import { ValueListExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import DataEntryExplorerFactory from '@adonis/webapp/services/providers/explorer-providers/explorer-factories/data-entry-explorer-factory'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'

export const DATA_ENTRY_LIBRARY_EXPLORER_NAME = 'data-entry'
export const RUNNING_PROJECT_LIBRARY_EXPLORER_NAME = 'data-entry-in-progress'

export default class DataExplorerProvider extends AbstractModuleExplorerProvider {

    dataExplorerFactory = new DataEntryExplorerFactory()

    getBaseExplorers(): ExplorerItem[] {

        return [
            new ExplorerItem({
                name: 'libraries',
                label: 'navigation.explorer.library.label',
                icon: IconEnum.LIBRARY,
                collapsed: false,
            }),
            new ExplorerItem({
                name: DATA_ENTRY_LIBRARY_EXPLORER_NAME,
                label: 'navigation.explorer.dataEntry.dataEntries.label',
                icon: IconEnum.PLATFORM_LIBRARY,
                collapsed: false,
                menu: [
                    {
                        title: 'navigation.menus.dataEntry.importImprovisedProject',
                        action: (router) => router.push({
                            name: ROUTE_EDITOR_IMPORT_IMPROVISED_PROJECT,
                        }),
                    },
                    {
                        title: 'navigation.menus.dataEntry.importProjectData',
                        action: (router) => router.push({
                            name: ROUTE_EDITOR_IMPORT_PROJECT_DATAS,
                        }),
                    },

                ],
            }),
        ]
    }

    initObjectExplorers(): Promise<any> {

        return this.httpService.get<SiteDataEntryExplorerGetDto>(
            this.store.getters['navigation/getActiveRoleSite'].siteIri,
            {groups: ['data_explorer_view']},
        )
            .then(value => value.data.platforms.forEach(platform => {
                if (platform.projects.some(projectDto => projectDto.projectDatas.length > 0)) {
                    this.store.dispatch('navigation/add2explorer', {
                        item: this.constructPlatformExplorerItem(platform, value.data.openSilexInstances?.length > 0),
                        attachment: DATA_ENTRY_LIBRARY_EXPLORER_NAME,
                    })
                }
            }))
    }

    constructDataEntryExplorerItem(dataEntry: ProjectDataExplorerGetDto, platformIri: string, hasOpenSilexInstance?: boolean): ExplorerItem {
        const dataEntryExplorer = this.dataExplorerFactory.createDataEntryExplorerItem(dataEntry, {hasOpenSilexInstance})
        dataEntry.sessions.forEach(session => {
            dataEntryExplorer.addChild(this.constructSessionExplorerItem(session))
        })

        dataEntry.simpleVariables.forEach(simpleVariable => {
            const variableExplorer = this.constructSimpleVariableExplorerItem(simpleVariable, platformIri)
            variableExplorer.order = simpleVariable.order
            dataEntryExplorer.addChild(variableExplorer)
        })

        dataEntry.generatorVariables.forEach(generatorVariableDto => {
            const variableExplorer = this.constructGeneratorVariableExplorerItem(generatorVariableDto, platformIri)
            variableExplorer.order = generatorVariableDto.order
            dataEntryExplorer.addChild(variableExplorer)
        })

        dataEntry.semiAutomaticVariables.forEach(device => {
            const variableExplorer = this.constructSemiAutomaticVariableExplorerItem(device, platformIri)
            variableExplorer.order = device.order
            dataEntryExplorer.addChild(variableExplorer)
        })
        return dataEntryExplorer
    }

    constructPlatformExplorerItem(platform: PlatformExplorerGetDto, hasOpenSilexInstance?: boolean): ExplorerItem {
        const item = this.dataExplorerFactory.createPlatformExplorerItem(platform)
        platform.projects.forEach(project => project.projectDatas.forEach(data =>
            item.addChild(this.constructDataEntryExplorerItem(data as ProjectDataExplorerGetDto, platform['@id'], hasOpenSilexInstance)),
        ))
        return item
    }

    constructSessionExplorerItem(session: SessionExplorerGetDto): ExplorerItem {

        return this.dataExplorerFactory.createSessionExplorerItem(session)
    }

    constructSemiAutomaticVariableExplorerItem(variable: SemiAutomaticVariableExplorerGetDto, platformIri: string,
                                               explorerSuffix?: string) {

        return this.dataExplorerFactory.createSemiAutomaticVariableExplorerItem(variable, {explorerSuffix, platformIri})
    }

    constructSimpleVariableExplorerItem(variable: SimpleVariableExplorerGetDto, platformIri: string)
        : ExplorerItem {

        const item = this.dataExplorerFactory.createVariableExplorerItem(variable, {platformIri})

        if (!!variable.valueList) {
            const valueListItem = this.constructValueListExplorerItem(variable.valueList)
            valueListItem.types = [ExplorerTypeEnum.NONE]
            item.addChild(valueListItem)
        }

        return item
    }

    constructValueListExplorerItem(valueList: ValueListExplorerGetDto): ExplorerItem {

        return this.dataExplorerFactory.createValueListExplorerItem(valueList)
    }

    constructGeneratorVariableExplorerItem(variable: GeneratorVariableExplorerGetDto, platformIri: string): ExplorerItem {

        const item = this.dataExplorerFactory.createGeneratorVariableExplorerItem(variable, {platformIri})
        variable.generatedSimpleVariables.forEach(genVar => item.addChild(this.constructSimpleVariableExplorerItem(genVar, platformIri)))
        variable.generatedGeneratorVariables.forEach(genVar => item.addChild(this.constructGeneratorVariableExplorerItem(genVar, platformIri)))
        return item
    }
}
