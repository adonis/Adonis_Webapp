import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import {
    ROUTE_EDITOR_EXPERIMENT,
    ROUTE_EDITOR_EXPERIMENT_CSV_IMPORT,
    ROUTE_EDITOR_FACTOR,
    ROUTE_EDITOR_GERMPLASM_FETCH,
    ROUTE_EDITOR_OEZ_NATURE,
    ROUTE_EDITOR_PLATFORM,
    ROUTE_EDITOR_PROTOCOL,
    ROUTE_EDITOR_PROTOCOL_CSV_IMPORT,
} from '@adonis/webapp/router/routes-names'
import { BlockExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/block-explorer-get-dto'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import { FactorExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/factor-explorer-get-dto'
import { IndividualExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/individual-explorer-get-dto'
import { OezNatureExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/oez-nature-explorer-get-dto'
import {
    OutExperimentationZoneExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/out-experimentation-zone-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'
import { SiteDesignExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/site-design-explorer-get-dto'
import { SubBlockExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/sub-block-explorer-get-dto'
import { SurfacicUnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/surfacic-unit-plot-explorer-get-dto'
import { TreatmentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/treatment-explorer-get-dto'
import { UnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/unit-plot-explorer-get-dto'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import DesignExplorerFactory from '@adonis/webapp/services/providers/explorer-providers/explorer-factories/design-explorer-factory'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import VueRouter from 'vue-router'

export const OEZ_NATURE_LIBRARY_EXPLORER_NAME = 'oezNatures'
export const FACTOR_LIBRARY_EXPLORER_NAME = 'factor'
export const PROTOCOL_LIBRARY_EXPLORER_NAME = 'protocol'
export const EXPERIMENT_LIBRARY_EXPLORER_NAME = 'unlinked-experiments'
export const DESIGN_LIST_OF_PLATFORM = 'design-list-of-platform'

export default class DesignExplorerProvider extends AbstractModuleExplorerProvider {

    private designExplorerFactory = new DesignExplorerFactory()

    getBaseExplorers(): ExplorerItem[] {

        return [
            new ExplorerItem({
                name: 'libraries',
                label: 'navigation.explorer.library.label',
                icon: IconEnum.LIBRARY,
                collapsed: false,
            })
                .addChild(new ExplorerItem({
                    name: FACTOR_LIBRARY_EXPLORER_NAME,
                    label: 'navigation.explorer.library.factors.label',
                    icon: IconEnum.FACTOR_LIBRARY,
                    menu: [
                        {
                            title: 'navigation.menus.factor.create',
                            action: (router) => router.push({name: ROUTE_EDITOR_FACTOR}),
                        },
                    ],
                    collapsed: true,
                }))
                .addChild(new ExplorerItem({
                    name: PROTOCOL_LIBRARY_EXPLORER_NAME,
                    label: 'navigation.explorer.library.protocols.label',
                    icon: IconEnum.PROTOCOL_LIBRARY,
                    menu: [
                        {
                            title: 'navigation.menus.protocol.create',
                            action: (router) => router.push({name: ROUTE_EDITOR_PROTOCOL}),
                        },
                        {
                            title: 'navigation.menus.protocol.csvImport',
                            action: (router) => router.push({
                                name: ROUTE_EDITOR_PROTOCOL_CSV_IMPORT,
                            }),
                        },
                    ],
                }))
                .addChild(new ExplorerItem({
                    name: EXPERIMENT_LIBRARY_EXPLORER_NAME,
                    label: 'navigation.explorer.library.unlinkedExperiments.label',
                    icon: IconEnum.EXPERIMENT_LIBRARY,
                    menu: [
                        {
                            title: 'navigation.menus.experiment.create',
                            action: (router) => router.push({
                                name: ROUTE_EDITOR_EXPERIMENT,
                            }),
                        },
                        {
                            title: 'navigation.menus.experiment.xmlImport',
                            action: () => CHORUS.designService.importExperimentXml(),
                        },
                        {
                            title: 'navigation.menus.experiment.csvImport',
                            action: (router) => router.push({
                                name: ROUTE_EDITOR_EXPERIMENT_CSV_IMPORT,
                            }),
                        },
                    ],
                }))
                .addChild(new ExplorerItem({
                    name: OEZ_NATURE_LIBRARY_EXPLORER_NAME,
                    label: 'navigation.explorer.library.oezNatures.label',
                    icon: IconEnum.OEZ_KIND_LIBRARY,
                    menu: [
                        {
                            title: 'navigation.menus.oezNature.create',
                            action: (router) => router.push({name: ROUTE_EDITOR_OEZ_NATURE}),
                        },
                    ],
                })),
            new ExplorerItem({
                name: DESIGN_LIST_OF_PLATFORM,
                label: 'navigation.explorer.platform.label',
                icon: IconEnum.PLATFORM_LIBRARY,
                collapsed: false,
                menu: [
                    {
                        title: 'navigation.menus.platform.create',
                        action: (router) => router.push({name: ROUTE_EDITOR_PLATFORM}),
                    },
                    {
                        title: 'navigation.menus.platform.xmlImport',
                        action: () => CHORUS.designService.importPlatformXml(),
                    },
                ],
            }),
        ]
    }

    initObjectExplorers(): Promise<any> {

        return this.httpService.get<SiteDesignExplorerGetDto>(
            this.store.getters['navigation/getActiveRoleSite'].siteIri,
            {groups: ['design_explorer_view']})
            .then(value => {
                value.data.factors.forEach(factor => {
                    const factorExplorer = this.constructFactorExplorerItem(factor)
                    this.store.dispatch('navigation/add2explorer', {item: factorExplorer, attachment: FACTOR_LIBRARY_EXPLORER_NAME})
                        .then()
                })
                value.data.protocols.forEach(protocol => {
                    const item = this.constructProtocolExplorerItem(protocol)
                    this.store.dispatch('navigation/add2explorer', {item, attachment: PROTOCOL_LIBRARY_EXPLORER_NAME})
                        .then()
                })
                value.data.experiments.forEach(experiment => {
                    const item = this.constructExperimentExplorerItem(experiment, false, false)
                    item.types = [ExplorerTypeEnum.UNLINKED_EXPERIMENT]
                    this.store.dispatch('navigation/add2explorer', {item, attachment: EXPERIMENT_LIBRARY_EXPLORER_NAME})
                        .then()
                })
                value.data.platforms.forEach(platform => {
                    const item = this.constructPlatformExplorerItem(platform, value.data.openSilexInstances.length > 0)
                    this.store.dispatch('navigation/add2explorer', {item, attachment: DESIGN_LIST_OF_PLATFORM})
                        .then()
                })
                value.data.oezNatures.forEach(oezNature => {
                    const item = this.constructOezNatureExplorerItem(oezNature)
                    this.store.dispatch('navigation/add2explorer', {item, attachment: OEZ_NATURE_LIBRARY_EXPLORER_NAME})
                        .then()
                })
                if (value.data.openSilexInstances?.length > 0) {
                    this.store.getters['navigation/getExplorerItem'](FACTOR_LIBRARY_EXPLORER_NAME).menu
                        ?.push(
                            {
                                title: 'navigation.menus.factor.fetchGermplasm',
                                action: (router: VueRouter) => router.push({name: ROUTE_EDITOR_GERMPLASM_FETCH}),
                            })
                }
            })
    }

    constructFactorExplorerItem(factor: FactorExplorerGetDto): ExplorerItem {

        const factorExplorer = this.designExplorerFactory.createFactorExplorerItem(factor)
        factor.modalities.forEach(modality => factorExplorer.addChild(this.designExplorerFactory.createModalityExplorerItem(modality)))
        return factorExplorer
    }

    constructTreatmentExplorerItem(treatment: TreatmentExplorerGetDto): ExplorerItem {

        const treatmentExplorer = this.designExplorerFactory.createTreatmentExplorerItem(treatment)
        treatment.modalities.forEach(modality => {
            treatmentExplorer.addChild(this.designExplorerFactory.createModalityExplorerItem(modality, {namePrefix: treatment.name}))
        })
        return treatmentExplorer
    }

    constructProtocolExplorerItem(protocol: ProtocolExplorerGetDto, linkedToExperiment = false): ExplorerItem {

        const item = this.designExplorerFactory.createProtocolExplorerItem(protocol, {linkedToExperiment})
        const treatments = this.designExplorerFactory.createProtocolTreatmentsExplorerItem(protocol, {
            collapsedCallback: () => this.httpService.getAll<TreatmentExplorerGetDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.TREATMENT),
                {groups: ['design_explorer_view'], protocol: protocol['@id'], pagination: false})
                .then(response =>
                    Promise.all(response.data['hydra:member'].map(treatment => this.store.dispatch('navigation/add2explorer', {
                        item: this.constructTreatmentExplorerItem(treatment),
                        attachment: treatments.name,
                    })))
                        .then(),
                ),
        })
        const factors = this.designExplorerFactory.createProtocolFactorExplorerItem(protocol)
        item.collapsed = true
        factors.collapsed = true
        treatments.collapsed = true
        item.addChild(factors)
            .addChild(treatments)
        protocol.factors.forEach(factor => {
            const factorExplorer = this.constructFactorExplorerItem(factor)
            factorExplorer.menu = []
            factorExplorer.collapsed = true
            factors.addChild(factorExplorer)
        })
        return item
    }

    constructPlatformExplorerItem(platform: PlatformExplorerGetDto, hasOpenSilexInstance?: boolean): ExplorerItem {

        const item = this.designExplorerFactory.createPlatformExplorerItem(platform)
        platform.experiments.forEach(experiment => {
            const son = this.constructExperimentExplorerItem(experiment, true, hasOpenSilexInstance)
            son.collapsed = true
            item.addChild(son)
        })
        return item
    }

    constructExperimentExplorerItem(experiment: ExperimentExplorerGetDto, linkedToPlatform: boolean, hasOpenSilexInstance?: boolean): ExplorerItem {
        const item = this.designExplorerFactory.createExperimentExplorerItem(experiment, {
            linkedToPlatform,
            hasOpenSilexInstance,
        })
        item.addChild(this.constructProtocolExplorerItem(experiment.protocol, true))
        return item
    }

    constructBlockExplorerItem(block: BlockExplorerGetDto): ExplorerItem {

        const item = this.designExplorerFactory.createBlockExplorerItem(block)
        return item
    }

    constructSubBlockExplorerItem(subBlock: SubBlockExplorerGetDto): ExplorerItem {

        const item = this.designExplorerFactory.createSubBlockExplorerItem(subBlock)

        subBlock.surfacicUnitPlots?.forEach(surfacicUnitPlot => item.addChild(this.constructSurfacicUnitPlotExplorerItem(surfacicUnitPlot)))
        subBlock.unitPlots?.forEach(unitPlot => item.addChild(this.constructUnitPlotExplorerItem(unitPlot)))
        return item
    }

    constructUnitPlotExplorerItem(unitPlot: UnitPlotExplorerGetDto): ExplorerItem {

        const item = this.designExplorerFactory.createUnitPlotExplorerItem(unitPlot)
        return item
    }

    constructSurfacicUnitPlotExplorerItem(surfacicUnitPlot: SurfacicUnitPlotExplorerGetDto): ExplorerItem {
        return this.designExplorerFactory.createSurfacicUnitPlotExplorerItem(surfacicUnitPlot)
    }

    constructIndividualExplorerItem(individual: IndividualExplorerGetDto): ExplorerItem {
        return this.designExplorerFactory.createIndividualExplorerItem(individual)
    }

    constructOezNatureExplorerItem(oezNature: OezNatureExplorerGetDto): ExplorerItem {
        return this.designExplorerFactory.createOezNatureExplorerItem(oezNature)
    }

    constructOezExplorerItem(oez: OutExperimentationZoneExplorerGetDto): ExplorerItem {
        return this.designExplorerFactory.createOezExplorerItem(oez)
    }
}
