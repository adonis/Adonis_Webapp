import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import {
  ROUTE_EDITOR_DATA_ENTRY_PROJECT,
  ROUTE_EDITOR_DEVICE,
  ROUTE_EDITOR_GENERATOR_VARIABLE,
  ROUTE_EDITOR_IMPORT_VARIABLE_OPENSILEX,
  ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
  ROUTE_EDITOR_STATE_CODE,
  ROUTE_EDITOR_VALUE_LIST,
  ROUTE_EDITOR_VARIABLE,
} from '@adonis/webapp/router/routes-names'
import {ConnectedVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/connected-variable-explorer-get-dto'
import {DeviceExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/device-explorer-get-dto'
import {GeneratorVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import {PathBaseExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/path-base-explorer-get-dto'
import {PathUserWorkflowExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/path-user-workflow-explorer-get-dto'
import {PlatformExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import {ProjectExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import {
  RequiredAnnotationExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/required-annotation-explorer-get-dto'
import {
  SemiAutomaticVariableExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/semi-automatic-variable-explorer-get-dto'
import {SimpleVariableExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import {SiteProjectExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/site-project-explorer-get-dto'
import {StateCodeExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/state-code-explorer-get-dto'
import {TestExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/test-explorer-get-dto'
import {ValueListExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'
import {VariableScaleExplorerGetDto} from '@adonis/webapp/services/http/entities/explorer-get-dto/variable-scale-explorer-get-dto'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import ProjectExplorerFactory from '@adonis/webapp/services/providers/explorer-providers/explorer-factories/project-explorer-factory'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import VueRouter from 'vue-router'

export const VARIABLE_LIBRARY_EXPLORER_NAME = 'variables'
export const SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME = 'auto_variables'
export const GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME = 'gen_variables'
export const SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME = 'ind_variables'
export const DEVICE_LIBRARY_EXPLORER_NAME = 'devices'
export const STATE_CODE_LIBRARY_EXPLORER_NAME = 'stateCodes'
export const VALUE_LIST_LIBRARY_EXPLORER_NAME = 'valueList'
export const DATA_ENTRY_PROJECT_LIBRARY_EXPLORER_NAME = 'dataEntryProject'

export default class ProjectExplorerProvider extends AbstractModuleExplorerProvider {

  private projectExplorerFactory = new ProjectExplorerFactory()

  getBaseExplorers(): ExplorerItem[] {

    return [
      new ExplorerItem({
        name: 'libraries',
        label: 'navigation.explorer.library.label',
        icon: IconEnum.LIBRARY,
        collapsed: false,
      })
        .addChild(new ExplorerItem({
          name: STATE_CODE_LIBRARY_EXPLORER_NAME,
          label: 'navigation.explorer.library.stateCodes.label',
          icon: IconEnum.STATE_CODE_LIBRARY,
          menu: [
            {
              title: 'navigation.menus.stateCode.create',
              action: (router) => router.push({name: ROUTE_EDITOR_STATE_CODE}),
            },
          ],
        }))
        .addChild(new ExplorerItem({
          name: VALUE_LIST_LIBRARY_EXPLORER_NAME,
          label: 'navigation.explorer.library.valueLists.label',
          icon: IconEnum.VALUE_LIST_LIBRARY,
          menu: [
            {
              title: 'navigation.menus.valueList.create',
              action: (router) => router.push({name: ROUTE_EDITOR_VALUE_LIST}),
            },
          ],
        }))
        .addChild(new ExplorerItem({
          name: DEVICE_LIBRARY_EXPLORER_NAME,
          label: 'navigation.explorer.library.devices.label',
          icon: IconEnum.DEVICE_LIBRARY,
          menu: [
            {
              title: 'navigation.menus.variable.createAutomatic',
              action: (router) => router.push({name: ROUTE_EDITOR_DEVICE}),
            },
          ],
        }))
        .addChild(new ExplorerItem({
          name: VARIABLE_LIBRARY_EXPLORER_NAME,
          label: 'navigation.explorer.library.variables.label',
          icon: IconEnum.VARIABLE_LIBRARY,
          menu: [
            {
              title: 'navigation.menus.variable.createIndependent',
              action: (router) => router.push({
                name: ROUTE_EDITOR_VARIABLE,
              }),
            },
            {
              title: 'navigation.menus.variable.createGenerator',
              action: (router) => router.push({
                name: ROUTE_EDITOR_GENERATOR_VARIABLE,
              }),
            },
            {
              title: 'navigation.menus.variable.createAutomatic',
              action: (router) => router.push({name: ROUTE_EDITOR_DEVICE}),
            },
            {
              title: 'navigation.menus.variable.import',
              action: () => CHORUS.projectService.importVariableXml(),
            },
            this.projectExplorerFactory.createVariablesExportMenu(),
          ],
        })
          .addChild(new ExplorerItem({
            name: SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
            label: 'navigation.explorer.library.variables.automatic.label',
            icon: IconEnum.VARIABLE_LIBRARY, // TODO changer l'icône
            menu: [
              {
                title: 'navigation.menus.variable.createAutomatic',
                action: (router) => router.push({
                  name: ROUTE_EDITOR_DEVICE,
                }),
              },
              this.projectExplorerFactory.createVariablesExportMenu(),
            ],
          }))
          .addChild(new ExplorerItem({
            name: SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME,
            label: 'navigation.explorer.library.variables.independent.label',
            icon: IconEnum.VARIABLE_LIBRARY, // TODO changer l'icône
            menu: [
              {
                title: 'navigation.menus.variable.createIndependent',
                action: (router) => router.push({
                  name: ROUTE_EDITOR_VARIABLE,
                }),
              },
              this.projectExplorerFactory.createVariablesExportMenu(),
            ],
          }))
          .addChild(new ExplorerItem({
            name: GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME,
            label: 'navigation.explorer.library.variables.generator.label',
            icon: IconEnum.VARIABLE_LIBRARY, // TODO changer l'icône
            menu: [
              {
                title: 'navigation.menus.variable.createGenerator',
                action: (router) => router.push({
                  name: ROUTE_EDITOR_GENERATOR_VARIABLE,
                }),
              },
              this.projectExplorerFactory.createVariablesExportMenu(),
            ],
          }))),
      new ExplorerItem({
        name: DATA_ENTRY_PROJECT_LIBRARY_EXPLORER_NAME,
        label: 'navigation.explorer.dataEntryProject.label',
        icon: IconEnum.PROJECT_LIBRARY,
        collapsed: false,
        menu: [
          {
            title: 'navigation.menus.dataEntryProject.create',
            action: (router) => router.push({name: ROUTE_EDITOR_DATA_ENTRY_PROJECT}),
          },
        ],
      }),
    ]
  }

  initObjectExplorers(): Promise<any> {

    return this.httpService.get<SiteProjectExplorerGetDto>(
      this.store.getters['navigation/getActiveRoleSite'].siteIri,
      {groups: ['project_explorer_view']},
    )
      .then(value => {
        value.data.simpleVariables.forEach(variable => {
          const variableExplorer = this.constructSimpleVariableExplorerItem(variable)
          variableExplorer.collapsed = true
          this.store.dispatch('navigation/add2explorer', {
            item: variableExplorer,
            attachment: SIMPLE_VARIABLE_LIBRARY_EXPLORER_NAME,
          }).then()
        })
        value.data.stateCodes.forEach(stateCode => {
          const stateCodeExplorerItem = this.constructStateCodeExplorerItem(stateCode)
          this.store.dispatch('navigation/add2explorer', {
            item: stateCodeExplorerItem,
            attachment: STATE_CODE_LIBRARY_EXPLORER_NAME,
          }).then()
        })
        value.data.valueLists.forEach(valueList => {
          const valueListExplorer = this.constructValueListExplorerItem(valueList)
          this.store.dispatch('navigation/add2explorer', {
            item: valueListExplorer,
            attachment: VALUE_LIST_LIBRARY_EXPLORER_NAME,
          }).then()
        })
        value.data.generatorVariables.forEach(variable => {
          const variableExplorer = this.constructGeneratorVariableExplorerItem(variable)
          variableExplorer.collapsed = true
          this.store.dispatch('navigation/add2explorer', {
            item: variableExplorer,
            attachment: GENERATOR_VARIABLE_LIBRARY_EXPLORER_NAME,
          }).then()
        })
        value.data.devices.forEach(device => {
          device.managedVariables.forEach(variable => this.store.dispatch('navigation/add2explorer', {
            item: this.constructSemiAutomaticVariableExplorerItem(variable, device.alias),
            attachment: SEMI_AUTOMATIC_VARIABLE_LIBRARY_EXPLORER_NAME,
          }))
          const variableExplorer = this.constructDeviceExplorerItem(device)
          variableExplorer.collapsed = true
          this.store.dispatch('navigation/add2explorer', {
            item: variableExplorer,
            attachment: DEVICE_LIBRARY_EXPLORER_NAME,
          }).then()
        })
        value.data.platforms.forEach(platform => {
          if (platform.projects.length > 0) {
            this.store.dispatch('navigation/add2explorer', {
              item: this.constructPlatformExplorerItem(platform),
              attachment: DATA_ENTRY_PROJECT_LIBRARY_EXPLORER_NAME,
            })
          }
        })

        if (value.data.openSilexInstances?.length > 0) {
          this.store.getters['navigation/getExplorerItem'](VARIABLE_LIBRARY_EXPLORER_NAME).menu
            ?.push(
              {
                title: 'navigation.menus.variable.importOpenSilex',
                action: (router: VueRouter) => router.push({
                  name: ROUTE_EDITOR_IMPORT_VARIABLE_OPENSILEX,
                }),
              })
        }
      })
  }

  constructSimpleVariableExplorerItem(variable: SimpleVariableExplorerGetDto,
                                      inProject = false,
                                      explorerType?: ExplorerTypeEnum)
    : ExplorerItem {

    const item = this.projectExplorerFactory.createVariableExplorerItem(variable, {withTest: inProject, explorerType})
    if (!!variable.scale) {
      item.addChild(this.constructVariableScaleExplorerItem(variable.scale, variable['@id']))
    }
    if (!!variable.valueList) {
      const valueListItem = this.constructValueListExplorerItem(variable.valueList)
      valueListItem.types = [ExplorerTypeEnum.NONE]
      item.addChild(valueListItem)
    }

    variable.tests.forEach(test => item.addChild(this.constructTestExplorerItem(test)))
    variable.connectedVariables.forEach(connectedVariable => item.addChild(this.constructConnectedVariableExplorerItem(connectedVariable)))
    item.order = inProject ? (variable.order ?? variable.id) : undefined

    return item
  }

  constructVariableScaleExplorerItem(variableScale: VariableScaleExplorerGetDto, variableIri: string): ExplorerItem {

    return this.projectExplorerFactory.createVariableScaleExplorerItem(variableScale, {variableIri})
  }

  constructGeneratorVariableExplorerItem(variable: GeneratorVariableExplorerGetDto, inProject = false): ExplorerItem {

    const item = this.projectExplorerFactory.createGeneratorVariableExplorerItem(variable)
    variable.generatedSimpleVariables.forEach(genVar => item.addChild(this.constructSimpleVariableExplorerItem(genVar)))
    variable.generatedGeneratorVariables.forEach(genVar => item.addChild(this.constructGeneratorVariableExplorerItem(genVar)))
    variable.tests.forEach(test => item.addChild(this.constructTestExplorerItem(test)))
    variable.connectedVariables.forEach(connectedVariable => item.addChild(this.constructConnectedVariableExplorerItem(connectedVariable)))
    item.order = inProject ? (variable.order ?? variable.id) : undefined
    return item
  }

  constructValueListExplorerItem(valueList: ValueListExplorerGetDto): ExplorerItem {

    return this.projectExplorerFactory.createValueListExplorerItem(valueList)
  }

  constructRequiredAnnotationExplorerItem(requiredAnnotation: RequiredAnnotationExplorerGetDto): ExplorerItem {

    return this.projectExplorerFactory.createRequiredAnnotationExplorerItem(requiredAnnotation)
  }

  constructSemiAutomaticVariableExplorerItem(variable: SemiAutomaticVariableExplorerGetDto,
                                             deviceName: string,
                                             explorerSuffix?: string,
                                             noMenu?: boolean) {

    const item = this.projectExplorerFactory.createSemiAutomaticVariableExplorerItem(variable, {deviceName, explorerSuffix, noMenu})
    variable.tests.forEach(test => item.addChild(this.constructTestExplorerItem(test)))
    variable.connectedVariables.forEach(connectedVariable => item.addChild(this.constructConnectedVariableExplorerItem(connectedVariable)))

    return item
  }

  constructDeviceExplorerItem(device: DeviceExplorerGetDto): ExplorerItem {

    const item = this.projectExplorerFactory.createDeviceExplorerItem(device)
    device.managedVariables.forEach(variable => {
      item.addChild(this.constructSemiAutomaticVariableExplorerItem(variable, device.alias, 'device', true))
    })
    return item
  }

  constructProjectExplorerItem(project: ProjectExplorerGetDto, platformIri: string): ExplorerItem {

    const item = this.projectExplorerFactory.createProjectBaseExplorerItem(project)
    const hasNoBaseUserPath = !project.pathBase?.userPaths || (project.pathBase?.userPaths.length === 0 ?? true)
    const pathContainer = this.projectExplorerFactory.createProjectPathExplorerItem(project, {withAction: hasNoBaseUserPath})
    item.addChild(pathContainer)
    const variableContainer = this.projectExplorerFactory.createProjectVariablesExplorerItem(project)
    item.addChild(variableContainer)
    const stateCodeContainer = this.projectExplorerFactory.createProjectStateCodeExplorerItem(project)
    item.addChild(stateCodeContainer)
    const annotationContainer = this.projectExplorerFactory.createProjectAnnotationExplorerItem(project)
    item.addChild(annotationContainer)
    const experimentContainer = this.projectExplorerFactory.createProjectExperimentExplorerItem(project)
    item.addChild(experimentContainer)
    project.experiments.forEach(experiment => experimentContainer.addChild(
      this.projectExplorerFactory.createExperimentExplorerItem(experiment, {projectIri: project['@id']}),
    ))
    project.stateCodes.forEach(stateCodeDto => stateCodeContainer.addChild(
      this.constructStateCodeExplorerItem(stateCodeDto, ExplorerTypeEnum.NONE),
    ))
    project.requiredAnnotations.forEach(annotation => {
      annotationContainer.addChild(this.constructRequiredAnnotationExplorerItem(annotation))
    })
    project.simpleVariables.forEach(simpleVariable => {
      const variableExplorer = this.constructSimpleVariableExplorerItem(simpleVariable, true, ExplorerTypeEnum.NONE)
      variableContainer.addChild(variableExplorer)
    })
    project.generatorVariables.forEach(generatorVariableDto => {
      const variableExplorer = this.constructGeneratorVariableExplorerItem(generatorVariableDto, true)
      variableExplorer.types = [ExplorerTypeEnum.NONE]
      variableExplorer.menu.push(
        {
          title: 'navigation.menus.test.create',
          action: (router) => router.push({
            name: ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
            query: {variableIri: generatorVariableDto['@id']},
          }),
        })
      variableContainer.addChild(variableExplorer)
    })
    project.devices.forEach(device => {
      const deviceExplorer = this.constructDeviceExplorerItem(device)
      deviceExplorer.types = [ExplorerTypeEnum.NONE]
      deviceExplorer.children.forEach(child => {
        child.menu = []
        child.types = [ExplorerTypeEnum.NONE]
      })
      variableContainer.addChild(deviceExplorer)
    })
    if (project.pathBase) {
      const pathBase = this.constructPathBaseExplorerItem(project.pathBase, project['@id'], platformIri)
      pathContainer.addChild(pathBase)
    }
    item.collapsed = true
    return item
  }

  constructPlatformExplorerItem(platform: PlatformExplorerGetDto): ExplorerItem {

    const item = this.projectExplorerFactory.createPlatformExplorerItem({
      iri: platform['@id'],
      name: platform.name,
    })
    platform.projects.forEach(project => item.addChild(this.constructProjectExplorerItem(project, platform['@id'])))
    return item
  }

  constructStateCodeExplorerItem(stateCode: StateCodeExplorerGetDto, explorerType?: ExplorerTypeEnum): ExplorerItem {

    return this.projectExplorerFactory.createStateCodeExplorerItem(stateCode, {explorerType})
  }

  constructTestExplorerItem(test: TestExplorerGetDto) {

    let explorerName = ''
    switch (test.type) {
      case TestTypeEnum.INTERVAL:
        explorerName = 'enums.objectTypes.testInterval'
        break
      case TestTypeEnum.GROWTH:
        explorerName = 'enums.objectTypes.testGrowth'
        break
      case TestTypeEnum.COMBINATION:
        explorerName = 'enums.objectTypes.testCombination'
        break
      case TestTypeEnum.PRECONDITIONED:
        explorerName = 'enums.objectTypes.testPreconditioned'
        break
    }
    return this.projectExplorerFactory.createTestExplorerItem(test, {explorerName})
  }

  constructPathBaseExplorerItem(pathBase: PathBaseExplorerGetDto, projectIri: string, platformIri: string): ExplorerItem {
    const explorerItem = this.projectExplorerFactory.createPathExplorerItem(pathBase, {projectIri, platformIri})

    pathBase.userPaths?.forEach(userPath => explorerItem.addChild(this.constructUserPathExplorerItem(userPath, userPath['@id'], projectIri, platformIri)))

    return explorerItem
  }

  constructUserPathExplorerItem(userPath: PathUserWorkflowExplorerGetDto, pathBaseIri: string, projectIri: string, platformIri: string): ExplorerItem {

    return this.projectExplorerFactory.createUserPathExplorerItem(userPath, {pathBaseIri, projectIri, platformIri})
  }

  constructConnectedVariableExplorerItem(connectedVariable: ConnectedVariableExplorerGetDto): ExplorerItem {

    return this.projectExplorerFactory.createLinkedVariableExplorerItem(connectedVariable)
  }
}
