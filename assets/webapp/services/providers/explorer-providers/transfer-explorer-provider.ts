import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { ROUTE_EDITOR_IMPORT_REQUEST_FILE, ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS } from '@adonis/webapp/router/routes-names'
import { ProjectExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'
import AbstractModuleExplorerProvider from '@adonis/webapp/services/providers/explorer-providers/abstract-module-explorer-provider'
import { RUNNING_PROJECT_LIBRARY_EXPLORER_NAME } from '@adonis/webapp/services/providers/explorer-providers/data-explorer-provider'
import TransferExplorerFactory from '@adonis/webapp/services/providers/explorer-providers/explorer-factories/transfer-explorer-factory'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'

export default class TransferExplorerProvider extends AbstractModuleExplorerProvider {

    private dataEntryExplorerFactory = new TransferExplorerFactory()

    getBaseExplorers(): ExplorerItem[] {

        return [
            new ExplorerItem({
                name: RUNNING_PROJECT_LIBRARY_EXPLORER_NAME,
                label: 'navigation.explorer.dataEntry.inProgress.label',
                icon: IconEnum.PROJECT_LIBRARY,
                menu: [
                    {
                        title: 'navigation.menus.dataEntry.status',
                        action: (router) => router.push({name: ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS}),
                    },
                    {
                        title: 'navigation.menus.dataEntry.importRequestFile',
                        action: (router) => router.push({name: ROUTE_EDITOR_IMPORT_REQUEST_FILE}),
                    },
                ],
            }),
        ]
    }

    initObjectExplorers(): Promise<any> {

        return Promise.all([
            this.httpService.getAll<ProjectExplorerGetDto>(
                this.httpService.getEndpointFromType(ObjectTypeEnum.DATA_ENTRY_PROJECT),
                {
                    'platform.site': this.store.getters['navigation/getActiveRoleSite'].siteIri,
                    groups: ['transfer_explorer_view'],
                    'exists[transferDate]': true,
                },
            )
                .then(value => {
                    value.data['hydra:member'].forEach(project => {
                        const runningProjectExplorer = this.constructRunningProjectExplorerItem(project)
                        this.store.dispatch('navigation/add2explorer', {
                            item: runningProjectExplorer,
                            attachment: RUNNING_PROJECT_LIBRARY_EXPLORER_NAME,
                        })
                            .then()
                    })
                }),
        ])
    }

    constructRunningProjectExplorerItem(project: ProjectExplorerGetDto) {

        return this.dataEntryExplorerFactory.createRunningProjectExplorerItem(project)
    }

}
