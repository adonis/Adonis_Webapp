import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import { IndividualChangeDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-change-dto'
import { MobileResponseDto } from '@adonis/webapp/services/http/entities/simple-dto/mobile-response-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'

export type StatusDataEntryDto = AbstractDtoObject & {

    user?: UserDto | string,
    status?: string,
    syncable?: boolean,
    responseFile?: { contentUrl: string },
    response?: MobileResponseDto,
    webappProject?: ProjectDto,
    changes?: IndividualChangeDto[],
    mobileProjectName?: string,
}
