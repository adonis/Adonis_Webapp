import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type ProjectDto = AbstractDtoObject & HasSiteDto & {

    name?: string,
    platform?: string,
    created?: Date,
    experiments?: string[],
    simpleVariables?: string[],
    generatorVariables?: string[],
    devices?: string[],
    comment?: string,
    owner?: string | UserDto,
    pathBase?: string,
    projectDatas?: string[],
    stateCodes?: string[],
    statusDataEntries?: string[],
}
