import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import CombinationOperationEnum from '@adonis/webapp/constants/combination-operation-enum'
import ComparisonOperationEnum from '@adonis/webapp/constants/comparison-operation-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type TestDto = AbstractDtoObject & HasSiteDto & {
  type?: TestTypeEnum,
  variable?: string | AbstractDtoObject,

  authIntervalMin?: number,
  probIntervalMin?: number,
  probIntervalMax?: number,
  authIntervalMax?: number,

  comparedVariable?: string | AbstractDtoObject,
  minGrowth?: number,
  maxGrowth?: number,

  combinedVariable1?: string | AbstractDtoObject,
  combinationOperation?: CombinationOperationEnum,
  combinedVariable2?: string | AbstractDtoObject,
  minLimit?: number,
  maxLimit?: number,

  compareWithVariable?: boolean,
  comparedVariable1?: string | AbstractDtoObject,
  comparisonOperation?: ComparisonOperationEnum,
  comparedVariable2?: string | AbstractDtoObject,
  comparedValue?: number,
  assignedValue?: string,
}
