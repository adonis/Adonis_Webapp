import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import SpreadingEnum from '@adonis/webapp/constants/spreading-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type StateCodeDto = AbstractDtoObject & HasSiteDto & {

  iri?: string,
  code?: number,
  title?: string,
  meaning?: string,
  spreading?: SpreadingEnum,
  color?: number,
  project?: string,
  permanent?: boolean,
}
