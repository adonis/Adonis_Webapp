import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type UnitPlotChangeItemDto = AbstractDtoObject & {

  stateCode: { code: number, label: string },
  treatment: { name: string, shortName: string },
  block: { name: string },
  subBlock: {
    block: { name: string },
  },
  apparitionDate: Date,
  demiseDate: Date,
  x: number,
  y: number,
}
