import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type CloneProjectDto = AbstractDtoObject & {
  project?: string
  newName?: string,
  result?: string,
  platform?: string,
}
