import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { OezNatureDto } from '@adonis/webapp/services/http/entities/simple-dto/oez-nature-dto'

export type OutExperimentationZoneDto = AbstractDtoObject & {
  number?: string,
  x?: number,
  y?: number,
  color?: number,
  nature?: string | OezNatureDto,
  comment?: string,
  openSilexUri?: string,
}
