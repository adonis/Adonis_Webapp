import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type UserGroupDto = AbstractDtoObject & HasSiteDto & {
  name?: string,
  users?: string[],
}
