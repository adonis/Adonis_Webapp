import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type ValueListDto = AbstractDtoObject & HasSiteDto & {

  name?: string,
  values?: {
    name: string,
  }[],
  variable?: string,
}
