import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'
import ParsingErrorEnum from '@adonis/webapp/constants/parsing-error-enum'

export type ParsingJobDto = AbstractDtoObject & {
  site?: string,
  objectType?: string,
  status?: JobStatusEnum,
  contentUrl?: string,
  uploadDate?: string,
  file?: any,
  objectIri?: string,
  errors?: ParsingErrorEnum[],
}
