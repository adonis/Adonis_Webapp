import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type VariableScaleDto = AbstractDtoObject & {
  name?: string,
  minValue?: number,
  maxValue?: number,
  open?: boolean,
  variable?: string,
  values?: {
    text: string,
    value: number,
    pic: string,
  }[],
}
