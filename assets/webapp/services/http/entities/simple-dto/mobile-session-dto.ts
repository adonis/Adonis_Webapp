export type MobileSessionDto = {

  id?: number,
  startedAt?: Date,
  endedAt?: Date,
}
