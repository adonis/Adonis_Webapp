import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'

export type SemiAutomaticVariableDto = AbstractDtoObject & {
  name?: string,
  shortName?: string,
  repetitions?: number,
  unit?: string,
  pathLevel?: PathLevelEnum,
  comment?: string,
  order?: number,
  format?: VariableFormatEnum,
  formatLength?: number,
  defaultTrueValue?: boolean,
  type?: VariableTypeEnum,
  mandatory?: boolean,
  identifier?: string,
  start?: number,
  end?: number,
  created?: Date,
  device?: string,
  project?: string,
  connectedVariables?: string[] | VariableConnectionDto [],
  openSilexUri?: string,
}
