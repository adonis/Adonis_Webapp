import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'

export type DataEntryFusionDto = AbstractDtoObject & {
  orderedDefaultProjectDatasIris?: string[]
  selectedVariableIris?: string[]
  specificOrderForItem?: {
    objectIri: string,
    orderedProjectDatasIris: string[],
  }[],
  result?: string,
  platform?: string,
  conflicts?: FieldMeasureDto[][],
  mergePriority?: string[],
  name: string,
}
