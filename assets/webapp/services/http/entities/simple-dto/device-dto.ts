import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import DeviceCommunicationProtocolEnum from '@adonis/webapp/constants/device-communication-protocol-enum'
import Rs232BitFormat from '@adonis/webapp/constants/rs232-bit-format'
import Rs232FlowControl from '@adonis/webapp/constants/rs232-flow-control'
import Rs232Parity from '@adonis/webapp/constants/rs232-parity'
import Rs232StopBit from '@adonis/webapp/constants/rs232-stop-bit'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'

export type DeviceDto = AbstractDtoObject & HasSiteDto & {
  alias?: string,
  name?: string,
  manufacturer?: string,
  type?: string,
  managedVariables?: SemiAutomaticVariableDto[],
  communicationProtocol?: DeviceCommunicationProtocolEnum,
  frameLength?: number,
  frameStart?: string,
  frameEnd?: string,
  frameCsv?: string,
  baudrate?: number,
  bitFormat?: Rs232BitFormat,
  flowControl?: Rs232FlowControl,
  parity?: Rs232Parity,
  stopBit?: Rs232StopBit,
  remoteControl?: boolean,
  project?: string,
  openSilexUri?: string,
}
