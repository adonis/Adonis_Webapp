import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'

export type VariableConnectionDto = AbstractDtoObject & {
  projectSimpleVariable?: string,
  projectGeneratorVariable?: string,
  projectSemiAutomaticVariable?: string,
  dataEntrySimpleVariable?: string | SimpleVariableDto,
  dataEntryGeneratorVariable?: string | GeneratorVariableDto,
  dataEntrySemiAutomaticVariable?: string | SemiAutomaticVariableDto,
}
