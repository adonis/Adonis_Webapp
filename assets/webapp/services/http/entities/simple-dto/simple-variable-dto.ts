import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { ValueListDto } from '@adonis/webapp/services/http/entities/simple-dto/value-list-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import { VariableScaleDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-scale-dto'

export type SimpleVariableDto = AbstractDtoObject & HasSiteDto & {
  name?: string,
  shortName?: string,
  repetitions?: number,
  unit?: string,
  pathLevel?: PathLevelEnum,
  comment?: string,
  order?: number,
  format?: VariableFormatEnum,
  formatLength?: number,
  defaultTrueValue?: boolean,
  type?: VariableTypeEnum,
  mandatory?: boolean,
  identifier?: string,
  created?: Date,
  project?: string,
  scale?: string | VariableScaleDto,
  valueList?: string | ValueListDto,
  connectedVariables?: string[] | VariableConnectionDto [],
  openSilexUri?: string,
  openSilexInstance?: string,
}
