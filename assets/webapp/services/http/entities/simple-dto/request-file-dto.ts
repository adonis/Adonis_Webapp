import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'

export type RequestFileDto = AbstractDtoObject & {
  name?: string,
  status?: JobStatusEnum,
  contentUrl?: string,
  uploadDate?: Date,
  linedError?: any,
  disabled?: boolean,
  deleted?: boolean,
}
