import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'
import { ExperimentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/experiment-data-view-dto'
import { IndividualDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/individual-data-view-dto'
import { SubBlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/sub-block-data-view-dto'
import { SurfacicUnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/surfacic-unit-plot-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { StateCodeDto } from '@adonis/webapp/services/http/entities/simple-dto/state-code-dto'

export type AnnotationDto = AbstractDtoObject & HasSiteDto & {
  categories?: string[],
  image?: string,
  keywords?: string[],
  name?: string,
  target?: string | BlockDataViewDto | ExperimentDataViewDto | IndividualDataViewDto | SubBlockDataViewDto | SurfacicUnitPlotDataViewDto | UnitPlotDataViewDto | (AbstractDtoObject & {
    value?: string,
    repetition?: number,
    timestamp?: string,
    state?: StateCodeDto,
  }),
  targetType?: PathLevelEnum,
  timestamp?: string,
  type?: number,
  value?: string,
}
