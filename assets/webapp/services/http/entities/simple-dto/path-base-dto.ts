import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { PathFilterNodeDto } from '@adonis/webapp/services/http/entities/simple-dto/path-filter-node-dto'
import { PathLevelAlgorithmsDto } from '@adonis/webapp/services/http/entities/simple-dto/path-level-algorithms-dto'

export type PathBaseDto = AbstractDtoObject & {
  name?: string,
  pathFilterNodes?: PathFilterNodeDto[],
  project?: string,
  askWhenEntering?: boolean,
  pathLevelAlgorithms?: PathLevelAlgorithmsDto[],
  selectedIris?: string[],
  orderedIris?: string[],
  userPaths?: string[],
}
