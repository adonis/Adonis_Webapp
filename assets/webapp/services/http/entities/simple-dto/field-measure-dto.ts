import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'
import { ExperimentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/experiment-data-view-dto'
import { IndividualDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/individual-data-view-dto'
import { SubBlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/sub-block-data-view-dto'
import { SurfacicUnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/surfacic-unit-plot-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { StateCodeDto } from '@adonis/webapp/services/http/entities/simple-dto/state-code-dto'

export type FieldMeasureDto = AbstractDtoObject & {

  variable?: string | AbstractDtoObject & {
    format: VariableFormatEnum,
  },
    target?: BlockDataViewDto | ExperimentDataViewDto | IndividualDataViewDto | SubBlockDataViewDto | SurfacicUnitPlotDataViewDto | UnitPlotDataViewDto | string,
  measures?: (AbstractDtoObject & {
    value?: string,
    repetition?: number,
    timestamp?: string,
    state?: StateCodeDto,
  })[],
  fieldGenerations?: {
    index?: number,
    prefix?: string,
    numeralIncrement?: boolean,
    children?: FieldMeasureDto[],
  }[],
  targetType?: PathLevelEnum,
}
