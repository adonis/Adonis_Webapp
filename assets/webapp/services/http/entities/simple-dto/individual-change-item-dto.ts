import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type IndividualChangeItemDto = AbstractDtoObject & {

  stateCode: { code: number, label: string },
  unitParcel: {
    treatment: { name: string, shortName: string },
    block: { name: string },
    subBlock: {
      block: { name: string },
    },
  },
  apparitionDate: Date,
  demiseDate: Date,
  x: number,
  y: number,
}
