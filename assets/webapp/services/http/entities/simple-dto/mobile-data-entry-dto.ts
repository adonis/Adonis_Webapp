import { MobileSessionDto } from '@adonis/webapp/services/http/entities/simple-dto/mobile-session-dto'

export type MobileDataEntryDto = {

  id?: number,
  sessions: MobileSessionDto[],
}
