import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'

export type SurfacicUnitPlotDto = AbstractDtoObject & {
  number?: string,
  x?: number,
  y?: number,
  dead?: boolean,
  appeared?: string,
  disappeared?: string,
  identifier?: string,
  latitude?: number,
  longitude?: number,
  height?: number
  notes?: string[],
  treatment?: string | TreatmentDto,
  color?: number,
  comment?: string,
  openSilexUri?: string,
    geometry?: string,
}
