import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type GraphicalTextZoneDto = AbstractDtoObject & {
  text?: string,
  color?: number,
  x?: number,
  y?: number,
  width?: number,
  height?: number,
  platform?: string,
  size?: number,
}
