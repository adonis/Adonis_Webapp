import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { AlgorithmDto } from '@adonis/webapp/services/http/entities/simple-dto/algorithm-dto'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'

export type ProtocolDto = AbstractDtoObject & {
  name?: string,
  aim?: string,
  comment?: string,
  created?: string,
  factors?: (string | FactorDto)[],
  site?: string,
  treatments?: (string | TreatmentDto)[],
  owner?: string,
  algorithm?: string | AlgorithmDto,
  protocolAttachments?: (string | AttachmentDto)[],
}
