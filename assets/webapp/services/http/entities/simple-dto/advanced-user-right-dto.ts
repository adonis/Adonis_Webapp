import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import AdvancedRightEnum from '@adonis/webapp/constants/advanced-right-enum'

export type AdvancedUserRightDto = AbstractDtoObject & {
  userId?: number,
  objectId?: number,
  classIdentifier?: AdvancedRightClassIdentifierEnum,
  right?: AdvancedRightEnum,
}