import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type DeleteDto = AbstractDtoObject & {
  objectIris: any[],
}
