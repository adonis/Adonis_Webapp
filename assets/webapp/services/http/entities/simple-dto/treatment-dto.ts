import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'

export type TreatmentDto = AbstractDtoObject & {
  name?: string,
  shortName?: string,
  repetitions?: number,
  modalities?: (string | ModalityDto)[]
  protocol?: string,
}
