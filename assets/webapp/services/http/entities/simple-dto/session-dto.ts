import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { AnnotationDto } from '@adonis/webapp/services/http/entities/simple-dto/annotation-dto'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'

export type SessionDto = AbstractDtoObject & {

  startedAt?: Date,
  endedAt?: Date,
  fieldMeasures?: FieldMeasureDto[],
  annotations?: AnnotationDto[],
  comment?: string,
  userName?: string,
}
