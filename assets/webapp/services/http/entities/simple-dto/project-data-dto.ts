import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SessionDto } from '@adonis/webapp/services/http/entities/simple-dto/session-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'

export type ProjectDataDto = AbstractDtoObject & {

  name?: string,
  start?: Date,
  end?: Date,
  sessions?: (SessionDto | string)[],
  project?: ProjectDto | string,
  userName?: string
  comment?: string,
  fusion?: boolean,
  simpleVariables?: (SimpleVariableDto | string)[],
  generatorVariables?: (GeneratorVariableDto | string)[],
  semiAutomaticVariables?: (SemiAutomaticVariableDto | string)[],
}
