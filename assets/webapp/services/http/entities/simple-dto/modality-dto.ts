import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type ModalityDto = AbstractDtoObject & HasSiteDto & {
  value?: string,
  shortName?: string,
  identifier?: string,
  factor?: string,
  uniqId?: string,
  openSilexUri?: string,
  openSilexInstance?: string,
}
