import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type PlaceExperimentDto = AbstractDtoObject & {
  experiment: string,
  dx: number,
  dy: number,
  platform: string,
}
