import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type CloneVariableDto = AbstractDtoObject & {
  project?: string
  variable?: string,
  result?: string,
}
