import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type EditDto = AbstractDtoObject & {
  objectIris: any[],
  dx?: number,
  dy?: number,

}
