import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import AlgorithmConditionEnum from '@adonis/webapp/constants/algorithm-condition-enum'
import AlgorithmParameterEnum from '@adonis/webapp/constants/algorithm-parameter-enum'

export type AlgorithmDto = AbstractDtoObject & {
  name?: string,
  algorithmParameters?: {
    name: string;
    order: number;
    specialParam?: AlgorithmParameterEnum;
  }[],
  algorithmConditions?: {
    type: AlgorithmConditionEnum,
  }[],
  withSubBlock?: boolean,
  withRepartition?: boolean,
}
