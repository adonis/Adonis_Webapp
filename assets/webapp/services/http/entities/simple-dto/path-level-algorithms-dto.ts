import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import PossibleMoveEnum from '@adonis/webapp/constants/possible-move-enum'
import PossibleStartPointEnum from '@adonis/webapp/constants/possible-start-point-enum'

export type PathLevelAlgorithmsDto = AbstractDtoObject & {
  pathLevel: PathLevelEnum,
  move: PossibleMoveEnum,
  startPoint: PossibleStartPointEnum,
  orderedLevelIris: string[],
}
