import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'

export type SubBlockDto = AbstractDtoObject & {
  number?: string,
  unitPlots?: (string | UnitPlotDto)[],
  surfacicUnitPlots?: (string | SurfacicUnitPlotDto)[],
  outExperimentationZones?: (string | OutExperimentationZoneDto)[]
  notes?: string[],
  color?: number,
  comment?: string,
  openSilexUri?: string,
    geometry?: string,
}
