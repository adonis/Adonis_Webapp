import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type UserPathDto = AbstractDtoObject & {
  workflow: string[],
  user?: string,
  pathBase?: string,
}
