import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { BlockDto } from '@adonis/webapp/services/http/entities/simple-dto/block-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'

export type ExperimentDto = AbstractDtoObject & HasSiteDto & {
  name?: string,
  created?: Date,
  protocol?: string | ProtocolDto,
  individualUP?: boolean,
  comment?: string,
  owner?: string,
  platform?: string
  algorithmConfig?: any,
  blocks?: (string | BlockDto)[],
  outExperimentationZones?: (string | OutExperimentationZoneDto)[]
  notes?: string[],
  color?: number,
  state?: ExperimentStateEnum,
  experimentAttachments?: (string | AttachmentDto)[],
  openSilexUri?: string,
    geometry?: string,
}
