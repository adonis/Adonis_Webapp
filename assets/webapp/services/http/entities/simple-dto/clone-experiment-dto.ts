import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type CloneExperimentDto = AbstractDtoObject & {
  experiment?: string
  newName?: string,
  result?: string,
}
