import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type NoteDto = AbstractDtoObject & {
  text?: string,
  creationDate?: Date,
  creatorLogin?: string,
  creatorAvatar?: string,
  deleted?: boolean,
}
