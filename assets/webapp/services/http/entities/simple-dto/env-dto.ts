import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type EnvDto = AbstractDtoObject & {
  ldapOn?: boolean,
}
