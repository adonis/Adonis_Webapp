import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type ParametersDto = AbstractDtoObject & {
  daysBeforeFileDelete?: number,
}
