import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { GraphicalTextZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-text-zone-dto'
import { NorthIndicatorDto } from '@adonis/webapp/services/http/entities/simple-dto/north-indicator-dto'

export type PlatformDto = AbstractDtoObject & HasSiteDto & {
  name?: string,
  siteName?: string,
  placeName?: string,
  created?: string,
  experiments?: (string | ExperimentDto)[],
  projects?: string[],
  comment?: string,
  owner?: string,
  color?: number,
  xMesh?: number,
  yMesh?: number,
  origin?: GraphicalOriginEnum,
  platformAttachments?: (string | AttachmentDto)[],
  northIndicator?: NorthIndicatorDto | string,
  graphicalTextZones?: (string | GraphicalTextZoneDto)[],
}
