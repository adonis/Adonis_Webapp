import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import AnnotationKindEnum from '@adonis/webapp/constants/annotation-kind-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export type RequiredAnnotationDto = AbstractDtoObject & {
  level?: PathLevelEnum
  type?: AnnotationKindEnum
  askWhenEntering?: boolean
  comment?: string,
  project?: string,
}
