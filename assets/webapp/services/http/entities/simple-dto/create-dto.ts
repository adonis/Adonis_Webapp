import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export type CreateDto = AbstractDtoObject & {
  parent: string,
  x1: number,
  x2: number,
  y1: number,
  y2: number,
  pathLevel: PathLevelEnum,
  useSubBlock: boolean,
  treatment: string,
  createdObjects?: any[],
}
