import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import FilterComparatorsEnum from '@adonis/webapp/constants/filter-comparators-enum'
import FilterKeyEnum from '@adonis/webapp/constants/filters/filter-key-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'

export type PathFilterNodeDto = AbstractDtoObject & HasSiteDto & {
  type?: FilterKeyEnum,
  text?: string,
  branches?: PathFilterNodeDto[],
  operator?: FilterComparatorsEnum,
  value?: string,
  variable?: string | SimpleVariableDto | SemiAutomaticVariableDto | GeneratorVariableDto,
}
