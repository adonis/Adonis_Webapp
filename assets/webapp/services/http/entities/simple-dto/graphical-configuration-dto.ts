import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import {
  BlockLabelsEnum,
  ExperimentLabelsEnum,
  IndividualLabelsEnum,
  SubBlockLabelsEnum,
  SurfacicUnitPlotLabelsEnum,
  UnitPlotLabelsEnum,
} from '@adonis/webapp/constants/graphical-labels-enums'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type GraphicalConfigurationDto = AbstractDtoObject & HasSiteDto & {
  individualColor?: number,
  unitPlotColor?: number,
  blockColor?: number,
  subBlockColor?: number,
  experimentColor?: number,
  platformColor?: number,
  selectedItemColor?: number
  site?: string,
  activeBreadcrumb?: boolean
  experimentLabels?: ExperimentLabelsEnum[],
  blocLabels?: BlockLabelsEnum[],
  subBlocLabels?: SubBlockLabelsEnum[],
  unitPlotLabels?: UnitPlotLabelsEnum[],
  surfacicUnitPlotLabels?: SurfacicUnitPlotLabelsEnum[],
  individualLabels?: IndividualLabelsEnum[],
}
