import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'

export type FactorDto = AbstractDtoObject & HasSiteDto & {
  name?: string,
  modalities?: (string | ModalityDto)[]
  protocol?: string,
  owner?: string,
  order?: number,
  openSilexUri?: string,
  openSilexInstance?: string,
  germplasm?: boolean,
}
