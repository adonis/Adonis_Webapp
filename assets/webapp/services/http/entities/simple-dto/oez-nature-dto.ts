import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import TextureEnum from '@adonis/webapp/constants/texture-enum'

export type OezNatureDto = AbstractDtoObject & {
  nature?: string,
  color?: number,
  texture?: TextureEnum,
  site?: string,
}
