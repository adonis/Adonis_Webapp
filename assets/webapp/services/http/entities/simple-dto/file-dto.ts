import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type FileDto = AbstractDtoObject & {
  fileName?: string,
  originalFileName?: string,
  size?: number,
  date?: string,
}
