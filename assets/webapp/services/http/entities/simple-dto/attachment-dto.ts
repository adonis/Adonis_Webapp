import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { FileDto } from '@adonis/webapp/services/http/entities/simple-dto/file-dto'

export type AttachmentDto = AbstractDtoObject & HasSiteDto & {
  file: FileDto | string,
}
