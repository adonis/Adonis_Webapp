import { MobileDataEntryDto } from '@adonis/webapp/services/http/entities/simple-dto/mobile-data-entry-dto'

export type MobileResponseDto = {

  id?: number,
  dataEntry: MobileDataEntryDto,
}
