import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type IndividualDto = AbstractDtoObject & {
  number?: string,
  x?: number,
  y?: number,
  dead?: boolean,
  appeared?: string,
  disappeared?: string,
  identifier?: string,
  latitude?: number,
  longitude?: number,
  height?: number
  notes?: string[],
  color?: number,
  comment?: string,
  openSilexUri?: string,
    geometry?: string,
}
