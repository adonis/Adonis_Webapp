import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type NorthIndicatorDto = AbstractDtoObject & {
  orientation?: number,
  x?: number,
  y?: number,
  width?: number,
  height?: number,
  platform?: string,
}
