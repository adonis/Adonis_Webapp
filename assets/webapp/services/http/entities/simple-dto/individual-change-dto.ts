import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { IndividualChangeItemDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-change-item-dto'
import { UnitPlotChangeItemDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-change-item-dto'

export type IndividualChangeDto = AbstractDtoObject & {

  requestIndividual: IndividualChangeItemDto,
  responseIndividual: IndividualChangeItemDto,
  requestUnitParcel: UnitPlotChangeItemDto,
  responseUnitParcel: UnitPlotChangeItemDto,
}
