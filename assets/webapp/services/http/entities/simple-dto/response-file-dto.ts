import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import JobStatusEnum from '@adonis/webapp/constants/job-status-enum'

export type ResponseFileDto = AbstractDtoObject & {
  name?: string,
  status?: JobStatusEnum,
  contentUrl?: string,
  uploadDate?: Date,
}
