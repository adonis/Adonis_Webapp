import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { IndividualDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'

export type UnitPlotDto = AbstractDtoObject & {
  number?: string,
  individuals?: (string | IndividualDto)[]
  outExperimentationZones?: (string | OutExperimentationZoneDto)[]
  treatment?: string | TreatmentDto,
  notes?: string[],
  color?: number,
  comment?: string,
  openSilexUri?: string,
    geometry?: string,
}
