import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'

export type GeneratorVariableDto = AbstractDtoObject & HasSiteDto & {
  generatedPrefix?: string,
  numeralIncrement?: boolean,
  pathWayHorizontal?: boolean,
  directCounting?: boolean,
  generatedSimpleVariables?: string[] | SimpleVariableDto[],
  generatedGeneratorVariables?: string[] | GeneratorVariableDto[],
  name?: string,
  shortName?: string,
  repetitions?: number,
  unit?: string,
  pathLevel?: PathLevelEnum,
  comment?: string,
  order?: number,
  format?: VariableFormatEnum,
  formatLength?: number,
  mandatory?: boolean,
  identifier?: string,
  created?: Date,
  project?: string,
  connectedVariables?: string[] | VariableConnectionDto [],
  openSilexUri?: string,
}
