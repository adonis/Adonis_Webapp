import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type CopyDto = AbstractDtoObject & {
  objectIris: any[],
  dx?: number,
  dy?: number,
  newParentIri?: string,
  createdObjects?: any[],
}
