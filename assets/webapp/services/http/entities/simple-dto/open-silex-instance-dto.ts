import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type OpenSilexInstanceDto = AbstractDtoObject & HasSiteDto & {
  site?: string,
  url?: string,
}
