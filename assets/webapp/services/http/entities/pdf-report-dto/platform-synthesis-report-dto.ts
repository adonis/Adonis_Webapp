import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'

export type PlatformSynthesisReportDto = AbstractDtoObject & {
  name?: string,
  site?: {
    label?: string,
  }
  siteName?: string,
  placeName?: string,
  created?: string,
  experiments?: {
    name?: string,
    created?: Date,
    protocol?: {
      name?: string,
      algorithm?: {
        name?: string,
      },
    },
    individualUP?: boolean,
    owner?: {
      surname?: string,
    },
    state?: ExperimentStateEnum,
    projects?: {
      projectDatas?: {
        variables: string[],
      }[],
    }[],
  }[],
  comment?: string,
  owner?: {
    surname?: string,
  },
}
