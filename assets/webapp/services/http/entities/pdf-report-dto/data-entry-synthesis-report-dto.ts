import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import ChangeTypeEnum from '@adonis/webapp/constants/change-type-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'

export type DataEntrySynthesisReportDto = AbstractDtoObject & {
  name?: string,
  start?: string,
  end?: string,
  changeReports?: {
    aproved?: boolean,
    changeType?: ChangeTypeEnum,
  }[],
  sessions?: {
    startedAt?: string,
    endedAt?: string,
    fieldMeasures?: {
      variable?: {
        '@id'?: string,
      },
      target: string,
      measures?: {
        state?: {
          code?: number,
          title?: string,
          permanent?: boolean,
        },
        annotations?: {
          targetType?: PathLevelEnum,
        }[],
      }[],
      targetType?: PathLevelEnum,
    }[],
    annotations?: {
      targetType?: PathLevelEnum,
      categories?: string[],
    }[],
  }[],
  project?: {
    name?: string,
    platform?: {
      name?: string,
      site?: {
        label?: string,
      },
    },
    experiments?: {
      name?: string,
    }[],
    requiredAnnotations?: {
      level?: PathLevelEnum,
    }[],
  },
  user?: {
    username?: string,
    name?: string,
    surname?: string,
  },
  comment?: string,
  variables?: {
    '@id'?: string
    '@type'?: 'SimpleVariable' | 'GeneratorVariable' | 'SemiAutomaticVariable',
    name?: string,
    shortName?: string,
    type?: VariableTypeEnum,
    pathLevel?: PathLevelEnum,
    unit?: string,
    repetitions?: number,
    generatorVariable?: {
      shortName?: string,
    },
  }[],
}
