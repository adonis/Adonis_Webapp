import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import PossibleMoveEnum from '@adonis/webapp/constants/possible-move-enum'
import PossibleStartPointEnum from '@adonis/webapp/constants/possible-start-point-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'

export type VariableSynthesisReportDto = AbstractDtoObject & {
  name?: string,
  type?: VariableTypeEnum,
  tests?: {
    type?: TestTypeEnum,
  }[],
  unit?: string,
  projectData?: {
    project?: {
      name?: string,
      owner?: {
        username?: string,
        name?: string,
        surname?: string,
      },
      experiments?: {
        name?: string,
      }[],
      pathBase?: {
        userPaths?: string[],
        pathLevelAlgorithms?: {
          move?: PossibleMoveEnum,
          pathLevel?: PathLevelEnum,
          startPoint?: PossibleStartPointEnum,
        }[],
      },
      platform?: {
        name?: string,
        site?: {
          label?: string,
        },
      },
      stateCodes?: {
        code?: number,
        title?: string,
      }[],
    }
    sessions?:
        {
          fieldMeasures?:
              {
                target?: string,
                measures?: {
                  value?: string,
                  state?: {
                    code?: number,
                    title?: string,
                  },
                }[],
              }[],
        }[],
    user?: {
      username?: string,
      name?: string,
      surname?: string,
    },
    start?: string,
    end?: string,
    comment?: string,
  },
  shortName?: string,
  repetitions?: number,
  pathLevel?: PathLevelEnum,
  comment?: string,

}
