import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import ChangeTypeEnum from '@adonis/webapp/constants/change-type-enum'
import { IndividualDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/individual-data-view-dto'
import { SurfacicUnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/surfacic-unit-plot-data-view-dto'

export type ModificationReportDto = AbstractDtoObject & {
  name?: string,
  start?: string,
  changeReports?: {
    aproved?: boolean,
    changeType?: ChangeTypeEnum,
    involvedIndividual?: IndividualDataViewDto,
    involvedSurfacicUnitPlot?: SurfacicUnitPlotDataViewDto,
    lastChangeDate: string,
    changeDate: string,
  }[],
  project?: {
    name?: string,
    experiments?: {
      name?: string,
    }[],
    platform: {
      site?: {
        label?: string,
      },
    },
  },
}
