import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { VariableRDto } from '@adonis/webapp/services/http/entities/pdf-report-dto/variable-r-dto'

export type ProjectSynthesisReportDto = AbstractDtoObject & {
  name?: string,
  platform?: {
    name?: string,
    site?: {
      label?: string,
    },
  },
  created?: Date,
  experiments?: {
    name?: string,
  }[],
  variables?: VariableRDto[],
  owner?: {
    username?: string,
    name?: string,
    surname?: string,
  },
  pathBase?: {
    userPaths?: {}[],
  },
  projectDatas?: {
    start?: string,
  }[],
}
