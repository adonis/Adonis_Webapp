import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type ProtocolSynthesisReportDto = AbstractDtoObject & {
  name?: string,
  aim?: string,
  comment?: string,
  created?: string,
  factors?: {
    name?: string,
    modalities?: {
      value?: string,
      shortName?: string,
    }[],
    protocol?: string,
    order?: number,
  }[],
  site?: {
    label?: string,
  },
  treatments?: {
    name?: string,
    shortName?: string,
    repetitions?: number,
    modalities?: {
      value?: string,
      shortName?: string,
    }[],
  }[],
  owner?: {
    name?: string,
    surname: string,
  },
  algorithm?: {
    name?: string,
  },
}
