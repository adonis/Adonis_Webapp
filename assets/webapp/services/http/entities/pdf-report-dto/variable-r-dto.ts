import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import CombinationOperationEnum from '@adonis/webapp/constants/combination-operation-enum'
import ComparisonOperationEnum from '@adonis/webapp/constants/comparison-operation-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'

export type VariableRDto = AbstractDtoObject & {
  '@id'?: string
  '@type'?: 'SimpleVariable' | 'GeneratorVariable' | 'SemiAutomaticVariable',
  generatedVariables?: VariableRDto[]
  name?: string,
  shortName?: string,
  type?: VariableTypeEnum,
  pathLevel?: PathLevelEnum,
  unit?: string,
  repetitions?: number,
  order?: number
  scale: {
    name: string,
  },
  connectedVariables: {}[],
  tests: {
    type: TestTypeEnum,
    authIntervalMin?: number,
    probIntervalMin?: number,
    probIntervalMax?: number,
    authIntervalMax?: number,

    comparedVariable?: { shortName: string },
    minGrowth?: number,
    maxGrowth?: number,

    combinedVariable1?: { shortName: string },
    combinationOperation?: CombinationOperationEnum,
    combinedVariable2?: { shortName: string },
    minLimit?: number,
    maxLimit?: number,

    compareWithVariable?: boolean,
    comparedVariable1?: { shortName: string },
    comparisonOperation?: ComparisonOperationEnum,
    comparedVariable2?: { shortName: string },
    comparedValue?: number,
    assignedValue?: string,
  }[],
}
