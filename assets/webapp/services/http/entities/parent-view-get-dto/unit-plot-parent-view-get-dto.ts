import { UnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/unit-plot-explorer-get-dto'
import { BlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/block-parent-view-get-dto'
import { SubBlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/sub-block-parent-view-get-dto'

export type UnitPlotParentViewGetDto = UnitPlotExplorerGetDto & {
  subBlock?: SubBlockParentViewGetDto,
  block?: BlockParentViewGetDto,
}
