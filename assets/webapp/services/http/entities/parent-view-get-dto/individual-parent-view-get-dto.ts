import { IndividualExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/individual-explorer-get-dto'
import { UnitPlotParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/unit-plot-parent-view-get-dto'

export type IndividualParentViewGetDto = IndividualExplorerGetDto & {
  unitPlot: UnitPlotParentViewGetDto;
}
