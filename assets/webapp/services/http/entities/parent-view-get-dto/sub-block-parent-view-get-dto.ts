import { SubBlockExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/sub-block-explorer-get-dto'
import { BlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/block-parent-view-get-dto'

export type SubBlockParentViewGetDto = SubBlockExplorerGetDto & {
  block: BlockParentViewGetDto,
}
