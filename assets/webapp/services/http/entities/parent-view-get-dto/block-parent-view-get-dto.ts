import { BlockExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/block-explorer-get-dto'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'

export type BlockParentViewGetDto = BlockExplorerGetDto & {
  experiment: ExperimentExplorerGetDto,
}
