import { SurfacicUnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/surfacic-unit-plot-explorer-get-dto'
import { BlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/block-parent-view-get-dto'
import { SubBlockParentViewGetDto } from '@adonis/webapp/services/http/entities/parent-view-get-dto/sub-block-parent-view-get-dto'

export type SurfacicUnitPlotParentViewGetDto = SurfacicUnitPlotExplorerGetDto & {
  subBlock?: SubBlockParentViewGetDto,
  block?: BlockParentViewGetDto,
}
