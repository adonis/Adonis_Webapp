import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { PlatformDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/platform-data-view-dto'

export type ExperimentDataViewDto = AbstractDtoObject & {
  name?: string,
  platform?: PlatformDataViewDto,
}
