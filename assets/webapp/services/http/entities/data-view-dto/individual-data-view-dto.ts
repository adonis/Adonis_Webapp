import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'

export type IndividualDataViewDto = AbstractDtoObject & {
  number?: string,
  unitPlot?: UnitPlotDataViewDto,
  x?: number,
  y?: number,
  dead?: boolean,
  appeared?: string,
  disappeared?: string,
  identifier?: string,
}
