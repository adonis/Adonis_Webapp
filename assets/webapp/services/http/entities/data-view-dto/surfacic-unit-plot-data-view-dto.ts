import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'
import { SubBlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/sub-block-data-view-dto'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'

export type SurfacicUnitPlotDataViewDto = AbstractDtoObject & {
  number?: string,
  x?: number,
  y?: number,
  dead?: boolean,
  appeared?: string,
  disappeared?: string,
  identifier?: string,
  subBlock?: SubBlockDataViewDto,
  block?: BlockDataViewDto,
  treatment?: TreatmentDataViewDto,
}
