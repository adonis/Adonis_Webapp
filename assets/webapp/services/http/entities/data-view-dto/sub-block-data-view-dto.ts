import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'

export type SubBlockDataViewDto = AbstractDtoObject & {
  number?: string,
  block?: BlockDataViewDto,
}
