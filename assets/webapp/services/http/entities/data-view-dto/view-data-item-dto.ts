import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import UnitPlotDataView from '@adonis/webapp/models/data-view/unit-plot-data-view'
import FieldMeasure from '@adonis/webapp/models/field-measure'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'

export type ViewDataItemDto = AbstractDtoObject & {
   code?: number,
   fieldMeasure?: FieldMeasureDto,
   projectData?: string,
   session?: string,
   treatment?: TreatmentDataViewDto,
   repetition?: number,
   value?: string,
   timestamp?: Date,
   username?: string,
   target?: string,
   variable?: string,
}
