import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'
import { SubBlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/sub-block-data-view-dto'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'

export type UnitPlotDataViewDto = AbstractDtoObject & {
  number?: string,
  treatment?: TreatmentDataViewDto,
  block?: BlockDataViewDto,
  subBlock?: SubBlockDataViewDto,
}
