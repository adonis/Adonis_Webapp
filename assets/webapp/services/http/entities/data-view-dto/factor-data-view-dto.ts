import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type FactorDataViewDto = AbstractDtoObject & HasSiteDto & {
  name?: string,
  shortName?: string,
  order?: number,
}
