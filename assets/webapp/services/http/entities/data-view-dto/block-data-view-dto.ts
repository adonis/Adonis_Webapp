import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ExperimentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/experiment-data-view-dto'

export type BlockDataViewDto = AbstractDtoObject & {
  number?: string,
  experiment?: ExperimentDataViewDto,
}
