import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { FactorDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/factor-data-view-dto'
import { HasSiteDto } from '@adonis/webapp/services/http/entities/has-site-dto'

export type ModalityDataViewDto = AbstractDtoObject & HasSiteDto & {
  value?: string,
  shortName?: string,
  factor?: FactorDataViewDto,
}
