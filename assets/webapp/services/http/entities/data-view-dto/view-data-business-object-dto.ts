import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import UnitPlotDataView from '@adonis/webapp/models/data-view/unit-plot-data-view'
import FieldMeasure from '@adonis/webapp/models/field-measure'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'

export type ViewDataBusinessObjectDto = AbstractDtoObject & {
   name?: string,
   x?: number,
   y?: number,
   dead?: boolean,
   appeared?: string,
   disappeared?: string,
   identifier?: string,
   projectData?: string,
   session?: string,
   treatment?: TreatmentDataViewDto,
   target?: string,
   targetType?: PathLevelEnum,
   platform: string,
   experiment: string,
   block: string,
   subBlock: string,
   unitPlot: string,
   surfacicUnitPlot: string,
   individual: string,
}
