import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ModalityDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/modality-data-view-dto'

export type TreatmentDataViewDto = AbstractDtoObject & {
  name?: string,
  shortName?: string,
  modalities?: ModalityDataViewDto[],
}
