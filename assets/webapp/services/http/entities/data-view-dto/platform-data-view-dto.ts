import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type PlatformDataViewDto = AbstractDtoObject & {
  name?: string,
}
