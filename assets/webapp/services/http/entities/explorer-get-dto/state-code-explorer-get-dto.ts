import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type StateCodeExplorerGetDto = AbstractDtoObject & {

  title?: string,
  code?: number,
  permanent?: boolean,
}
