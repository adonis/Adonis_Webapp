import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ModalityExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/modality-explorer-get-dto'

export type TreatmentExplorerGetDto = AbstractDtoObject & {
  name: string,
  shortName: string,
  modalities: ModalityExplorerGetDto[],
}