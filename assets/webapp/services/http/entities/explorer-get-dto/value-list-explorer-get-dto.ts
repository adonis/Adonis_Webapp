import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type ValueListExplorerGetDto = AbstractDtoObject & {

  name?: string,
}
