import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { OezNatureExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/oez-nature-explorer-get-dto'

export type OutExperimentationZoneExplorerGetDto = AbstractDtoObject & {
  x: number,
  y: number,
  nature: OezNatureExplorerGetDto,
}
