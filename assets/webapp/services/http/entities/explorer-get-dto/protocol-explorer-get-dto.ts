import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { FactorExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/factor-explorer-get-dto'
import { TreatmentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/treatment-explorer-get-dto'

export type ProtocolExplorerGetDto = AbstractDtoObject & {
  name: string,
  treatments: TreatmentExplorerGetDto[],
  factors: FactorExplorerGetDto[],
  owner: string,
}
