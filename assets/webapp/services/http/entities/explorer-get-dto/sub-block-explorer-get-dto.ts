import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { SurfacicUnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/surfacic-unit-plot-explorer-get-dto'
import { UnitPlotExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/unit-plot-explorer-get-dto'

export type SubBlockExplorerGetDto = AbstractDtoObject & {
  number: string,
  unitPlots?: UnitPlotExplorerGetDto[],
  surfacicUnitPlots?: SurfacicUnitPlotExplorerGetDto[],
}
