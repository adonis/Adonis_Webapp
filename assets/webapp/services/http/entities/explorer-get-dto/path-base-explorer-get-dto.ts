import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { PathUserWorkflowExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/path-user-workflow-explorer-get-dto'

export type PathBaseExplorerGetDto = AbstractDtoObject & {
  name: string,
  userPaths?: PathUserWorkflowExplorerGetDto[],
}
