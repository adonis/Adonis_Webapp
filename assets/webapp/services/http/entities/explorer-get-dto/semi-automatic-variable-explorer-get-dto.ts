import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ConnectedVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/connected-variable-explorer-get-dto'
import { TestExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/test-explorer-get-dto'

export type SemiAutomaticVariableExplorerGetDto = AbstractDtoObject & {
  name?: string,
  order: number,
  tests: TestExplorerGetDto[],
  connectedVariables: ConnectedVariableExplorerGetDto[],
  device?: {
    alias: string,
  },
}
