import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ConnectedVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/connected-variable-explorer-get-dto'
import { SimpleVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import { TestExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/test-explorer-get-dto'

export type GeneratorVariableExplorerGetDto = AbstractDtoObject & {
  name: string,
  generatedSimpleVariables: SimpleVariableExplorerGetDto[],
  generatedGeneratorVariables: GeneratorVariableExplorerGetDto[],
  order: number,
  tests: TestExplorerGetDto[],
  connectedVariables: ConnectedVariableExplorerGetDto[],
}
