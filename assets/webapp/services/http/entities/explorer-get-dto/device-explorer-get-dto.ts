import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { SemiAutomaticVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/semi-automatic-variable-explorer-get-dto'

export type DeviceExplorerGetDto = AbstractDtoObject & {
  alias?: string,
  managedVariables?: SemiAutomaticVariableExplorerGetDto[],
}
