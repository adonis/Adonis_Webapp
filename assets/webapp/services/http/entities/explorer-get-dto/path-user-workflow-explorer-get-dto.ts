import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type PathUserWorkflowExplorerGetDto = AbstractDtoObject & {
  username: string,
}
