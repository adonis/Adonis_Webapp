import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type BlockExplorerGetDto = AbstractDtoObject & {
  number: string,
}
