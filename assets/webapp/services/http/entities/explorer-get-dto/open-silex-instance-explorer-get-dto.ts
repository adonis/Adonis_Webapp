import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type OpenSilexInstanceExplorerGetDto = AbstractDtoObject & {
  url: string,
}
