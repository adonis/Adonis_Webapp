import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ModalityExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/modality-explorer-get-dto'

export type FactorExplorerGetDto = AbstractDtoObject & {
  name: string,
  modalities: ModalityExplorerGetDto[],
  order: number,
  germplasm: boolean,
}
