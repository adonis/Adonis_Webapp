import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import AnnotationKindEnum from '@adonis/webapp/constants/annotation-kind-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export type RequiredAnnotationExplorerGetDto = AbstractDtoObject & {
  level?: PathLevelEnum,
  type?: AnnotationKindEnum,
}
