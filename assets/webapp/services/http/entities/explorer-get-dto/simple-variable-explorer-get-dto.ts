import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import { ConnectedVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/connected-variable-explorer-get-dto'
import { TestExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/test-explorer-get-dto'
import { ValueListExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'
import { VariableScaleExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/variable-scale-explorer-get-dto'

export type SimpleVariableExplorerGetDto = AbstractDtoObject & {
  name: string,
  order: number,
  tests: TestExplorerGetDto[],
  type: VariableTypeEnum,
  scale: VariableScaleExplorerGetDto,
  valueList: ValueListExplorerGetDto,
  connectedVariables: ConnectedVariableExplorerGetDto[],
}
