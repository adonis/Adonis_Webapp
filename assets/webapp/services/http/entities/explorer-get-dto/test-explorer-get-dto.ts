import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'

export type TestExplorerGetDto = AbstractDtoObject & {
  type: TestTypeEnum,
}