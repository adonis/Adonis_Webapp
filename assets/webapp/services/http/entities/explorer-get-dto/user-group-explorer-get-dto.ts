import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type UserGroupExplorerGetDto = AbstractDtoObject & {
  name: string,
}