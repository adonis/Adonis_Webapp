import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { TreatmentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/treatment-explorer-get-dto'

export type UnitPlotExplorerGetDto = AbstractDtoObject & {
  number: string,
  treatment: TreatmentExplorerGetDto,
}
