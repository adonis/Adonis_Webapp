import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import { FactorExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/factor-explorer-get-dto'
import { OezNatureExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/oez-nature-explorer-get-dto'
import {
  OpenSilexInstanceExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'

export type SiteDesignExplorerGetDto = AbstractDtoObject & {

  factors: FactorExplorerGetDto[],
  protocols: ProtocolExplorerGetDto[],
  experiments: ExperimentExplorerGetDto[],
  platforms: PlatformExplorerGetDto[],
  oezNatures: OezNatureExplorerGetDto[],
    openSilexInstances: OpenSilexInstanceExplorerGetDto[],
}
