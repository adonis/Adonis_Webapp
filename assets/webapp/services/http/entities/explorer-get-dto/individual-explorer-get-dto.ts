import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type IndividualExplorerGetDto = AbstractDtoObject & {
  number: string,
  x: number,
  y: number,
  dead: boolean,
}
