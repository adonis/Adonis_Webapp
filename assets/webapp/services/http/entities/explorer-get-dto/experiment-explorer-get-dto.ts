import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import { ProtocolExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/protocol-explorer-get-dto'

export type ExperimentExplorerGetDto = AbstractDtoObject & {
  name: string,
  protocol: ProtocolExplorerGetDto,
  owner: string,
  state: ExperimentStateEnum,
}
