import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type ModalityExplorerGetDto = AbstractDtoObject & {
  value: string,
  shortName: string,
}