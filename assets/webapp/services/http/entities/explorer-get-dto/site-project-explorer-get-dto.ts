import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { DeviceExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/device-explorer-get-dto'
import { GeneratorVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import {
  OpenSilexInstanceExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'
import { SimpleVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import { StateCodeExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/state-code-explorer-get-dto'
import { ValueListExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/value-list-explorer-get-dto'

export type SiteProjectExplorerGetDto = AbstractDtoObject & {

  simpleVariables: SimpleVariableExplorerGetDto[],
  stateCodes: StateCodeExplorerGetDto[],
  valueLists: ValueListExplorerGetDto[],
  generatorVariables: GeneratorVariableExplorerGetDto[],
  devices: DeviceExplorerGetDto[],
  platforms: PlatformExplorerGetDto[],
    openSilexInstances: OpenSilexInstanceExplorerGetDto[],
}
