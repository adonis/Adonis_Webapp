import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { DeviceExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/device-explorer-get-dto'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import { GeneratorVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import { PathBaseExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/path-base-explorer-get-dto'
import { ProjectDataExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-data-explorer-get-dto'
import {
  RequiredAnnotationExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/required-annotation-explorer-get-dto'
import { SimpleVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'
import { StateCodeExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/state-code-explorer-get-dto'

export type ProjectExplorerGetDto = AbstractDtoObject & {

  name: string,
  owner: string,
  experiments?: ExperimentExplorerGetDto[],
  simpleVariables?: SimpleVariableExplorerGetDto[],
  generatorVariables?: GeneratorVariableExplorerGetDto[],
  devices?: DeviceExplorerGetDto[],
  transferDate?: Date,
  stateCodes?: StateCodeExplorerGetDto[],
  projectDatas?: (string|ProjectDataExplorerGetDto)[],
  pathBase?: PathBaseExplorerGetDto,
  requiredAnnotations?: RequiredAnnotationExplorerGetDto[],
}
