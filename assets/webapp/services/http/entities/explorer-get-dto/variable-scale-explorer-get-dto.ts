import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type VariableScaleExplorerGetDto = AbstractDtoObject & {
  name: string,
}