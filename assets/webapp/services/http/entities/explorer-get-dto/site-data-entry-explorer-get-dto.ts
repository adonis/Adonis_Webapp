import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import {
  OpenSilexInstanceExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { PlatformExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/platform-explorer-get-dto'

export type SiteDataEntryExplorerGetDto = AbstractDtoObject & {

  platforms: PlatformExplorerGetDto[],
  openSilexInstances: OpenSilexInstanceExplorerGetDto[],
}
