import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type OezNatureExplorerGetDto = AbstractDtoObject & {
  nature: string,
}
