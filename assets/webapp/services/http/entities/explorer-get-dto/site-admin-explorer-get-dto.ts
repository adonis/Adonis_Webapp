import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import {
  OpenSilexInstanceExplorerGetDto,
} from '@adonis/webapp/services/http/entities/explorer-get-dto/open-silex-instance-explorer-get-dto'
import { UserGroupExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/user-group-explorer-get-dto'

export type SiteAdminExplorerGetDto = AbstractDtoObject & {
  userGroups: UserGroupExplorerGetDto[],
  openSilexInstances: OpenSilexInstanceExplorerGetDto[],
}
