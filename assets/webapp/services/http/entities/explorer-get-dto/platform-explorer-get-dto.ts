import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { ExperimentExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/experiment-explorer-get-dto'
import { ProjectExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/project-explorer-get-dto'

export type PlatformExplorerGetDto = AbstractDtoObject & {

  name: string,
  experiments?: ExperimentExplorerGetDto[],
  owner: string,
  projects?: ProjectExplorerGetDto[],
}
