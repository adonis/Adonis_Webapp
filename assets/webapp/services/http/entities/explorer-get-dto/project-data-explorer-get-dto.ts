import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { GeneratorVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/generator-variable-explorer-get-dto'
import {
  SemiAutomaticVariableExplorerGetDto
} from '@adonis/webapp/services/http/entities/explorer-get-dto/semi-automatic-variable-explorer-get-dto'
import { SessionExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/session-explorer-get-dto'
import { SimpleVariableExplorerGetDto } from '@adonis/webapp/services/http/entities/explorer-get-dto/simple-variable-explorer-get-dto'

export type ProjectDataExplorerGetDto = AbstractDtoObject & {

  name: string,
  start: string,
  end: string,
  sessions: SessionExplorerGetDto[],
  simpleVariables?: SimpleVariableExplorerGetDto[],
  generatorVariables?: GeneratorVariableExplorerGetDto[],
  semiAutomaticVariables?: SemiAutomaticVariableExplorerGetDto[],
  project: string,
  fusion: boolean,
  needPagination: boolean,
}
