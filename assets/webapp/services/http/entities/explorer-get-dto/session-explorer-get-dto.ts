import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type SessionExplorerGetDto = AbstractDtoObject & {

  startedAt: string, // Date String
  endedAt: string, // Date String
  needPagination: boolean,
}
