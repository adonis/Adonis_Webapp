
import { AbstractOpenSilexDtoObject } from '@adonis/webapp/services/http/open-silex-entities/open-silex-dto-types'

export type OpenSilexFactorDto = AbstractOpenSilexDtoObject & {
  name?: string,
  levels?: {
    uri: string,
    name: string,
  }[],
}
