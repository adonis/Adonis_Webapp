import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import { ScientificObjectExportDto } from '@adonis/webapp/services/http/open-silex-entities/experiment-export/scientific-object-export-dto'

export type ExperimentExportDto = AbstractDtoObject & {
    openSilexInstance?: OpenSilexInstanceDto | string
    name: string,
    individuals?: ScientificObjectExportDto[],
    unitPlots?: ScientificObjectExportDto[],
    surfacicUnitPlots?: ScientificObjectExportDto[],
    subBlocks?: ScientificObjectExportDto[],
    blocks?: ScientificObjectExportDto[],
    protocol?: {
        name: string
        aim: string
        iri: string,
        factors: {
            name: string,
            iri: string
            germplasm: boolean
            modalities: {
                openSilexUri: string,
                value: string,
                iri: string,
            }[],
        }[],
        treatments: {
            modalities: string[],
            iri: string,
        }[],
    },
}
