import { AbstractDtoObject } from '@adonis/shared/models/dto-types'

export type ScientificObjectExportDto = AbstractDtoObject & {
    appeared: string,
    disappeared: string,
    number: string,
    treatment: string,
    parent: string,
    iri: string,
    geometry: string,
}
