import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import AdvancedRightClassIdentifierEnum from '@adonis/webapp/constants/advanced-right-class-identifier-enum'
import AdvancedRightEnum from '@adonis/webapp/constants/advanced-right-enum'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import { AbstractOpenSilexDtoObject } from '@adonis/webapp/services/http/open-silex-entities/open-silex-dto-types'

export type OpenSilexGermplasmDto = AbstractOpenSilexDtoObject & {
  name?: string,
}
