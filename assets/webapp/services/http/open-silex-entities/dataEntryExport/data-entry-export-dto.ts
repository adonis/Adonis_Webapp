import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'

export type DataEntryExportDto = AbstractDtoObject & {
    openSilexInstance?: OpenSilexInstanceDto | string,
    name: string,
    end: string,
    project: {
        experiments: {
            name: string,
            openSilexInstance?: OpenSilexInstanceDto,
            protocolIri?: string,
        }[],
        variables: {
            openSilexUri?: string,
            openSilexInstance?: OpenSilexInstanceDto,
            name: string,
            unit: string,
        }[],
    },
    sessions: {
        user?: string,
        debut: string,
        fin: string,
        fields: {
            measures: {
                value?: string,
                timestamp?: string,
            }[],
            variableOpenSilexIri?: string,
            targetIri?: string,
        }[],
    }[],
    operator: {
        iri: string,
        name: string,
        surname: string,
        email: string,
    },
}
