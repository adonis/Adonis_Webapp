import { AbstractOpenSilexDtoObject } from '@adonis/webapp/services/http/open-silex-entities/open-silex-dto-types'

export type OpenSilexVariableDto = AbstractOpenSilexDtoObject & {
    name?: string,
    alternative_name?: string,
    description?: string,
    unit?: {
        name?: string,
    },
    datatype?: 'http://www.w3.org/2001/XMLSchema#boolean' |
        'http://www.w3.org/2001/XMLSchema#date' |
        'http://www.w3.org/2001/XMLSchema#dateTime' |
        'http://www.w3.org/2001/XMLSchema#decimal' |
        'http://www.w3.org/2001/XMLSchema#integer' |
        'http://www.w3.org/2001/XMLSchema#string',
}
