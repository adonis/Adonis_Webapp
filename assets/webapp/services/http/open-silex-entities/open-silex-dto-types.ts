export type AbstractOpenSilexDtoObject = {

    'uri'?: string,
}

export type AbstractOpenSilexDtoResponse<S> = {
    'result': S,
    'metadata': {
        'pagination': {
            'pageSize': number,
            'currentPage': number,
            'totalCount': number,
            'totalPages': number,
        },
        'status': [],
        'datafiles': [],
    },
}

export type AbstractOpenSilexDtoCollection<S> = {
    'result': S[],
    'metadata': {
        'pagination': {
            'pageSize': number,
            'currentPage': number,
            'totalCount': number,
            'totalPages': number,
        },
        'status': [],
        'datafiles': [],
    },
}

export class OpenSilexHttpError {
    constructor(
        public status: number,
        public error: any,
        public name = '',
    ) {

    }

}
