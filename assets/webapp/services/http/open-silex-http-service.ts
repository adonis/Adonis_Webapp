import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import OpenSilexObjectTypeEnum from '@adonis/webapp/constants/open-silex-object-type-enum'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'
import {
    AbstractOpenSilexDtoObject,
    AbstractOpenSilexDtoResponse,
    OpenSilexHttpError
} from '@adonis/webapp/services/http/open-silex-entities/open-silex-dto-types'
import { OpenSilexLoginDto } from '@adonis/webapp/services/http/open-silex-entities/open-silex-login-dto'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import { AxiosResponse } from 'axios'
import { Store } from 'vuex'

export default class OpenSilexHttpService {

    constructor(
        private store: Store<any>,
        private notifier: NotificationService,
    ) {
    }

    public post<S extends AbstractOpenSilexDtoObject>(openSilexInstance: OpenSilexInstance, iri: string, subject: any, params = {}): Promise<AbstractOpenSilexDtoResponse<S>> {
        const queryParms = new URLSearchParams(params)
        const attempt = () => this.fetchWithTimeout(`${openSilexInstance.url}/${iri}?${queryParms}`, {
            ...this.getConfig(openSilexInstance),
            method: 'POST',
            body: JSON.stringify(subject),
        })
            .then(response =>
                response.ok ?
                    response.json() :
                    response.json()
                        .then(err => {
                            throw new OpenSilexHttpError(response.status, err)
                        }),
            )
        return attempt()
            .catch((reason: OpenSilexHttpError) => this.reconnect(openSilexInstance, reason))
            .catch((reason: OpenSilexHttpError) => {
                this.onError(reason, ObjectActionEnum.CREATED)
                return Promise.reject(reason)
            })
    }

    public postFile<S extends AbstractOpenSilexDtoObject>(openSilexInstance: OpenSilexInstance, iri: string, body: FormData, params = {}): Promise<AbstractOpenSilexDtoResponse<S>> {
        const queryParms = new URLSearchParams(params)
        const attempt = () => this.fetchWithTimeout(`${openSilexInstance.url}/${iri}?${queryParms}`, {
            headers: {
                'Authorization': this.store.getters['openSilexConnection/authenticationHeader'](openSilexInstance),
            },
            mode: 'cors',
            method: 'POST',
            body,
        })
            .then(response =>
                response.ok ?
                    response.json() :
                    response.json()
                        .then(err => {
                            throw new OpenSilexHttpError(response.status, err)
                        }),
            )
        return attempt()
            .catch((reason: OpenSilexHttpError) => this.reconnect(openSilexInstance, reason))
            .catch((reason: OpenSilexHttpError) => {
                this.onError(reason, ObjectActionEnum.CREATED)
                return Promise.reject(reason)
            })
    }

    public get<S extends AbstractOpenSilexDtoObject>(openSilexInstance: OpenSilexInstance, iri: string, params = {}): Promise<AbstractOpenSilexDtoResponse<S>> {
        const queryParms = new URLSearchParams(params)
        const attempt = () => this.fetchWithTimeout(`${openSilexInstance.url}/${iri}?${queryParms}`, {
            ...this.getConfig(openSilexInstance),
            method: 'GET',
        })
            .then(response =>
                response.ok ?
                    response.json() :
                    response.json()
                        .then(err => {
                            throw new OpenSilexHttpError(response.status, err)
                        }),
            )
        return attempt()
            .catch((reason: OpenSilexHttpError) => this.reconnect(openSilexInstance, reason))
            .catch((reason: OpenSilexHttpError) => {
                this.onError(reason, ObjectActionEnum.FETCHED)
                return Promise.reject(reason)
            })
    }

    public delete(openSilexInstance: OpenSilexInstance, iri: string, params = {}): Promise<AxiosResponse> {
        const queryParms = new URLSearchParams(params)
        const attempt = () => this.fetchWithTimeout(`${openSilexInstance.url}/${iri}?${queryParms}`, {
            ...this.getConfig(openSilexInstance),
            method: 'DELETE',
        })
            .then(response =>
                response.ok ?
                    response.json() :
                    response.json()
                        .then(err => {
                            throw new OpenSilexHttpError(response.status, err)
                        }),
            )
        return attempt()
            .catch((reason: OpenSilexHttpError) => this.reconnect(openSilexInstance, reason))
            .catch((reason: OpenSilexHttpError) => {
                this.onError(reason, ObjectActionEnum.FETCHED)
                return Promise.reject(reason)
            })
    }

    public connect(openSilexInstance: OpenSilexInstance, credentials: {
        identifier: string,
        password: string,
    }): Promise<OpenSilexLoginDto> {
        return this.post<OpenSilexLoginDto>(openSilexInstance, '/rest/security/authenticate', credentials)
            .then(item => item.result)
    }

    protected getConfig(openSilexInstance: OpenSilexInstance): RequestInit {

        return {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': this.store.getters['openSilexConnection/authenticationHeader'](openSilexInstance),
            },
            mode: 'cors',
        }
    }

    protected reconnect(openSilexInstance: OpenSilexInstance, error: OpenSilexHttpError) {
        if (401 === error.status) {
            return this.store.dispatch('openSilexConnection/askInstanceCredentials', openSilexInstance)
                .then(() => Promise.reject(error))
        }
        return Promise.reject(error)
    }

    protected onError(error: OpenSilexHttpError, action: ObjectActionEnum) {
        if (error.name === 'AbortError') {
            return this.notifier.timeoutError()
        }
        switch (error.status) {
            case 400:
                return this.notifier.objectMultipleFailure(error.error)
            case 401:
                return null
            case 403:
                return this.notifier.wrongPassword()
            case 404:
                return this.notifier.notFound()
            case 409:
                return this.notifier.objectFailure(`${error.error.result.title} ${error.error.result.message}`)
        }
        return this.notifier.defaultObjectFailure(action)
    }

    async fetchWithTimeout(resource: string, options = {}) {
        const timeout = 8000

        const controller = new AbortController()
        const id = setTimeout(() => controller.abort(), timeout)

        const response = await fetch(resource, {
            ...options,
            signal: controller.signal,
        })
        clearTimeout(id)

        return response
    }

    public getEndpointFromType(type: OpenSilexObjectTypeEnum, uri = ''): string {
        switch (type) {
            case OpenSilexObjectTypeEnum.GERMPLASM:
                return `/rest/core/germplasm/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.GERMPLASM_GROUP:
                return `/rest/core/germplasm_group/with-germplasm/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.SCIENTIFIC_OBJECT_IMPORT_VALIDATION:
                return `rest/core/scientific_objects/import_validation`
            case OpenSilexObjectTypeEnum.SCIENTIFIC_OBJECT_IMPORT:
                return `rest/core/scientific_objects/import`
            case OpenSilexObjectTypeEnum.SCIENTIFIC_OBJECT:
                return `rest/core/scientific_objects/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.EXPERIMENT:
                return `rest/core/experiments/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.FACTOR:
                return `rest/core/experiments/factors/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.SYSTEM_INFO:
                return `rest/core/system/info`
            case OpenSilexObjectTypeEnum.VARIABLE:
                return `rest/core/variables/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.PROVENANCE:
                return `rest/core/provenances/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.DEVICE:
                return `rest/core/devices/${encodeURIComponent(uri)}`
            case OpenSilexObjectTypeEnum.DATA_IMPORT_VALIDATION:
                return `rest/core/data/import_validation`
            case OpenSilexObjectTypeEnum.DATA_IMPORT:
                return `rest/core/data/import`
            case OpenSilexObjectTypeEnum.PERSON:
                return `rest/security/persons/${encodeURIComponent(uri)}`
        }
        return undefined
    }
}
