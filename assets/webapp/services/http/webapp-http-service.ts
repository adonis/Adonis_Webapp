import HttpService from '@adonis/shared/services/http-service'
import NotificationService from '@adonis/webapp/services/notification/notification-service'
import { Store } from 'vuex'

export default class WebappHttpService extends HttpService {

    constructor(
        store: Store<any>,
        notifier: NotificationService,
    ) {
        super(store, notifier)
    }
}
