import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import NotificationServiceInterface from '@adonis/shared/services/notification-service-interface'
import AlertTypeEnum from '@adonis/webapp/constants/alert-type-enum'
import ParsingErrorEnum from '@adonis/webapp/constants/parsing-error-enum'
import SnackAlertModel from '@adonis/webapp/stores/notification/models/snack-alert.model'
import { AxiosError } from 'axios'
import { Store } from 'vuex'

export default class NotificationService implements NotificationServiceInterface {

    constructor(
        private store: Store<any>,
    ) {
    }

    objectSuccess(object: ObjectTypeEnum, action: ObjectActionEnum): void {

        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.SUCCESS,
                duration: 4,
                messages: [{message: `notifications.alerts.${object}.${action}`}],
            }),
        })
    }

    defaultObjectFailure(action: ObjectActionEnum, reason?: AxiosError): void {

        const serverErrorCode = (reason?.response as any)?.data?.['hydra:description']

        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: undefined,
                messages: [{message: serverErrorCode ? `serverErrors.${serverErrorCode}` : `notifications.alerts.failure.${action}`}],
            }),
        })
    }

    objectFailure(message: string): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: undefined,
                messages: [{message}],
            }),
        })
    }

    objectMultipleFailure(messages: string[]): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: undefined,
                messages: messages.map(message => ({message})),
            }),
        })
    }

    parsingError(errors: any): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: undefined,
                messages: Object.keys(errors)
                    .reduce((acc, line) => [
                        ...acc,
                        ...errors[line].map((error: ParsingErrorEnum) => ({
                            message: 'notifications.alerts.failure.linedError',
                            params: {line, error},
                        })),
                    ], []),
            }),
        })
    }

    parsingWarnings(errors: any): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.WARNING,
                duration: undefined,
                messages: Object.keys(errors)
                    .reduce((acc, line) => [
                        ...acc,
                        ...errors[line].map((error: ParsingErrorEnum) => ({
                            message: 'notifications.alerts.failure.linedError',
                            params: {line, error},
                        })),
                    ], []),
            }),
        })
    }

    accessDenied(): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: 4,
                messages: [{message: `notifications.alerts.failure.accessDenied`}],
            }),
        })
    }

    wrongPassword(): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: 4,
                messages: [{message: `notifications.alerts.failure.wrongPassword`}],
            }),
        })
    }

    notFound(): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: 4,
                messages: [{message: `notifications.alerts.failure.notFound`}],
            }),
        })
    }

    serverError(): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: 4,
                messages: [{message: `notifications.alerts.failure.serverError`}],
            }),
        })
    }

    timeoutError(): void {
        this.store.dispatch('notification/alert', {
            alert: new SnackAlertModel({
                type: AlertTypeEnum.FAILURE,
                duration: undefined,
                messages: [{message: `notifications.alerts.failure.openSilexConnexionTimeout`}],
            }),
        })
    }

    clear() {
        this.store.dispatch('notification/clear')
    }
}
