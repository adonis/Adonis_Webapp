import SubBlock from '@adonis/webapp/models/sub-block'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { SubBlockDto } from '@adonis/webapp/services/http/entities/simple-dto/sub-block-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'

export default class SubBlockTransformer extends AbstractTransformer<SubBlockDto, SubBlock, any> {

    private _noteTransformer: NoteTransformer

    dtoToObject(from: SubBlockDto): SubBlock {
        return new SubBlock(
            from['@id'],
            from.id,
            from.number,
            from.unitPlots as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.surfacicUnitPlots as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.outExperimentationZones as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.comment,
            from.openSilexUri,
            from.geometry
        )
    }

    objectToFormInterface(from: SubBlock): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: any): SubBlockDto {
        return undefined
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }
}
