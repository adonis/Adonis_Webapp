import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import PathBaseFormInterface from '@adonis/webapp/form-interfaces/path-base-form-interface'
import PathBaseMovementFormInterface from '@adonis/webapp/form-interfaces/path-base-movement-form-interface'
import PathBase from '@adonis/webapp/models/path-base'
import { PathBaseDto } from '@adonis/webapp/services/http/entities/simple-dto/path-base-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { UserPathDto } from '@adonis/webapp/services/http/entities/simple-dto/user-path-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import PathFilterNodeTransformer from '@adonis/webapp/services/transformers/actions/path-filter-node-transformer'
import PathLevelAlgorithmTransformer from '@adonis/webapp/services/transformers/actions/path-level-algorithm-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import UserPathTransformer from '@adonis/webapp/services/transformers/actions/user-path-transformer'

export default class PathBaseTransformer extends AbstractTransformer<PathBaseDto, PathBase, {
    base: PathBaseFormInterface,
    movement: PathBaseMovementFormInterface
}> {

    private _pathFilterNodeTransformer: PathFilterNodeTransformer
    private _pathLevelAlgorithmTransformer: PathLevelAlgorithmTransformer
    private _userPathTransformer: UserPathTransformer
    private _projectTransformer: ProjectTransformer

    dtoToObject(from: PathBaseDto): PathBase {
        return new PathBase(
            from['@id'],
            from.id,
            from.name,
            from.pathFilterNodes.map(item => this._pathFilterNodeTransformer.dtoToObject(item)),
            from.askWhenEntering,
            from.pathLevelAlgorithms.map(item => this._pathLevelAlgorithmTransformer.dtoToObject(item)),
            from.selectedIris,
            from.orderedIris,
            from.userPaths,
            () => {
                const promise = this.httpService.getAll<UserPathDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.USER_PATH),
                    {pagination: false, pathBase: from['@id']})
                return from.userPaths.map((uri, index) => {
                    return promise
                        .then(response => this._userPathTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.project,
            () => this.httpService.get<ProjectDto>(from.project)
                .then(response => this._projectTransformer.dtoToObject(response.data)),
        )
    }

    objectToFormInterface(from: PathBase): Promise<{ base: PathBaseFormInterface, movement: PathBaseMovementFormInterface }> {
        return Promise.all(from.filters.map(filter => this._pathFilterNodeTransformer.objectToFormInterface(filter)))
            .then(async filters => ({
                base: {
                    name: from.name,
                    effectiveFilterTree: filters,
                    selectedIris: from.selectedIris,
                },
                movement: {
                    askWhenEntering: from.askWhenEntering,
                    pathLevelAlgorithms: await Promise.all(from.levels.map(level => this._pathLevelAlgorithmTransformer.objectToFormInterface(level))),
                    orderedIris: from.orderedIris,
                },
            }))
    }

    formInterfaceToDto(from: { base: PathBaseFormInterface, movement: PathBaseMovementFormInterface }): PathBaseDto {
        return {
            name: from.base.name,
            pathFilterNodes: from.base.effectiveFilterTree.map(item => this._pathFilterNodeTransformer.formInterfaceToDto(item)),
            askWhenEntering: from.movement.askWhenEntering,
            pathLevelAlgorithms: from.movement.pathLevelAlgorithms.map(level => this._pathLevelAlgorithmTransformer.formInterfaceToDto(level)),
            selectedIris: from.base.selectedIris,
            orderedIris: from.movement.orderedIris,
        }
    }

    set pathFilterNodeTransformer(value: PathFilterNodeTransformer) {
        this._pathFilterNodeTransformer = value
    }

    set pathLevelAlgorithmTransformer(value: PathLevelAlgorithmTransformer) {
        this._pathLevelAlgorithmTransformer = value
    }

    set userPathTransformer(value: UserPathTransformer) {
        this._userPathTransformer = value
    }

    set projectTransformer(value: ProjectTransformer) {
        this._projectTransformer = value
    }
}
