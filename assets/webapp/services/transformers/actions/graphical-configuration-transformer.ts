import GraphicalConfigurationFormInterface from '@adonis/webapp/form-interfaces/graphical-configuration-form-interface'
import GraphicalConfiguration from '@adonis/webapp/models/graphical-configuration'
import { GraphicalConfigurationDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-configuration-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class GraphicalConfigurationTransformer extends AbstractTransformer<GraphicalConfigurationDto, GraphicalConfiguration, GraphicalConfigurationFormInterface> {

  dtoToObject( from: GraphicalConfigurationDto ): GraphicalConfiguration {
    return new GraphicalConfiguration(
        from['@id'],
        from.individualColor,
        from.unitPlotColor,
        from.blockColor,
        from.subBlockColor,
        from.experimentColor,
        from.platformColor,
        from.selectedItemColor,
        from.activeBreadcrumb,
        from.experimentLabels,
        from.blocLabels,
        from.subBlocLabels,
        from.unitPlotLabels,
        from.surfacicUnitPlotLabels,
        from.individualLabels,
    )
  }

  objectToFormInterface( from: GraphicalConfiguration ): Promise<GraphicalConfigurationFormInterface> {
    return Promise.resolve( {
      individualColor: from.individualColor,
      unitPlotColor: from.unitPlotColor,
      blockColor: from.blockColor,
      subBlockColor: from.subBlockColor,
      experimentColor: from.experimentColor,
      platformColor: from.platformColor,
      selectedItemColor: from.selectedItemColor,
      activeBreadcrumb: from.activeBreadcrumb,
      experimentLabels: from.experimentLabels,
      blocLabels: from.blocLabels,
      subBlocLabels: from.subBlocLabels,
      unitPlotLabels: from.unitPlotLabels,
      surfacicUnitPlotLabels: from.surfacicUnitPlotLabels,
      individualLabels: from.individualLabels,
    } )
  }

  formInterfaceToDto( from: GraphicalConfigurationFormInterface ): GraphicalConfigurationDto {
    return {
      individualColor: from.individualColor,
      unitPlotColor: from.unitPlotColor,
      blockColor: from.blockColor,
      subBlockColor: from.subBlockColor,
      experimentColor: from.experimentColor,
      platformColor: from.platformColor,
      selectedItemColor: from.selectedItemColor,
      experimentLabels: from.experimentLabels,
      blocLabels: from.blocLabels,
      subBlocLabels: from.subBlocLabels,
      unitPlotLabels: from.unitPlotLabels,
      surfacicUnitPlotLabels: from.surfacicUnitPlotLabels,
      individualLabels: from.individualLabels,
    }
  }
}
