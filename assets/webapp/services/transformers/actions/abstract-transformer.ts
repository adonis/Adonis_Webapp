import ApiEntity from '@adonis/shared/models/api-entity'
import {AbstractDtoObject} from '@adonis/shared/models/dto-types'
import SharedAbstractTransformer from '@adonis/shared/services/transformers/abstract-transformer'
import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'

export default abstract class AbstractTransformer<S extends AbstractDtoObject, T extends ApiEntity, F> extends SharedAbstractTransformer<S, T> {

  public constructor(protected httpService: WebappHttpService) {
    super(httpService)
  }

  public abstract objectToFormInterface(from: T): Promise<F>

  public abstract formInterfaceToDto(from: F): S

  public formInterfaceToCreateDto(from: F): S {
    return this.formInterfaceToDto(from)
  }

  public formInterfaceToUpdateDto(from: F): S {
    return this.formInterfaceToDto(from)
  }
}
