import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { ValueListDto } from '@adonis/webapp/services/http/entities/simple-dto/value-list-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import { VariableScaleDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-scale-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import OpenSilexInstanceTransformer from '@adonis/webapp/services/transformers/actions/open-silex-instance-transformer'
import ValueListTransformer from '@adonis/webapp/services/transformers/actions/value-list-transformer'
import VariableConnectionTransformer from '@adonis/webapp/services/transformers/actions/variable-connection-transformer'
import VariableScaleTransformer from '@adonis/webapp/services/transformers/actions/variable-scale-transformer'

export default class SimpleVariableTransformer extends AbstractTransformer<SimpleVariableDto, SimpleVariable, VariableFormInterface> {

    private _variableScaleTransformer: VariableScaleTransformer

    private _valueListTransformer: ValueListTransformer

    private _variableConnectionTransformer: VariableConnectionTransformer

    private _openSilexInstanceTransformer: OpenSilexInstanceTransformer

    set openSilexInstanceTransformer(value: OpenSilexInstanceTransformer) {
        this._openSilexInstanceTransformer = value
    }

    dtoToObject(from: SimpleVariableDto): SimpleVariable {
        return new SimpleVariable(
            from['@id'],
            from.id,
            from.name,
            from.shortName,
            from.repetitions,
            from.unit,
            from.pathLevel,
            from.comment,
            from.order,
            from.format,
            from.formatLength,
            from.defaultTrueValue,
            from.type,
            from.mandatory,
            from.identifier,
            from.created,
            from.project,
            (typeof from.scale === 'undefined' || typeof from.scale === 'string') ? from.scale : (from.scale as VariableScaleDto)['@id'],
            () => !from.scale ? Promise.resolve(null) : this.httpService.get<VariableScaleDto>(typeof from.scale === 'string' ? from.scale : (from.scale as VariableScaleDto)['@id'])
                .then(response => this._variableScaleTransformer.dtoToObject(response.data)),
            from.valueList as string,
            () => !from.valueList ? Promise.resolve(null) : this.httpService.get<ValueListDto>((from.valueList as string))
                .then(response => this._valueListTransformer.dtoToObject(response.data)),
            from.connectedVariables as string[],
            () => {
                const promise = this.httpService.getAll<VariableConnectionDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.VARIABLE_CONNECTION),
                    {pagination: false, projectGeneratorVariable: from['@id']})
                return (from.connectedVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._variableConnectionTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.openSilexUri,
            from.openSilexInstance,
            () => !from.openSilexInstance ? Promise.resolve(null) : this.httpService.get<OpenSilexInstanceDto>(from.openSilexInstance)
                .then(response => this._openSilexInstanceTransformer.dtoToObject(response.data)),
        )
    }

    async objectToFormInterface(from: SimpleVariable): Promise<VariableFormInterface> {
        return {
            name: from.name,
            shortName: from.shortName,
            type: from.type,
            format: from.format,
            formatLength: from.formatLength,
            pathLevel: from.pathLevel,
            defaultTrueValue: from.defaultTrueValue,
            repetitions: from.repetitions,
            unit: from.unit,
            required: from.mandatory,
            identifier: from.identifier,
            comment: from.comment,
            valueList: await from.valueList ? await this._valueListTransformer.objectToFormInterface(await from.valueList) : null,
            scale: await from.variableScale ? await this._variableScaleTransformer.objectToFormInterface(await from.variableScale) : null,
            openSilexInstance: await from.openSilexInstance,
            openSilexUri: from.openSilexUri,
        }
    }

    formInterfaceToUpdateDto(from: VariableFormInterface): SimpleVariableDto {
        return {
            ...this.formInterfaceToDto(from),
            type: undefined,
        }
    }

    set variableScaleTransformer(value: VariableScaleTransformer) {
        this._variableScaleTransformer = value
    }

    set valueListTransformer(value: ValueListTransformer) {
        this._valueListTransformer = value
    }

    set variableConnectionTransformer(value: VariableConnectionTransformer) {
        this._variableConnectionTransformer = value
    }

    formInterfaceToDto(from: VariableFormInterface): SimpleVariableDto {
        return {
            name: from.name,
            shortName: from.shortName,
            repetitions: from.repetitions,
            unit: from.unit,
            pathLevel: from.pathLevel,
            comment: from.comment,
            format: from.format,
            formatLength: from.formatLength,
            defaultTrueValue: from.defaultTrueValue,
            type: from.type,
            mandatory: from.required,
            valueList: !from.valueList ? undefined : this._valueListTransformer.formInterfaceToDto(from.valueList),
            scale: !from.scale ? undefined : this._variableScaleTransformer.formInterfaceToDto(from.scale),
            identifier: from.identifier,
            openSilexUri: from.openSilexUri,
            openSilexInstance: from.openSilexInstance?.iri,
        }
    }
}
