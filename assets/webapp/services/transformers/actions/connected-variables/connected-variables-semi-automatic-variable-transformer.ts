import ManagedVariableExtraInfoFormInterface from '@adonis/webapp/form-interfaces/managed-variable-extra-info-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import {SemiAutomaticVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import {VariableConnectionDto} from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ConnectedVariablesVariableConnectionTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-variable-connection-transformer'

// tslint:disable-next-line:max-line-length
export default class ConnectedVariablesSemiAutomaticVariableTransformer extends AbstractTransformer<SemiAutomaticVariableDto, SemiAutomaticVariable, (VariableFormInterface & ManagedVariableExtraInfoFormInterface)> {

  private _connectedVariablesVariableConnectionTransformer: ConnectedVariablesVariableConnectionTransformer

  public dtoToObject(from: SemiAutomaticVariableDto): SemiAutomaticVariable {
    return new SemiAutomaticVariable(
      from['@id'],
      from.id,
      from.name,
      from.shortName,
      from.repetitions,
      from.unit,
      from.pathLevel,
      from.comment,
      from.order,
      from.format,
      from.formatLength,
      from.defaultTrueValue,
      from.type,
      from.mandatory,
      from.identifier,
      from.start,
      from.end,
      new Date(from.created),
      from.project,
      (from.connectedVariables as VariableConnectionDto[]).map(item => item['@id']),
      () => (from.connectedVariables as VariableConnectionDto[]).map(item => Promise.resolve(this._connectedVariablesVariableConnectionTransformer.dtoToObject(item))),
      from.openSilexUri,
    )
  }

  public objectToFormInterface(from: SemiAutomaticVariable): Promise<(VariableFormInterface & ManagedVariableExtraInfoFormInterface)> {
    return Promise.resolve({
      name: from.name,
      shortName: from.shortName,
      type: from.type,
      format: from.format,
      formatLength: from.formatLength,
      pathLevel: from.pathLevel,
      defaultTrueValue: from.defaultTrueValue,
      repetitions: from.repetitions,
      unit: from.unit,
      required: from.mandatory,
      identifier: from.identifier,
      comment: from.comment,
      frameStart: from.start,
      frameEnd: from.end,
    })
  }

  public formInterfaceToDto(from: VariableFormInterface & ManagedVariableExtraInfoFormInterface): SemiAutomaticVariableDto {
    return {
      name: from.name,
      shortName: from.shortName,
      repetitions: from.repetitions,
      unit: from.unit,
      pathLevel: from.pathLevel,
      comment: from.comment,
      format: from.format,
      formatLength: from.formatLength,
      defaultTrueValue: from.defaultTrueValue,
      type: from.type,
      mandatory: from.required,
      identifier: from.identifier,
      start: from.frameStart,
      end: from.frameEnd,
    }
  }

  public formInterfaceToUpdateDto(from: VariableFormInterface & ManagedVariableExtraInfoFormInterface): SemiAutomaticVariableDto {
    return {
      ...this.formInterfaceToDto(from),
      type: undefined,
    }
  }

  public set connectedVariablesVariableConnectionTransformer(value: ConnectedVariablesVariableConnectionTransformer) {
    this._connectedVariablesVariableConnectionTransformer = value
  }
}
