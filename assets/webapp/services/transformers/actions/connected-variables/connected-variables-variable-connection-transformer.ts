import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import VariableConnectionFormInterface from '@adonis/webapp/form-interfaces/variable-connection-form-interface'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import VariableConnection from '@adonis/webapp/models/variable-connection'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ConnectedVariablesGeneratorVariableTransformer
    from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-generator-variable-transformer'
import ConnectedVariablesSemiAutomaticVariableTransformer
    from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-semi-automatic-variable-transformer'
import ConnectedVariablesSimpleVariableTransformer
    from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-simple-variable-transformer'

export default class ConnectedVariablesVariableConnectionTransformer extends AbstractTransformer<VariableConnectionDto, VariableConnection, VariableConnectionFormInterface> {

    private _connectedVariablesGeneratorVariableTransformer: ConnectedVariablesGeneratorVariableTransformer
    private _connectedVariablesSimpleVariableTransformer: ConnectedVariablesSimpleVariableTransformer
    private _connectedVariablesSemiAutomaticVariableTransformer: ConnectedVariablesSemiAutomaticVariableTransformer

    dtoToObject(from: VariableConnectionDto): VariableConnection {
        const dataEntryVariableDto: SimpleVariableDto | SemiAutomaticVariableDto | GeneratorVariableDto =
            from.dataEntrySimpleVariable as SimpleVariableDto ??
            from.dataEntryGeneratorVariable as GeneratorVariableDto ??
            from.dataEntrySemiAutomaticVariable as SemiAutomaticVariableDto
        return new VariableConnection(
            from['@id'],
            from.projectSimpleVariable ?? from.projectGeneratorVariable ?? from.projectSemiAutomaticVariable,
            () => this.httpService.get<AbstractDtoObject>(from.projectSimpleVariable ?? from.projectGeneratorVariable ?? from.projectSemiAutomaticVariable)
                .then(response => {
                    if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE))) {
                        return this._connectedVariablesSimpleVariableTransformer.dtoToObject(response.data as SimpleVariableDto)
                    } else if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE))) {
                        return this._connectedVariablesGeneratorVariableTransformer.dtoToObject(response.data as GeneratorVariableDto)
                    } else if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE))) {
                        return this._connectedVariablesSemiAutomaticVariableTransformer.dtoToObject(response.data as SemiAutomaticVariableDto)
                    }
                }),
            dataEntryVariableDto['@id'],
            () => {
                if (dataEntryVariableDto['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE))) {
                    return Promise.resolve(this._connectedVariablesSimpleVariableTransformer.dtoToObject(dataEntryVariableDto))
                } else if (dataEntryVariableDto['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE))) {
                    return Promise.resolve(this._connectedVariablesGeneratorVariableTransformer.dtoToObject(dataEntryVariableDto))
                } else if (dataEntryVariableDto['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE))) {
                    return Promise.resolve(this._connectedVariablesSemiAutomaticVariableTransformer.dtoToObject(dataEntryVariableDto))
                }
            },
        )
    }

    async objectToFormInterface(from: VariableConnection): Promise<VariableConnectionFormInterface> {
        return {
            iri: from.iri,
            projectVariable: await from.projectVariable,
            dataEntryVariable: await from.dataEntryVariable,
        }
    }

    formInterfaceToDto(from: VariableConnectionFormInterface): VariableConnectionDto {
        return {
            projectSimpleVariable: from.projectVariable instanceof SimpleVariable ? from.projectVariable.iri : null,
            projectGeneratorVariable: from.projectVariable instanceof GeneratorVariable ? from.projectVariable.iri : null,
            projectSemiAutomaticVariable: from.projectVariable instanceof SemiAutomaticVariable ? from.projectVariable.iri : null,
            dataEntrySimpleVariable: from.dataEntryVariable instanceof SimpleVariable ? from.dataEntryVariable.iri : null,
            dataEntryGeneratorVariable: from.dataEntryVariable instanceof GeneratorVariable ? from.dataEntryVariable.iri : null,
            dataEntrySemiAutomaticVariable: from.dataEntryVariable instanceof SemiAutomaticVariable ? from.dataEntryVariable.iri : null,
        }
    }

    set connectedVariablesGeneratorVariableTransformer(value: ConnectedVariablesGeneratorVariableTransformer) {
        this._connectedVariablesGeneratorVariableTransformer = value
    }

    set connectedVariablesSimpleVariableTransformer(value: ConnectedVariablesSimpleVariableTransformer) {
        this._connectedVariablesSimpleVariableTransformer = value
    }

    set connectedVariablesSemiAutomaticVariableTransformer(value: ConnectedVariablesSemiAutomaticVariableTransformer) {
        this._connectedVariablesSemiAutomaticVariableTransformer = value
    }
}
