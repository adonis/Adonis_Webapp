import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { ValueListDto } from '@adonis/webapp/services/http/entities/simple-dto/value-list-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import { VariableScaleDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-scale-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ConnectedVariablesVariableConnectionTransformer
    from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-variable-connection-transformer'
import ValueListTransformer from '@adonis/webapp/services/transformers/actions/value-list-transformer'
import VariableScaleTransformer from '@adonis/webapp/services/transformers/actions/variable-scale-transformer'

export default class ConnectedVariablesSimpleVariableTransformer extends AbstractTransformer<SimpleVariableDto, SimpleVariable, VariableFormInterface> {

    private _variableScaleTransformer: VariableScaleTransformer

    set variableScaleTransformer(value: VariableScaleTransformer) {
        this._variableScaleTransformer = value
    }

    private _valueListTransformer: ValueListTransformer

    set valueListTransformer(value: ValueListTransformer) {
        this._valueListTransformer = value
    }

    private _connectedVariablesVariableConnectionTransformer: ConnectedVariablesVariableConnectionTransformer

    set connectedVariablesVariableConnectionTransformer(value: ConnectedVariablesVariableConnectionTransformer) {
        this._connectedVariablesVariableConnectionTransformer = value
    }

    dtoToObject(from: SimpleVariableDto): SimpleVariable {
        return new SimpleVariable(
            from['@id'],
            from.id,
            from.name,
            from.shortName,
            from.repetitions,
            from.unit,
            from.pathLevel,
            from.comment,
            from.order,
            from.format,
            from.formatLength,
            from.defaultTrueValue,
            from.type,
            from.mandatory,
            from.identifier,
            new Date(from.created),
            from.project,
            from.scale as string,
            () => !from.scale ? Promise.resolve(null) : this.httpService.get<VariableScaleDto>(from.scale as string)
                .then(response => this._variableScaleTransformer.dtoToObject(response.data)),
            from.valueList as string,
            () => !from.valueList ? Promise.resolve(null) : this.httpService.get<ValueListDto>((from.valueList as string))
                .then(response => this._valueListTransformer.dtoToObject(response.data)),
            (from.connectedVariables as VariableConnectionDto[]).map(item => item['@id']),
            () => (from.connectedVariables as VariableConnectionDto[]).map(item => Promise.resolve(this._connectedVariablesVariableConnectionTransformer.dtoToObject(item))),
            from.openSilexUri,
            from.openSilexInstance,
            () => Promise.resolve(undefined),
        )
    }

    async objectToFormInterface(from: SimpleVariable): Promise<VariableFormInterface> {
        return {
            name: from.name,
            shortName: from.shortName,
            type: from.type,
            format: from.format,
            formatLength: from.formatLength,
            pathLevel: from.pathLevel,
            defaultTrueValue: from.defaultTrueValue,
            repetitions: from.repetitions,
            unit: from.unit,
            required: from.mandatory,
            identifier: from.identifier,
            comment: from.comment,
            valueList: await from.valueList ? await this._valueListTransformer.objectToFormInterface(await from.valueList) : null,
            scale: await from.variableScale ? await this._variableScaleTransformer.objectToFormInterface(await from.variableScale) : null,
            openSilexInstance: await from.openSilexInstance,
            openSilexUri: from.openSilexUri,
        }
    }

    formInterfaceToDto(from: VariableFormInterface): SimpleVariableDto {
        return {
            name: from.name,
            shortName: from.shortName,
            repetitions: from.repetitions,
            unit: from.unit,
            pathLevel: from.pathLevel,
            comment: from.comment,
            format: from.format,
            formatLength: from.formatLength,
            defaultTrueValue: from.defaultTrueValue,
            type: from.type,
            mandatory: from.required,
            valueList: from.valueList === undefined ? undefined : this._valueListTransformer.formInterfaceToDto(from.valueList),
            scale: from.scale === undefined ? undefined : this._variableScaleTransformer.formInterfaceToDto(from.scale),
            identifier: from.identifier,
            openSilexUri: from.openSilexUri,
            openSilexInstance: from.openSilexInstance?.iri,
        }
    }

    formInterfaceToUpdateDto(from: VariableFormInterface): SimpleVariableDto {
        return {
            ...this.formInterfaceToDto(from),
            type: undefined,
        }
    }
}
