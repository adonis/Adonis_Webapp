import DeviceFormInterface from '@adonis/webapp/form-interfaces/device-form-interface'
import Rs232FormInterface from '@adonis/webapp/form-interfaces/rs232-form-interface'
import Device from '@adonis/webapp/models/device'
import { DeviceDto } from '@adonis/webapp/services/http/entities/simple-dto/device-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ConnectedVariablesSemiAutomaticVariableTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-semi-automatic-variable-transformer'

export default class ConnectedVariableDeviceTransformer extends AbstractTransformer<DeviceDto, Device, DeviceFormInterface> {

  private _connectedVariablesSemiAutomaticVariableTransformer: ConnectedVariablesSemiAutomaticVariableTransformer

  dtoToObject( from: DeviceDto ): Device {
    return new Device(
        from['@id'],
        from.id,
        from.alias,
        from.name,
        from.manufacturer,
        from.type,
        from.managedVariables.map( managedVariable => managedVariable['@id'] ),
        () => from.managedVariables.map(
            managedVariable => Promise.resolve(
                this._connectedVariablesSemiAutomaticVariableTransformer.dtoToObject( managedVariable ),
            ),
        ),
        from.communicationProtocol,
        from.frameLength,
        from.frameStart,
        from.frameEnd,
        from.frameCsv,
        from.baudrate,
        from.bitFormat,
        from.flowControl,
        from.parity,
        from.stopBit,
        from.remoteControl,
        from.openSilexUri,
    )
  }

  objectToFormInterface( from: Device ): Promise<DeviceFormInterface> {
    return Promise.all( from.managedVariables )
        .then( managedVariables => Promise.all(
            managedVariables.map(
                managedVariable => this._connectedVariablesSemiAutomaticVariableTransformer.objectToFormInterface( managedVariable ) ),
            ),
        )
        .then( managedVariables => ({
          alias: from.alias,
          name: from.name,
          manufacturer: from.manufacturer,
          type: from.type,
          communicationProtocol: from.communicationProtocol,
          communicationProtocolsInfos: {
            frameLength: from.frameLength,
            frameStart: from.frameStart,
            frameEnd: from.frameEnd,
            frameCsv: from.frameCsv,
            baudrate: from.baudrate,
            bitFormat: from.bitFormat,
            flowControl: from.flowControl,
            parity: from.parity,
            stopBit: from.stopBit,
            remoteControl: from.remoteControl,
          },
          managedVariables,
        }) )
  }

  formInterfaceToDto( from: DeviceFormInterface ): DeviceDto {
    return {
      alias: from.alias,
      name: from.name,
      manufacturer: from.manufacturer,
      type: from.type,
      managedVariables: from.managedVariables.map( managedVariable => this._connectedVariablesSemiAutomaticVariableTransformer.formInterfaceToDto( managedVariable ) ),
      communicationProtocol: from.communicationProtocol,
      frameLength: from.communicationProtocolsInfos.frameLength,
      frameStart: from.communicationProtocolsInfos.frameStart,
      frameEnd: from.communicationProtocolsInfos.frameEnd,
      frameCsv: from.communicationProtocolsInfos.frameCsv,
      baudrate: (from.communicationProtocolsInfos as Rs232FormInterface).baudrate,
      bitFormat: (from.communicationProtocolsInfos as Rs232FormInterface).bitFormat,
      flowControl: (from.communicationProtocolsInfos as Rs232FormInterface).flowControl,
      parity: (from.communicationProtocolsInfos as Rs232FormInterface).parity,
      stopBit: (from.communicationProtocolsInfos as Rs232FormInterface).stopBit,
      remoteControl: (from.communicationProtocolsInfos as Rs232FormInterface).remoteControl,
    }
  }

  set connectedVariablesSemiAutomaticVariableTransformer( value: ConnectedVariablesSemiAutomaticVariableTransformer ) {
    this._connectedVariablesSemiAutomaticVariableTransformer = value
  }
}
