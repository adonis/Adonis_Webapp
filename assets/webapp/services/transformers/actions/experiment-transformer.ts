import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import ExperimentFormInterface from '@adonis/webapp/form-interfaces/experiment-form-interface'
import Experiment from '@adonis/webapp/models/experiment'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import AttachmentTransformer from '@adonis/webapp/services/transformers/actions/attachment-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import ProtocolTransformer from '@adonis/webapp/services/transformers/actions/protocol-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class ExperimentTransformer extends AbstractTransformer<ExperimentDto, Experiment, ExperimentFormInterface> {

    private _protocolTransformer: ProtocolTransformer
    private _userTransformer: UserTransformer
    private _noteTransformer: NoteTransformer
    private _attachmentTransformer: AttachmentTransformer

    dtoToObject(from: ExperimentDto): Experiment {
        return new Experiment(
            from['@id'],
            from.id,
            from.name,
            from.protocol as string,
            () => this.httpService.get<ProtocolDto>(from.protocol as string)
                .then(response => this._protocolTransformer.dtoToObject(response.data)),
            from.comment,
            from.created,
            from.individualUP,
            from.owner,
            () => this.httpService.get<UserDto>(from.owner)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.blocks as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.outExperimentationZones as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.state,
            from.experimentAttachments as string[],
            () => {
                const promise = this.httpService.getAll<AttachmentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT_ATTACHMENT),
                    {pagination: false, experiment: from['@id']})
                return (from.experimentAttachments as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._attachmentTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.openSilexUri,
            from.geometry,
        )
    }

    objectToFormInterface(from: Experiment): Promise<ExperimentFormInterface> {
        return Promise.resolve({
            name: from.name,
            state: from.state,
        })
    }

    formInterfaceToDto(from: ExperimentFormInterface): ExperimentDto {
        return {
            name: from.name,
            individualUP: from.individualUP,
            comment: from.comment,
            algorithmConfig: from.algorithmConfig,
            experimentAttachments: from.attachments.map(attachment => this._attachmentTransformer.formInterfaceToDto(attachment)),
        }
    }

    formInterfaceToUpdateDto(from: ExperimentFormInterface): ExperimentDto {
        return {
            state: from.state,
        }
    }

    set protocolTransformer(value: ProtocolTransformer) {
        this._protocolTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }

    set attachmentTransformer(value: AttachmentTransformer) {
        this._attachmentTransformer = value
    }
}
