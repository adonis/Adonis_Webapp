import NorthIndicatorFormInterface from '@adonis/webapp/form-interfaces/north-indicator-form-interface'
import NorthIndicator from '@adonis/webapp/models/north-indicator'
import { NorthIndicatorDto } from '@adonis/webapp/services/http/entities/simple-dto/north-indicator-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class NorthIndicatorTransformer extends AbstractTransformer<NorthIndicatorDto, NorthIndicator, NorthIndicatorFormInterface> {

  dtoToObject( from: NorthIndicatorDto ): NorthIndicator {
    return new NorthIndicator(
        from['@id'],
        from.id,
        from.orientation,
        from.x,
        from.y,
        from.width,
        from.height,
    )
  }

  objectToFormInterface( from: NorthIndicator ): Promise<NorthIndicatorFormInterface> {
    return Promise.resolve( {
      orientation: from.orientation,
      x: from.x,
      y: from.y,
      width: from.width,
      height: from.height,
    } )
  }

  formInterfaceToDto( from: NorthIndicatorFormInterface ): NorthIndicatorDto {
    return {
      orientation: from.orientation,
      x: from.x,
      y: from.y,
      width: from.width,
      height: from.height,
    }
  }
}
