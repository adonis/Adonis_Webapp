import ValueListFormInterface from '@adonis/webapp/form-interfaces/value-list-form-interface'
import ValueList from '@adonis/webapp/models/value-list'
import { ValueListDto } from '@adonis/webapp/services/http/entities/simple-dto/value-list-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ValueListTransformer extends AbstractTransformer<ValueListDto, ValueList, ValueListFormInterface> {

  dtoToObject( from: ValueListDto ): ValueList {

    return new ValueList(
        from['@id'],
        from.id,
        from.name,
        from.values,
    )
  }

  objectToFormInterface( from: ValueList ): Promise<ValueListFormInterface> {

    return Promise.resolve( {
      name: from.name,
      values: from.values.map( item => item.name ),
    } )
  }

  formInterfaceToDto( from: ValueListFormInterface ): ValueListDto {
    return {
      name: from.name,
      values: from.values.map( value => ({ name: value }) ),
    }
  }
}
