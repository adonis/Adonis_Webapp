import ExperimentFormInterface from '@adonis/webapp/form-interfaces/experiment-form-interface'
import Block from '@adonis/webapp/models/block'
import { BlockDto } from '@adonis/webapp/services/http/entities/simple-dto/block-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'

export default class BlockTransformer extends AbstractTransformer<BlockDto, Block, any> {

    private _noteTransformer: NoteTransformer

    dtoToObject(from: BlockDto): Block {
        return new Block(
            from['@id'],
            from.id,
            from.number,
            from.unitPlots as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.surfacicUnitPlots as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.subBlocks as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.outExperimentationZones as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.comment,
            from.openSilexUri,
            from.geometry
        )
    }

    objectToFormInterface(from: Block): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: ExperimentFormInterface): BlockDto {
        return undefined
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }
}
