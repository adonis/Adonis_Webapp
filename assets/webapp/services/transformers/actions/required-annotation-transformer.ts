import RequiredAnnotationFormInterface from '@adonis/webapp/form-interfaces/required-annotation-form-interface'
import RequiredAnnotation from '@adonis/webapp/models/required-annotation'
import { RequiredAnnotationDto } from '@adonis/webapp/services/http/entities/simple-dto/required-annotation-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class RequiredAnnotationTransformer extends AbstractTransformer<RequiredAnnotationDto, RequiredAnnotation, RequiredAnnotationFormInterface> {

  dtoToObject( from: RequiredAnnotationDto ): RequiredAnnotation {
    return new RequiredAnnotation(
        from['@id'],
        from.level,
        from.askWhenEntering,
        from.type,
        from.comment,
    )
  }

  objectToFormInterface( from: RequiredAnnotation ): Promise<RequiredAnnotationFormInterface> {
    return Promise.resolve( {
      level: from.level,
      type: from.type,
      askWhenEntering: from.askWhenEntering,
      comment: from.comment,
    } )
  }

  formInterfaceToDto( from: RequiredAnnotationFormInterface ): RequiredAnnotationDto {
    return {
      level: from.level,
      type: from.type,
      askWhenEntering: from.askWhenEntering,
      comment: from.comment,
    }
  }
}
