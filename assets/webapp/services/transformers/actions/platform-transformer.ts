import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import PlatformFormInterface from '@adonis/webapp/form-interfaces/platform-form-interface'
import Platform from '@adonis/webapp/models/platform'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { GraphicalTextZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-text-zone-dto'
import { NorthIndicatorDto } from '@adonis/webapp/services/http/entities/simple-dto/north-indicator-dto'
import { PlatformDto } from '@adonis/webapp/services/http/entities/simple-dto/platform-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import AttachmentTransformer from '@adonis/webapp/services/transformers/actions/attachment-transformer'
import ExperimentTransformer from '@adonis/webapp/services/transformers/actions/experiment-transformer'
import GraphicalTextZoneTransformer from '@adonis/webapp/services/transformers/actions/graphical-text-zone-transformer'
import NorthIndicatorTransformer from '@adonis/webapp/services/transformers/actions/north-indicator-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class PlatformTransformer extends AbstractTransformer<PlatformDto, Platform, PlatformFormInterface> {

    private _experimentTransformer: ExperimentTransformer
    private _projectTransformer: ProjectTransformer
    private _userTransformer: UserTransformer
    private _attachmentTransformer: AttachmentTransformer
    private _northIndicatorTransformer: NorthIndicatorTransformer
    private _graphicalTextZoneTransformer: GraphicalTextZoneTransformer

    dtoToObject(from: PlatformDto): Platform {
        return new Platform(
            from['@id'],
            from.id,
            from.name,
            from.siteName,
            from.placeName,
            from.experiments as string[],
            () => {
                const promise = this.httpService.getAll<ExperimentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.EXPERIMENT),
                    {pagination: false, platform: from['@id']})
                return from.experiments.map((uri, index) => {
                    return promise
                        .then(response => this._experimentTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.projects as string[],
            () => {
                const promise = this.httpService.getAll<ProjectDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DATA_ENTRY_PROJECT),
                    {pagination: false, platform: from['@id']})
                return from.projects.map((uri, index) => {
                    return promise
                        .then(response => this._projectTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.comment,
            from.created,
            from.owner,
            () => this.httpService.get<UserDto>(from.owner)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.color,
            from.xMesh,
            from.yMesh,
            from.origin,
            from.platformAttachments as string[],
            () => {
                const promise = this.httpService.getAll<AttachmentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PLATFORM_ATTACHMENT),
                    {pagination: false, platform: from['@id']})
                return (from.platformAttachments as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._attachmentTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.northIndicator as string,
            () => this.httpService.get<NorthIndicatorDto>(from.owner)
                .then(response => this._northIndicatorTransformer.dtoToObject(response.data)),
            from.graphicalTextZones as string[],
            () => {
                const promise = this.httpService.getAll<GraphicalTextZoneDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.GRAPHICAL_TEXT_ZONE),
                    {pagination: false, platform: from['@id']})
                return from.graphicalTextZones.map((uri, index) => {
                    return promise
                        .then(response => this._graphicalTextZoneTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
        )
    }

    objectToFormInterface(from: Platform): Promise<PlatformFormInterface> {
        return Promise.all(from.experiments)
            .then(experiments => ({
                name: from.name,
                location: from.placeName,
                site: from.siteName,
                comment: from.comment,
                creator: from.ownerIri,
                createdAt: from.created,
                experiments,
                xMesh: from.xMesh,
                yMesh: from.yMesh,
                origin: from.origin,
            }))
    }

    formInterfaceToDto(from: PlatformFormInterface): PlatformDto {
        return {
            name: from.name,
            comment: from.comment,
            siteName: from.site,
            placeName: from.location,
            experiments: [],
            platformAttachments: from.attachments.map(attachment => this._attachmentTransformer.formInterfaceToDto(attachment)),
        }
    }

    formInterfaceToUpdateDto(from: PlatformFormInterface): PlatformDto {
        return {
            xMesh: from.xMesh,
            yMesh: from.yMesh,
            origin: from.origin,
        }
    }

    set experimentTransformer(value: ExperimentTransformer) {
        this._experimentTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set projectTransformer(value: ProjectTransformer) {
        this._projectTransformer = value
    }

    set attachmentTransformer(value: AttachmentTransformer) {
        this._attachmentTransformer = value
    }

    set northIndicatorTransformer(value: NorthIndicatorTransformer) {
        this._northIndicatorTransformer = value
    }

    set graphicalTextZoneTransformer(value: GraphicalTextZoneTransformer) {
        this._graphicalTextZoneTransformer = value
    }
}
