import CloneExperimentFormInterface from '@adonis/webapp/form-interfaces/clone-experiment-form-interface'
import { CloneExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/clone-experiment-dto'
import { CloneProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/clone-project-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class CloneExperimentTransformer extends AbstractTransformer<CloneExperimentDto, any, CloneExperimentFormInterface> {

  dtoToObject( from: CloneProjectDto ): any {
    return undefined
  }

  objectToFormInterface( from: any ): Promise<CloneExperimentFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: CloneExperimentFormInterface ): CloneExperimentDto {
    return {
      newName: from.newName,
      experiment: from.experimentIri,
    }
  }
}
