import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ProjectData from '@adonis/webapp/models/project-data'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SessionTransformer from '@adonis/webapp/services/transformers/actions/session-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'

export default class ProjectDataTransformer extends AbstractTransformer<ProjectDataDto, ProjectData, any> {

    private _sessionTransformer: SessionTransformer

    private _projectTransformer: ProjectTransformer

    private _simpleVariableTransformer: SimpleVariableTransformer

    private _generatorVariableTransformer: GeneratorVariableTransformer

    private _semiAutomaticVariableTransformer: SemiAutomaticVariableTransformer

    dtoToObject(from: ProjectDataDto): ProjectData {

        return new ProjectData(
            from['@id'],
            from.id,
            from.name,
            new Date(from.start),
            new Date(from.end),
            from.userName,
            from.comment,
            from.fusion,
            from.sessions as string[],
            () => from.sessions.map(session => Promise.resolve(undefined)),
            from.project as string,
            () => this.httpService.get<ProjectDto>(from.project as string)
                .then(response => this._projectTransformer.dtoToObject(response.data)),
            from.simpleVariables as string[],
            () => {
                const promise = this.httpService.getAll<SimpleVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE),
                    {pagination: false, projectData: from['@id']})
                return (from.simpleVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._simpleVariableTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.generatorVariables as string[],
            () => {
                const promise = this.httpService.getAll<GeneratorVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE),
                    {pagination: false, projectData: from['@id']})
                return (from.generatorVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._generatorVariableTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.semiAutomaticVariables as string[],
            () => {
                const promise = this.httpService.getAll<SemiAutomaticVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE),
                    {pagination: false, projectData: from['@id']})
                return (from.semiAutomaticVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._semiAutomaticVariableTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
        )
    }

    formInterfaceToDto(from: any): ProjectDataDto {
        return undefined
    }

    objectToFormInterface(from: ProjectData): Promise<any> {
        return Promise.resolve(undefined)
    }

    set sessionTransformer(sessionTransformer: SessionTransformer) {
        this._sessionTransformer = sessionTransformer
    }

    set projectTransformer(value: ProjectTransformer) {
        this._projectTransformer = value
    }

    set simpleVariableTransformer(value: SimpleVariableTransformer) {
        this._simpleVariableTransformer = value
    }

    set generatorVariableTransformer(value: GeneratorVariableTransformer) {
        this._generatorVariableTransformer = value
    }

    set semiAutomaticVariableTransformer(value: SemiAutomaticVariableTransformer) {
        this._semiAutomaticVariableTransformer = value
    }
}
