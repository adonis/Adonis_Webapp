import AttachmentFormInterface from '@adonis/webapp/form-interfaces/attachment-form-interface'
import Attachment from '@adonis/webapp/models/attachment'
import File from '@adonis/webapp/models/file'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { FileDto } from '@adonis/webapp/services/http/entities/simple-dto/file-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AttachmentTransformer extends AbstractTransformer<AttachmentDto, Attachment, AttachmentFormInterface> {

  dtoToObject( from: AttachmentDto ): Attachment {
    return new Attachment(
        from['@id'],
        (from.file as FileDto)['@id'],
        () => Promise.resolve(new File(
            (from.file as FileDto)['@id'],
            (from.file as FileDto).id,
            (from.file as FileDto).fileName,
            (from.file as FileDto).originalFileName,
            (from.file as FileDto).size,
            (from.file as FileDto).date,
        )),
    )
  }

  objectToFormInterface( from: Attachment ): Promise<AttachmentFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: AttachmentFormInterface ): AttachmentDto {
    return {
      file: from.fileIri,
    }
  }
}
