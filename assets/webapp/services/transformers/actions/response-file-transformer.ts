import ResponseFile from '@adonis/webapp/models/response-file'
import { ResponseFileDto } from '@adonis/webapp/services/http/entities/simple-dto/response-file-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ResponseFileTransformer extends AbstractTransformer<ResponseFileDto, ResponseFile, any> {

  dtoToObject( from: ResponseFileDto ): ResponseFile {
    return new ResponseFile(
        from['@id'],
        from.id,
        from.name,
        from.status,
        from.contentUrl,
        from.uploadDate,
    )
  }

  formInterfaceToDto( from: any ): ResponseFileDto {
    return undefined
  }

  objectToFormInterface( from: ResponseFile ): Promise<any> {
    return Promise.resolve( undefined )
  }

}
