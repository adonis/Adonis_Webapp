import RequestFile from '@adonis/webapp/models/request-file'
import { RequestFileDto } from '@adonis/webapp/services/http/entities/simple-dto/request-file-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class RequestFileTransformer extends AbstractTransformer<RequestFileDto, RequestFile, any> {

  dtoToObject( from: RequestFileDto ): RequestFile {
    return new RequestFile(
        from['@id'],
        from.id,
        from.name,
        from.status,
        from.contentUrl,
        from.uploadDate,
        from.linedError,
        from.disabled,
        from.deleted,
    )
  }

  formInterfaceToDto( from: any ): RequestFileDto {
    return undefined
  }

  objectToFormInterface( from: RequestFile ): Promise<any> {
    return Promise.resolve( undefined )
  }

}
