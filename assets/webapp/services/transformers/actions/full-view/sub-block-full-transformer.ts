import SubBlock from '@adonis/webapp/models/sub-block'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { SubBlockDto } from '@adonis/webapp/services/http/entities/simple-dto/sub-block-dto'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import SurfacicUnitPlotFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/surfacic-unit-plot-full-transformer'
import UnitPlotFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/unit-plot-full-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import OutExperimentationZoneTransformer from '@adonis/webapp/services/transformers/actions/out-experimentation-zone-transformer'

export default class SubBlockFullTransformer extends AbstractTransformer<SubBlockDto, SubBlock, any> {

  private _unitPlotTransformer: UnitPlotFullTransformer
  private _surfacicUnitPlotTransformer: SurfacicUnitPlotFullTransformer
  private _noteTransformer: NoteTransformer
  private _outExperimentationZoneFullTransformer: OutExperimentationZoneTransformer

  dtoToObject( from: SubBlockDto ): SubBlock {
    return new SubBlock(
        from['@id'],
        from.id,
        from.number,
        from.unitPlots.map( ( unitPlot: UnitPlotDto ) => unitPlot['@id'] ),
        () => from.unitPlots.map( ( unitPlot: UnitPlotDto ) =>
            Promise.resolve( this._unitPlotTransformer.dtoToObject( unitPlot ) ),
        ),
        from.surfacicUnitPlots.map( ( surfacicUnitPlot: SurfacicUnitPlotDto ) => surfacicUnitPlot['@id'] ),
        () => from.surfacicUnitPlots.map( ( surfacicUnitPlot: SurfacicUnitPlotDto ) =>
            Promise.resolve( this._surfacicUnitPlotTransformer.dtoToObject( surfacicUnitPlot ) ),
        ),
        from.outExperimentationZones.map( ( outExperimentationZone: OutExperimentationZoneDto ) => outExperimentationZone['@id'] ),
        () => from.outExperimentationZones.map( ( outExperimentationZone: OutExperimentationZoneDto ) =>
            Promise.resolve( this._outExperimentationZoneFullTransformer.dtoToObject( outExperimentationZone ) ),
        ),
        from.notes,
        () => {
          const promise = this.httpService.getAll<NoteDto>( `${from['@id']}/notes` )
          return from.notes.map( ( uri, index ) =>
              promise.then( response => this._noteTransformer.dtoToObject( response.data['hydra:member'][index] ) ),
          )
        },
        from.color,
        from.comment,
        from.openSilexUri,
        from.geometry,
    )
  }

  objectToFormInterface( from: SubBlock ): Promise<any> {
    return undefined
  }

  formInterfaceToDto( from: any ): SubBlockDto {
    return undefined
  }

  set unitPlotTransformer( value: UnitPlotFullTransformer ) {
    this._unitPlotTransformer = value
  }

  set surfacicUnitPlotTransformer( value: SurfacicUnitPlotFullTransformer ) {
    this._surfacicUnitPlotTransformer = value
  }

  set noteTransformer( value: NoteTransformer ) {
    this._noteTransformer = value
  }

  set outExperimentationZoneFullTransformer( value: OutExperimentationZoneTransformer ) {
    this._outExperimentationZoneFullTransformer = value
  }
}
