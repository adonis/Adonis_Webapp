import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import TreatmentFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/treatment-full-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'

export default class SurfacicUnitPlotFullTransformer extends AbstractTransformer<SurfacicUnitPlotDto, SurfacicUnitPlot, any> {

  private _treatmentTransformer: TreatmentFullTransformer
  private _noteTransformer: NoteTransformer

  dtoToObject( from: SurfacicUnitPlotDto ): SurfacicUnitPlot {
    return new SurfacicUnitPlot(
        from['@id'],
        from.id,
        from.number,
        from.x,
        from.y,
        from.dead,
        new Date( from.appeared ),
        !from.disappeared ? undefined : new Date( from.disappeared ),
        from.identifier,
        from.latitude,
        from.longitude,
        from.height,
        from.treatment as string,
        () => undefined,
        from.notes,
        () => {
          const promise = this.httpService.getAll<NoteDto>( `${from['@id']}/notes` )
          return from.notes.map( ( uri, index ) =>
              promise.then( response => this._noteTransformer.dtoToObject( response.data['hydra:member'][index] ) ),
          )
        },
        from.color,
        from.comment,
        from.openSilexUri,
        from.geometry,
    )
  }

  objectToFormInterface( from: SurfacicUnitPlot ): Promise<any> {
    return undefined
  }

  formInterfaceToDto( from: any ): SurfacicUnitPlotDto {
    return undefined
  }

  set treatmentTransformer( value: TreatmentFullTransformer ) {
    this._treatmentTransformer = value
  }

  set noteTransformer( value: NoteTransformer ) {
    this._noteTransformer = value
  }
}
