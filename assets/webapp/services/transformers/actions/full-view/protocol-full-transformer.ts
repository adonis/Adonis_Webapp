import ProtocolFormInterface from '@adonis/webapp/form-interfaces/protocol-form-interface'
import Protocol from '@adonis/webapp/models/protocol'
import { AlgorithmDto } from '@adonis/webapp/services/http/entities/simple-dto/algorithm-dto'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import AlgorithmTransformer from '@adonis/webapp/services/transformers/actions/algorithm-transformer'
import FactorFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/factor-full-transformer'
import TreatmentFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/treatment-full-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class ProtocolFullTransformer extends AbstractTransformer<ProtocolDto, Protocol, ProtocolFormInterface> {

  private _factorTransformer: FactorFullTransformer
  private _treatmentTransformer: TreatmentFullTransformer
  private _userTransformer: UserTransformer
  private _algorithmTransformer: AlgorithmTransformer

  dtoToObject( from: ProtocolDto ): Protocol {
    return new Protocol(
        from['@id'],
        from.id,
        from.name,
        from.aim,
        from.factors.map( (factor: FactorDto) => factor['@id'] ),
        () => from.factors.map( (factor: FactorDto) => Promise.resolve( this._factorTransformer.dtoToObject( factor ) ) ),
        from.treatments.map( (treatment: TreatmentDto) => treatment['@id'] ),
        () => from.treatments.map( (treatment: TreatmentDto) => Promise.resolve( this._treatmentTransformer.dtoToObject( treatment ) ) ),
        from.comment,
        new Date( from.created ),
        from.owner,
        () => Promise.resolve(undefined),
        !from.algorithm ? null : (from.algorithm as AlgorithmDto)['@id'],
        () => Promise.resolve(!from.algorithm ? null : this._algorithmTransformer.dtoToObject(from.algorithm as AlgorithmDto)),
        [],
        () => undefined,
    )
  }

  objectToFormInterface( from: Protocol ): Promise<ProtocolFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: ProtocolFormInterface ): ProtocolDto {
    return undefined
  }

  set factorTransformer( value: FactorFullTransformer ) {
    this._factorTransformer = value
  }

  set treatmentTransformer( value: TreatmentFullTransformer ) {
    this._treatmentTransformer = value
  }

  set userTransformer( value: UserTransformer ) {
    this._userTransformer = value
  }

  set algorithmTransformer( value: AlgorithmTransformer ) {
    this._algorithmTransformer = value
  }
}
