import { UserDto } from '@adonis/shared/models/dto/user-dto'
import ExperimentFormInterface from '@adonis/webapp/form-interfaces/experiment-form-interface'
import Experiment from '@adonis/webapp/models/experiment'
import { BlockDto } from '@adonis/webapp/services/http/entities/simple-dto/block-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import BlockFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/block-full-transformer'
import ProtocolFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/protocol-full-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import OutExperimentationZoneTransformer from '@adonis/webapp/services/transformers/actions/out-experimentation-zone-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class ExperimentFullTransformer extends AbstractTransformer<ExperimentDto, Experiment, ExperimentFormInterface> {

    private _protocolTransformer: ProtocolFullTransformer
    private _userTransformer: UserTransformer
    private _blockTransformer: BlockFullTransformer
    private _noteTransformer: NoteTransformer
    private _outExperimentationZoneFullTransformer: OutExperimentationZoneTransformer

    dtoToObject(from: ExperimentDto): Experiment {
        return new Experiment(
            from['@id'],
            from.id,
            from.name,
            from.protocol as ProtocolDto['@id'],
            () => Promise.resolve(this._protocolTransformer.dtoToObject(from.protocol as ProtocolDto)),
            from.comment,
            from.created,
            from.individualUP,
            from.owner,
            () => this.httpService.get<UserDto>(from.owner)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.blocks.map((block: BlockDto) => block['@id']),
            () => from.blocks.map((block: BlockDto) => Promise.resolve(this._blockTransformer.dtoToObject(block))),
            from.outExperimentationZones.map((outExperimentationZone: OutExperimentationZoneDto) => outExperimentationZone['@id']),
            () => from.outExperimentationZones.map((outExperimentationZone: OutExperimentationZoneDto) =>
                Promise.resolve(this._outExperimentationZoneFullTransformer.dtoToObject(outExperimentationZone)),
            ),
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.state,
            undefined,
            () => undefined,
            from.openSilexUri,
            from.geometry,
        )
    }

    objectToFormInterface(from: Experiment): Promise<ExperimentFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: ExperimentFormInterface): ExperimentDto {
        return undefined
    }

    set protocolTransformer(value: ProtocolFullTransformer) {
        this._protocolTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set blockTransformer(value: BlockFullTransformer) {
        this._blockTransformer = value
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }

    set outExperimentationZoneFullTransformer(value: OutExperimentationZoneTransformer) {
        this._outExperimentationZoneFullTransformer = value
    }
}
