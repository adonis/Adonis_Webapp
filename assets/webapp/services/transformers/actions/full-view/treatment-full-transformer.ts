import TreatmentFormInterface from '@adonis/webapp/form-interfaces/treatment-form-interface'
import Treatment from '@adonis/webapp/models/treatment'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ModalityTransformer from '@adonis/webapp/services/transformers/actions/modality-transformer'

export default class TreatmentFullTransformer extends AbstractTransformer<TreatmentDto, Treatment, TreatmentFormInterface> {

  private _modalityTransformer: ModalityTransformer

  dtoToObject( from: TreatmentDto ): Treatment {
    return new Treatment(
        from['@id'],
        from.id,
        from.name,
        from.shortName,
        from.repetitions,
        from.modalities.map( (modality: ModalityDto) => modality['@id'] ),
        () => from.modalities.map( (modality: ModalityDto) => Promise.resolve( this._modalityTransformer.dtoToObject( modality ) ) ),
    )
  }

  objectToFormInterface( from: Treatment ): Promise<TreatmentFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: TreatmentFormInterface ): TreatmentDto {
    return undefined
  }

  set modalityTransformer( value: ModalityTransformer ) {
    this._modalityTransformer = value
  }
}
