import ExperimentFormInterface from '@adonis/webapp/form-interfaces/experiment-form-interface'
import Block from '@adonis/webapp/models/block'
import { BlockDto } from '@adonis/webapp/services/http/entities/simple-dto/block-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { SubBlockDto } from '@adonis/webapp/services/http/entities/simple-dto/sub-block-dto'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import SubBlockFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/sub-block-full-transformer'
import SurfacicUnitPlotFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/surfacic-unit-plot-full-transformer'
import UnitPlotFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/unit-plot-full-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import OutExperimentationZoneTransformer from '@adonis/webapp/services/transformers/actions/out-experimentation-zone-transformer'

export default class BlockFullTransformer extends AbstractTransformer<BlockDto, Block, any> {

  private _unitPlotTransformer: UnitPlotFullTransformer
  private _surfacicUnitPlotTransformer: SurfacicUnitPlotFullTransformer
  private _subBlockTransformer: SubBlockFullTransformer
  private _noteTransformer: NoteTransformer
  private _outExperimentationZoneFullTransformer: OutExperimentationZoneTransformer

  dtoToObject( from: BlockDto ): Block {
    return new Block(
        from['@id'],
        from.id,
        from.number,
        from.unitPlots.map( ( unitPlot: UnitPlotDto ) => unitPlot['@id'] ),
        () => from.unitPlots.map( ( unitPlot: UnitPlotDto ) =>
            Promise.resolve( this._unitPlotTransformer.dtoToObject( unitPlot ) ),
        ),
        from.surfacicUnitPlots.map( ( surfacicUnitPlot: SurfacicUnitPlotDto ) => surfacicUnitPlot['@id'] ),
        () => from.surfacicUnitPlots.map( ( surfacicUnitPlot: SurfacicUnitPlotDto ) =>
            Promise.resolve( this._surfacicUnitPlotTransformer.dtoToObject( surfacicUnitPlot ) ),
        ),
        from.subBlocks.map( ( subBlock: SubBlockDto ) => subBlock['@id'] ),
        () => from.subBlocks.map( ( subBlock: SubBlockDto ) =>
            Promise.resolve( this._subBlockTransformer.dtoToObject( subBlock ) ),
        ),
        from.outExperimentationZones.map( ( outExperimentationZone: OutExperimentationZoneDto ) => outExperimentationZone['@id'] ),
        () => from.outExperimentationZones.map( ( outExperimentationZone: OutExperimentationZoneDto ) =>
            Promise.resolve( this._outExperimentationZoneFullTransformer.dtoToObject( outExperimentationZone ) ),
        ),
        from.notes,
        () => {
          const promise = this.httpService.getAll<NoteDto>( `${from['@id']}/notes` )
          return from.notes.map( ( uri, index ) =>
              promise.then( response => this._noteTransformer.dtoToObject( response.data['hydra:member'][index] ) ),
          )
        },
        from.color,
        from.comment,
        from.openSilexUri,
        from.geometry,
    )
  }

  objectToFormInterface( from: Block ): Promise<any> {
    return undefined
  }

  formInterfaceToDto( from: ExperimentFormInterface ): BlockDto {
    return undefined
  }

  set unitPlotTransformer( value: UnitPlotFullTransformer ) {
    this._unitPlotTransformer = value
  }

  set surfacicUnitPlotTransformer( value: SurfacicUnitPlotFullTransformer ) {
    this._surfacicUnitPlotTransformer = value
  }

  set subBlockTransformer( value: SubBlockFullTransformer ) {
    this._subBlockTransformer = value
  }

  set noteTransformer( value: NoteTransformer ) {
    this._noteTransformer = value
  }

  set outExperimentationZoneFullTransformer( value: OutExperimentationZoneTransformer ) {
    this._outExperimentationZoneFullTransformer = value
  }
}
