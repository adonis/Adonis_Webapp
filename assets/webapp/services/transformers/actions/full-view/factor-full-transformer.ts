import FactorFormInterface from '@adonis/webapp/form-interfaces/factor-form-interface'
import Factor from '@adonis/webapp/models/factor'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ModalityTransformer from '@adonis/webapp/services/transformers/actions/modality-transformer'

export default class FactorFullTransformer extends AbstractTransformer<FactorDto, Factor, FactorFormInterface> {

  private _modalityTransformer: ModalityTransformer

  dtoToObject( from: FactorDto ): Factor {
    return new Factor(
        from['@id'],
        from.id,
        from.name,
        from.modalities.map( (modality: ModalityDto) => modality['@id'] ),
        () => from.modalities.map( (modality: ModalityDto) => Promise.resolve( this._modalityTransformer.dtoToObject( modality ) ) ),
        from.order,
        from.openSilexUri,
        from.openSilexInstance,
        () => Promise.resolve(undefined),
        from.germplasm,
    )
  }

  objectToFormInterface( from: Factor ): Promise<FactorFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: FactorFormInterface ): FactorDto {
    return undefined
  }

  set modalityTransformer( value: ModalityTransformer ) {
    this._modalityTransformer = value
  }
}
