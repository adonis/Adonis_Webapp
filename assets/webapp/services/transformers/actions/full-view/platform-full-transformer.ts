import { UserDto } from '@adonis/shared/models/dto/user-dto'
import PlatformFormInterface from '@adonis/webapp/form-interfaces/platform-form-interface'
import Platform from '@adonis/webapp/models/platform'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { GraphicalTextZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-text-zone-dto'
import { NorthIndicatorDto } from '@adonis/webapp/services/http/entities/simple-dto/north-indicator-dto'
import { PlatformDto } from '@adonis/webapp/services/http/entities/simple-dto/platform-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ExperimentFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/experiment-full-transformer'
import GraphicalTextZoneTransformer from '@adonis/webapp/services/transformers/actions/graphical-text-zone-transformer'
import NorthIndicatorTransformer from '@adonis/webapp/services/transformers/actions/north-indicator-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class PlatformFullTransformer extends AbstractTransformer<PlatformDto, Platform, PlatformFormInterface> {

    private _experimentTransformer: ExperimentFullTransformer
    private _userTransformer: UserTransformer
    private _northIndicatorTransformer: NorthIndicatorTransformer
    private _graphicalTextZoneTransformer: GraphicalTextZoneTransformer

    dtoToObject(from: PlatformDto): Platform {
        return new Platform(
            from['@id'],
            from.id,
            from.name,
            from.siteName,
            from.placeName,
            from.experiments.map((experiment: ExperimentDto) => experiment['@id']),
            () => from.experiments.map((experiment: ExperimentDto) => Promise.resolve(this._experimentTransformer.dtoToObject(experiment))),
            [],
            () => [],
            from.comment,
            from.created,
            from.owner,
            () => this.httpService.get<UserDto>(from.owner)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.color,
            from.xMesh,
            from.yMesh,
            from.origin,
            [],
            () => undefined,
            (from.northIndicator as NorthIndicatorDto)?.['@id'],
            () => Promise.resolve(!from.northIndicator ?
                null :
                this._northIndicatorTransformer.dtoToObject(from.northIndicator as NorthIndicatorDto)),
            from.graphicalTextZones.map((textZone: GraphicalTextZoneDto) => textZone['@id']),
            () => from.graphicalTextZones.map((textZone: GraphicalTextZoneDto) => Promise.resolve(this._graphicalTextZoneTransformer.dtoToObject(textZone))),
        )
    }

    objectToFormInterface(from: Platform): Promise<PlatformFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: PlatformFormInterface): PlatformDto {
        return undefined
    }

    set experimentTransformer(value: ExperimentFullTransformer) {
        this._experimentTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set northIndicatorTransformer(value: NorthIndicatorTransformer) {
        this._northIndicatorTransformer = value
    }

    set graphicalTextZoneTransformer(value: GraphicalTextZoneTransformer) {
        this._graphicalTextZoneTransformer = value
    }
}
