import UnitPlot from '@adonis/webapp/models/unit-plot'
import { IndividualDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import TreatmentFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/treatment-full-transformer'
import IndividualTransformer from '@adonis/webapp/services/transformers/actions/individual-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import OutExperimentationZoneTransformer from '@adonis/webapp/services/transformers/actions/out-experimentation-zone-transformer'

export default class UnitPlotFullTransformer extends AbstractTransformer<UnitPlotDto, UnitPlot, any> {

  private _individualTransformer: IndividualTransformer
  private _treatmentTransformer: TreatmentFullTransformer
  private _noteTransformer: NoteTransformer
  private _outExperimentationZoneFullTransformer: OutExperimentationZoneTransformer

  dtoToObject( from: UnitPlotDto ): UnitPlot {
    return new UnitPlot(
        from['@id'],
        from.id,
        from.number,
        from.individuals.map( ( individual: IndividualDto ) => individual['@id'] ),
        () => from.individuals.map( ( individual: IndividualDto ) =>
            Promise.resolve( this._individualTransformer.dtoToObject( individual ) ),
        ),
        from.outExperimentationZones.map( ( outExperimentationZone: OutExperimentationZoneDto ) => outExperimentationZone['@id'] ),
        () => from.outExperimentationZones.map( ( outExperimentationZone: OutExperimentationZoneDto ) =>
            Promise.resolve( this._outExperimentationZoneFullTransformer.dtoToObject( outExperimentationZone ) ),
        ),
        from.treatment as string,
        () => undefined,
        from.notes,
        () => {
          const promise = this.httpService.getAll<NoteDto>( `${from['@id']}/notes` )
          return from.notes.map( ( uri, index ) =>
              promise.then( response => this._noteTransformer.dtoToObject( response.data['hydra:member'][index] ) ),
          )
        },
        from.color,
        from.comment,
        from.openSilexUri,
        from.geometry,
    )
  }

  objectToFormInterface( from: UnitPlot ): Promise<any> {
    return undefined
  }

  formInterfaceToDto( from: any ): UnitPlotDto {
    return undefined
  }

  set individualTransformer( value: IndividualTransformer ) {
    this._individualTransformer = value
  }

  set treatmentTransformer( value: TreatmentFullTransformer ) {
    this._treatmentTransformer = value
  }

  set noteTransformer( value: NoteTransformer ) {
    this._noteTransformer = value
  }

  set outExperimentationZoneFullTransformer( value: OutExperimentationZoneTransformer ) {
    this._outExperimentationZoneFullTransformer = value
  }
}
