import Algorithm from '@adonis/webapp/models/algorithm'
import { AlgorithmDto } from '@adonis/webapp/services/http/entities/simple-dto/algorithm-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AlgorithmTransformer extends AbstractTransformer<AlgorithmDto, Algorithm, void> {

  dtoToObject( from: AlgorithmDto ): Algorithm {
    return new Algorithm(
        from['@id'],
        from.name,
        from.algorithmParameters,
        from.algorithmConditions,
        from.withSubBlock,
        from.withRepartition,
    )
  }

  objectToFormInterface( from: Algorithm ): Promise<void> {
    return undefined
  }

  formInterfaceToDto( from: void ): AlgorithmDto {
    return undefined
  }
}
