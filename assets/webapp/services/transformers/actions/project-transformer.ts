import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import ProjectFormInterface from '@adonis/webapp/form-interfaces/project-form-interface'
import Project from '@adonis/webapp/models/project'
import { DeviceDto } from '@adonis/webapp/services/http/entities/simple-dto/device-dto'
import { ExperimentDto } from '@adonis/webapp/services/http/entities/simple-dto/experiment-dto'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { PathBaseDto } from '@adonis/webapp/services/http/entities/simple-dto/path-base-dto'
import { PlatformDto } from '@adonis/webapp/services/http/entities/simple-dto/platform-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { StateCodeDto } from '@adonis/webapp/services/http/entities/simple-dto/state-code-dto'
import { StatusDataEntryDto } from '@adonis/webapp/services/http/entities/simple-dto/status-data-entry-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import DeviceTransformer from '@adonis/webapp/services/transformers/actions/device-transformer'
import ExperimentTransformer from '@adonis/webapp/services/transformers/actions/experiment-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import PathBaseTransformer from '@adonis/webapp/services/transformers/actions/path-base-transformer'
import PlatformTransformer from '@adonis/webapp/services/transformers/actions/platform-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'
import StateCodeTransformer from '@adonis/webapp/services/transformers/actions/state-code-transformer'
import StatusDataEntryTransformer from '@adonis/webapp/services/transformers/actions/status-data-entry-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class ProjectTransformer extends AbstractTransformer<ProjectDto, Project, ProjectFormInterface> {

    private _experimentTransformer: ExperimentTransformer
    private _simpleVariableTransformer: SimpleVariableTransformer
    private _generatorVariableTransformer: GeneratorVariableTransformer
    private _deviceTransformer: DeviceTransformer
    private _userTransformer: UserTransformer
    private _platformTransformer: PlatformTransformer
    private _pathBaseTransformer: PathBaseTransformer
    private _stateCodeTransformer: StateCodeTransformer
    private _statusDataEntryTransformer: StatusDataEntryTransformer

    dtoToObject(from: ProjectDto): Project {
        return new Project(
            from['@id'],
            from.id,
            from.name,
            from.platform,
            () => this.httpService.get<PlatformDto>(from.platform)
                .then(response => this._platformTransformer.dtoToObject(response.data)),
            from.experiments,
            () => from.experiments.map(experiment => this.httpService.get<ExperimentDto>(experiment)
                .then(response => this._experimentTransformer.dtoToObject(response.data))),
            from.simpleVariables,
            () => {
                const promise = this.httpService.getAll<SimpleVariableDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE),
                    {pagination: false, project: from['@id']},
                )
                return from.simpleVariables.map((uri, index) => {
                    return promise
                        .then(response => this._simpleVariableTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.generatorVariables,
            () => {
                const promise = this.httpService.getAll<GeneratorVariableDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE),
                    {pagination: false, project: from['@id']},
                )
                return from.generatorVariables.map((uri, index) => {
                    return promise
                        .then(response => this._generatorVariableTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.devices,
            () => {
                const promise = this.httpService.getAll<DeviceDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.DEVICE),
                    {pagination: false, project: from['@id']})
                return from.devices.map((uri, index) => {
                    return promise
                        .then(response => this._deviceTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.comment,
            from.created,
            from.owner as string,
            () => this.httpService.get<UserDto>(from.owner as string)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.pathBase,
            () => this.httpService.get<PathBaseDto>(from.pathBase)
                .then(response => this._pathBaseTransformer.dtoToObject(response.data)),
            from.projectDatas,
            () => {
                const promise = this.httpService.getAll<StateCodeDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.STATE_CODE),
                    {pagination: false, project: from['@id']})
                return from.stateCodes.map((uri, index) => {
                    return promise
                        .then(response => this._stateCodeTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.stateCodes,
            () => {
                const promise = this.httpService.getAll<StatusDataEntryDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.STATUS_DATA_ENTRY),
                    {pagination: false, webappProject: from['@id']})
                return from.statusDataEntries.map((uri, index) => {
                    return promise
                        .then(response => this._statusDataEntryTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.statusDataEntries,
        )
    }

    objectToFormInterface(from: Project): Promise<ProjectFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: ProjectFormInterface): ProjectDto {
        return {
            name: from.name,
            platform: from.platform.iri,
            experiments: from.experiments.map(experiment => experiment.iri),
            comment: from.comment,
        }
    }

    set experimentTransformer(value: ExperimentTransformer) {
        this._experimentTransformer = value
    }

    set simpleVariableTransformer(value: SimpleVariableTransformer) {
        this._simpleVariableTransformer = value
    }

    set generatorVariableTransformer(value: GeneratorVariableTransformer) {
        this._generatorVariableTransformer = value
    }

    set deviceTransformer(value: DeviceTransformer) {
        this._deviceTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set platformTransformer(value: PlatformTransformer) {
        this._platformTransformer = value
    }

    set pathBaseTransformer(value: PathBaseTransformer) {
        this._pathBaseTransformer = value
    }

    set stateCodeTransformer(value: StateCodeTransformer) {
        this._stateCodeTransformer = value
    }

    set statusDataEntryTransformer(value: StatusDataEntryTransformer) {
        this._statusDataEntryTransformer = value
    }
}
