import GraphicalTextZone from '@adonis/webapp/models/graphical-text-zone'
import { GraphicalTextZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/graphical-text-zone-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class GraphicalTextZoneTransformer extends AbstractTransformer<GraphicalTextZoneDto, GraphicalTextZone, any> {

  dtoToObject( from: GraphicalTextZoneDto ): GraphicalTextZone {
    return new GraphicalTextZone(
        from['@id'],
        from.id,
        from.text,
        from.color,
        from.x,
        from.y,
        from.width,
        from.height,
        from.size,
    )
  }

  objectToFormInterface( from: GraphicalTextZone ): Promise<any> {
    return undefined
  }

  formInterfaceToDto( from: any ): GraphicalTextZoneDto {
    return undefined
  }
}
