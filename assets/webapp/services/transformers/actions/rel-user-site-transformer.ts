import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'
import { SiteDto } from '@adonis/shared/models/dto/site-dto'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import RelUserSite from '@adonis/shared/models/rel-user-site'
import RoleUserSiteFormInterface from '@adonis/webapp/form-interfaces/role-user-site-form-interface'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import SiteTransformer from '@adonis/webapp/services/transformers/actions/site-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class RelUserSiteTransformer extends AbstractTransformer<RelUserSiteDto, RelUserSite, RoleUserSiteFormInterface> {

    private _siteTransformer: SiteTransformer
    private _userTransformer: UserTransformer

    dtoToObject(from: RelUserSiteDto): RelUserSite {
        return new RelUserSite(
            from['@id'],
            from.site,
            () => this.httpService.get<SiteDto>(from.site)
                .then(response => this._siteTransformer.dtoToObject(response.data)),
            from.user,
            () => this.httpService.get<UserDto>(from.user)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.role,
        )
    }

    objectToFormInterface(from: RelUserSite): Promise<RoleUserSiteFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: RoleUserSiteFormInterface): RelUserSiteDto {
        return {
            site: from.site.iri,
            user: from.user.iri,
            role: from.role,
        }
    }

    formInterfaceToUpdateDto(from: RoleUserSiteFormInterface): RelUserSiteDto {
        return {
            role: from.role,
        }
    }

    set siteTransformer(value: SiteTransformer) {
        this._siteTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }
}
