import UnitPlot from '@adonis/webapp/models/unit-plot'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import { UnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/unit-plot-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import TreatmentTransformer from '@adonis/webapp/services/transformers/actions/treatment-transformer'

export default class UnitPlotTransformer extends AbstractTransformer<UnitPlotDto, UnitPlot, any> {

    private _noteTransformer: NoteTransformer
    private _treatmentTransformer: TreatmentTransformer

    dtoToObject(from: UnitPlotDto): UnitPlot {
        return new UnitPlot(
            from['@id'],
            from.id,
            from.number,
            from.individuals as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.outExperimentationZones as string[],
            () => {
                throw new Error('Not Yet Implemented')
            },
            from.treatment as string,
            () => this.httpService.get<TreatmentDto>(from.treatment as string)
                .then(response => this._treatmentTransformer.dtoToObject(response.data)),
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.comment,
            from.openSilexUri,
            from.geometry,
        )
    }

    objectToFormInterface(from: UnitPlot): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: any): UnitPlotDto {
        return undefined
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }

    set treatmentTransformer(value: TreatmentTransformer) {
        this._treatmentTransformer = value
    }
}
