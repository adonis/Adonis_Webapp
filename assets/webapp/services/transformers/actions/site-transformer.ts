import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'
import { SiteDto } from '@adonis/shared/models/dto/site-dto'
import Site from '@adonis/shared/models/site'
import SiteFormInterface from '@adonis/webapp/form-interfaces/site-form-interface'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import RelUserSiteTransformer from '@adonis/webapp/services/transformers/actions/rel-user-site-transformer'

export default class SiteTransformer extends AbstractTransformer<SiteDto, Site, SiteFormInterface> {

    private _relUserSiteTransformer: RelUserSiteTransformer

    dtoToObject(from: SiteDto): Site {
        return new Site(
            from['@id'],
            from.id,
            from.label,
            from.userRoles as string[],
            () => {
                const promise = this.httpService.getAll<RelUserSiteDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.ROLE_USER_SITE),
                    {pagination: false, site: from['@id']})
                return (from.userRoles as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._relUserSiteTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.platforms,
        )
    }

    objectToFormInterface(from: Site): Promise<SiteFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: SiteFormInterface): SiteDto {
        return {
            label: from.name,
        }
    }

    set relUserSiteTransformer(value: RelUserSiteTransformer) {
        this._relUserSiteTransformer = value
    }
}
