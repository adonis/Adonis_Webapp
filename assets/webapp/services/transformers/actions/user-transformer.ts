import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import User from '@adonis/shared/models/user'
import UserFormInterface from '@adonis/webapp/form-interfaces/user-form-interface'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import RelUserSiteTransformer from '@adonis/webapp/services/transformers/actions/rel-user-site-transformer'

export default class UserTransformer extends AbstractTransformer<UserDto, User, UserFormInterface> {

    private _relUserSiteTransformer: RelUserSiteTransformer

    dtoToObject(from: UserDto): User {
        return new User(
            from['@id'],
            from.id,
            from.username,
            from.name,
            from.surname,
            from.email,
            from.siteRoles,
            () => {
                const promise = this.httpService.getAll<RelUserSiteDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.ROLE_USER_SITE),
                    {pagination: false, user: from['@id']})
                return from.siteRoles.map((uri, index) => {
                    return promise
                        .then(response => this._relUserSiteTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.active,
            from.roles,
            from.avatar,
        )
    }

    objectToFormInterface(from: User): Promise<UserFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: UserFormInterface): UserDto {
        return {
            name: from.name,
            email: from.email,
            username: from.login,
            surname: from.surname,
        }
    }

    set relUserSiteTransformer(value: RelUserSiteTransformer) {
        this._relUserSiteTransformer = value
    }
}
