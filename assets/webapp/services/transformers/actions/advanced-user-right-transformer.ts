import AdvancedUserRight from '@adonis/webapp/models/advanced-user-right'
import { AdvancedUserRightDto } from '@adonis/webapp/services/http/entities/simple-dto/advanced-user-right-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AdvancedUserRightTransformer extends AbstractTransformer<AdvancedUserRightDto, AdvancedUserRight, void> {

  dtoToObject( from: AdvancedUserRightDto ): AdvancedUserRight {
    return new AdvancedUserRight(
        from['@id'],
        from.userId,
        from.objectId,
        from.classIdentifier,
        from.right,
    )
  }

  objectToFormInterface( from: AdvancedUserRight ): Promise<void> {
    return undefined
  }

  formInterfaceToDto( from: void ): AdvancedUserRightDto {
    return undefined
  }
}
