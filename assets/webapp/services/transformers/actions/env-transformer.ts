import ModalityFormInterface from '@adonis/webapp/form-interfaces/modality-form-interface'
import Env from '@adonis/webapp/models/env'
import Modality from '@adonis/webapp/models/modality'
import { EnvDto } from '@adonis/webapp/services/http/entities/simple-dto/env-dto'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import FactorTransformer from '@adonis/webapp/services/transformers/actions/factor-transformer'

export default class EnvTransformer extends AbstractTransformer<EnvDto, Env, void> {

  dtoToObject( from: EnvDto ): Env {
    return new Env(
        from['@id'],
        from.id,
        from.ldapOn,
    )
  }

  objectToFormInterface( from: Env ): Promise<void> {
    return undefined
  }

  formInterfaceToDto( from: void ): EnvDto {
    return undefined
  }
}
