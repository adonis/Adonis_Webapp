import CloneProjectFormInterface from '@adonis/webapp/form-interfaces/clone-project-form-interface'
import { CloneProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/clone-project-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class CloneProjectTransformer extends AbstractTransformer<CloneProjectDto, any, CloneProjectFormInterface> {

  dtoToObject( from: CloneProjectDto ): any {
    return undefined
  }

  objectToFormInterface( from: any ): Promise<CloneProjectFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: CloneProjectFormInterface ): CloneProjectDto {
    return {
      newName: from.newName,
      project: from.projectIri,
    }
  }
}
