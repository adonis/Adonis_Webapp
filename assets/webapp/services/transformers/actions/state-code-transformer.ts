import SpreadingEnum from '@adonis/webapp/constants/spreading-enum'
import StateCodeFormInterface from '@adonis/webapp/form-interfaces/state-code-form-interface'
import StateCode from '@adonis/webapp/models/state-code'
import { StateCodeDto } from '@adonis/webapp/services/http/entities/simple-dto/state-code-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class StateCodeTransformer extends AbstractTransformer<StateCodeDto, StateCode, StateCodeFormInterface> {

  dtoToObject( from: StateCodeDto ): StateCode {

    return new StateCode(
        from['@id'],
        from.id,
        from.code,
        from.title,
        from.meaning,
        from.spreading,
        from.color,
        from.permanent,
    )
  }

  objectToFormInterface( from: StateCode ): Promise<StateCodeFormInterface> {

    return Promise.resolve( {
      code: from.code,
      title: from.title,
      meaning: from.meaning,
      spreading: from.spreading as SpreadingEnum,
      color: from.color,
    } )
  }

  formInterfaceToDto( from: StateCodeFormInterface ): StateCodeDto {
    return {
      title: from.title,
      code: from.code,
      meaning: from.meaning,
      spreading: from.spreading,
      color: from.color,
    }
  }

}
