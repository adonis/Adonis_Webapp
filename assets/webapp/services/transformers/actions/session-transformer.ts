import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import SessionFormInterface from '@adonis/webapp/form-interfaces/session-form-interface'
import Annotation from '@adonis/webapp/models/annotation'
import Session from '@adonis/webapp/models/session'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'
import { ExperimentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/experiment-data-view-dto'
import { IndividualDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/individual-data-view-dto'
import { PlatformDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/platform-data-view-dto'
import { SubBlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/sub-block-data-view-dto'
import { SurfacicUnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/surfacic-unit-plot-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { AnnotationDto } from '@adonis/webapp/services/http/entities/simple-dto/annotation-dto'
import { SessionDto } from '@adonis/webapp/services/http/entities/simple-dto/session-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import AnnotationTransformer from '@adonis/webapp/services/transformers/actions/annotation-transformer'
import FieldMeasureTransformer from '@adonis/webapp/services/transformers/actions/field-measure-transformer'

export default class SessionTransformer extends AbstractTransformer<SessionDto, Session, SessionFormInterface> {

    private _fieldMeasureTransformer: FieldMeasureTransformer
    private _annotationTransformer: AnnotationTransformer

    dtoToObject(from: SessionDto): Session {

        return new Session(
            from['@id'],
            from.id,
            new Date(from.startedAt),
            new Date(from.endedAt),
            from.comment,
            from.fieldMeasures.map(field => field['@id']),
            () => from.fieldMeasures.map(field => Promise.resolve(this._fieldMeasureTransformer.dtoToObject(field))),
            from.annotations.map(annotation => annotation['@id']),
            () => from.annotations.map(annotation => Promise.resolve(!!annotation.targetType ?
                this.transformObjectAnnotation(annotation) :
                this._annotationTransformer.dtoToObject(annotation))
            ),
            from.userName,
        )
    }

    private transformObjectAnnotation(annotation: AnnotationDto): Annotation {
        return new Annotation(
            annotation['@id'],
            annotation.id,
            annotation.categories,
            annotation.image,
            annotation.keywords,
            annotation.name,
            this._fieldMeasureTransformer.transformTarget(
                annotation.target as BlockDataViewDto | ExperimentDataViewDto | IndividualDataViewDto | SubBlockDataViewDto | SurfacicUnitPlotDataViewDto | UnitPlotDataViewDto | PlatformDataViewDto,
                annotation.targetType) ?? (annotation.target as AbstractDtoObject)['@id'],
            annotation.targetType,
            new Date(annotation.timestamp),
            annotation.type,
            annotation.value,
        )
    }

    formInterfaceToDto(from: SessionFormInterface): SessionDto {

        return undefined
    }

    objectToFormInterface(from: Session): Promise<SessionFormInterface> {

        return Promise.resolve(undefined)
    }

    set fieldMeasureTransformer(fieldMeasureTransformer: FieldMeasureTransformer) {
        this._fieldMeasureTransformer = fieldMeasureTransformer
    }

    set annotationTransformer(value: AnnotationTransformer) {
        this._annotationTransformer = value
    }
}
