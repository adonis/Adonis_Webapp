import { SiteDto } from '@adonis/shared/models/dto/site-dto'
import OpenSilexInstanceFormInterface from '@adonis/webapp/form-interfaces/open-silex-instance-form-interface'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import SiteTransformer from '@adonis/webapp/services/transformers/actions/site-transformer'

export default class OpenSilexInstanceTransformer extends AbstractTransformer<OpenSilexInstanceDto, OpenSilexInstance, OpenSilexInstanceFormInterface> {

    private _siteTransformer: SiteTransformer

    dtoToObject(from: OpenSilexInstanceDto): OpenSilexInstance {
        return new OpenSilexInstance(
            from['@id'],
            from.url,
            from.site,
            () => this.httpService.get<SiteDto>(from.site)
                .then(response => this._siteTransformer.dtoToObject(response.data)),
        )
    }

    objectToFormInterface(from: OpenSilexInstance): Promise<OpenSilexInstanceFormInterface> {
        return from.site.then(site => ({
            url: from.url,
            site,
        }))
    }

    formInterfaceToDto(from: OpenSilexInstanceFormInterface): OpenSilexInstanceDto {
        return {
            site: from.site?.iri,
            url: from.url,
        }
    }

    set siteTransformer(value: SiteTransformer) {
        this._siteTransformer = value
    }
}
