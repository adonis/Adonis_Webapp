import ProjectData from '@adonis/webapp/models/project-data'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SessionDto } from '@adonis/webapp/services/http/entities/simple-dto/session-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SessionTransformer from '@adonis/webapp/services/transformers/actions/session-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'

export default class ProjectDataDataViewTransformer extends AbstractTransformer<ProjectDataDto, ProjectData, any> {

  private _sessionTransformer: SessionTransformer

  private _projectTransformer: ProjectTransformer

  private _simpleVariableTransformer: SimpleVariableTransformer

  private _generatorVariableTransformer: GeneratorVariableTransformer

  private _semiAutomaticVariableTransformer: SemiAutomaticVariableTransformer

  dtoToObject( from: ProjectDataDto ): ProjectData {

    return new ProjectData(
        from['@id'],
        from.id,
        from.name,
        new Date( from.start ),
        new Date( from.end ),
        from.userName,
        from.comment,
        undefined,
        (from.sessions as SessionDto[]).map( session => session['@id'] ),
        () => from.sessions.map( session => Promise.resolve( this._sessionTransformer.dtoToObject( session as SessionDto ) ) ),
        (from.project as ProjectDto)['@id'],
        () => this.httpService.get<ProjectDto>( (from.project as ProjectDto)['@id'] )
            .then( response => this._projectTransformer.dtoToObject( response.data ) ),
        (from.simpleVariables as SimpleVariableDto[]).map( simpleVariable => simpleVariable['@id'] ),
        () => from.simpleVariables.map( simpleVariable => Promise.resolve( this._simpleVariableTransformer.dtoToObject( simpleVariable as SimpleVariableDto ) ) ),
        (from.generatorVariables as GeneratorVariableDto[]).map( generatorVariable => generatorVariable['@id'] ),
        () => from.generatorVariables.map( generatorVariable => Promise.resolve( this._generatorVariableTransformer.dtoToObject( generatorVariable as GeneratorVariableDto ) ) ),
        (from.semiAutomaticVariables as SemiAutomaticVariableDto[]).map( variable => variable['@id'] ),
        () => from.semiAutomaticVariables.map( variable => Promise.resolve( this._semiAutomaticVariableTransformer.dtoToObject( variable as SemiAutomaticVariableDto ) ) ),
    )
  }

  formInterfaceToDto( from: any ): ProjectDataDto {
    return undefined
  }

  objectToFormInterface( from: ProjectData ): Promise<any> {
    return Promise.resolve( undefined )
  }

  set sessionTransformer( sessionTransformer: SessionTransformer ) {
    this._sessionTransformer = sessionTransformer
  }

  set projectTransformer( value: ProjectTransformer ) {
    this._projectTransformer = value
  }

  set simpleVariableTransformer( value: SimpleVariableTransformer ) {
    this._simpleVariableTransformer = value
  }

  set generatorVariableTransformer( value: GeneratorVariableTransformer ) {
    this._generatorVariableTransformer = value
  }

  set semiAutomaticVariableTransformer( value: SemiAutomaticVariableTransformer ) {
    this._semiAutomaticVariableTransformer = value
  }
}
