import FactorDataView from '@adonis/webapp/models/data-view/factor-data-view'
import ModalityDataView from '@adonis/webapp/models/data-view/modality-data-view'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import { ViewDataItem } from '@adonis/webapp/models/data-view/view-data-item'
import ProjectData from '@adonis/webapp/models/project-data'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { ViewDataItemDto } from '@adonis/webapp/services/http/entities/data-view-dto/view-data-item-dto'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { ProjectDataDto } from '@adonis/webapp/services/http/entities/simple-dto/project-data-dto'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SessionDto } from '@adonis/webapp/services/http/entities/simple-dto/session-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import FieldMeasureTransformer from '@adonis/webapp/services/transformers/actions/field-measure-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SessionTransformer from '@adonis/webapp/services/transformers/actions/session-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'

export default class ViewDataItemTransformer extends AbstractTransformer<ViewDataItemDto, ViewDataItem, any> {

  private _fieldMeasureTransformer: FieldMeasureTransformer

  dtoToObject( from: ViewDataItemDto ): ViewDataItem {

    return new ViewDataItem(
        from['@id'],
        from.id,
        from.code,
        this._fieldMeasureTransformer.dtoToObject(from.fieldMeasure),
        from.projectData,
        from.session,
        from.repetition,
        from.value,
        from.timestamp,
        from.username,
        from.target,
        from.variable,
        )
  }

  formInterfaceToDto( from: any ): ViewDataItemDto {
    return undefined
  }

  objectToFormInterface( from: ViewDataItem ): Promise<any> {
    return Promise.resolve( undefined )
  }

  set fieldMeasureTransformer(value: FieldMeasureTransformer) {
    this._fieldMeasureTransformer = value
  }
}
