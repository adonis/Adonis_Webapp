import FactorDataView from '@adonis/webapp/models/data-view/factor-data-view'
import ModalityDataView from '@adonis/webapp/models/data-view/modality-data-view'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import { ViewDataBusinessObject } from '@adonis/webapp/models/data-view/view-data-business-object'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'
import { ViewDataBusinessObjectDto } from '@adonis/webapp/services/http/entities/data-view-dto/view-data-business-object-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ViewDataBusinessObjectTransformer extends AbstractTransformer<ViewDataBusinessObjectDto, ViewDataBusinessObject, any> {

    dtoToObject(from: ViewDataBusinessObjectDto): ViewDataBusinessObject {

        return new ViewDataBusinessObject(
            from['@id'],
            from.id,
            from.name,
            from.x,
            from.y,
            from.dead,
            !!from.appeared ? new Date(from.appeared) : null,
            !!from.disappeared ? new Date(from.disappeared) : null,
            from.identifier,
            from.projectData,
            from.session,
            this.transformTreatment(from.treatment),
            from.target,
            from.targetType,
            from.platform,
            from.experiment,
            from.block,
            from.subBlock,
            from.unitPlot,
            from.surfacicUnitPlot,
            from.individual,
        )
    }

    private transformTreatment(treatment: TreatmentDataViewDto): TreatmentDataView {
        return new TreatmentDataView(
            treatment['@id'],
            treatment.id,
            treatment.name,
            treatment.shortName,
            treatment.modalities.map(modality => new ModalityDataView(
                modality['@id'],
                modality.id,
                modality.value,
                modality.shortName,
                new FactorDataView(
                    modality.factor['@id'],
                    modality.factor.id,
                    modality.factor.name,
                    modality.factor.order,
                ),
            )),
        )
    }

    formInterfaceToDto(from: any): ViewDataBusinessObjectDto {
        return undefined
    }

    objectToFormInterface(from: ViewDataBusinessObject): Promise<any> {
        return Promise.resolve(undefined)
    }
}
