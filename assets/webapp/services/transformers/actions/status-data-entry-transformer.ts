import { UserDto } from '@adonis/shared/models/dto/user-dto'
import StatusDataEntry from '@adonis/webapp/models/status-data-entry'
import { ProjectDto } from '@adonis/webapp/services/http/entities/simple-dto/project-dto'
import { StatusDataEntryDto } from '@adonis/webapp/services/http/entities/simple-dto/status-data-entry-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import MobileResponseTransformer from '@adonis/webapp/services/transformers/actions/mobile-response-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class StatusDataEntryTransformer extends AbstractTransformer<StatusDataEntryDto, StatusDataEntry, any> {

    private _projectTransformer: ProjectTransformer
    private _mobileResponseTransformer: MobileResponseTransformer
    private _userTransformer: UserTransformer

    dtoToObject(from: StatusDataEntryDto): StatusDataEntry {

        return new StatusDataEntry(
            from['@id'],
            from.id,
            (from.user as UserDto)?.['@id'] ?? (from.user as string),
            () => !!(from.user as UserDto)?.['@id'] ?
                Promise.resolve(this._userTransformer.dtoToObject((from.user as UserDto))) :
                Promise.resolve(null),
            from.webappProject?.['@id'],
            () => {
                return this.httpService.get<ProjectDto>(from.webappProject?.['@id'])
                    .then(response => this._projectTransformer.dtoToObject(response.data))
            },
            (from.webappProject?.owner as UserDto)?.['@id'],
            from.webappProject?.name,
            from.mobileProjectName,
            from.status,
            from.syncable,
            undefined,
            () => !!from.response ?
                Promise.resolve(this._mobileResponseTransformer.dtoToObject(from.response)) :
                Promise.resolve(null),
            from.changes?.map(change => {
                const requestObject = change.requestIndividual?.unitParcel ?? change.requestUnitParcel
                const responseObject = change.requestIndividual ?? change.requestUnitParcel
                return ({
                    iri: change['@id'],
                    requestIndividual: !change.requestIndividual ? undefined : {
                        stateCode: change.requestIndividual.stateCode?.code,
                        stateTitle: change.requestIndividual.stateCode?.label,
                        appeared: change.requestIndividual.apparitionDate ? new Date(change.requestIndividual.apparitionDate) : null,
                        disappeared: change.requestIndividual.demiseDate ? new Date(change.requestIndividual.demiseDate) : null,
                    },
                    responseIndividual: !change.responseIndividual ? undefined : {
                        stateCode: change.responseIndividual.stateCode?.code,
                        stateTitle: change.responseIndividual.stateCode?.label,
                        appeared: change.requestIndividual.apparitionDate ? new Date(change.requestIndividual.apparitionDate) : null,
                        disappeared: change.requestIndividual.demiseDate ? new Date(change.requestIndividual.demiseDate) : null,
                    },
                    requestUnitPlot: !change.requestUnitParcel ? undefined : {
                        stateCode: change.requestUnitParcel.stateCode?.code,
                        stateTitle: change.requestUnitParcel.stateCode?.label,
                        appeared: change.requestUnitParcel.apparitionDate ? new Date(change.requestUnitParcel.apparitionDate) : null,
                        disappeared: change.requestUnitParcel.demiseDate ? new Date(change.requestUnitParcel.demiseDate) : null,
                    },
                    responseUnitPlot: !change.responseUnitParcel ? undefined : {
                        stateCode: change.responseUnitParcel.stateCode?.code,
                        stateTitle: change.responseUnitParcel.stateCode?.label,
                        appeared: change.responseUnitParcel.apparitionDate ? new Date(change.responseUnitParcel.apparitionDate) : null,
                        disappeared: change.responseUnitParcel.demiseDate ? new Date(change.responseUnitParcel.demiseDate) : null,
                    },
                    x: responseObject.x,
                    y: responseObject.y,
                    treatmentName: requestObject.treatment.name,
                    treatmentShortName: requestObject.treatment.shortName,
                    blockName: requestObject.subBlock?.block.name ?? requestObject.block.name,
                })
            }),
        )
    }

    objectToFormInterface(from: StatusDataEntry): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: any): StatusDataEntryDto {
        return undefined
    }

    formInterfaceToUpdateDto(from: any): StatusDataEntryDto {
        return undefined
    }

    set projectTransformer(value: ProjectTransformer) {
        this._projectTransformer = value
    }

    set mobileResponseTransformer(value: MobileResponseTransformer) {
        this._mobileResponseTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }
}
