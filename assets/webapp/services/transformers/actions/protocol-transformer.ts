import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import ModalityFormInterface from '@adonis/webapp/form-interfaces/modality-form-interface'
import ProtocolFormInterface from '@adonis/webapp/form-interfaces/protocol-form-interface'
import Protocol from '@adonis/webapp/models/protocol'
import { AlgorithmDto } from '@adonis/webapp/services/http/entities/simple-dto/algorithm-dto'
import { AttachmentDto } from '@adonis/webapp/services/http/entities/simple-dto/attachment-dto'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { ProtocolDto } from '@adonis/webapp/services/http/entities/simple-dto/protocol-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import AlgorithmTransformer from '@adonis/webapp/services/transformers/actions/algorithm-transformer'
import AttachmentTransformer from '@adonis/webapp/services/transformers/actions/attachment-transformer'
import FactorTransformer from '@adonis/webapp/services/transformers/actions/factor-transformer'
import TreatmentTransformer from '@adonis/webapp/services/transformers/actions/treatment-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'
import { v4 } from 'uuid'

export default class ProtocolTransformer extends AbstractTransformer<ProtocolDto, Protocol, ProtocolFormInterface> {

    private _factorTransformer: FactorTransformer
    private _treatmentTransformer: TreatmentTransformer
    private _userTransformer: UserTransformer
    private _algorithmTransformer: AlgorithmTransformer
    private _attachmentTransformer: AttachmentTransformer

    dtoToObject(from: ProtocolDto): Protocol {
        return new Protocol(
            from['@id'],
            from.id,
            from.name,
            from.aim,
            from.factors as string[],
            () => {
                const promise = this.httpService.getAll<FactorDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.FACTOR),
                    {pagination: false, protocol: from['@id']})
                return (from.factors as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._factorTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.treatments as string[],
            () => {
                const promise = this.httpService.getAll<TreatmentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.TREATMENT),
                    {pagination: false, protocol: from['@id']})
                return (from.treatments as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._treatmentTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.comment,
            new Date(from.created),
            from.owner,
            () => this.httpService.get<UserDto>(from.owner)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.algorithm as string,
            () => !from.algorithm ? Promise.resolve(null) : this.httpService.get<AlgorithmDto>(from.algorithm as string)
                .then(response => this._algorithmTransformer.dtoToObject(response.data)),
            from.protocolAttachments as string[],
            () => {
                const promise = this.httpService.getAll<AttachmentDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.PROTOCOL_ATTACHMENT),
                    {pagination: false, protocol: from['@id']})
                return (from.protocolAttachments as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._attachmentTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
        )
    }

    objectToFormInterface(from: Protocol): Promise<ProtocolFormInterface> {
        // tout récupérer en une requête
        return Promise.all(from.factors.map(promise => promise.then(factor => this._factorTransformer.objectToFormInterface(factor))))
            .then(async (factors) => ({
                name: from.name,
                creatorLogin: (await from.owner)?.username,
                dateOfCreation: from.created.toISOString()
                    .substr(0, 16),
                aim: from.aim,
                comment: from.comment,
                factors,
                treatments: [],
                algorithm: await from.algorithm,
            }))
            .then((protoInterface: ProtocolFormInterface) => {
                const map: Map<string, ModalityFormInterface> = new Map()
                protoInterface.factors
                    .forEach(factorInterface => factorInterface.modalities
                        .forEach(modalityInterface => map.set(modalityInterface.uniqId, modalityInterface)))
                return Promise.all(from.treatments.map(promise => promise.then(treatment => {
                    return this._treatmentTransformer.objectToFormInterface(treatment)
                        .then(treatmentInterface => {
                            treatmentInterface.modalities = treatment.modalitiesIriTab.map(iri => map.get(iri))
                            return treatmentInterface
                        })
                })))
                    .then(treatmentInterfaces => {
                        protoInterface.treatments = treatmentInterfaces
                        return protoInterface
                    })
            })
    }

    formInterfaceToDto(from: ProtocolFormInterface): ProtocolDto {
        from.factors.forEach(factorInterface => factorInterface.modalities.forEach(modality => modality.uniqId = v4()))
        return {
            name: from.name,
            aim: from.aim,
            factors: from.factors.map(factorInterface => {
                return {
                    name: factorInterface.name,
                    modalities: factorInterface.modalities.map(modality => {
                        return {
                            uniqId: modality.uniqId,
                            value: modality.value,
                            shortName: modality.shortName,
                            identifier: modality.id,
                            openSilexUri: modality.openSilexUri,
                            openSilexInstance: modality.openSilexInstance?.iri,
                        }
                    }),
                    order: factorInterface.order,
                    germplasm: factorInterface.germplasm,
                    openSilexInstance: factorInterface.openSilexInstance?.iri,
                    openSilexUri: factorInterface.openSilexUri,
                }
            }),
            comment: from.comment,
            treatments: from.treatments.map(treatmentInterface => {
                return {
                    name: treatmentInterface.name,
                    shortName: treatmentInterface.shortName,
                    repetitions: treatmentInterface.repetitions,
                    modalities: treatmentInterface.modalities.map(modalityInterface => modalityInterface.uniqId),
                }
            }),
            algorithm: from.algorithm?.iri,
            protocolAttachments: from.attachments?.map(attachment => this._attachmentTransformer.formInterfaceToDto(attachment)) ?? [],
        }
    }

    set factorTransformer(value: FactorTransformer) {
        this._factorTransformer = value
    }

    set treatmentTransformer(value: TreatmentTransformer) {
        this._treatmentTransformer = value
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set algorithmTransformer(value: AlgorithmTransformer) {
        this._algorithmTransformer = value
    }

    set attachmentTransformer(value: AttachmentTransformer) {
        this._attachmentTransformer = value
    }
}
