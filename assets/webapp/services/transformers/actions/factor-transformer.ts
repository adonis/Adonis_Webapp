import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import FactorFormInterface from '@adonis/webapp/form-interfaces/factor-form-interface'
import Factor from '@adonis/webapp/models/factor'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ModalityTransformer from '@adonis/webapp/services/transformers/actions/modality-transformer'
import OpenSilexInstanceTransformer from '@adonis/webapp/services/transformers/actions/open-silex-instance-transformer'

export default class FactorTransformer extends AbstractTransformer<FactorDto, Factor, FactorFormInterface> {

    private _modalityTransformer: ModalityTransformer
    private _openSilexInstanceTransformer: OpenSilexInstanceTransformer

    dtoToObject(from: FactorDto): Factor {
        return new Factor(
            from['@id'],
            from.id,
            from.name,
            from.modalities as string[],
            () => {
                const promise = this.httpService.getAll<ModalityDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.MODALITY),
                    {pagination: false, factor: from['@id']})
                return (from.modalities as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._modalityTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.order,
            from.openSilexUri,
            from.openSilexInstance,
            () => !from.openSilexInstance ? Promise.resolve( null ) : this.httpService.get<OpenSilexInstanceDto>(from.openSilexInstance)
                .then(response => this._openSilexInstanceTransformer.dtoToObject(response.data)),
            from.germplasm,
        )
    }

    async objectToFormInterface(from: Factor): Promise<FactorFormInterface> {
        const modalities = await Promise.all(from.modalities.map(promise => promise.then(modality => this._modalityTransformer.objectToFormInterface(modality))))
        return {
            name: from.label,
            modalities,
            order: from.order,
            germplasm: from.germplasm,
            openSilexInstance: await from.openSilexInstance,
            openSilexUri: from.openSilexUri,
        }
    }

    formInterfaceToDto(from: FactorFormInterface): FactorDto {
        return {
            name: from.name,
            modalities: from.modalities.map(modality => this._modalityTransformer.formInterfaceToDto(modality)),
            openSilexUri: from.openSilexUri,
            openSilexInstance: from.openSilexInstance?.iri,
            germplasm: from.germplasm,
        }
    }

    set modalityTransformer(value: ModalityTransformer) {
        this._modalityTransformer = value
    }

    set openSilexInstanceTransformer(value: OpenSilexInstanceTransformer) {
        this._openSilexInstanceTransformer = value
    }
}
