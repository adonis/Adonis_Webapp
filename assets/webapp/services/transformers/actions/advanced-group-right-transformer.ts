import AdvancedGroupRight from '@adonis/webapp/models/advanced-group-right'
import { AdvancedGroupRightDto } from '@adonis/webapp/services/http/entities/simple-dto/advanced-group-right-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AdvancedGroupRightTransformer extends AbstractTransformer<AdvancedGroupRightDto, AdvancedGroupRight, void> {

  dtoToObject( from: AdvancedGroupRightDto ): AdvancedGroupRight {
    return new AdvancedGroupRight(
        from['@id'],
        from.groupId,
        from.objectId,
        from.classIdentifier,
        from.right,
    )
  }

  objectToFormInterface( from: AdvancedGroupRight ): Promise<void> {
    return undefined
  }

  formInterfaceToDto( from: void ): AdvancedGroupRightDto {
    return undefined
  }
}
