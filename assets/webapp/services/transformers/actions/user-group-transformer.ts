import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { SiteDto } from '@adonis/shared/models/dto/site-dto'
import { UserDto } from '@adonis/shared/models/dto/user-dto'
import UserGroupFormInterface from '@adonis/webapp/form-interfaces/user-group-form-interface'
import UserGroup from '@adonis/webapp/models/user-group'
import { UserGroupDto } from '@adonis/webapp/services/http/entities/simple-dto/user-group-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import SiteTransformer from '@adonis/webapp/services/transformers/actions/site-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class UserGroupTransformer extends AbstractTransformer<UserGroupDto, UserGroup, UserGroupFormInterface> {

    private _userTransformer: UserTransformer
    private _siteTransformer: SiteTransformer

    dtoToObject(from: UserGroupDto): UserGroup {
        return new UserGroup(
            from['@id'],
            from.name,
            from.users,
            () => {
                const promise = this.httpService.getAll<UserDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.USER),
                    {pagination: false, groups: from['@id']})
                return from.users.map((uri, index) => {
                    return promise
                        .then(response => this._userTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.site,
            () => this.httpService.get<SiteDto>(from.site)
                .then(response => this._siteTransformer.dtoToObject(response.data)),
        )
    }

    objectToFormInterface(from: UserGroup): Promise<UserGroupFormInterface> {
        return undefined
    }

    formInterfaceToDto(from: UserGroupFormInterface): UserGroupDto {
        return {
            name: from.name,
            users: from.users.map(user => user.iri),
        }
    }

    set userTransformer(value: UserTransformer) {
        this._userTransformer = value
    }

    set siteTransformer(value: SiteTransformer) {
        this._siteTransformer = value
    }
}
