import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import VariableConnectionFormInterface from '@adonis/webapp/form-interfaces/variable-connection-form-interface'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'
import VariableConnection from '@adonis/webapp/models/variable-connection'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'

export default class VariableConnectionTransformer extends AbstractTransformer<VariableConnectionDto, VariableConnection, VariableConnectionFormInterface> {

    private _generatorVariableTransformer: GeneratorVariableTransformer
    private _simpleVariableTransformer: SimpleVariableTransformer
    private _semiAutomaticVariableTransformer: SemiAutomaticVariableTransformer

    dtoToObject(from: VariableConnectionDto): VariableConnection {
        const dataEntryVariableIri: string =
            from.dataEntrySimpleVariable as string ??
            from.dataEntryGeneratorVariable as string ??
            from.dataEntrySemiAutomaticVariable as string
        return new VariableConnection(
            from['@id'],
            from.projectSimpleVariable ?? from.projectGeneratorVariable ?? from.projectSemiAutomaticVariable,
            () => this.httpService.get<AbstractDtoObject>(from.projectSimpleVariable ?? from.projectGeneratorVariable ?? from.projectSemiAutomaticVariable)
                .then(response => {
                    if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE))) {
                        return this._simpleVariableTransformer.dtoToObject(response.data as SimpleVariableDto)
                    } else if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE))) {
                        return this._generatorVariableTransformer.dtoToObject(response.data as GeneratorVariableDto)
                    } else if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE))) {
                        return this._semiAutomaticVariableTransformer.dtoToObject(response.data as SemiAutomaticVariableDto)
                    }
                }),
            dataEntryVariableIri,
            () => this.httpService.get<AbstractDtoObject>(dataEntryVariableIri)
                .then(response => {
                    if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE))) {
                        return this._simpleVariableTransformer.dtoToObject(response.data as SimpleVariableDto)
                    } else if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE))) {
                        return this._generatorVariableTransformer.dtoToObject(response.data as GeneratorVariableDto)
                    } else if (response.data['@id'].includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE))) {
                        return this._semiAutomaticVariableTransformer.dtoToObject(response.data as SemiAutomaticVariableDto)
                    }
                }),
        )
    }

    async objectToFormInterface(from: VariableConnection): Promise<VariableConnectionFormInterface> {
        return {
            iri: from.iri,
            projectVariable: await from.projectVariable,
            dataEntryVariable: await from.dataEntryVariable,
        }
    }

    formInterfaceToDto(from: VariableConnectionFormInterface): VariableConnectionDto {
        return {
            projectSimpleVariable: from.projectVariable instanceof SimpleVariable ? from.projectVariable.iri : null,
            projectGeneratorVariable: from.projectVariable instanceof GeneratorVariable ? from.projectVariable.iri : null,
            projectSemiAutomaticVariable: from.projectVariable instanceof SemiAutomaticVariable ? from.projectVariable.iri : null,
            dataEntrySimpleVariable: from.dataEntryVariable instanceof SimpleVariable ? from.dataEntryVariable.iri : null,
            dataEntryGeneratorVariable: from.dataEntryVariable instanceof GeneratorVariable ? from.dataEntryVariable.iri : null,
            dataEntrySemiAutomaticVariable: from.dataEntryVariable instanceof SemiAutomaticVariable ? from.dataEntryVariable.iri : null,
        }
    }

    set generatorVariableTransformer(value: GeneratorVariableTransformer) {
        this._generatorVariableTransformer = value
    }

    set simpleVariableTransformer(value: SimpleVariableTransformer) {
        this._simpleVariableTransformer = value
    }

    set semiAutomaticVariableTransformer(value: SemiAutomaticVariableTransformer) {
        this._semiAutomaticVariableTransformer = value
    }
}
