import { UserDto } from '@adonis/shared/models/dto/user-dto'
import UserPathFormInterface from '@adonis/webapp/form-interfaces/user-path-form-interface'
import UserPathWorkflow from '@adonis/webapp/models/path-user-workflow'
import { PathBaseDto } from '@adonis/webapp/services/http/entities/simple-dto/path-base-dto'
import { UserPathDto } from '@adonis/webapp/services/http/entities/simple-dto/user-path-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import PathBaseTransformer from '@adonis/webapp/services/transformers/actions/path-base-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'

export default class UserPathTransformer extends AbstractTransformer<UserPathDto, UserPathWorkflow, UserPathFormInterface> {

    private _userTransformer: UserTransformer
    private _pathBaseTransformer: PathBaseTransformer

    dtoToObject(from: UserPathDto): UserPathWorkflow {

        return new UserPathWorkflow(
            from['@id'],
            from.id,
            from.workflow,
            from.user,
            () => this.httpService.get<UserDto>(from.user)
                .then(response => this._userTransformer.dtoToObject(response.data)),
            from.pathBase,
            () => this.httpService.get<PathBaseDto>(from.pathBase)
                .then(response => this._pathBaseTransformer.dtoToObject(response.data)),
        )
    }

    formInterfaceToDto(from: UserPathFormInterface): UserPathDto {
        return {
            workflow: from.selectedObjects,
            user: from.user.iri,
        }
    }

    objectToFormInterface(from: UserPathWorkflow): Promise<UserPathFormInterface> {

        return from.user.then(user => ({
            user,
            selectedObjects: from?.workflow,
        }))
    }

    set userTransformer(userTransformer: UserTransformer) {
        this._userTransformer = userTransformer
    }

    set pathBaseTransformer(pathBaseTransformer: PathBaseTransformer) {
        this._pathBaseTransformer = pathBaseTransformer
    }

}
