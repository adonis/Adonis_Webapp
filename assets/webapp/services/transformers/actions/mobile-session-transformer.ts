import MobileSession from '@adonis/webapp/models/mobile-session'
import { MobileSessionDto } from '@adonis/webapp/services/http/entities/simple-dto/mobile-session-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class MobileSessionTransformer extends AbstractTransformer<MobileSessionDto, MobileSession, any> {

  dtoToObject( from: MobileSessionDto ): MobileSession {

    return new MobileSession(
        undefined,
        from.id,
        new Date( from.startedAt ),
        new Date( from.endedAt ),
    )
  }

  objectToFormInterface( from: MobileSession ): Promise<any> {

    return undefined
  }

  formInterfaceToDto( from: any ): MobileSessionDto {
    return undefined
  }

}
