import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import GeneratorInfoFormInterface from '@adonis/webapp/form-interfaces/generator-info-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'
import VariableConnectionTransformer from '@adonis/webapp/services/transformers/actions/variable-connection-transformer'

// tslint:disable-next-line:max-line-length
export default class GeneratorVariableTransformer extends AbstractTransformer<GeneratorVariableDto, GeneratorVariable, GeneratorInfoFormInterface & VariableFormInterface> {

    private _simpleVariableTransformer: SimpleVariableTransformer

    private _variableConnectionTransformer: VariableConnectionTransformer

    dtoToObject(from: GeneratorVariableDto): GeneratorVariable {
        return new GeneratorVariable(
            from['@id'],
            from.id,
            from.name,
            from.shortName,
            from.repetitions,
            from.unit,
            from.pathLevel,
            from.comment,
            from.order,
            from.format,
            from.formatLength,
            from.mandatory,
            from.identifier,
            new Date(from.created),
            from.generatedPrefix,
            from.numeralIncrement,
            from.pathWayHorizontal,
            from.directCounting,
            from.generatedSimpleVariables as string[],
            () => {
                const promise = this.httpService.getAll<SimpleVariableDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE),
                    {pagination: false, generatorVariable: from['@id']})
                return (from.generatedSimpleVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._simpleVariableTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.generatedGeneratorVariables as string[],
            () => {
                const promise = this.httpService.getAll<GeneratorVariableDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE),
                    {pagination: false, generatorVariable: from['@id']})
                return (from.generatedGeneratorVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.project,
            from.connectedVariables as string[],
            () => {
                const promise = this.httpService.getAll<VariableConnectionDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.VARIABLE_CONNECTION),
                    {pagination: false, projectGeneratorVariable: from['@id']})
                return (from.connectedVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._variableConnectionTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.openSilexUri,
        )
    }

    objectToFormInterface(from: GeneratorVariable): Promise<GeneratorInfoFormInterface & VariableFormInterface> {
        return Promise.resolve()
            .then(() => ({
                name: from.name,
                shortName: from.shortName,
                type: VariableTypeEnum.INTEGER,
                format: from.format,
                formatLength: from.formatLength,
                pathLevel: from.pathLevel,
                repetitions: from.repetitions,
                unit: from.unit,
                required: from.mandatory,
                identifier: from.identifier,
                comment: from.comment,
                prefix: from.generatedPrefix,
                numeralIncrement: from.numeralIncrement,
                pathWayHorizontal: from.pathWayHorizontal,
                directCounting: from.directCounting,
                generatedGeneratorVariables: from.generatedGeneratorVariablesIriTab,
                generatedSimpleVariables: from.generatedSimpleVariablesIriTab,
            }))
    }

    formInterfaceToDto(generatorVariable: GeneratorInfoFormInterface & VariableFormInterface): GeneratorVariableDto {
        return {
            name: generatorVariable.name,
            shortName: generatorVariable.shortName,
            repetitions: generatorVariable.repetitions,
            unit: generatorVariable.unit,
            pathLevel: generatorVariable.pathLevel,
            comment: generatorVariable.comment,
            format: generatorVariable.format,
            mandatory: generatorVariable.required,
            generatedPrefix: generatorVariable.prefix,
            numeralIncrement: generatorVariable.numeralIncrement,
            pathWayHorizontal: generatorVariable.pathWayHorizontal,
            directCounting: generatorVariable.directCounting,
            identifier: generatorVariable.identifier,
            generatedSimpleVariables: generatorVariable.generatedSimpleVariables,
            generatedGeneratorVariables: generatorVariable.generatedGeneratorVariables,
        }
    }

    set simpleVariableTransformer(value: SimpleVariableTransformer) {
        this._simpleVariableTransformer = value
    }

    set variableConnectionTransformer(value: VariableConnectionTransformer) {
        this._variableConnectionTransformer = value
    }
}
