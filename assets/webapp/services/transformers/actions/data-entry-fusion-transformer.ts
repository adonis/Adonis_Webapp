import DataEntryFusionFormInterface from '@adonis/webapp/form-interfaces/data-entry-fusion-form-interface'
import { DataEntryFusionDto } from '@adonis/webapp/services/http/entities/simple-dto/data-entry-fusion-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class DataEntryFusionTransformer extends AbstractTransformer<DataEntryFusionDto, any, DataEntryFusionFormInterface> {

  dtoToObject( from: DataEntryFusionDto ): any {
    return undefined
  }

  objectToFormInterface( from: any ): Promise<DataEntryFusionFormInterface> {
    return undefined
  }

  formInterfaceToDto( from: DataEntryFusionFormInterface ): DataEntryFusionDto {
    const res: {
      objectIri: string,
      orderedProjectDatasIris: string[],
    }[] = []
    from.specificOrderForItem.forEach( ( value, key ) => res.push( {
      objectIri: key,
      orderedProjectDatasIris: value,
    } ) )
    return {
      orderedDefaultProjectDatasIris: from.orderedDefaultProjectDatasIris,
      selectedVariableIris: from.selectedVariableIris,
      specificOrderForItem: res,
      mergePriority: from.mergePriority,
      name: from.name,
    }
  }
}
