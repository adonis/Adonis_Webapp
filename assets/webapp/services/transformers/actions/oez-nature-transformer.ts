import OezNatureFormInterface from '@adonis/webapp/form-interfaces/oez-nature-form-interface'
import OezNature from '@adonis/webapp/models/oez-nature'
import { OezNatureDto } from '@adonis/webapp/services/http/entities/simple-dto/oez-nature-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class OezNatureTransformer extends AbstractTransformer<OezNatureDto, OezNature, OezNatureFormInterface> {

  dtoToObject( from: OezNatureDto ): OezNature {
    return new OezNature(
        from['@id'],
        from.nature,
        from.color,
        from.texture ?? null,
    )
  }

  objectToFormInterface( from: OezNature ): Promise<OezNatureFormInterface> {
    return Promise.resolve( {
      nature: from.nature,
      color: from.color,
      texture: from.texture,
    } )
  }

  formInterfaceToDto( from: OezNatureFormInterface ): OezNatureDto {
    return {
      nature: from.nature,
      color: from.color,
      texture: from.texture ?? null,
    }
  }
}
