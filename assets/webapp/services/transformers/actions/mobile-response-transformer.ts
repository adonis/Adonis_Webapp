import MobileResponse from '@adonis/webapp/models/mobile-response'
import { MobileResponseDto } from '@adonis/webapp/services/http/entities/simple-dto/mobile-response-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import MobileDataEntryTransformer from '@adonis/webapp/services/transformers/actions/mobile-data-entry-transformer'

export default class MobileResponseTransformer extends AbstractTransformer<MobileResponseDto, MobileResponse, any> {

  private _mobileDataEntryTransformer: MobileDataEntryTransformer

  dtoToObject( from: MobileResponseDto ): MobileResponse {

    return new MobileResponse(
        undefined,
        from.id,
        undefined,
        () => Promise.resolve(this._mobileDataEntryTransformer.dtoToObject(from.dataEntry)),
    )
  }

  objectToFormInterface( from: MobileResponse ): Promise<any> {

    return undefined
  }

  formInterfaceToDto( from: any ): MobileResponseDto {
    return undefined
  }

  set mobileDataEntryTransformer( value: MobileDataEntryTransformer ) {
    this._mobileDataEntryTransformer = value
  }
}
