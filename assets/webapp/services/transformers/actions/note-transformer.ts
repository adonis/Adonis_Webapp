import Note from '@adonis/webapp/models/note'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class NoteTransformer extends AbstractTransformer<NoteDto, Note, any> {

  dtoToObject( from: NoteDto ): Note {
    return new Note(
        from['@id'],
        from.id,
        from.text,
        new Date(from.creationDate),
        from.creatorLogin,
        from.creatorAvatar,
        !!from.deleted,
    )
  }

  formInterfaceToDto( from: any ): NoteDto {
    return undefined
  }

  objectToFormInterface( from: Note ): Promise<any> {
    return Promise.resolve( undefined )
  }

}
