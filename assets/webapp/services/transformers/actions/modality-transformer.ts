import ModalityFormInterface from '@adonis/webapp/form-interfaces/modality-form-interface'
import Modality from '@adonis/webapp/models/modality'
import { FactorDto } from '@adonis/webapp/services/http/entities/simple-dto/factor-dto'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import { OpenSilexInstanceDto } from '@adonis/webapp/services/http/entities/simple-dto/open-silex-instance-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import FactorTransformer from '@adonis/webapp/services/transformers/actions/factor-transformer'
import OpenSilexInstanceTransformer from '@adonis/webapp/services/transformers/actions/open-silex-instance-transformer'

export default class ModalityTransformer extends AbstractTransformer<ModalityDto, Modality, ModalityFormInterface> {

    private _factorTransformer: FactorTransformer

    private _openSilexInstanceTransformer: OpenSilexInstanceTransformer

    dtoToObject(from: ModalityDto): Modality {
        return new Modality(
            from['@id'],
            from.id,
            from.value,
            from.shortName,
            from.identifier,
            from.factor,
            () => this.httpService.get<FactorDto>(from.factor)
                .then(response => this._factorTransformer.dtoToObject(response.data)),
            from.openSilexUri,
            from.openSilexInstance,
            () => !from.openSilexInstance ? Promise.resolve( null ) : this.httpService.get<OpenSilexInstanceDto>(from.openSilexInstance)
                .then(response => this._openSilexInstanceTransformer.dtoToObject(response.data)),
        )
    }

    async objectToFormInterface(from: Modality): Promise<ModalityFormInterface> {
        return {
            id: from.identifier,
            value: from.value,
            shortName: from.shortName,
            uniqId: from.iri,
            openSilexUri: from.openSilexUri,
            openSilexInstance: await from.openSilexInstance,
        }
    }

    formInterfaceToDto(from: ModalityFormInterface): ModalityDto {
        return {
            value: from.value,
            shortName: from.shortName,
            identifier: from.id,
            openSilexInstance: from.openSilexInstance?.iri,
            openSilexUri: from.openSilexUri,
        }
    }

    set factorTransformer(value: FactorTransformer) {
        this._factorTransformer = value
    }

    set openSilexInstanceTransformer(value: OpenSilexInstanceTransformer) {
        this._openSilexInstanceTransformer = value
    }
}
