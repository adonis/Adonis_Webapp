import MobileDataEntry from '@adonis/webapp/models/mobile-data-entry'
import { MobileDataEntryDto } from '@adonis/webapp/services/http/entities/simple-dto/mobile-data-entry-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import MobileSessionTransformer from '@adonis/webapp/services/transformers/actions/mobile-session-transformer'

export default class MobileDataEntryTransformer extends AbstractTransformer<MobileDataEntryDto, MobileDataEntry, any> {

  private _mobileSessionTransformer: MobileSessionTransformer

  dtoToObject( from: MobileDataEntryDto ): MobileDataEntry {

    return new MobileDataEntry(
        undefined,
        from.id,
        from.sessions.map(() => undefined),
        () => from.sessions.map(session => Promise.resolve(this._mobileSessionTransformer.dtoToObject(session))),
    )
  }

  objectToFormInterface( from: MobileDataEntry ): Promise<any> {

    return undefined
  }

  formInterfaceToDto( from: any ): MobileDataEntryDto {
    return undefined
  }

  set mobileSessionTransformer( value: MobileSessionTransformer ) {
    this._mobileSessionTransformer = value
  }
}
