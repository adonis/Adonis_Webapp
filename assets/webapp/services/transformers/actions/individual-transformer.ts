import { Individual } from '@adonis/webapp/models/individual'
import { IndividualDto } from '@adonis/webapp/services/http/entities/simple-dto/individual-dto'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'

export default class IndividualTransformer extends AbstractTransformer<IndividualDto, Individual, any> {

    private _noteTransformer: NoteTransformer

    dtoToObject(from: IndividualDto): Individual {
        return new Individual(
            from['@id'],
            from.id,
            from.number,
            from.x,
            from.y,
            from.dead,
            new Date(from.appeared),
            !from.disappeared ? undefined : new Date(from.disappeared),
            from.identifier,
            from.latitude,
            from.longitude,
            from.height,
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.comment,
            from.openSilexUri,
            from.geometry,
        )
    }

    objectToFormInterface(from: Individual): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: any): IndividualDto {
        return undefined
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }
}
