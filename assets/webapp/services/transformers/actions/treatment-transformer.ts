import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import TreatmentFormInterface from '@adonis/webapp/form-interfaces/treatment-form-interface'
import Treatment from '@adonis/webapp/models/treatment'
import { ModalityDto } from '@adonis/webapp/services/http/entities/simple-dto/modality-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import ModalityTransformer from '@adonis/webapp/services/transformers/actions/modality-transformer'

export default class TreatmentTransformer extends AbstractTransformer<TreatmentDto, Treatment, TreatmentFormInterface> {

    private _modalityTransformer: ModalityTransformer

    dtoToObject(from: TreatmentDto): Treatment {
        return new Treatment(
            from['@id'],
            from.id,
            from.name,
            from.shortName,
            from.repetitions,
            from.modalities as string[],
            () => {
                const promise = this.httpService.getAll<ModalityDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.MODALITY),
                    {pagination: false, treatments: from['@id']})
                return from.modalities.map((uri, index) =>
                    promise.then(response => this._modalityTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
        )
    }

    objectToFormInterface(from: Treatment): Promise<TreatmentFormInterface> {
        return Promise.resolve({
            name: from.name,
            shortName: from.shortName,
            repetitions: from.repetitions,
            modalities: [], // Will be filled inside the protocol transformer
        })
    }

    set modalityTransformer(value: ModalityTransformer) {
        this._modalityTransformer = value
    }

    formInterfaceToDto(from: TreatmentFormInterface): TreatmentDto {
        return undefined
    }
}
