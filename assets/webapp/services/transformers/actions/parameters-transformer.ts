import ParametersFormInterface from '@adonis/webapp/form-interfaces/parameters-form-interface'
import Parameters from '@adonis/webapp/models/parameters'
import { ParametersDto } from '@adonis/webapp/services/http/entities/simple-dto/parameters-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class ParametersTransformer extends AbstractTransformer<ParametersDto, Parameters, ParametersFormInterface> {

  dtoToObject( from: ParametersDto ): Parameters {
    return new Parameters(
        from['@id'],
        from.id,
        from.daysBeforeFileDelete,
    )
  }

  objectToFormInterface( from: Parameters ): Promise<ParametersFormInterface> {
    return Promise.resolve( {
      daysBeforeFileDelete: from.daysBeforeFileDelete,
    } )
  }

  formInterfaceToDto( from: ParametersFormInterface ): ParametersDto {
    return {
      daysBeforeFileDelete: from.daysBeforeFileDelete,
    }
  }
}
