import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { AbstractDtoObject } from '@adonis/shared/models/dto-types'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import TestFormInterface from '@adonis/webapp/form-interfaces/test-form-interface'
import Test from '@adonis/webapp/models/test'
import { GeneratorVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { SimpleVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import { TestDto } from '@adonis/webapp/services/http/entities/simple-dto/test-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'

export default class TestTransformer extends AbstractTransformer<TestDto, Test, TestFormInterface> {

    private _simpleVariableTransformer: SimpleVariableTransformer
    private _generatorVariableTransformer: GeneratorVariableTransformer
    private _semiAutomaticVariableTransformer: SemiAutomaticVariableTransformer

    dtoToObject(from: TestDto): Test {
        return new Test(
            from['@id'],
            from.id,
            from.type,
            (from.variable as AbstractDtoObject)['@id'],
            this.variableCallbackFromIri((from.variable as AbstractDtoObject)['@id']),
            from.authIntervalMin,
            from.probIntervalMin,
            from.probIntervalMax,
            from.authIntervalMax,
            (from.comparedVariable as AbstractDtoObject)?.['@id'],
            this.variableCallbackFromIri((from.comparedVariable as AbstractDtoObject)?.['@id']),
            from.minGrowth,
            from.maxGrowth,
            (from.combinedVariable1 as AbstractDtoObject)?.['@id'],
            this.variableCallbackFromIri((from.combinedVariable1 as AbstractDtoObject)?.['@id']),
            from.combinationOperation,
            (from.combinedVariable2 as AbstractDtoObject)?.['@id'],
            this.variableCallbackFromIri((from.combinedVariable2 as AbstractDtoObject)?.['@id']),
            from.minLimit,
            from.maxLimit,
            from.compareWithVariable,
            (from.comparedVariable1 as AbstractDtoObject)?.['@id'],
            this.variableCallbackFromIri((from.comparedVariable1 as AbstractDtoObject)?.['@id']),
            from.comparisonOperation,
            (from.comparedVariable2 as AbstractDtoObject)?.['@id'],
            this.variableCallbackFromIri((from.comparedVariable2 as AbstractDtoObject)?.['@id']),
            from.comparedValue,
            from.assignedValue,
        )
    }

    private variableCallbackFromIri(variableIri: string) {
        return () => !variableIri ? undefined : this.httpService.get<GeneratorVariableDto | SimpleVariableDto | SemiAutomaticVariableDto>(variableIri)
            .then(response => {
                switch (true) {
                    case variableIri.includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE)):
                        return this._simpleVariableTransformer.dtoToObject(response.data)
                    case variableIri.includes(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE)):
                        return this._generatorVariableTransformer.dtoToObject(response.data)
                    case variableIri.includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE)):
                        return this._semiAutomaticVariableTransformer.dtoToObject(response.data)
                    default:
                        return undefined
                }
            })
    }

    async objectToFormInterface(from: Test): Promise<TestFormInterface> {
        switch (from.type) {
            case TestTypeEnum.INTERVAL:
                return {
                    type: from.type,
                    variable: await from.variable,
                    authIntervalMin: from.authIntervalMin,
                    probIntervalMin: from.probIntervalMin,
                    probIntervalMax: from.probIntervalMax,
                    authIntervalMax: from.authIntervalMax,
                }
            case TestTypeEnum.GROWTH:
                return {
                    type: from.type,
                    variable: await from.variable,
                    comparedVariable: await from.comparedVariable,
                    minGrowth: from.minGrowth,
                    maxGrowth: from.maxGrowth,
                }
            case TestTypeEnum.COMBINATION:
                return {
                    type: from.type,
                    variable: await from.variable,
                    combinedVariable1: await from.combinedVariable1,
                    combinationOperation: from.combinationOperation,
                    combinedVariable2: await from.combinedVariable2,
                    minLimit: from.minLimit,
                    maxLimit: from.maxLimit,
                }
            case TestTypeEnum.PRECONDITIONED:
                return {
                    type: from.type,
                    variable: await from.variable,
                    compareWithVariable: from.compareWithVariable,
                    comparedVariable1: await from.comparedVariable1,
                    comparisonOperation: from.comparisonOperation,
                    comparedVariable2: await from.comparedVariable2,
                    comparedValue: from.comparedValue,
                    assignedValue: from.assignedValue,
                }
            default:
                return undefined
        }
    }

    formInterfaceToDto(from: TestFormInterface): TestDto {
        switch (from.type) {
            case TestTypeEnum.INTERVAL:
                return {
                    type: from.type,
                    variable: from.variable.iri,
                    authIntervalMin: from.authIntervalMin,
                    probIntervalMin: from.probIntervalMin,
                    probIntervalMax: from.probIntervalMax,
                    authIntervalMax: from.authIntervalMax,
                }
            case TestTypeEnum.GROWTH:
                return {
                    type: from.type,
                    variable: from.variable.iri,
                    comparedVariable: from.comparedVariable.iri,
                    minGrowth: from.minGrowth,
                    maxGrowth: from.maxGrowth,
                }
            case TestTypeEnum.COMBINATION:
                return {
                    type: from.type,
                    variable: from.variable.iri,
                    combinedVariable1: from.combinedVariable1.iri,
                    combinationOperation: from.combinationOperation,
                    combinedVariable2: from.combinedVariable2.iri,
                    minLimit: from.minLimit,
                    maxLimit: from.maxLimit,
                }
            case TestTypeEnum.PRECONDITIONED:
                return {
                    type: from.type,
                    variable: from.variable.iri,
                    compareWithVariable: from.compareWithVariable,
                    comparedVariable1: from.comparedVariable1.iri,
                    comparisonOperation: from.comparisonOperation,
                    comparedVariable2: from.comparedVariable2?.iri,
                    comparedValue: from.comparedValue,
                    assignedValue: from.assignedValue,
                }
        }
    }

    formInterfaceToUpdateDto(from: TestFormInterface): TestDto {
        return {
            ...this.formInterfaceToDto(from),
            type: undefined,
            variable: undefined,
        }
    }

    set simpleVariableTransformer(value: SimpleVariableTransformer) {
        this._simpleVariableTransformer = value
    }

    set generatorVariableTransformer(value: GeneratorVariableTransformer) {
        this._generatorVariableTransformer = value
    }

    set semiAutomaticVariableTransformer(value: SemiAutomaticVariableTransformer) {
        this._semiAutomaticVariableTransformer = value
    }
}
