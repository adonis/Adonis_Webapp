import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import {FILTER_TREE_NODE_FACTORY} from '@adonis/webapp/constants/filters/filter-factory'
import FilterKeyEnum from '@adonis/webapp/constants/filters/filter-key-enum'
import FilterTreeLeaf from '@adonis/webapp/form-interfaces/filters/filter-tree-leaf'
import FilterTreeNode from '@adonis/webapp/form-interfaces/filters/filter-tree-node'
import PathFilterNode from '@adonis/webapp/models/path-filter-node'
import {GeneratorVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/generator-variable-dto'
import {PathFilterNodeDto} from '@adonis/webapp/services/http/entities/simple-dto/path-filter-node-dto'
import {SemiAutomaticVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import {SimpleVariableDto} from '@adonis/webapp/services/http/entities/simple-dto/simple-variable-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'
import SimpleVariable from "@adonis/webapp/models/simple-variable"
import GeneratorVariable from "@adonis/webapp/models/generator-variable"
import SemiAutomaticVariable from "@adonis/webapp/models/semi-automatic-variable"

export default class PathFilterNodeTransformer extends AbstractTransformer<PathFilterNodeDto, PathFilterNode, FilterTreeLeaf | FilterTreeNode> {

  private _simpleVariableTransformer: SimpleVariableTransformer
  private _generatorVariableTransformer: GeneratorVariableTransformer
  private _semiAutomaticVariableTransformer: SemiAutomaticVariableTransformer

  public dtoToObject(from: PathFilterNodeDto): PathFilterNode {
    const variable = from.variable !== undefined ? from.variable as (SimpleVariableDto | SemiAutomaticVariableDto | GeneratorVariableDto) : undefined

    return new PathFilterNode(
      from['@id'],
      from.id,
      from.type,
      from.text,
      from.branches.map(item => this.dtoToObject(item)),
      from.operator,
      from.value,
      () => {
        let variableIri = variable !== undefined ? variable['@id'] : undefined
        let variableObject: undefined | SimpleVariable | GeneratorVariable | SemiAutomaticVariable = undefined
        if (variable === undefined) {
          variableObject = undefined
        } else if (variableIri.includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE))) {
          variableObject = this._simpleVariableTransformer.dtoToObject(variable as SimpleVariableDto)
        } else if (variableIri.includes(this.httpService.getEndpointFromType(ObjectTypeEnum.GENERATOR_VARIABLE))) {
          variableObject = this._generatorVariableTransformer.dtoToObject(variable as GeneratorVariableDto)
        } else if (variableIri.includes(this.httpService.getEndpointFromType(ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE))) {
          variableObject = this._semiAutomaticVariableTransformer.dtoToObject(variable as SemiAutomaticVariableDto)
        }

        return Promise.resolve(variableObject)
      },
      variable === undefined ? undefined : variable['@id'],
    )
  }

  public objectToFormInterface(from: PathFilterNode): Promise<FilterTreeLeaf | FilterTreeNode> {
    const recursiveTransform = async (node: PathFilterNode): Promise<FilterTreeLeaf | FilterTreeNode> => {
      switch (node.type) {
        case FilterKeyEnum.OR_FILTER:
        case FilterKeyEnum.AND_FILTER:
        case FilterKeyEnum.NOT_FILTER:
          return {
            ...FILTER_TREE_NODE_FACTORY.constructFilterNode(node.type, node.text),
            branches: await Promise.all((node.branches.map(async item => await recursiveTransform(item)))),
          }
        case FilterKeyEnum.EXPERIMENT_NAME_FILTER:
        case FilterKeyEnum.BLOCK_NAME_FILTER:
        case FilterKeyEnum.SUB_BLOCK_NAME_FILTER:
        case FilterKeyEnum.INDIVIDUAL_X_FILTER:
        case FilterKeyEnum.INDIVIDUAL_Y_FILTER:
        case FilterKeyEnum.UNIT_PLOT_NAME_FILTER:
        case FilterKeyEnum.INDIVIDUAL_APPARITION_DATE_FILTER:
        case FilterKeyEnum.INDIVIDUAL_DISPARITION_DATE_FILTER:
        case FilterKeyEnum.INDIVIDUAL_NUMBER_FILTER:
        case FilterKeyEnum.UNIT_PLOT_TREATMENT_FILTER:
        case FilterKeyEnum.SURFACIC_UNIT_PLOT_TREATMENT_FILTER:
        case FilterKeyEnum.SURFACIC_UNIT_PLOT_Y_FILTER:
        case FilterKeyEnum.SURFACIC_UNIT_PLOT_X_FILTER:
        case FilterKeyEnum.SURFACIC_UNIT_PLOT_NAME_FILTER:
        case FilterKeyEnum.SURFACIC_UNIT_PLOT_DISPARITION_DATE_FILTER:
        case FilterKeyEnum.CONNECTED_VARIABLE_FILTER:
        case FilterKeyEnum.UNIT_PLOT_SHORT_TREATMENT_FILTER:
        case FilterKeyEnum.SURFACIC_UNIT_PLOT_SHORT_TREATMENT_FILTER:
          const variable = await node.variable
          return {
            ...FILTER_TREE_NODE_FACTORY.constructFilterLeaf(node.type, node.value, node.operator, node.text, variable),
          }

      }
    }
    return Promise.resolve(recursiveTransform(from))
  }

  public formInterfaceToDto(from: FilterTreeLeaf | FilterTreeNode): PathFilterNodeDto {
    return FILTER_TREE_NODE_FACTORY.isFilterTreeNode(from) ? {
      type: from.type,
      text: from.text,
      branches: from.branches.map(item => this.formInterfaceToDto(item)),
    } : {
      type: from.type,
      text: from.text,
      operator: from.filter.operator,
      value: from.filter.value,
      variable: from.filter.variable?.iri,
    }
  }

  public set simpleVariableTransformer(value: SimpleVariableTransformer) {
    this._simpleVariableTransformer = value
  }

  public set generatorVariableTransformer(value: GeneratorVariableTransformer) {
    this._generatorVariableTransformer = value
  }

  public set semiAutomaticVariableTransformer(value: SemiAutomaticVariableTransformer) {
    this._semiAutomaticVariableTransformer = value
  }
}
