import { OutExperimentationZone } from '@adonis/webapp/models/out-experimentation-zone'
import { OezNatureDto } from '@adonis/webapp/services/http/entities/simple-dto/oez-nature-dto'
import { OutExperimentationZoneDto } from '@adonis/webapp/services/http/entities/simple-dto/out-experimentation-zone-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import OezNatureTransformer from '@adonis/webapp/services/transformers/actions/oez-nature-transformer'

export default class OutExperimentationZoneTransformer extends AbstractTransformer<OutExperimentationZoneDto, OutExperimentationZone, any> {

    private _oezNatureTransformer: OezNatureTransformer

    dtoToObject(from: OutExperimentationZoneDto): OutExperimentationZone {
        return new OutExperimentationZone(
            from['@id'],
            from.id,
            from.number,
            from.x,
            from.y,
            from.comment,
            (from.nature as OezNatureDto)['@id'],
            () => Promise.resolve(this._oezNatureTransformer.dtoToObject((from.nature as OezNatureDto))),
            from.color ?? (from.nature as OezNatureDto).color,
            (from.nature as OezNatureDto).texture,
            from.openSilexUri,
        )
    }

    objectToFormInterface(from: OutExperimentationZone): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: any): OutExperimentationZoneDto {
        return undefined
    }

    set oezNatureTransformer(value: OezNatureTransformer) {
        this._oezNatureTransformer = value
    }
}
