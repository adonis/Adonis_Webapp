import { SurfacicUnitPlot } from '@adonis/webapp/models/surfacic-unit-plot'
import { NoteDto } from '@adonis/webapp/services/http/entities/simple-dto/note-dto'
import { SurfacicUnitPlotDto } from '@adonis/webapp/services/http/entities/simple-dto/surfacic-unit-plot-dto'
import { TreatmentDto } from '@adonis/webapp/services/http/entities/simple-dto/treatment-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import TreatmentTransformer from '@adonis/webapp/services/transformers/actions/treatment-transformer'

export default class SurfacicUnitPlotTransformer extends AbstractTransformer<SurfacicUnitPlotDto, SurfacicUnitPlot, any> {

    private _noteTransformer: NoteTransformer
    private _treatmentTransformer: TreatmentTransformer

    dtoToObject(from: SurfacicUnitPlotDto): SurfacicUnitPlot {
        return new SurfacicUnitPlot(
            from['@id'],
            from.id,
            from.number,
            from.x,
            from.y,
            from.dead,
            new Date(from.appeared),
            !from.disappeared ? undefined : new Date(from.disappeared),
            from.identifier,
            from.latitude,
            from.longitude,
            from.height,
            from.treatment as string,
            () => this.httpService.get<TreatmentDto>(from.treatment as string)
                .then(response => this._treatmentTransformer.dtoToObject(response.data)),
            from.notes,
            () => {
                const promise = this.httpService.getAll<NoteDto>(`${from['@id']}/notes`)
                return from.notes.map((uri, index) =>
                    promise.then(response => this._noteTransformer.dtoToObject(response.data['hydra:member'][index])),
                )
            },
            from.color,
            from.comment,
            from.openSilexUri,
            from.geometry,
        )
    }

    objectToFormInterface(from: SurfacicUnitPlot): Promise<any> {
        return undefined
    }

    formInterfaceToDto(from: any): SurfacicUnitPlotDto {
        return undefined
    }

    set noteTransformer(value: NoteTransformer) {
        this._noteTransformer = value
    }

    set treatmentTransformer(value: TreatmentTransformer) {
        this._treatmentTransformer = value
    }
}
