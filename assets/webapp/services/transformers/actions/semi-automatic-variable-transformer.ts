import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ManagedVariableExtraInfoFormInterface from '@adonis/webapp/form-interfaces/managed-variable-extra-info-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import { SemiAutomaticVariableDto } from '@adonis/webapp/services/http/entities/simple-dto/semi-automatic-variable-dto'
import { VariableConnectionDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-connection-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'
import VariableConnectionTransformer from '@adonis/webapp/services/transformers/actions/variable-connection-transformer'

// tslint:disable-next-line:max-line-length
export default class SemiAutomaticVariableTransformer extends AbstractTransformer<SemiAutomaticVariableDto, SemiAutomaticVariable, (VariableFormInterface & ManagedVariableExtraInfoFormInterface)> {

    private _variableConnectionTransformer: VariableConnectionTransformer

    dtoToObject(from: SemiAutomaticVariableDto): SemiAutomaticVariable {
        return new SemiAutomaticVariable(
            from['@id'],
            from.id,
            from.name,
            from.shortName,
            from.repetitions,
            from.unit,
            from.pathLevel,
            from.comment,
            from.order,
            from.format,
            from.formatLength,
            from.defaultTrueValue,
            from.type,
            from.mandatory,
            from.identifier,
            from.start,
            from.end,
            from.created,
            from.project,
            from.connectedVariables as string[],
            () => {
                const promise = this.httpService.getAll<VariableConnectionDto>(
                    this.httpService.getEndpointFromType(ObjectTypeEnum.VARIABLE_CONNECTION),
                    {pagination: false, projectGeneratorVariable: from['@id']})
                return (from.connectedVariables as string[]).map((uri, index) => {
                    return promise
                        .then(response => this._variableConnectionTransformer.dtoToObject(response.data['hydra:member'][index]))
                })
            },
            from.openSilexUri,
        )
    }

    objectToFormInterface(from: SemiAutomaticVariable): Promise<(VariableFormInterface & ManagedVariableExtraInfoFormInterface)> {
        return Promise.resolve({
            name: from.name,
            shortName: from.shortName,
            type: from.type,
            format: from.format,
            formatLength: from.formatLength,
            pathLevel: from.pathLevel,
            defaultTrueValue: from.defaultTrueValue,
            repetitions: from.repetitions,
            unit: from.unit,
            required: from.mandatory,
            identifier: from.identifier,
            comment: from.comment,
            frameStart: from.start,
            frameEnd: from.end,
        })
    }

    formInterfaceToDto(from: VariableFormInterface & ManagedVariableExtraInfoFormInterface): SemiAutomaticVariableDto {
        return {
            name: from.name,
            shortName: from.shortName,
            repetitions: from.repetitions,
            unit: from.unit,
            pathLevel: from.pathLevel,
            comment: from.comment,
            format: from.format,
            formatLength: from.formatLength,
            defaultTrueValue: from.defaultTrueValue,
            type: from.type,
            mandatory: from.required,
            identifier: from.identifier,
            start: from.frameStart,
            end: from.frameEnd,
        }
    }

    formInterfaceToUpdateDto(from: VariableFormInterface & ManagedVariableExtraInfoFormInterface): SemiAutomaticVariableDto {
        return {
            ...this.formInterfaceToDto(from),
            type: undefined,
        }
    }

    set variableConnectionTransformer(value: VariableConnectionTransformer) {
        this._variableConnectionTransformer = value
    }
}
