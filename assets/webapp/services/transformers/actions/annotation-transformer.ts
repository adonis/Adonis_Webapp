import Annotation from '@adonis/webapp/models/annotation'
import { AnnotationDto } from '@adonis/webapp/services/http/entities/simple-dto/annotation-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class AnnotationTransformer extends AbstractTransformer<AnnotationDto, Annotation, any> {

  dtoToObject( from: AnnotationDto ): Annotation {
    return new Annotation(
        from['@id'],
        from.id,
        from.categories,
        from.image,
        from.keywords,
        from.name,
        from.target as string,
        from.targetType,
        new Date(from.timestamp),
        from.type,
        from.value,
    )
  }

  objectToFormInterface( from: Annotation ): Promise<any> {
    return undefined
  }

  formInterfaceToDto( from: any ): AnnotationDto {
    return undefined
  }
}
