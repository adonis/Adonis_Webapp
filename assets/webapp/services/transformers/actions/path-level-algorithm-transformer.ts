import PathLevelAlgorithmFormInterface from '@adonis/webapp/form-interfaces/path-level-algorithm-form-interface'
import PathLevelAlgorithm from '@adonis/webapp/models/path-level-algorithm'
import { PathLevelAlgorithmsDto } from '@adonis/webapp/services/http/entities/simple-dto/path-level-algorithms-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class PathLevelAlgorithmTransformer extends AbstractTransformer<PathLevelAlgorithmsDto, PathLevelAlgorithm, PathLevelAlgorithmFormInterface> {

  dtoToObject( from: PathLevelAlgorithmsDto ): PathLevelAlgorithm {
    return new PathLevelAlgorithm(
        from['@id'],
        from.id,
        from.pathLevel,
        from.move,
        from.startPoint,
        from.orderedLevelIris,
    )
  }

  objectToFormInterface( from: PathLevelAlgorithm ): Promise<PathLevelAlgorithmFormInterface> {
    return Promise.resolve({
      level: from.pathLevel,
      move: from.move,
      startPoint: from.startPoint,
      orderedLevelIris: from.orderedLevelIris,
    })
  }

  formInterfaceToDto( from: PathLevelAlgorithmFormInterface ): PathLevelAlgorithmsDto {
    return {
      pathLevel: from.level,
      move: from.move,
      startPoint: from.startPoint,
      orderedLevelIris: from.orderedLevelIris,
    }
  }
}
