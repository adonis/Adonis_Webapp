import VariableScaleFormInterface from '@adonis/webapp/form-interfaces/variable-scale-form-interface'
import VariableScale from '@adonis/webapp/models/variable-scale'
import { VariableScaleDto } from '@adonis/webapp/services/http/entities/simple-dto/variable-scale-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

export default class VariableScaleTransformer extends AbstractTransformer<VariableScaleDto, VariableScale, VariableScaleFormInterface> {

  dtoToObject( from: VariableScaleDto ): VariableScale {
    return new VariableScale(
        from['@id'],
        from.id,
        from.name,
        from.minValue,
        from.maxValue,
        from.open,
        from.values.sort( ( a, b ) => a.value - b.value ),
    )
  }

  objectToFormInterface( from: VariableScale ): Promise<VariableScaleFormInterface> {
    return Promise.resolve( {
      name: from.name,
      maxValue: from.maxValue,
      minValue: from.minValue,
      open: from.open,
      values: from.values,
    } )
  }

  formInterfaceToDto( from: VariableScaleFormInterface ): VariableScaleDto {
    return {
      name: from.name,
      maxValue: from.maxValue,
      minValue: from.minValue,
      open: from.open,
      values: from.values,
    }
  }
}
