import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import BlockDataView from '@adonis/webapp/models/data-view/block-data-view'
import ExperimentDataView from '@adonis/webapp/models/data-view/experiment-data-view'
import FactorDataView from '@adonis/webapp/models/data-view/factor-data-view'
import { IndividualDataView } from '@adonis/webapp/models/data-view/individual-data-view'
import ModalityDataView from '@adonis/webapp/models/data-view/modality-data-view'
import PlatformDataView from '@adonis/webapp/models/data-view/platform-data-view'
import SubBlockDataView from '@adonis/webapp/models/data-view/sub-block-data-view'
import { SurfacicUnitPlotDataView } from '@adonis/webapp/models/data-view/surfacic-unit-plot-data-view'
import TreatmentDataView from '@adonis/webapp/models/data-view/treatment-data-view'
import UnitPlotDataView from '@adonis/webapp/models/data-view/unit-plot-data-view'
import FieldMeasure from '@adonis/webapp/models/field-measure'
import { BlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/block-data-view-dto'
import { ExperimentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/experiment-data-view-dto'
import { IndividualDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/individual-data-view-dto'
import { PlatformDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/platform-data-view-dto'
import { SubBlockDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/sub-block-data-view-dto'
import { SurfacicUnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/surfacic-unit-plot-data-view-dto'
import { TreatmentDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/treatment-data-view-dto'
import { UnitPlotDataViewDto } from '@adonis/webapp/services/http/entities/data-view-dto/unit-plot-data-view-dto'
import { FieldMeasureDto } from '@adonis/webapp/services/http/entities/simple-dto/field-measure-dto'
import AbstractTransformer from '@adonis/webapp/services/transformers/actions/abstract-transformer'

/**
 *
 */
export default class FieldMeasureTransformer extends AbstractTransformer<FieldMeasureDto, FieldMeasure, any> {

    dtoToObject(from: FieldMeasureDto): FieldMeasure {
        return new FieldMeasure(
            from['@id'],
            from.id,
            from.measures.map(fromMeasure => ({
                iri: fromMeasure['@id'],
                value: fromMeasure.value,
                state: fromMeasure.state?.code,
                stateColor: fromMeasure.state?.color,
                repetition: fromMeasure.repetition,
                timestamp: new Date(fromMeasure.timestamp),
            })),
            from.fieldGenerations?.map(fromGeneration => ({
                index: fromGeneration.index,
                prefix: fromGeneration.prefix,
                numeralIncrement: fromGeneration.numeralIncrement,
                children: fromGeneration.children.map(child => this.dtoToObject(child)),
            })) ?? [],
            this.transformTarget(from.target as any, from.targetType),
            from.target as string,
            from.targetType,
            typeof from.variable === 'string' ? from.variable : from.variable?.['@id'],
            typeof from.variable === 'string' ? undefined : from.variable,
        )
    }

    formInterfaceToDto(from: any): FieldMeasureDto {

        return undefined
    }

    objectToFormInterface(from: FieldMeasure): Promise<any> {

        return Promise.resolve(undefined)
    }

    transformTarget(
        target: BlockDataViewDto | ExperimentDataViewDto | IndividualDataViewDto | SubBlockDataViewDto | SurfacicUnitPlotDataViewDto |
            UnitPlotDataViewDto | PlatformDataViewDto,
        targetType: PathLevelEnum,
    ): BlockDataView | ExperimentDataView | IndividualDataView | SubBlockDataView | SurfacicUnitPlotDataView | UnitPlotDataView |
        PlatformDataView {
        if (!target) {
            return null
        }
        switch (targetType) {
            case PathLevelEnum.PLATFORM:
                return new PlatformDataView(
                    target['@id'],
                    target.id,
                    (target as PlatformDataViewDto).name,
                )
            case PathLevelEnum.INDIVIDUAL:
                return new IndividualDataView(
                    target['@id'],
                    target.id,
                    (target as IndividualDataViewDto).number,
                    (target as IndividualDataViewDto).x,
                    (target as IndividualDataViewDto).y,
                    (target as IndividualDataViewDto).dead,
                    !(target as IndividualDataViewDto).appeared ? undefined : new Date((target as IndividualDataViewDto).appeared),
                    !(target as IndividualDataViewDto).disappeared ? undefined : new Date((target as IndividualDataViewDto).disappeared),
                    (target as IndividualDataViewDto).identifier,
                    this.transformTarget((target as IndividualDataViewDto).unitPlot, PathLevelEnum.UNIT_PLOT) as UnitPlotDataView,
                )
            case PathLevelEnum.UNIT_PLOT:
                return new UnitPlotDataView(
                    target['@id'],
                    target.id,
                    (target as UnitPlotDataViewDto).number,
                    this.transformTarget((target as UnitPlotDataViewDto).block, PathLevelEnum.BLOCK) as BlockDataView,
                    this.transformTarget((target as UnitPlotDataViewDto).subBlock, PathLevelEnum.SUB_BLOCK) as SubBlockDataView,
                    this.transformTreatment((target as UnitPlotDataViewDto).treatment),
                )
            case PathLevelEnum.SURFACE_UNIT_PLOT:
                return new SurfacicUnitPlotDataView(
                    target['@id'],
                    target.id,
                    (target as SurfacicUnitPlotDataViewDto).number,
                    (target as SurfacicUnitPlotDataViewDto).x,
                    (target as SurfacicUnitPlotDataViewDto).y,
                    (target as SurfacicUnitPlotDataViewDto).dead,
                    !(target as SurfacicUnitPlotDataViewDto).appeared
                        ? undefined
                        : new Date((target as SurfacicUnitPlotDataViewDto).appeared),
                    !(target as SurfacicUnitPlotDataViewDto).disappeared
                        ? undefined
                        : new Date((target as SurfacicUnitPlotDataViewDto).disappeared),
                    (target as SurfacicUnitPlotDataViewDto).identifier,
                    this.transformTarget((target as SurfacicUnitPlotDataViewDto).block, PathLevelEnum.BLOCK) as BlockDataView,
                    this.transformTarget((target as SurfacicUnitPlotDataViewDto).subBlock, PathLevelEnum.SUB_BLOCK) as SubBlockDataView,
                    this.transformTreatment((target as SurfacicUnitPlotDataViewDto).treatment),
                )
            case PathLevelEnum.BLOCK:
                return new BlockDataView(
                    target['@id'],
                    target.id,
                    (target as UnitPlotDataViewDto).number,
                    this.transformTarget((target as BlockDataViewDto).experiment, PathLevelEnum.EXPERIMENT) as ExperimentDataView,
                )
            case PathLevelEnum.SUB_BLOCK:
                return new SubBlockDataView(
                    target['@id'],
                    target.id,
                    (target as SubBlockDataViewDto).number,
                    this.transformTarget((target as SubBlockDataViewDto).block, PathLevelEnum.BLOCK) as BlockDataView,
                )
            case PathLevelEnum.EXPERIMENT:
                return new ExperimentDataView(
                    target['@id'],
                    target.id,
                    (target as ExperimentDataViewDto).name,
                    this.transformTarget((target as ExperimentDataViewDto).platform, PathLevelEnum.PLATFORM) as PlatformDataView,
                )
        }
        return null
    }

    private transformTreatment(treatment: TreatmentDataViewDto): TreatmentDataView {
        return new TreatmentDataView(
            treatment['@id'],
            treatment.id,
            treatment.name,
            treatment.shortName,
            treatment.modalities.map(modality => new ModalityDataView(
                modality['@id'],
                modality.id,
                modality.value,
                modality.shortName,
                new FactorDataView(
                    modality.factor['@id'],
                    modality.factor.id,
                    modality.factor.name,
                    modality.factor.order,
                ),
            )),
        )
    }

}
