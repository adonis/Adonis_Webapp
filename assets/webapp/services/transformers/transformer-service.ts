import WebappHttpService from '@adonis/webapp/services/http/webapp-http-service'
import AdvancedGroupRightTransformer from '@adonis/webapp/services/transformers/actions/advanced-group-right-transformer'
import AdvancedUserRightTransformer from '@adonis/webapp/services/transformers/actions/advanced-user-right-transformer'
import AlgorithmTransformer from '@adonis/webapp/services/transformers/actions/algorithm-transformer'
import AnnotationTransformer from '@adonis/webapp/services/transformers/actions/annotation-transformer'
import AttachmentTransformer from '@adonis/webapp/services/transformers/actions/attachment-transformer'
import BlockTransformer from '@adonis/webapp/services/transformers/actions/block-transformer'
import CloneExperimentTransformer from '@adonis/webapp/services/transformers/actions/clone-experiment-transformer'
import CloneProjectTransformer from '@adonis/webapp/services/transformers/actions/clone-project-transformer'
import ConnectedVariableDeviceTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variable-device-transformer'
import ConnectedVariablesGeneratorVariableTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-generator-variable-transformer'
import ConnectedVariablesSemiAutomaticVariableTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-semi-automatic-variable-transformer'
import ConnectedVariablesSimpleVariableTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-simple-variable-transformer'
import ConnectedVariablesVariableConnectionTransformer
  from '@adonis/webapp/services/transformers/actions/connected-variables/connected-variables-variable-connection-transformer'
import DataEntryFusionTransformer from '@adonis/webapp/services/transformers/actions/data-entry-fusion-transformer'
import ProjectDataDataFusionTransformer from '@adonis/webapp/services/transformers/actions/data-fusion/project-data-data-fusion-transformer'
import ProjectDataDataPathTransformer from '@adonis/webapp/services/transformers/actions/data-path/project-data-data-path-transformer'
import ProjectDataDataViewTransformer from '@adonis/webapp/services/transformers/actions/data-view/project-data-data-view-transformer'
import ViewDataBusinessObjectTransformer from '@adonis/webapp/services/transformers/actions/data-view/view-data-business-object-transformer'
import ViewDataItemTransformer from '@adonis/webapp/services/transformers/actions/data-view/view-data-item-transformer'
import DeviceTransformer from '@adonis/webapp/services/transformers/actions/device-transformer'
import EnvTransformer from '@adonis/webapp/services/transformers/actions/env-transformer'
import ExperimentTransformer from '@adonis/webapp/services/transformers/actions/experiment-transformer'
import FactorTransformer from '@adonis/webapp/services/transformers/actions/factor-transformer'
import FieldMeasureTransformer from '@adonis/webapp/services/transformers/actions/field-measure-transformer'
import BlockFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/block-full-transformer'
import ExperimentFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/experiment-full-transformer'
import FactorFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/factor-full-transformer'
import PlatformFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/platform-full-transformer'
import ProtocolFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/protocol-full-transformer'
import SubBlockFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/sub-block-full-transformer'
import SurfacicUnitPlotFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/surfacic-unit-plot-full-transformer'
import TreatmentFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/treatment-full-transformer'
import UnitPlotFullTransformer from '@adonis/webapp/services/transformers/actions/full-view/unit-plot-full-transformer'
import GeneratorVariableTransformer from '@adonis/webapp/services/transformers/actions/generator-variable-transformer'
import GraphicalConfigurationTransformer from '@adonis/webapp/services/transformers/actions/graphical-configuration-transformer'
import GraphicalTextZoneTransformer from '@adonis/webapp/services/transformers/actions/graphical-text-zone-transformer'
import IndividualTransformer from '@adonis/webapp/services/transformers/actions/individual-transformer'
import MobileDataEntryTransformer from '@adonis/webapp/services/transformers/actions/mobile-data-entry-transformer'
import MobileResponseTransformer from '@adonis/webapp/services/transformers/actions/mobile-response-transformer'
import MobileSessionTransformer from '@adonis/webapp/services/transformers/actions/mobile-session-transformer'
import ModalityTransformer from '@adonis/webapp/services/transformers/actions/modality-transformer'
import NorthIndicatorTransformer from '@adonis/webapp/services/transformers/actions/north-indicator-transformer'
import NoteTransformer from '@adonis/webapp/services/transformers/actions/note-transformer'
import OezNatureTransformer from '@adonis/webapp/services/transformers/actions/oez-nature-transformer'
import OpenSilexInstanceTransformer from '@adonis/webapp/services/transformers/actions/open-silex-instance-transformer'
import OutExperimentationZoneTransformer from '@adonis/webapp/services/transformers/actions/out-experimentation-zone-transformer'
import ParametersTransformer from '@adonis/webapp/services/transformers/actions/parameters-transformer'
import PathBaseTransformer from '@adonis/webapp/services/transformers/actions/path-base-transformer'
import PathFilterNodeTransformer from '@adonis/webapp/services/transformers/actions/path-filter-node-transformer'
import PathLevelAlgorithmTransformer from '@adonis/webapp/services/transformers/actions/path-level-algorithm-transformer'
import PlatformTransformer from '@adonis/webapp/services/transformers/actions/platform-transformer'
import ProjectDataTransformer from '@adonis/webapp/services/transformers/actions/project-data-transformer'
import ProjectTransformer from '@adonis/webapp/services/transformers/actions/project-transformer'
import ProtocolTransformer from '@adonis/webapp/services/transformers/actions/protocol-transformer'
import RelUserSiteTransformer from '@adonis/webapp/services/transformers/actions/rel-user-site-transformer'
import RequestFileTransformer from '@adonis/webapp/services/transformers/actions/request-file-transformer'
import RequiredAnnotationTransformer from '@adonis/webapp/services/transformers/actions/required-annotation-transformer'
import ResponseFileTransformer from '@adonis/webapp/services/transformers/actions/response-file-transformer'
import SemiAutomaticVariableTransformer from '@adonis/webapp/services/transformers/actions/semi-automatic-variable-transformer'
import SessionTransformer from '@adonis/webapp/services/transformers/actions/session-transformer'
import SimpleVariableTransformer from '@adonis/webapp/services/transformers/actions/simple-variable-transformer'
import SiteTransformer from '@adonis/webapp/services/transformers/actions/site-transformer'
import StateCodeTransformer from '@adonis/webapp/services/transformers/actions/state-code-transformer'
import StatusDataEntryTransformer from '@adonis/webapp/services/transformers/actions/status-data-entry-transformer'
import SubBlockTransformer from '@adonis/webapp/services/transformers/actions/sub-block-transformer'
import SurfacicUnitPlotTransformer from '@adonis/webapp/services/transformers/actions/surfacic-unit-plot-transformer'
import TestTransformer from '@adonis/webapp/services/transformers/actions/test-transformer'
import TreatmentTransformer from '@adonis/webapp/services/transformers/actions/treatment-transformer'
import UnitPlotTransformer from '@adonis/webapp/services/transformers/actions/unit-plot-transformer'
import UserGroupTransformer from '@adonis/webapp/services/transformers/actions/user-group-transformer'
import UserPathTransformer from '@adonis/webapp/services/transformers/actions/user-path-transformer'
import UserTransformer from '@adonis/webapp/services/transformers/actions/user-transformer'
import ValueListTransformer from '@adonis/webapp/services/transformers/actions/value-list-transformer'
import VariableConnectionTransformer from '@adonis/webapp/services/transformers/actions/variable-connection-transformer'
import VariableScaleTransformer from '@adonis/webapp/services/transformers/actions/variable-scale-transformer'

export default class TransformerService {

  public siteTransformer: SiteTransformer
  public userTransformer: UserTransformer
  public relUserSiteTransformer: RelUserSiteTransformer
  public factorTransformer: FactorTransformer
  public modalityTransformer: ModalityTransformer
  public treatmentTransformer: TreatmentTransformer
  public protocolTransformer: ProtocolTransformer
  public userGroupTransformer: UserGroupTransformer
  public advancedUserRightTransformer: AdvancedUserRightTransformer
  public advancedGroupRightTransformer: AdvancedGroupRightTransformer
  public experimentTransformer: ExperimentTransformer
  public blockTransformer: BlockTransformer
  public subBlockTransformer: SubBlockTransformer
  public unitPlotTransformer: UnitPlotTransformer
  public surfacicUnitPlotTransformer: SurfacicUnitPlotTransformer
  public outExperimentationZoneTransformer: OutExperimentationZoneTransformer
  public platformTransformer: PlatformTransformer
  public simpleVariableTransformer: SimpleVariableTransformer
  public valueListTransformer: ValueListTransformer
  public stateCodeTransformer: StateCodeTransformer
  public generatorVariableTransformer: GeneratorVariableTransformer
  public deviceTransformer: DeviceTransformer
  public semiAutomaticVariableTransformer: SemiAutomaticVariableTransformer
  public projectTransformer: ProjectTransformer
  public testTransformer: TestTransformer
  public statusDataEntryTransformer: StatusDataEntryTransformer
  public projectDataTransformer: ProjectDataTransformer
  public sessionTransformer: SessionTransformer
  public fieldMeasureTransformer: FieldMeasureTransformer
  public individualTransformer: IndividualTransformer
  public pathBaseTransformer: PathBaseTransformer
  public pathFilterNodeTransformer: PathFilterNodeTransformer
  public pathLevelAlgorithmTransformer: PathLevelAlgorithmTransformer
  public pathUserWorkflowTransformer: UserPathTransformer
  public variableScaleTransformer: VariableScaleTransformer
  public graphicalConfigurationTransformer: GraphicalConfigurationTransformer
  public noteTransformer: NoteTransformer
  public requiredAnnotationTransformer: RequiredAnnotationTransformer
  public oezNatureTransformer: OezNatureTransformer
  public algorithmTransformer: AlgorithmTransformer
  public variableConnectionTransformer: VariableConnectionTransformer
  public requestFileTransformer: RequestFileTransformer
  public responseFileTransformer: ResponseFileTransformer
  public dataEntryFusionTransformer: DataEntryFusionTransformer
  public cloneProjectTransformer: CloneProjectTransformer
  public attachmentTransformer: AttachmentTransformer
  public annotationTransformer: AnnotationTransformer
  public northIndicatorTransformer: NorthIndicatorTransformer
  public graphicalTextZoneTransformer: GraphicalTextZoneTransformer
  public parametersTransformer: ParametersTransformer
  public mobileResponseTransformer: MobileResponseTransformer
  public mobileDataEntryTransformer: MobileDataEntryTransformer
  public mobileSessionTransformer: MobileSessionTransformer
  public cloneExperimentTransformer: CloneExperimentTransformer
  public envTransformer: EnvTransformer
  public openSilexInstanceTransformer: OpenSilexInstanceTransformer

  public factorFullTransformer: FactorFullTransformer
  public treatmentFullTransformer: TreatmentFullTransformer
  public protocolFullTransformer: ProtocolFullTransformer
  public experimentFullTransformer: ExperimentFullTransformer
  public platformFullTransformer: PlatformFullTransformer
  public blockFullTransformer: BlockFullTransformer
  public subBlockFullTransformer: SubBlockFullTransformer
  public unitPlotFullTransformer: UnitPlotFullTransformer
  public surfacicUnitPlotFullTransformer: SurfacicUnitPlotFullTransformer

  public projectDataDataViewTransformer: ProjectDataDataViewTransformer
  public viewDataItemTransformer: ViewDataItemTransformer
  public viewDataBusinessObjectTransformer: ViewDataBusinessObjectTransformer

  public projectDataDataPathTransformer: ProjectDataDataPathTransformer

  public projectDataDataFusionTransformer: ProjectDataDataFusionTransformer

  public connectedVariablesVariableConnectionTransformer: ConnectedVariablesVariableConnectionTransformer
  public connectedVariablesSimpleVariableTransformer: ConnectedVariablesSimpleVariableTransformer
  public connectedVariablesGeneratorVariableTransformer: ConnectedVariablesGeneratorVariableTransformer
  public connectedVariablesSemiAutomaticVariableTransformer: ConnectedVariablesSemiAutomaticVariableTransformer
  public connectedVariableDeviceTransformer: ConnectedVariableDeviceTransformer

  constructor(
      private httpService: WebappHttpService,
  ) {
    this.siteTransformer = new SiteTransformer( httpService )
    this.userTransformer = new UserTransformer( httpService )
    this.relUserSiteTransformer = new RelUserSiteTransformer( httpService )
    this.factorTransformer = new FactorTransformer( httpService )
    this.modalityTransformer = new ModalityTransformer( httpService )
    this.protocolTransformer = new ProtocolTransformer( httpService )
    this.treatmentTransformer = new TreatmentTransformer( httpService )
    this.userGroupTransformer = new UserGroupTransformer( httpService )
    this.advancedUserRightTransformer = new AdvancedUserRightTransformer( httpService )
    this.advancedGroupRightTransformer = new AdvancedGroupRightTransformer( httpService )
    this.experimentTransformer = new ExperimentTransformer( httpService )
    this.blockTransformer = new BlockTransformer( httpService )
    this.subBlockTransformer = new SubBlockTransformer( httpService )
    this.unitPlotTransformer = new UnitPlotTransformer( httpService )
    this.surfacicUnitPlotTransformer = new SurfacicUnitPlotTransformer( httpService )
    this.outExperimentationZoneTransformer = new OutExperimentationZoneTransformer( httpService )
    this.platformTransformer = new PlatformTransformer( httpService )
    this.simpleVariableTransformer = new SimpleVariableTransformer( httpService )
    this.generatorVariableTransformer = new GeneratorVariableTransformer( httpService )
    this.simpleVariableTransformer = new SimpleVariableTransformer( httpService )
    this.valueListTransformer = new ValueListTransformer( httpService )
    this.stateCodeTransformer = new StateCodeTransformer( httpService )
    this.deviceTransformer = new DeviceTransformer( httpService )
    this.semiAutomaticVariableTransformer = new SemiAutomaticVariableTransformer( httpService )
    this.projectTransformer = new ProjectTransformer( httpService )
    this.testTransformer = new TestTransformer( httpService )
    this.statusDataEntryTransformer = new StatusDataEntryTransformer( httpService )
    this.projectDataTransformer = new ProjectDataTransformer( httpService )
    this.sessionTransformer = new SessionTransformer( httpService )
    this.fieldMeasureTransformer = new FieldMeasureTransformer( httpService )
    this.individualTransformer = new IndividualTransformer( httpService )
    this.pathBaseTransformer = new PathBaseTransformer( httpService )
    this.pathFilterNodeTransformer = new PathFilterNodeTransformer( httpService )
    this.pathLevelAlgorithmTransformer = new PathLevelAlgorithmTransformer( httpService )
    this.pathUserWorkflowTransformer = new UserPathTransformer( httpService )
    this.variableScaleTransformer = new VariableScaleTransformer( httpService )
    this.graphicalConfigurationTransformer = new GraphicalConfigurationTransformer( httpService )
    this.noteTransformer = new NoteTransformer( httpService )
    this.requiredAnnotationTransformer = new RequiredAnnotationTransformer( httpService )
    this.oezNatureTransformer = new OezNatureTransformer( httpService )
    this.algorithmTransformer = new AlgorithmTransformer( httpService )
    this.variableConnectionTransformer = new VariableConnectionTransformer( httpService )
    this.requestFileTransformer = new RequestFileTransformer( httpService )
    this.responseFileTransformer = new ResponseFileTransformer( httpService )
    this.dataEntryFusionTransformer = new DataEntryFusionTransformer( httpService )
    this.cloneProjectTransformer = new CloneProjectTransformer( httpService )
    this.attachmentTransformer = new AttachmentTransformer( httpService )
    this.annotationTransformer = new AnnotationTransformer( httpService )
    this.northIndicatorTransformer = new NorthIndicatorTransformer( httpService )
    this.graphicalTextZoneTransformer = new GraphicalTextZoneTransformer( httpService )
    this.parametersTransformer = new ParametersTransformer( httpService )
    this.mobileResponseTransformer = new MobileResponseTransformer( httpService )
    this.mobileDataEntryTransformer = new MobileDataEntryTransformer( httpService )
    this.mobileSessionTransformer = new MobileSessionTransformer( httpService )
    this.cloneExperimentTransformer = new CloneExperimentTransformer( httpService )
    this.envTransformer = new EnvTransformer( httpService )
    this.openSilexInstanceTransformer = new OpenSilexInstanceTransformer( httpService )

    this.factorFullTransformer = new FactorFullTransformer( httpService )
    this.treatmentFullTransformer = new TreatmentFullTransformer( httpService )
    this.protocolFullTransformer = new ProtocolFullTransformer( httpService )
    this.experimentFullTransformer = new ExperimentFullTransformer( httpService )
    this.platformFullTransformer = new PlatformFullTransformer( httpService )
    this.blockFullTransformer = new BlockFullTransformer( httpService )
    this.subBlockFullTransformer = new SubBlockFullTransformer( httpService )
    this.unitPlotFullTransformer = new UnitPlotFullTransformer( httpService )
    this.surfacicUnitPlotFullTransformer = new SurfacicUnitPlotFullTransformer( httpService )

    this.projectDataDataViewTransformer = new ProjectDataDataViewTransformer(httpService)
    this.viewDataItemTransformer = new ViewDataItemTransformer(httpService)
    this.viewDataBusinessObjectTransformer = new ViewDataBusinessObjectTransformer(httpService)

    this.projectDataDataPathTransformer = new ProjectDataDataPathTransformer(httpService)

    this.projectDataDataFusionTransformer = new ProjectDataDataFusionTransformer(httpService)

    this.connectedVariablesVariableConnectionTransformer = new ConnectedVariablesVariableConnectionTransformer( httpService )
    this.connectedVariablesSimpleVariableTransformer = new ConnectedVariablesSimpleVariableTransformer( httpService )
    this.connectedVariablesGeneratorVariableTransformer = new ConnectedVariablesGeneratorVariableTransformer( httpService )
    this.connectedVariablesSemiAutomaticVariableTransformer = new ConnectedVariablesSemiAutomaticVariableTransformer( httpService )
    this.connectedVariableDeviceTransformer = new ConnectedVariableDeviceTransformer( httpService )

    this.siteTransformer.relUserSiteTransformer = this.relUserSiteTransformer
    this.userTransformer.relUserSiteTransformer = this.relUserSiteTransformer
    this.relUserSiteTransformer.siteTransformer = this.siteTransformer
    this.relUserSiteTransformer.userTransformer = this.userTransformer
    this.factorTransformer.modalityTransformer = this.modalityTransformer
    this.factorTransformer.openSilexInstanceTransformer = this.openSilexInstanceTransformer
    this.modalityTransformer.factorTransformer = this.factorTransformer
    this.modalityTransformer.openSilexInstanceTransformer = this.openSilexInstanceTransformer
    this.protocolTransformer.factorTransformer = this.factorTransformer
    this.protocolTransformer.treatmentTransformer = this.treatmentTransformer
    this.protocolTransformer.userTransformer = this.userTransformer
    this.protocolTransformer.algorithmTransformer = this.algorithmTransformer
    this.protocolTransformer.attachmentTransformer = this.attachmentTransformer
    this.treatmentTransformer.modalityTransformer = this.modalityTransformer
    this.userGroupTransformer.siteTransformer = this.siteTransformer
    this.userGroupTransformer.userTransformer = this.userTransformer
    this.experimentTransformer.protocolTransformer = this.protocolTransformer
    this.experimentTransformer.userTransformer = this.userTransformer
    this.experimentTransformer.noteTransformer = this.noteTransformer
    this.experimentTransformer.attachmentTransformer = this.attachmentTransformer
    this.blockTransformer.noteTransformer = this.noteTransformer
    this.subBlockTransformer.noteTransformer = this.noteTransformer
    this.unitPlotTransformer.noteTransformer = this.noteTransformer
    this.unitPlotTransformer.treatmentTransformer = this.treatmentTransformer
    this.surfacicUnitPlotTransformer.noteTransformer = this.noteTransformer
    this.surfacicUnitPlotTransformer.treatmentTransformer = this.treatmentTransformer
    this.platformTransformer.experimentTransformer = this.experimentTransformer
    this.platformTransformer.userTransformer = this.userTransformer
    this.platformTransformer.projectTransformer = this.projectTransformer
    this.platformTransformer.attachmentTransformer = this.attachmentTransformer
    this.platformTransformer.northIndicatorTransformer = this.northIndicatorTransformer
    this.platformTransformer.graphicalTextZoneTransformer = this.graphicalTextZoneTransformer
    this.simpleVariableTransformer.variableScaleTransformer = this.variableScaleTransformer
    this.simpleVariableTransformer.valueListTransformer = this.valueListTransformer
    this.simpleVariableTransformer.variableConnectionTransformer = this.variableConnectionTransformer
    this.simpleVariableTransformer.openSilexInstanceTransformer = this.openSilexInstanceTransformer
    this.generatorVariableTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.generatorVariableTransformer.variableConnectionTransformer = this.variableConnectionTransformer
    this.deviceTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.projectTransformer.userTransformer = this.userTransformer
    this.projectTransformer.platformTransformer = this.platformTransformer
    this.projectTransformer.experimentTransformer = this.experimentTransformer
    this.projectTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.projectTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.projectTransformer.deviceTransformer = this.deviceTransformer
    this.projectTransformer.pathBaseTransformer = this.pathBaseTransformer
    this.projectTransformer.stateCodeTransformer = this.stateCodeTransformer
    this.projectTransformer.statusDataEntryTransformer = this.statusDataEntryTransformer
    this.testTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.testTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.testTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.statusDataEntryTransformer.projectTransformer = this.projectTransformer
    this.statusDataEntryTransformer.mobileResponseTransformer = this.mobileResponseTransformer
    this.statusDataEntryTransformer.userTransformer = this.userTransformer
    this.projectDataTransformer.sessionTransformer = this.sessionTransformer
    this.projectDataTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.projectDataTransformer.projectTransformer = this.projectTransformer
    this.projectDataTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.projectDataTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.sessionTransformer.fieldMeasureTransformer = this.fieldMeasureTransformer
    this.sessionTransformer.annotationTransformer = this.annotationTransformer
    this.pathBaseTransformer.pathFilterNodeTransformer = this.pathFilterNodeTransformer
    this.pathBaseTransformer.pathLevelAlgorithmTransformer = this.pathLevelAlgorithmTransformer
    this.pathBaseTransformer.userPathTransformer = this.pathUserWorkflowTransformer
    this.pathBaseTransformer.projectTransformer = this.projectTransformer
    this.pathUserWorkflowTransformer.userTransformer = this.userTransformer
    this.pathUserWorkflowTransformer.pathBaseTransformer = this.pathBaseTransformer
    this.individualTransformer.noteTransformer = this.noteTransformer
    this.pathFilterNodeTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.pathFilterNodeTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.pathFilterNodeTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.variableConnectionTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.variableConnectionTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.variableConnectionTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.mobileResponseTransformer.mobileDataEntryTransformer = this.mobileDataEntryTransformer
    this.mobileDataEntryTransformer.mobileSessionTransformer = this.mobileSessionTransformer
    this.openSilexInstanceTransformer.siteTransformer = this.siteTransformer

    this.factorFullTransformer.modalityTransformer = this.modalityTransformer
    this.protocolFullTransformer.factorTransformer = this.factorFullTransformer
    this.protocolFullTransformer.treatmentTransformer = this.treatmentFullTransformer
    this.protocolFullTransformer.userTransformer = this.userTransformer
    this.protocolFullTransformer.algorithmTransformer = this.algorithmTransformer
    this.treatmentFullTransformer.modalityTransformer = this.modalityTransformer
    this.experimentFullTransformer.protocolTransformer = this.protocolFullTransformer
    this.experimentFullTransformer.userTransformer = this.userTransformer
    this.experimentFullTransformer.blockTransformer = this.blockFullTransformer
    this.experimentFullTransformer.noteTransformer = this.noteTransformer
    this.experimentFullTransformer.outExperimentationZoneFullTransformer = this.outExperimentationZoneTransformer
    this.platformFullTransformer.experimentTransformer = this.experimentFullTransformer
    this.platformFullTransformer.userTransformer = this.userTransformer
    this.platformFullTransformer.northIndicatorTransformer = this.northIndicatorTransformer
    this.platformFullTransformer.graphicalTextZoneTransformer = this.graphicalTextZoneTransformer
    this.blockFullTransformer.subBlockTransformer = this.subBlockFullTransformer
    this.blockFullTransformer.unitPlotTransformer = this.unitPlotFullTransformer
    this.blockFullTransformer.surfacicUnitPlotTransformer = this.surfacicUnitPlotFullTransformer
    this.blockFullTransformer.noteTransformer = this.noteTransformer
    this.blockFullTransformer.outExperimentationZoneFullTransformer = this.outExperimentationZoneTransformer
    this.subBlockFullTransformer.unitPlotTransformer = this.unitPlotFullTransformer
    this.subBlockFullTransformer.surfacicUnitPlotTransformer = this.surfacicUnitPlotFullTransformer
    this.subBlockFullTransformer.noteTransformer = this.noteTransformer
    this.subBlockFullTransformer.outExperimentationZoneFullTransformer = this.outExperimentationZoneTransformer
    this.unitPlotFullTransformer.treatmentTransformer = this.treatmentFullTransformer
    this.unitPlotFullTransformer.individualTransformer = this.individualTransformer
    this.unitPlotFullTransformer.noteTransformer = this.noteTransformer
    this.unitPlotFullTransformer.outExperimentationZoneFullTransformer = this.outExperimentationZoneTransformer
    this.surfacicUnitPlotFullTransformer.treatmentTransformer = this.treatmentFullTransformer
    this.outExperimentationZoneTransformer.oezNatureTransformer = this.oezNatureTransformer
    this.surfacicUnitPlotFullTransformer.noteTransformer = this.noteTransformer

    this.projectDataDataViewTransformer.sessionTransformer = this.sessionTransformer
    this.projectDataDataViewTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.projectDataDataViewTransformer.projectTransformer = this.projectTransformer
    this.projectDataDataViewTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.projectDataDataViewTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.viewDataItemTransformer.fieldMeasureTransformer = this.fieldMeasureTransformer

    this.projectDataDataPathTransformer.sessionTransformer = this.sessionTransformer
    this.projectDataDataPathTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.projectDataDataPathTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.projectDataDataPathTransformer.simpleVariableTransformer = this.simpleVariableTransformer

    this.projectDataDataFusionTransformer.semiAutomaticVariableTransformer = this.semiAutomaticVariableTransformer
    this.projectDataDataFusionTransformer.generatorVariableTransformer = this.generatorVariableTransformer
    this.projectDataDataFusionTransformer.simpleVariableTransformer = this.simpleVariableTransformer

    this.connectedVariablesVariableConnectionTransformer.connectedVariablesGeneratorVariableTransformer = this.connectedVariablesGeneratorVariableTransformer
    this.connectedVariablesVariableConnectionTransformer.connectedVariablesSimpleVariableTransformer = this.connectedVariablesSimpleVariableTransformer
    this.connectedVariablesVariableConnectionTransformer.connectedVariablesSemiAutomaticVariableTransformer = this.connectedVariablesSemiAutomaticVariableTransformer
    this.connectedVariablesSimpleVariableTransformer.variableScaleTransformer = this.variableScaleTransformer
    this.connectedVariablesSimpleVariableTransformer.valueListTransformer = this.valueListTransformer
    this.connectedVariablesSimpleVariableTransformer.connectedVariablesVariableConnectionTransformer = this.connectedVariablesVariableConnectionTransformer
    this.connectedVariablesGeneratorVariableTransformer.simpleVariableTransformer = this.simpleVariableTransformer
    this.connectedVariablesGeneratorVariableTransformer.connectedVariablesVariableConnectionTransformer = this.connectedVariablesVariableConnectionTransformer
    this.connectedVariablesSemiAutomaticVariableTransformer.connectedVariablesVariableConnectionTransformer = this.connectedVariablesVariableConnectionTransformer
    this.connectedVariableDeviceTransformer.connectedVariablesSemiAutomaticVariableTransformer = this.connectedVariablesSemiAutomaticVariableTransformer
  }

}
