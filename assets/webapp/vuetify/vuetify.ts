import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import en from 'vuetify/src/locale/en'
import fr from 'vuetify/src/locale/fr'
import { Iconfont } from 'vuetify/types/services/icons'

Vue.use( Vuetify )

export const userColor = 'white'
export const footerColor = 'tertiary'
export const menuColor = 'white'

const CONFIG = {
  lang: {
    current: 'fr',
    locales: { fr, en },
  },
  icons: {
    iconfont: 'md' as Iconfont,
  },
  theme: {
    themes: {
      light: {
        primary: '#678815',
        secondary: '#22316C',
        tertiary: '#c9b8c2',
        error: '#e10000',
        success: '#678815',
        info: '#2196f3',
      },
    },
  },
}

export default new Vuetify( CONFIG )
