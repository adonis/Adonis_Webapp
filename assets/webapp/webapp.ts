import '@adonis/shared/plugins/moment-vue'
import '@adonis/shared/plugins/portal-vue'
import i18n from '@adonis/webapp/i18n/vue-i18n'
import router from '@adonis/webapp/router/vue-router'
import store from '@adonis/webapp/stores/vuex'
import vuetify from '@adonis/webapp/vuetify/vuetify'

import Webapp from './Webapp.vue'

// Montage de l'application Vue avec pour racine le composant App dans la div id="app" de l'html.
new Webapp( {
  vuetify,
  router,
  store,
  i18n,
} ).$mount( '#app' )
