import shared from '@adonis/shared/translations/shared'
import commons from '@adonis/webapp/i18n/commons/commons'
import enums from '@adonis/webapp/i18n/enums/enums'
import notifications from '@adonis/webapp/i18n/notifications/notifications'
import serverErrors from '@adonis/webapp/i18n/notifications/server-errors'
import files from './files/files'
import modules from './modules/modules'
import navigation from './navigations/navigation'
import superAdmin from './super-admin/super-admin'
import userInformations from './user-informations/user-informations'

export default {
  en: {
    shared: shared.en,
    commons: commons.en,
    navigation: navigation.en,
    modules: modules.en,
    enums: enums.en,
    notifications: notifications.en,
    superAdmin: superAdmin.en,
    userInformations: userInformations.en,
    files: files.en,
    serverErrors: serverErrors.en,
  },
  fr: {
    shared: shared.fr,
    commons: commons.fr,
    navigation: navigation.fr,
    modules: modules.fr,
    enums: enums.fr,
    notifications: notifications.fr,
    superAdmin: superAdmin.fr,
    userInformations: userInformations.fr,
    files: files.fr,
    serverErrors: serverErrors.fr,
  },
}
