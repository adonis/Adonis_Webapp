import csv from './csv'
import pdf from './pdf'

export default {
  en: {
    csv: csv.en,
    pdf: pdf.en,
  },
  fr: {
    csv: csv.fr,
    pdf: pdf.fr,
  },
}
