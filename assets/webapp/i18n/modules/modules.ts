import admin from '@adonis/webapp/i18n/modules/admin'
import data from '@adonis/webapp/i18n/modules/data'
import design from '@adonis/webapp/i18n/modules/design'
import project from '@adonis/webapp/i18n/modules/project'
import transfer from '@adonis/webapp/i18n/modules/transfer'

export default {
  en: {
    design: design.en,
    project: project.en,
    transfer: transfer.en,
    data: data.en,
    admin: admin.en,
    commons: {
      form: {
        error: {
          isRequired: 'is required',
        },
      },
    },
  },
  fr: {
    design: design.fr,
    project: project.fr,
    transfer: transfer.fr,
    data: data.fr,
    admin: admin.fr,
    commons: {
      form: {
        error: {
          isRequired: 'est requis',
        },
      },
    },
  },
}
