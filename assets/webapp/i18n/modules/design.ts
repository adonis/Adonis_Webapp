import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import VueI18n from 'vue-i18n'

export default {
  en: {
    label: 'Design',
    forms: {
      platform: {
        formName: '@:(enums.objectTypes.platform)',
        fields: {
          name: 'Name of @.lower:(enums.objectTypes.platform)',
          location: 'Location',
          site: 'Site',
          creator: 'Name of the creator',
          dateOfCreation: 'Date of creation',
          comment: 'Comment',
          experiments: {
            listName: 'Linked @.lower:(enums.objectTypes.experiment)s',
            listEmpty: 'No linked @.lower:(enums.objectTypes.experiment)',
          },
        },
        errors: {
          nameIsRequired: '@:modules.design.forms.platform.name @:modules.commons.form.error.isRequired',
          locationIsRequired: '@:modules.design.forms.platform.location @:modules.commons.form.error.isRequired',
          siteIsRequired: '@:modules.design.forms.platform.site @:modules.commons.form.error.isRequired',
          nameVerification: 'Name is being verified',
          nameAlreadyExist: 'Name already in use',
        },
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      platformGraphicInfos: {
        formName: '@:(enums.objectTypes.platform)',
        xMesh: 'Grid width',
        yMesh: 'Grid height',
        origin: 'Graphical origin',
        errors: {
          meshIsRequired: 'Grid infos are required',
          meshBounds: 'Size must be between 50 and 150',
        },
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      linkExperiment: {
        formName: 'Link experiment to the Platform',
        fields: {
          name: 'Name of @.lower:(enums.objectTypes.platform)',
          experiments: {
            listName: 'List of linked @.lower:(enums.objectTypes.experiment)s',
            listEmpty: 'No linked @.lower:(enums.objectTypes.experiment)',
          },
        },
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      experimentSelection: {
        formName: 'Select between unlinked @.lower:(enums.objectTypes.experiment)s',
        emptyList: 'No @.lower:(enums.objectTypes.experiment) available',
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      oezNature: {
        formName: 'Out experimentation zone kind',
        nature: 'Nature',
        color: 'Color',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        colorChooser: {
          btn: 'Choose a color',
          title: 'Color chooser',
          cancel: '@:commons.cancel',
          validate: '@:commons.validate',
        },
        texture: 'Texture',
        textureChooser: {
          btn: 'Choose a texture',
          title: 'Texture chooser',
          cancel: '@:commons.cancel',
          validate: '@:commons.validate',
        },
        error: {
          nature: {
            empty: 'Nature is mandatory',
            verification: 'Nature is beeing verified',
            exists: 'Nature already exists',
          },
          color: {
            empty: 'Color must be set',
          },
        },
      },
      factor: {
        formName: 'Factor',
        name: 'Factor\'s name',
        modalities: {
          listName: 'Modalities',
          value: 'Value',
          shortName: 'Short name',
          id: 'Identifier',
          addAction: 'Add modality',
        },
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          name: {
            empty: 'Factor name must be set',
          },
          modalityValue: {
            duplicate: 'Modality value must be unique',
            empty: 'Modality Value must be set',
          },
          nameVerification: 'Name is being verified',
          nameAlreadyExist: 'Name already in use',
          modalityRequired: 'A modality is required',
        },
      },
      protocol: {
        formName: 'Protocol',
        name: 'name',
        creator: 'Creator\'s Name',
        creationDate: 'Creation date',
        aim: 'Aim',
        comment: 'Comments',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          nameIsRequired: '@:modules.design.forms.protocol.name @:modules.commons.form.error.isRequired',
          aimIsRequired: '@:modules.design.forms.protocol.aim @:modules.commons.form.error.isRequired',
          nameVerification: 'Name is being verified',
          nameAlreadyExist: 'Name already in use',
        },
      },
      algorithm: {
        formName: 'Randomizer algorithm',
        algorithm: 'Algorithm',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          algorithmIsRequired: '@:modules.design.forms.algorithm.algorithm @:modules.commons.form.error.isRequired',
        },
      },
      factorSelection: {
        formName: 'Factor selection',
        availableFactors: 'Available factors',
        noFactor: 'No available factors',
        selectedFactors: 'Selected factors',
        addFactor: 'Add factor',
        addGermplasm: 'Add germplasm',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        addFactorToLib: 'Add new factor to the library',
      },
      modalitySelection: {
        formName: 'Modality selection',
        availableModalities: 'Available modalities',
        noModality: 'Each factor must have at least a selected modality',
        selectedModalities: 'Selected modalities',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
      },
      treatments: {
        formName: 'Treatments',
        name: 'Name',
        shortName: 'Short name',
        repetitions: 'Repetition',
        applyRepetitions: 'Apply',
        deletedTreatments: 'Deleted treatments',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          shortNameIsRequired: '@:modules.design.forms.treatments.shortName @:modules.commons.form.error.isRequired',
          repetitionIsRequired: '@:modules.design.forms.treatments.repetitions @:modules.commons.form.error.isRequired',
          repetitionInt: '@:modules.design.forms.treatments.repetitions must be an integer',
          repetitionNeg: '@:modules.design.forms.treatments.repetitions must be positive',
        },
      },
      experiment: {
        formName: 'Experiment',
        name: 'Name',
        creationDate: 'Creation date',
        unitPlotType: 'Unit plot type',
        protocol: 'Protocol',
        comment: 'Comment',
        addNewProtocol: 'Add a new protocol',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          nameIsRequired: '@:modules.design.forms.experiment.name @:modules.commons.form.error.isRequired',
          protocolIsRequired: '@:modules.design.forms.experiment.protocol @:modules.commons.form.error.isRequired',
          nameVerification: 'Name is being verified',
          nameAlreadyExist: 'Name already in use',
        },
      },
      algorithmConfig: {
        formName: 'Algorithm configuration',
        puNumber: 'Unit plot number',
        blockNumber: 'Block number',
        subBlockNumber: 'Sub block number',
        blocsByLine: 'Blocks per line',
        subBlockByLine: 'Sub block per line',
        puByLine: 'Unit plot per line',
        individualsByPuNumber: 'Individual by Unit plot number',
        individualByLine: 'Individual per line',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        splitPlot: {
          independentFactors: 'Independent factors',
          factor1: 'Factor 1',
          factor2: 'Factor 2',
          factor3: 'Factor 3',
        },
        error: {
          valueIsRequired: 'Value @:modules.commons.form.error.isRequired',
          valueIsPositive: 'Value must be positive',
          valueIsInteger: 'Value must be an integer',
          blockNumberInfUnitPlot: 'Block number must be lower than unit plot number',
          subBlockNumberInfUnitPlot: 'Sub block number must be lower than unit plot number',
          blockByLineInfBlockNumber: 'Blocks by line must be lower than total block number',
          subBlockByLineInfByBlock: 'Sub blocks by line must be lower than sub blocks by block',
          puByLineInfByParent: 'Unit plot by line must be lower than unit plot by parent',
          individualByLineInfByParent: 'Individuals by line must be lower than individual by unit plot',
        },
      },
      experimentState: {
        formName: 'Experiment state',
        name: 'Name',
        currentState: 'Current state',
        currentHint: 'Description',
        currentHintText: (ctx: VueI18n.MessageContext) => {
          switch (ctx.named('state')) {
            case ExperimentStateEnum.LOCKED:
              return 'Cannot be modified and can be used in a data entry project'
            case ExperimentStateEnum.CREATED:
              return 'Initial state'
            case ExperimentStateEnum.VALIDATED:
              return 'Cannot be modified'
            case ExperimentStateEnum.NON_UNLOCKABLE:
              return 'Sent on a mobile device'
          }
        },
        newState: 'New State',
        newHint: 'Consequences',
        newHintText: (ctx: VueI18n.MessageContext) => {
          switch (ctx.named('state')) {
            case ExperimentStateEnum.LOCKED:
              return 'Experiment will be unmodifiable. It will be available for a data entry project'
            case ExperimentStateEnum.CREATED:
              return 'Experiment will be editable again'
            case ExperimentStateEnum.VALIDATED:
              return 'Experiment will be unmodifiable. It will be available for a data entry project'
            default:
              return ''
          }
        },
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
      },
      importExperiment: {
        cardTitle: 'Import experiment from CSV file',
        separator: 'Separator used in the CSV file',
        individual: 'Individuals unit plots',
        surfaced: 'Surfaced unit plots',
        validate: 'Import',
        cancel: '@:commons.cancel',
        required: 'Mandatory field',
        withGeometry: 'With geometry attribute (WKT format)',
        withoutGeometry: 'Without geometry attribute',
        withOpenSilexUri: 'Import OpenSilex uris',
        withoutOpenSilexUri: 'Doesn\'t include OpenSilex uris',
        openSilexInstance: 'OpenSilex instance',
        selects: {
          x: 'X',
          y: 'Y',
          id: 'Identifier',
          block: 'Block',
          blockGeometry: 'Block geometry',
          device: 'Experiment',
          shortTreatment: 'Treatment (Short)',
          unitParcel: 'Unit Plot',
          unitPlotGeometry: 'UP geometry',
          subBlock: 'Sub block',
          subBlockGeometry: 'Sub block geometry',
          treatment: 'Treatment',
          outExperimentationZone: 'Out experimentation zone',
          outExperimentationZoneGeometry: 'OEZ geometry ',
          factorName: 'Factor {idx}',
          factorShortName: 'Factor {idx} (Short)',
          factorId: 'Factor {idx} (Identifier)',
          factorOpenSilexUri: 'Factor {idx} (OpenSilex Germplasm Uri)',
          individualGeometry: 'Individual geometry',
          surfacicUpGeometry: 'Surfacic up geometry',
          experimentGeometry: 'Experiment geometry',
        },
      },
      importProtocol: {
        cardTitle: 'Import protocol from CSV file',
        separator: 'Separator used in the CSV file',
        protocolName: 'Protocol name',
        aim: 'Aim',
        comment: 'Comments',
        validate: 'Import',
        cancel: '@:commons.cancel',
        errors: {
          required: 'Mandatory field',
          nameVerification: 'Name is being verified',
          nameAlreadyExist: 'Name already in use',
        },
        selects: {
          algorithm: 'Algorithm',
          shortTreatment: 'Treatment (Short)',
          treatment: 'Treatment',
          repetitions: 'Repetitions',
          factorName: 'Factor {idx}',
          factorShortName: 'Factor {idx} (Short)',
          factorId: 'Factor {idx} (Identifier)',
        },
      },
      identificationCode: {
        formName: 'Identification codes',
        createCodes: 'Generate codes',
        importFromFile: 'Import from CSV',
        fileInput: 'CSV file',
        csvSeparator: 'CSV separator',
        xColumn: 'X column',
        yColumn: 'Y column',
        codeColumn: 'Code column',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        confirmDialog: {
          title: 'Confirm identification renew',
          message: 'Existing identification codes will be replaced by the new ones. Are you sure ?',
          validate: 'Yes',
          cancel: 'No',
        },
      },
      duplicateExperiment: {
        formName: 'Experiment copy',
        name: 'New name',
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      germplasmFetch: {
        formName: 'Germplasm import',
        name: 'Factor\'s name',
        openSilexInstance: 'OpenSilex instance',
        modalities: {
          openSilexUri: 'Germplasm uri',
          listName: 'Modalities',
          value: 'Value',
          shortName: 'Short name',
          id: 'Identifier',
          addAction: 'Add modality',
        },
        importGroup: 'Fetch germplasms by group',
        importGermplasm: 'Fetch germplasms',
        groupOpenSilexUri: 'Group Uri',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          name: {
            empty: 'Factor name must be set',
          },
          modalityValue: {
            duplicate: 'Modality value must be unique',
            empty: 'Modality Value must be set',
          },
          modalityOpenSilexUri: {
            duplicate: 'Modality uri must be unique',
            empty: 'Modality uri must be set',
          },
          groupOpenSilexUri: {
            empty: 'Group uri must be set',
          },
          openSilexInstance: {
            empty: 'OpenSilex instance must be set',
          },
          nameVerification: 'Name is being verified',
          nameAlreadyExist: 'Name already in use',
          modalityRequired: 'A modality is required',
        },
      },
      exportExperiment: {
        formName: 'Experiment OpenSilex export',
        openSilexInstance: 'OpenSilex instance',
        alreadyExported: 'Experiment already exported',
        alreadyExportedButDeleted: 'Experiment already exported but doesn\'t exists anymore on instance',
        disconnectedFromInstance: 'Experiment already exported but you\'re not connected on this instance',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          openSilexInstance: {
            empty: 'OpenSilex instance must be set',
          },
        },
      },
    },
    editors: {
      platform: {
        steps: {
          info: '@:(enums.objectTypes.platform) information step',
          addExperiment: 'Create',
          linkExperiment: 'Associate',
          experiment: '{platform} / Associate a @.lower:(enums.objectTypes.experiment)',
          addProtocol: 'Add a @.lower:(enums.objectTypes.protocol)',
          protocol: '{platform} / {experiment} / @:(enums.objectActions.create) a @.lower:(enums.objectTypes.protocol)',
        },
      },
      protocol: {
        stepBaseInformations: 'Base informations',
        stepFactorChoice: 'Linked factors',
        stepModality: 'Filter modalities',
        previous: '@:commons.previous',
        next: '@:commons.next',
        stepTreatments: 'Compose treatments',
        stepAlgorithm: 'Define randomization algorithm',
        notApplicableAlgorithm: 'Algorithm is not applicable to this parameters',
      },
      experiment: {
        stepExperiment: 'Experiment\'s informations',
        stepProtocol: 'Protocol\'s creation',
        stepAlgorithm: 'Algorithm configuration',
        previous: '@:commons.previous',
        next: '@:commons.next',
      },
    },
  },
  fr: {
    label: 'Conception',
    forms: {
      platform: {
        formName: '@:(enums.objectTypes.platform)',
        fields: {
          name: 'Nom de la @.lower:(enums.objectTypes.platform)',
          location: 'Lieu',
          site: 'Site',
          creator: 'Nom du créateur',
          dateOfCreation: 'Date de création',
          comment: 'Commentaire',
          experiments: {
            listName: '@:(enums.objectTypes.experiment)s associés',
            listEmpty: 'Aucun @.lower:(enums.objectTypes.experiment) associé',
          },
        },
        errors: {
          nameIsRequired: 'Le @.lower:(modules.design.forms.platform.fields.name) @:modules.commons.form.error.isRequired',
          locationIsRequired: 'Le @.lower:(modules.design.forms.platform.fields.location) @:modules.commons.form.error.isRequired',
          siteIsRequired: 'Le @.lower:(modules.design.forms.platform.fields.site) @:modules.commons.form.error.isRequired',
          nameVerification: 'Le nom est en cours de vérification',
          nameAlreadyExist: 'Le nom est déjà assigné',
        },
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      platformGraphicInfos: {
        formName: '@:(enums.objectTypes.platform)',
        xMesh: 'Largeur de la maille',
        yMesh: 'Hauteur de la maille',
        origin: 'Origine graphique',
        errors: {
          meshIsRequired: 'Informations sur la taille obligatoire',
          meshBounds: 'la taille doit être comprise entre 50 et 150',
        },
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      linkExperiment: {
        formName: 'Lier des dispositifs à la plateforme',
        fields: {
          name: 'Nom de la @.lower:(enums.objectTypes.platform)',
          experiments: {
            listName: 'Liste des @.lower:(enums.objectTypes.experiment)s associés',
            listEmpty: 'Aucun @.lower:(enums.objectTypes.experiment)',
          },
        },
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      experimentSelection: {
        formName: 'Sélectionner les @.lower:(enums.objectTypes.experiment)s à associer',
        emptyList: 'Aucun @.lower:(enums.objectTypes.experiment) disponible',
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      oezNature: {
        formName: 'Nature ZHE',
        nature: 'Nature',
        color: 'Couleur',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        colorChooser: {
          btn: 'Choisir une couleur',
          title: 'Choix de la couleur',
          cancel: '@:commons.cancel',
          validate: '@:commons.validate',
        },
        texture: 'Texture',
        textureChooser: {
          btn: 'Choisir une texture',
          title: 'Choix de la texture',
          cancel: '@:commons.cancel',
          validate: '@:commons.validate',
        },
        error: {
          nature: {
            empty: 'La nature est obligatoire',
            verification: 'La nature est en cours de vérification',
            exists: 'La nature existe déjà',
          },
          color: {
            empty: 'La couleur est obligatoire',
          },
        },
      },
      factor: {
        formName: 'Facteur',
        name: 'Nom du facteur',
        modalities: {
          listName: 'Modalités associées au facteur',
          value: 'Valeur',
          shortName: 'Nom court',
          id: 'Identifiant',
          addAction: 'Ajouter la modalité',
        },
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          name: {
            empty: 'Le nom du facteur est obligatoire',
          },
          modalityValue: {
            duplicate: 'La valeur de la modalité doit être unique',
            empty: 'La valeur de la modalité est obligatoire',
          },
          nameVerification: 'Le nom est en cours de vérification',
          nameAlreadyExist: 'Le nom est déjà assigné',
          modalityRequired: 'Une modalité est requise',
        },
      },
      protocol: {
        formName: 'Protocole',
        name: 'Nom',
        creator: 'Nom du créateur',
        creationDate: 'Date de création',
        aim: 'Objectifs',
        comment: 'Commentaires',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          nameIsRequired: '@:modules.design.forms.protocol.name @:modules.commons.form.error.isRequired',
          aimIsRequired: '@:modules.design.forms.protocol.aim @:modules.commons.form.error.isRequired',
          nameVerification: 'Le nom est en cours de vérification',
          nameAlreadyExist: 'Le nom est déjà assigné',
        },
      },
      algorithm: {
        formName: 'Algorithme de tirage',
        algorithm: 'Algorithme',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          algorithmIsRequired: '@:modules.design.forms.algorithm.algorithm @:modules.commons.form.error.isRequired',
        },
      },
      factorSelection: {
        formName: 'Selection de facteurs',
        availableFactors: 'Facteurs disponibles',
        noFactor: 'Aucun facteur n\'est disponible',
        selectedFactors: 'Facteurs sélectionnés',
        addFactor: 'Ajouter un facteur',
        addGermplasm: 'Ajouter un germplasm',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        addFactorToLib: 'Ajouter le facteur à la bibliothèque',
      },
      modalitySelection: {
        formName: 'Selection de modalités',
        availableModalities: 'Modalités disponibles',
        noModality: 'Chaque facteur doit avoir au moins une modalité sélectionnée',
        selectedModalities: 'Modalités sélectionnées',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
      },
      treatments: {
        formName: 'Traitements',
        name: 'Nom',
        shortName: 'Nom court',
        repetitions: 'Répétitions',
        applyRepetitions: 'Appliquer à tous',
        deletedTreatments: 'Traitements supprimés',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          shortNameIsRequired: 'Le @:modules.design.forms.treatments.shortName @:modules.commons.form.error.isRequired',
          repetitionIsRequired: 'Le nombre de @:modules.design.forms.treatments.repetitions @:modules.commons.form.error.isRequired',
          repetitionInt: 'Le nombre de @:modules.design.forms.treatments.repetitions doit être un entier',
          repetitionNeg: 'Le nombre de @:modules.design.forms.treatments.repetitions doit être positif',
        },
      },
      experiment: {
        formName: 'Dispositif',
        name: 'Nom',
        creationDate: 'Date de création',
        unitPlotType: 'type de parcelle unitaire',
        protocol: 'Protocole',
        comment: 'Commentaire',
        addNewProtocol: 'Ajouter',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          nameIsRequired: 'Le @:modules.design.forms.experiment.name @:modules.commons.form.error.isRequired',
          protocolIsRequired: 'Le @:modules.design.forms.experiment.protocol @:modules.commons.form.error.isRequired',
          nameVerification: 'Le nom est en cours de vérification',
          nameAlreadyExist: 'Le nom est déjà assigné',
        },
      },
      algorithmConfig: {
        formName: 'Configuration de l\'algorithme',
        puNumber: 'Nombre total de PU',
        blockNumber: 'Nombre de blocs',
        subBlockNumber: 'Nombre de sous blocs par blocs',
        blocsByLine: 'Nombre de blocs par ligne du dispositif',
        subBlockByLine: 'Nombre de sous blocs par ligne de bloc',
        puByLine: 'Nombre de PU par ligne de (sous) bloc',
        individualsByPuNumber: 'Nombre d\'individus par PU',
        individualByLine: 'Nombre d\'individus par ligne de PU',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        splitPlot: {
          independentFactors: 'Facteurs indépendants',
          factor1: 'Factor 1',
          factor2: 'Factor 2',
          factor3: 'Factor 3',
        },
        error: {
          valueIsRequired: 'La valeur @:modules.commons.form.error.isRequired',
          valueIsPositive: 'La valeur doit être positive',
          valueIsInteger: 'La valeur doit être entière',
          blockNumberInfUnitPlot: 'Le nombre de blocs doit être inférieur ou égal au nombre de PU',
          subBlockNumberInfUnitPlot: 'Le nombre de sous blocs doit être inférieur ou égal au nombre de PU',
          blockByLineInfBlockNumber: 'Le nombre de blocs par ligne doit être inférieur au nombre total de blocs',
          subBlockByLineInfByBlock: 'Le nombre de sous blocs par ligne doit être inférieur au nombre par bloc',
          puByLineInfByParent: 'Le nombre de PU par ligne doit être inferieur au nombre de PU par parent',
          individualByLineInfByParent: 'Le nombre d\'individus par ligne doit être inférieur au nombre d\'individus par PU',
        },
      },
      experimentState: {
        formName: 'Etat du dispositif',
        name: 'Nom',
        currentState: 'Etat actuel',
        currentHint: 'Description',
        currentHintText: (ctx: VueI18n.MessageContext) => {
          switch (ctx.named('state')) {
            case ExperimentStateEnum.LOCKED:
              return 'Ne peut pas être modifié mais est utilisable dans un projet de saisie'
            case ExperimentStateEnum.CREATED:
              return 'Etat initial'
            case ExperimentStateEnum.VALIDATED:
              return 'Ne peut pas être modifié'
            case ExperimentStateEnum.NON_UNLOCKABLE:
              return 'Envoyé sur un appareil mobile'
          }
        },
        newState: 'Nouvel état',
        newHint: 'Consequences',
        newHintText: (ctx: VueI18n.MessageContext) => {
          switch (ctx.named('state')) {
            case ExperimentStateEnum.LOCKED:
              return 'Le dispositif ne sera plus modifiable mais pourra être utilisé dans un projet. Cet état sera automatiquement atteint en cas de liaison à un projet de saisie'
            case ExperimentStateEnum.CREATED:
              return 'Le dispositif sera à nouveau éditable'
            case ExperimentStateEnum.VALIDATED:
              return 'Le dispositif ne sera plus modifiable mais pourra être utilisé dans un projet'
            default:
              return ''
          }
        },
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
      },
      importExperiment: {
        cardTitle: 'Importer un dispositif depuis un CSV',
        fileSelectionField: 'Fichier à importer',
        separator: 'Séparateur utilisé dans le CSV',
        individual: 'PU individuelles',
        surfaced: 'PU surfaciques',
        validate: 'Importer',
        cancel: '@:commons.cancel',
        required: 'Champ obligatoire',
        withGeometry: 'Avec attribut geometry (WKT format)',
        withoutGeometry: 'Sans attribut geometry',
        withOpenSilexUri: 'Importer les uris OpenSilex',
        withoutOpenSilexUri: 'Ne pas inclure les uris OpenSilex',
        openSilexInstance: 'Instance OpenSilex',
        selects: {
          x: 'X',
          y: 'Y',
          id: 'Identifiant',
          individualGeometry: 'Individu geometry',
          surfacicUpGeometry: 'Pu surfacique geometry',
          experimentGeometry: 'Dispositif geometry',
          block: 'Bloc',
          blockGeometry: 'Bloc geometry',
          device: 'Dispositif',
          shortTreatment: 'Traitement (court)',
          unitParcel: 'PU',
          unitPlotGeometry: 'PU geometry',
          subBlock: 'Sous-bloc',
          subBlockGeometry: 'Sous-bloc geometry',
          treatment: 'Traitement',
          outExperimentationZone: 'ZHE',
          outExperimentationZoneGeometry: 'ZHE geometry',
          factorName: 'Facteur {idx}',
          factorShortName: 'Facteur {idx} (nom court)',
          factorId: 'Facteur {idx} (Identifiant)',
          factorOpenSilexUri: 'Facteur {idx} (Uri OpenSilex germplasm)',
        },
      },
      importProtocol: {
        cardTitle: 'Importer un protocole depuis un CSV',
        fileSelectionField: 'Fichier à importer',
        protocolName: 'Nom du protocole',
        separator: 'Séparateur utilisé dans le CSV',
        aim: 'Objectifs',
        comment: 'Commentaires',
        validate: 'Importer',
        cancel: '@:commons.cancel',
        errors: {
          required: 'Champ obligatoire',
          nameVerification: 'Le nom est en cours de vérification',
          nameAlreadyExist: 'Le nom est déjà assigné',
        },
        selects: {
          algorithm: 'Algorithme',
          shortTreatment: 'Traitement (court)',
          treatment: 'Traitement',
          repetitions: 'Repetitions',
          factorName: 'Facteur {idx}',
          factorShortName: 'Facteur {idx} (nom court)',
          factorId: 'Facteur {idx} (Identifiant)',
        },
      },
      identificationCode: {
        formName: 'Codes d\'identification',
        createCodes: 'Générer les codes',
        importFromFile: 'Importer depuis un CSV',
        fileInput: 'Fichier CSV',
        csvSeparator: 'Séparateur CSV',
        xColumn: 'X',
        yColumn: 'Y',
        codeColumn: 'Code',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        confirmDialog: {
          title: 'Confirmer la génération (ou le remplacement) de codes d\'identification',
          message: 'Si des codes d\'identifications existent ils seront remplacés, êtes vous sûr ?',
          validate: 'Oui',
          cancel: 'Non',
        },
      },
      duplicateExperiment: {
        formName: 'Duplication de dispositif',
        name: 'Nouveau nom',
        cancel: '@:(commons.cancel)',
        validate: '@:(commons.validate)',
      },
      germplasmFetch: {
        formName: 'Import de germplasm',
        name: 'Nom du facteur',
        openSilexInstance: 'Instance OpenSilex',
        modalities: {
          openSilexUri: 'Uri du germplasm à importer',
          listName: 'Modalités associées au facteur',
          value: 'Valeur',
          shortName: 'Nom court',
          id: 'Identifiant',
          addAction: 'Ajouter la modalité',
        },
        importGroup: 'Importer des germplasms par groupe',
        importGermplasm: 'Importer des germplasms',
        groupOpenSilexUri: 'Uri du groupe',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          name: {
            empty: 'Le nom du facteur est obligatoire',
          },
          modalityValue: {
            duplicate: 'La valeur de la modalité doit être unique',
            empty: 'La valeur de la modalité est obligatoire',
          },
          modalityOpenSilexUri: {
            duplicate: 'L\'uri de la modalité doit être unique',
            empty: 'L\'uri de la modalité est obligatoire',
          },
          groupOpenSilexUri: {
            empty: 'L\'uri du groupe est obligatoire',
          },
          openSilexInstance: {
            empty: 'L\'instance OpenSilex est obligatoire',
          },
          nameVerification: 'Le nom est en cours de vérification',
          nameAlreadyExist: 'Le nom est déjà assigné',
          modalityRequired: 'Une modalité est requise',
        },
      },
      exportExperiment: {
        formName: 'Export de dispositif vers OpenSilex',
        openSilexInstance: 'Instance OpenSilex',
        alreadyExported: 'Dispositif déjà exporté',
        alreadyExportedButDeleted: 'Dispositif déjà exporté mais n\'est plus présent sur l\'instance OpenSilex, il sera restauré à l\'identique',
        disconnectedFromInstance: 'Dispositif déjà exporté mais la connexion a échoué',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          openSilexInstance: {
            empty: 'L\'instance OpenSilex est obligatoire',
          },
        },
      },
    },
    editors: {
      platform: {
        steps: {
          info: 'Informations générales concernant la @.lower:(enums.objectTypes.platform)',
          addExperiment: 'Ajouter',
          linkExperiment: 'Associer',
          experiment: '{platform} / Associer un @.lower:(enums.objectTypes.experiment)',
          addProtocol: 'Ajouter un @.lower:(enums.objectTypes.protocol)',
          protocol: '{platform} / {experiment} / @:(enums.objectActions.create) un @.lower:(enums.objectTypes.protocol)',
        },
      },
      protocol: {
        stepBaseInformations: 'Informations de base',
        stepFactorChoice: 'Définition des facteurs',
        stepModality: 'Filtre sur les modalités',
        previous: '@:commons.previous',
        next: '@:commons.next',
        stepTreatments: 'Composition des traitements',
        stepAlgorithm: 'Choix de l\'algorithme de tirage aléatoire',
        notApplicableAlgorithm: 'L\'algorithme n\'est pas utilisable avec ces paramètres',
      },
      experiment: {
        stepExperiment: 'Informations sur le dispositif',
        stepProtocol: 'Création d\'un protocole',
        stepAlgorithm: 'Configuration de l\'algorithme',
        previous: '@:commons.previous',
        next: '@:commons.next',
      },
    },
  },
}
