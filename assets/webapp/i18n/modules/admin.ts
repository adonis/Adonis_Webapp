export default {
    en: {
        label: 'Admin',
        editors: {
            siteDuplicate: {
                steps: {
                    nextBtn: '@:commons.next',
                    siteData: '@:enums.objectTypes.site data',
                    duplicationParams: 'Duplication parameters',
                },
            },
        },
        forms: {
            site: {
                formName: '@:enums.objectTypes.site',
                fields: {
                    name: '@:enums.objectTypes.site name',
                },
                errors: {
                    nameIsRequired: '@:modules.admin.forms.site.fields.name @:modules.commons.form.error.isRequired',
                    siteNameVerification: 'Site name is being verified',
                    siteNameAlreadyExist: 'Site name already in use',
                },
                cancel: '@:commons.cancel',
                validate: '@:commons.validate',
            },
            siteUser: {
                formName: '@:(enums.objectTypes.user)s',
                headers: {
                    userName: '@:enums.objectTypes.user',
                    userRole: '@:enums.objectTypes.user role',
                },
                actions: {
                    editBtn: 'Edit the @.lower:enums.objectTypes.user',
                    associateBtn: 'Associate a @.lower:enums.objectTypes.user',
                    removeBtn: 'Dissociate form @.lower:enums.objectTypes.site',
                },
            },
            siteDuplicate: {
                formName: 'Select data to be duplicate',
                fields: {
                    librariesCheck: 'Duplicate libraries',
                    usersCheck: 'Duplicate user associations',
                    platformsCheck: 'Duplicate platforms',
                    projectsCheck: 'Duplicate data entry projects',
                    entriesCheck: 'Duplicate data entries',
                },
                actions: {
                    validateBtn: 'Duplicate @.lower:enums.objectTypes.site',
                    cancelBtn: '@:commons.previous',
                },
                mail: 'The task will run in background. An email will be sent to you as soon as the site will be cloned',
                connVarAlert: 'If you used connected variables, corresponding data entries will be cloned too',
            },
            userGroup: {
                formName: 'User group',
                name: 'User group name',
                user: 'User',
                addUser: 'Add the user',
                cancel: '@:commons.cancel',
                validate: '@:commons.validate',
                errors: {
                    groupNameIsRequired: '@:modules.admin.forms.userGroup.name @:modules.commons.form.error.isRequired',
                    groupNameVerification: '@:modules.admin.forms.userGroup.name is in verification',
                    groupNameAlreadyExist: '@:modules.admin.forms.userGroup.name already used',
                },
                userList: {
                    nameColLabel: 'Name',
                    surnameColLabel: 'Surname',
                    usernameColLabel: 'Username',
                    actionColLabel: 'Actions',
                    actions: {
                        deleteBtn: 'Remove the user from group',
                    },
                },
            },
            openSilexInstance: {
                formName: 'OpenSilex instance',
                url: 'OpenSilex base url',
                hint: 'Exemple : https://opensilex.org/[organisation]',
                cancel: '@:commons.cancel',
                validate: '@:commons.validate',
                errors: {
                    urlIsRequired: 'url @:modules.commons.form.error.isRequired',
                    urlVerification: 'Url under validation please wait',
                    urlInvalid: 'Invalid Url, cannot contact instance',
                },
            },
        },
        views: {
            siteManage: {
                actions: {
                    createBtn: 'Create a new @.lower:enums.objectTypes.site vierge',
                    updateBtn: 'Update site name',
                    duplicateBtn: 'Duplicate the @.lower:enums.objectTypes.site',
                    deleteBtn: 'Delete',
                },
            },
        },
    },
    fr: {
        label: 'Administration',
        editors: {
            siteDuplicate: {
                steps: {
                    nextBtn: '@:commons.next',
                    siteData: 'Information concernant le nouveau @.lower:enums.objectTypes.site',
                    duplicationParams: 'Paramètre de la duplication',
                },
            },
        },
        forms: {
            site: {
                formName: '@:enums.objectTypes.site',
                fields: {
                    name: 'Nom du @.lower:enums.objectTypes.site',
                },
                errors: {
                    nameIsRequired: '@:modules.admin.forms.site.fields.name @:modules.commons.form.error.isRequired',
                    siteNameVerification: 'Le @:modules.admin.forms.site.fields.name est en cours de vérification',
                    siteNameAlreadyExist: 'Le @:modules.admin.forms.site.fields.name est déjà assigné',
                },
                cancel: '@:commons.cancel',
                validate: '@:commons.validate',
            },
            siteUser: {
                formName: '@:(enums.objectTypes.user)s',
                headers: {
                    userName: '@:enums.objectTypes.user',
                    userRole: 'Rôle @.lower:enums.objectTypes.user',
                },
                actions: {
                    editBtn: 'Editer l\'@.lower:enums.objectTypes.user',
                    associateBtn: 'Associer un @.lower:enums.objectTypes.user',
                    removeBtn: 'Dissocier du @.lower:enums.objectTypes.site',
                },
            },
            siteDuplicate: {
                formName: 'Sélection des éléments à dupliquer',
                fields: {
                    librariesCheck: 'Dupliquer les bibliothèques',
                    usersCheck: 'Associer les utilisateurs',
                    platformsCheck: 'Dupliquer les plateformes',
                    projectsCheck: 'Dupliquer les projets de saisie',
                    entriesCheck: 'Dupliquer les données saisies',
                },
                actions: {
                    validateBtn: 'Dupliquer le @.lower:enums.objectTypes.site',
                    cancelBtn: '@:commons.previous',
                },
                mail: 'La copie va être exécutée en tache de fond. Un email vous sera envoyé aussitôt l\'opération terminée.',
                connVarAlert: 'Si vous utilisez des variables connectées, Les saisies correspondantes seront dupliquées',
            },
            userGroup: {
                formName: 'Groupe d\'utilisateur',
                name: 'Nom du groupe',
                user: 'Utilisateur',
                addUser: 'Ajouter au groupe',
                cancel: '@:commons.cancel',
                validate: '@:commons.validate',
                errors: {
                    groupNameIsRequired: 'Le @:modules.admin.forms.userGroup.name @:modules.commons.form.error.isRequired',
                    groupNameVerification: 'Le @:modules.admin.forms.userGroup.name est en cours de vérification',
                    groupNameAlreadyExist: 'Le @:modules.admin.forms.userGroup.name est déjà utilisé',
                },
                userList: {
                    nameColLabel: 'Prénom',
                    surnameColLabel: 'Nom',
                    usernameColLabel: 'Login',
                    actionColLabel: 'Actions',
                    actions: {
                        deleteBtn: 'Retirer l\'utilisateur du groupe',
                    },
                },
            },
            openSilexInstance: {
                formName: 'Instance OpenSilex',
                url: 'OpenSilex base url',
                hint: 'Exemple : https://opensilex.org/[organisation]',
                cancel: '@:commons.cancel',
                validate: '@:commons.validate',
                errors: {
                    urlIsRequired: 'L\'url @:modules.commons.form.error.isRequired',
                    urlVerification: 'Url en cours de validation',
                    urlInvalid: 'Url invalide, impossible de contacter L\'instance OpenSilex',
                },
            },
        },
        views: {
            siteManage: {
                actions: {
                    createBtn: 'Créer un nouveau @.lower:enums.objectTypes.site vierge',
                    updateBtn: 'Modifier le nom',
                    duplicateBtn: 'Dupliquer le @.lower:enums.objectTypes.site',
                    deleteBtn: 'Supprimer',
                },
            },
        },
    },
}
