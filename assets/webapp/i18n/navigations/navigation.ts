import menus from '@adonis/webapp/i18n/navigations/menus'
// @ts-ignore
import propertyView from '@adonis/webapp/i18n/navigations/propertyView'

export default {
  en: {
    userMenu: {
      dashboard: {
        openLabel: 'Dashboard',
      },
      superAdmin: {
        openLabel: 'Super admin',
        infos: 'Manage users and sites',
      },
      siteAdmin: {
        openLabel: 'Site admin',
      },
      closeLabel: 'Close',
      logoutLabel: 'Logout',
      accountInformation: {
        openLabel: 'Change personal informations',
      },
    },
    module: {
      select: 'Select a module',
    },
    site: {
      select: 'Select a site',
      noSiteSelected: 'No site selected',
    },
    explorer: {
      library: {
        label: 'Libraries',
        factors: {
          label: 'Factors',
        },
        protocols: {
          label: 'Protocols',
          factors: 'Factors',
          treatments: 'Treatments',
        },
        unlinkedExperiments: {
          label: 'Unlinked experiments',
        },
        oezNatures: {
          label: 'Out of zone',
        },
        stateCodes: {
          label: 'State codes',
        },
        valueLists: {
          label: 'Value lists',
        },
        devices: {
          label: 'Devices',
        },
        variables: {
          label: 'Variables',
          independent: {
            label: 'Independent',
          },
          generator: {
            label: 'Generator',
          },
          automatic: {
            label: 'Automatic',
          },
        },
      },
      dataEntry: {
        inProgress: {
          label: 'Data entry project in progress',
        },
        dataEntries: {
          label: 'Data entries',
        },
      },
      sites: {
        label: 'Sites',
      },
      site: {
        label: 'Site',
      },
      user: {
        label: 'Users',
      },
      parameters: {
        label: 'Parameters',
      },
      experiment: {
        label: 'Unlinked experiments',
      },
      platform: {
        label: 'Platforms',
      },
      protocol: {
        label: 'Protocols',
      },
      dataEntryProject: {
        label: 'Data entry project',
        experiments: 'Experiments',
        path: 'Paths',
        variable: 'Variables',
        stateCode: 'State codes',
        annotation: 'Required annotation',
      },
      disabledObjects: {
        label: 'Disabled objects',
        experiments: 'Experiments',
        platforms: 'Platforms',
        projects: 'Data entry projects',
        protocols: 'Protocols',
      },
      userGroups: {
        label: 'User groups',
      },
      openSilexInstance: {
        label: 'OpenSilex Instances',
      },
    },
    menus: menus.en,
    propertyView: propertyView.en,
  },
  fr: {
    userMenu: {
      dashboard: {
        openLabel: 'Mon tableau de bord',
      },
      superAdmin: {
        openLabel: 'Super administrateur',
        infos: 'Gestion des utilisateurs et des sites',
      },
      siteAdmin: {
        openLabel: 'Administration du site',
      },
      closeLabel: 'Fermer',
      logoutLabel: 'Déconnecter',
      accountInformation: {
        openLabel: 'Mon profil',
      },
    },
    module: {
      select: 'Changer de module',
    },
    site: {
      select: 'Changer de site',
      noSiteSelected: 'Aucun site sélectionné',
    },
    explorer: {
      library: {
        label: 'Bibliothèques',
        factors: {
          label: 'Facteurs',
        },
        protocols: {
          label: 'Protocoles',
          factors: 'Facteurs',
          treatments: 'Traitements',
        },
        unlinkedExperiments: {
          label: 'Dispositifs non rattachés',
        },
        oezNatures: {
          label: 'Natures de ZHE',
        },
        stateCodes: {
          label: 'Code d\'état',
        },
        valueLists: {
          label: 'Liste de valeurs',
        },
        devices: {
          label: 'Matériels',
        },
        variables: {
          label: 'Variables',
          independent: {
            label: 'Indépendantes',
          },
          generator: {
            label: 'Génératrices',
          },
          automatic: {
            label: 'Semi-automatiques',
          },
        },
      },
      dataEntry: {
        inProgress: {
          label: 'Projet de saisie transféré',
        },
        dataEntries: {
          label: 'Saisies',
        },
      },
      sites: {
        label: 'Sites',
      },
      site: {
        label: 'Site',
      },
      users: {
        label: 'Utilisateurs',
      },
      parameters: {
        label: 'Paramètres',
      },
      experiment: {
        label: 'Dispositifs non affectés',
      },
      platform: {
        label: 'Plateformes',
      },
      protocol: {
        label: 'Protocoles',
      },
      dataEntryProject: {
        label: 'Projets de saisie',
        experiments: 'Dispositifs',
        path: 'Cheminements',
        variable: 'Variables',
        stateCode: 'Codes d\'état',
        annotation: 'Annotation à saisir',
      },
      disabledObjects: {
        label: 'Objets désactivés',
        experiments: 'Dispositifs',
        platforms: 'Plateformes',
        projects: 'Projets de saisie',
        protocols: 'Protocoles',
      },
      userGroups: {
        label: 'Groupes d\'utilisateurs',
      },
      openSilexInstance: {
        label: 'Instances OpenSilex',
      },
    },
    menus: menus.fr,
    propertyView: propertyView.fr,
  },
}
