export default {
  en: {
    forms: {
      userIcon: {
        formName: 'New user icon',
        avatar: 'User\'s avatar',
        validate: '@:commons.validate',
        errors: {
          fileSizeOverLimit: 'File size is over the limit : 100 ko',
          notImage: 'File must be a Gif, Jpeg or Png',
        },
      },
      changePassword: {
        formName: 'Change Password',
        info: 'Use this form to change your login password',
        passwordRules: {
          title: 'Password has to respect the following rules:',
          1: 'Must contains at least 1 lowercase character',
          2: 'Must contains at least 1 uppercase character',
          3: 'Must contains at least 1 digit OR 1 special character',
          4: 'Must be at least 8 characters long',
        },
        currentPasswordField: 'Current password',
        changePasswordField: 'New password',
        passwordRepeatField: 'Enter password again',
        incorrectCurrent: 'Current password is incorrect',
        required: 'Field is required',
        invalid: 'Invalid',
        mustMatch: 'Fields must match together',
        savePasswordActionLabel: 'Change password',
        savePasswordSuccess: 'Password changed',
      },
    },
  },
  fr: {
    forms: {
      userIcon: {
        formName: 'Nouvel icône utilisateur',
        avatar: 'Avatar utilisateur',
        validate: '@:commons.validate',
        errors: {
          fileSizeOverLimit: 'La taille du fichier dépasse la limite autorisée : 100 ko',
          notImage: 'Le fichier doit être au format Gif, Jpeg or Png',
        },
      },
      changePassword: {
        formName: 'Changement de mot de passe',
        info: 'Utiliser le formulaire ci-dessous pour modifier le mot de passe associé à votre compte',
        passwordRules: {
          title: 'Le mot de passe doit respecter les règles suivantes :',
          1: 'Doit contenir au moins 1 lettre en majuscule',
          2: 'Doit contenir au moins 1 lettre en minuscule',
          3: 'Doit contenir au moins 1 chiffre OU 1 caractère spécial',
          4: 'Doit être composé d\'au moins 8 caractères',
        },
        currentPasswordField: 'Mot de passe actuel',
        changePasswordField: 'Nouveau mot de passe',
        passwordRepeatField: 'Vérification nouveau mot de passe',
        incorrectCurrent: 'Current password is incorrect',
        required: 'Champ requis',
        invalid: 'Invalide',
        mustMatch: 'Les champs doivent correspondre',
        savePasswordActionLabel: 'Changer mon mot de passe',
        savePasswordSuccess: 'Mot de passe modifié',
      },
    },
  },
}
