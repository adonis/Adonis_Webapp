export default {
  en: {
    label: 'Super Admin',
    editors: {
      site: {
        stepSite: '@:enums.objectTypes.site information',
        addNewUser: '@:enums.objectActions.create',
        stepUser: '@:enums.objectTypes.user information',
        previous: '@:commons.previous',
      },
      siteList: {
        confirms: {
          deleteSite: {
            title: '@:enums.objectTypes.site deletion',
            message: 'Caution! the @.lower:enums.objectTypes.site "{siteName}" will be permanently deleted.',
            confirmLabel: 'Delete the @.lower:enums.objectTypes.site',
            cancelLabel: '@:commons.cancel',
          },
        },
      },
      userList: {
        confirms: {
          renewPassword: {
            title: '@:enums.objectTypes.user: Renew password',
            message: 'By clicking confirm, the password of @.lower:enums.objectTypes.user "{username}" will be regenerated. The password will be sent by email to the user.',
            confirmLabel: '@:commons.confirm',
            cancelLabel: '@:commons.cancel',
          },
          active: {
            title: '@:enums.objectTypes.user activation',
            message: 'By clicking confirm, the @.lower:(enums.objectTypes.user) "{username}" will be set as active.',
            confirmLabel: '@:commons.confirm',
            cancelLabel: '@:commons.cancel',
          },
          inactive: {
            title: '@:enums.objectTypes.user inactivation',
            message: 'By clicking confirm, the @.lower:enums.objectTypes.user "{username}" will be set as inactive.',
            confirmLabel: '@:commons.confirm',
            cancelLabel: '@:commons.cancel',
          },
          deleteUser: {
            title: '@:enums.objectTypes.user deletion',
            message: 'Caution! the @.lower:enums.objectTypes.user account "{username}" will be permanently deleted.',
            confirmLabel: 'Delete the @.lower:enums.objectTypes.user account',
            cancelLabel: '@:commons.cancel',
          },
        },
      },
    },
    forms: {
      parameter: {
        formName: 'Admin parameters',
        fields: {
          autoDeleteEmptyUser: 'Auto delete @.lower:enums.objectTypes.user when unrelated to a site',
          deleteEmptyUserDelay: 'Deletion delay (in day)',
          daysBeforeFileDelete: 'File deletion delay (in day)',
        },
        errors: {
          required: '@:superAdmin.forms.parameter.fields.daysBeforeFileDelete @:modules.commons.form.error.isRequired',
          positive: '@:superAdmin.forms.parameter.fields.daysBeforeFileDelete must be greater than 0',
        },
        cancel: '@:commons.cancel',
        validate: '@:commons.save',
      },
      site: {
        formName: '@:enums.objectTypes.site',
        siteName: '@:enums.objectTypes.site name',
        admin: '@:enums.objectTypes.site administrator',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        errors: {
          siteNameIsRequired: '@:superAdmin.forms.site.siteName @:modules.commons.form.error.isRequired',
          adminIsRequired: '@:superAdmin.forms.site.admin @:modules.commons.form.error.isRequired',
          siteNameVerification: 'Site name is being verified',
          siteNameAlreadyExist: 'Site name already in use',
        },
      },
    },
    views: {
      siteList: {
        viewName: '@:enums.objectTypes.site administration',
        labelColLabel: '@:enums.objectTypes.site name',
        actionColLabel: 'Actions',
        filters: {
          siteName: 'Filter on @.lower:enums.objectTypes.site name',
          deletable: 'Filter on deleted @.lower:enums.objectTypes.site',
          deletableValues: {
            deletable: 'Deleted only',
          },
        },
        actions: {
          editBtn: '@:enums.objectActions.modify @:enums.objectTypes.site data',
          associateBtn: 'Associate a @.lower:enums.objectTypes.user',
          deleteBtn: '@:enums.objectActions.delete @:enums.objectTypes.site',
        },
      },
      userList: {
        viewName: '@:enums.objectTypes.user administration',
        usernameColLabel: 'Login',
        nameColLabel: 'Name',
        surnameColLabel: 'Surname',
        siteColLabel: 'Sites',
        actionColLabel: 'Actions',
        filters: {
          userName: 'Filter on @.lower:enums.objectTypes.user login',
          active: 'Filter on @.lower:enums.objectTypes.user state',
          activeValues: {
            all: 'All',
            active: 'Active only',
            inactive: 'Inactive only',
          },
          deletable: 'Filter on @.lower:enums.objectTypes.user unrelated to a site',
          deletableValues: {
            deletable: 'Related only',
            notDeletable: 'Unrelated only',
          },
        },
        actions: {
          editBtn: '@:enums.objectActions.modify',
          autorenewBtn: 'Renew @.lower:enums.objectTypes.user password',
          inactiveBtn: 'Inactive @.lower:enums.objectTypes.user',
          activeBtn: 'active @.lower:enums.objectTypes.user',
          deleteBtn: 'delete @.lower:enums.objectTypes.user',
        },
      },
    },
  },
  fr: {
    label: 'Super Administration',
    editors: {
      site: {
        stepSite: 'Informations du @.lower:enums.objectTypes.site',
        addNewUser: '@:enums.objectActions.create',
        stepUser: 'Informations de l\'@.lower:enums.objectTypes.user',
        previous: '@:commons.previous',
      },
      siteList: {
        confirms: {
          deleteSite: {
            title: 'Suppression d\'un @.lower:enums.objectTypes.site',
            message: 'Attention ! Le @.lower:enums.objectTypes.site « {siteName} » sera définitivement supprimé.',
            confirmLabel: 'Supprimer le @.lower:enums.objectTypes.site',
            cancelLabel: '@:commons.cancel',
          },
        },
      },
      userList: {
        confirms: {
          renewPassword: {
            title: 'Mot de passe du compte @.lower:enums.objectTypes.user',
            message: 'Le mot de passe de l\'@.lower:enums.objectTypes.user « {username} » va être réinitialiser. Le mot de passe sera transmis à l\'utilisateur par email.',
            confirmLabel: '@:commons.confirm',
            cancelLabel: '@:commons.cancel',
          },
          active: {
            title: 'Activation du compte @.lower:enums.objectTypes.user',
            message: 'Le compte de l\'@.lower:enums.objectTypes.user « {username} » sera activé.',
            confirmLabel: '@:commons.confirm',
            cancelLabel: '@:commons.cancel',
          },
          inactive: {
            title: 'Désactivation du compte @.lower:enums.objectTypes.user',
            message: 'Le compte de l\'@.lower:enums.objectTypes.user « {username} » sera désactivé.',
            confirmLabel: '@:commons.confirm',
            cancelLabel: '@:commons.cancel',
          },
          deleteUser: {
            title: 'Suppression du compte @.lower:enums.objectTypes.user',
            message: 'Attention ! Le compte de l\'@.lower:enums.objectTypes.user « {username} » sera définitivement supprimé.',
            confirmLabel: 'Supprimer le compte',
            cancelLabel: '@:commons.cancel',
          },
        },
      },
    },
    forms: {
      parameter: {
        formName: 'Paramètres administrateur',
        fields: {
          autoDeleteEmptyUser: 'Auto suppression des @.lower:(enums.objectTypes.user)s non affectés',
          deleteEmptyUserDelay: 'Délai de suppression (en jours)',
          daysBeforeFileDelete: 'Délai avant suppression des fichiers (en jours)',
        },
        errors: {
          required: 'Le @.lower:superAdmin.forms.parameter.fields.daysBeforeFileDelete @:modules.commons.form.error.isRequired',
          positive: 'Le @.lower:superAdmin.forms.parameter.fields.daysBeforeFileDelete doit être supérieur à 0',
        },
        cancel: '@:commons.cancel',
        validate: '@:commons.save',
      },
      site: {
        formName: '@:enums.objectTypes.site',
        siteName: 'Nom du @.lower:enums.objectTypes.site',
        admin: 'Administrateur du @.lower:enums.objectTypes.site',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        errors: {
          siteNameIsRequired: 'Le @:superAdmin.forms.site.siteName @:modules.commons.form.error.isRequired',
          adminIsRequired: 'L\'@:superAdmin.forms.site.admin @:modules.commons.form.error.isRequired',
          siteNameVerification: 'Le @:superAdmin.forms.site.siteName est en cours de vérification',
          siteNameAlreadyExist: 'Le @:superAdmin.forms.site.siteName est déjà assigné',
        },
      },
    },
    views: {
      siteList: {
        viewName: 'Administration des @.lower:(enums.objectTypes.site)s',
        labelColLabel: 'Nom du @.lower:enums.objectTypes.site',
        actionColLabel: 'Actions',
        filters: {
          siteName: 'Filtre sur le nom du @.lower:enums.objectTypes.site',
          deletable: 'Filtre sur les @.lower:(enums.objectTypes.site)s supprimés',
          deletableValues: {
            deletable: 'Supprimés seulement',
          },
        },
        actions: {
          editBtn: '@:enums.objectActions.modify le @:enums.objectTypes.site',
          associateBtn: 'Associer un @.lower:enums.objectTypes.user',
          deleteBtn: '@:enums.objectActions.delete @:enums.objectTypes.site',
        },
      },
      userList: {
        viewName: 'Administration des @.lower:(enums.objectTypes.user)s',
        usernameColLabel: 'Login',
        nameColLabel: 'Prénom',
        surnameColLabel: 'Nom',
        siteColLabel: '@:(enums.objectTypes.site)s',
        actionColLabel: 'Actions',
        filters: {
          userName: 'Filtre sur le login de l\'@.lower:enums.objectTypes.user',
          active: 'Filtre sur le statut @.lower:enums.objectTypes.user',
          activeValues: {
            active: 'Actif seulement',
            inactive: 'Inactif seulement',
          },
          deletable: 'Filtre sur les @.lower:(enums.objectTypes.user)s non affectés',
          deletableValues: {
            deletable: 'Affecté seulement',
            notDeletable: 'Non affecté seulement',
          },
        },
        actions: {
          editBtn: '@:enums.objectActions.modify',
          autorenewBtn: 'Régénérer le mot de passe @.lower:enums.objectTypes.user',
          inactiveBtn: 'Inactiver l\'@.lower:enums.objectTypes.user',
          activeBtn: 'Activer l\'@.lower:enums.objectTypes.user',
          deleteBtn: 'Supprimer l\'@.lower:enums.objectTypes.user',
        },
      },
    },
  },
}
