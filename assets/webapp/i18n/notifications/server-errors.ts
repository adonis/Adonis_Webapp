export default {
    en: {
        semiAutoVariable: {
            deleteOperation: {
                testOrSaisieStillConnected: 'An error occurred during deletion: variable still in use',
            },
        },
        simpleVariable: {
            deleteOperation: {
                testOrSaisieStillConnected: 'An error occurred during deletion: variable still in use',
            },
        },
        generatorVariable: {
            deleteOperation: {
                testOrSaisieStillConnected: 'An error occurred during deletion: variable still in use',
            },
        },
    },
    fr: {
        semiAutoVariable: {
            deleteOperation: {
                testOrSaisieStillConnected: 'Erreur lors de la suppression : la variable est utilisée dans un test ou une saisie connectée',
            },
        },
        simpleVariable: {
            deleteOperation: {
                testOrSaisieStillConnected: 'Erreur lors de la suppression : la variable est utilisée dans un test ou une saisie connectée',
            },
        },
        generatorVariable: {
            deleteOperation: {
                testOrSaisieStillConnected: 'Erreur lors de la suppression : la variable est utilisée dans un test ou une saisie connectée',
            },
        },
    },
}
