import parsingErrorEnums from '@adonis/webapp/i18n/enums/parsing-error-enums'
import VueI18n from 'vue-i18n'

export default {
  en: {
    alerts: {
      failure: {
        created: 'An error occurred during creation',
        modified: 'An error occurred during modification',
        deleted: 'An error occurred during deletion',
        fetched: 'An error occurred during fetch',
        wrongPassword: 'Invalid credentials',
        serverError: 'Server error, try again later',
        insufficientRight: 'Access denied : Insufficient rights',
        illegalCopy: 'Objects involved in the copy are incompatible',
        experimentStatusIncompatible: 'Experiment status doesn\'t allow this',
        incompletePath: 'Drawn path is incomplete or inexistant',
        useLinkMenu: 'Use the dedicated platform menu',
        swapTreatmentImpossible: 'Selected objects must be 2 unit plots',
        notFound: 'Uri not found',
        linedError: ( ctx: VueI18n.MessageContext ) => {
          return `Line ${ctx.named( 'line' )} : ${parsingErrorEnums.en(ctx)}`
        },
        openSilexConnexionError: 'No OpenSilex instance found',
        openSilexConnexionTimeout: 'Timeout, no response from OpenSilex, check service availability',
      },
      platform: {
        created: '@:(enums.objectTypes.platform) @:(enums.objectActions.created)',
        modified: '@:(enums.objectTypes.platform) @:(enums.objectActions.modified)',
        deleted: '@:(enums.objectTypes.platform) @:(enums.objectActions.deleted)',
      },
      protocol: {
        created: '@:(enums.objectTypes.protocol) @:(enums.objectActions.created)',
        modified: '@:(enums.objectTypes.protocol) @:(enums.objectActions.modified)',
        deleted: '@:(enums.objectTypes.protocol) @:(enums.objectActions.deleted)',
      },
      factor: {
        created: '@:(enums.objectTypes.factor) @:(enums.objectActions.created)',
        modified: '@:(enums.objectTypes.factor) @:(enums.objectActions.modified)',
        deleted: '@:(enums.objectTypes.factor) @:(enums.objectActions.deleted)',
      },
      oezNature: {
        created: '@:(enums.objectTypes.oezNature) @:(enums.objectActions.created)',
        modified: '@:(enums.objectTypes.oezNature) @:(enums.objectActions.modified)',
        deleted: '@:(enums.objectTypes.oezNature) @:(enums.objectActions.deleted)',
      },
      stateCode: {
        created: '@.lower:(enums.objectTypes.stateCode) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.stateCode) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.stateCode) @:(enums.objectActions.deleted)',
      },
      variable: {
        created: 'La @.lower:(enums.objectTypes.variable) @:(enums.objectActions.created)',
        modified: 'La @.lower:(enums.objectTypes.variable) @:(enums.objectActions.modified)',
        deleted: 'La @.lower:(enums.objectTypes.variable) @:(enums.objectActions.deleted)',
      },
      genVariable: {
        created: '@.lower:(enums.objectTypes.genVariable) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.genVariable) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.genVariable) @:(enums.objectActions.deleted)',
      },
      valueList: {
        created: '@.lower:(enums.objectTypes.valueList) @:(enums.objectActions.created)e',
        modified: '@.lower:(enums.objectTypes.valueList) @:(enums.objectActions.modified)e',
        deleted: '@.lower:(enums.objectTypes.valueList) @:(enums.objectActions.deleted)e',
      },
      experiment: {
        created: '@.lower:(enums.objectTypes.experiment) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.experiment) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.experiment) @:(enums.objectActions.deleted)',
      },
      device: {
        created: '@.lower:(enums.objectTypes.device) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.device) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.device) @:(enums.objectActions.deleted)',
      },
      dataEntryProject: {
        created: '@.lower:(enums.objectTypes.dataEntryProject) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.dataEntryProject) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.dataEntryProject) @:(enums.objectActions.deleted)',
      },
      requiredAnnotation: {
        created: '@.lower:(enums.objectTypes.requiredAnnotation) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.requiredAnnotation) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.requiredAnnotation) @:(enums.objectActions.deleted)',
      },
      path: {
        created: '@.lower:(enums.objectTypes.path) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.path) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.path) @:(enums.objectActions.deleted)',
      },
      userPath: {
        created: '@.lower:(enums.objectTypes.userPath) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.userPath) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.userPath) @:(enums.objectActions.deleted)',
      },
      dataEntry: {
        created: '@.lower:(enums.objectTypes.userPath) successfully imported',
        modified: '@.lower:(enums.objectTypes.userPath) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.userPath) @:(enums.objectActions.deleted)',
      },
      user: {
        created: '@:(enums.objectTypes.user) @:(enums.objectActions.created)',
        modified: '@:(enums.objectTypes.user) @:(enums.objectActions.modified)',
        deleted: '@:(enums.objectTypes.user) @:(enums.objectActions.deleted)',
      },
      site: {
        created: '@.lower:(enums.objectTypes.site) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.site) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.site) @:(enums.objectActions.deleted)',
      },
      roleUserSite: {
        created: '@.lower:(enums.objectTypes.roleUserSite) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.roleUserSite) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.roleUserSite) @:(enums.objectActions.deleted)',
      },
      userGroup: {
        created: '@.lower:(enums.objectTypes.userGroup) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.userGroup) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.userGroup) @:(enums.objectActions.deleted)',
      },
      openSilexInstance: {
        created: '@.lower:(enums.objectTypes.openSilexInstance) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.openSilexInstance) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.openSilexInstance) @:(enums.objectActions.deleted)',
      },
      advancedRightUser: {
        modified: '@.lower:(enums.objectTypes.advancedRight) updated',
      },
      test: {
        created: '@.lower:(enums.objectTypes.test) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.test) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.test) @:(enums.objectActions.deleted)',
      },
      measure: {
        created: '@.lower:(enums.objectTypes.measure) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.measure) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.measure) @:(enums.objectActions.deleted)',
      },
      edit: {
        deleted: 'Deleted',
      },
      northIndicator: {
        created: '@.lower:(enums.objectTypes.northIndicator) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.northIndicator) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.northIndicator) @:(enums.objectActions.deleted)',
      },
      graphicalTextZone: {
        created: '@.lower:(enums.objectTypes.graphicalTextZone) @:(enums.objectActions.created)',
        modified: '@.lower:(enums.objectTypes.graphicalTextZone) @:(enums.objectActions.modified)',
        deleted: '@.lower:(enums.objectTypes.graphicalTextZone) @:(enums.objectActions.deleted)',
      },
      unitPlot: {
        modified: '@.lower:(enums.objectTypes.unitPlot) @:(enums.objectActions.modified)',
      },
      variableConnection: {
        created: 'Connected variable @:(enums.objectActions.created)',
        modified: 'Connected variable @:(enums.objectActions.modified)',
        deleted: 'Connected variable @:(enums.objectActions.deleted)',
      },
      graphicalConfiguration: {
        created: 'Graphical configuration @:(enums.objectActions.created)',
        modified: 'Graphical configuration @:(enums.objectActions.modified)',
        deleted: 'Graphical configuration @:(enums.objectActions.deleted)',
      },
      fusionDataEntry: {
        created: 'Data entry fusion done',
      },
    },
  },
  fr: {
    alerts: {
      failure: {
        created: 'Une erreur est survenue durant la création',
        modified: 'Une erreur est survenue durant la modification',
        deleted: 'Une erreur est survenue durant la suppression',
        fetched: 'Une erreur est survenue durant la récupération des données',
        wrongPassword: 'Mot de passe invalide',
        serverError: 'Erreur serveur, réessayez plus tard',
        insufficientRight: 'Accès interdit : Droits insuffisants',
        illegalCopy: 'Les objets copiés ne sont pas compatibles avec l\'objet sélectionné',
        experimentStatusIncompatible: 'Le statut du dispositif ne permet pas cette action',
        incompletePath: 'Le cheminement est incomplete ou inexistant',
        useLinkMenu: 'Utilisez le menu de rattachement de dispositif quand la plateforme contient déjà un dispositif',
        swapTreatmentImpossible: 'Les objets sélectionnés doivent être 2 parcelles',
        notFound: 'Uri non trouvé',
        linedError: ( ctx: VueI18n.MessageContext ) => {
          return `Ligne ${ctx.named( 'line' )} : ${parsingErrorEnums.fr(ctx)}`
        },
        openSilexConnexionError: 'Instance OpenSilex non trouvée à cet Url',
        openSilexConnexionTimeout: 'Aucune réponse de l\'instance OpenSilex, vérifiez l\'état du service',
      },
      platform: {
        created: 'La @:(enums.objectTypes.platform) @:(enums.objectActions.created)e',
        modified: 'La @:(enums.objectTypes.platform) @:(enums.objectActions.modified)e',
        deleted: 'La @:(enums.objectTypes.platform) @:(enums.objectActions.deleted)e',
      },
      protocol: {
        created: 'Le @.lower:(enums.objectTypes.protocol) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.protocol) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.protocol) @:(enums.objectActions.deleted)',
      },
      factor: {
        created: 'Le @.lower:(enums.objectTypes.factor) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.factor) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.factor) @:(enums.objectActions.deleted)',
      },
      oezNature: {
        created: 'La @.lower:(enums.objectTypes.oezNature) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.oezNature) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.oezNature) @:(enums.objectActions.deleted)e',
      },
      stateCode: {
        created: 'Le @.lower:(enums.objectTypes.stateCode) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.stateCode) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.stateCode) @:(enums.objectActions.deleted)',
      },
      variable: {
        created: 'La @.lower:(enums.objectTypes.variable) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.variable) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.variable) @:(enums.objectActions.deleted)e',
      },
      genVariable: {
        created: 'La @.lower:(enums.objectTypes.genVariable) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.genVariable) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.genVariable) @:(enums.objectActions.deleted)e',
      },
      valueList: {
        created: 'La @.lower:(enums.objectTypes.valueList) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.valueList) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.valueList) @:(enums.objectActions.deleted)e',
      },
      experiment: {
        created: 'Le @.lower:(enums.objectTypes.experiment) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.experiment) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.experiment) @:(enums.objectActions.deleted)',
      },
      device: {
        created: 'Le @.lower:(enums.objectTypes.device) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.device) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.device) @:(enums.objectActions.deleted)',
      },
      dataEntryProject: {
        created: 'Le @.lower:(enums.objectTypes.dataEntryProject) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.dataEntryProject) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.dataEntryProject) @:(enums.objectActions.deleted)',
      },
      requiredAnnotation: {
        created: 'La @.lower:(enums.objectTypes.requiredAnnotation) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.requiredAnnotation) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.requiredAnnotation) @:(enums.objectActions.deleted)e',
      },
      path: {
        created: 'Le @.lower:(enums.objectTypes.path) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.path) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.path) @:(enums.objectActions.deleted)',
      },
      userPath: {
        created: 'Le @.lower:(enums.objectTypes.userPath) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.userPath) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.userPath) @:(enums.objectActions.deleted)',
      },
      openSilexInstance: {
        created: 'Le @.lower:(enums.objectTypes.openSilexInstance) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.openSilexInstance) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.openSilexInstance) @:(enums.objectActions.deleted)',
      },
      dataEntry: {
        created: 'La @.lower:(enums.objectTypes.dataEntry) a été importée',
        modified: 'La @.lower:(enums.objectTypes.dataEntry) @:(enums.objectActions.modified)',
        deleted: 'La @.lower:(enums.objectTypes.dataEntry) @:(enums.objectActions.deleted)',
      },
      user: {
        created: 'L\'@.lower:(enums.objectTypes.user) @:(enums.objectActions.created)',
        modified: 'L\'@.lower:(enums.objectTypes.user) @:(enums.objectActions.modified)',
        deleted: 'L\'@.lower:(enums.objectTypes.user) @:(enums.objectActions.deleted)',
      },
      site: {
        created: 'Le @.lower:(enums.objectTypes.site) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.site) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.site) @:(enums.objectActions.deleted)',
      },
      roleUserSite: {
        created: 'Le @.lower:(enums.objectTypes.roleUserSite) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.roleUserSite) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.roleUserSite) @:(enums.objectActions.deleted)',
      },
      userGroup: {
        created: 'Le @.lower:(enums.objectTypes.userGroup) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.userGroup) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.userGroup) @:(enums.objectActions.deleted)',
      },
      advancedRightUser: {
        modified: 'Les @.lower:(enums.objectTypes.advancedRight) ont été mis à jour',
      },
      test: {
        created: 'Le @.lower:(enums.objectTypes.test) @:(enums.objectActions.created)',
        modified: 'Le @.lower:(enums.objectTypes.test) @:(enums.objectActions.modified)',
        deleted: 'Le @.lower:(enums.objectTypes.test) @:(enums.objectActions.deleted)',
      },
      measure: {
        created: 'La @.lower:(enums.objectTypes.measure) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.measure) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.measure) @:(enums.objectActions.deleted)e',
      },
      note: {
        modified: 'La @.lower:(enums.objectTypes.note) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.note) @:(enums.objectActions.deleted)e',
      },
      edit: {
        deleted: 'Objet supprimé',
      },
      northIndicator: {
        created: 'La @.lower:(enums.objectTypes.northIndicator) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.northIndicator) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.northIndicator) @:(enums.objectActions.deleted)e',
      },
      graphicalTextZone: {
        created: 'La @.lower:(enums.objectTypes.graphicalTextZone) @:(enums.objectActions.created)e',
        modified: 'La @.lower:(enums.objectTypes.graphicalTextZone) @:(enums.objectActions.modified)e',
        deleted: 'La @.lower:(enums.objectTypes.graphicalTextZone) @:(enums.objectActions.deleted)e',
      },
      unitPlot: {
        modified: 'La @.lower:(enums.objectTypes.unitPlot) @:(enums.objectActions.modified)',
      },
      variableConnection: {
        created: 'La variable connectée @:(enums.objectActions.created)e',
        modified: 'La variable connectée @:(enums.objectActions.modified)e',
        deleted: 'La variable connectée @:(enums.objectActions.deleted)e',
      },
      graphicalConfiguration: {
        created: 'La configuration graphique @:(enums.objectActions.created)e',
        modified: 'La configuration graphique @:(enums.objectActions.modified)e',
        deleted: 'La configuration graphique @:(enums.objectActions.deleted)e',
      },
      fusionDataEntry: {
        created: 'Fusion de saisie effectuée',
      },
    },
  },
}
