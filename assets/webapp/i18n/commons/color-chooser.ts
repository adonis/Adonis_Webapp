export default {
  en: {
    color: 'Color',
    btn: 'Choose a color',
    title: 'Color chooser',
    cancel: '@:commons.cancel',
    validate: '@:commons.validate',
    error: {
      empty: 'Color must be set',
    },
  },
  fr: {
    color: 'Couleur',
    btn: 'Choisir une couleur',
    title: 'Choix de la couleur',
    cancel: '@:commons.cancel',
    validate: '@:commons.validate',
    error: {
      empty: 'La couleur est obligatoire',
    },
  },
}
