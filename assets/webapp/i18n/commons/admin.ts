export default {
  en: {
    editors: {
      roleUserSite: {
        stepRole: 'Role informations',
        addNewUser: '@:enums.objectActions.create',
        stepUser: '@:enums.objectTypes.user information',
        previous: '@:commons.previous',
      },
    },
    forms: {
      user: {
        formName: '@:enums.objectTypes.user',
        surname: 'Surname',
        name: 'Name',
        email: 'Email',
        login: 'Login',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        error: {
          nameIsRequired: '@:commons.admin.forms.user.name @:modules.commons.form.error.isRequired',
          surnameIsRequired: '@:commons.admin.forms.user.surname @:modules.commons.form.error.isRequired',
          emailIsRequired: '@:commons.admin.forms.user.email @:modules.commons.form.error.isRequired',
          emailIsMalformed: '@:commons.admin.forms.user.email is malformed',
          loginIsRequired: '@:commons.admin.forms.user.login @:modules.commons.form.error.isRequired',
          loginVerification: 'Login is being verified',
          loginCannotContainSpecialChar: 'Login cannot contain special character',
          loginAlreadyExist: 'Login already in use',
        },
      },
      roleUserSite: {
        formName: 'Role in the @.lower:enums.objectTypes.site',
        siteName: '@:enums.objectTypes.site name',
        user: '@:enums.objectTypes.user to link',
        role: '@:enums.objectTypes.user role',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        errors: {
          roleIsRequired: '@:commons.admin.forms.roleUserSite.role @:modules.commons.form.error.isRequired',
          userIsRequired: '@:commons.admin.forms.roleUserSite.user @:modules.commons.form.error.isRequired',
        },
      },
    },
  },
  fr: {
    editors: {
      roleUserSite: {
        stepRole: 'Informations sur le rôle',
        addNewUser: '@:enums.objectActions.create',
        stepUser: 'Informations de l\'@.lower:enums.objectTypes.user',
        previous: '@:commons.previous',
      },
    },
    forms: {
      user: {
        formName: '@:enums.objectTypes.user',
        surname: 'Nom',
        name: 'Prénom',
        email: 'Mail',
        login: 'Login',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        errors: {
          nameIsRequired: 'Le @.lower:commons.admin.forms.user.name @:modules.commons.form.error.isRequired',
          surnameIsRequired: 'Le @.lower:commons.admin.forms.user.surname @:modules.commons.form.error.isRequired',
          emailIsRequired: 'L\'adresse @.lower:commons.admin.forms.user.email @:(modules.commons.form.error.isRequired)e',
          emailIsMalformed: 'L\'@.lower:commons.admin.forms.user.email est mal formé',
          loginIsRequired: 'Le @.lower:commons.admin.forms.user.login @:modules.commons.form.error.isRequired',
          loginVerification: 'Le login est en cours de vérification',
          loginCannotContainSpecialChar: 'Le login ne peut pas contenir de caractères spéciaux',
          loginAlreadyExist: 'Le login est déjà assigné',
        },
      },
      roleUserSite: {
        formName: 'Rôle dans le @.lower:enums.objectTypes.site',
        siteName: 'Nom du @.lower:enums.objectTypes.site',
        user: '@:enums.objectTypes.user à associer',
        role: 'Rôle de l\'@.lower:enums.objectTypes.user',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
        errors: {
          roleIsRequired: 'Le @:commons.admin.forms.roleUserSite.role @:modules.commons.form.error.isRequired',
          userIsRequired: 'L\'@:commons.admin.forms.roleUserSite.user @:modules.commons.form.error.isRequired',
        },
      },
    },
  },
}
