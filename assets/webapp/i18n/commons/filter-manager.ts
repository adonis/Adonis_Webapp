export default {
  en: {
    fields: {
      filter: 'Filter attribute',
      operator: 'Operator',
      comparedValue: 'Compared value',
    },
    addConnectedVariable: 'Add a previous variable to filter on it',
    addFilter: 'Add filter',
    addNodeLabel: 'Add a node:',
    addAnd: 'And',
    addOr: 'Or',
    addNot: 'Not',
    tooltipMaxChildren: 'This node must have {count} child | This node must have {count} children',
    tooltipMinChildren: 'This node must have at least one child',
    error: {
      filterIsRequired: '@:commons.filterManager.fields.filter @:(modules.commons.form.error.isRequired)',
      operatorIsRequired: '@:commons.filterManager.fields.operator @:(modules.commons.form.error.isRequired)',
    },
    literalExpr: 'Literal expression:',
    noFilter: 'No filter',
  },
  fr: {
    fields: {
      filter: 'Attribut à filtrer',
      operator: 'Opérateur',
      comparedValue: 'Valeur à comparer',
    },
    addConnectedVariable: 'Ajouter une variable d\'une saisie précédente pour filtrer',
    addFilter: 'Ajouter le filtre',
    addNodeLabel: 'Ajouter un noeud :',
    addAnd: 'Et',
    addOr: 'Ou',
    addNot: 'Non',
    tooltipMaxChildren: 'Ce noeud doit avoir {count} fils pour être pris en compte',
    tooltipMinChildren: 'Ce noeud doit avoir au moins un fils pour être pris en compte',
    error: {
      filterIsRequired: 'L\'@:commons.filterManager.fields.filter @:(modules.commons.form.error.isRequired)',
      operatorIsRequired: 'L\'@:commons.filterManager.fields.operator @:(modules.commons.form.error.isRequired)',
    },
    literalExpr: 'Expression littérale :',
    noFilter: 'Aucun filtre appliqué',
  },
}
