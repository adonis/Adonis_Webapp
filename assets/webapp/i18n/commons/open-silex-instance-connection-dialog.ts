export default {
  en: {
    identifier: 'Identifier',
    password: 'Password',
    title: 'OpenSilex instance connection',
    cancel: '@:commons.cancel',
    validate: '@:commons.validate',
    error: {
      invalidCredentials: 'Error : invalid credentials',
    },
  },
  fr: {
    identifier: 'Identifiant',
    password: 'Mot de passe',
    title: 'Connexion à l\'instance OpenSilex',
    cancel: '@:commons.cancel',
    validate: '@:commons.validate',
    error: {
      invalidCredentials: 'Erreur : identifiants incorrects',
    },
  },
}
