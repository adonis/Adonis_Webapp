export default {
  en: {
    attachmentHint: 'No file uploaded | One file uploaded :  | {count} files uploaded : ',
    attachment: 'Attachment',
    addAttachment: 'Add attachment',
  },
  fr: {
    attachmentHint: 'Aucun fichier uploadé | Un fichier uploadé : | {count} fichiers uploadés : ',
    attachment: 'Pièces jointes',
    addAttachment: 'Uploader les fichiers',
  },
}
