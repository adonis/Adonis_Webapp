export default {
  en: {
    editors: {
      manageRight: {
        userTab: 'Users',
        groupTab: 'Groups',
      },
    },
    forms: {
      manageRight: {
        formName: 'Manage object user rights',
        usernameColLabel: 'Login',
        nameColLabel: 'Name',
        surnameColLabel: 'Surname',
        groupNameColLabel: 'Group Name',
        rightColLabel: 'Right',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
      },
    },
  },
  fr: {
    editors: {
      manageRight: {
        userTab: 'Utilisateurs',
        groupTab: 'Groupes',
      },
    },
    forms: {
      manageRight: {
        formName: 'Gestion des droits utilisateurs sur l\'objet',
        usernameColLabel: 'Login',
        nameColLabel: 'Prénom',
        surnameColLabel: 'Nom',
        groupNameColLabel: 'Nom du groupe',
        rightColLabel: 'Droit',
        cancel: '@:commons.cancel',
        validate: '@:commons.validate',
      },
    },
  },
}
