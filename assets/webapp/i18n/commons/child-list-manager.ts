export default {
  en: {
    noData: 'No associated data',
  },
  fr: {
    noData: 'Aucune donnée associée',
  },
}
