import {
  BlockLabelsEnum,
  ExperimentLabelsEnum,
  IndividualLabelsEnum,
  SubBlockLabelsEnum,
  SurfacicUnitPlotLabelsEnum,
  UnitPlotLabelsEnum
} from '@adonis/webapp/constants/graphical-labels-enums'
import VueI18n from 'vue-i18n'

export default {
  en: {
    editor: {
      deletePopup: {
        title: 'Delete selected items',
        message1: 'You\'re about to delete {count} selected item | You\'re about to delete {count} selected items',
        message2: 'Are you sure ?',
      },
      useSubBlockDialog: {
        title: 'Use a sub block ?',
        message: 'Do you want to add a sub block within ?',
        yes: 'Yes',
        no: 'No',
        cancel: 'Cancel',
      },
      treatmentDialog: {
        title: 'Choose a treatment',
        message: 'One or more unit plot will be added, coose a treatment for them',
      },
      tickLabels: {
        veryLow: 'Very low',
        low: 'Low',
        medium: 'Medium',
        high: 'High',
        veryHigh: 'Very high',
      },
      colorDialog: {
        title: 'Change object\'s color',
        fieldLabel: 'New color',
      },
      scaleDialog: {
        title: 'Change generated image quality',
      },
    },
    toolbox: {
      center: 'Center',
      delete: 'Delete',
      select: 'Select',
      advancedSelect: 'Advanced select',
      textSize: 'Text size',
      textSizeUp: 'Increase',
      textSizeDown: 'Decrease',
      pathLevel: 'Viewing level',
      advancedFilters: 'Advanced filters',
      cancelFilters: 'Remove filters',
      swapTreatment: 'Swap treatments',
      oezNatures: 'Out Experimentation Zone',
      addIndividual: 'Add individual',
      addSurfacicUnitPlot: 'Add surfacic unit plot',
      addUnitPlot: 'Add unit plot',
      addSubBlock: 'Add sub block',
      addBlock: 'Add block',
      placeExperiment: 'Place experiment "{experimentName}"',
      path: 'Path tool',
      exportImage: 'Export image',
      addTextZone: 'Add text zone',
      editNorthIndicator: 'Edit north indicator',
    },
    contextMenu: {
      delete: 'Delete',
      changeColor: 'Change object color',
      copy: 'Copy',
      paste: 'Paste',
      declareDead: 'Declare as dead',
      replant: 'Replant',
      editTextZone: 'Edit text zone',
    },
    northIndicator: {
      formName: 'North indicator',
      x: 'X',
      y: 'Y',
      width: 'Width',
      height: 'Height',
      orientation: 'Orientation',
      cancel: '@:commons.cancel',
      delete: 'Delete',
      validate: '@:commons.validate',
    },
    textZone: {
      formName: 'Text zone',
      width: 'Width',
      height: 'Height',
      text: 'Text',
      size: 'Size',
      color: 'Color',
      cancel: '@:commons.cancel',
      delete: 'Delete',
      validate: '@:commons.validate',
    },
    advancedFilters: {
      formName: 'Advanced filters',
      pathLevel: 'Path level',
      cancel: '@:commons.cancel',
      delete: 'Delete',
      validate: '@:commons.validate',
    },
    graphicalConfiguration: {
      formName: 'Graphical configuration',
      tabColors: 'Colors',
      platformColor: 'Platforms color',
      experimentColor: 'Experiments color',
      blockColor: 'Blocks color',
      subBlockColor: 'Sub blocks color',
      unitPlotColor: 'Unit plots color',
      individualColor: 'Individuals color',
      selectionRectangleColor: 'Selection rectangle color',
      selectedItemColor: 'Selected item color',
      tabLabels: 'Labels',
      breadcrumb: 'Breadcrumbs',
      breadcrumbActive: 'Active',
      experimentLabel: 'Experiments',
      experimentLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case ExperimentLabelsEnum.NAME:
            return 'Name'
          case ExperimentLabelsEnum.STATE:
            return 'State'
          case ExperimentLabelsEnum.BLOCK_NUMBER:
            return 'Block number'
        }
      },
      blocLabel: 'Blocks',
      blocLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case BlockLabelsEnum.NUMBER:
            return 'Block N°'
          case BlockLabelsEnum.EXPERIMENT_NAME:
            return 'Experiment name'
          case BlockLabelsEnum.UP_NUMBER:
            return 'UP Number'
        }
      },
      subBlocLabel: 'Sub blocks',
      subBlocLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case SubBlockLabelsEnum.BLOC_NAME:
            return 'Block N°'
          case SubBlockLabelsEnum.NUMBER:
            return 'Sub block N°'
          case SubBlockLabelsEnum.UP_NUMBER:
            return 'UP Number'
        }
      },
      unitPlotLabel: 'Unit plot',
      unitPlotLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case UnitPlotLabelsEnum.NUMBER:
            return 'UP N°'
          case UnitPlotLabelsEnum.BLOC_NAME:
            return 'Block N°'
          case UnitPlotLabelsEnum.SUB_BLOC_NAME:
            return 'Sub block N°'
          case UnitPlotLabelsEnum.TREATMENT_L:
            return 'Long treatment'
          case UnitPlotLabelsEnum.TREATMENT_S:
            return 'Short traitement'
          case UnitPlotLabelsEnum.FACTOR_1:
            return 'Factor 1'
          case UnitPlotLabelsEnum.FACTOR_2:
            return 'Factor 2'
          case UnitPlotLabelsEnum.FACTOR_3:
            return 'Factor 3'
          case UnitPlotLabelsEnum.INDIVIDUAL_NUMBER:
            return 'Individuals number'
        }
      },
      surfacicUnitPlotLabel: 'Surfacic unit plot',
      surfacicUnitPlotLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case SurfacicUnitPlotLabelsEnum.NUMBER:
            return 'UP N°'
          case SurfacicUnitPlotLabelsEnum.BLOC_NAME:
            return 'Block N°'
          case SurfacicUnitPlotLabelsEnum.SUB_BLOC_NAME:
            return 'Sub block N°'
          case SurfacicUnitPlotLabelsEnum.TREATMENT_L:
            return 'Long treatment'
          case SurfacicUnitPlotLabelsEnum.TREATMENT_S:
            return 'Short traitement'
          case SurfacicUnitPlotLabelsEnum.FACTOR_1:
            return 'Factor 1'
          case SurfacicUnitPlotLabelsEnum.FACTOR_2:
            return 'Factor 2'
          case SurfacicUnitPlotLabelsEnum.FACTOR_3:
            return 'Factor 3'
          case SurfacicUnitPlotLabelsEnum.X:
            return 'X'
          case SurfacicUnitPlotLabelsEnum.Y:
            return 'Y'
          case SurfacicUnitPlotLabelsEnum.ID:
            return 'ID'
        }
      },
      individualLabel: 'Individual',
      individualLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case IndividualLabelsEnum.NUMBER:
            return 'Individual N°'
          case IndividualLabelsEnum.X:
            return 'X'
          case IndividualLabelsEnum.Y:
            return 'Y'
          case IndividualLabelsEnum.ID:
            return 'ID'
          case IndividualLabelsEnum.UP_NUMBER:
            return 'UP N°'
          case IndividualLabelsEnum.SUB_BLOC_NAME:
            return 'Sub block N°'
          case IndividualLabelsEnum.TREATMENT_L:
            return 'Long treatment'
          case IndividualLabelsEnum.TREATMENT_S:
            return 'Short traitement'
          case IndividualLabelsEnum.BLOC_NAME:
            return 'Block N°'
        }
      },
      cancel: '@:commons.cancel',
      validate: '@:commons.validate',
    },
  },
  fr: {
    editor: {
      deletePopup: {
        title: 'Supprimer les éléments sélectionnés',
        message1: 'Vous allez supprimer {count} objet sélectionné | Vous allez supprimer {count} objets sélectionnés',
        message2: 'Etes-vous sûr ?',
      },
      useSubBlockDialog: {
        title: 'Utiliser un sous bloc',
        message: 'Voulez vous utiliser un sous bloc ?',
        yes: 'Oui',
        no: 'Non',
        cancel: 'Annuler',
      },
      treatmentDialog: {
        title: 'Choisir un traitement',
        message: 'Une ou plusieurs parcelles unitaires vont être ajoutées, choisissez leur traitement',
      },
      tickLabels: {
        veryLow: 'Très basse',
        low: 'Basse',
        medium: 'Moyenne',
        high: 'Haute',
        veryHigh: 'Très haute',
      },
      colorDialog: {
        title: 'Changer la couleur de l\'objet',
        fieldLabel: 'Nouvelle couleur',
      },
      scaleDialog: {
        title: 'Changer la qualité de l\'image',
      },
    },
    toolbox: {
      center: 'Centrer',
      delete: 'Supprimer',
      select: 'Selection',
      advancedSelect: 'Sélection avancée',
      textSize: 'Taille du texte',
      textSizeUp: 'Augmenter',
      textSizeDown: 'Diminuer',
      pathLevel: 'Niveau de visualisation',
      advancedFilters: 'Filtres avancés',
      cancelFilters: 'Annuler les filtres',
      swapTreatment: 'Echanger les traitements',
      oezNatures: 'ZHE',
      addIndividual: 'Placer individu',
      addSurfacicUnitPlot: 'Placer PU surfacique',
      addUnitPlot: 'Placer PU',
      addSubBlock: 'Placer sous bloc',
      addBlock: 'Placer bloc',
      placeExperiment: 'Placer dispositif "{experimentName}"',
      path: 'Outil cheminement',
      exportImage: 'Exporter une image',
      addTextZone: 'Placer zone de texte',
      editNorthIndicator: 'Modifier flèche nord',
    },
    contextMenu: {
      delete: 'Supprimer',
      changeColor: 'Changer la couleur de l\'objet',
      copy: 'Copier',
      paste: 'Coller',
      declareDead: 'Declarer comme mort',
      replant: 'Replanter',
      editTextZone: 'Modifier la zone de texte',
    },
    northIndicator: {
      formName: 'Flèche nord',
      x: 'X',
      y: 'Y',
      width: 'Largeur',
      height: 'Hauteur',
      orientation: 'Orientation',
      cancel: '@:commons.cancel',
      delete: 'Supprimer',
      validate: '@:commons.validate',
    },
    textZone: {
      formName: 'Zone de texte',
      width: 'Largeur',
      height: 'Hauteur',
      text: 'Texte',
      size: 'Taille',
      color: 'Couleur',
      cancel: '@:commons.cancel',
      delete: 'Supprimer',
      validate: '@:commons.validate',
    },
    advancedFilters: {
      formName: 'Filtres avancés',
      pathLevel: 'Niveau du filtre',
      cancel: '@:commons.cancel',
      delete: 'Supprimer',
      validate: '@:commons.validate',
    },
    graphicalConfiguration: {
      formName: 'Configuration graphique',
      tabColors: 'Couleurs',
      platformColor: 'Couleur des plateformes',
      experimentColor: 'Couleur des dispositifs',
      blockColor: 'Couleur des blocs',
      subBlockColor: 'Couleur des sous blocs',
      unitPlotColor: 'Couleur des parcelles unitaires',
      individualColor: 'Couleur des individus',
      selectionRectangleColor: 'Couleur du rectangle de sélection',
      selectedItemColor: 'Couleur du contour des objets sélectionnés',
      tabLabels: 'Labels',
      breadcrumb: 'Info bulle',
      breadcrumbActive: 'Actives',
      experimentLabel: 'Dispositifs',
      experimentLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case ExperimentLabelsEnum.NAME:
            return 'Nom'
          case ExperimentLabelsEnum.STATE:
            return 'Etat'
          case ExperimentLabelsEnum.BLOCK_NUMBER:
            return 'Nombre de blocs'
        }
      },
      blocLabel: 'Blocs',
      blocLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case BlockLabelsEnum.NUMBER:
            return 'N° de bloc'
          case BlockLabelsEnum.EXPERIMENT_NAME:
            return 'Nom Dispositif'
          case BlockLabelsEnum.UP_NUMBER:
            return 'Nombre de PU'
        }
      },
      subBlocLabel: 'Sous blocs',
      subBlocLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case SubBlockLabelsEnum.BLOC_NAME:
            return 'N° de bloc'
          case SubBlockLabelsEnum.NUMBER:
            return 'N° de sous bloc'
          case SubBlockLabelsEnum.UP_NUMBER:
            return 'Nombre de PU'
        }
      },
      unitPlotLabel: 'Parcelles unitaires',
      unitPlotLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case UnitPlotLabelsEnum.NUMBER:
            return 'N° de PU'
          case UnitPlotLabelsEnum.BLOC_NAME:
            return 'N° de bloc'
          case UnitPlotLabelsEnum.SUB_BLOC_NAME:
            return 'N° de sous bloc'
          case UnitPlotLabelsEnum.TREATMENT_L:
            return 'Traitement long'
          case UnitPlotLabelsEnum.TREATMENT_S:
            return 'Traitement court'
          case UnitPlotLabelsEnum.FACTOR_1:
            return 'Facteur 1'
          case UnitPlotLabelsEnum.FACTOR_2:
            return 'Facteur 2'
          case UnitPlotLabelsEnum.FACTOR_3:
            return 'Facteur 3'
          case UnitPlotLabelsEnum.INDIVIDUAL_NUMBER:
            return 'Nombre d\'individus'
        }
      },
      surfacicUnitPlotLabel: 'PU surfaciques',
      surfacicUnitPlotLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case SurfacicUnitPlotLabelsEnum.NUMBER:
            return 'N° de PU'
          case SurfacicUnitPlotLabelsEnum.BLOC_NAME:
            return 'N° de bloc'
          case SurfacicUnitPlotLabelsEnum.SUB_BLOC_NAME:
            return 'N° de sous bloc'
          case SurfacicUnitPlotLabelsEnum.TREATMENT_L:
            return 'Traitement long'
          case SurfacicUnitPlotLabelsEnum.TREATMENT_S:
            return 'Traitement court'
          case SurfacicUnitPlotLabelsEnum.FACTOR_1:
            return 'Facteur 1'
          case SurfacicUnitPlotLabelsEnum.FACTOR_2:
            return 'Facteur 2'
          case SurfacicUnitPlotLabelsEnum.FACTOR_3:
            return 'Facteur 3'
          case SurfacicUnitPlotLabelsEnum.X:
            return 'X'
          case SurfacicUnitPlotLabelsEnum.Y:
            return 'Y'
          case SurfacicUnitPlotLabelsEnum.ID:
            return 'ID'
        }
      },
      individualLabel: 'Individus',
      individualLabelItem: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'value' )) {
          case IndividualLabelsEnum.NUMBER:
            return 'N° d\' individu'
          case IndividualLabelsEnum.X:
            return 'X'
          case IndividualLabelsEnum.Y:
            return 'Y'
          case IndividualLabelsEnum.ID:
            return 'ID'
          case IndividualLabelsEnum.UP_NUMBER:
            return 'N° de PU '
          case IndividualLabelsEnum.SUB_BLOC_NAME:
            return 'N° de sous bloc'
          case IndividualLabelsEnum.TREATMENT_L:
            return 'Traitement long'
          case IndividualLabelsEnum.TREATMENT_S:
            return 'Traitement court'
          case IndividualLabelsEnum.BLOC_NAME:
            return 'N° de bloc'
        }
      },
      cancel: '@:commons.cancel',
      validate: '@:commons.validate',
    },
  },
}
