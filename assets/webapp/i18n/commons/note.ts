export default {
  en: {
    form: {
      formName: 'Notepad',
      deletedNote: 'This note has been deleted',
      deleteConfirm: {
        title: 'Note deletion',
        message: 'By clicking confirm, the note will be deleted. Continue ?',
        confirm: '@:commons.confirm',
        cancel: '@:commons.cancel',
      },
    },
  },
  fr: {
    form: {
      formName: 'Carnet de notes',
      deletedNote: 'Cette note a été supprimée',
      deleteConfirm: {
        title: 'Supprimer une note',
        message: 'En cliquant sur confirmer, la note sera supprimée. Continuer ?',
        confirm: '@:commons.confirm',
        cancel: '@:commons.cancel',
      },
    },
  },
}
