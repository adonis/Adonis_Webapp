import admin from '@adonis/webapp/i18n/commons/admin'
import childListManager from '@adonis/webapp/i18n/commons/child-list-manager'
import colorChooser from '@adonis/webapp/i18n/commons/color-chooser'
import fileInputField from '@adonis/webapp/i18n/commons/file-input-field'
import filterManager from '@adonis/webapp/i18n/commons/filter-manager'
import graphics from '@adonis/webapp/i18n/commons/graphics'
import note from '@adonis/webapp/i18n/commons/note'
import openSilexInstanceConnectionDialog from '@adonis/webapp/i18n/commons/open-silex-instance-connection-dialog'
import rights from '@adonis/webapp/i18n/commons/rights'

export default {
  en: {
    cancel: 'Cancel',
    validate: 'Finish',
    confirm: 'Confirm',
    save: 'Save',
    error: {
      isRequired: 'is required',
    },
    deletePopup: {
      title: 'Delete confirm popup',
      message: 'Are you sure you want to delete this object',
    },
    previous: 'Previous',
    next: 'Next',
    filterManager: filterManager.en,
    childListManager: childListManager.en,
    admin: admin.en,
    rights: rights.en,
    graphics: graphics.en,
    colorChooser: colorChooser.en,
    note: note.en,
    fileInputField: fileInputField.en,
    openSilexInstanceConnectionDialog: openSilexInstanceConnectionDialog.en,
    openSilexUri: 'OpenSilex Uri',
      geometry: 'Geometry',
  },
  fr: {
    cancel: 'Annuler',
    validate: 'Terminer',
    confirm: 'Confirmer',
    save: 'Enregistrer',
    error: {
      isRequired: 'est requis',
    },
    deletePopup: {
      title: 'Confirmation de suppression',
      message: 'Etes vous sur de vouloir supprimer cet objet ?',
    },
    previous: 'Précédent',
    next: 'Suivant',
    filterManager: filterManager.fr,
    childListManager: childListManager.fr,
    admin: admin.fr,
    rights: rights.fr,
    graphics: graphics.fr,
    colorChooser: colorChooser.fr,
    note: note.fr,
    fileInputField: fileInputField.fr,
    openSilexInstanceConnectionDialog: openSilexInstanceConnectionDialog.fr,
    openSilexUri: 'Uri OpenSilex',
      geometry: 'Geometry',
  },
}
