import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VueI18n from 'vue-i18n'

export default {
  en: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'level' )) {
      case PathLevelEnum.NONE:
        return 'None'
      case PathLevelEnum.INDIVIDUAL:
        return 'Individual'
      case PathLevelEnum.EXPERIMENT:
        return 'Experiment'
      case PathLevelEnum.BLOCK:
        return 'Bloc'
      case PathLevelEnum.SUB_BLOCK:
        return 'Sub bloc'
      case PathLevelEnum.UNIT_PLOT:
        return 'Unit plot'
      case PathLevelEnum.SURFACE_UNIT_PLOT:
        return 'Surfacic unit plot'
      case PathLevelEnum.PLATFORM:
        return 'Platform'
    }
  },
  fr: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'level' )) {
      case PathLevelEnum.NONE:
        return 'Aucune'
      case PathLevelEnum.INDIVIDUAL:
        return 'Individu'
      case PathLevelEnum.EXPERIMENT:
        return 'Dispositif'
      case PathLevelEnum.BLOCK:
        return 'Bloc'
      case PathLevelEnum.SUB_BLOCK:
        return 'Sous bloc'
      case PathLevelEnum.UNIT_PLOT:
        return 'Parcelle unitaire'
      case PathLevelEnum.SURFACE_UNIT_PLOT:
        return 'Parcelle surfacique'
      case PathLevelEnum.PLATFORM:
        return 'Plateforme'
    }
  },
}
