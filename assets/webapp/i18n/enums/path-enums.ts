import PossibleStartPointEnum from '@adonis/webapp/constants/possible-start-point-enum'
import VueI18n from 'vue-i18n'

export default {
  en: {
    startPoints: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'startPoint' )) {
        case PossibleStartPointEnum.BOT_LFT_DTOP:
          return 'Bottom left to top'
        case PossibleStartPointEnum.BOT_LFT_DRGT:
          return 'Bottom left to right'
        case PossibleStartPointEnum.BOT_RGT_DTOP:
          return 'Bottom right to top'
        case PossibleStartPointEnum.BOT_RGT_DLFT:
          return 'Bottom right to left'
        case PossibleStartPointEnum.TOP_LFT_DBOT:
          return 'Top left to bottom'
        case PossibleStartPointEnum.TOP_LFT_DRGT:
          return 'Top left to right'
        case PossibleStartPointEnum.TOP_RGT_DBOT:
          return 'Top right to bottom'
        case PossibleStartPointEnum.TOP_RGT_DLFT:
          return 'Top right to left'
      }
    },
  },
  fr: {
    startPoints: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'startPoint' )) {
        case PossibleStartPointEnum.BOT_LFT_DTOP:
          return 'Bas gauche vers haut'
        case PossibleStartPointEnum.BOT_LFT_DRGT:
          return 'Bas gauche vers droite'
        case PossibleStartPointEnum.BOT_RGT_DTOP:
          return 'Bas droite vers haut'
        case PossibleStartPointEnum.BOT_RGT_DLFT:
          return 'Bas droite vers gauche'
        case PossibleStartPointEnum.TOP_LFT_DBOT:
          return 'Haut gauche vers bas'
        case PossibleStartPointEnum.TOP_LFT_DRGT:
          return 'Haut gauche vers droite'
        case PossibleStartPointEnum.TOP_RGT_DBOT:
          return 'Haut droite vers bas'
        case PossibleStartPointEnum.TOP_RGT_DLFT:
          return 'Haut droite vers gauche'
      }
    },
  },
}
