import ParsingErrorEnum from '@adonis/webapp/constants/parsing-error-enum'
import VueI18n from 'vue-i18n'

export default {
  en: (ctx: VueI18n.MessageContext) => {
    switch (ctx.named('error')) {
      case ParsingErrorEnum.UNKNOWN_ERROR:
        return 'A non-managed issue occured during file parsing'
      case ParsingErrorEnum.ERROR_INCORRECT_FILE_TYPE:
        return 'Incorrect file type, you may verify the type of file you want to upload'
      case ParsingErrorEnum.ERROR_DURING_UNZIPPING:
        return 'An error occured during unzipping operation'
      case ParsingErrorEnum.ERROR_EXTRACTED_FILE_NOT_FOUND:
        return 'The name of the xml file does not match the expected name'
      case ParsingErrorEnum.ERROR_DATA_ENTRY_PROJECT_MISSING:
        return 'The name of the data entry project was not found in the xml file'
      case ParsingErrorEnum.ERROR_PLATFORM_MISSING:
        return 'There is no plateform linked to the project'
      case ParsingErrorEnum.ERROR_NO_EXPERIMENT:
        return 'There is no experiment linked to the plateform'
      case ParsingErrorEnum.ERROR_NO_BLOC:
        return 'There is no bloc linked to the experiment'
      case ParsingErrorEnum.ERROR_NO_BLOC_CHILDREN:
        return 'The bloc don\'t have any children'
      case ParsingErrorEnum.ERROR_NO_WORKPATH:
        return 'No items in the path'
      case ParsingErrorEnum.ERROR_DUPLICATED_URI:
        return 'Duplicated URI in XML file'
      case ParsingErrorEnum.ERROR_WORKPATH_ITEM_NOT_FOUND:
        return 'URI in path doesn\'t match with any item'
      case ParsingErrorEnum.ERROR_NO_PROTOCOL:
        return 'No protocol found under project root'
      case ParsingErrorEnum.ERROR_PROTOCOL_INFO_MISSING:
        return 'Protocol information is missing'
      case ParsingErrorEnum.ERROR_MODALITY_ITEM_NOT_FOUND:
        return 'A modality was not found but referenced'
      case ParsingErrorEnum.ERROR_STATE_CODE_INFO_MISSING:
        return 'A state code mandatory information is missing'
      case ParsingErrorEnum.NO_GENERATED_VARIABLE:
        return 'A generator variable does not have any generated variable'
      case ParsingErrorEnum.ERROR_TEST_INFO_MISSING:
        return 'A test mandatory information is missing'
      case ParsingErrorEnum.UNKNOWN_TEST_TYPE:
        return 'Unknown test type'
      case ParsingErrorEnum.UNKNOWN_TEST_LINKED_VARIABLE:
        return 'Unknown test-linked variable'
      case ParsingErrorEnum.ERROR_PREVIOUS_VALUE_INFO_MISSING:
        return 'A connected value information is missing'
      case ParsingErrorEnum.UNKNOWN_PREVIOUS_VALUE_LINKED_OBJECT:
        return 'Unknown previous value linked object'
      case ParsingErrorEnum.UNKNOWN_UNITPLOT_LINKED_TREATMENT:
        return 'Unknown unit plot linked treatment'
      case ParsingErrorEnum.URI_NOT_FOUND:
        return 'URI not found in XML'
      case ParsingErrorEnum.ERROR_FILE_NOT_FOUND:
        return 'File not found on the server'
      case ParsingErrorEnum.TOO_MANY_VARIABLE_LIBRARIES:
        return 'Too many variable libraries for a user (expected 1)'
      case ParsingErrorEnum.TOO_MANY_EXPERIMENT_LIBRARIES:
        return 'Too many experiment libraries for a user (expected 1)'
      case ParsingErrorEnum.ERROR_INCORRECT_ANNOTATION_TYPE:
        return 'Unknown annotation type'
      case ParsingErrorEnum.ERROR_UNKNOWN_DEVICE:
        return 'Material not set for a semi-auto variable'
      case ParsingErrorEnum.ERROR_NO_PLATFORM:
        return 'No plateform found in the similar namespace in the XML'
      case ParsingErrorEnum.ERROR_NOTE_CREATOR_NOT_FOUND:
        return 'No creator account found for one of the entry note'
      case ParsingErrorEnum.ERROR_VALUE_HINT_LIST_INFO_MISSING:
        return 'Incomplete value hint list informations'
      case ParsingErrorEnum.ERROR_SCALE_INFO_MISSING:
        return 'Incomplete scale informations'
      case ParsingErrorEnum.ERROR_OUT_EXPERIMENTATION_ZONE_INFO_MISSING:
        return 'Incomplete out experimentation zone informations'
      case ParsingErrorEnum.ERROR_NAME_ALREADY_IN_USE:
        return 'Name already in use, change the existing object name and retry'
      case ParsingErrorEnum.WARNING_UNKNOWN_SESSION_USER:
        return 'Unable to retrieve the experimenter for one or more sessions, you are set as the experimenter and a comment is added'
      case ParsingErrorEnum.ERROR_CSV_MISSING_FACTOR_NAME:
        return 'The factor modality is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_X:
        return 'The x position is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_Y:
        return 'The y position is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_BLOCK_NUMBER:
        return 'The block number is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_UNIT_PLOT_NAME:
        return 'The unit plot is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_EXPERIMENT_NAME:
        return 'The experiment name is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_TREATMENT:
        return 'The treatment is missing'
      case ParsingErrorEnum.ERROR_CSV_ALREADY_USED_POSITION:
        return 'The specified position is already in use'
      case ParsingErrorEnum.ERROR_CSV_UNIT_PLOT_MULTIPLE_TREATMENT:
        return 'The unit plot already have a different treatment'
      case ParsingErrorEnum.ERROR_CSV_INDIVIDUAL_ID_EXISTS:
        return 'The identifier is already used'
      case ParsingErrorEnum.ERROR_CSV_TREATMENT_MULTIPLE_FACTOR_COMBINATIONS:
        return 'The treatment has already been used with a different modality combination'
      case ParsingErrorEnum.ERROR_CSV_NON_NUMERIC_POSITION:
        return 'The position can only be numeric'
      case ParsingErrorEnum.ERROR_CSV_EMPTY_VARIABLE_VALUE:
        return 'Missing value for a variable'
      case ParsingErrorEnum.ERROR_CSV_DATA_ENTRY_OBJECT_NOT_FOUND:
        return 'No object found for variable level'
      case ParsingErrorEnum.ERROR_CSV_OBJECT_NOT_FOUND:
        return 'No object found for position'
      case ParsingErrorEnum.ERROR_CSV_MISSING_ORDER:
        return 'Order is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_IDENTIFIER:
        return 'Identifier is missing'
      case ParsingErrorEnum.ERROR_CSV_MISSING_OPENSILEX_URI:
        return 'OpenSilex uri is missing'
      case ParsingErrorEnum.ERROR_CSV_ONLY_ONE_GERMPLASM:
        return 'Protocol can only have one germplasm'
      case ParsingErrorEnum.ERROR_CSV_DIFFERENT_URI_FOR_SAME_MODALITY:
        return 'Different uri for same modality'
      case ParsingErrorEnum.ERROR_CSV_EXPERIMENT_WKT_MALFORMED:
        return 'Experiment geometry invalid'
      case ParsingErrorEnum.ERROR_CSV_BLOCK_WKT_MALFORMED:
        return 'Block geometry invalid'
      case ParsingErrorEnum.ERROR_CSV_SUBBLOCK_WKT_MALFORMED:
        return 'Sub-block geometry invalid'
      case ParsingErrorEnum.ERROR_CSV_UP_WKT_MALFORMED:
        return 'Unit plot geometry invalid'
      case ParsingErrorEnum.ERROR_CSV_INDIVIDUAL_WKT_MALFORMED:
        return 'Individual geometry invalid'
      case ParsingErrorEnum.ERROR_CSV_OEZ_WKT_MALFORMED:
        return 'OEZ geometry invalid'
      case ParsingErrorEnum.ERROR_CSV_DIFFERENTS_TREATMENTS_WITH_SAME_MODALITIES:
        return 'Another treatment has already been used with same modality combination'

      case ParsingErrorEnum.PARSING_STATE_READ_ZIP_FILE:
        return 'Error while reading the zip file'
      case ParsingErrorEnum.PARSING_STATE_FIND_NAMESPACES:
        return 'Error while finding the XML namespaces'
      case ParsingErrorEnum.PARSING_STATE_READ_STATE_CODE:
        return 'Unable to read the state codes'
      case ParsingErrorEnum.PARSING_STATE_READ_PROJECT_CREATOR:
        return 'Unable to read the project creator'
      case ParsingErrorEnum.PARSING_STATE_READ_DESKTOP_USER_COLLECTION:
        return 'Unable to read the desktop user list'
      case ParsingErrorEnum.PARSING_STATE_READ_OEZ_NATURE:
        return 'Unable to read the out experimentation zone nature'
      case ParsingErrorEnum.PARSING_STATE_READ_PLATFORM:
        return 'Unable to read the platform'
      case ParsingErrorEnum.PARSING_STATE_READ_CONNECTED_VARIABLE_COLLECTION:
        return 'Unable to read the connected variables'
      case ParsingErrorEnum.PARSING_STATE_READ_PROJECT:
        return 'Unable to read the project'
      case ParsingErrorEnum.PARSING_STATE_READ_GRAPHICAL_STRUCTURE:
        return 'Unable to read the graphical structure'
      case ParsingErrorEnum.PARSING_STATE_DATABASE_SAVE:
        return 'Unknown error during the database persistance'
      case ParsingErrorEnum.SEMANTIC_DUPLICATE_BLOCK_NUMBER:
        return 'Duplicate block number in a single experiment'
    }
  },
  fr: (ctx: VueI18n.MessageContext) => {
    switch (ctx.named('error')) {
      case ParsingErrorEnum.UNKNOWN_ERROR:
        return 'Une erreur de traitement non gérée est survenue'
      case ParsingErrorEnum.ERROR_INCORRECT_FILE_TYPE:
        return 'Type de fichier incorrect, vérifiez que le fichier est bien du type que vous souhaitez importer'
      case ParsingErrorEnum.ERROR_DURING_UNZIPPING:
        return 'Erreur durant l\'ouverture du fichier zip'
      case ParsingErrorEnum.ERROR_EXTRACTED_FILE_NOT_FOUND:
        return 'Le nom du fichier xml ne correspond pas avec le nom attendu (celui du fichier zip)'
      case ParsingErrorEnum.ERROR_DATA_ENTRY_PROJECT_MISSING:
        return 'Le nom du projet de saisie n\'a pas été trouvé'
      case ParsingErrorEnum.ERROR_PLATFORM_MISSING:
        return 'Aucune plateforme n\'est liée au projet de saisie'
      case ParsingErrorEnum.ERROR_NO_EXPERIMENT:
        return 'Aucun dispositif n\'est liée à la plateforme'
      case ParsingErrorEnum.ERROR_NO_BLOC:
        return 'Aucun bloc n\'est liée au dispositif'
      case ParsingErrorEnum.ERROR_NO_BLOC_CHILDREN:
        return 'Un bloc ne contient ni parcelle unitaire ni sous bloc'
      case ParsingErrorEnum.ERROR_NO_WORKPATH:
        return 'Aucun objet trouvé dans la partie cheminement'
      case ParsingErrorEnum.ERROR_DUPLICATED_URI:
        return 'URI dupliqué dans le fichier XML'
      case ParsingErrorEnum.ERROR_WORKPATH_ITEM_NOT_FOUND:
        return 'Une URI du cheminement ne correspond à aucun objet métier'
      case ParsingErrorEnum.ERROR_NO_PROTOCOL:
        return 'Aucun protocole trouvé pour le dispositif'
      case ParsingErrorEnum.ERROR_PROTOCOL_INFO_MISSING:
        return 'Une information obligatoire sur un protocole est manquante'
      case ParsingErrorEnum.ERROR_MODALITY_ITEM_NOT_FOUND:
        return 'Aucune modalité trouvée'
      case ParsingErrorEnum.ERROR_STATE_CODE_INFO_MISSING:
        return 'Une information obligatoire sur les codes d\'état est manquante'
      case ParsingErrorEnum.NO_GENERATED_VARIABLE:
        return 'Une variable génératrice n\'a pas de variable générées'
      case ParsingErrorEnum.ERROR_TEST_INFO_MISSING:
        return 'Une information obligatoire est manquante dans un test sur variable'
      case ParsingErrorEnum.UNKNOWN_TEST_TYPE:
        return 'Type de test inconnu'
      case ParsingErrorEnum.UNKNOWN_TEST_LINKED_VARIABLE:
        return 'Une variable définie dans un test n\'est pas déclarée'
      case ParsingErrorEnum.ERROR_PREVIOUS_VALUE_INFO_MISSING:
        return 'Une information concernant une variable connectée est manquante'
      case ParsingErrorEnum.UNKNOWN_PREVIOUS_VALUE_LINKED_OBJECT:
        return 'Objet métier lié à la variable connectée inconnu'
      case ParsingErrorEnum.UNKNOWN_UNITPLOT_LINKED_TREATMENT:
        return 'Traitement lié à la parcelle unitaire inconnu'
      case ParsingErrorEnum.URI_NOT_FOUND:
        return 'URI non trouvée dans le XML'
      case ParsingErrorEnum.ERROR_FILE_NOT_FOUND:
        return 'Fichier non présent sur le serveur'
      case ParsingErrorEnum.TOO_MANY_VARIABLE_LIBRARIES:
        return 'Trop de librairies de variables pour un utilisateur (1 attendue)'
      case ParsingErrorEnum.TOO_MANY_EXPERIMENT_LIBRARIES:
        return 'Trop de librairies de dispositifs pour un utilisateur (1 attendu)'
      case ParsingErrorEnum.ERROR_INCORRECT_ANNOTATION_TYPE:
        return 'Type d\'annotation inconnu'
      case ParsingErrorEnum.ERROR_UNKNOWN_DEVICE:
        return 'Aucun materiel lié à une variable semi-auto'
      case ParsingErrorEnum.ERROR_NO_PLATFORM:
        return 'Aucune plateforme assosciée au namespace correspondant dans le fichier XML'
      case ParsingErrorEnum.ERROR_NOTE_CREATOR_NOT_FOUND:
        return 'Aucun compte n\'est associé au créateur de la note'
      case ParsingErrorEnum.ERROR_VALUE_HINT_LIST_INFO_MISSING:
        return 'Informations d\'une liste de valeur incomplète'
      case ParsingErrorEnum.ERROR_SCALE_INFO_MISSING:
        return 'Informations d\'une échelle de notation incomplète'
      case ParsingErrorEnum.ERROR_OUT_EXPERIMENTATION_ZONE_INFO_MISSING:
        return 'Informations d\'une ZHE incomplète'
      case ParsingErrorEnum.ERROR_NAME_ALREADY_IN_USE:
        return 'Le nom est déjà utilisé changez le nom de l\'objet existant et réessayez'
      case ParsingErrorEnum.WARNING_UNKNOWN_SESSION_USER:
        return 'Impossible de retrouver l\'expérimentateur d\'une ou plusieurs sessions, vous êtes l\'expérimentateur de ces sessions et un commentaire a été ajouté sur ces sessions'
      case ParsingErrorEnum.ERROR_CSV_MISSING_FACTOR_NAME:
        return 'Le nom de facteur est manquant'
      case ParsingErrorEnum.ERROR_CSV_MISSING_X:
        return 'La position en x est manquante'
      case ParsingErrorEnum.ERROR_CSV_MISSING_Y:
        return 'La position en y  est manquante'
      case ParsingErrorEnum.ERROR_CSV_MISSING_BLOCK_NUMBER:
        return 'Le numero de bloc  est manquant'
      case ParsingErrorEnum.ERROR_CSV_MISSING_UNIT_PLOT_NAME:
        return 'L\'identifiant de PU  est manquant'
      case ParsingErrorEnum.ERROR_CSV_MISSING_EXPERIMENT_NAME:
        return 'Le nom de dispositif  est manquant'
      case ParsingErrorEnum.ERROR_CSV_MISSING_TREATMENT:
        return 'Le traitement  est manquant'
      case ParsingErrorEnum.ERROR_CSV_ALREADY_USED_POSITION:
        return 'La position est déjà utilisée précédement'
      case ParsingErrorEnum.ERROR_CSV_UNIT_PLOT_MULTIPLE_TREATMENT:
        return 'La parcelle unitaire a déjà un traitement different'
      case ParsingErrorEnum.ERROR_CSV_INDIVIDUAL_ID_EXISTS:
        return 'L\'identifiant est déjà utilisé'
      case ParsingErrorEnum.ERROR_CSV_TREATMENT_MULTIPLE_FACTOR_COMBINATIONS:
        return 'Le traitement existe déjà pour des modalités différentes'
      case ParsingErrorEnum.ERROR_CSV_NON_NUMERIC_POSITION:
        return 'La position n\'utilise pas de caractères numériques'
      case ParsingErrorEnum.ERROR_CSV_EMPTY_VARIABLE_VALUE:
        return 'Valeur saisie manquante'
      case ParsingErrorEnum.ERROR_CSV_DATA_ENTRY_OBJECT_NOT_FOUND:
        return 'Objet du niveau de la variable non trouvé'
      case ParsingErrorEnum.ERROR_CSV_OBJECT_NOT_FOUND:
        return 'Objet non trouvé pour la position'
      case ParsingErrorEnum.ERROR_CSV_MISSING_ORDER:
        return 'Valeur d\'ordre manquante'
      case ParsingErrorEnum.ERROR_CSV_MISSING_IDENTIFIER:
        return 'Identifiant manquant'
      case ParsingErrorEnum.ERROR_CSV_MISSING_OPENSILEX_URI:
        return 'Uri OpenSilex manquant pour une des modalités'
      case ParsingErrorEnum.ERROR_CSV_ONLY_ONE_GERMPLASM:
        return 'Un seul facteur germplasm par protocole autorisé'
      case ParsingErrorEnum.ERROR_CSV_DIFFERENT_URI_FOR_SAME_MODALITY:
        return 'Utilisation d\'une Uri différente pour une modalité de même nom'
      case ParsingErrorEnum.ERROR_CSV_EXPERIMENT_WKT_MALFORMED:
        return 'Dispositif geometry invalide'
      case ParsingErrorEnum.ERROR_CSV_BLOCK_WKT_MALFORMED:
        return 'Bloc geometry invalide'
      case ParsingErrorEnum.ERROR_CSV_SUBBLOCK_WKT_MALFORMED:
        return 'Sous-bloc geometry invalide'
      case ParsingErrorEnum.ERROR_CSV_UP_WKT_MALFORMED:
        return 'Parcelle geometry invalide'
      case ParsingErrorEnum.ERROR_CSV_INDIVIDUAL_WKT_MALFORMED:
        return 'Individu geometry invalide'
      case ParsingErrorEnum.ERROR_CSV_OEZ_WKT_MALFORMED:
        return 'ZHE geometry invalide'
      case ParsingErrorEnum.ERROR_CSV_DIFFERENTS_TREATMENTS_WITH_SAME_MODALITIES:
        return 'Un autre traitement existe déjà avec les mêmes modalités'

      case ParsingErrorEnum.PARSING_STATE_READ_ZIP_FILE:
        return 'Erreur durant l\'ouverture du fichier zip : archive corrompue'
      case ParsingErrorEnum.PARSING_STATE_FIND_NAMESPACES:
        return 'Impossible de lire les namespace XML'
      case ParsingErrorEnum.PARSING_STATE_READ_STATE_CODE:
        return 'Impossible de lire les codes d\'état'
      case ParsingErrorEnum.PARSING_STATE_READ_PROJECT_CREATOR:
        return 'Erreur durant la recherche du créateur du projet'
      case ParsingErrorEnum.PARSING_STATE_READ_DESKTOP_USER_COLLECTION:
        return 'Impossible de lire les utilisateurs Adonis bureau'
      case ParsingErrorEnum.PARSING_STATE_READ_OEZ_NATURE:
        return 'Impossible de lire les natures ZHE'
      case ParsingErrorEnum.PARSING_STATE_READ_PLATFORM:
        return 'Impossible de lire la plateforme'
      case ParsingErrorEnum.PARSING_STATE_READ_CONNECTED_VARIABLE_COLLECTION:
        return 'Impossible de lire les variables connectées'
      case ParsingErrorEnum.PARSING_STATE_READ_PROJECT:
        return 'Impossible de lire le projet'
      case ParsingErrorEnum.PARSING_STATE_READ_GRAPHICAL_STRUCTURE:
        return 'Impossible de lire la structure graphique'
      case ParsingErrorEnum.PARSING_STATE_DATABASE_SAVE:
        return 'Erreur durant la sauvegarde en base de données'
      case ParsingErrorEnum.SEMANTIC_DUPLICATE_BLOCK_NUMBER:
        return 'Numéro de bloc dupliqué dans un dispositif'
    }
  },
}
