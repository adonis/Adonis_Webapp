import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import VueI18n from 'vue-i18n'

export default {
  en: {
    types: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'type' )) {
        case VariableTypeEnum.REAL:
          return 'Real'
        case VariableTypeEnum.ALPHANUMERIC:
          return 'Alphanumeric'
        case VariableTypeEnum.BOOLEAN:
          return 'Boolean'
        case VariableTypeEnum.INTEGER:
          return 'Integer'
        case VariableTypeEnum.DATE:
          return 'Date'
        case VariableTypeEnum.HOUR:
          return 'Time'
      }
    },
    formats: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'format' )) {
        case VariableFormatEnum.FREE:
          return 'Free'
        case VariableFormatEnum.CHARACTER_NUMBER:
          return 'Number of characters'
        case VariableFormatEnum.JJMMYYYY:
          return 'jjmmyyyy'
        case VariableFormatEnum.QUANTIEM:
          return 'Quantiem'
      }
    },
  },
  fr: {
    types: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'type' )) {
        case VariableTypeEnum.REAL:
          return 'Réel'
        case VariableTypeEnum.ALPHANUMERIC:
          return 'Alphanumérique'
        case VariableTypeEnum.BOOLEAN:
          return 'Booleen'
        case VariableTypeEnum.INTEGER:
          return 'Entier'
        case VariableTypeEnum.DATE:
          return 'Date'
        case VariableTypeEnum.HOUR:
          return 'Heure'
      }
    },
    formats: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'format' )) {
        case VariableFormatEnum.FREE:
          return 'Libre'
        case VariableFormatEnum.CHARACTER_NUMBER:
          return 'Nombre de caractères'
        case VariableFormatEnum.JJMMYYYY:
          return 'jjmmyyyy'
        case VariableFormatEnum.QUANTIEM:
          return 'Quantième'
      }
    },
  },
}
