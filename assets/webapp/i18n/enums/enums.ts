import advancedRight from '@adonis/webapp/i18n/enums/advanced-right-enums'
import annotationEnums from '@adonis/webapp/i18n/enums/annotation-enums'
import deviceEnums from '@adonis/webapp/i18n/enums/device-enums'
import experimentStateEnums from '@adonis/webapp/i18n/enums/experiment-state-enums'
import levels from '@adonis/webapp/i18n/enums/level-enums'
import objectActions from '@adonis/webapp/i18n/enums/object-action-enums'
import objectTypes from '@adonis/webapp/i18n/enums/object-type-enums'
import parsingErrorEnums from '@adonis/webapp/i18n/enums/parsing-error-enums'
import pathEnums from '@adonis/webapp/i18n/enums/path-enums'
import platformEnums from '@adonis/webapp/i18n/enums/platformEnums'
import roles from '@adonis/webapp/i18n/enums/role-enums'
import spreading from '@adonis/webapp/i18n/enums/spreading-enum'
import testTypes from '@adonis/webapp/i18n/enums/test-type-enum'
import variableEnums from '@adonis/webapp/i18n/enums/variable-enums'

export default {
  en: {
    annotation: annotationEnums.en,
    variable: variableEnums.en,
    device: deviceEnums.en,
    path: pathEnums.en,
    platform: platformEnums.en,
    levels: levels.en,
    roles: roles.en,
    objectTypes: objectTypes.en,
    testTypes: testTypes.en,
    objectActions: objectActions.en,
    advancedRight: advancedRight.en,
    spreading: spreading.en,
    graphicalOrigin: spreading.en,
    experimentState: experimentStateEnums.en,
    parsingError: parsingErrorEnums.en,
  },
  fr: {
    annotation: annotationEnums.fr,
    variable: variableEnums.fr,
    device: deviceEnums.fr,
    path: pathEnums.fr,
    platform: platformEnums.fr,
    levels: levels.fr,
    roles: roles.fr,
    objectTypes: objectTypes.fr,
    testTypes: testTypes.fr,
    objectActions: objectActions.fr,
    advancedRight: advancedRight.fr,
    spreading: spreading.fr,
    experimentState: experimentStateEnums.fr,
    parsingError: parsingErrorEnums.fr,
  },
}
