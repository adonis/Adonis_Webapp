import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import VueI18n from 'vue-i18n'

export default {
  en: {
    individualPu: ( ctx: VueI18n.MessageContext ) => {
      if (ctx.named( 'individualPu' )) {
        return 'Individual'
      } else {
        return 'Surfacic'
      }
    },
    graphicalOrigin: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'origin' )) {
        case GraphicalOriginEnum.TOP_LEFT:
          return 'Top left'
        case GraphicalOriginEnum.BOTTOM_LEFT:
          return 'Bottom left'
        case GraphicalOriginEnum.TOP_RIGHT:
          return 'Top right'
        case GraphicalOriginEnum.BOTTOM_RIGHT:
          return 'Bottom right'
      }
    },
  },
  fr: {
    individualPu: ( ctx: VueI18n.MessageContext ) => {
      if (ctx.named( 'individualPu' )) {
        return 'Individuel'
      } else {
        return 'Surfacique'
      }
    },
    graphicalOrigin: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'origin' )) {
        case GraphicalOriginEnum.TOP_LEFT:
          return 'Haut gauche'
        case GraphicalOriginEnum.BOTTOM_LEFT:
          return 'Bas gauche'
        case GraphicalOriginEnum.TOP_RIGHT:
          return 'Haut droit'
        case GraphicalOriginEnum.BOTTOM_RIGHT:
          return 'Bas droit'
      }
    },
  },
}
