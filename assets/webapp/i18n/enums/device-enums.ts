import DeviceCommunicationProtocolEnum from '@adonis/webapp/constants/device-communication-protocol-enum'
import Rs232FlowControl from '@adonis/webapp/constants/rs232-flow-control'
import Rs232Parity from '@adonis/webapp/constants/rs232-parity'
import VueI18n from 'vue-i18n'

export default {
  en: {
    communications: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'communication' )) {
        case DeviceCommunicationProtocolEnum.NONE:
          return 'None'
        case DeviceCommunicationProtocolEnum.RS232:
          return 'RS232'
        case DeviceCommunicationProtocolEnum.USB:
          return 'Usb'
        case DeviceCommunicationProtocolEnum.BLUETOOTH:
          return 'Bluetooth'
      }
    },
    rs232: {
      flowControls: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'flowControl' )) {
          case Rs232FlowControl.NONE:
            return 'None'
          case Rs232FlowControl.XONXOFF:
            return 'XonXoff'
          case Rs232FlowControl.RTSCTS:
            return 'Rtscts'
        }
      },
      parities: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'parity' )) {
          case Rs232Parity.NONE:
            return 'None'
          case Rs232Parity.EVEN:
            return 'Even'
          case Rs232Parity.ODD:
            return 'Odd'
          case Rs232Parity.MARK:
            return 'Mark'
          case Rs232Parity.SPACE:
            return 'Space'
        }
      },
    },
  },
  fr: {
    communications: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'communication' )) {
        case DeviceCommunicationProtocolEnum.NONE:
          return 'Aucun'
        case DeviceCommunicationProtocolEnum.RS232:
          return 'RS232'
        case DeviceCommunicationProtocolEnum.USB:
          return 'Usb'
        case DeviceCommunicationProtocolEnum.BLUETOOTH:
          return 'Bluetooth'
      }
    },
    rs232: {
      flowControls: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'flowControl' )) {
          case Rs232FlowControl.NONE:
            return 'Aucun'
          case Rs232FlowControl.XONXOFF:
            return 'XonXoff'
          case Rs232FlowControl.RTSCTS:
            return 'Rtscts'
        }
      },
      parities: ( ctx: VueI18n.MessageContext ) => {
        switch (ctx.named( 'parity' )) {
          case Rs232Parity.NONE:
            return 'Aucune'
          case Rs232Parity.EVEN:
            return 'Even'
          case Rs232Parity.ODD:
            return 'Odd'
          case Rs232Parity.MARK:
            return 'Mark'
          case Rs232Parity.SPACE:
            return 'Space'
        }
      },
    },
  },
}
