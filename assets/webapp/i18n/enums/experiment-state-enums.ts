import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import VueI18n from 'vue-i18n'

export default {
  en: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'state' )) {
      case ExperimentStateEnum.LOCKED:
        return 'Locked'
      case ExperimentStateEnum.CREATED:
        return 'Created'
      case ExperimentStateEnum.VALIDATED:
        return 'Validated'
      case ExperimentStateEnum.NON_UNLOCKABLE:
        return 'Non unlockable'
    }
  },
  fr: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'state' )) {
      case ExperimentStateEnum.LOCKED:
        return 'Verrouillé'
      case ExperimentStateEnum.CREATED:
        return 'Créé'
      case ExperimentStateEnum.VALIDATED:
        return 'Validé'
      case ExperimentStateEnum.NON_UNLOCKABLE:
        return 'Non déverrouillable'
    }
  },
}
