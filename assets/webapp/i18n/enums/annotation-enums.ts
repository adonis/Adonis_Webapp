import AnnotationCategoryEnum from '@adonis/webapp/constants/annotation-category-enum'
import AnnotationKindEnum from '@adonis/webapp/constants/annotation-kind-enum'
import VueI18n from 'vue-i18n'

export default {
  en: {
    kinds: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'kind' )) {
        case AnnotationKindEnum.TEXT:
          return 'Text'
        case AnnotationKindEnum.SOUND:
          return 'Sound'
        case AnnotationKindEnum.PICTURE:
          return 'Picture'
      }
    },
    askWhenEntering: ( ctx: VueI18n.MessageContext ) => ctx.named( 'askWhenEntering' ) ? 'When entering' : 'When leaving',
    categories: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'category' )) {
        case AnnotationCategoryEnum.OBSERVATION:
          return 'Observation'
        case AnnotationCategoryEnum.MEASURE:
          return 'Measure'
        case AnnotationCategoryEnum.EVENT:
          return 'Event'
        case AnnotationCategoryEnum.DIFFICULTY:
          return 'Difficulty'
      }
    },
  },
  fr: {
    kinds: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'kind' )) {
        case AnnotationKindEnum.TEXT:
          return 'Texte'
        case AnnotationKindEnum.SOUND:
          return 'Son'
        case AnnotationKindEnum.PICTURE:
          return 'Image'
      }
    },
    askWhenEntering: ( ctx: VueI18n.MessageContext ) => ctx.named( 'askWhenEntering' ) ? 'A l\'entrée de l\'objet' : 'A la sortie de l\'objet',
    categories: ( ctx: VueI18n.MessageContext ) => {
      switch (ctx.named( 'category' )) {
        case AnnotationCategoryEnum.OBSERVATION:
          return 'Observation'
        case AnnotationCategoryEnum.MEASURE:
          return 'Mesure'
        case AnnotationCategoryEnum.EVENT:
          return 'Evenement'
        case AnnotationCategoryEnum.DIFFICULTY:
          return 'Difficulté'
      }
    },
  },
}
