import SiteRoleEnum from '@adonis/webapp/constants/site-role-enum'
import VueI18n from 'vue-i18n'

export default {
  en: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'role' )) {
      case SiteRoleEnum.SITE_ADMIN:
        return 'Site administrator'
      case SiteRoleEnum.SITE_MANAGER:
        return 'Platform manager'
      case SiteRoleEnum.SITE_EXPERT:
        return 'Experimenter'
    }
  },
  fr: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'role' )) {
      case SiteRoleEnum.SITE_ADMIN:
        return 'Administrateur de site'
      case SiteRoleEnum.SITE_MANAGER:
        return 'Gestionnaire de plateforme'
      case SiteRoleEnum.SITE_EXPERT:
        return 'Expérimentateur'
    }
  },
}
