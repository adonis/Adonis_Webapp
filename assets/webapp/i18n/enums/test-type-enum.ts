import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import VueI18n from 'vue-i18n'

export default {
  en: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'type' )) {
      case TestTypeEnum.COMBINATION:
        return 'Combination test'
      case TestTypeEnum.PRECONDITIONED:
        return 'Preconditioned assignation test'
      case TestTypeEnum.GROWTH:
        return 'Growth test'
      case TestTypeEnum.INTERVAL:
        return 'Interval test'
    }
  },
  fr: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'type' )) {
      case TestTypeEnum.COMBINATION:
        return 'Test de combinaison'
      case TestTypeEnum.PRECONDITIONED:
        return 'Test de pré-calcul conditionnel'
      case TestTypeEnum.GROWTH:
        return 'Test d\'accroissement'
      case TestTypeEnum.INTERVAL:
        return 'Test sur intervalle'
    }
  },
}
