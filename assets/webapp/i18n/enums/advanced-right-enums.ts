import AdvancedRightEnum from '@adonis/webapp/constants/advanced-right-enum'
import VueI18n from 'vue-i18n'

export default {
  en: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'right' )) {
      case AdvancedRightEnum.NONE:
        return 'None'
      case AdvancedRightEnum.VIEW:
        return 'View'
      case AdvancedRightEnum.EDIT:
        return 'Edit'
    }
  },
  fr: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'right' )) {
      case AdvancedRightEnum.NONE:
        return 'Aucun'
      case AdvancedRightEnum.VIEW:
        return 'Visualiser'
      case AdvancedRightEnum.EDIT:
        return 'Modifier'
    }
  },
}
