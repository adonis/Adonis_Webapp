import SpreadingEnum from '@adonis/webapp/constants/spreading-enum'
import VueI18n from 'vue-i18n'

export default {
  en: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'level' )) {
      case SpreadingEnum.UNIT_PLOT:
        return 'Unit plot'
      case SpreadingEnum.INDIVIDUAL:
        return 'Individual'
      default:
        return 'None'
    }
  },
  fr: ( ctx: VueI18n.MessageContext ) => {
    switch (ctx.named( 'level' )) {
      case SpreadingEnum.UNIT_PLOT:
        return 'Parcelle unitaire'
      case SpreadingEnum.INDIVIDUAL:
        return 'Individu'
      default:
        return 'Aucune'
    }
  },
}
