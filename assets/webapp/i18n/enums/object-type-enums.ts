import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import VueI18n from 'vue-i18n'

const objectSwitch = (ctx: VueI18n.MessageContext) => {
    switch (ctx.named('role') as ObjectTypeEnum) {
        case ObjectTypeEnum.DATA_ENTRY:
            return 'enums.objectTypes.dataEntry'
        case ObjectTypeEnum.PLATFORM:
            return 'enums.objectTypes.platform'
        case ObjectTypeEnum.PROTOCOL:
            return 'enums.objectTypes.protocol'
        case ObjectTypeEnum.FACTOR:
            return 'enums.objectTypes.factor'
        case ObjectTypeEnum.OEZ_NATURE:
            return 'enums.objectTypes.oezNature'
        case ObjectTypeEnum.STATE_CODE:
            return 'enums.objectTypes.stateCode'
        case ObjectTypeEnum.SIMPLE_VARIABLE:
            return 'enums.objectTypes.variable'
        case ObjectTypeEnum.GENERATOR_VARIABLE:
            return 'enums.objectTypes.genVariable'
        case ObjectTypeEnum.VALUE_LIST:
            return 'enums.objectTypes.valueList'
        case ObjectTypeEnum.TEST:
            return 'enums.objectTypes.test'
        case ObjectTypeEnum.TEST_INTERVAL:
            return 'enums.objectTypes.testInterval'
        case ObjectTypeEnum.TEST_GROWTH:
            return 'enums.objectTypes.testGrowth'
        case ObjectTypeEnum.TEST_COMBINATION:
            return 'enums.objectTypes.testCombination'
        case ObjectTypeEnum.TEST_PRECONDITIONED:
            return 'enums.objectTypes.testPreconditioned'
        case ObjectTypeEnum.DEVICE:
            return 'enums.objectTypes.device'
        case ObjectTypeEnum.DATA_ENTRY_PROJECT:
            return 'enums.objectTypes.dataEntryProject'
        case ObjectTypeEnum.REQUIRED_ANNOTATION:
            return 'enums.objectTypes.requiredAnnotation'
        case ObjectTypeEnum.PATH_BASE:
            return 'enums.objectTypes.path'
        case ObjectTypeEnum.USER_PATH:
            return 'enums.objectTypes.userPath'
        case ObjectTypeEnum.EXPERIMENT:
            return 'enums.objectTypes.experiment'
        case ObjectTypeEnum.NOTE:
            return 'enums.objectTypes.note'
        case ObjectTypeEnum.USER:
            return 'enums.objectTypes.user'
        case ObjectTypeEnum.SITE:
            return 'enums.objectTypes.site'
        case ObjectTypeEnum.ROLE_USER_SITE:
            return 'enums.objectTypes.roleUserSite'
        case ObjectTypeEnum.USER_GROUP:
            return 'enums.objectTypes.userGroup'
        case ObjectTypeEnum.ADVANCED_RIGHT_USER:
            return 'enums.objectTypes.advancedRight'
        case ObjectTypeEnum.ADVANCED_RIGHT_GROUP:
            return 'enums.objectTypes.advancedRight'
        case ObjectTypeEnum.MEASURE:
            return 'enums.objectTypes.measure'
        case ObjectTypeEnum.BLOCK:
            return 'enums.objectTypes.block'
        case ObjectTypeEnum.SUB_BLOCK:
            return 'enums.objectTypes.subBlock'
        case ObjectTypeEnum.UNIT_PLOT:
            return 'enums.objectTypes.unitPlot'
        case ObjectTypeEnum.SURFACIC_UNIT_PLOT:
            return 'enums.objectTypes.surfacicUnitPlot'
        case ObjectTypeEnum.INDIVIDUAL:
            return 'enums.objectTypes.individual'
        case ObjectTypeEnum.SESSION:
            return 'enums.objectTypes.session'
        case ObjectTypeEnum.NORTH_INDICATOR:
            return 'enums.objectTypes.northIndicator'
        case ObjectTypeEnum.GRAPHICAL_TEXT_ZONE:
            return 'enums.objectTypes.graphicalTextZone'
        case ObjectTypeEnum.OEZ:
            return 'enums.objectTypes.oez'
        case ObjectTypeEnum.MODALITY:
            return 'enums.objectTypes.modality'
        case ObjectTypeEnum.SEMI_AUTOMATIC_VARIABLE:
            return 'enums.objectTypes.semiAutomaticVariable'
        case ObjectTypeEnum.TREATMENT:
            return 'enums.objectTypes.treatment'
        case ObjectTypeEnum.OPEN_SILEX_INSTANCE:
            return 'enums.objectTypes.openSilexInstance'
    }
}

export default {
    en: {
        platform: 'Platform',
        protocol: 'Protocol',
        factor: 'Factor',
        oezNature: 'OEZ Nature',
        stateCode: 'State code',
        variable: 'Variable',
        genVariable: 'Generator variable',
        valueList: 'Value list',
        test: 'Test on variable',
        testInterval: 'Interval test',
        testGrowth: 'Growth test',
        testCombination: 'Combination test',
        testPreconditioned: 'Preconditioned assignation test',
        device: 'Device',
        dataEntryProject: 'Data entry project',
        requiredAnnotation: 'Required annotation',
        path: 'Path',
        userPath: 'User path',
        experiment: 'Experiment',
        dataEntry: 'Date entry',
        block: 'Block',
        subBlock: 'Sub block',
        unitPlot: 'Unit plot',
        surfacicUnitPlot: 'Surfacic unit plot',
        treatment: 'Treatment',
        individual: 'Individual',
        user: 'User',
        site: 'Site',
        roleUserSite: 'In site user role',
        userGroup: 'User group',
        advancedRight: 'Rights',
        measure: 'Measure',
        variableScale: 'Scale',
        note: 'Note',
        session: 'Session',
        northIndicator: 'North Indicator',
        graphicalTextZone: 'Text area',
        oez: 'Out experimentation zone',
        modality: 'Modality',
        semiAutomaticVariable: 'Semi automatic variable',
        openSilexInstance: 'OpenSilex link',
        switch: objectSwitch,
    },
    fr: {
        platform: 'Plateforme',
        protocol: 'Protocole',
        factor: 'Facteur',
        oezNature: 'Nature de ZHE',
        stateCode: 'Code d\'état',
        variable: 'Variable',
        genVariable: 'Variable génératrice',
        valueList: 'Liste de valeur',
        test: 'Test sur variable',
        testInterval: 'Test sur intervalle',
        testGrowth: 'Test d\'accroissement',
        testCombination: 'Test de combinaison',
        testPreconditioned: 'Test de pré-calcul conditionnel',
        device: 'Matériel',
        dataEntryProject: 'Projet de saisie',
        requiredAnnotation: 'Annotation à saisir',
        path: 'Cheminement',
        userPath: 'Cheminement utilisateur',
        experiment: 'Dispositif',
        dataEntry: 'Saisie',
        block: 'Bloc',
        subBlock: 'Sous bloc',
        unitPlot: 'Parcelle unitaire',
        surfacicUnitPlot: 'Parcelle surfacique',
        treatment: 'Traitement',
        individual: 'Individu',
        user: 'Utilisateur',
        site: 'Site',
        roleUserSite: 'Role utilisateur dans site',
        userGroup: 'Groupe d\'utilisateur',
        advancedRight: 'Droits',
        measure: 'Mesure',
        variableScale: 'Échelle de notation',
        note: 'Note',
        session: 'Session',
        northIndicator: 'Flèche nord',
        graphicalTextZone: 'Zone de texte',
        oez: 'Zone hors expérimentation',
        modality: 'Modalité',
        semiAutomaticVariable: 'Variable semi automatique',
        openSilexInstance: 'Lien OpenSilex',
        switch: objectSwitch,
    },
}
