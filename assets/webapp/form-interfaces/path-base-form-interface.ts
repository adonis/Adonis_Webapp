import FilterTreeLeaf from '@adonis/webapp/form-interfaces/filters/filter-tree-leaf'
import FilterTreeNode from '@adonis/webapp/form-interfaces/filters/filter-tree-node'
import UserPathFormInterface from '@adonis/webapp/form-interfaces/user-path-form-interface'

export default interface PathBaseFormInterface {
  name: string
  effectiveFilterTree: (FilterTreeNode | FilterTreeLeaf)[]
  selectedIris: string[]
  userPaths?: UserPathFormInterface[],
}
