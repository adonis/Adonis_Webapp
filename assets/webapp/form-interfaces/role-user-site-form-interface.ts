import Site from '@adonis/shared/models/site'
import User from '@adonis/shared/models/user'
import SiteRoleEnum from '@adonis/webapp/constants/site-role-enum'

export default class RoleUserSiteFormInterface {
    user: User
    site: Site
    role: SiteRoleEnum
}
