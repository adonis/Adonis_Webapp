export default class DataEntryFusionFormInterface {
  orderedDefaultProjectDatasIris: string[]
  selectedVariableIris: string[]
  specificOrderForItem: Map<string, string[]>
  mergePriority?: string[]
  name: string
}
