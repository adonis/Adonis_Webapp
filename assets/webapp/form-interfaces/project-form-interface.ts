import Experiment from '@adonis/webapp/models/experiment'
import Platform from '@adonis/webapp/models/platform'

export default class ProjectFormInterface {

  name: string
  creatorLogin: string
  creationDate: string
  platform: Platform
  experiments: Experiment[]
  comment: string
}
