import ModalityFormInterface from '@adonis/webapp/form-interfaces/modality-form-interface'

export default class TreatmentFormInterface {
  name: string
  shortName: string
  repetitions: number
  modalities: ModalityFormInterface[]
}
