export default class NorthIndicatorFormInterface {
  orientation?: number
  x?: number
  y?: number
  width?: number
  height?: number
}
