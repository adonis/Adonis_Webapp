import User from '@adonis/shared/models/user'

export default interface UserPathFormInterface {
    user: User
    selectedObjects: string[]
}
