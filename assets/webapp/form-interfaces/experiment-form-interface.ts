import ExperimentStateEnum from '@adonis/webapp/constants/experiment-state-enum'
import AlgorithmConfigFormInterface from '@adonis/webapp/form-interfaces/algorithm-config-form-interface'
import AttachmentFormInterface from '@adonis/webapp/form-interfaces/attachment-form-interface'
import ProtocolFormInterface from '@adonis/webapp/form-interfaces/protocol-form-interface'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export default class ExperimentFormInterface {

  name?: string
  date?: string
  protocol?: ProtocolFormInterface
  individualUP?: boolean
  comment?: string
  algorithmConfig?: AlgorithmConfigFormInterface
  state?: ExperimentStateEnum
  attachments?: AttachmentFormInterface[]
  openSilexUri?: string
  openSilexInstance?: OpenSilexInstance
}
