import Site from '@adonis/shared/models/site'

export default class OpenSilexInstanceFormInterface {
    site: Site
    url: string
}
