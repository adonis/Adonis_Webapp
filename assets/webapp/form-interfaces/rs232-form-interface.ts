import Rs232BitFormat from '@adonis/webapp/constants/rs232-bit-format'
import Rs232FlowControl from '@adonis/webapp/constants/rs232-flow-control'
import Rs232Parity from '@adonis/webapp/constants/rs232-parity'
import Rs232StopBit from '@adonis/webapp/constants/rs232-stop-bit'

export default class Rs232FormInterface {
  frameLength: number
  frameStart: string
  frameEnd: string
  frameCsv: string
  baudrate: number
  bitFormat: Rs232BitFormat
  flowControl: Rs232FlowControl
  parity: Rs232Parity
  stopBit: Rs232StopBit
  remoteControl: boolean
}
