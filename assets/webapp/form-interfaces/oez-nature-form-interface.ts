import TextureEnum from '@adonis/webapp/constants/texture-enum'

export default class OezNatureFormInterface {
  nature: string
  color: number
  texture: TextureEnum
}
