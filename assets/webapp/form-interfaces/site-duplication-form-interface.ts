
export default interface SiteDuplicationFormInterface {
  name: string,
  cloneLibraries: boolean,
  cloneUsers: boolean,
  clonePlatforms: boolean,
  cloneProjects: boolean,
  cloneEntries: boolean,
}
