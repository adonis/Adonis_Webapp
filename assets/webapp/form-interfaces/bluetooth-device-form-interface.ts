export default class BluetoothDeviceFormInterface {
  frameLength: number
  frameStart: string
  frameEnd: string
  frameCsv: string
}
