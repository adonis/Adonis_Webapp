export default class PathBaseCsvBindingFormInterface {
    identifyWithPosition: boolean
    x: string
    y: string
    identifier: string
    separator = ';'
    order: string
    name: string
    project: string
    askWhenEntering: boolean
    file: any
}
