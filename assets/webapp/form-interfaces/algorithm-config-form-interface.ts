export default class AlgorithmConfigFormInterface {

  puNumber: number
  blockNumber: number
  subBlockNumber?: number
  blocsByLine?: number
  subBlockByLine: number
  puByLine: number
  individualsByPuNumber: number
  individualByLine: number
  extraParameters: any
}
