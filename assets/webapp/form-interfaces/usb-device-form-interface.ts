export default class UsbDeviceFormInterface {
  frameLength: number
  frameStart: string
  frameEnd: string
  frameCsv: string
}
