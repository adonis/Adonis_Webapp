import User from '@adonis/shared/models/user'

export default class UserGroupFormInterface {
    users: User[]
    name: string
}
