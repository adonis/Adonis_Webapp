import FilterKeyEnum from '@adonis/webapp/constants/filters/filter-key-enum'
import Filter from '@adonis/webapp/form-interfaces/filters/filter'

export default class FilterTreeLeaf {
  id: string
  type: FilterKeyEnum
  text: string
  filter: Filter
}
