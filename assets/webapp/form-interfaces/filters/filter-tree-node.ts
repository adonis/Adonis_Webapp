import FilterKeyEnum from '@adonis/webapp/constants/filters/filter-key-enum'
import FilterTreeLeaf from '@adonis/webapp/form-interfaces/filters/filter-tree-leaf'

export default class FilterTreeNode {
  id: string
  type: FilterKeyEnum
  text: string
  branches: (FilterTreeNode|FilterTreeLeaf)[]
  maxChildren: number
}
