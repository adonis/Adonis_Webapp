import FilterComparatorsEnum from '@adonis/webapp/constants/filter-comparators-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default class Filter {
  level: PathLevelEnum
  operator: FilterComparatorsEnum
  value: string
  variable?: (SimpleVariable | SemiAutomaticVariable | GeneratorVariable)
  function: ( object: any, comparator: FilterComparatorsEnum, valeur: string ) => boolean
}
