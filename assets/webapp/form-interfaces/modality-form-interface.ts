import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export default class ModalityFormInterface {
  value: string
  shortName: string
  id?: string
  uniqId?: string
  openSilexUri?: string
  openSilexInstance?: OpenSilexInstance
}
