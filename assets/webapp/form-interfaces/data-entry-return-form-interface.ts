import SessionFormInterface from '@adonis/webapp/form-interfaces/session-form-interface'

export default class DataEntryReturnFormInterface {
  platform: string
  sessions: SessionFormInterface[]
  projectName: string
  start: Date
}
