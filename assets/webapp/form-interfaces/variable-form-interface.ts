import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from '@adonis/shared/constants/variable-format-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import ValueListFormInterface from '@adonis/webapp/form-interfaces/value-list-form-interface'
import VariableScaleFormInterface from '@adonis/webapp/form-interfaces/variable-scale-form-interface'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export default class VariableFormInterface {
  name: string
  shortName: string
  type: VariableTypeEnum
  format?: VariableFormatEnum
  formatLength?: number
  pathLevel: PathLevelEnum
  defaultTrueValue?: boolean
  repetitions: number
  unit?: string
  required: boolean
  identifier?: string
  comment?: string
  addToLib?: boolean
  valueList?: ValueListFormInterface
  scale?: VariableScaleFormInterface
  openSilexUri?: string
  openSilexInstance?: OpenSilexInstance
}
