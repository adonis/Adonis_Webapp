import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'

export default class GeneratorInfoFormInterface {
  prefix: string
  numeralIncrement: boolean
  pathWayHorizontal: boolean
  directCounting: boolean
  generatedGeneratorVariables: string[]
  generatedSimpleVariables: string[]
}

export function isGeneratorVariable( toBeCheck: VariableFormInterface | (VariableFormInterface & GeneratorInfoFormInterface) ): toBeCheck is VariableFormInterface & GeneratorInfoFormInterface {
  if (!!(toBeCheck as GeneratorInfoFormInterface).generatedGeneratorVariables) {
    return true
  }
  return false
}
