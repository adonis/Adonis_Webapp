import ModalityFormInterface from '@adonis/webapp/form-interfaces/modality-form-interface'
import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export default class FactorFormInterface {
  name: string
  modalities: ModalityFormInterface[]
  order?: number
  openSilexUri?: string
  openSilexInstance?: OpenSilexInstance
  germplasm?: boolean
}
