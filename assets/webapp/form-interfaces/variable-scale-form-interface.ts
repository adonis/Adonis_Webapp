import VariableScaleItemFormInterface from '@adonis/webapp/form-interfaces/variable-scale-item-form-interface'

export default class VariableScaleFormInterface {
  name: string
  minValue: number
  maxValue: number
  open: boolean
  values: VariableScaleItemFormInterface[]
}
