import CombinationOperationEnum from '@adonis/webapp/constants/combination-operation-enum'
import ComparisonOperationEnum from '@adonis/webapp/constants/comparison-operation-enum'
import TestTypeEnum from '@adonis/webapp/constants/test-type-enum'
import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default class TestFormInterface {

  type: TestTypeEnum
  variable: SimpleVariable | SemiAutomaticVariable | GeneratorVariable

  authIntervalMin?: number
  probIntervalMin?: number
  probIntervalMax?: number
  authIntervalMax?: number

  comparedVariable?: SimpleVariable | SemiAutomaticVariable | GeneratorVariable
  minGrowth?: number
  maxGrowth?: number

  combinedVariable1?: SimpleVariable | SemiAutomaticVariable | GeneratorVariable
  combinationOperation?: CombinationOperationEnum
  combinedVariable2?: SimpleVariable | SemiAutomaticVariable | GeneratorVariable
  minLimit?: number
  maxLimit?: number

  compareWithVariable?: boolean
  comparedVariable1?: SimpleVariable | SemiAutomaticVariable | GeneratorVariable
  comparisonOperation?: ComparisonOperationEnum
  comparedVariable2?: SimpleVariable | SemiAutomaticVariable | GeneratorVariable
  comparedValue?: number
  assignedValue?: string
}
