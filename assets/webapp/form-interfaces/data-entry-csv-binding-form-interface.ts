
export default class DataEntryCsvBindingFormInterface {
  projectIri: string
  x: string
  y: string
  unitPlot: string
  block: string
  subBlock: string
  experiment: string
  variables: {
    iri: string
    value: string
    timestamp?: string
  }[] = []
  separator = ';'
  file: any
  replaceMissingValue: boolean
  integrateDeadAndReplanted: boolean
}
