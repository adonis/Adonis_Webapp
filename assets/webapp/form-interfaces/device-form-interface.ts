import DeviceCommunicationProtocolEnum from '@adonis/webapp/constants/device-communication-protocol-enum'
import BluetoothDeviceFormInterface from '@adonis/webapp/form-interfaces/bluetooth-device-form-interface'
import ManagedVariableExtraInfoFormInterface from '@adonis/webapp/form-interfaces/managed-variable-extra-info-form-interface'
import Rs232FormInterface from '@adonis/webapp/form-interfaces/rs232-form-interface'
import UsbDeviceFormInterface from '@adonis/webapp/form-interfaces/usb-device-form-interface'
import VariableFormInterface from '@adonis/webapp/form-interfaces/variable-form-interface'

export default class DeviceFormInterface {
  alias: string
  name: string
  manufacturer: string
  type: string
  communicationProtocol: DeviceCommunicationProtocolEnum
  communicationProtocolsInfos: BluetoothDeviceFormInterface | UsbDeviceFormInterface | Rs232FormInterface
  managedVariables: (VariableFormInterface & ManagedVariableExtraInfoFormInterface)[]
}
