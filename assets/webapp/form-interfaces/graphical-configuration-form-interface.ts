import {
  BlockLabelsEnum,
  ExperimentLabelsEnum,
  IndividualLabelsEnum,
  SubBlockLabelsEnum,
  SurfacicUnitPlotLabelsEnum,
  UnitPlotLabelsEnum
} from '@adonis/webapp/constants/graphical-labels-enums'

export default class GraphicalConfigurationFormInterface {
  individualColor?: number
  unitPlotColor?: number
  blockColor?: number
  subBlockColor?: number
  experimentColor?: number
  platformColor?: number
  selectedItemColor?: number
  activeBreadcrumb?: boolean
  experimentLabels?: ExperimentLabelsEnum[]
  blocLabels?: BlockLabelsEnum[]
  subBlocLabels?: SubBlockLabelsEnum[]
  unitPlotLabels?: UnitPlotLabelsEnum[]
  surfacicUnitPlotLabels?: SurfacicUnitPlotLabelsEnum[]
  individualLabels?: IndividualLabelsEnum[]
}
