import GraphicalOriginEnum from '@adonis/webapp/constants/graphical-origin-enum'
import AttachmentFormInterface from '@adonis/webapp/form-interfaces/attachment-form-interface'
import Experiment from '@adonis/webapp/models/experiment'

export default interface PlatformFormInterface {

  name?: string
  location?: string
  site?: string
  comment?: string
  creator?: string,
  createdAt?: string,
  experiments?: Experiment[],
  xMesh?: number,
  yMesh?: number,
  origin?: GraphicalOriginEnum
  attachments?: AttachmentFormInterface[]
}
