export default interface SessionFormInterface {

  id: number|string
  experimenterLogin: string
  startedAt: Date
  endedAt: Date
}
