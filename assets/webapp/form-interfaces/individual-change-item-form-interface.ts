export default interface IndividualChangeItemFormInterface {

  stateCode: number,
  stateTitle: string,
  appeared: Date,
  disappeared: Date,
}
