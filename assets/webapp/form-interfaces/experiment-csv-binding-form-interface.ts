
export default class ExperimentCsvBindingFormInterface {
  x: string
  y: string
  id: string
  individualGeometry?: string
  unitPlot: string
  unitPlotGeometry?: string
  block: string
  blockGeometry?: string
  subBlock: string
  subBlockGeometry?: string
  experiment: string
  experimentGeometry?: string
  treatment: string
  shortTreatment: string
  factors: {
    name: string
    id: string
    shortName: string
    openSilexUri?: string,
  }[] = []
  outExperimentationZone: string
  outExperimentationZoneGeometry?: string
  individualUp = true
  separator = ';'
  file: any
  openSilexInstanceIri?: string
}
