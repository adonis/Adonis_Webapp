import User from '@adonis/shared/models/user'
import SiteFormInterface from '@adonis/webapp/form-interfaces/site-form-interface'

export default interface SiteFormWithAdminInterface extends SiteFormInterface {
    admin: User
}
