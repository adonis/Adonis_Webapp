import PathLevelAlgorithmFormInterface from '@adonis/webapp/form-interfaces/path-level-algorithm-form-interface'

export default interface PathBaseMovementFormInterface {
  askWhenEntering: boolean,
  pathLevelAlgorithms: PathLevelAlgorithmFormInterface[],
  orderedIris: string[],
}
