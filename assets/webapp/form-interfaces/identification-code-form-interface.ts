
export default class IdentificationCodeFormInterface {
  importFromFile: boolean
  file?: any
  separator: string
  xColumn?: string
  yColumn?: string
  codeColumn?: string
}
