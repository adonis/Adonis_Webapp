import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import PossibleMoveEnum from '@adonis/webapp/constants/possible-move-enum'
import PossibleStartPointEnum from '@adonis/webapp/constants/possible-start-point-enum'

export default interface PathLevelAlgorithmFormInterface {
  level: PathLevelEnum,
  move: PossibleMoveEnum,
  startPoint: PossibleStartPointEnum,
  orderedLevelIris: string[]
}
