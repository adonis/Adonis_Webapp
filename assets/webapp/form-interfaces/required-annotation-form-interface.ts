import AnnotationKindEnum from '@adonis/webapp/constants/annotation-kind-enum'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'

export default class RequiredAnnotationFormInterface {
  level: PathLevelEnum
  type: AnnotationKindEnum
  askWhenEntering: boolean
  comment: string
}
