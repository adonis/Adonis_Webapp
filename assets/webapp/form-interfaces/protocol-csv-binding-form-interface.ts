
export default class ProtocolCsvBindingFormInterface {
  treatment: string
  shortTreatment: string
  repetitions: string
  factors: {
    name: string
    id: string
    shortName: string,
  }[] = []
  separator = ';'
  protocolName: string
  algorithm: string
  file: any
  aim: string
  comment: string
}
