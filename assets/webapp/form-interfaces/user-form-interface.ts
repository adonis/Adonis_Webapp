export default class UserFormInterface {
  name: string
  surname: string
  email: string
  login: string
}