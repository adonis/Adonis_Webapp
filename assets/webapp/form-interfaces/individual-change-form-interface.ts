import IndividualChangeItemFormInterface from '@adonis/webapp/form-interfaces/individual-change-item-form-interface'

export default interface IndividualChangeFormInterface {

  iri: string,
  requestIndividual: IndividualChangeItemFormInterface,
  responseIndividual: IndividualChangeItemFormInterface,
  requestUnitPlot: IndividualChangeItemFormInterface,
  responseUnitPlot: IndividualChangeItemFormInterface,
  treatmentName: string,
  treatmentShortName: string,
  blockName: string,
  x: number,
  y: number,
}
