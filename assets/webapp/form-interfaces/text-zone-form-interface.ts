export default class TextZoneFormInterface {
  text?: string
  color?: number
  width?: number
  height?: number
  size?: number
}
