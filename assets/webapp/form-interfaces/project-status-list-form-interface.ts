import StatusDataEntry from '@adonis/webapp/models/status-data-entry'

export default interface ProjectStatusListFormInterface {

  webappProject: string,
  userProjectStatus: StatusDataEntry,
  appProjectStatus: StatusDataEntry[],
}
