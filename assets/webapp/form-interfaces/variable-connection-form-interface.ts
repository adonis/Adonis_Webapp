import GeneratorVariable from '@adonis/webapp/models/generator-variable'
import SemiAutomaticVariable from '@adonis/webapp/models/semi-automatic-variable'
import SimpleVariable from '@adonis/webapp/models/simple-variable'

export default interface VariableConnectionFormInterface {
  iri?: string
  projectVariable: (SimpleVariable | SemiAutomaticVariable | GeneratorVariable)
  dataEntryVariable: (SimpleVariable | SemiAutomaticVariable | GeneratorVariable)
}
