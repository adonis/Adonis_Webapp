import AttachmentFormInterface from '@adonis/webapp/form-interfaces/attachment-form-interface'
import FactorFormInterface from '@adonis/webapp/form-interfaces/factor-form-interface'
import TreatmentFormInterface from '@adonis/webapp/form-interfaces/treatment-form-interface'
import Algorithm from '@adonis/webapp/models/algorithm'

export default class ProtocolFormInterface {

  name: string
  creatorLogin: string
  dateOfCreation: string
  aim: string
  comment: string
  factors: FactorFormInterface[]
  treatments: TreatmentFormInterface[]
  algorithm?: Algorithm
  attachments?: AttachmentFormInterface[]
}
