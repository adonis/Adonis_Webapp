import SpreadingEnum from '@adonis/webapp/constants/spreading-enum'

export default class StateCodeFormInterface {

  code: number
  title: string
  meaning: string
  spreading: SpreadingEnum
  color: number
}
