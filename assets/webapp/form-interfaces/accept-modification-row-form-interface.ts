import IndividualChangeFormInterface from '@adonis/webapp/form-interfaces/individual-change-form-interface'

export default interface AcceptModificationRowFormInterface {

  kind: string,
  x: number,
  y: number,
  block: string,
  treatmentName: string,
  change: IndividualChangeFormInterface,
  approved: boolean,
}
