import SnackAlertModel from '@adonis/webapp/stores/notification/models/snack-alert.model'

export default class NotificationState {

  alert: SnackAlertModel = null
}
