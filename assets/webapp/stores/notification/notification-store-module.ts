import SnackAlertModel from '@adonis/webapp/stores/notification/models/snack-alert.model'
import NotificationState from '@adonis/webapp/stores/notification/notification-state'
import NotificationStore from '@adonis/webapp/stores/notification/notification-store'

const state = new NotificationState()

export default {

  namespaced: true,

  state,

  getters: {

    alert( state: NotificationState ): SnackAlertModel {

      return state.alert
    },
  },

  actions: {

    alert( store: NotificationStore, payload: { alert: SnackAlertModel } ): void {

      const alert: SnackAlertModel = store.getters['alert'] as SnackAlertModel
      if (!!alert && alert.open) {
        store.commit( 'alert', { alert: { ...alert, open: false } } )
      }
      store.commit( 'alert', payload )
    },
      clear(store: NotificationStore): void {
          store.commit('clear')
      },
  },

  mutations: {

    alert( state: NotificationState, payload: { alert: SnackAlertModel } ): void {

      state.alert = payload.alert
    },

      clear(state: NotificationState): void {

          state.alert = null
      },
  },
}
