import NotificationState from '@adonis/webapp/stores/notification/notification-state'

export default class NotificationStore {

  commit: ( ...args: any ) => any
  dispatch: ( ...args: any ) => any
  state: NotificationState
  getters: any
}
