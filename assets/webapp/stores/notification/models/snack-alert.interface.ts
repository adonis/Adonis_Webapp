import AlertTypeEnum from '@adonis/webapp/constants/alert-type-enum'

export default interface SnackAlertInterface {

  type: AlertTypeEnum
  messages: {
    message: string,
    params?: any,
  }[]
  duration?: number
  open?: boolean
}
