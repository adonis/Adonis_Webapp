import AlertTypeEnum from '@adonis/webapp/constants/alert-type-enum'
import SnackAlertInterface from '@adonis/webapp/stores/notification/models/snack-alert.interface'

export default class SnackAlertModel implements SnackAlertInterface {

  type: AlertTypeEnum
  messages: {
    message: string,
    params?: any,
  }[]
  duration = 0
  open = true

  constructor( props: SnackAlertInterface ) {

    this.type = props.type
    this.messages = props.messages
    this.duration = !!props.duration
        ? props.duration
        : 0
    this.open = true
  }

}
