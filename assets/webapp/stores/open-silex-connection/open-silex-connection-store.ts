import { OpenSilexConnectionState } from '@adonis/webapp/stores/open-silex-connection/open-silex-connection-state'
import { ActionContext, Commit, Dispatch } from 'vuex'

export default class OpenSilexConnectionStore implements ActionContext<OpenSilexConnectionState, any> {
  commit: Commit
  dispatch: Dispatch
  getters: any
  rootGetters: any
  rootState: any
  state: OpenSilexConnectionState

}
