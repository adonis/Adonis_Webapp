import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'

export class OpenSilexConnectionState {
    userTokenMap: any
    askingInstanceCredentials: OpenSilexInstance = null

    constructor(openSilexConnectionState: OpenSilexConnectionState = null) {
        this.userTokenMap = openSilexConnectionState?.userTokenMap ?? {}
    }

}
