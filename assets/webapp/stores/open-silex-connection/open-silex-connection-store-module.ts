import OpenSilexInstance from '@adonis/webapp/models/open-silex-instance'
import { OpenSilexConnectionState } from '@adonis/webapp/stores/open-silex-connection/open-silex-connection-state'
import OpenSilexConnectionStore from '@adonis/webapp/stores/open-silex-connection/open-silex-connection-store'

export const STORAGE_KEY = 'Adonis::OpenSilexConnectionStore'
export default {

    namespaced: true,

    state: !!window.localStorage.getItem(STORAGE_KEY) ?
        new OpenSilexConnectionState(JSON.parse(window.localStorage.getItem(STORAGE_KEY))) :
        new OpenSilexConnectionState(),

    getters: {

        isAuthenticatedOnInstance(currentState: OpenSilexConnectionState): (openSilexInstance: OpenSilexInstance) => boolean {
            return openSilexInstance => {
                if (!openSilexInstance) {
                    return null
                }
                return null !== currentState.userTokenMap[openSilexInstance.url]
            }
        },

        authenticationHeader(currentState: OpenSilexConnectionState): (openSilexInstance: OpenSilexInstance) => string {
            return openSilexInstance => {
                if (!openSilexInstance) {
                    return null
                }
                return (null !== currentState.userTokenMap[openSilexInstance.url])
                    ? 'Bearer ' + currentState.userTokenMap[openSilexInstance.url]
                    : null
            }
        },

        askingOpenSilexCredentials(currentState: OpenSilexConnectionState): OpenSilexInstance {
            return currentState.askingInstanceCredentials
        },
    },

    actions: {
        askInstanceCredentials(connectionStore: OpenSilexConnectionStore, payload: OpenSilexInstance): Promise<any> {
            connectionStore.commit('setAskingInstanceCredentials', payload)
            if (!!payload) {
                connectionStore.commit('setTokens', {
                    openSilexInstance: payload,
                    token: null,
                })
            }
            return Promise.resolve()
        },

        updateTokens(connectionStore: OpenSilexConnectionStore, payload: {
            openSilexInstance: OpenSilexInstance,
            token: string,
        }): Promise<any> {
            connectionStore.commit('setAskingInstanceCredentials', null)
            connectionStore.commit('setTokens', payload)
            return Promise.resolve()
        },

        removeTokens(connectionStore: OpenSilexConnectionStore, payload: {
            openSilexInstance: OpenSilexInstance,
        }): Promise<any> {
            connectionStore.commit('setAskingInstanceCredentials', null)
            connectionStore.commit('setTokens', {
                ...payload,
                token: null,
            })
            return Promise.resolve()
        },
    },

    mutations: {

        setTokens(currentState: OpenSilexConnectionState, payload: {
            openSilexInstance: OpenSilexInstance,
            token: string,
        }): void {
            currentState.userTokenMap[payload.openSilexInstance.url] = payload.token
            window.localStorage.setItem(STORAGE_KEY, JSON.stringify(currentState))
        },

        setAskingInstanceCredentials(currentState: OpenSilexConnectionState, payload: OpenSilexInstance): void {
            currentState.askingInstanceCredentials = payload
        },
    },
}
