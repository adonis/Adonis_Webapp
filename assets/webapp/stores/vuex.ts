import CONFIRM_STORE_MODULE from '@adonis/shared/stores/confirm/confirm-store-module'
import CONNECTION_STORE_MODULE from '@adonis/shared/stores/connection/connection-store-module'
import HISTORY_STORE_MODULE from '@adonis/webapp/stores/history/history-store-module'
import NAVIGATION_STORE_MODULE from '@adonis/webapp/stores/navigation/navigation-store-module'
import NOTIFICATION_STORE_MODULE from '@adonis/webapp/stores/notification/notification-store-module'
import OPENSILEX_CONNECTION_STORE_MODULE from '@adonis/webapp/stores/open-silex-connection/open-silex-connection-store-module'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use( Vuex )

const store = new Vuex.Store( {
      modules: {
        connection: CONNECTION_STORE_MODULE,
        confirm: CONFIRM_STORE_MODULE,
        navigation: NAVIGATION_STORE_MODULE,
        notification: NOTIFICATION_STORE_MODULE,
        history: HISTORY_STORE_MODULE,
        openSilexConnection: OPENSILEX_CONNECTION_STORE_MODULE,
      },
    },
)

export default store
