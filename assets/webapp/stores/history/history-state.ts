import ReversibleAction from '@adonis/webapp/stores/history/models/reversible-action'

export default class HistoryState {

  undoStack: ReversibleAction<any>[] = []
  redoStack: ReversibleAction<any>[] = []
}
