import HistoryState from '@adonis/webapp/stores/history/history-state'
import HistoryStore from '@adonis/webapp/stores/history/history-store'
import ReversibleAction from '@adonis/webapp/stores/history/models/reversible-action'

export default {

  namespaced: true,

  state: new HistoryState(),

  getters: {

    canUndo( state: HistoryState ): boolean {
      return state.undoStack.length > 0 && state.undoStack[state.undoStack.length - 1].done
    },

    canRedo( state: HistoryState ): boolean {
      return state.redoStack.length > 0 && state.redoStack[state.redoStack.length - 1].done
    },
  },

  actions: {

    register( store: HistoryStore, payload: { action: ReversibleAction<any> } ): void {
      store.commit( 'register', payload )
    },

    undo( store: HistoryStore ): void {
      if (store.getters.canUndo) {
        store.commit( 'undo' )
      }
    },

    redo( store: HistoryStore ): void {
      if (store.getters.canRedo) {
        store.commit( 'redo' )
      }
    },

  },

  mutations: {

    register( state: HistoryState, payload: { action: ReversibleAction<any> } ): void {
      state.undoStack.push( payload.action )
      state.redoStack = []

      payload.action.promise.catch( error => {
        state.undoStack = state.undoStack.filter( item => item !== payload.action )
        return Promise.reject( error )
      } )
    },

    undo( state: HistoryState ): void {
      const action = state.undoStack.pop()
      const reversibleAction = new ReversibleAction<any>( {
        undoFunc: action.doFunc,
        doFunc: action.undoFunc,
      } )
      state.redoStack.push( reversibleAction )
      reversibleAction.promise.catch( error => {
        state.redoStack = state.redoStack.filter( item => item !== reversibleAction )
        return Promise.reject( error )
      } )
    },

    redo( state: HistoryState ): void {
      const action = state.redoStack.pop()
      const reversibleAction = new ReversibleAction<any>( {
        undoFunc: action.doFunc,
        doFunc: action.undoFunc,
      } )
      state.undoStack.push( reversibleAction )
      reversibleAction.promise.catch( error => {
        state.undoStack = state.undoStack.filter( item => item !== reversibleAction )
        return Promise.reject( error )
      } )
    },
  },
}
