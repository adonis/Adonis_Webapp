import HistoryState from '@adonis/webapp/stores/history/history-state'

export default class HistoryStore {

  commit: ( ...args: any ) => any
  dispatch: ( ...args: any ) => any
  state: HistoryState
  getters: any
}
