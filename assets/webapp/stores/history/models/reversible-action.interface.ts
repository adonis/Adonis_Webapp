
export default interface ReversibleActionInterface<R> {

  undoFunc: () => Promise<R>
  doFunc: () => Promise<R>
}
