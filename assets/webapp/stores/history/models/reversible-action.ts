import ReversibleActionInterface from '@adonis/webapp/stores/history/models/reversible-action.interface'

export default class ReversibleAction<R> implements ReversibleActionInterface<R> {

  promise: Promise<R>
  undoFunc: () => Promise<R>
  doFunc: () => Promise<R>
  done: boolean
  error: boolean

  constructor( props: ReversibleActionInterface<R> ) {
    this.undoFunc = props.undoFunc
    this.doFunc = props.doFunc
    this.promise = this.doFunc()
    this.done = this.error = false
    this.promise.then(() => this.done = true)
        .catch(error => {
          this.error = true
          return Promise.reject(error)
        })
  }

}
