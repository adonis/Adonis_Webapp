import RelUserSite from '@adonis/shared/models/rel-user-site'
import CHORUS from '@adonis/webapp/services/service-singleton'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import Module from '@adonis/webapp/stores/navigation/models/module.model'
import QuickAction from '@adonis/webapp/stores/navigation/models/quick-action.model'
import NavigationState from '@adonis/webapp/stores/navigation/navigation-state'
import NavigationStore, { ATTACH_BASE } from '@adonis/webapp/stores/navigation/navigation-store'
import { Route } from 'vue-router'

export default {

    namespaced: true,

    state: new NavigationState(),

    getters: {

        getRoleSite(state: NavigationState): (iri: string) => RelUserSite {

            return (iri: string) => !!state.roleSitesTrigger && state.roleSites.get(iri)
        },

        getRoleSiteBySiteIri(state: NavigationState): (siteIri: string) => RelUserSite {

            return (siteIri: string) => !!state.roleSitesTrigger && Array.from(state.roleSites.values())
                .find(roleSite => roleSite.siteIri === siteIri)
        },

        getActiveRoleSite(state: NavigationState): RelUserSite {

            return !!state.roleSitesTrigger && state.activeRoleSite
        },

        getRoleSites(state: NavigationState): RelUserSite[] {

            return !!state.roleSitesTrigger && Array.from(state.roleSites.values())
        },

        getModule(state: NavigationState): (name: string) => Module {

            return (name: string) => !!state.modulesTrigger && state.modules.get(name)
        },

        getActiveModule(state: NavigationState): Module {

            return !!state.modulesTrigger && state.activeModule
        },

        getModules(state: NavigationState): Module[] {

            return !!state.modulesTrigger && Array.from(state.modules.values())
        },

        getExplorerItem(state: NavigationState): (key: string) => ExplorerItem {

            return (key: string) => !!state.explorersTrigger && state.explorerItems.get(key)
        },

        getExplorerItems(state: NavigationState): ExplorerItem[] {

            return !!state.explorersTrigger && [...state.explorerRoots]
        },

        getSelectedExplorerItems(state: NavigationState): ExplorerItem[] {

            return !!state.explorersTrigger && Array.from(state.explorerItems.values())
                .filter(item => item.selected)
        },

        getQuickActions(state: NavigationState): QuickAction[] {

            return [...state.quickActions]
        },

        isPropertyViewPinned(state: NavigationState): boolean {

            return state.propertyViewPinned
        },

        isPropertyViewOpened(state: NavigationState): boolean {

            return state.propertyViewOpened
        },

        getPreviousRoute(state: NavigationState): Route {

            return state.previousRoutes[state.previousRoutes.length - 1]
        },
    },

    actions: {

        async clearRoleSite(store: NavigationStore) {

            return store.dispatch('clearModule')
                .then(() => store.commit('clearRoleSite'))
        },

        async removeRoleSiteFromSiteIri(store: NavigationStore, payload: { siteIri: string }) {

            const role = Array.from(store.state.roleSites.values())
                .find(roleSite => roleSite.siteIri === payload.siteIri)
            if (!!role) {
                store.commit('removeRoleSite', {roleSiteIri: role.iri})
            }
        },

        async refreshRoleSite(store: NavigationStore, payload: { roleSiteIri: string }) {

            return CHORUS.roleUserSiteProvider.getByIri(payload.roleSiteIri)
                .then(value => {
                    if (store.state.roleSites.has(payload.roleSiteIri)) {
                        store.commit('removeRoleSite', {roleSiteIri: payload.roleSiteIri})
                        store.commit('add2roleSite', {roleSite: value})
                    }
                })
        },

        async activeRelSite(store: NavigationStore, payload: { roleSite: RelUserSite }) {

            if (!payload.roleSite || store.state.roleSites.has(payload.roleSite.iri)) {
                return store.dispatch('clearModule')
                    .then(() => Promise.all(
                        CHORUS.moduleProvider.getModules(payload.roleSite)
                            .map(module => store.dispatch('add2module', {module})))
                        .then(() => store.commit('activeRoleSite', payload)))
            } else {
                throw new Error(`Unable to activate a site with key ${payload.roleSite.iri} as it does not exist.`)
            }
        },

        async add2roleSite(store: NavigationStore, payload: { roleSite: RelUserSite }) {
            if (!!payload.roleSite && !store.state.roleSites.has(payload.roleSite.iri)) {
                store.commit('add2roleSite', payload)
            } else {
                throw new Error(`Unable to add a site with key ${payload.roleSite.iri} as it has already been added.`)
            }
        },

        async clearModule(store: NavigationStore) {
            return store.dispatch('clearExplorer')
                .then(() => store.commit('clearModule'))
        },

        async activeModule(store: NavigationStore, payload: { module: Module }) {
            if (!payload.module || store.state.modules.has(payload.module.name)) {
                return store.dispatch('clearExplorer')
                    .then(() => store.commit('activeModule', payload))
                    .then(() => {
                        Promise.all(CHORUS.explorerProvider.getExplorersForModule(payload.module)
                            .map(item => store.dispatch('add2explorer', {item, attachment: ATTACH_BASE})))
                            .then(() => CHORUS.explorerProvider.initObjectExplorerForModule(payload.module))
                    })
            } else {
                throw new Error(`Unable to activate a module with key ${payload.module.name} as it does not exist.`)
            }
        },

        async add2module(store: NavigationStore, payload: { module: Module }) {

            if (!!payload.module && !store.state.modules.has(payload.module.name)) {
                store.commit('add2module', payload)
            } else {
                throw new Error(`Unable to add a module with key ${payload.module.name} as it has already been added.`)
            }
        },

        async clearExplorer(store: NavigationStore) {
            store.commit('clearExplorer')
        },

        async add2explorer(store: NavigationStore, payload: { item: ExplorerItem, attachment: string }) {

            if (!store.state.explorerItems.has(payload.item.name)) {
                store.commit('add2explorer', payload)
                if (payload.item.hasChildren) {
                    return Promise.all(payload.item.children.map((subItem: ExplorerItem) => store.dispatch('add2explorer', {
                        item: subItem,
                        attachment: payload.item.name,
                    })))
                }
            } else {
                throw new Error(`Call NavigationStoreModule.add2explorer() action with an already registered item : ${payload.item.name}`)
            }
        },

        async updateExplorer(store: NavigationStore, payload: { item: ExplorerItem, attachment?: string }) {

            if (store.state.explorerItems.has(payload.item.name)) {
                const attachment = payload.attachment ?? store.state.explorerParentName.get(payload.item.name)
                return store.dispatch('removeExplorer', {itemName: payload.item.name})
                    .then(() => store.dispatch('add2explorer', {item: payload.item, attachment}))
            } else {
                throw new Error(`Call NavigationStoreModule.updateExplorer() action on non existant explorer name : ${payload.item.name}`)
            }
        },

        async removeExplorer(store: NavigationStore, payload: { itemName: string }) {

            if (store.state.explorerItems.has(payload.itemName)) {
                const item: ExplorerItem = store.state.explorerItems.get(payload.itemName)
                const removeItem = () => store.commit('removeExplorer', payload)
                if (item.hasChildren) {
                    return Promise.all(item.children.map(child => store.dispatch('removeExplorer', {itemName: child.name})))
                        .then(removeItem)
                } else {
                    removeItem()
                }
            } else {
                throw new Error(`Call NavigationStoreModule.removeExplorer() action on non existant explorer name : ${payload.itemName}`)
            }
        },

        async removeExplorerChildren(store: NavigationStore, payload: { itemName: string }) {

            if (store.state.explorerItems.has(payload.itemName)) {
                const item: ExplorerItem = store.state.explorerItems.get(payload.itemName)
                if (item.hasChildren) {
                    return Promise.all(item.children.map(child => store.dispatch('removeExplorer', {itemName: child.name})))
                }
            } else {
                throw new Error(`Call NavigationStoreModule.removeExplorer() action on non existant explorer name : ${payload.itemName}`)
            }
        },

        async selectExplorer(store: NavigationStore, payload: { itemNames: string[] }) {
            store.commit('unselectAllExplorer')
            payload.itemNames.forEach(itemName => {
                // Open all ancestor node
                const parentCallback = (explorerName: string) => {
                    if (
                        store.state.explorerParentName.has(explorerName)
                    ) {
                        store.state.explorerItems.get(store.state.explorerParentName.get(explorerName)).collapsed = false
                        parentCallback(store.state.explorerParentName.get(explorerName))
                    }
                }
                parentCallback(itemName)
                store.commit('selectExplorerItem', {itemName})
            })
        },

        async clearQuickAction(store: NavigationStore): Promise<void> {
            store.commit('clearQuickAction')
        },

        async add2quickAction(store: NavigationStore, payload: { actions: QuickAction[] }) {
            payload.actions.forEach(action => {
                if (!store.state.quickActions.some(item => item.name === action.name)) {
                    store.commit('add2quickAction', {action})
                }
            })
        },

        async setPropertyViewPinned(store: NavigationStore, payload: { pinned: boolean }) {
            store.commit('setPropertyViewPinned', payload)
        },

        async setPropertyViewOpened(store: NavigationStore, payload: { opened: boolean }) {
            store.commit('setPropertyViewOpened', payload)
        },

        async addVisitedRoute(store: NavigationStore, payload: { route: Route }) {
            store.commit('addVisitedRoute', payload)
        },
    },

    mutations: {

        clearRoleSite(state: NavigationState): void {

            state.roleSites.clear()
            state.activeRoleSite = null
            state.updateSitesTrigger()
        },

        removeRoleSite(state: NavigationState, payload: { roleSiteIri: string }): void {

            state.roleSites.delete(payload.roleSiteIri)
            state.updateSitesTrigger()
        },

        activeRoleSite(state: NavigationState, payload: { roleSite: RelUserSite }): void {

            state.activeRoleSite = payload.roleSite
        },

        add2roleSite(state: NavigationState, payload: { roleSite: RelUserSite }): void {

            state.roleSites.set(payload.roleSite.iri, payload.roleSite)
            state.updateSitesTrigger()
        },

        clearModule(state: NavigationState): void {

            state.modules.clear()
            state.activeModule = null
            state.updateModulesTrigger()
        },

        activeModule(state: NavigationState, payload: { module: Module }): void {

            state.activeModule = payload.module
        },

        add2module(state: NavigationState, payload: { module: Module }): void {

            state.modules.set(payload.module.name, payload.module)
            state.updateModulesTrigger()
        },

        clearExplorer(state: NavigationState): void {

            state.explorerItems.clear()
            state.explorerRoots = []
            state.updateExplorersTrigger()
        },

        add2explorer(state: NavigationState, payload: { item: ExplorerItem, attachment: string }): void {

            if (ATTACH_BASE === payload.attachment) {
                state.explorerRoots.push(payload.item)
            } else {
                const parent = state.explorerItems.get(payload.attachment)
                parent.addChild(payload.item)
                state.explorerParentName.set(payload.item.name, parent.name)
            }
            state.explorerItems.set(payload.item.name, payload.item)
            state.updateExplorersTrigger()
        },

        removeExplorer(state: NavigationState, payload: { itemName: string }): void {
            state.explorerItems.delete(payload.itemName)
            if (state.explorerParentName.has(payload.itemName)) {
                const parent = state.explorerItems.get(state.explorerParentName.get(payload.itemName))
                parent.children = parent.children.filter(child => payload.itemName !== child.name)
            } else if (state.explorerRoots.some(explorer => explorer.name === payload.itemName)) {
                state.explorerRoots = state.explorerRoots.filter(explorer => explorer.name !== payload.itemName)
            }
            state.updateExplorersTrigger()
        },

        unselectAllExplorer(state: NavigationState) {
            state.explorerItems.forEach(item => item.selected = false)
        },

        selectExplorerItem(state: NavigationState, payload: { itemName: string }) {
            const explorer = state.explorerItems.get(payload.itemName)
            explorer.selected = true
        },

        clearQuickAction(state: NavigationState): void {

            state.quickActions = []
        },

        add2quickAction(state: NavigationState, payload: { action: QuickAction }): void {

            state.quickActions.push(payload.action)
        },

        setPropertyViewPinned(state: NavigationState, payload: { pinned: boolean }): void {
            state.propertyViewPinned = payload.pinned
        },

        setPropertyViewOpened(state: NavigationState, payload: { opened: boolean }): void {
            state.propertyViewOpened = payload.opened
        },

        addVisitedRoute(state: NavigationState, payload: { route: Route }): void {
            state.previousRoutes.push(payload.route)
        },
    },
}
