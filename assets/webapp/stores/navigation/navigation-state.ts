import RelUserSite from '@adonis/shared/models/rel-user-site'
import ExplorerItem from '@adonis/webapp/stores/navigation/models/explorer-item.model'
import Module from '@adonis/webapp/stores/navigation/models/module.model'
import { Route } from 'vue-router'

export default class NavigationState {

    roleSitesTrigger = 1
    modulesTrigger = 1
    explorersTrigger = 1

    roleSites = new Map<string, RelUserSite>()
    activeRoleSite: RelUserSite = null
    modules = new Map<string, Module>()
    activeModule: Module = null
    explorerItems = new Map<string, ExplorerItem>()
    explorerParentName = new Map<string, string>()
    explorerRoots: ExplorerItem[] = []
    quickActions: any[] = []
    propertyViewPinned = true
    propertyViewOpened = false
    previousRoutes: Route[] = []

    updateSitesTrigger() {
        this.roleSitesTrigger = NavigationState.updateTrigger(this.roleSitesTrigger)
    }

    updateModulesTrigger() {
        this.modulesTrigger = NavigationState.updateTrigger(this.modulesTrigger)
    }

    updateExplorersTrigger() {
        this.explorersTrigger = NavigationState.updateTrigger(this.explorersTrigger)
    }

    private static updateTrigger(trigger: number): number {
        return (trigger % 100) + 1
    }
}
