import NavigationState from '@adonis/webapp/stores/navigation/navigation-state'

export const ATTACH_BASE = 'base'

export default class NavigationStore {

  commit: ( ...args: any ) => any
  dispatch: ( ...args: any ) => any
  state: NavigationState
}
