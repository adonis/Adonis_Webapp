import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import VueRouter from 'vue-router'

export default interface ExplorerItemInterface {
    iri?: string
    icon: IconEnum
    name: string
    label?: string
    labelTab?: string[]
    labelOption?: any[]
    collapsed?: boolean
    selected?: boolean
    menu?: { title: string, action: (router: VueRouter) => void }[]
    types?: ExplorerTypeEnum[]
    objectType?: ObjectTypeEnum
    redirectRouteName?: string
    order?: number
    count?: number
    loading?: boolean
    deleted?: boolean
    collapsedCallback?: () => Promise<void>
}
