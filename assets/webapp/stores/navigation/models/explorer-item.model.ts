import IconEnum from '@adonis/shared/constants/icon-name-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import ExplorerTypeEnum from '@adonis/webapp/constants/explorer-type-enum'
import ExplorerItemInterface from '@adonis/webapp/stores/navigation/models/explorer-item.interface'
import VueRouter from 'vue-router'

export default class ExplorerItem implements ExplorerItemInterface {
    iri?: string
    icon: IconEnum
    name: string
    label: string
    labelTab: string[]
    labelOption: any[]
    private _collapsed: boolean
    selected: boolean
    children: ExplorerItem[]
    types: ExplorerTypeEnum[]
    objectType: ObjectTypeEnum
    redirectRouteName: string
    count!: number
    order?: number
    menu?: { title: string, action: (router: VueRouter) => void }[]
    loading: boolean
    deleted?: boolean
    private _fetched: boolean
    private _collapsedCallback: () => Promise<any>

    constructor(props: ExplorerItemInterface) {
        this.iri = props.iri
        this.icon = props.icon
        this.name = props.name
        this.label = props.label
        this.labelTab = props.labelTab
        this.labelOption = props.labelOption
        this._collapsed = props.collapsed ?? true
        this.selected = !!props.selected
        this.children = []
        this.menu = props.menu
        this.types = props.types ?? [ExplorerTypeEnum.NONE]
        this.objectType = props.objectType
        this.redirectRouteName = props.redirectRouteName
        this.order = props.order
        this.count = props.count ?? null
        this.loading = props.loading ?? false
        this.deleted = props.deleted ?? false
        this._collapsedCallback = props.collapsedCallback
        this._fetched = !this._collapsedCallback
    }

    get hasChildren(): boolean {
        return !this._fetched || this.children.length > 0
    }

    hasChild(child: ExplorerItem): boolean {
        return this.children.some(childIn => child.name === childIn.name)
    }

    addChild(child: ExplorerItem, count = false): ExplorerItem {
        if (!this.hasChild(child)) {
            this.children.push(child)
            if (count) {
                ++this.count
            }
        }
        return this
    }

    clearChildren() {
        this.children = []
    }

    get collapsed(): boolean {
        return this._collapsed
    }

    set collapsed(value: boolean) {
        if (!value && !this._fetched) {
            this.loading = true
            this._collapsedCallback()
                .catch(() => undefined)
                .then(() => {
                    this.loading = false
                })
            this._fetched = true
        }
        this._collapsed = value
    }
}
