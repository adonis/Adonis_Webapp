export default interface QuickActionInterface {

  icon: string
  name: string
  action: () => any
  active?: () => boolean
}
