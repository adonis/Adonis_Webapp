export default interface NavigationModuleInterface {

  name: string
  label: string
  selectable: boolean
  defaultRedirect?: string
}
