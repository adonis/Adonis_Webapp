import NavigationModuleInterface from '@adonis/webapp/stores/navigation/models/module.interface'

export default class Module implements NavigationModuleInterface {

  name: string
  label: string
  selectable: boolean
  defaultRedirect?: string

  constructor( props: NavigationModuleInterface ) {

    this.name = props.name
    this.label = props.label
    this.selectable = props.selectable
    this.defaultRedirect = props.defaultRedirect
  }
}
