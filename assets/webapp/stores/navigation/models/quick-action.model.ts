import QuickActionInterface from '@adonis/webapp/stores/navigation/models/quick-action.interface'

export default class QuickAction implements QuickActionInterface {

  icon: string
  name: string
  action: () => any
  active: () => boolean

  constructor( props: QuickActionInterface ) {

    this.icon = props.icon
    this.name = props.name
    this.action = props.action
    this.active = props.active ?? (() => true)
  }
}
