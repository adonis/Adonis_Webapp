import { ROUTE_CONNECT } from '@adonis/shared/router/routes-config'
import { ConnectionState } from '@adonis/shared/stores/connection/connection-state'
import { STORAGE_KEY } from '@adonis/shared/stores/connection/connection-store-module'
import routes from '@adonis/webapp/router/routes-roots'
import CHORUS from '@adonis/webapp/services/service-singleton'
import store from '@adonis/webapp/stores/vuex'
import Vue from 'vue'
import VueRouter, { Route } from 'vue-router'

Vue.use( VueRouter )

const router = new VueRouter( {
  routes,
} )

router.beforeEach( ( to: Route, from: Route, next: ( ...args: any ) => any ) => {
  if (ROUTE_CONNECT === to.name) {
    store.dispatch( 'connection/disconnect' )
        .then( next() )
  } else if (!!store.getters['connection/user']) {
    next()
  } else {
    if (!!window.localStorage.getItem( STORAGE_KEY )) {
      const oldState: ConnectionState = JSON.parse( window.localStorage.getItem( STORAGE_KEY ) )
      store.dispatch( 'connection/updateTokens', { token: oldState.userToken, refresh_token: oldState.refreshToken } )
          .then( () => CHORUS.userProvider.getByIri( oldState.user.iri ) )
          .then( user => store.dispatch( 'connection/updateUser', user )
              .then( () => user ) )
          .then( user => Promise.all(
              user.siteRoles.map( promise => promise.then( roleSite => store.dispatch( 'navigation/add2roleSite', { roleSite } ) ) )
          ) )
          .then( () => next() )
          .catch( () => next( { name: ROUTE_CONNECT } ) )
    } else {
      next( { name: ROUTE_CONNECT } )
    }

  }
} )

router.afterEach(((to, from) => store.dispatch('navigation/addVisitedRoute', {route: from})))

export default router
