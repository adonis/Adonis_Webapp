import {
    ROUTE_EDITOR_DATA_ENTRY_RETURN,
    ROUTE_EDITOR_DATA_ENTRY_TABLE,
    ROUTE_EDITOR_EXPORT_PROJECT_DATAS,
    ROUTE_EDITOR_FUSION_PROJECT_DATAS,
    ROUTE_EDITOR_IMPORT_IMPROVISED_PROJECT,
    ROUTE_EDITOR_IMPORT_PROJECT_DATAS,
    ROUTE_SITE,
} from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesModuleData: RouteConfig[] = [
    {
        name: ROUTE_EDITOR_DATA_ENTRY_RETURN,
        path: 'data-entry-return',
        props: route => ({statusDataEntryIri: route.query.statusDataEntryIri}),
        component: () => import('@adonis/webapp/components/modules/data/editors/DataEntryReturnEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_DATA_ENTRY_TABLE,
        path: 'data-entry-table',
        component: () => import('@adonis/webapp/components/modules/data/editors/DataEntryTableEditorWrapper.vue'),
        props: route => ({
            objectType: route.query.objectType,
            objectIri: typeof route.query.objectIri === 'string' ? [route.query.objectIri] : route.query.objectIri,
            edit: !!route.query.edit,
        }),
        beforeEnter: (to, from, next) => {
            if (to.query.objectIri && to.query.objectType) {
                next()
            } else { // Stay or return to module home page.
                next({name: ROUTE_SITE, params: to.params})
            }
        },
    },
    {
        name: ROUTE_EDITOR_IMPORT_IMPROVISED_PROJECT,
        path: 'import-improvised-project',
        component: () => import('@adonis/webapp/components/modules/data/editors/ImportImprovisedProjectEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_FUSION_PROJECT_DATAS,
        path: 'fusion-project-data',
        props: route => ({projectIri: route.query.projectIri}),
        component: () => import('@adonis/webapp/components/modules/data/editors/DataEntryFusionEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_IMPORT_PROJECT_DATAS,
        path: 'import-project-data',
        component: () => import('@adonis/webapp/components/modules/data/editors/DataEntryCsvImportEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_EXPORT_PROJECT_DATAS,
        path: 'export-project-data',
        props: route => ({dataEntryIri: route.query.dataEntryIri}),
        component: () => import('@adonis/webapp/components/modules/data/editors/DataEntryOpenSilexExportEditor.vue'),
    },
]
