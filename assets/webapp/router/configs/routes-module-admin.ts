import {
    ROUTE_EDITOR_MODULE_ADMIN_SITE_ASSOCIATE_USER,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_CREATE,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_DUPLICATE,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE,
    ROUTE_EDITOR_MODULE_ADMIN_SITE_UPDATE,
    ROUTE_EDITOR_OPEN_SILEX_INSTANCE,
    ROUTE_EDITOR_USER,
    ROUTE_EDITOR_USER_GROUP,
} from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesModuleAdmin: RouteConfig[] = [
    {
        name: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE,
        path: 'site/manage',
        component: () => import('@adonis/webapp/components/modules/admin/editors/SiteManageEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_MODULE_ADMIN_SITE_CREATE,
        path: 'site/create',
        component: () => import('@adonis/webapp/components/modules/admin/editors/SiteCreateEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_MODULE_ADMIN_SITE_UPDATE,
        path: 'site/update',
        component: () => import('@adonis/webapp/components/commons/admin/editors/SiteUpdateEditor.vue'),
        props: route => ({siteIri: route.query.siteIri, rollbackRoute: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE}),
        beforeEnter: (to, from, next) => {
            if (!to.query.siteIri) {
                next({name: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE, params: to.params})
            } else {
                next()
            }
        },
    },
    {
        name: ROUTE_EDITOR_MODULE_ADMIN_SITE_ASSOCIATE_USER,
        path: 'site/associate-user',
        component: () => import('@adonis/webapp/components/commons/admin/editors/RoleUserSiteEditor.vue'),
        props: route => ({siteIri: route.query.siteIri, rollbackRoute: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE}),
        beforeEnter: (to, from, next) => {
            if (!to.query.siteIri) {
                next({name: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE, params: to.params})
            } else {
                next()
            }
        },
    },
    {
        name: ROUTE_EDITOR_MODULE_ADMIN_SITE_DUPLICATE,
        path: 'site/duplicate',
        component: () => import('@adonis/webapp/components/modules/admin/editors/SiteDuplicateEditor.vue'),
    },
    {
        name: ROUTE_EDITOR_USER_GROUP,
        path: 'user-group',
        component: () => import('@adonis/webapp/components/modules/admin/editors/UserGroupEditor.vue'),
        props: route => ({userGroupIri: route.query.userGroupIri}),
    },
    {
        name: ROUTE_EDITOR_USER,
        path: 'user',
        component: () => import('@adonis/webapp/components/commons/admin/editors/UserEditor.vue'),
        props: route => ({userIri: route.query.userIri, rollbackRoute: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE}),
    },
    {
        name: ROUTE_EDITOR_OPEN_SILEX_INSTANCE,
        path: 'open-silex-instance',
        component: () => import('@adonis/webapp/components/modules/admin/editors/OpenSilexInstanceEditor.vue'),
        props: route => ({openSilexInstanceIri: route.query.openSilexInstanceIri}),
    },
]
