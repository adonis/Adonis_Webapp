import { ROUTE_EDITOR_IMPORT_REQUEST_FILE, ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS } from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesModuleTransfer: RouteConfig[] = [
  {
    name: ROUTE_EDITOR_MODULE_TRANSFER_PROJECT_STATUS,
    path: 'transfer-project-status',
    props: route => ({ projectIri: route.query.projectIri }),
    component: () => import('@adonis/webapp/components/modules/transfer/editors/TransferProjectStatusEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_IMPORT_REQUEST_FILE,
    path: 'import-request-file',
    component: () => import('@adonis/webapp/components/modules/transfer/editors/ImportRequestFileEditor.vue'),
  },
]
