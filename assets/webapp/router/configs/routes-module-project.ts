import {
  ROUTE_EDITOR_DATA_ENTRY_PROJECT,
  ROUTE_EDITOR_DEVICE,
  ROUTE_EDITOR_DUPLICATE_PROJECT,
  ROUTE_EDITOR_GENERATOR_VARIABLE,
  ROUTE_EDITOR_IMPORT_VARIABLE_OPENSILEX,
  ROUTE_EDITOR_LINK_VALUE_LIST,
  ROUTE_EDITOR_MODULE_PROJECT_LINK_STATE_CODE_TO_PROJECT,
  ROUTE_EDITOR_MODULE_PROJECT_LINK_VARIABLE_TO_PROJECT,
  ROUTE_EDITOR_MODULE_PROJECT_ORDER_VARIABLE,
  ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
  ROUTE_EDITOR_MODULE_PROJECT_TRANSFER_TO_MOBILE,
  ROUTE_EDITOR_PATH_BASE_CSV_IMPORT,
  ROUTE_EDITOR_PATH_CONFIGURATION,
  ROUTE_EDITOR_REQUIRED_ANNOTATION,
  ROUTE_EDITOR_STATE_CODE,
  ROUTE_EDITOR_USER_PATH,
  ROUTE_EDITOR_VALUE_LIST,
  ROUTE_EDITOR_VARIABLE,
  ROUTE_EDITOR_VARIABLE_SCALE,
  ROUTE_EDITOR_VARIABLE_SEMI_AUTOMATIC,
  ROUTE_SITE,
} from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesModuleProject: RouteConfig[] = [

  {
    name: ROUTE_EDITOR_GENERATOR_VARIABLE,
    path: 'generator-variable',
    props: route => ({ variableIri: route.query.variableIri, projectIri: route.query.projectIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/GeneratorVariableEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_VARIABLE,
    path: 'variable',
    props: route => ({ variableIri: route.query.variableIri, projectIri: route.query.projectIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/VariableEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_VALUE_LIST,
    path: 'value-list',
    props: route => ({ valueListIri: route.query.valueListIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/ValueListEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_STATE_CODE,
    path: 'state-code',
    props: route => ({ stateCodeIri: route.query.stateCodeIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/StateCodeEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_DEVICE,
    path: 'device',
    props: route => ({ deviceIri: route.query.deviceIri, projectIri: route.query.projectIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/DeviceEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_DATA_ENTRY_PROJECT,
    path: 'data-entry-project',
    component: () => import('@adonis/webapp/components/modules/project/editors/DataEntryProjectEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_MODULE_PROJECT_TRANSFER_TO_MOBILE,
    path: 'data-entry-project-transfer-to-mobile',
    props: route => ({ projectIri: route.query.projectIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/TransferProjectToMobileEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_REQUIRED_ANNOTATION,
    path: 'required-annotation',
    component: () => import('@adonis/webapp/components/modules/project/editors/RequiredAnnotationEditor.vue'),
    props: route => ({ requiredAnnotationIri: route.query.requiredAnnotationIri, projectIri: route.query.projectIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri && !to.query.requiredAnnotationIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_PATH_CONFIGURATION,
    path: 'path-configuration',
    component: () => import('@adonis/webapp/components/modules/project/editors/PathBaseEditor.vue'),
    props: route => ({ projectIri: route.query.projectIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_USER_PATH,
    path: 'user-path',
    component: () => import('@adonis/webapp/components/modules/project/editors/PathForUserEditor.vue'),
    props: route => ({ projectIri: route.query.projectIri, userPathIri: route.query.userPathIri, duplicate: !!route.query.duplicate }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_MODULE_PROJECT_TEST_ON_VARIABLE,
    path: 'test-on-variable',
    component: () => import('@adonis/webapp/components/modules/project/editors/TestOnVariableEditor.vue'),
    props: route => ({ variableIri: route.query.variableIri, testIri: route.query.testIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.variableIri && !to.query.testIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_MODULE_PROJECT_LINK_VARIABLE_TO_PROJECT,
    path: 'link-variable',
    component: () => import('@adonis/webapp/components/modules/project/editors/VariableSelectionEditor.vue'),
    props: route => ({ projectIri: route.query.projectIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_MODULE_PROJECT_LINK_STATE_CODE_TO_PROJECT,
    path: 'link-state-code',
    component: () => import('@adonis/webapp/components/modules/project/editors/StateCodeSelectionEditor.vue'),
    props: route => ({ projectIri: route.query.projectIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_MODULE_PROJECT_ORDER_VARIABLE,
    path: 'order-variable',
    component: () => import('@adonis/webapp/components/modules/project/editors/OrderVariableEditor.vue'),
    props: route => ({ projectIri: route.query.projectIri, generatorVariableIri: route.query.generatorVariableIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri && !to.query.generatorVariableIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_VARIABLE_SEMI_AUTOMATIC,
    path: 'semi-automatic-variable',
    props: route => ({ variableIri: route.query.variableIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/SemiAutomativVariableEditor.vue'),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.variableIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_VARIABLE_SCALE,
    path: 'variable-scale',
    props: route => ({ variableIri: route.query.variableIri}),
    component: () => import('@adonis/webapp/components/modules/project/editors/VariableScaleEditor.vue'),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.variableIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_LINK_VALUE_LIST,
    path: 'link-value-list',
    props: route => ({ variableIri: route.query.variableIri}),
    component: () => import('@adonis/webapp/components/modules/project/editors/LinkValueListEditor.vue'),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.variableIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_DUPLICATE_PROJECT,
    path: 'duplicate-project',
    props: route => ({ projectIri: route.query.projectIri }),
    component: () => import('@adonis/webapp/components/modules/project/editors/DuplicateProjectEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_PATH_BASE_CSV_IMPORT,
    path: 'pathBaseCsvImport',
    props: route => ({ projectIri: route.query.projectIri}),
    component: () => import('@adonis/webapp/components/modules/project/editors/PathBaseCsvImportEditor.vue'),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.projectIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
    {
        name: ROUTE_EDITOR_IMPORT_VARIABLE_OPENSILEX,
        path: 'import-variable-opensilex',
        props: route => ({projectIri: route.query.projectIri}),
        component: () => import('@adonis/webapp/components/modules/project/editors/OpenSilexVariableEditor.vue'),
    },
]
