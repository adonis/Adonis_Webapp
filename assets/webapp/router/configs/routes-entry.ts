import RoleEnum from '@adonis/shared/constants/role-enum'
import { routesModuleAdmin } from '@adonis/webapp/router/configs/routes-module-admin'
import { routesModuleCommons } from '@adonis/webapp/router/configs/routes-module-commons'
import { routesModuleData } from '@adonis/webapp/router/configs/routes-module-data'
import { routesModuleDesign } from '@adonis/webapp/router/configs/routes-module-design'
import { routesModuleProject } from '@adonis/webapp/router/configs/routes-module-project'
import { routesModuleTransfer } from '@adonis/webapp/router/configs/routes-module-transfer'
import { routesSuperAdmin } from '@adonis/webapp/router/configs/routes-super-admin'
import { ROUTE_DASHBOARD, ROUTE_SITE, ROUTE_SUPER_ADMIN, ROUTE_USER_INFORMATIONS } from '@adonis/webapp/router/routes-names'
import CHORUS from '@adonis/webapp/services/service-singleton'
import store from '@adonis/webapp/stores/vuex'
import { RouteConfig } from 'vue-router/types/router'

export const routesEntry: RouteConfig[] = [
  {
    name: ROUTE_SITE,
    path: 's/:roleSiteIri/m/:moduleName',
    component: () => import('@adonis/webapp/components/modules/AdoWebappSite.vue'),
    children: [
      ...routesModuleCommons,
      ...routesModuleDesign,
      ...routesModuleProject,
      ...routesModuleTransfer,
      ...routesModuleData,
      ...routesModuleAdmin,
    ],
  },
  {
    name: ROUTE_DASHBOARD,
    path: '',
    component: () => import('@adonis/webapp/components/dashboard/AdoDashboard.vue'),
  },
  {
    name: ROUTE_USER_INFORMATIONS,
    path: 'user-informations',
    component: () => import('@adonis/webapp/components/user-informations/UserInformations.vue'),
  },
  {
    name: ROUTE_SUPER_ADMIN,
    path: 'super-admin',
    component: () => import('@adonis/webapp/components/super-admin/AdoSuperAdmin.vue'),
    beforeEnter: ( to, from, next ) => {
      const roles: RoleEnum[] = store.getters['connection/user'].roles
      if (roles.includes( RoleEnum.ADMIN )) {
        next()
      } else {
        CHORUS.notificationService.accessDenied()
        next( { name: ROUTE_DASHBOARD } )
      }

    },
    children: [
      ...routesSuperAdmin,
    ],
  },
]
