import GraphicalEditorModeEnum from '@adonis/webapp/constants/graphical-editor-mode-enum'
import {
  ROUTE_EDITOR_ADVANCED_RIGHTS,
  ROUTE_EDITOR_GRAPHIC,
  ROUTE_EDITOR_GRAPHICAL_CONFIGURATION,
  ROUTE_NOTE_PAD,
  ROUTE_PDF_GENERATOR,
} from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesModuleCommons: RouteConfig[] = [
  {
    name: ROUTE_EDITOR_ADVANCED_RIGHTS,
    path: 'right',
    component: () => import('@adonis/webapp/components/commons/rights/editors/AdvancedRightEditor.vue'),
    props: route => ({ objectId: route.query.objectId, objectType: route.query.objectType, owner: route.query.owner }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.objectId || !to.query.objectType || !to.query.owner) {
        next( false )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_GRAPHIC,
    path: 'graphical-editor',
    component: () => import('@adonis/webapp/components/commons/graphics/editors/GraphicalEditor.vue'),
    props: route => ({
      objectIri: route.query.objectIri,
      mode: route.query.mode,
      pathBaseIri: route.query.pathBaseIri,
      userPathIri: route.query.userPathIri,
      experimentIris: Array.isArray(route.query.experimentIris) ? route.query.experimentIris : [],
      variableIri: route.query.variableIri,
    }),
    beforeEnter: ( to, from, next ) => {
      if (
          !to.query.objectIri || !to.query.mode ||
          (to.query.mode === GraphicalEditorModeEnum.PLACE_EXPERIMENT && !to.query.experimentIris) ||
          (to.query.mode === GraphicalEditorModeEnum.VIEW_PATH && !to.query.pathBaseIri && !to.query.userPathIri) ||
          (to.query.mode === GraphicalEditorModeEnum.VIEW_VARIABLE && !to.query.variableIri)
      ) {
        next( false )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_NOTE_PAD,
    path: 'note-pad',
    component: () => import('@adonis/webapp/components/commons/note-pad/editors/NoteEditor.vue'),
    props: route => ({ objectId: route.query.objectId, objectType: route.query.objectType, edit: !!route.query.edit }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.objectId || !to.query.objectType) {
        next( false )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_GRAPHICAL_CONFIGURATION,
    path: 'graphical-configuration',
    component: () => import('@adonis/webapp/components/commons/graphics/editors/GraphicalConfigurationEditor.vue'),
  },
  {
    name: ROUTE_PDF_GENERATOR,
    path: 'pdf-generator',
    component: () => import('@adonis/webapp/components/commons/pdf/PdfGenerator.vue'),
    props: route => ({ objectIri: route.query.objectIri, pdfType: route.query.pdfType }),
  },
]
