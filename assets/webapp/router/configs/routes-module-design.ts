import {
  ROUTE_EDITOR_DUPLICATE_EXPERIMENT,
  ROUTE_EDITOR_EXPERIMENT,
  ROUTE_EDITOR_EXPERIMENT_CSV_IMPORT, ROUTE_EDITOR_EXPERIMENT_EXPORT,
  ROUTE_EDITOR_EXPERIMENT_IDENTIFICATION_CODE,
  ROUTE_EDITOR_EXPERIMENT_STATE,
  ROUTE_EDITOR_FACTOR, ROUTE_EDITOR_GERMPLASM_FETCH,
  ROUTE_EDITOR_LINK_EXPERIMENT,
  ROUTE_EDITOR_LINKED_VARIABLE,
  ROUTE_EDITOR_OEZ_NATURE,
  ROUTE_EDITOR_PLATFORM,
  ROUTE_EDITOR_PLATFORM_GRAPHICAL_INFO,
  ROUTE_EDITOR_PROTOCOL,
  ROUTE_EDITOR_PROTOCOL_CSV_IMPORT,
  ROUTE_SITE,
} from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesModuleDesign: RouteConfig[] = [
  {
    name: ROUTE_EDITOR_FACTOR,
    path: 'factor',
    component: () => import('@adonis/webapp/components/modules/design/editors/FactorEditor.vue'),
    props: route => ({ factorIri: route.query.factorIri }),
  },
  {
    name: ROUTE_EDITOR_OEZ_NATURE,
    path: 'oez-nature',
    component: () => import('@adonis/webapp/components/modules/design/editors/OezNatureEditor.vue'),
    props: route => ({ oezNatureIri: route.query.oezNatureIri }),
  },
  {
    name: ROUTE_EDITOR_PROTOCOL,
    path: 'protocol',
    component: () => import('@adonis/webapp/components/modules/design/editors/ProtocolEditor.vue'),
    props: route => ({ protocolIri: route.query.protocolIri }),
  },
  {
    name: ROUTE_EDITOR_EXPERIMENT,
    path: 'experiment',
    component: () => import('@adonis/webapp/components/modules/design/editors/ExperimentEditor.vue'),
    props: route => ({ experimentIri: route.query.experimentIri }),
  },
  {
    name: ROUTE_EDITOR_LINKED_VARIABLE,
    path: 'linked-variable',
    component: () => import('@adonis/webapp/components/modules/project/editors/LinkedVariableEditor.vue'),
    props: route => ({ projectIri: route.query.projectIri }),
  },
  {
    name: ROUTE_EDITOR_PLATFORM,
    path: 'platform',
    component: () => import('@adonis/webapp/components/modules/design/editors/PlatformEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_PLATFORM_GRAPHICAL_INFO,
    path: 'platformGraphicalInfo',
    component: () => import('@adonis/webapp/components/modules/design/editors/PlatformGraphicInfoEditor.vue'),
    props: route => ({ platformIri: route.query.platformIri }),
  },
  {
    name: ROUTE_EDITOR_LINK_EXPERIMENT,
    path: 'linkExperiment',
    component: () => import('@adonis/webapp/components/modules/design/editors/LinkExperimentEditor.vue'),
    props: route => ({ platformIri: route.query.platformIri }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.platformIri) {
        next( { name: ROUTE_SITE, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_EXPERIMENT_STATE,
    path: 'experimentState',
    component: () => import('@adonis/webapp/components/modules/design/editors/ExperimentStateEditor.vue'),
    props: route => ({ experimentIri: route.query.experimentIri }),
  },
  {
    name: ROUTE_EDITOR_EXPERIMENT_IDENTIFICATION_CODE,
    path: 'experimentIdentificationCode',
    component: () => import('@adonis/webapp/components/modules/design/editors/IdentificationCodeEditor.vue'),
    props: route => ({ experimentIri: route.query.experimentIri }),
  },
  {
    name: ROUTE_EDITOR_EXPERIMENT_CSV_IMPORT,
    path: 'experimentCsvImport',
    component: () => import('@adonis/webapp/components/modules/design/editors/ExperimentCsvImportEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_PROTOCOL_CSV_IMPORT,
    path: 'protocolCsvImport',
    component: () => import('@adonis/webapp/components/modules/design/editors/ProtocolCsvImportEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_DUPLICATE_EXPERIMENT,
    path: 'duplicate-experiment',
    props: route => ({ experimentIri: route.query.experimentIri }),
    component: () => import('@adonis/webapp/components/modules/design/editors/DuplicateExperimentEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_GERMPLASM_FETCH,
    path: 'germplasm-fetch',
    component: () => import('@adonis/webapp/components/modules/design/editors/GermplasmFetchEditor.vue'),
    props: route => ({ factorIri: route.query.factorIri }),
  },
  {
    name: ROUTE_EDITOR_EXPERIMENT_EXPORT,
    path: 'experiment-export',
    component: () => import('@adonis/webapp/components/modules/design/editors/ExperimentExportEditor.vue'),
    props: route => ({ experimentIri: route.query.experimentIri }),
  },
]
