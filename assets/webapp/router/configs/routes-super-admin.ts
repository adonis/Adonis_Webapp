import {
  ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE,
  ROUTE_EDITOR_PARAMETER,
  ROUTE_EDITOR_ROLE_USER_SITE,
  ROUTE_EDITOR_SITE,
  ROUTE_EDITOR_SITE_LIST,
  ROUTE_EDITOR_SUPER_ADMIN_SITE_UPDATE,
  ROUTE_EDITOR_SUPER_ADMIN_USER,
  ROUTE_EDITOR_USER_LIST,
  ROUTE_SUPER_ADMIN,
} from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

export const routesSuperAdmin: RouteConfig[] = [
  {
    name: ROUTE_EDITOR_USER_LIST,
    path: 'user-list',
    component: () => import('@adonis/webapp/components/super-admin/editors/UserListEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_SUPER_ADMIN_USER,
    path: 'user',
    component: () => import('@adonis/webapp/components/commons/admin/editors/UserEditor.vue'),
    props: route => ({ userIri: route.query.userIri, rollbackRoute: ROUTE_EDITOR_USER_LIST }),
  },
  {
    name: ROUTE_EDITOR_SITE_LIST,
    path: 'site-list',
    component: () => import('@adonis/webapp/components/super-admin/editors/SiteListEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_SITE,
    path: 'site-create',
    component: () => import('@adonis/webapp/components/super-admin/editors/SiteWithAdminEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_ROLE_USER_SITE,
    path: 'role-user-site',
    component: () => import('@adonis/webapp/components/commons/admin/editors/RoleUserSiteEditor.vue'),
    props: route => ({ siteIri: route.query.siteIri, rollbackRoute: ROUTE_SUPER_ADMIN }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.siteIri) {
        next( { name: ROUTE_SUPER_ADMIN, params: to.params } )
      } else {
        next()
      }
    },
  },
  {
    name: ROUTE_EDITOR_PARAMETER,
    path: 'parameters',
    component: () => import('@adonis/webapp/components/super-admin/editors/ParameterEditor.vue'),
  },
  {
    name: ROUTE_EDITOR_SUPER_ADMIN_SITE_UPDATE,
    path: 'site/update',
    component: () => import('../../components/commons/admin/editors/SiteUpdateEditor.vue'),
    props: route => ({ siteIri: route.query.siteIri, rollbackRoute: ROUTE_EDITOR_SITE_LIST }),
    beforeEnter: ( to, from, next ) => {
      if (!to.query.siteIri) {
        next( { name: ROUTE_EDITOR_MODULE_ADMIN_SITE_MANAGE, params: to.params } )
      } else {
        next()
      }
    },
  },
]
