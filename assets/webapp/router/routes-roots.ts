import { ROUTE_CONNECT } from '@adonis/shared/router/routes-config'
import { routesEntry } from '@adonis/webapp/router/configs/routes-entry'
import { ROUTE_DASHBOARD, ROUTE_DEFAULT } from '@adonis/webapp/router/routes-names'
import { RouteConfig } from 'vue-router/types/router'

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('@adonis/webapp/components/AdoWebappEntry.vue'),
    children: [
      ...routesEntry,
    ],
  },
  {
    name: ROUTE_CONNECT,
    path: '/connect',
    component: () => import('@adonis/webapp/components/connection/WebappConnection.vue'),
    props: { application: 'Webapp', redirectRouteName: ROUTE_DASHBOARD },
  },
  {
    name: ROUTE_DEFAULT,
    path: '/*',
    redirect: { name: ROUTE_CONNECT },
  },
]

export default routes
