import i18n from '@adonis/app/plugins/vue-i18n'
import '@adonis/app/plugins/vue-indexedDb'
import router from '@adonis/app/plugins/vue-router'
import '@adonis/app/plugins/vue-speech-recognition'
import store from '@adonis/app/plugins/vuex'

import AppHttpService from '@adonis/app/services/app-http-service'
import '@adonis/shared/closable-directive'
import ObjectActionEnum from '@adonis/shared/constants/object-action-enum'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { RelUserSiteDto } from '@adonis/shared/models/dto/rel-user-site-dto'
import { UserDto } from '@adonis/shared/models/dto/user-dto'

import User from '@adonis/shared/models/user'
import '@adonis/shared/plugins/moment-vue'
import '@adonis/shared/plugins/portal-vue'
import vuetify from '@adonis/shared/plugins/vuetify'
import { ROUTE_CONNECT } from '@adonis/shared/router/routes-config'
import TransformerService from '@adonis/shared/services/transformer-service'
import RelUserSiteTransformer from '@adonis/shared/services/transformers/rel-user-site-transformer'
import UserTransformer from '@adonis/shared/services/transformers/user-transformer'
import { ConnectionState } from '@adonis/shared/stores/connection/connection-state'
import { STORAGE_KEY } from '@adonis/shared/stores/connection/connection-store-module'
import { Route } from 'vue-router'

import App from './App.vue'

if ('serviceWorker' in navigator) {
    // Use the window load event to keep the page load performant
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/app/service-worker.js')
            .catch(registrationError => {
                console.log('SW registration failed: ', registrationError)
            })
    })
}

router.beforeEach((to: Route, from: Route, next: (...args: any) => any) => {
    if (ROUTE_CONNECT === to.name) {
        store.dispatch('connection/disconnect')
            .then(next())
    } else if (!!store.getters['connection/user']) {
        next()
    } else {
        if (!!window.localStorage.getItem(STORAGE_KEY)) {
            const httpService = new AppHttpService(store, {
                accessDenied() {
                },
                defaultObjectFailure(action: ObjectActionEnum) {
                },
                objectSuccess(object: ObjectTypeEnum, action: ObjectActionEnum) {
                },
            })
            const oldState: ConnectionState = JSON.parse(window.localStorage.getItem(STORAGE_KEY))
            store.dispatch('connection/updateTokens', {token: oldState.userToken, refresh_token: oldState.refreshToken})
                .then(() => {
                    return httpService.get<UserDto>(oldState.user.iri)
                        .then(value => {
                            return store.dispatch('connection/updateUser', (new TransformerService(httpService)).userTransformer.dtoToObject(value.data))
                        })
                        .catch(() => {
                            return store.dispatch('connection/updateUser', new User(
                                oldState.user.iri,
                                oldState.user.id,
                                oldState.user.username,
                                oldState.user.name,
                                oldState.user.surname,
                                oldState.user.email,
                                oldState.user.siteRolesIriTab,
                                () => {
                                    const promise = httpService.getAll<RelUserSiteDto>(httpService.getEndpointFromType(ObjectTypeEnum.ROLE_USER_SITE),
                                        {pagination: false, user: oldState.user.iri})
                                    return oldState.user.siteRolesIriTab.map((uri, index) => {
                                        return promise
                                            .then(response => (new RelUserSiteTransformer(httpService)).dtoToObject(response.data['hydra:member'][index]))
                                    })
                                },
                                oldState.user.active,
                                oldState.user.roles,
                                oldState.user.avatar,
                            ))
                        })
                })
                .then(() => next())
                .catch(e => next({name: ROUTE_CONNECT}))
        } else {
            next({name: ROUTE_CONNECT})
        }

    }
})

// Montage de l'application Vue avec pour racine le composant App dans la div id="app" de l'html.
new App({
    vuetify,
    router,
    store,
    i18n,
}).$mount('#app')
