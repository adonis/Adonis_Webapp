import BreadcrumbData from '@adonis/app/models/app-functionalities/breadcrumb-data'
import BusinessBranch from '@adonis/app/models/app-functionalities/business-branch'
import Block, {BLOCK_CLASS} from '@adonis/app/models/business-objects/block'
import {BusinessObject} from '@adonis/app/models/business-objects/business-object'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import Device, {DEVICE_CLASS} from '@adonis/app/models/business-objects/device'
import Individual, {INDIVIDUAL_CLASS} from '@adonis/app/models/business-objects/individual'
import Platform from '@adonis/app/models/business-objects/platform'
import SubBlock, {SUBBLOCK_CLASS} from '@adonis/app/models/business-objects/sub-block'
import Treatment from '@adonis/app/models/business-objects/treatment'
import UnitParcel, {UNITPARCEL_CLASS} from '@adonis/app/models/business-objects/unit-parcel'
import {HasAnnotations} from '@adonis/app/models/evaluation-variables/annotation'
import {Evaluable} from '@adonis/app/models/evaluation-variables/evaluation'
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";

/**
 * class PlatformService.
 */
export default class PlatformService {

  /**
   * Retrieve the current evaluable.
   *
   * @param project
   * @private
   */
  public static getLastEvaluable( project: DataEntryProject ) {
    return !!project.dataEntry.lastPosition
        ? PlatformService.getObjectFromUri( project.platform, project.dataEntry.lastPosition )
        : null
  }

  /**
   * Retrieve the current evaluable.
   *
   * @param project
   * @private
   */
  public static getCurrentEvaluable( project: DataEntryProject ) {
    return !!project.dataEntry.currentPosition
        ? PlatformService.getObjectFromUri( project.platform, project.dataEntry.currentPosition )
        : null
  }

  /**
   * Get all Device instances throughout the platform.
   *
   * @param platform
   *
   * @return Device[]
   */
  static getDevices( platform: Platform ): Device[] {
    return platform.devices
  }

  /**
   * Get all Block instances throughout the platform.
   *
   * @param platform
   *
   * @return Block[]
   */
  static getBlocks( platform: Platform ): Block[] {
    let blocks: Block[] = []
    this.getDevices( platform )
        .forEach( ( device: Device ) => {
          blocks = [...blocks, ...device.blocks]
        } )
    return blocks
  }

  /**
   * Get all SubBlock instances throughout the platform.
   *
   * @param platform
   *
   * @return SubBlock[]
   */
  static getSubBlocks( platform: Platform ): SubBlock[] {
    let subBlocks: SubBlock[] = []
    this.getBlocks( platform )
        .forEach( ( block: Block ) => {
          subBlocks = [...subBlocks, ...block.subBlocks]
        } )
    return subBlocks
  }

  /**
   * Get all UnitParcel instances throughout the platform.
   *
   * @param platform
   *
   * @return UnitParcel[]
   */
  static getUnitParcels( platform: Platform ): UnitParcel[] {
    let unitParcels: UnitParcel[] = []
    this.getBlocks( platform )
        .forEach( ( block: Block ) => {
          unitParcels = [...unitParcels, ...block.unitParcels]
        } )
    this.getSubBlocks( platform )
        .forEach( ( subBlock: SubBlock ) => {
          unitParcels = [...unitParcels, ...subBlock.unitParcels]
        } )
    return unitParcels
  }

  /**
   * Get all UnitParcel instances throughout the platform which are linked to the given treatment.
   *
   * @param platform
   * @param treatment
   *
   * @return UnitParcel[]
   */
  static getUnitParcelsByTreatment( platform: Platform, treatment: Treatment ): UnitParcel[] {
    return this.getUnitParcels( platform )
        .filter( ( parcel: UnitParcel ) => {
          return treatment.name === parcel.treatmentName
        } )
  }

  /**
   * Get all Treatment instances throughout the platform.
   *
   * @param platform
   *
   * @return Treatment[]
   */
  static getTreatments( platform: Platform ): Treatment[] {
    let treatments: Treatment[] = []
    this.getDevices( platform )
        .forEach( ( device: Device ) => {
          treatments = [...treatments, ...device.protocol.treatments]
        } )
    return treatments
  }

  /**
   * Retrieve a Treatment instance by its name.
   *
   * @param platform
   * @param treatmentName
   *
   * @return Treatment
   */
  static getTreatmentByName( platform: Platform, treatmentName: string ): Treatment {
    let treatments: Treatment[] = []
    this.getDevices( platform )
        .forEach( ( device: Device ) => {
          treatments = [...treatments, ...device.protocol.treatments]
        } )
    return treatments.filter( ( treatment: Treatment ) => {
      return treatmentName === treatment.name
    } )[0]
  }

  /**
   * Get all Individual instances throughout the platform.
   *
   * @param platform
   *
   * @return Individual[]
   */
  static getIndividuals( platform: Platform ): Individual[] {
    let individuals: Individual[] = []
    this.getUnitParcels( platform )
        .forEach( ( unitParcel: UnitParcel ) => {
          individuals = [...individuals, ...unitParcel.individuals]
        } )
    return individuals
  }

  /**
   * Get all objects in the platform at the requested level (class name).
   *
   * @param platform
   * @param className
   *
   * @return Evaluable[]
   */
  static getObjectsOfLevel( platform: Platform, className: string ): Evaluable[] {
    let objects: Evaluable[] = []
    switch (className) {
      case DEVICE_CLASS:
        objects = this.getDevices( platform )
        break
      case BLOCK_CLASS:
        objects = this.getBlocks( platform )
        break
      case SUBBLOCK_CLASS:
        objects = this.getSubBlocks( platform )
        break
      case UNITPARCEL_CLASS:
        objects = this.getUnitParcels( platform )
        break
      case INDIVIDUAL_CLASS:
        objects = this.getIndividuals( platform )
        break
      default:
        throw Error( 'Unable to determine the object class : ' + className )
    }
    return objects
  }

  /**
   * Get the object behind a given uri.
   *
   * @param platform
   * @param uri
   *
   * @return Evaluable
   */
  static getObjectFromUri( platform: Platform, uri: string ): Evaluable {
    let object: Evaluable = null
    if (!!uri) {
      object = platform.uriMap.get( uri )
    }
    return object
  }

  /**
   * First individual position.
   *
   * @param object
   *
   * @return number[]
   */
  public static getParcelCoordinate( object: Evaluable ): number[] {
    let coordinate: number[]
    if (object instanceof UnitParcel) {
      const parcelleActualCoordinate = [object.x ?? -1, object.y ?? -1]
      coordinate = object.individuals?.reduce( ( acc: number[], individual: Individual ) => {
            return 0 >= acc[0] || acc[0] >= individual.x && acc[1] >= individual.y ? [individual.x, individual.y] : acc
          }, parcelleActualCoordinate )
          ?? parcelleActualCoordinate
    } else if (object instanceof Individual) {
      coordinate = [object.x, object.y]
    } else {
      coordinate = null
    }
    return coordinate
  }

  /**
   * Get the breadcrumb information to display about the object behind the given uri.
   *
   * @param platform
   * @param targetUri
   *
   * @return BreadcrumbData
   */
  static getBreadcrumbsData( platform: Platform, targetUri: string ): BreadcrumbData {

    const object: Evaluable = platform.uriMap.get( targetUri )

    // Get the position if exists: Available for Individuals and UnitParcels.
    let positionValue = '-'
    const position = this.getParcelCoordinate( object )
    if (!!position) {
      positionValue = `${position[0]},${position[1]}`
    }

    // Get the id of the element (bar-code or RFID).
    const identValue: string = object instanceof Individual || object instanceof UnitParcel
        ? object.ident
        : '-'

    // Get structure information.
    const { individual, parcel, subBlock, block, device } = this.getBranchObjects( object )
    const treatment = !!parcel && !!parcel.treatmentName
        ? this.getTreatmentByName( platform, parcel.treatmentName )
        : null

    const individualName: string = !!individual
        ? individual.name
        : '-'
    const unitParcelName: string = !!parcel
        ? parcel.name
        : '-'
    const treatmentName: string = !!treatment
        ? treatment.shortName
        : '-'
    const subBlockName: string = !!subBlock
        ? subBlock.name
        : '-'
    const blockName: string = !!block
        ? block.name
        : '-'
    const deviceName: string = !!device
        ? device.name
        : '-'

    return new BreadcrumbData(
        deviceName,
        blockName,
        subBlockName,
        unitParcelName,
        treatmentName,
        individualName,
        identValue,
        positionValue,
        new BusinessBranch(
            individual,
            parcel,
            subBlock,
            block,
            device,
        ),
    )
  }

  /**
   * Get all objects on the same branch from device to the given object, relative to the structure.
   *
   * @param object
   *
   * @return BusinessBranch
   */
  static getBranchObjects( object: Evaluable ): BusinessBranch {
    const individual = object instanceof Individual
        ? object
        : null
    let parcel = object instanceof UnitParcel
        ? object
        : null
    if (!parcel && !!individual) {
      parcel = individual.parent as UnitParcel
    }
    let subBlock = object instanceof SubBlock
        ? object
        : null
    let block = object instanceof Block
        ? object
        : null

    if (!!subBlock) {
      block = subBlock.parent
    } else if (!!parcel && !!parcel.parent) {
      if (parcel.parent instanceof SubBlock) {
        subBlock = parcel.parent as SubBlock
        block = subBlock.parent as Block
      } else {
        block = parcel.parent
      }
    }

    let device: Device = object instanceof Device
        ? object
        : null
    if (!device && !!block) {
      device = block.parent as Device
    }
    return new BusinessBranch( individual, parcel, subBlock, block, device )
  }

  /**
   *
   * @param branch
   * @param level
   * @return Evaluable|null
   */
  public static getObjectOfLevelFromBranch( branch: BusinessBranch, level: PathLevelEnum ): Evaluable {
    let evaluable: Evaluable;
    switch (level) {
      case PathLevelEnum.EXPERIMENT:
        evaluable = branch.device
        break
      case PathLevelEnum.BLOCK:
        evaluable = branch.block
        break
      case PathLevelEnum.SUB_BLOCK:
        evaluable = branch.subBlock
        break
      case PathLevelEnum.UNIT_PLOT:
        evaluable = branch.parcel
        break
      case PathLevelEnum.INDIVIDUAL:
        evaluable = branch.individual
        break
      default:
        evaluable = null;
        break
    }
    return evaluable
  }

  /**
   * Get the parent of an object given its uri.
   *
   * @param platform
   * @param targetUri
   *
   * @return UnitParcel|SubBlock|Block|Device
   */
  static getParentOfObject( platform: Platform, targetUri: string ): UnitParcel | SubBlock | Block | Device {
    const targetObject = this.getObjectFromUri( platform, targetUri ) as BusinessObject
    return targetObject.parent as UnitParcel | SubBlock | Block | Device
  }

  /**
   * Given an uri, returns its type.
   *
   * @param objectUri
   *
   * @return string
   */
  static getTypeFromUri( objectUri: string ): string {
    return objectUri.split( '.' )[0]
  }

  /**
   * Given an uri, returns its path level.
   *
   * @param objectUri
   *
   * @return PathLevelEnum
   */
  public static getPathLevelFromUri( objectUri: string ): PathLevelEnum {
    let pathLevel: PathLevelEnum;
    switch (this.getTypeFromUri( objectUri )) {
      case DEVICE_CLASS:
        pathLevel = PathLevelEnum.EXPERIMENT;
        break
      case BLOCK_CLASS:
        pathLevel = PathLevelEnum.BLOCK;
        break
      case SUBBLOCK_CLASS:
        pathLevel = PathLevelEnum.SUB_BLOCK;
        break
      case UNITPARCEL_CLASS:
        pathLevel = PathLevelEnum.UNIT_PLOT;
        break
      case INDIVIDUAL_CLASS:
        pathLevel = PathLevelEnum.INDIVIDUAL;
        break
      default:
        pathLevel = null;
        break
    }

    return pathLevel;
  }

  /**
   * @param object
   * @param vComponent
   * @return string
   */
  static objectName( object: HasAnnotations | Evaluable, vComponent: Vue ): string {
    let className: string
    switch (this.getTypeFromUri( (object as Evaluable).uri )) {
      case DEVICE_CLASS:
        className = vComponent.$t( 'dataEntry.form.breadcrumb.device.short' ).toString()
        break
      case BLOCK_CLASS:
        className = vComponent.$t( 'dataEntry.form.breadcrumb.block.short' ).toString()
        break
      case SUBBLOCK_CLASS:
        className = vComponent.$t( 'dataEntry.form.breadcrumb.subBlock.short' ).toString()
        break
      case UNITPARCEL_CLASS:
        className = vComponent.$t( 'dataEntry.form.breadcrumb.unitParcel.short' ).toString()
        break
      case INDIVIDUAL_CLASS:
        className = vComponent.$t( 'dataEntry.form.breadcrumb.individual.short' ).toString()
        break
      default:
        className = 'Inc.'
        break
    }
    let objectName: string
    if (!!className) {
      const ident = (object as Individual | UnitParcel | SubBlock | Block | Device).name
      const position = (object instanceof Individual || object instanceof UnitParcel) && !!object.x && !!object.y
          ? `(${object.x}, ${object.y})`
          : ''
      objectName = `${className} ${ident} ${position}`
    } else {
      objectName = ''
    }
    return objectName.trim()
  }
}
