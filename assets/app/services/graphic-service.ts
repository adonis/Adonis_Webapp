import GraphicalStructure, {
    ORIGIN_BOTTOM_LEFT,
    ORIGIN_BOTTOM_RIGHT,
    ORIGIN_TOP_RIGHT,
} from '@adonis/app/models/app-functionalities/graphical-structure'
import Block from '@adonis/app/models/business-objects/block'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import Device from '@adonis/app/models/business-objects/device'
import Individual from '@adonis/app/models/business-objects/individual'
import OutExperimentationZone from '@adonis/app/models/business-objects/OutExperimentationZone'
import SubBlock from '@adonis/app/models/business-objects/sub-block'
import UnitParcel from '@adonis/app/models/business-objects/unit-parcel'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import { CODE_NONE } from '@adonis/app/models/evaluation-variables/state-code'
import GraphicRequestWrapper, { GraphicRequestWrapperInterface } from '@adonis/app/modules/data-entry/graphic/graphic-request-wrapper'
import PlatformService from '@adonis/app/services/platform-service'
import { evaluate } from 'mathjs'

/**
 *
 */
export default class GraphicService {

    static BASE_MOVEMENT_SPACING = 5
    static FONT_SIZE = 20
    static STROKE_COLOR = 'rgb(51,51,51)'
    static CURRENT_COLOR = 'rgb(0,0,0)'

    static GRAPH_BUILD_SIZE = 5
    static MAX_GRAPH_LOOP = 10000

    /** Individual margin level. */
    public static INDIVIDUAL_LEVEL = 1

    /** Parcel margin level. */
    public static PARCEL_LEVEL = 2

    /**
     * Given an object, returns the margin level to draw this item.
     *
     * @param evaluable
     *
     * @return number
     */
    public static getLevelByClass(evaluable: Evaluable): number {
        let level: number
        if (evaluable instanceof Device) {
            level = 5
        }
        if (evaluable instanceof Block) {
            level = 4
        }
        if (evaluable instanceof SubBlock) {
            level = 3
        }
        if (evaluable instanceof UnitParcel) {
            level = this.PARCEL_LEVEL
        }
        if (evaluable instanceof Individual || evaluable instanceof OutExperimentationZone) {
            level = this.INDIVIDUAL_LEVEL
        }
        return level
    }

    /**
     * Remove the graphic polygon information from all branch object.
     *
     * @param item
     */
    static removeGraphicPolygon(item: Evaluable): void {
        const businessBranch = PlatformService.getBranchObjects(item)
        if (!!businessBranch.individual) {
            businessBranch.individual.polygonContour = null
        }
        if (!!businessBranch.parcel) {
            businessBranch.parcel.polygonContour = null
        }
        if (!!businessBranch.subBlock) {
            businessBranch.subBlock.polygonContour = null
        }
        if (!!businessBranch.block) {
            businessBranch.block.polygonContour = null
        }
        if (!!businessBranch.device) {
            businessBranch.device.polygonContour = null
        }
    }

    /**
     * Based on element x, y position, create the points array to represent the object.
     *
     * @param origin
     * @param size
     * @param element
     *
     * @return string[]
     */
    public static getPoints(origin: string, size: number[], element: BusinessObject): string[] {
        let points: string[] = []
        const children = element.directChildren
        if (0 === children.length) {
            if (element instanceof UnitParcel || element instanceof Individual || element instanceof OutExperimentationZone) {
                points.push(GraphicService.adaptPositionWithOrigin(origin, size, [element.x, element.y].join(',')))
            }
        } else {
            element.directChildren
                .forEach(child => {
                    points = [...points, ...GraphicService.getPoints(origin, size, child)]
                })
        }
        return points
    }

    /**
     * Ordering the points array.
     *
     * @param points
     *
     * @return string[]
     */
    public static orderByX(points: string[]): string[] {
        return Array.from(
            points,
            point => point.split(','),
        )
            .sort((firstPoint: string[], secondPoint: string[]) => {
                return Number(firstPoint[0]) - Number(secondPoint[0])
            })
            .map(pointXY => pointXY.join(','))
    }

    /**
     * Ordering the points array.
     *
     * @param points
     *
     * @return string[]
     */
    public static orderByY(points: string[]): string[] {
        return Array.from(
            points,
            point => point.split(','),
        )
            .sort((firstPoint: string[], secondPoint: string[]) => {
                return Number(firstPoint[1]) - Number(secondPoint[1])
            })
            .map(pointXY => pointXY.join(','))
    }

    /**
     * Ordering the points array.
     *
     * @param points
     *
     * @return string[]
     */
    public static orderFromTopLeft(points: string[]): string[] {
        return Array.from(
            points,
            point => point.split(','),
        )
            .sort((firstPoint: string[], secondPoint: string[]) => {
                const xFirst = Number(firstPoint[0])
                const yFirst = Number(firstPoint[1])
                const xSecond = Number(secondPoint[0])
                const ySecond = Number(secondPoint[1])
                if (xFirst !== xSecond) {
                    return xFirst - xSecond
                } else {
                    return yFirst - ySecond
                }
            })
            .map(pointXY => pointXY.join(','))
    }

    /**
     * Get the X, Y position of the neighbour of a point given a direction.
     *
     * @param point
     * @param direction
     *
     * @return string
     */
    public static getNeighbour(point: string, direction: number): string {

        const neighbour = point.split(',')
            .map(v => Number(v))
        switch (direction) {

            case 0:
                --neighbour[1]
                break

            case 1:
                ++neighbour[0]
                break

            case 2:
                ++neighbour[1]
                break

            case 3:
                --neighbour[0]
                break

        }

        return neighbour.join(',')
    }

    /**
     * Change the direction following this loop : 0 -> 1 -> 2 -> 3 -> 0.
     *
     * @param direction
     * @param diff
     *
     * @return number
     */
    public static switchDirection(direction: number, diff: number): number {

        direction = (direction + diff) % 4
        return 0 <= direction
            ? direction
            : direction + 4

    }

    /**
     * Get the color of the polygon representing an element according to its state and level.
     *
     * @param graphicalStructure
     * @param evaluable
     * @param individualPU
     * @param stateColorMap
     * @param natureZheMap
     * @param visibleUris
     *
     * @return string
     */
    public static extractStructureColor(
        graphicalStructure: GraphicalStructure,
        evaluable: Evaluable,
        individualPU: boolean,
        stateColorMap: Map<number, string>,
        natureZheMap: Map<string, string>,
        visibleUris: string[],
    ): string {
        let color = null

        if (!visibleUris || 0 === visibleUris.length || visibleUris.includes(evaluable.uri)) {
            color = graphicalStructure.emptyColor
            if (evaluable instanceof Device) {
                color = graphicalStructure.deviceColor
            } else if (evaluable instanceof Block) {
                color = graphicalStructure.blockColor
            } else if (evaluable instanceof SubBlock) {
                color = graphicalStructure.subBlockColor
            } else if (evaluable instanceof UnitParcel) {
                if (CODE_NONE !== evaluable.stateCode) {
                    color = stateColorMap.get(evaluable.stateCode)
                } else if (individualPU) {
                    color = graphicalStructure.individualUnitParcelColor
                } else {
                    color = graphicalStructure.surfaceUnitParcelColor
                }
            } else if (evaluable instanceof Individual) {
                if (CODE_NONE !== evaluable.stateCode && !!evaluable.stateCode) {
                    color = stateColorMap.get(evaluable.stateCode)
                } else {
                    color = graphicalStructure.individualColor
                }
            } else if (evaluable instanceof OutExperimentationZone && natureZheMap.has(evaluable.natureZHEUri)) {
                color = natureZheMap.get(evaluable.natureZHEUri)
            }
        }

        color = !!color
            ? color
            : '{153,153,153}'

        const colorCode = color.substring(
            color.lastIndexOf('{') + 1,
            color.lastIndexOf('}'),
        )

        return `rgb(${colorCode})`
    }

    /**
     * Remove from polygon string the points that follow a same line to keep only extremum points.
     *
     * @param polygon
     *
     * @return string
     */
    public static optimizeContour(polygon: string): string {
        const currentPoints = polygon.split(' ')
        if (3 > currentPoints.length) {
            return polygon
        }
        const points: string[] = [currentPoints[0]]
        for (let i = 1; i < currentPoints.length - 1; i++) {
            const lastPoint: number[] = currentPoints[i - 1].split(',')
                .map(v => Number(v))
            const point: number[] = currentPoints[i].split(',')
                .map(v => Number(v))
            const nexPoint: number[] = currentPoints[i + 1].split(',')
                .map(v => Number(v))
            if (
                (lastPoint[0] === point[0] && lastPoint[0] === nexPoint[0] &&
                    (
                        lastPoint[1] < point[1] && point[1] < nexPoint[1] ||
                        nexPoint[1] < point[1] && point[1] < lastPoint[1]
                    )
                ) ||
                (lastPoint[1] === point[1] && lastPoint[1] === nexPoint[1] &&
                    (
                        lastPoint[0] < point[0] && point[0] < nexPoint[0] ||
                        nexPoint[0] < point[0] && point[0] < lastPoint[0]
                    )
                )
            ) {
                continue
            }
            points.push(point.join(','))
        }
        points.push(currentPoints[currentPoints.length - 1])
        return points.join(' ')
    }

    /**
     * Returns the X, Y location of the mean center of the polygon.
     *
     * @param polygon
     *
     * @return number[]
     */
    public static getPolygonMeanCenter(polygon: string): number[] {
        let count = 0
        let center = [0, 0]
        polygon.split(' ')
            .forEach((point: string) => {
                if ('' !== point) {
                    ++count
                    const pointXY = point.split(',')
                    center[0] += Number(pointXY[0])
                    center[1] += Number(pointXY[1])
                }
            })
        if (0 < count) {
            center[0] /= count
            center[1] /= count
        } else {
            center = [0, 0]
        }

        return center
    }

    /**
     * Return sub information concerning the given object if exists.
     *
     * @param object
     *
     * @return string
     */
    public static objectSubTitle(object: BusinessObject): string {
        let title: string = null
        const branch = PlatformService.getBranchObjects(object)
        if (!!branch.parcel || !(object instanceof Block) && !!branch.block) {
            title = ''
            if (!(object instanceof Block) && !!branch.block) {
                title += branch.block.labelizedName + ', '
            }
            if (!!branch.parcel) {
                title += 'T: ' + branch.parcel.treatmentName
            }
        }
        return title
    }

    /**
     * Update drawer wrapper context to handle multiple polygon structure.
     *
     * @param drawer
     * @param graphicalStructure
     */
    public static updateDrawer(
        drawer: GraphicRequestWrapperInterface,
        graphicalStructure: GraphicalStructure,
    ): void {
        const mapPoints = new Map<string, string>()
        const points = drawer.pointsInForm.map((pointStr: string) => {
            const pointNum = pointStr.split(',')
                .map((coordinate: string) => {
                    return Number(coordinate)
                })
            const extract = this.extractPointWithMargin(pointNum, graphicalStructure)
            mapPoints.set(`${extract.xCenter},${extract.yCenter}`, pointStr)
            return [extract.xCenter, extract.yCenter]
        })
        const polygon = drawer.polygonContour[drawer.polygonContour.length - 1]
        const polygonEdges: number[][] = polygon.split(' ')
            .map((pointStr: string) => {
                return pointStr.split(',')
                    .map((coordinate: string) => {
                        return Number(coordinate)
                    })
            })

        const filterPoints = points.filter((point: number[]) => {
            return this.isInPolygon(point, polygonEdges)
        })

        drawer.pointsInForm = filterPoints.map((pointNum: number[]) => {
            return mapPoints.get(pointNum.join(','))
        })

        if (0 < filterPoints.length) {
            drawer.initialDirection = 3
            drawer.initialPoint = drawer.pointsInForm[0]
            drawer.polygonContour.push('')
        }
    }

    /**
     * Create a draw wrapper for the given evaluable. If evaluable has children, call back the function for its children.
     *
     * @param businessObject
     * @param individualPU
     * @param graphicalStructure
     * @param maxSize
     * @param stateCodeColorMap
     * @param natureZheColorMap
     * @param restrictedObject
     *
     * @return GraphicRequestWrapperInterface[]
     */
    public static buildDrawerOfEvaluable(
        businessObject: BusinessObject,
        individualPU: boolean,
        graphicalStructure: GraphicalStructure,
        maxSize: number[],
        stateCodeColorMap: Map<number, string>,
        natureZheColorMap: Map<string, string>,
        restrictedObject: string[],
    ): GraphicRequestWrapperInterface[] {

        const points = GraphicService.orderFromTopLeft(
            GraphicService.getPoints(graphicalStructure.getOrigin(), maxSize, businessObject),
        )
        const color = GraphicService.extractStructureColor(
            graphicalStructure,
            businessObject,
            individualPU,
            stateCodeColorMap,
            natureZheColorMap,
            restrictedObject,
        )
        let drawers = [
            new GraphicRequestWrapper(
                GraphicService.getLevelByClass(businessObject),
                points,
                !!businessObject.polygonContour
                    ? businessObject.polygonContour
                    : [''],
                points[0],
                3,
                businessObject,
                individualPU,
                color,
            ),
        ]

        businessObject.directChildren.forEach((object: BusinessObject) => {
            drawers = [
                ...drawers, ...this.buildDrawerOfEvaluable(
                    object,
                    individualPU,
                    graphicalStructure,
                    maxSize,
                    stateCodeColorMap,
                    natureZheColorMap,
                    restrictedObject,
                ),
            ]
        })

        return drawers
    }

    /**
     * Based on the grid position and the rectangle size, get the position of the center in pixel.
     *
     * @param point
     * @param graphicalStructure
     *
     * @return { xCenter: number, yCenter: number }
     */
    public static extractPointWithMargin(
        point: number[],
        graphicalStructure: GraphicalStructure,
    ): { xCenter: number, yCenter: number } {

        const marginCompensation = GraphicService.BASE_MOVEMENT_SPACING * 6 * 2
        const xCenterMovement = graphicalStructure.baseWidth + marginCompensation
        const yCenterMovement = graphicalStructure.baseHeight + marginCompensation
        const xCenter = (point[0] - 1) * xCenterMovement
        const yCenter = (point[1] - 1) * yCenterMovement

        return {xCenter, yCenter}

    }

    /**
     * Recursive algorithm to build the polygon contour by looking to neighbours.
     *
     * @param point
     * @param direction
     * @param drawer
     * @param graphicalStructure
     * @param first
     */
    public static explore(
        point: string,
        direction: number,
        drawer: GraphicRequestWrapper,
        graphicalStructure: GraphicalStructure,
        first = true,
    ): void {

        let neighbour: string
        let repetition = 1
        let moveForward = false

        let polygonContour = drawer.polygonContour[drawer.polygonContour.length - 1]

        do {

            if (!first && drawer.initialDirection === direction && drawer.initialPoint === point) {
                return
            }

            if (1 < repetition) {
                direction = GraphicService.switchDirection(direction, +1)
            }

            neighbour = GraphicService.getNeighbour(point, direction)
            ++repetition

            polygonContour += this.getLeftBottomPointOfRectangleByDirection(
                point.split(',')
                    .map(v => Number(v)),
                direction,
                drawer.level,
                graphicalStructure,
            )
            polygonContour += ' '
            moveForward = drawer.pointsInForm.includes(neighbour)

        } while (!moveForward && 4 >= repetition)

        drawer.polygonContour[drawer.polygonContour.length - 1] = polygonContour

        if (moveForward) {

            direction = GraphicService.switchDirection(direction, -1)
            this.explore(neighbour, direction, drawer, graphicalStructure, false)

        }

    }

    /**
     * Determines whether a point is in a polygon contour or not.
     *
     * @param point
     * @param edges
     *
     * @return boolean
     */
    private static isInPolygon(point: number[], edges: number[][]): boolean {
        // ray-casting algorithm based on
        // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html/pnpoly.html

        const x = point[0]
        const y = point[1]

        let inside = false
        for (let i = 0, j = edges.length - 1; i < edges.length; j = i++) {
            const xi = edges[i][0]
            const yi = edges[i][1]
            const xj = edges[j][0]
            const yj = edges[j][1]

            // @ts-ignore
            const intersect = ((yi > y) !== (yj > y)) && (x < (xj - xi) * (y - yi) / (yj - yi) + xi)
            if (intersect) {
                inside = !inside
            }
        }

        return !inside
    }

    /**
     * Given the textual representation of the origin "top-left", etc... returns the X, Y position of the origin.
     *
     * @param origin
     * @param maxPosition
     *
     * @return number[]
     */
    private static getOriginPosition(origin: string, maxPosition: number[]): number[] {

        let originPos
        switch (origin) {

            case ORIGIN_TOP_RIGHT:
                originPos = [maxPosition[0], 1]
                break

            case ORIGIN_BOTTOM_RIGHT:
                originPos = [maxPosition[0], maxPosition[1]]
                break

            case ORIGIN_BOTTOM_LEFT:
                originPos = [1, maxPosition[1]]
                break

            default:
                originPos = [1, 1]
                break

        }

        return originPos
    }

    /**
     * Given the origin position, will return the new X,Y position of the point after required transformation.
     *
     * @param origin
     * @param size
     * @param point
     *
     * @string
     */
    private static adaptPositionWithOrigin(origin: string, size: number[], point: string): string {

        const pointXYCalculated = GraphicService.getOriginPosition(origin, size)
        const xOperator = origin.includes('right')
            ? '-'
            : '+'
        const yOperator = origin.includes('bottom')
            ? '-'
            : '+'

        const pointXY = point.split(',')

        return [
            evaluate(pointXYCalculated[0] + xOperator + Number(pointXY[0])),
            evaluate(pointXYCalculated[1] + yOperator + Number(pointXY[1])),
        ].join(',')
    }

    /**
     * Given a direction, get the position of the left bottom point of the rectangle using all graphical structure
     * information like baseWidth and baseHeight to get the rectangle size.
     *
     * @param center
     * @param direction
     * @param level
     * @param graphicalStructure
     *
     * @return string
     */
    private static getLeftBottomPointOfRectangleByDirection(
        center: number[],
        direction: number,
        level: number,
        graphicalStructure: GraphicalStructure,
    ): string {

        const {xCenter, yCenter} = this.extractPointWithMargin(center, graphicalStructure)

        const xBaseMovement = graphicalStructure.baseWidth / 2
        const yBaseMovement = graphicalStructure.baseHeight / 2
        const xMovement = xBaseMovement + GraphicService.BASE_MOVEMENT_SPACING * level
        const yMovement = yBaseMovement + GraphicService.BASE_MOVEMENT_SPACING * level
        let leftBottomPoint
        switch (direction) {

            case 0:
                leftBottomPoint = (xCenter - xMovement) + ',' + (yCenter - yMovement)
                break

            case 1:
                leftBottomPoint = (xCenter + xMovement) + ',' + (yCenter - yMovement)
                break

            case 2:
                leftBottomPoint = (xCenter + xMovement) + ',' + (yCenter + yMovement)
                break

            case 3:
                leftBottomPoint = (xCenter - xMovement) + ',' + (yCenter + yMovement)
                break

        }

        return leftBottomPoint
    }

}
