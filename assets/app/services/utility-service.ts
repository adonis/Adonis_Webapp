import {ApiGetAbstractProject} from '@adonis/app/models/business-objects/abstract-project'
import Variable from '@adonis/app/models/evaluation-variables/variable'
import User from '@adonis/shared/models/user'
import {Vue} from 'vue-property-decorator'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum";

/**
 * Utility service to perform common object transformation.
 */
export default class UtilityService {

    /**
     * Format a date to string representation including only the day, month and year.
     *
     * @param date
     *
     * @return string
     */
    public static formatDate(date: Date): string {
        const formatOptions = {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
        }
        const dateTimeFormat = new Intl.DateTimeFormat('en', formatOptions)
        const [{value: month}, , {value: day}, , {value: year}] = dateTimeFormat.formatToParts(date)
        return `${day}/${month}/${year}`
    }

    /**
     * Format a date to string representation including hours and minutes.
     *
     * @param date
     *
     * @return string
     */
    public static formatDateTime(date: Date): string {
        const formatOptions = {
            year: 'numeric',
            month: '2-digit',
            day: '2-digit',
            hour: '2-digit',
            hourCycle: 'h24',
            minute: '2-digit',
        }
        const dateTimeFormat = new Intl.DateTimeFormat('en', formatOptions)
        // tslint:disable-next-line:max-line-length
        const [{value: month}, , {value: day}, , {value: year}, , {value: hours}, , {value: minutes}] = dateTimeFormat.formatToParts(date)
        return `${day}/${month}/${year} ${hours}:${minutes}`
    }

    /**
     * Transform a duration in millisecond into a hours/minutes/second count.
     *
     * @param millisecond
     *
     * @return string
     */
    public static formatDuration(millisecond: number): string {
        const totalSeconds: number = millisecond / 1000
        const hours: number = Math.floor(totalSeconds / 3600)
        const minutes: number = Math.floor((totalSeconds - (hours * 3600)) / 60)
        const seconds: number = Math.floor(totalSeconds - (hours * 3600) - (minutes * 60))
        // Format to add a 0.
        const hoursStr: string = hours < 10
            ? `0${hours}`
            : `${hours}`
        const minutesStr: string = minutes < 10
            ? `0${minutes}`
            : `${minutes}`
        const secondsStr: string = seconds < 10
            ? `0${seconds}`
            : `${seconds}`
        return `${hoursStr}:${minutesStr}:${secondsStr}`
    }

    /**
     * Transform Hex code into Ascii.
     *
     * @param hex
     *
     * @return string
     */
    public static formatHexToAscii(hex: any): string {
        hex = hex.toString() // Force conversion to string
        let str = ''
        for (let i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2) {
            str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
        }
        return str
    }

    /**
     * Get a string representation of the variable type.
     *
     * @param vue
     * @param variable
     *
     * @return string
     */
    public static variableTypeToString(vue: Vue, variable: Variable): string {
        let type: string = vue.$t('dataEntry.form.variableViewer.types.alpha')
            .toString()
        if (!!variable) {
            switch (variable.type) {
                case VariableTypeEnum.REAL:
                    type = vue.$t('dataEntry.form.variableViewer.types.real')
                        .toString()
                    break
                case VariableTypeEnum.INTEGER:
                    type = vue.$t('dataEntry.form.variableViewer.types.int')
                        .toString()
                    break
                case VariableTypeEnum.DATE:
                    type = vue.$t('dataEntry.form.variableViewer.types.date')
                        .toString()
                    break
                case VariableTypeEnum.HOUR:
                    type = vue.$t('dataEntry.form.variableViewer.types.time')
                        .toString()
                    break
                case VariableTypeEnum.ALPHANUMERIC:
                    type = vue.$t('dataEntry.form.variableViewer.types.alpha')
                        .toString()
                    break
                case VariableTypeEnum.BOOLEAN:
                    type = vue.$t('dataEntry.form.variableViewer.types.boolean')
                        .toString()
                    break
                default:
                    break
            }
        }
        return type
    }

    /**
     * Filter projects to get only those owned by the user.
     * Keep only project without owner and project with same owner uri as user.
     *
     * @param projects
     * @param user
     *
     * @return ApiGetAbstractProject[]
     */
    public static filterProjectOfUser(projects: ApiGetAbstractProject[], user: User): ApiGetAbstractProject[] {
        return projects.filter((project: ApiGetAbstractProject) => {
            return user.iri === project.ownerIri
        })
    }

    /**
     * Allow to save a url blob to file.
     *
     * @param url
     * @param fileName
     */
    static saveToFile(url: string, fileName: string) {
        const a: any = document.createElement('a')
        document.body.appendChild(a)
        a.style = 'display: none'
        a.href = url
        a.download = fileName
        a.click()
        window.URL.revokeObjectURL(url)
        a.remove()
    }
}
