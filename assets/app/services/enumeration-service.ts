import {
  ORIGIN_BOTTOM_LEFT,
  ORIGIN_BOTTOM_RIGHT,
  ORIGIN_TOP_LEFT,
  ORIGIN_TOP_RIGHT,
} from '@adonis/app/models/app-functionalities/graphical-structure'
import { BLOCK_CLASS } from '@adonis/app/models/business-objects/block'
import { DEVICE_CLASS } from '@adonis/app/models/business-objects/device'
import { INDIVIDUAL_CLASS } from '@adonis/app/models/business-objects/individual'
import { PLATFORM_CLASS } from '@adonis/app/models/business-objects/platform'
import { PROTO_UNKNOWN } from '@adonis/app/models/business-objects/protocol'
import { SUBBLOCK_CLASS } from '@adonis/app/models/business-objects/sub-block'
import { UNITPARCEL_CLASS } from '@adonis/app/models/business-objects/unit-parcel'
import {VDATE_FORMAT_NUM_ORDER} from '@adonis/app/models/evaluation-variables/variable'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum";
import { OPERATOR_ADDITION, OPERATOR_DIVISION, OPERATOR_MULTIPLICATION, OPERATOR_SUBTRACTION } from '@adonis/app/models/field-tests/test'

/**
 * Class EnumerationService.
 */
export default class EnumerationService {

  /**
   * Temporary converter allowing to translate french types used in the XML document being imported.
   *
   * @param frenchType
   *
   * @return string
   */
  static convertFromFrench( frenchType: string ): string {
    let englishTranslation: string = null
    if (!!frenchType) {
      switch (frenchType) {
        case 'Individu':
        case 'individu':
        case 'individual':
          englishTranslation = INDIVIDUAL_CLASS
          break
        case 'Parcelle Unitaire':
        case 'parcelle':
        case 'unitPlot':
        case 'unit_plot':
          englishTranslation = UNITPARCEL_CLASS
          break
        case 'Sous Bloc':
        case 'sousBloc':
        case 'subBlock':
          englishTranslation = SUBBLOCK_CLASS
          break
        case 'Bloc':
        case 'bloc':
        case 'block':
          englishTranslation = BLOCK_CLASS
          break
        case 'Dispositif':
        case 'dispositif':
        case 'experiment':
          englishTranslation = DEVICE_CLASS
          break
        case 'plateforme':
          englishTranslation = PLATFORM_CLASS
          break
        case 'reel':
          englishTranslation = VariableTypeEnum.REAL;
          break
        case 'alphanumerique':
          englishTranslation = VariableTypeEnum.ALPHANUMERIC;
          break
        case 'entiere':
          englishTranslation = VariableTypeEnum.INTEGER;
          break
        case 'booleen':
          englishTranslation = VariableTypeEnum.BOOLEAN;
          break
        case 'date':
          englishTranslation = VariableTypeEnum.DATE;
          break
        case 'heure':
          englishTranslation = VariableTypeEnum.HOUR;
          break
        case 'quantieme':
          englishTranslation = VDATE_FORMAT_NUM_ORDER
          break
        case 'addition':
          englishTranslation = OPERATOR_ADDITION
          break
        case 'soustraction':
          englishTranslation = OPERATOR_SUBTRACTION
          break
        case 'multiplication':
          englishTranslation = OPERATOR_MULTIPLICATION
          break
        case 'division':
          englishTranslation = OPERATOR_DIVISION
          break
        case 'hautGauche':
          englishTranslation = ORIGIN_TOP_LEFT
          break
        case 'basGauche':
          englishTranslation = ORIGIN_BOTTOM_LEFT
          break
        case 'hautDroite':
          englishTranslation = ORIGIN_TOP_RIGHT
          break
        case 'basDroite':
          englishTranslation = ORIGIN_BOTTOM_RIGHT
          break
        case 'Sans Tirage':
          englishTranslation = PROTO_UNKNOWN
          break
        default:
          englishTranslation = frenchType
          break
      }
    }
    return englishTranslation
  }
}
