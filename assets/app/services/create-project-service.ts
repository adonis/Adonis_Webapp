import DesktopUser from '@adonis/app/models/app-functionalities/desktop-user'
import {ApiGetGraphicalStructure} from '@adonis/app/models/app-functionalities/graphical-structure'
import {BusinessObject} from '@adonis/app/models/business-objects/business-object'
import DataEntryProject, {STATUS_NEW} from '@adonis/app/models/business-objects/data-entry-project'
import Device from '@adonis/app/models/business-objects/device'
import Material from '@adonis/app/models/evaluation-variables/material'
import {ApiGetStateCode, CODE_TYPE_DEAD, CODE_TYPE_MISSING,} from '@adonis/app/models/evaluation-variables/state-code'
import Variable from '@adonis/app/models/evaluation-variables/variable'
import StructureService from '@adonis/app/services/structure-service'
import User from '@adonis/shared/models/user'
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";

/**
 * Service in charge of "improvised" data entry creation.
 */
export default class CreateProjectService {

    /**  */
    private static DEFAULT_GRAPHICAL_STRUCTURE: ApiGetGraphicalStructure = {
        abnormalIndividualColor: 'RGB{255,255,0}',
        abnormalUnitParcelColor: 'RGB{51,204,0}',
        baseHeight: 35,
        baseWidth: 50,
        blockColor: 'RGB{51,153,204}',
        blockLabel: 'blocNum',
        deviceColor: 'RGB{153,255,153}',
        deviceLabel: 'dispoNom',
        emptyColor: 'RGB{255,255,153}',
        individualColor: 'RGB{255,255,0}',
        individualLabel: 'indNum',
        individualUnitParcelColor: 'RGB{51,204,0}',
        individualUnitParcelLabel: 'puTraitCourt',
        origin: 'hautGauche',
        platformColor: 'RGB{255,255,255}',
        subBlockColor: 'RGB{51,204,255}',
        subBlockLabel: 'sousblocNum',
        surfaceUnitParcelColor: 'RGB{51,204,0}',
        surfaceUnitParcelLabel: 'puNum',
        tooltipActive: true,
    }

    /**  */
    private static DEFAULT_STATE_CODE_LIST: ApiGetStateCode[] = [
        {
            code: -6,
            color: 'RGB{255,255,0}',
            label: 'Donnée manquante',
            type: CODE_TYPE_MISSING,
        },
        {
            code: -9,
            color: 'RGB{255,0,0}',
            label: 'Mort',
            type: CODE_TYPE_DEAD,
            propagation: PathLevelEnum.INDIVIDUAL,
        },
    ]

    /**
     * Given some information, create a new data entry project.
     *
     * @param user
     * @param projectName
     * @param selectedDevices
     * @param deadCode
     * @param project
     *
     * @return DataEntryProject
     */
    public static createProjectFromDevice(
        project: DataEntryProject,
        selectedDevices: Device[],
        projectName: string,
        user: User,
        deadCode: number,
    ): DataEntryProject {
        const dateNow = new Date()
        const platform = StructureService.buildPlatform({
            id: 1,
            creationDate: dateNow,
            devices: selectedDevices,
            name: project.platform.name,
            site: project.platform.site,
            place: project.platform.place,
        })
        platform.devices.forEach((device: Device) => {
            device.protocol.creatorLogin = user.username
            this.deletePolygonContour(device)
        })
        const userDesktop = new DesktopUser(
            user.id,
            user.username,
            '',
            user.username,
            '',
            user.email,
        )
        return new DataEntryProject(
            -((new Date()).getTime()),
            project.projectId,
            user.iri,
            projectName,
            platform,
            user.username,
            [
                userDesktop,
            ],
            [...project.workpaths],
            [],
            [],
            [],
            [],
            [],
            null,
            project.stateCodes.map((stateCodesProps: ApiGetStateCode) => {
                if (CODE_TYPE_DEAD === stateCodesProps.type) {
                    stateCodesProps.code = deadCode
                }
                return StructureService.buildStateCode(stateCodesProps)
            }),
            [],
            [],
            STATUS_NEW,
            dateNow,
            [],
            new Map<string, Variable>(),
            new Map<string, Material>(),
            new Map<string, any>(),
            project.naturesZHE,
            StructureService.buildGraphicalStructure(this.DEFAULT_GRAPHICAL_STRUCTURE),
            true,
        )
    }

    /**
     * Delete all polygon contour of an item and its children.
     *
     * @param item
     */
    private static deletePolygonContour(item: BusinessObject): void {
        item.directChildren.forEach((subItem: BusinessObject) => {
            this.deletePolygonContour(subItem)
        })
        item.polygonContour = null
    }
}
