import HttpService from '@adonis/shared/services/http-service'
import NotificationServiceInterface from '@adonis/shared/services/notification-service-interface'
import { Store } from 'vuex'

export default class AppHttpService extends HttpService {

    constructor(
        store: Store<any>,
        notifier: NotificationServiceInterface,
    ) {
        super(store, notifier)
    }
}
