import DesktopUser, { ApiGetDesktopUser } from '@adonis/app/models/app-functionalities/desktop-user'
import GraphicalStructure, {
    ApiGetGraphicalStructure,
    ORIGIN_BOTTOM_RIGHT,
} from '@adonis/app/models/app-functionalities/graphical-structure'
import { ApiGetAbstractProject } from '@adonis/app/models/business-objects/abstract-project'
import Anomaly, { ApiGetAnomaly } from '@adonis/app/models/business-objects/anomaly'
import Block, { ApiGetBlock } from '@adonis/app/models/business-objects/block'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import DataEntryProject, { ApiGetDataEntryProject, STATUS_NEW } from '@adonis/app/models/business-objects/data-entry-project'
import Device, { ApiGetDevice } from '@adonis/app/models/business-objects/device'
import Factor, { ApiGetFactor } from '@adonis/app/models/business-objects/factor'
import Individual, { ApiGetIndividual } from '@adonis/app/models/business-objects/individual'
import Modality, { ApiGetModality } from '@adonis/app/models/business-objects/modality'
import NatureZHE, { ApiGetNatureZHE } from '@adonis/app/models/business-objects/NatureZHE'
import OutExperimentationZone, { ApiGetOutExperimentationZone } from '@adonis/app/models/business-objects/OutExperimentationZone'
import Platform, { ApiGetPlatform } from '@adonis/app/models/business-objects/platform'
import Protocol, { ApiGetProtocol } from '@adonis/app/models/business-objects/protocol'
import SubBlock, { ApiGetSubBlock } from '@adonis/app/models/business-objects/sub-block'
import Treatment, { ApiGetTreatment } from '@adonis/app/models/business-objects/treatment'
import UnitParcel, { ApiGetUnitParcel } from '@adonis/app/models/business-objects/unit-parcel'
import Workpath, { ApiGetWorkpath } from '@adonis/app/models/business-objects/workpath'
import Note, { NoteInterface } from '@adonis/app/models/entry-notes/note'
import Annotation, { ApiGetAnnotation } from '@adonis/app/models/evaluation-variables/annotation'
import DataEntry, { ApiGetDataEntry } from '@adonis/app/models/evaluation-variables/data-entry'
import Driver, { ApiGetDriver } from '@adonis/app/models/evaluation-variables/driver'
import FormField, { ApiGetFormField } from '@adonis/app/models/evaluation-variables/form-field'
import GeneratedField, { ApiGetGeneratedField } from '@adonis/app/models/evaluation-variables/generated-form-fields'
import GeneratorVariable, { ApiGetGeneratorVariable } from '@adonis/app/models/evaluation-variables/generator-variable'
import Mark, { ApiGetMark } from '@adonis/app/models/evaluation-variables/mark'
import MarkList, { ApiGetMarkList } from '@adonis/app/models/evaluation-variables/mark-list'
import Material, { ApiGetMaterial } from '@adonis/app/models/evaluation-variables/material'
import Measure, { ApiGetMeasure } from '@adonis/app/models/evaluation-variables/measure'
import PreviousValue, { ApiGetPreviousValue } from '@adonis/app/models/evaluation-variables/previous-value'
import RequiredAnnotation, { ApiGetRequiredAnnotation } from '@adonis/app/models/evaluation-variables/required-annotation'
import Session, { ApiGetSession } from '@adonis/app/models/evaluation-variables/session'
import StateCode, { ApiGetStateCode, STATE_NONE } from '@adonis/app/models/evaluation-variables/state-code'
import UniqueVariable, { ApiGetUniqueVariable } from '@adonis/app/models/evaluation-variables/unique-variable'
import ValueHint, { ApiGetValueHint } from '@adonis/app/models/evaluation-variables/value-hint'
import ValueHintList, { ApiGetValueHintList } from '@adonis/app/models/evaluation-variables/value-hint-list'
import Variable, { ApiGetVariable } from '@adonis/app/models/evaluation-variables/variable'
import CombinationTest, { ApiGetCombinationTest } from '@adonis/app/models/field-tests/combination-test'
import GrowthTest, { ApiGetGrowthTest } from '@adonis/app/models/field-tests/growth-test'
import PreconditionedCalculation, { ApiGetPreconditionedCalculation } from '@adonis/app/models/field-tests/preconditioned-calculation'
import RangeTest, { ApiGetRangeTest } from '@adonis/app/models/field-tests/range-test'
import Test from '@adonis/app/models/field-tests/test'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'
import { SimpleVariableDto } from '@adonis/shared/models/dto/simple-variable-dto'
import { VariableScaleDto, VariableScaleValueDto } from "@adonis/shared/models/dto/variable-scale-dto"

/**
 * Class CommunicationService.
 */
export default class StructureService {

    /**
     * Build an instance of DataEntryProject given a json object implementing interface ApiGetDataEntryProject.
     * Will rebuild every links and relations between all entities within the project.
     * Enable to use assertion instanceof on all business objects.
     *
     * @param apiDataEntryProject
     * @param projectId
     *
     * @return DataEntryProject
     */
    public static buildDataEntryProject(
        apiDataEntryProject: ApiGetDataEntryProject,
        projectId: number = null,
    ): DataEntryProject {
        const apiDateEntryProjectStatusId: number | string = !!projectId
            ? projectId
            : apiDataEntryProject.projectId
        const status: string = apiDataEntryProject.status
            ? apiDataEntryProject.status
            : STATUS_NEW
        // Handles variables. Will complete the variable map.
        const variablesMap: Map<string, Variable> = new Map()
        const vars = this.extractVariables(apiDataEntryProject, variablesMap)
        const connVars = this.extractConnectedVariables(apiDataEntryProject, variablesMap)

        // To build dataEntry, we need the variables map to be completed.
        const dataEntry = this.buildDataEntry(variablesMap, apiDataEntryProject.dataEntry)

        let requiredAnnotations: RequiredAnnotation[] = []
        if (!!apiDataEntryProject.requiredAnnotations) {
            requiredAnnotations = apiDataEntryProject.requiredAnnotations.map(
                (apiGetRequiredAnnotation: ApiGetRequiredAnnotation) => {
                    return this.buildRequiredAnnotation(apiGetRequiredAnnotation)
                },
            )
        }

        const {materialsMap, connectionsMap, materials, categories, keywords} = this.extractMapsAndReferences(
            apiDataEntryProject,
            vars,
        )

        // Build project.
        return new DataEntryProject(
            apiDataEntryProject.id,
            apiDateEntryProjectStatusId,
            apiDataEntryProject.ownerIri,
            apiDataEntryProject.name,
            this.buildPlatform(apiDataEntryProject.platform),
            apiDataEntryProject.creatorLogin,
            apiDataEntryProject.desktopUsers.map((workpathProps: ApiGetDesktopUser) => {
                return this.buildDesktopUser(workpathProps)
            }),
            apiDataEntryProject.workpaths
                .filter(workpath => !!workpath)
                .map((workpathProps: ApiGetWorkpath) => {
                    return this.buildWorkpath(workpathProps)
                }),
            vars.uniqueVariables,
            vars.generatorVariables,
            connVars.uniqueVariables,
            connVars.generatorVariables,
            materials,
            dataEntry,
            apiDataEntryProject.stateCodes.map((stateCodesProps: ApiGetStateCode) => {
                return this.buildStateCode(stateCodesProps)
            }),
            categories,
            keywords,
            status,
            apiDataEntryProject.creationDate,
            requiredAnnotations,
            variablesMap,
            materialsMap,
            connectionsMap,
            !apiDataEntryProject.naturesZHE
                ? []
                : apiDataEntryProject.naturesZHE.map((natureZHE: ApiGetNatureZHE) => {
                    return this.buildNatureZHE(natureZHE)
                }),
            this.buildGraphicalStructure(apiDataEntryProject.graphicalStructure),
            !!apiDataEntryProject.improvised,
        )
    }

    /**
     * Given an object/structure that implements ApiGetVariable, will return an instance of class UniqueVariable or
     * GeneratorVariable.
     *
     * @param variableProps
     */
    public static buildVariable(variableProps: ApiGetVariable) {
        let variable: Variable
        if (!!(variableProps as ApiGetGeneratorVariable).generatedPrefix) {
            variable = this.buildGeneratorVariable(variableProps as ApiGetGeneratorVariable)
        } else {
            variable = this.buildUniqueVariable(variableProps as ApiGetUniqueVariable)
        }
        return variable
    }

    /**
     * Get a Platform instance from its interface.
     *
     * @param platformProps
     *
     * @return Platform
     */
    public static buildPlatform(platformProps: ApiGetPlatform): Platform {
        const uriMap = new Map()
        const positionMap = new Map()
        const identMap = new Map()
        const zheMap = new Map()
        const platform = new Platform(
            platformProps.id,
            platformProps.name,
            [],
            uriMap,
            positionMap,
            identMap,
            zheMap,
            platformProps.creationDate,
            platformProps.site,
            platformProps.place,
        )
        platform.devices = platformProps.devices
            .map((deviceProps: ApiGetDevice) => {
                return this.buildDevice(deviceProps, platform)
            })
        return platform
    }

    /**
     * Get a GraphicalStructure instance from its interface.
     *
     * @param apiGraphicalStructure
     *
     * @return GraphicalStructure
     */
    public static buildGraphicalStructure(apiGraphicalStructure: ApiGetGraphicalStructure): GraphicalStructure {
        let graphicalStructure: GraphicalStructure = null
        if (!!apiGraphicalStructure) {
            graphicalStructure = new GraphicalStructure(
                apiGraphicalStructure.platformColor,
                apiGraphicalStructure.deviceColor,
                apiGraphicalStructure.blockColor,
                apiGraphicalStructure.subBlockColor,
                apiGraphicalStructure.individualUnitParcelColor,
                apiGraphicalStructure.surfaceUnitParcelColor,
                apiGraphicalStructure.individualColor,
                apiGraphicalStructure.abnormalIndividualColor,
                apiGraphicalStructure.emptyColor,
                !!apiGraphicalStructure.origin
                    ? apiGraphicalStructure.origin
                    : ORIGIN_BOTTOM_RIGHT,
                !!apiGraphicalStructure.baseHeight
                    ? apiGraphicalStructure.baseHeight
                    : 50,
                !!apiGraphicalStructure.baseWidth
                    ? apiGraphicalStructure.baseWidth
                    : 50,
                apiGraphicalStructure.abnormalUnitParcelColor,
                apiGraphicalStructure.tooltipActive,
                apiGraphicalStructure.deviceLabel,
                apiGraphicalStructure.blockLabel,
                apiGraphicalStructure.subBlockLabel,
                apiGraphicalStructure.individualUnitParcelLabel,
                apiGraphicalStructure.surfaceUnitParcelLabel,
                apiGraphicalStructure.individualLabel,
            )
        }
        return graphicalStructure
    }

    /**
     * Get a StateCode instance from its interface.
     *
     * @param stateCodesProps
     *
     * @return StateCode
     */
    public static buildStateCode(stateCodesProps: ApiGetStateCode): StateCode {
        if (!stateCodesProps) {
            return STATE_NONE
        }
        return new StateCode(
            stateCodesProps.code,
            stateCodesProps.label,
            stateCodesProps.type,
            stateCodesProps.description,
            stateCodesProps.propagation,
            stateCodesProps.color,
        )
    }

    /**
     * Extract all maps and references used in given project.
     *
     * @param apiProject
     * @param vars
     *
     * { materialsMap, connectionsMap, materials, categories, keywords }
     */
    private static extractMapsAndReferences(
        apiProject: ApiGetAbstractProject,
        vars: { variablesMap: Map<string, Variable>; uniqueVariables: UniqueVariable[]; generatorVariables: GeneratorVariable[] },
    ) {
        const materialsMap = new Map<string, Material>()
        const connectionsMap = new Map<string, any>()
        const materials = !!apiProject.materials
            ? apiProject.materials.map((apiGetMaterial: ApiGetMaterial) => {
                const material = this.buildMaterial(vars.uniqueVariables, apiGetMaterial)
                materialsMap.set(apiGetMaterial.uid, material)
                return material
            })
            : []

        const categories = !!apiProject.categories
            ? apiProject.categories
            : []

        const keywords = !!apiProject.keywords
            ? apiProject.keywords
            : []

        return {materialsMap, connectionsMap, materials, categories, keywords}
    }

    /**
     * Extract all base variables using extractVariableCommon function.
     *
     * @param apiAbstractProject
     * @param variablesMap
     *
     * @return { variablesMap, uniqueVariables, generatorVariables }
     */
    private static extractVariables(
        apiAbstractProject: ApiGetAbstractProject,
        variablesMap: Map<string, Variable>,
    ) {
        return this.extractVariableCommon(
            apiAbstractProject.uniqueVariables,
            apiAbstractProject.generatorVariables, variablesMap,
        )
    }

    /**
     * Extract all connected variables using extractVariableCommon function.
     *
     * @param apiDataEntryProject
     * @param variablesMap
     *
     * @return { variablesMap, uniqueVariables, generatorVariables }
     */
    private static extractConnectedVariables(
        apiDataEntryProject: ApiGetDataEntryProject,
        variablesMap: Map<string, Variable>,
    ) {
        return this.extractVariableCommon(
            apiDataEntryProject.connectedUniqueVariables,
            apiDataEntryProject.connectedGeneratorVariables,
            variablesMap,
        )
    }

    /**
     * Create a Variable instance given and interface for each unique and generator variable given in params.
     *
     * @param uniqueVariablesProps
     * @param generatorVariablesProps
     * @param variablesMap
     *
     * @return { variablesMap, uniqueVariables, generatorVariables }
     */
    private static extractVariableCommon(
        uniqueVariablesProps: ApiGetUniqueVariable[],
        generatorVariablesProps: ApiGetGeneratorVariable[],
        variablesMap: Map<string, Variable>,
    ) {
        const uniqueVariables = !!uniqueVariablesProps
            ? uniqueVariablesProps.map((uniqueVariableProps: ApiGetUniqueVariable) => {
                return this.buildUniqueVariable(uniqueVariableProps, variablesMap)
            })
            : []
        const generatorVariables = !!generatorVariablesProps
            ? generatorVariablesProps
                .map((generatorVariableProps: ApiGetGeneratorVariable) => {
                    return this.buildGeneratorVariable(generatorVariableProps, variablesMap)
                })
            : []
        return {variablesMap, uniqueVariables, generatorVariables}
    }

    /**
     * Create a DataEntry instance given and interface.
     *
     * @param variablesMap
     * @param dataEntryProps
     *
     * @return DataEntry
     */
    private static buildDataEntry(
        variablesMap: Map<string, Variable>,
        dataEntryProps: ApiGetDataEntry,
    ): DataEntry {
        let dataEntry: DataEntry = null
        if (!!dataEntryProps) {
            dataEntry = new DataEntry(
                dataEntryProps.status,
                dataEntryProps.startedAt,
                dataEntryProps.endedAt,
                dataEntryProps.currentPosition,
                dataEntryProps.lastPosition,
                dataEntryProps.workpath,
                dataEntryProps.currentWorkpath,
                dataEntryProps.restrictedObject,
                dataEntryProps.sessionIDs,
                dataEntryProps.askIdent,
                dataEntryProps.doPrint,
                dataEntryProps.lastFieldDoMove,
                dataEntryProps.disableNumeralKeyboard,
                dataEntryProps.user,
                !dataEntryProps.visitedBusinessObjectsURI
                    ? []
                    : dataEntryProps.visitedBusinessObjectsURI,
                dataEntryProps.speechRecognition,
                dataEntryProps.currentSessionId,
            )
        }
        return dataEntry
    }

    /**
     * Create a Session instance given and interface.
     *
     * @param variablesMap
     * @param sessionProps
     *
     * @return Session
     */
    public static buildSession(
        variablesMap: Map<string, Variable>,
        sessionProps: ApiGetSession,
    ): Session {
        return new Session(
            sessionProps.id,
            sessionProps.startedAt,
            sessionProps.endedAt,
            sessionProps.formFields.map((formFieldProps: ApiGetFormField) => {
                return this.buildFormField(variablesMap, formFieldProps)
            }),
            sessionProps.projectId,
        )
    }

    /**
     * Get a FormField instance given its interface.
     *
     * @param variablesMap
     * @param formFieldProps
     *
     * @return FormField
     */
    static buildFormField(
        variablesMap: Map<string, Variable>,
        formFieldProps: ApiGetFormField,
    ): FormField {
        const generatedFields = !!formFieldProps.generatedFields
            ? formFieldProps.generatedFields
                .map((generatedFieldProps: ApiGetGeneratedField) => {
                    return this.buildGeneratedField(variablesMap, generatedFieldProps)
                })
            : null
        return new FormField(
            formFieldProps.targetUri,
            formFieldProps.variableUri,
            formFieldProps.measures.map((measureProps: ApiGetMeasure) => {
                return this.buildMeasure(measureProps)
            }),
            generatedFields,
        )
    }

    /**
     * Get a GeneratedField instance given its interface.
     *
     * @param variablesMap
     * @param generatedFieldProps
     *
     * @return GeneratedField
     */
    private static buildGeneratedField(
        variablesMap: Map<string, Variable>,
        generatedFieldProps: ApiGetGeneratedField,
    ): GeneratedField {
        return new GeneratedField(
            generatedFieldProps.index,
            generatedFieldProps.prefix,
            generatedFieldProps.numeralIncrement,
            generatedFieldProps.formFields.map((formFieldProps: ApiGetFormField) => {
                return this.buildFormField(variablesMap, formFieldProps)
            }),
        )
    }

    /**
     * Get a Measure instance from its interface.
     *
     * @param measureProps
     *
     * @return Measure
     */
    private static buildMeasure(measureProps: ApiGetMeasure): Measure {
        return new Measure(
            measureProps.value,
            measureProps.targetUri,
            measureProps.variableUid,
            measureProps.index,
            measureProps.repetition,
            measureProps.timestamp,
            this.buildStateCode(measureProps.state),
            this.buildAnnotation(measureProps.annotations),
        )
    }

    /**
     * Get a Device instance from its interface.
     *
     * @param deviceProps
     * @param platform
     *
     * @return Device
     */
    private static buildDevice(deviceProps: ApiGetDevice, platform: Platform = null): Device {
        const device = new Device(
            deviceProps.uri,
            deviceProps.name,
            deviceProps.individualPU,
            [],
            this.buildAnnotation(deviceProps.annotations),
            this.buildProtocol(deviceProps.protocol),
            deviceProps.creationDate,
            deviceProps.validationDate,
            [],
            deviceProps.polygonContour,
            deviceProps.center,
            [],
            !!deviceProps.anomaly
                ? this.buildAnomaly(deviceProps.anomaly)
                : null,
        )
        if (!!deviceProps.notes) {
            device.notes = deviceProps.notes
                .map((noteProps: NoteInterface) => {
                    return this.buildNotes(noteProps)
                })
        }
        device.blocks = deviceProps.blocks
            .map((blockProps: ApiGetBlock) => {
                return this.buildBlock(device, blockProps, platform)
            })
        if (!!deviceProps.outExperimentationZones) {
            device.outExperimentationZones = deviceProps.outExperimentationZones
                .map((outExperimentationZone: ApiGetOutExperimentationZone) => {
                    return this.buildOutExperimentationZone(device, outExperimentationZone, platform)
                })
        }
        if (!!platform) {
            platform.uriMap.set(device.uri, device)
        }
        return device
    }

    /**
     * Get a Block instance given its interface.
     *
     * @param parent
     * @param blockProps
     * @param platform
     *
     * @return Block
     */
    private static buildBlock(parent: Device, blockProps: ApiGetBlock, platform: Platform = null): Block {
        const block = new Block(
            parent,
            blockProps.uri,
            blockProps.name,
            [],
            [],
            this.buildAnnotation(blockProps.annotations),
            [],
            blockProps.polygonContour,
            blockProps.center,
            [],
            !!blockProps.anomaly
                ? this.buildAnomaly(blockProps.anomaly)
                : null,
        )
        if (!!blockProps.notes) {
            block.notes = blockProps.notes
                .map((noteProps: NoteInterface) => {
                    return this.buildNotes(noteProps)
                })
        }
        block.subBlocks = blockProps.subBlocks
            .map((subBlockProps: ApiGetSubBlock) => {
                return this.buildSubBlock(parent, block, subBlockProps, platform)
            })
        return this.buildCommonBlock(block, blockProps, parent, platform) as Block
    }

    /**
     * Get a SubBlock instance given its interface.
     *
     * @param device
     * @param parent
     * @param subBlockProps
     * @param platform
     *
     * @return SubBlock
     */
    private static buildSubBlock(
        device: Device,
        parent: Block,
        subBlockProps: ApiGetSubBlock,
        platform: Platform = null,
    ): SubBlock {
        const subBlock = new SubBlock(
            parent,
            subBlockProps.uri,
            subBlockProps.name,
            [],
            this.buildAnnotation(subBlockProps.annotations),
            [],
            subBlockProps.polygonContour,
            subBlockProps.center,
            [],
            !!subBlockProps.anomaly
                ? this.buildAnomaly(subBlockProps.anomaly)
                : null,
        )
        if (!!subBlockProps.notes) {
            subBlock.notes = subBlockProps.notes
                .map((noteProps: NoteInterface) => {
                    return this.buildNotes(noteProps)
                })
        }
        return this.buildCommonBlock(subBlock, subBlockProps, device, platform) as SubBlock
    }

    /**
     * Build common information of SubBlock an Block.
     *
     * @param block
     * @param apiBlock
     * @param device
     * @param platform
     *
     * @return SubBlock | Block
     */
    private static buildCommonBlock(
        block: SubBlock | Block,
        apiBlock: ApiGetSubBlock | ApiGetBlock,
        device: Device,
        platform: Platform = null,
    ): SubBlock | Block {
        block.unitParcels = apiBlock.unitParcels
            .map((parcelProps: ApiGetUnitParcel) => {
                return this.buildUnitParcel(device, block, parcelProps, platform)
            })
        if (!!apiBlock.outExperimentationZones) {
            block.outExperimentationZones = apiBlock.outExperimentationZones
                .map((outExperimentationZone: ApiGetOutExperimentationZone) => {
                    return this.buildOutExperimentationZone(block, outExperimentationZone, platform)
                })
        }
        if (!!platform) {
            platform.uriMap.set(block.uri, block)
        }
        return block
    }

    /**
     * Get an instance of UnitParcel given its interface value.
     *
     * @param device
     * @param parent
     * @param parcelProps
     * @param platform
     *
     * @return UnitParcel
     */
    private static buildUnitParcel(
        device: Device,
        parent: Block | SubBlock,
        parcelProps: ApiGetUnitParcel,
        platform: Platform = null,
    ): UnitParcel {
        const stateCode = !!parcelProps.stateCode
            ? parcelProps.stateCode
            : 0
        const parcel = new UnitParcel(
            parent,
            parcelProps.uri,
            parcelProps.name,
            parcelProps.x,
            parcelProps.y,
            [],
            this.buildAnnotation(parcelProps.annotations),
            parcelProps.treatmentName,
            stateCode,
            parcelProps.aparitionDate,
            parcelProps.demiseDate,
            parcelProps.ident,
            [],
            parcelProps.polygonContour,
            parcelProps.center,
            [],
            !!parcelProps.anomaly
                ? this.buildAnomaly(parcelProps.anomaly)
                : null,
        )
        if (!!parcelProps.notes) {
            parcel.notes = parcelProps.notes
                .map((noteProps: NoteInterface) => {
                    return this.buildNotes(noteProps)
                })
        }
        parcel.individuals = parcelProps.individuals
            .map((individualProps: ApiGetIndividual) => {
                return this.buildIndividual(parcel, individualProps, platform)
            })
        if (!!parcelProps.outExperimentationZones) {
            parcel.outExperimentationZones = parcelProps.outExperimentationZones
                .map((outExperimentationZone: ApiGetOutExperimentationZone) => {
                    return this.buildOutExperimentationZone(parcel, outExperimentationZone, platform)
                })
        }
        if (!!platform) {
            platform.uriMap.set(parcel.uri, parcel)
            if (!!parcel.ident) {
                platform.identMap.set(parcel.ident, parcel)
            }
            if (!device.individualPU) {
                platform.positionMap.set(`${parcel.x},${parcel.y}`, parcel)
            }
        }
        return parcel
    }

    /**
     * Get an Individual instance from its interface value.
     *
     * @param parent
     * @param individualProps
     * @param platform
     * @private
     */
    private static buildIndividual(
        parent: UnitParcel,
        individualProps: ApiGetIndividual,
        platform: Platform = null,
    ): Individual {
        const stateCode = !!individualProps.stateCode
            ? individualProps.stateCode
            : 0
        const individual = new Individual(
            parent,
            individualProps.uri,
            individualProps.name,
            individualProps.x,
            individualProps.y,
            this.buildAnnotation(individualProps.annotations),
            stateCode,
            individualProps.aparitionDate,
            individualProps.demiseDate,
            individualProps.ident,
            [],
            individualProps.polygonContour,
            individualProps.center,
            !!individualProps.anomaly
                ? this.buildAnomaly(individualProps.anomaly)
                : null,
        )
        if (!!individualProps.notes) {
            individual.notes = individualProps.notes
                .map((noteProps: NoteInterface) => {
                    return this.buildNotes(noteProps)
                })
        }
        if (!!platform) {
            platform.uriMap.set(individual.uri, individual)
            platform.positionMap.set(`${individual.x},${individual.y}`, individual)
            if (!!individual.ident) {
                platform.identMap.set(individual.ident, individual)
            }
        }
        return individual
    }

    /**
     * Get all instances of annotation from ApiGetAnnotation interface array.
     *
     * @param apiGetAnnotations
     *
     * @return Annotation[]
     */
    private static buildAnnotation(apiGetAnnotations: ApiGetAnnotation[]): Annotation[] {
        let annotations: Annotation[] = []
        if (!!apiGetAnnotations) {
            annotations = apiGetAnnotations.map((apiGetAnnotation: ApiGetAnnotation) => {
                return new Annotation(
                    apiGetAnnotation.name,
                    apiGetAnnotation.type,
                    apiGetAnnotation.value,
                    apiGetAnnotation.image,
                    new Date(),
                    apiGetAnnotation.categories,
                    apiGetAnnotation.keywords,
                )
            })
        }
        return annotations
    }

    /**
     * Get instance of DesktopUser from its interface value.
     *
     * @param desktopUserProps
     *
     * @return DesktopUser
     */
    private static buildDesktopUser(desktopUserProps: ApiGetDesktopUser): DesktopUser {
        return new DesktopUser(
            desktopUserProps.id,
            desktopUserProps.name,
            desktopUserProps.firstname,
            desktopUserProps.login,
            desktopUserProps.password,
            desktopUserProps.email,
        )
    }

    /**
     * Get an instance of Workpath from its interface value.
     *
     * @param workpathProps
     *
     * @return Workpath
     */
    private static buildWorkpath(workpathProps: ApiGetWorkpath): Workpath {
        return new Workpath(
            workpathProps.id,
            workpathProps.username,
            workpathProps.path,
            workpathProps.startEnd,
        )
    }

    /**
     * Get an UniqueVariable instance from its interface value.
     *
     * @param uniqueVariableProps
     * @param variablesMap
     *
     * @return UniqueVariable
     */
    public static buildUniqueVariable(
        uniqueVariableProps: ApiGetUniqueVariable,
        variablesMap: Map<string, Variable> = null,
    ): UniqueVariable {
        const uniqueVariable = new UniqueVariable(
            uniqueVariableProps.name,
            uniqueVariableProps.shortName,
            Number(uniqueVariableProps.repetitions),
            uniqueVariableProps.unit,
            uniqueVariableProps.askTimestamp,
            uniqueVariableProps.pathLevel,
            uniqueVariableProps.comment,
            uniqueVariableProps.order,
            uniqueVariableProps.active,
            uniqueVariableProps.format,
            uniqueVariableProps.type,
            uniqueVariableProps.mandatory,
            this.buildDefaultValue(uniqueVariableProps.defaultValue, uniqueVariableProps.type),
            this.buildGrowthTests(uniqueVariableProps.growthTests),
            this.buildRangeTests(uniqueVariableProps.rangeTests),
            this.buildCombinationTests(uniqueVariableProps.combinationTests),
            this.buildPreconditionedCalculations(uniqueVariableProps.preconditionedCalculations),
            this.buildScale(uniqueVariableProps.scale),
            this.buildPreviousValues(uniqueVariableProps.previousValues),
            uniqueVariableProps.creationDate,
            uniqueVariableProps?.modificationDate ?? uniqueVariableProps.creationDate,
            uniqueVariableProps.uri,
            null,
            this.buildValueHintList(uniqueVariableProps.valueHintList),
            uniqueVariableProps.frameStartPosition,
            uniqueVariableProps.frameEndPosition,
            uniqueVariableProps.materialUid,
        )
        if (!!variablesMap) {
            variablesMap.set(uniqueVariable.uri, uniqueVariable)
        }
        return uniqueVariable
    }

    /**
     * Get an GeneratorVariable interface from its interface value.
     *
     * @param generatorVariableProps
     * @param variablesMap
     *
     * @return GeneratorVariable
     */
    private static buildGeneratorVariable(
        generatorVariableProps: ApiGetGeneratorVariable,
        variablesMap: Map<string, Variable> = null,
    ): GeneratorVariable {
        const generatorVariable = new GeneratorVariable(
            generatorVariableProps.name,
            generatorVariableProps.shortName,
            generatorVariableProps.repetitions,
            generatorVariableProps.unit,
            generatorVariableProps.askTimestamp,
            generatorVariableProps.pathLevel,
            generatorVariableProps.comment,
            generatorVariableProps.order,
            generatorVariableProps.active,
            generatorVariableProps.format,
            generatorVariableProps.type,
            generatorVariableProps.mandatory,
            this.buildDefaultValue(generatorVariableProps.defaultValue, generatorVariableProps.type),
            this.buildGrowthTests(generatorVariableProps.growthTests),
            this.buildRangeTests(generatorVariableProps.rangeTests),
            this.buildCombinationTests(generatorVariableProps.combinationTests),
            this.buildPreconditionedCalculations(generatorVariableProps.preconditionedCalculations),
            this.buildScale(generatorVariableProps.scale),
            this.buildPreviousValues(generatorVariableProps.previousValues),
            generatorVariableProps.creationDate,
            generatorVariableProps.modificationDate,
            generatorVariableProps.uri,
            null,
            generatorVariableProps.generatedPrefix,
            generatorVariableProps.numeralIncrement,
            null,
            null,
            generatorVariableProps.frameStartPosition,
            generatorVariableProps.frameEndPosition,
            generatorVariableProps.materialUid,
        )
        generatorVariable.generatorVariables = generatorVariableProps.generatorVariables
            .map((variableProps: ApiGetGeneratorVariable) => {
                const generatorVariable1 = this.buildGeneratorVariable(variableProps, variablesMap)
                generatorVariable1.parent = generatorVariable
                return generatorVariable1
            })
        generatorVariable.uniqueVariables = generatorVariableProps.uniqueVariables.map((uniqueVariableProps: ApiGetUniqueVariable) => {
            const uniqueVariable = this.buildUniqueVariable(uniqueVariableProps, variablesMap)
            uniqueVariable.parent = generatorVariable
            return uniqueVariable
        })
        if (!!variablesMap) {
            variablesMap.set(generatorVariable.uri, generatorVariable)
        }
        return generatorVariable
    }

    /**
     * get an instance of GrowthTest from each interface value.
     *
     * @param tests
     *
     * @return GrowthTest[]
     */
    private static buildGrowthTests(tests: ApiGetGrowthTest[]): GrowthTest[] {
        if (!!tests) {
            return tests
                .map((testProps: ApiGetGrowthTest) => {
                    return new GrowthTest(
                        testProps.name,
                        testProps.active,
                        testProps.comparisonVariableUid,
                        testProps.minDiff,
                        testProps.maxDiff,
                    )
                })
                .filter((test: Test) => {
                    return !!test
                })
        } else {
            return []
        }
    }

    /**
     * get an instance of RangeTest from each interface value.
     *
     * @param tests
     *
     * @return RangeTest[]
     */
    private static buildRangeTests(tests: ApiGetRangeTest[]): RangeTest[] {
        if (!!tests) {
            return tests
                .map((testProps: ApiGetRangeTest) => {
                    return new RangeTest(
                        testProps.name,
                        testProps.active,
                        testProps.mandatoryMinValue,
                        testProps.mandatoryMaxValue,
                        testProps.optionalMinValue,
                        testProps.optionalMaxValue,
                    )
                })
                .filter((test: Test) => {
                    return !!test
                })
        } else {
            return []
        }
    }

    /**
     * get an instance of CombinationTest from each interface value.
     *
     * @param tests
     *
     * @return CombinationTest[]
     */
    private static buildCombinationTests(tests: ApiGetCombinationTest[]): CombinationTest[] {
        if (!!tests) {
            return tests
                .map((testProps: ApiGetCombinationTest) => {
                    const highLimit = Number(testProps.highLimit)
                    const lowLimit = Number(testProps.lowLimit)
                    return new CombinationTest(
                        testProps.name,
                        testProps.active,
                        testProps.firstOperandUid,
                        testProps.secondOperandUid,
                        testProps.operator,
                        highLimit,
                        lowLimit,
                    )
                })
                .filter((test: Test) => {
                    return !!test
                })
        } else {
            return []
        }
    }

    /**
     * get an instance of PreconditionedCalculation from each interface value.
     *
     * @param tests
     *
     * @return PreconditionedCalculation[]
     */
    private static buildPreconditionedCalculations(tests: ApiGetPreconditionedCalculation[]): PreconditionedCalculation[] {
        if (!!tests) {
            return tests
                .map((testProps: ApiGetPreconditionedCalculation) => {
                    return new PreconditionedCalculation(
                        testProps.name,
                        testProps.active,
                        testProps.comparisonVariableUid,
                        testProps.condition,
                        testProps.comparedValue,
                        testProps.comparedVariableUid,
                        testProps.affectationValue,
                    )
                })
                .filter((test: Test) => {
                    return !!test
                })
        } else {
            return []
        }
    }

    /**
     * get an instance of PreviousValue from each interface value.
     *
     * @param previousValues
     *
     * @return PreviousValue[]
     */
    private static buildPreviousValues(previousValues: ApiGetPreviousValue[]): PreviousValue[] {
        if (!!previousValues) {
            return previousValues.map((prevProps: ApiGetPreviousValue) => {
                const date = new Date(prevProps.date)
                return new PreviousValue(
                    prevProps.value,
                    prevProps.objectUri,
                    date,
                    prevProps.stateCode,
                    prevProps.originVariableUid,
                )
            })
        } else {
            return []
        }
    }

    /**
     * Get an instance of MarkList from its interface value.
     *
     * @param scaleProps
     *
     * @return MarkList
     */
    private static buildScale(scaleProps: ApiGetMarkList): MarkList {
        let markList: MarkList = null
        if (!!scaleProps) {
            let marks: Mark[] = []
            if (!!scaleProps.marks) {
                marks = scaleProps.marks.map((markProps: ApiGetMark) => {
                    return new Mark(
                        markProps.text,
                        markProps.value,
                        markProps.imageName,
                        markProps.imageData,
                    )
                })
            }
            markList = new MarkList(
                scaleProps.name,
                scaleProps.minValue,
                scaleProps.maxValue,
                scaleProps.exclusive,
                marks,
            )
        }
        return markList
    }

    /**
     * Get an instance of ValueHintList from its interface value.
     *
     * @param valueHintListProps
     *
     * @return ValueHintList
     */
    private static buildValueHintList(valueHintListProps: ApiGetValueHintList): ValueHintList {
        let valueHintList: ValueHintList = null
        if (!!valueHintListProps) {
            let valueHints: ValueHint[] = []
            if (!!valueHintListProps.valueHints) {
                valueHints = valueHintListProps.valueHints.map((valueProps: ApiGetValueHint) => {
                    return new ValueHint(valueProps.value)
                })
            }
            valueHintList = new ValueHintList(
                valueHintListProps.name,
                valueHints,
            )
        }
        return valueHintList
    }

    /**
     * Get default value of boolean type field.
     *
     * @param defaultValue
     * @param vType
     *
     * @return string | number | boolean
     */
    private static buildDefaultValue(
        defaultValue: string | number | boolean,
        vType: VariableTypeEnum|''|undefined|null,
    ): string | number | boolean {
        if (VariableTypeEnum.BOOLEAN === vType) {
            return 'true' === defaultValue || true === defaultValue;
        }

        return defaultValue;
    }

    /**
     * Get protocol instance.
     *
     * @param protocolProps
     *
     * @return Protocol
     */
    private static buildProtocol(protocolProps: ApiGetProtocol): Protocol {
        return new Protocol(
            protocolProps.name,
            protocolProps.aim,
            protocolProps.algorithm,
            protocolProps.factors.map((factorProps: ApiGetFactor) => {
                return this.buildFactor(factorProps)
            }),
            protocolProps.treatments.map((treatmentProps: ApiGetTreatment) => {
                return this.buildTreatment(treatmentProps)
            }),
            protocolProps.creationdate,
            protocolProps.nbIndividualsPerParcel,
            protocolProps.creatorLogin,
        )
    }

    /**
     * Get a Treatment instance.
     *
     * @param treatmentProps
     *
     * @return Treatment
     */
    private static buildTreatment(treatmentProps: ApiGetTreatment): Treatment {
        return new Treatment(
            treatmentProps.uri,
            treatmentProps.name,
            treatmentProps.shortName,
            treatmentProps.repetitions,
            treatmentProps.modalitiesValue,
        )
    }

    /**
     * get a Factor instance.
     *
     * @param factorProps
     *
     * @retrun Factor
     */
    private static buildFactor(factorProps: ApiGetFactor): Factor {
        return new Factor(
            factorProps.name,
            factorProps.modalities.map((modalityProps: ApiGetModality) => {
                return this.buildModality(modalityProps)
            }),
        )
    }

    /**
     * get a Modality instance.
     *
     * @param modalityProps
     *
     * @return Modality
     */
    private static buildModality(modalityProps: ApiGetModality): Modality {
        return new Modality(modalityProps.value)
    }

    /**
     * Get a RequiredAnnotation instance.
     *
     * @param apiGetRequiredAnnotation
     *
     * @return RequiredAnnotation
     */
    private static buildRequiredAnnotation(apiGetRequiredAnnotation: ApiGetRequiredAnnotation): RequiredAnnotation {
        return new RequiredAnnotation(
            apiGetRequiredAnnotation.pathLevel,
            apiGetRequiredAnnotation.type,
            apiGetRequiredAnnotation.comment,
            apiGetRequiredAnnotation.active,
            apiGetRequiredAnnotation.askWhenEntering,
            apiGetRequiredAnnotation.buisnessObjects,
            apiGetRequiredAnnotation.category,
            apiGetRequiredAnnotation.keywords,
        )
    }

    /**
     * Get a Material instance.
     *
     * @param variables
     * @param apiGetMaterial
     *
     * @return Material
     */
    private static buildMaterial(variables: Variable[], apiGetMaterial: ApiGetMaterial): Material {
        return new Material(
            apiGetMaterial.name,
            apiGetMaterial.physicalDevice,
            apiGetMaterial.manufacturer,
            apiGetMaterial.type,
            this.buildDriver(apiGetMaterial.driver),
            apiGetMaterial.uid,
            variables.filter((variable: Variable) => {
                return !!variable.materialUid && apiGetMaterial.uid === variable.materialUid
            }),
        )
    }

    /**
     * Get a Driver instance.
     *
     * @param driver
     *
     * @return Driver
     */
    private static buildDriver(driver: ApiGetDriver): Driver {
        return new Driver(
            driver.type,
            driver.timeout,
            driver.frameLength,
            driver.frameStart,
            driver.frameEnd,
            driver.csvSeparator,
            driver.baudrate,
            driver.stopbit,
            driver.parity,
            driver.flowControl,
            driver.push,
            driver.request,
            driver.databitsFormat,
            driver.port,
        )
    }

    /**
     * Get a NatureZHE instance.
     *
     * @param natureZHE
     *
     * @return NatureZHE
     */
    private static buildNatureZHE(natureZHE: ApiGetNatureZHE) {
        return new NatureZHE(
            natureZHE.name,
            natureZHE.color,
            natureZHE.texture,
            natureZHE.uri,
        )
    }

    /**
     * Get a OutExperimentationZone instance.
     *
     * @param parent
     * @param outExperimentationZone
     * @param platform
     *
     * @return OutExperimentationZone
     */
    private static buildOutExperimentationZone(
        parent: BusinessObject,
        outExperimentationZone: ApiGetOutExperimentationZone,
        platform: Platform = null,
    ): OutExperimentationZone {
        const outputOutExperimentationZone = new OutExperimentationZone(
            parent,
            outExperimentationZone.uri,
            outExperimentationZone.x,
            outExperimentationZone.y,
            outExperimentationZone.num,
            outExperimentationZone.natureZHEUri,
            outExperimentationZone.notes,
            outExperimentationZone.polygonContour,
            outExperimentationZone.center,
            !!outExperimentationZone.anomaly
                ? this.buildAnomaly(outExperimentationZone.anomaly)
                : null,
        )
        if (!!platform) {
            platform.zheMap.set(`${outputOutExperimentationZone.x},${outputOutExperimentationZone.y}`, outputOutExperimentationZone)
        }
        return outputOutExperimentationZone
    }

    /**
     * Get an Anomaly instance.
     *
     * @param anomalyProps
     *
     * @return Anomaly
     */
    private static buildAnomaly(anomalyProps: ApiGetAnomaly): Anomaly {
        return new Anomaly(
            anomalyProps.description,
            anomalyProps.type,
            anomalyProps.constatedTreatmentName,
        )
    }

    /**
     * Get a Note instance.
     *
     * @param noteProps
     *
     * @return Note
     */
    private static buildNotes(noteProps: NoteInterface): Note {
        return new Note(
            noteProps.text,
            noteProps.deleted,
            noteProps.creatorUri,
            noteProps.creatorName,
            new Date(noteProps.creationDate),
        )
    }

    /**
     * Given an object/structure that implements ApiGetVariable, will return an instance of class UniqueVariable or
     * GeneratorVariable.
     *
     * @param variableProps
     */
    public static buildVariableFromLibrary(variableProps: SimpleVariableDto) {
        return this.buildUniqueVariable({
            active: true,
            askTimestamp: true,
            combinationTests: [], // TODO
            comment: variableProps.comment,
            creationDate: variableProps.created ?? new Date(),
            defaultValue: variableProps.type === VariableTypeEnum.BOOLEAN ? variableProps.defaultTrueValue : null,
            format: (variableProps.type === VariableTypeEnum.ALPHANUMERIC || variableProps.type === VariableTypeEnum.REAL) ?
                variableProps.formatLength?.toString() :
                variableProps.format,
            frameEndPosition: null,
            frameStartPosition: null,
            growthTests: [], // TODO
            mandatory: variableProps.mandatory,
            materialUid: null,
            modificationDate: null,
            name: variableProps.name,
            order: 0,
            pathLevel: variableProps.pathLevel,
            preconditionedCalculations: [], // TODO
            previousValues: [],
            rangeTests: [], // TODO
            repetitions: variableProps.repetitions,
            scale: this.buildSimpleVariableScale(variableProps.scale),
            shortName: variableProps.shortName,
            type: variableProps.type,
            unit: variableProps.unit,
            uri: variableProps['@id'],
            valueHintList: null,
        })
    }

    /**
     * Get an instance of MarkList from its interface value.
     *
     * @param scaleProps
     *
     * @return MarkList
     */
    private static buildSimpleVariableScale(scaleProps: string | VariableScaleDto): MarkList {
        if (typeof scaleProps === 'string') {
            // TODO
            // Dans quel cas obtient-on un string ?
            return null;
        }

        let markList: MarkList = null
        if (!!scaleProps) {
            let marks: Mark[] = []
            if (!!scaleProps.values) {
                marks = scaleProps.values.map((markProps: VariableScaleValueDto) => {
                    return new Mark(markProps.text, markProps.value, '', markProps.pic);
                });
            }
            markList = new MarkList(
                scaleProps.name,
                scaleProps.minValue,
                scaleProps.maxValue,
                !scaleProps.open,
                marks,
            )
        }
        return markList
    }

}
