import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import { evaluate } from 'mathjs'
import FormField from '../models/evaluation-variables/form-field'
import Measure from '../models/evaluation-variables/measure'
import PreviousValue from '../models/evaluation-variables/previous-value'
import Variable from '../models/evaluation-variables/variable'
import CombinationTest from '../models/field-tests/combination-test'
import GrowthTest from '../models/field-tests/growth-test'
import PreconditionedCalculation from '../models/field-tests/preconditioned-calculation'
import RangeTest from '../models/field-tests/range-test'
import Test, {
    COMPARISON_EQUALS,
    COMPARISON_LOWER_EQUALS,
    COMPARISON_LOWER_STRICT,
    COMPARISON_UPPER_EQUALS,
    COMPARISON_UPPER_STRICT,
} from '../models/field-tests/test'
import TestError from '../models/field-tests/test-error'

/**
 * Class TestVariableService.
 */
export default class TestVariableService {

    /**
     * Handle variable tests.
     *
     * @param formFields
     * @param variable
     * @param measure
     * @param onSelect
     *
     * @return Promise<TestError>
     */
    static handleVariableTests(
        project: DataEntryProject,
        formFields: FormField[],
        variable: Variable,
        measure: Measure,
        onSelect = false,
    ): Promise<TestError> {
        return new Promise<TestError>((resolve: (...args: any) => void, reject: (...args: any) => void) => {
            const variableTests = !!variable.tests
                ? variable.tests.filter(test => test.active)
                : []
            if (0 < variableTests.length) {
                variableTests.forEach((test: Test) => {
                    let error: TestError

                    if (!onSelect && test instanceof RangeTest) {
                        error = this.handleRangeTest(project, test, variable, measure)
                    } else if (!onSelect && test instanceof CombinationTest) {
                        error = this.handleCombinationTest(project, test, formFields, variable, measure)
                    } else if (onSelect && test instanceof PreconditionedCalculation) {
                        error = this.handlePreconditionedCalculation(project, test, formFields, variable, measure)
                    } else if (!onSelect && test instanceof GrowthTest) {
                        error = this.handleGrowthTest(project, test, formFields, variable, measure)
                    }

                    if (!!error) {
                        reject(error)
                    }
                })
            }

            resolve()
        })
    }

    /**
     * Perform growth test. Check if the test interval contains
     * the difference between the measure's value and the test comparison variable value.
     * Return NULL when test pass, TestError instance otherwise.
     *
     * @param test
     * @param formFields
     * @param variable
     * @param measure
     *
     * @return TestError
     */
    static handleGrowthTest(
        project: DataEntryProject,
        test: GrowthTest,
        formFields: FormField[],
        variable: Variable,
        measure: Measure,
    ): TestError {
        const error: TestError = null
        const comparisonValue: number = this.extractValueByVariableUid(project, formFields, variable, measure.targetUri, test.comparisonVariableUid)
        const measuredValue: number = !!measure.value
            ? Number(measure.value)
            : null
        if (null !== comparisonValue && null !== measuredValue) {
            const minValue = comparisonValue + test.minDiff
            const maxValue = comparisonValue + test.maxDiff
            if (minValue > measuredValue || maxValue < measuredValue) {
                return new TestError(
                    test,
                    variable,
                    measure,
                    true,
                    {comparisonValue},
                )
            }
        }
        return error
    }

    /**
     * Search in <formFields> array a variable with the specific name <variableName>.
     *
     * @param formFields
     * @param variableUid
     *
     * @return FormField
     */
    static getFormFieldByVariableUid(formFields: FormField[], variableUid: string): FormField {
        const filterFormFields: FormField[] = formFields.filter((formField: FormField) => {
            return variableUid === formField.variableUri
        })
        return !!filterFormFields && 1 === filterFormFields.length
            ? filterFormFields[0]
            : null
    }

    /**
     * Perform RangeTest test.
     * Return NULL when test pass, TestError instance otherwise.
     *
     * @param test
     * @param variable
     * @param measure
     *
     * @return TestError
     */
    private static handleRangeTest(project: DataEntryProject, test: RangeTest, variable: Variable, measure: Measure): TestError {
        let error: TestError = null
        const value: number = !!measure.value && measure.value !== ''
            ? Number(measure.value)
            : null
        if (null !== value) {
            if (value > test.mandatoryMaxValue || value < test.mandatoryMinValue) {
                error = new TestError(
                    test,
                    variable,
                    measure,
                    true,
                )
            } else if (value > test.optionalMaxValue || value < test.optionalMinValue) {
                error = new TestError(
                    test,
                    variable,
                    measure,
                    false,
                )
            }
        }
        return error
    }

    /**
     * Perform CombinationTest test.
     * Return NULL when test pass, TestError instance otherwise.
     *
     * @param test
     * @param formFields
     * @param variable
     * @param measure
     *
     * @return TestError
     */
    private static handleCombinationTest(
        project: DataEntryProject,
        test: CombinationTest,
        formFields: FormField[],
        variable: Variable,
        measure: Measure,
    ): TestError {
        let error: TestError = null
        const firstValue: number = this.extractValueByVariableUid(project, formFields, variable, measure.targetUri, test.firstOperandUid)
        const secondValue: number = this.extractValueByVariableUid(project, formFields, variable, measure.targetUri, test.secondOperandUid)
        if (null !== firstValue && null !== secondValue) {
            const operator = test.getOperator()
            const evaluation: number = evaluate(`${firstValue}${operator}${secondValue}`)
            if (test.lowLimit > evaluation || test.highLimit < evaluation) {
                error = new TestError(
                    test,
                    variable,
                    measure,
                    true,
                    {
                        firstValue,
                        secondValue,
                        evaluation,
                    },
                )
            }
        }
        return error
    }

    /**
     * Perform PreconditionedCalculation test.
     * Return NULL when test pass, TestError instance otherwise.
     *
     * @param test
     * @param formFields
     * @param variable
     * @param measure
     *
     * @return TestError
     */
    private static handlePreconditionedCalculation(
        project: DataEntryProject,
        test: PreconditionedCalculation,
        formFields: FormField[],
        variable: Variable,
        measure: Measure,
    ): TestError {
        const error: TestError = null
        // Only handle measure that are not completed by the operator.
        if (null !== measure.value) {
            return error
        }
        // Measure not complete, then test preconditioned calculation.
        const comparisonValue: number = this.extractValueByVariableUid(project, formFields, variable, measure.targetUri, test.comparisonVariableUid)
        const comparedValue: number = !!test.comparedVariableUid
            ? this.extractValueByVariableUid(project, formFields, variable, measure.targetUri, test.comparedVariableUid)
            : test.comparedValue
        // values are required to perform test check.
        if (null !== comparisonValue && null !== comparedValue) {
            const condition = test.getCondition()
            let evaluation: boolean
            switch (condition) {
                case COMPARISON_EQUALS:
                    evaluation = comparisonValue === comparedValue
                    break
                case COMPARISON_UPPER_STRICT:
                    evaluation = comparisonValue > comparedValue
                    break
                case COMPARISON_UPPER_EQUALS:
                    evaluation = comparisonValue >= comparedValue
                    break
                case COMPARISON_LOWER_STRICT:
                    evaluation = comparisonValue <= comparedValue
                    break
                case COMPARISON_LOWER_EQUALS:
                    evaluation = comparisonValue < comparedValue
                    break
            }
            if (evaluation) {
                return new TestError(
                    test,
                    variable,
                    measure,
                    true,
                    {
                        var1: test.comparisonVariableUid,
                        var2: test.comparedVariableUid,
                        var2bis: test.comparedValue,
                        value: test.affectationValue,
                        condition,
                    },
                )
            }
        }
        return error
    }

    /**
     * Get the value if exists of the variable with name <variableName>. Will search in <formFields> array if the
     * variable exists, otherwise will search in connected variable of <variable> by searching in previous values if one
     * exists concerning the connected variable and the target object defined by <targetUri>.
     *
     * @param formFields
     * @param variable
     * @param targetUri
     * @param variableUid
     *
     * @return number
     */
    private static extractValueByVariableUid(
        project: DataEntryProject,
        formFields: FormField[],
        variable: Variable,
        targetUri: string,
        variableUid: string,
    ): number {
        const comparisonFormField: FormField = this.getFormFieldByVariableUid(formFields, variableUid)
        const previousValue: PreviousValue = this.getPreviousValue(project, formFields, targetUri, variableUid)
        const hasLocalValue = !!comparisonFormField && !!comparisonFormField.measures[0]
            && (!!comparisonFormField.measures[0].value || 0 === comparisonFormField.measures[0].value)
            && comparisonFormField.measures[0].value !== ''
        return hasLocalValue
            ? Number(comparisonFormField.measures[0].value)
            : !!previousValue
                ? Number(previousValue.value)
                : null
    }

    /**
     * Look over all form fields previous values if one exists concerning this target for this variable uid.
     *
     * @param project
     * @param formFields
     * @param targetUri
     * @param variableUid
     *
     * @return PreviousValue
     */
    private static getPreviousValue(project: DataEntryProject, formFields: FormField[], targetUri: string, variableUid: string): PreviousValue {
        let previous: PreviousValue = null
        project.variablesMap.forEach(variable => previous = previous ?? variable.previousValues.filter((prevValue: PreviousValue) => {
            return prevValue.objectUri.replace('/', '') === targetUri &&
                prevValue.originVariableUid === variableUid
        })[0])
        return previous
    }
}
