import { AdonisLocalRecovery } from '@adonis/app/models/app-functionalities/adonis-local-recovery'
import { IindexedDb } from '@adonis/app/plugins/IDBPlugin/IdbPlugin'
import UtilityService from '@adonis/app/services/utility-service'

/**
 * A service to handle indexedDb data export and import in case of emergency.
 */
export default class DatabaseRecoveryService {

  static exportData( idbController: IindexedDb ): void {
    idbController.backup()
        .then( ( database: AdonisLocalRecovery ) => {
          const dataStr = JSON.stringify( database, ( key: string, value: any ) => {
              if (key === 'parent') {
                  return
              }
              return value
          } )
          const fileBlob = new Blob( [dataStr], { type: 'application/json;charset=utf-8' } )
          const filename = `adonis-dbv${database.dbVersion}-${new Date().getTime()}.json`
          UtilityService.saveToFile( URL.createObjectURL( fileBlob ), filename )
        } )
  }

  static importData( idbController: IindexedDb, file: any ): Promise<void> {
    return new Promise( ( resolve ) => {
      const reader = new FileReader()
      reader.onload = e => {
        const fileContent = e.target.result
        if ('string' === typeof fileContent) {
          idbController.restore( JSON.parse( fileContent ) )
              .then( () => resolve() )
        }
      }
      reader.readAsText( file )
    } )
  }
}
