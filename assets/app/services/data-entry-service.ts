import { FieldRegistration } from '@adonis/app/models/app-functionalities/field-registration'
import AbstractProject from '@adonis/app/models/business-objects/abstract-project'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import { IindexedDb } from '@adonis/app/plugins/IDBPlugin/IdbPlugin'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import StructureService from '@adonis/app/services/structure-service'
import { v4 as uuidv4 } from 'uuid'
import Vue from 'vue'
import DesktopUser from '../models/app-functionalities/desktop-user'
import Block, { BLOCK_CLASS } from '../models/business-objects/block'
import DataEntryProject, { STATUS_DONE, STATUS_NEW, STATUS_SENT, STATUS_WIP, } from '../models/business-objects/data-entry-project'
import Device, { DEVICE_CLASS } from '../models/business-objects/device'
import Individual, { INDIVIDUAL_CLASS } from '../models/business-objects/individual'
import SubBlock, { SUBBLOCK_CLASS } from '../models/business-objects/sub-block'
import UnitParcel, { UNITPARCEL_CLASS } from '../models/business-objects/unit-parcel'
import Workpath from '../models/business-objects/workpath'
import DataEntry from '../models/evaluation-variables/data-entry'
import { Evaluable } from '../models/evaluation-variables/evaluation'
import Session from '../models/evaluation-variables/session'
import Variable from '../models/evaluation-variables/variable'
import { IDB_DATA_ENTRY, IDB_FIELD_REGISTRY, IDB_SESSIONS } from '../plugins/vue-indexedDb'
import PlatformService from './platform-service'
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";

/**
 * DataEntryService.
 */
export default class DataEntryService {

  /**
   * Save project to database. It will update the store's state and save all modification to indexedDB.
   *
   * @param vm
   * @param abstractProject
   * @param wip
   * @param doUpdate
   *
   * @return Promise<void>
   */
  static async saveProjectToDatabase(
      vm: Vue,
      abstractProject: AbstractProject,
      wip = true,
      doUpdate = true,
  ): Promise<void> {
    // Before saving into indexDb structure, we need to remove all opened ports from drivers to allow indexedDb to
    // work correctly. We also save it into a map to ensure that we will be able to reconnect all opened ports after
    // indexedDb registration.
    const connectionsMap = abstractProject.connectionsMap
    abstractProject.connectionsMap = null
    // Perform indexedDb registration.
    vm.$indexedDb.put( IDB_DATA_ENTRY, abstractProject )
        .then()

    // Now data entry project is properly saved into indexedDb. We bring back all opened port into the project.
    abstractProject.connectionsMap = connectionsMap
    // Perform update on vuex store to update vue components.
    if (doUpdate) {
      await this.doUpdate( vm, abstractProject, wip )
    }
  }

  /**
   * Start a project which is new. It will open the project and set working path, working object and sessions.
   *
   * @param dataEntryProject
   *
   * @return DataEntryProject
   */
  static handleStartProject( dataEntryProject: AbstractProject ): AbstractProject {

    if (STATUS_NEW !== dataEntryProject.status) {
      throw new Error( 'Trying to start an already started data entry project' )
    }

    dataEntryProject.dataEntry = new DataEntry(
        STATUS_WIP,
        new Date(),
        null,
        null,
        null,
        [],
        null,
        [],
        [],
        false,
        false,
        true,
        false,
        null,
        [],
        false,
        null,
    )
    dataEntryProject.status = STATUS_WIP

    return dataEntryProject
  }

  /**
   * Create a new session in given data entry project.
   * Handles previous unclosed data entry sessions.
   *
   * @param indexedDb
   * @param dataEntryProject
   * @param workpath
   *
   * @return DataEntryProject
   */
  static async handleNewSession(
      indexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
      workpath: Workpath | null,
  ): Promise<AbstractProject> {

    if (STATUS_WIP !== dataEntryProject.status) {
      throw new Error( 'Trying to start a new session on a data entry project which is not running' )
    }
    if (dataEntryProject instanceof DataEntryProject && !!workpath) {
      this.populateSessionWorkpath( dataEntryProject, workpath )
    }
    // Close opened session when exists.
    await this.handleCloseSession( indexedDb, dataEntryProject )

    // Open a new session in data-entry.
    const session = new Session(
        uuidv4(),
        new Date(),
        null,
        [],
        dataEntryProject.id,
    )
    await indexedDb.add( IDB_SESSIONS, session )
    dataEntryProject.dataEntry.sessionIDs.push( session.id )
    dataEntryProject.dataEntry.currentSessionId = session.id
    dataEntryProject.dataEntry.askIdent = false
    if (undefined === dataEntryProject.dataEntry.lastFieldDoMove) {
      dataEntryProject.dataEntry.lastFieldDoMove = true
    }
    dataEntryProject.dataEntry.lastPosition = null
    if (!!workpath) {
      dataEntryProject.dataEntry.user = dataEntryProject.desktopUsers.find(
          ( user: DesktopUser ) => workpath.username === user.name,
      )
    } else if (!!dataEntryProject.creator) {
      dataEntryProject.dataEntry.user = dataEntryProject.creator
    } else {
      throw Error( 'Project doesn\'t have registered desktop user.' )
    }

    return dataEntryProject
  }

  /**
   * Get the current session.
   *
   * @param indexedDb
   * @param abstractProject
   *
   * @return Session
   */
  static async getCurrentSession( indexedDb: IindexedDb, abstractProject: AbstractProject ): Promise<Session> {
    const session: Session = StructureService.buildSession(
        abstractProject.variablesMap,
        await indexedDb.get( IDB_SESSIONS, abstractProject.dataEntry.currentSessionId ),
    )
    if (!!session.endedAt) {
      throw new Error( 'Session closed. Unable to save any data.' )
    }
    return session
  }

  /**
   * Destroy empty sessions and check if a session is open and has not been closed manually.
   * (not closed manually: leaving the app without interrupting the open data entry project)
   *
   * @param indexedDb
   * @param project
   * @param stop
   * @param vm
   *
   * @return Promise<void>
   */
  static handleCloseSession( indexedDb: IindexedDb, project: AbstractProject, stop = false, vm: Vue = null ): Promise<void> {

    if (STATUS_WIP !== project.status) {
      throw new Error( 'Trying to close a session on a data entry project which is not running' )
    }

    return this.retrieveSessionForProject( indexedDb, project )
        .then( sessions => {

          project.dataEntry.sessionIDs = []
          sessions.forEach( ( session: Session ) => {
            if (session.formFields.length === 0) {
              indexedDb.delete( IDB_SESSIONS, session.id )
                  .then()
            } else {
              project.dataEntry.sessionIDs.push( session.id )
              if (!session.endedAt) {
                session.endedAt = new Date()
                indexedDb.put( IDB_SESSIONS, session )
                    .then()
              }
            }
          } )

          if (stop && !!vm && project instanceof DataEntryProject && !!project.dataEntry?.workpath) {

            const firstSession = sessions.sort( ( s1: Session, s2: Session ) => s1.startedAt.getTime() - s2.startedAt.getTime() )[0]
                ?? new Session(
                    uuidv4(),
                    new Date(),
                    null,
                    [],
                    project.id,
                )

            const visited = project.dataEntry.visitedBusinessObjectsURI
            const toVisite = project.dataEntry.workpath.filter( uri => !visited.includes( uri ) )

            return Promise.all( toVisite.map( uri => {
              const formFieldGroups: FormFieldGroup[] = []
              return FormMeasureService.addFormFieldGroupByUri( project, uri, formFieldGroups, vm )
                  .then( () => formFieldGroups )
            } ) )
                .then( ffgArrayArray => {

                  const ofArray = ffgArrayArray
                      .reduce(
                          ( acc: FormFieldGroup[], ffgArray: FormFieldGroup[] ) => [...acc, ...ffgArray],
                          [],
                      )
                      .reduce(
                          ( acc: FormField[], ffg: FormFieldGroup ) => [...acc, ...ffg.objectFields],
                          [],
                      )

                  return FormMeasureService.saveFormFieldsToSession( indexedDb, project, ofArray, firstSession, true )
                      .then( () => indexedDb.clear( IDB_FIELD_REGISTRY ) )
                } )

          } else {
            return indexedDb.clear( IDB_FIELD_REGISTRY )
          }
        } )
  }

  /**
   * Stop given data entry project : The data entry will be marked as ended.
   * Handles previous unclosed data entry sessions.
   *
   * @param vm
   * @param indexedDb
   * @param dataEntryProject
   *
   * @return Promise<DataEntryProject>
   */
  static handleStopProject(
      vm: Vue,
      indexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
  ): Promise<AbstractProject> {

    if (STATUS_WIP !== dataEntryProject.status) {
      throw new Error( 'Trying to stop a data entry project which is not running' )
    }

    return this.handleCloseSession( indexedDb, dataEntryProject, true, vm )
        .then( () => {

          dataEntryProject.dataEntry.endedAt = new Date()
          dataEntryProject.status = STATUS_DONE
          return dataEntryProject
        } )

  }

  /**
   * Move the project to the x next element or to the x previous element.
   *
   * @param dataEntryProject
   * @param move
   *
   * @return DataEntryProject
   */
  static moveTo( dataEntryProject: AbstractProject, move = +1 ): AbstractProject {

    if (STATUS_WIP !== dataEntryProject.status) {
      throw new Error( 'Trying to move project working location on a data entry project which is not running' )
    }

    const currentIndex: number = dataEntryProject.dataEntry.workpath.indexOf( dataEntryProject.dataEntry.currentPosition )
    if (0 <= currentIndex) {
      const newIndex = currentIndex + move
      if (0 <= newIndex && dataEntryProject.dataEntry.workpath.length > newIndex) {
        dataEntryProject.dataEntry.lastPosition = dataEntryProject.dataEntry.currentPosition
        dataEntryProject.dataEntry.currentPosition = dataEntryProject.dataEntry.workpath[newIndex]
      }
    }
    return dataEntryProject
  }

  /**
   * Move the project to the given target; Movement only possible on structural objects.
   *
   * @param dataEntryProject
   * @param target
   *
   * @return DataEntryProject
   */
  static moveToObject( dataEntryProject: AbstractProject, target: Evaluable ): AbstractProject {

    if (target instanceof Individual || target instanceof UnitParcel ||
        target instanceof SubBlock || target instanceof Block ||
        target instanceof Device
    ) {
      dataEntryProject.dataEntry.lastPosition = dataEntryProject.dataEntry.currentPosition
      dataEntryProject.dataEntry.currentPosition = target.uri
      return dataEntryProject
    } else if (!!target) {
      throw new Error( 'DataEntryService.moveToObject() called on an object that is not one of Individual, UnitParcel, SubBlock, Block or Device.' )
    }
  }

  /**
   * Translate a project data entry status.
   *
   * @param vm
   * @param status2translate
   *
   * @return string
   */
  static translateDataEntryProjectStatus( vm: Vue, status2translate: string ): string {

    let status: string
    switch (status2translate) {
      case STATUS_NEW:
        status = vm.$t( 'dataEntry.situation.project.statusValue.toStart' )
            .toString()
        break
      case STATUS_WIP:
        status = vm.$t( 'dataEntry.situation.project.statusValue.running' )
            .toString()
        break
      case STATUS_DONE:
        status = vm.$t( 'dataEntry.situation.project.statusValue.done' )
            .toString()
        break
      case STATUS_SENT:
        status = vm.$t( 'dataEntry.situation.project.statusValue.sent' )
            .toString()
        break
      default:
        status = status2translate
        break
    }
    return status
  }

  /**
   * Defines if the workpath contains the evaluable.
   *
   * @param project
   * @param object
   *
   * @return boolean
   */
  static evaluableInWorkpath( project: DataEntryProject, object: BusinessObject ): boolean {
    const objectIsIn = !!object && (0 === project.dataEntry.workpath.length || project.dataEntry.workpath.includes( object.uri ))
    let oneChildIsIn = false
    if (!objectIsIn && !!object) {
      object.directChildren.forEach( ( child: BusinessObject ) => {
        oneChildIsIn = oneChildIsIn || this.evaluableInWorkpath( project, child )
      } )
    }
    return objectIsIn || oneChildIsIn
  }

  /**
   * Handle new session Workpath.
   *
   * @param project
   * @param workpath
   */
  public static populateSessionWorkpath( project: DataEntryProject, workpath: Workpath ): void {
    const currentPosition = project.dataEntry.currentPosition
    const workpaths: string[] = workpath.path.split( '/' )
    const uriToShowInGraphic: string[] = []
    workpaths.shift() // Workpath start by '/' char, so remove the first empty element.
    let branchedWorkpaths: string[][] = []
    const finalWorkpaths: string[] = []
    const variablesMapByLevel = this.buildVariablesMapByLevel( project )
    let lastVector: number[] = null
    let lastCoordinate: number[] = null
    let currentVector: number[] = null
    let currentCoordinate: number[] = null
    let currentEvaluable: Evaluable = null
    workpaths.forEach( ( workpathString: string ) => {
      // Add inside branched workpath all parent who has at list one variable.
      branchedWorkpaths = [
        ...branchedWorkpaths,
        this.extendWorkpathsToParent(
            project,
            variablesMapByLevel,
            workpathString,
        ),
      ]
      // Add inside visitableObjects all object uri who are visible in graphic view
      this.extendWorkpathsToParent(
          project,
          null,
          workpathString,
      )
          .forEach( ( uri: string ) => {
            if (!uriToShowInGraphic.includes( uri )) {
              uriToShowInGraphic.unshift( uri )
            }
          } )
      // Determine the movement vector
      currentVector = null
      currentCoordinate = null
      currentEvaluable = PlatformService.getObjectFromUri( project.platform, workpathString )
      if (!!currentEvaluable) {
        currentCoordinate = PlatformService.getParcelCoordinate( currentEvaluable )
        if (!!lastCoordinate && !!currentCoordinate) {
          currentVector = [currentCoordinate[0] - lastCoordinate[0], currentCoordinate[1] - lastCoordinate[1]]
        }
      }
      const movementAlert = !!currentVector && 1 !== Math.abs( currentVector[0] ) + Math.abs( currentVector[1] )
      const directionAlert = !!lastVector && !!currentVector && (
          Math.sign( currentVector[0] ) !== Math.sign( lastVector[0] ) ||
          Math.sign( currentVector[1] ) !== Math.sign( lastVector[1] )
      )
      if (movementAlert || directionAlert) {
        project.dataEntry.movementAlertMap.set( workpathString, {
          currentUri: workpathString,
          currentPosition: [...currentCoordinate],
          lastPosition: [...lastCoordinate],
          deltaPosition: [...currentVector],
        } )
      }
      lastVector = currentVector
      lastCoordinate = currentCoordinate
    } )
    if (workpath.startEnd) {
      const memoryMap: Map<string, string> = new Map()
      branchedWorkpaths.forEach( ( branch: string[] ) => {
        branch.forEach( ( workpathString: string, bIndex: number ) => {
          if (!finalWorkpaths.includes( workpathString )) {
            if (branch.length === bIndex + 1) {
              finalWorkpaths.push( workpathString )
            } else {
              const type: string = PlatformService.getTypeFromUri( workpathString )
              const memoryWorkpath: string = memoryMap.get( type )
              if (!!memoryWorkpath && memoryWorkpath !== workpathString && !finalWorkpaths.includes( memoryWorkpath )) {
                finalWorkpaths.push( memoryWorkpath )
              }
              memoryMap.set( type, workpathString )
            }
          }
        } )
      } );
      [INDIVIDUAL_CLASS, UNITPARCEL_CLASS, SUBBLOCK_CLASS, BLOCK_CLASS, DEVICE_CLASS].forEach( type => {
        const memoryWorkpath: string = memoryMap.get( type )
        if (!!memoryWorkpath && !finalWorkpaths.includes( memoryWorkpath )) {
          finalWorkpaths.push( memoryWorkpath )
        }
      } )
    } else {
      branchedWorkpaths.forEach( ( branch: string[] ) => {
        branch.forEach( ( workpathString: string ) => {
          if (!finalWorkpaths.includes( workpathString )) {
            finalWorkpaths.push( workpathString )
          }
        } )
      } )
    }

    // In case current position exists in the workpath, only updates the workpaths array,
    // otherwise set the current position to the first element in the workpaths array.
    project.dataEntry.lastPosition = project.dataEntry.currentPosition
    project.dataEntry.currentPosition = finalWorkpaths.includes( currentPosition )
        ? currentPosition
        : finalWorkpaths[0]
    project.dataEntry.workpath = finalWorkpaths
    project.dataEntry.currentWorkpath = workpath.username
    project.dataEntry.restrictedObject = uriToShowInGraphic
  }

  /**
   * Find the sessions corresponding to a project given in param
   * @param indexedDb
   * @param abstractProject
   */
  public static async retrieveSessionForProject(
      indexedDb: IindexedDb,
      abstractProject: AbstractProject,
  ): Promise<Session[]> {
    const promises = abstractProject.dataEntry.sessionIDs.map( async ( id ) => {
      const sessionProps = await indexedDb.get( IDB_SESSIONS, id )
      if (!!sessionProps) {
        return StructureService.buildSession( abstractProject.variablesMap, sessionProps )
      }
    } )
    return (await Promise.all( promises )).filter( ( session ) => !!session )
  }

  /**
   * Dispatch in store the changes in dataEntryProject.
   * If wip, the project will stay or be opened, otherwise it will be closed
   *
   * vm parameter is required to perform dispatch in vuex store.
   *
   * @param vm
   * @param abstractProject
   * @param wip
   *
   * @return Promise<void>
   */
  private static async doUpdate( vm: Vue, abstractProject: AbstractProject, wip: boolean ): Promise<void> {
    if (wip) {
      if (!vm.$store.getters['app/isWorkInProgress']) {
        await vm.$store.dispatch( 'app/setWorkInProgress', true )
      }
      return vm.$store.dispatch( 'dataEntry/setWorkInProgress', {
        vm,
        wip: abstractProject,
      } )
    } else {
      if (vm.$store.getters['app/isWorkInProgress']) {
        await vm.$store.dispatch( 'app/setWorkInProgress', false )
      }
      return vm.$store.dispatch(
          'dataEntry/setWorkInProgress', {
            vm,
            wip: null,
          } )
    }
  }

  /**
   * Build the map which list all variables concerning a same level between available levels:
   *   'Individual', 'UnitParcel', 'SubBlock', 'Block', 'Device'
   *
   * @param dataEntryProject
   *
   * @return Map<PathLevelEnum, Variable[]>
   */
  private static buildVariablesMapByLevel( dataEntryProject: DataEntryProject ): Map<PathLevelEnum, Variable[]> {
    const mapVariablesByLevel: Map<PathLevelEnum, Variable[]> = new Map()
    mapVariablesByLevel.set( PathLevelEnum.INDIVIDUAL, [] )
    mapVariablesByLevel.set( PathLevelEnum.UNIT_PLOT, [] )
    mapVariablesByLevel.set( PathLevelEnum.SUB_BLOCK, [] )
    mapVariablesByLevel.set( PathLevelEnum.BLOCK, [] )
    mapVariablesByLevel.set( PathLevelEnum.EXPERIMENT, [] );
    [...dataEntryProject.uniqueVariables, ...dataEntryProject.generatorVariables].forEach( variable => {
      const variables = mapVariablesByLevel.get( variable.pathLevel )
      mapVariablesByLevel.set( variable.pathLevel, [...variables, variable] )
    } )
    return mapVariablesByLevel
  }

  /**
   * Given an object, will return the it's parent uris.
   * The array return is filter on object which is concerned by at least 1 variable.
   *
   * @param dentryProject
   * @param mapVariables
   * @param uri
   *
   * @return string[]
   */
  private static extendWorkpathsToParent(
      dentryProject: DataEntryProject,
      mapVariables: Map<PathLevelEnum, Variable[]>,
      uri: string,
  ): string[] {
    const extendedWorkpath: string[] = []
    const object: Evaluable = dentryProject.platform.uriMap.get( uri )
    const { individual, parcel, subBlock, block, device } = PlatformService.getBranchObjects( object )
    const hasClassLevelActiveVariable = ( level: PathLevelEnum ) => {
      return !mapVariables ||
          mapVariables.get( level )
              .filter( ( variable: Variable ) => {
                return variable.active
              } )
              .length
    }
    if (!!individual) {
      if (hasClassLevelActiveVariable( PathLevelEnum.INDIVIDUAL )) {
        extendedWorkpath.unshift( individual.uri )
      }
    }
    if (!!parcel) {
      if (hasClassLevelActiveVariable( PathLevelEnum.UNIT_PLOT )) {
        extendedWorkpath.unshift( parcel.uri )
      }
    }
    if (!!subBlock) {
      if (hasClassLevelActiveVariable( PathLevelEnum.SUB_BLOCK )) {
        extendedWorkpath.unshift( subBlock.uri )
      }
    }
    if (!!block) {
      if (hasClassLevelActiveVariable( PathLevelEnum.BLOCK )) {
        extendedWorkpath.unshift( block.uri )
      }
    }
    if (!!device) {
      if (hasClassLevelActiveVariable( PathLevelEnum.EXPERIMENT )) {
        extendedWorkpath.unshift( device.uri )
      }
    }

    return extendedWorkpath
  }

  /**
   * Read each session associated with the project to generate values working table.
   * Allows better efficiency in the working process, while it increases the project opening time.
   *
   * @param iIndexedDb
   * @param project
   */
  static async buildFormFieldDatabase( iIndexedDb: IindexedDb, project: AbstractProject ): Promise<void> {
    return iIndexedDb.clear( IDB_FIELD_REGISTRY )
        .then( async () => {
              const sessionId = project.dataEntry.currentSessionId
              let sessions = await DataEntryService.retrieveSessionForProject( iIndexedDb, project )
              // First order sessions descending to get the latest value first.
              sessions = sessions.sort( ( session1: Session, session2: Session ) => {
                return session1.startedAt < session2.startedAt
                    ? 1
                    : -1
              } )

              const handleField = async ( formField: FormField ) => {
                const newestValue = await iIndexedDb.get( IDB_FIELD_REGISTRY, formField.uuid )
                return !newestValue
                    ? new FieldRegistration( formField.uuid, formField, sessionId )
                    : null
              }

              const promiseSession = ( session: Session ) =>
                  Promise.all( session.formFields.map( handleField ) )
                      .then(
                          registries => iIndexedDb.addAll( IDB_FIELD_REGISTRY, registries.filter( r => !!r ) ),
                      )

              await sessions.reduce(
                  ( p: Promise<void>, session: Session ) => p.then( _ => promiseSession( session ) ),
                  Promise.resolve(),
              )
            },
        )
  }
}
