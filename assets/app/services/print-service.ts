import AbstractProject from '@adonis/app/models/business-objects/abstract-project'
import Block from '@adonis/app/models/business-objects/block'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import Individual from '@adonis/app/models/business-objects/individual'
import SubBlock from '@adonis/app/models/business-objects/sub-block'
import UnitParcel from '@adonis/app/models/business-objects/unit-parcel'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import UtilityService from '@adonis/app/services/utility-service'

/**
 * A service to handle printing functions.
 */
export default class PrintService {

  /** Flag to define if the print service must show end print on close print call. */
  private static active = false

  /** String to divide the elements. */
  private static dividerString = `${'*'.repeat( 30 )}`

  /** String to divide the elements. */
  private static dividerSmallString = `${'*'.repeat( 2 )}`

  private static printRequestQueue: string[] = []

  /**
   * Print a row on specific port.
   *
   * @param rowContent
   */
  public static addPrintRequest( rowContent: string ): void {
    this.printRequestQueue.push( rowContent.trimRight() )
  }

  /**
   * Start the project printing requires some information to be print.
   *
   * @param vm
   * @param dataEntryProject
   */
  public static startPrintDataEntry( vm: Vue, dataEntryProject: DataEntryProject ): void {
    this.active = true
    const projectName = dataEntryProject.name
    const dateString = this.printDate( new Date() )

    this.addPrintRequest( '' )
    this.addPrintRequest( this.dividerString )
    this.addPrintRequest(
        vm.$t( 'dataEntry.printer.startTitle' )
            .toString(),
    )
    this.addPrintRequest( projectName )
    this.addPrintRequest( dateString )
    this.addPrintRequest( this.dividerString )
    this.addPrintRequest( '' )
    this.addPrintRequest( '' )

    this.flushPrintQueue()
  }

  /**
   * Start the project printing requires some information to be print.
   *
   * @param vm
   * @param dataEntryProject
   */
  public static closePrintDataEntry( vm: Vue, dataEntryProject: DataEntryProject ): void {
    if (this.active) {
      const projectName = dataEntryProject.name
      const dateString = this.printDate( new Date() )

      this.addPrintRequest( '' )
      this.addPrintRequest( this.dividerString )
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.endTitle' )
              .toString(),
      )
      this.addPrintRequest( projectName )
      this.addPrintRequest( dateString )
      this.addPrintRequest( this.dividerString )

      this.flushPrintQueue()
      this.active = false
    }
  }

  /**
   * Print all information about the validated formFieldGroup.
   *
   * @param vm
   * @param formFieldGroup
   */
  public static printFormFieldGroup( vm: Vue, project: AbstractProject, formFieldGroup: FormFieldGroup ): void {
    this.printContextualInformation( vm, formFieldGroup )

    formFieldGroup.objectFields.forEach( ( formField: FormField ) => {
      const variableName = project.variablesMap.get(formField.variableUri).shortName
      formField.measures.forEach( ( measure: Measure ) => {
        const dateString = this.printDate( measure.timestamp )
        const measureValue = !!measure.value
            ? measure.value
            : measure.state.code
        this.addPrintRequest(
            vm.$t( 'dataEntry.printer.dateLabel', { date: dateString } )
                .toString(),
        )
        this.addPrintRequest(
            vm.$t( 'dataEntry.printer.measureLabel', { variable: variableName, value: measureValue } )
                .toString(),
        )
        this.addPrintRequest( this.dividerSmallString )
      } )
    } )

    this.flushPrintQueue()
  }

  /**
   * Add to print queue the contextual information of measures being printed.
   *
   * @param vm
   * @param formFieldGroup
   */
  private static printContextualInformation(
      vm: Vue,
      formFieldGroup: FormFieldGroup,
  ): void {
    const object = formFieldGroup.object
    const device = object.parentDevice()
    this.addPrintRequest(
        vm.$t( 'dataEntry.printer.deviceLabel', { device: device.name } )
            .toString(),
    )
    const breadcrumbData = formFieldGroup.breadcrumbData
    if (object instanceof Individual || object instanceof UnitParcel && !device.individualPU) {
      // Case of last child element (individual or unit parcel when working in surface project.
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.positionLabel', { x: object.x, y: object.y } )
              .toString(),
      )
      if (!!breadcrumbData.identValue && '' !== breadcrumbData.identValue) {
        this.addPrintRequest(
            vm.$t( 'dataEntry.printer.unitParcelIdentLabel', {
              parcel: breadcrumbData.unitParcelName,
              ident: breadcrumbData.identValue,
            } )
                .toString(),
        )
      } else {
        this.addPrintRequest(
            vm.$t( 'dataEntry.printer.unitParcelLabel', { parcel: breadcrumbData.unitParcelName } )
                .toString(),
        )
      }
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.treatmentLabel', { treatment: breadcrumbData.treatmentName } )
              .toString(),
      )
    } else if (object instanceof UnitParcel && device.individualPU) {
      // Case of unit parcel when working in individual level project.
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.blockLabel', { block: breadcrumbData.blockName } )
              .toString(),
      )
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.unitParcelLabel', { parcel: breadcrumbData.unitParcelName } )
              .toString(),
      )
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.treatmentLabel', { treatment: breadcrumbData.treatmentName } )
              .toString(),
      )
    } else if (object instanceof SubBlock) {
      // Case of sub block context.
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.blockLabel', { block: breadcrumbData.blockName } )
              .toString(),
      )
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.subBlockLabel', { subBlock: breadcrumbData.subBlockName } )
              .toString(),
      )
    } else if (object instanceof Block) {
      // Case of sub block context.
      this.addPrintRequest(
          vm.$t( 'dataEntry.printer.blockLabel', { block: breadcrumbData.blockName } )
              .toString(),
      )
    }
    this.addPrintRequest( this.dividerSmallString )
  }

  /**
   * Send a print request to the navigator in order to print all element in the print queue into of text plain file.
   */
  private static flushPrintQueue(): void {
    // Include some row due to printer margin.
    // Build the file to be printed.
    const printContent = this.printRequestQueue.join( '\r\n' ) + ' \r\n \r\n'
    const data = new Blob( [printContent], { type: 'text/plain' } )
    const dataUrl = window.URL.createObjectURL( data )
    // Prepare print frame hook.
    const closePrint = function () {
      document.body.removeChild( this.__container__ ) // In this context, this represents the print frame.
    }
    const setPrint = function () {
      this.contentWindow.__container__ = this // In this context, this represents the print frame.
      this.contentWindow.onbeforeunload = closePrint
      this.contentWindow.onafterprint = closePrint
      this.contentWindow.focus() // Required for IE
      this.contentWindow.print()
    }
    // Instantiate iframe.
    const printFrame = document.createElement( 'iframe' )
    printFrame.onload = setPrint
    printFrame.style.position = 'fixed'
    printFrame.style.right = '0'
    printFrame.style.bottom = '0'
    printFrame.style.width = '0'
    printFrame.style.height = '0'
    printFrame.style.border = '0'
    printFrame.src = dataUrl
    document.body.appendChild( printFrame )
    // Clear print queue.
    this.printRequestQueue = []
  }

  /**
   * Print date in printer format.
   *
   * @param timestamp
   *
   * @return string
   */
  private static printDate( timestamp: Date ): string {
    const dateValue = new Date( timestamp )
    return UtilityService.formatDateTime( dateValue )
  }

}
