import BreadcrumbData from '@adonis/app/models/app-functionalities/breadcrumb-data'
import { FieldRegistration } from '@adonis/app/models/app-functionalities/field-registration'
import {
    MeasureReportData,
    REPORT_TYPE_ANNOTATION,
    REPORT_TYPE_MEASURE,
    REPORT_TYPE_OBJECT,
} from '@adonis/app/models/app-functionalities/measure-report-data'
import AbstractProject from '@adonis/app/models/business-objects/abstract-project'
import { BLOCK_CLASS } from '@adonis/app/models/business-objects/block'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import { DEVICE_CLASS } from '@adonis/app/models/business-objects/device'
import Individual, { INDIVIDUAL_CLASS } from '@adonis/app/models/business-objects/individual'
import { SUBBLOCK_CLASS } from '@adonis/app/models/business-objects/sub-block'
import UnitParcel, { UNITPARCEL_CLASS } from '@adonis/app/models/business-objects/unit-parcel'
import { HasAnnotations } from '@adonis/app/models/evaluation-variables/annotation'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import GeneratedField from '@adonis/app/models/evaluation-variables/generated-form-fields'
import GeneratorVariable from '@adonis/app/models/evaluation-variables/generator-variable'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import Session from '@adonis/app/models/evaluation-variables/session'
import StateCode, { CODE_NONE, CODE_TYPE_DEAD, CODE_TYPE_MISSING, STATE_NONE } from '@adonis/app/models/evaluation-variables/state-code'
import UniqueVariable from '@adonis/app/models/evaluation-variables/unique-variable'
import ValueHint from '@adonis/app/models/evaluation-variables/value-hint'
import Variable, { HasVariables } from '@adonis/app/models/evaluation-variables/variable'
import FieldVariable from '@adonis/app/modules/data-entry/form/fields/field-variable'
import { IindexedDb } from '@adonis/app/plugins/IDBPlugin/IdbPlugin'
import { IDB_FIELD_REGISTRY, IDB_SESSIONS } from '@adonis/app/plugins/vue-indexedDb'
import DataEntryService from '@adonis/app/services/data-entry-service'
import PlatformService from '@adonis/app/services/platform-service'
import StructureService from '@adonis/app/services/structure-service'
import { TYPE_PRIMARY, TYPE_SECONDARY } from '@adonis/shared/constants'
import PathLevelEnum from "@adonis/shared/constants/path-level-enum"
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum"
import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { ConfirmAction } from '@adonis/shared/models/confirm-action'
import { Vue } from 'vue-property-decorator'

/**
 * Class MeasureService.
 */
export default class FormMeasureService {

  /**
   * @param variableName
   * @param position
   * @param repetitions
   * @param mandatory
   *
   * @return string
   */
  static fieldLabel( variableName: string, position: number, repetitions: number, mandatory = false ): string {
    const append: string = 1 < repetitions
        ? `(${position}/${repetitions})`
        : ''
    return mandatory
        ? `* ${variableName + append}`
        : variableName + append
  }

  /**
   * @param measure
   * @param stateCodes
   *
   * @return StateCode
   */
  static handleMeasureStateCode( measure: Measure, stateCodes: StateCode[] ): StateCode {
    let newState: StateCode = measure.state
    const measureAsNumber: number = Number( measure.value )
    if (!!measure.value && !isNaN( measureAsNumber ) && 0 > measureAsNumber) {
      stateCodes.forEach( ( stateCode: StateCode ) => {
        if (stateCode.code === measureAsNumber) {
          newState = stateCode
        }
      } )
    }
    return newState
  }

  /**
   * Build a new instance of Measure based on the given information relative to a specification iteration of variable.
   *
   * @param dataEntryProject
   * @param parent
   * @param variable
   * @param target
   * @param index
   * @param repetition
   *
   * @return Measure
   */
  static createNewMeasureForVariable(
      dataEntryProject: AbstractProject,
      parent: HasVariables,
      variable: Variable,
      target: string,
      index: number,
      repetition: number,
  ): Measure {
    const object = dataEntryProject.platform.uriMap.get( target )
    let state
    if (object instanceof Individual || object instanceof UnitParcel) {
      const codeNumber = object.stateCode
      state = dataEntryProject.stateCodes.find( ( stateCode: StateCode ) => stateCode.code === codeNumber )
    }
    const value = variable.defaultValue ?? null

    if (!value && VariableTypeEnum.DATE === variable.type) {
      state = dataEntryProject.stateCodes.find( ( stateCode: StateCode ) => CODE_TYPE_MISSING === stateCode.type )
    }

    if (!state) {
      state = STATE_NONE
    }

    return new Measure(
        value,
        target,
        variable.uri,
        index,
        repetition,
        new Date(),
        state,
        [],
    )
  }

  /**
   * Propagate the given state code to all object concerned by it in the data entry project.
   *
   * @param iIndexedDb
   * @param dataEntryProject
   * @param targetUri
   * @param stateCode
   * @param forcePropagationLevel
   *
   * @return DataEntryProject
   */
  static async propagateStateCode(
      iIndexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
      targetUri: string,
      stateCode: StateCode,
      forcePropagationLevel: PathLevelEnum = null,
  ): Promise<AbstractProject> {
    const target = dataEntryProject.platform.uriMap.get( targetUri )
    const propagationLevel = !forcePropagationLevel
        ? stateCode.getPropagation( target instanceof Individual ? false : dataEntryProject.isCurrentSurfaceParcel )
        : forcePropagationLevel
    switch (propagationLevel) {
        case PathLevelEnum.UNIT_PLOT:
        let unitParcel: UnitParcel
        if (target instanceof Individual) {
          unitParcel = PlatformService.getParentOfObject( dataEntryProject.platform, targetUri ) as UnitParcel
        } else if (target instanceof UnitParcel) {
          unitParcel = target as UnitParcel
          target.stateCode = stateCode.code
          if (CODE_TYPE_DEAD === stateCode.type) {
            target.demiseDate = new Date()
          }
        }
        if (!!unitParcel) {
          unitParcel.stateCode = stateCode.code
          dataEntryProject = await this.propagateStateCodeToTargetUri( iIndexedDb, dataEntryProject, dataEntryProject,
              unitParcel.uri, stateCode )
          await Promise.all( unitParcel.individuals.map( async ( individual: Individual ) => {
            dataEntryProject = await this.propagateStateCodeToTargetUri( iIndexedDb, dataEntryProject, dataEntryProject,
                individual.uri, stateCode )
          } ) )
        }
        break
        case PathLevelEnum.INDIVIDUAL:
        if (target instanceof Individual || target instanceof UnitParcel) {
          target.stateCode = stateCode.code
          if (CODE_TYPE_DEAD === stateCode.type) {
            target.demiseDate = new Date()
          }
          dataEntryProject = await this.propagateStateCodeToTargetUri( iIndexedDb, dataEntryProject, dataEntryProject,
              targetUri, stateCode )
        }
        break
      default:
        break
    }
    return dataEntryProject
  }

  /**
   * Get all form fields regarding a given uri and a specific index of generation.
   *
   * @param iIndexedDb
   * @param dataEntryProject
   * @param baseObject
   * @param targetUri
   * @param index
   *
   * return FormField[]
   */
  static getFormFieldOfLevel(
      iIndexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
      baseObject: HasVariables,
      targetUri: string,
      index = 1,
  ): Promise<FormField[]> {
    const { uniqueVariables, generatorVariables } = this.getVariableOfLevel( baseObject, targetUri )
    const variables = [...uniqueVariables, ...generatorVariables]
    const flattenGenerated = ( generatorVars: GeneratorVariable[] ): void => {
      generatorVars.forEach( generatorVar => {
        variables.push( ...generatorVar.uniqueVariables )
        variables.push( ...generatorVar.generatorVariables )
        flattenGenerated( generatorVar.generatorVariables )
      } )
    }
    flattenGenerated( generatorVariables )
    const varsMap = new Map<string, Variable>()
    variables.forEach( variable => varsMap.set( variable.uri, variable ) )

    return new Promise( resolve => {

      let ffs = this.generateEmptyFormFields( dataEntryProject, baseObject, uniqueVariables,
          generatorVariables, targetUri, index )

      // Sort using variable order and return fields array.
      ffs = ffs.sort( ( fieldA: FormField, fieldB: FormField ) => {
        const variableA = dataEntryProject.variablesMap.get(fieldA.variableUri)
        const variableB = dataEntryProject.variablesMap.get(fieldB.variableUri)
        return variableA.order - variableB.order
      } )

      resolve( ffs )

    } ).then( ( formFields: FormField[] ) => {

      return Promise.all( formFields.map( async ( ff: FormField ) => {

                const fr = await iIndexedDb.get( IDB_FIELD_REGISTRY, ff.uuid )
                return !!fr
                    ? StructureService.buildFormField( varsMap, fr.formField )
                    : ff
              },
          ),
      )
    } )
  }

  /**
   * Create empty form fields for each variables.
   *
   * @param dataEntryProject
   * @param baseObject
   * @param uniqueVariables
   * @param generatorVariables
   * @param targetUri
   * @param index
   */
  static generateEmptyFormFields(
      dataEntryProject: AbstractProject,
      baseObject: HasVariables,
      uniqueVariables: UniqueVariable[],
      generatorVariables: GeneratorVariable[],
      targetUri: string,
      index: number,
  ): FormField[] {
    const formFields: FormField[] = []
    // Get the unique variable fields.
    uniqueVariables.forEach( ( uniqueVariable: UniqueVariable ) => {
      // Variable is active, build it's field.
      const generatedField: GeneratedField[] = null
      formFields.push( new FormField(
          targetUri,
          uniqueVariable.uri,
          this.getVariableMeasureFromProject(
              dataEntryProject,
              baseObject,
              uniqueVariable,
              targetUri,
              index,
          ),
          generatedField,
      ) )
    } )
    // Get the generated variable fields.
    generatorVariables.forEach( ( generatorVariable: GeneratorVariable ) => {
      // Variable is active, build it's field.
      const measures: Measure[] = this.getVariableMeasureFromProject(
          dataEntryProject,
          baseObject,
          generatorVariable,
          targetUri,
          index,
      )

      const { genUniqueVariables, genGeneratorVariables } = this.getVariableOfLevel( generatorVariable, targetUri )
      const children: GeneratedField[] = []
      for (let generatedIndex = 1; generatedIndex < Number( measures[0].value ) + 1; ++generatedIndex) {
        children.push( new GeneratedField(
            generatedIndex,
            generatorVariable.generatedPrefix,
            generatorVariable.numeralIncrement,
            this.generateEmptyFormFields(
                dataEntryProject,
                generatorVariable,
                genUniqueVariables,
                genGeneratorVariables,
                targetUri,
                generatedIndex,
            ),
        ) )
      }
      formFields.push( new FormField(
          targetUri,
          generatorVariable.uri,
          measures,
          children,
      ) )
    } )
    return formFields
  }

  /**
   * Search for all variables of a specific level in an object that implements HasVariables interface.
   *
   * @param baseObject
   * @param targetUri
   *
   * @return {UniqueVariable[], GeneratorVariable[]}
   */
  static getVariableOfLevel( baseObject: HasVariables, targetUri: string ): any {

    const level = PlatformService.getPathLevelFromUri(targetUri);

    // Get the unique variable fields.
    const uniqueVariables: UniqueVariable[] = baseObject.uniqueVariables
        .filter( uniqueVariable => level === uniqueVariable.pathLevel )

    // Get the generated variable fields.
    const generatorVariables: GeneratorVariable[] = baseObject.generatorVariables
        .filter( generatorVariable => level === generatorVariable.pathLevel )

    return { uniqueVariables, generatorVariables }
  }

  /**
   * Save measures to current session. Set missing state for each missing value
   *
   * @param iIndexedDb
   * @param dataEntryProject
   * @param formFields
   * @param setBusinessObjectVisit
   *
   * @return DataEntryProject
   */
  static saveFormFieldsToProject(
      iIndexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
      formFields: FormField[],
      setBusinessObjectVisit = false,
  ): AbstractProject {

    // Copy measures into the current session.
    const session$ = DataEntryService.getCurrentSession( iIndexedDb, dataEntryProject )
    session$.then( session => this.saveFormFieldsToSession( iIndexedDb, dataEntryProject, formFields, session, setBusinessObjectVisit ) )

    // Save form measures into the working database.
    const registries: FieldRegistration[] = []
    if (dataEntryProject.dataEntry.visitedBusinessObjectsURI.includes( dataEntryProject.dataEntry.currentPosition )) {
      for (const formField of formFields) {
        registries.push( new FieldRegistration( formField.uuid, formField, dataEntryProject.dataEntry.currentSessionId ) )
      }
      iIndexedDb.putAll( IDB_FIELD_REGISTRY, registries ) // Async working.
          .then()
    } else {
      for (const formField of formFields) {
        registries.push( new FieldRegistration( formField.uuid, formField, dataEntryProject.dataEntry.currentSessionId ) )
      }
      iIndexedDb.addAll( IDB_FIELD_REGISTRY, registries ) // Async working.
          .then()
    }

    return dataEntryProject
  }

  /**
   * Extract missing measure from form fields.
   *
   * @param project
   * @param formFields
   * @param mandatory
   *
   * @return FormField[]
   */
  static extractMissingMeasures( project: AbstractProject, formFields: FormField[], mandatory = false ) {
    let missingMeasures: Measure[] = []
    formFields.forEach( ( formField: FormField ) => {
      const variable = project.variablesMap.get(formField.variableUri)
      if (!mandatory || variable.mandatory && variable.active) {
        // Loop on form field measures.
        formField.measures.forEach( ( measure: Measure ) => {
          if (this.isMissingMeasure( measure )) {
            missingMeasures = [...missingMeasures, measure]
          }
        } )
        // In case of a generator variable.
        if (!!formField.generatedFields && 0 < formField.generatedFields.length) {
          formField.generatedFields.forEach( ( generatedField: GeneratedField ) => {
            missingMeasures = [
              ...missingMeasures,
              ...this.extractMissingMeasures( project, generatedField.formFields, mandatory ),
            ]
          } )
        }
      }
    } )
    return missingMeasures
  }

  static isMissingMeasure( measure: Measure ): boolean {
    return CODE_NONE === measure.state.code &&
        [null, undefined, ''].includes( measure.value as string )
  }

  /**
   * Create confirm popup data to handle AddToList event when value list does not include value.
   *
   * @param vm
   * @param variable
   * @param value
   * @param resolve
   *
   * @return AsyncConfirmPopup
   */
  static createConfirmAddToValueHintList(
      vm: FieldVariable,
      variable: UniqueVariable,
      value: string,
      resolve: ( ...args: any ) => any,
  ): AsyncConfirmPopup {
    return new AsyncConfirmPopup(
        vm.$t( 'dataEntry.form.dentryFields.fieldVariableAlphanumeric.title' )
            .toString(),
        [
          vm.$t( 'dataEntry.form.dentryFields.fieldVariableAlphanumeric.addToListMessage', { value } )
              .toString(),
        ],
        [
          new ConfirmAction(
              vm.$t( 'dataEntry.form.dentryFields.fieldVariableAlphanumeric.confirm' )
                  .toString(),
              TYPE_PRIMARY,
              () => {
                (vm.variable as UniqueVariable).valueHintList.valueHints.push(
                    new ValueHint( value ),
                )
                resolve()
              },
          ),
          new ConfirmAction(
              vm.$t( 'dataEntry.form.dentryFields.fieldVariableAlphanumeric.cancel' )
                  .toString(),
              TYPE_SECONDARY,
              resolve,
          ),
        ],
    )
  }

  /**
   * Create confirm popup data to handle AddToList event when value list does not include value.
   *
   * @param vm
   * @param variableObject
   * @param value
   *
   * @return AsyncConfirmPopup
   */
  static createConfirmOutOfScaleLimit(
      vm: FieldVariable,
      variableObject: Variable,
      value: string,
  ): AsyncConfirmPopup {
    const variable = variableObject.name
    const scale = variableObject.scale.name
    return new AsyncConfirmPopup(
        vm.$t( 'dataEntry.form.dentryFields.fieldVariableReal.title' )
            .toString(),
        [
          vm.$t( 'dataEntry.form.dentryFields.fieldVariableReal.outOfScaleLimit', { value, variable } )
              .toString(),
          variableObject.scale.exclusive
              ? vm.$t( 'dataEntry.form.dentryFields.fieldVariableReal.exclusiveScale', { scale } )
                  .toString()
              : vm.$t( 'dataEntry.form.dentryFields.fieldVariableReal.openScale', { scale } )
                  .toString(),
        ],
        [
          new ConfirmAction(
              vm.$t( 'dataEntry.form.dentryFields.fieldVariableReal.confirm' )
                  .toString(),
              TYPE_PRIMARY,
              () => {
                vm.select( true, true )
              },
          ),
        ],
    )
  }

  /**
   * Create confirm popup data to handle form validation when mandatory measure are not completed.
   *
   * @param vm
   * @param onConfirm
   *
   * @return AsyncConfirmPopup
   */
  static createConfirmValueMissing( vm: Vue, onConfirm: ( ...args: any ) => any ): AsyncConfirmPopup {
    return new AsyncConfirmPopup(
        vm.$t( 'dataEntry.form.validate.title' )
            .toString(),
        [
          vm.$t( 'dataEntry.form.validate.message' )
              .toString(),
        ],
        [
          new ConfirmAction(
              vm.$t( 'dataEntry.form.validate.confirm' )
                  .toString(),
              TYPE_PRIMARY,
              () => {
                onConfirm( true )
              },
          ),
          new ConfirmAction(
              vm.$t( 'dataEntry.form.validate.cancel' )
                  .toString(),
              TYPE_SECONDARY,
              () => {
                onConfirm( false )
              },
          ),
        ],
    )
  }

  /**
   * Confirm moving form to already visited object.
   *
   * @param vm
   * @param onConfirm
   *
   * @return AsyncConfirmPopup
   */
  static createConfirmMoveToAlreadyVisitedObject( vm: Vue, onConfirm: ( ...args: any ) => any ): AsyncConfirmPopup {
    return new AsyncConfirmPopup(
        vm.$t( 'dataEntry.form.position.ident.confirmVisitedObject.title' )
            .toString(),
        [
          vm.$t( 'dataEntry.form.position.ident.confirmVisitedObject.message' )
              .toString(),
        ],
        [
          new ConfirmAction(
              vm.$t( 'dataEntry.form.position.ident.confirmVisitedObject.confirm' )
                  .toString(),
              TYPE_PRIMARY,
              () => {
                onConfirm( true )
              },
          ),
          new ConfirmAction(
              vm.$t( 'dataEntry.form.position.ident.confirmVisitedObject.cancel' )
                  .toString(),
              TYPE_SECONDARY,
              () => {
                onConfirm( false )
              },
          ),
        ],
    )
  }

  /**
   * Build the measure report: How many measure to do and how many are done for each variable level.
   *
   * @param vm
   * @param project
   *
   * @return MeasureReportData[]
   */
  public static async buildMeasureReports( vm: Vue, project: AbstractProject ): Promise<MeasureReportData[]> {

    const dataTypes = [
      { cl: DEVICE_CLASS, txt: 'device' },
      { cl: BLOCK_CLASS, txt: 'block' },
      { cl: SUBBLOCK_CLASS, txt: 'subBlock' },
      { cl: UNITPARCEL_CLASS, txt: 'unitParcel' },
      { cl: INDIVIDUAL_CLASS, txt: 'individual' },
    ]

    const defaultReports = ( entryCount: number, annotationCount: number, objectCount: number ) => [
      {
        name: vm.$t( 'dataEntry.situation.measure.type.entries' )
            .toString(),
        type: REPORT_TYPE_MEASURE,
        measureDone: 0,
        measureTodo: entryCount,
      },
      {
        name: vm.$t( 'dataEntry.situation.measure.type.annotation' )
            .toString(),
        type: REPORT_TYPE_ANNOTATION,
        measureDone: 0,
        measureTodo: annotationCount,
      },
      {
        name: vm.$t( 'dataEntry.situation.measure.type.objects' )
            .toString(),
        type: REPORT_TYPE_OBJECT,
        measureDone: 0,
        measureTodo: objectCount,
      },
    ]

    const extractCount = ( dataCount: { cl: string, txt: string } ) => {
      const { uniqueVariables, generatorVariables } = FormMeasureService.getVariableOfLevel( project, dataCount.cl )
      const objectTodoCount = project.dataEntry.workpath.filter( w => w.includes( dataCount.cl ) ).length
      // Entries and annotations calculation.
      let entryTodoCount = 0
      let annotationTodoCount = 0
      const varsMap = new Map<string, Variable>();
      [...uniqueVariables, ...generatorVariables].forEach( variable => {
        entryTodoCount += variable.repetitions
        annotationTodoCount += 0
        varsMap.set( variable.uri, variable )
      } )
      entryTodoCount *= objectTodoCount
      return { objectTodoCount, entryTodoCount, annotationTodoCount }
    }

    const reportsMap = new Map<string, MeasureReportData>()
    dataTypes.forEach( dataType => {

      const { objectTodoCount, entryTodoCount, annotationTodoCount } = extractCount( dataType )

      reportsMap.set( dataType.cl, {
        name: vm.$t( 'dataEntry.situation.measure.level.' + dataType.txt + '.title' )
            .toString(),
        reports: defaultReports( entryTodoCount, annotationTodoCount, objectTodoCount ),
      } )
    } )

    for (const registration of (await vm.$indexedDb.getAll( IDB_FIELD_REGISTRY ))) {
      const className = PlatformService.getTypeFromUri( registration.fieldUuid )
      const reportData = reportsMap.get( className )
      ++reportData.reports[0].measureDone
    }

    for (const targetUri of project.dataEntry.visitedBusinessObjectsURI) {
      const className = PlatformService.getTypeFromUri( targetUri )
      const reportData = reportsMap.get( className )
      ++reportData.reports[2].measureDone
    }

    return Array.from( reportsMap.values() )
  }

  /**
   * get all variable of project as string.
   *
   * @param dataEntryProject
   * @param withRepetition
   * @param withPrevious
   * @param specificLevel
   *
   * @return string[]
   */
  static getVariableNames(
      dataEntryProject: DataEntryProject,
      withRepetition = true,
      withPrevious = false,
      specificLevel: PathLevelEnum = null,
  ): string[] {
    const variableNames: string[] = []
    const variablesArray: Variable[] = []
    const completeVariables = ( varContainer: HasVariables ) => {
      const localVariables = [...varContainer.uniqueVariables, ...varContainer.generatorVariables]
      localVariables
          .sort(
              ( variableA: Variable, variableB: Variable ) => {
                return variableA.order < variableB.order
                    ? -1
                    : 1
              },
          )
          .forEach(
              ( variable: Variable ) => {
                variablesArray.push( variable )
                if (variable instanceof GeneratorVariable) {
                  completeVariables( variable )
                }
              },
          )
    }
    completeVariables( dataEntryProject )
    variablesArray
        .filter( ( variable: Variable ) => {
          return !specificLevel || specificLevel === variable.pathLevel
        } )
        .forEach( ( variable: Variable ) => {
          if (withPrevious && !!variable.previousValues && 0 < variable.previousValues.length) {
            variableNames.push(
                dataEntryProject.variablesMap.get(
                    variable.previousValues[0].originVariableUid,
                ).name,
            )
          }
          if (withRepetition) {
            for (let repetition = 1; repetition <= variable.repetitions; repetition++) {
              variableNames.push( FormMeasureService.fieldLabel( variable.name, repetition, variable.repetitions ) )
            }
          } else {
            variableNames.push( variable.name )
          }
          if (variable instanceof GeneratorVariable) {
            variableNames.push( variable.generatedPrefix )
          }
        } )
    return variableNames
  }

  /**
   *
   * @param formFieldGroup
   * @param measures
   */
  static onSemiAutoMeasures( formFieldGroup: FormFieldGroup, measures: Map<string, any> ): FormFieldGroup {
    formFieldGroup.objectFields.forEach( ( field: FormField ) => {
      field.measures.forEach( ( measure: Measure ) => {
        if (measures.has( measure.variableUid )) {
          measure.value = measures.get( measure.variableUid )
        }
      } )
    } )
    return formFieldGroup
  }

  /**
   * Create the confirm popup asked when the dead state code is removed
   *
   * @param vm
   * @param onConfirm
   */
  static createConfirmReplantingPopup( vm: Vue, onConfirm: ( ...args: any ) => any ): AsyncConfirmPopup {
    return new AsyncConfirmPopup(
        vm.$t( 'dataEntry.form.state.replantingPopup.title' )
            .toString(),
        [
          vm.$t( 'dataEntry.form.state.replantingPopup.message' )
              .toString(),
        ],
        [
          new ConfirmAction(
              vm.$t( 'dataEntry.form.state.replantingPopup.confirm' )
                  .toString(),
              TYPE_PRIMARY,
              onConfirm,
              true,
          ),
          new ConfirmAction(
              vm.$t( 'dataEntry.form.state.replantingPopup.cancel' )
                  .toString(),
          ),
        ],
        'confirmReplantingPopupBody',
    )
  }

  public static validateFormFieldContent( variable: Variable, measure: Measure ): boolean {
    switch (variable.type) {
      case VariableTypeEnum.ALPHANUMERIC:
        return true
      case VariableTypeEnum.INTEGER:
        return this.validateIntField( measure )
      case VariableTypeEnum.REAL:
        return this.validateRealField( measure )
      case VariableTypeEnum.BOOLEAN:
        return true
      case VariableTypeEnum.DATE:
        return true
      case VariableTypeEnum.HOUR:
        return true
      default:
        console.error( 'unknown variable type' )
        return false
    }
  }

  /**
   * Get all form fields regarding the given target uri to propagate the state code on those.
   *
   * @param iIndexedDb
   * @param dataEntryProject
   * @param baseObject
   * @param targetUri
   * @param stateCode
   *
   * @return DataEntryProject
   */
  private static async propagateStateCodeToTargetUri(
      iIndexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
      baseObject: HasVariables,
      targetUri: string,
      stateCode: StateCode,
  ): Promise<AbstractProject> {
    let formFields = await this.getFormFieldOfLevel( iIndexedDb, dataEntryProject, baseObject, targetUri )
    formFields = this.propagateStateCodeToFormFieldsMeasures( formFields, stateCode )
    dataEntryProject = this.saveFormFieldsToProject( iIndexedDb, dataEntryProject, formFields )

    return dataEntryProject
  }

  /**
   * Get all measures throughout the form fields to assign the state code.
   *
   * @param formFields
   * @param stateCode
   *
   * @return FormField[]
   */
  private static propagateStateCodeToFormFieldsMeasures( formFields: FormField[], stateCode: StateCode ): FormField[] {
    formFields.forEach( ( formField: FormField ) => {
      formField.measures.forEach( ( measure: Measure ) => {
        measure.state = stateCode
        measure.timestamp = new Date()
        measure.value = null
      } )
      if (!!formField.generatedFields) {
        formField.generatedFields.forEach( ( generatedField: GeneratedField ) => {
          // Recursion over generated fields.
          generatedField.formFields = this.propagateStateCodeToFormFieldsMeasures( generatedField.formFields, stateCode )
        } )
      }
    } )
    return formFields
  }

  /**
   * Save measures to given session.
   *
   * @param indexedDb
   * @param dataEntryProject
   * @param formFields
   * @param session
   * @param setBusinessObjectVisit
   *
   * @return Promise<any>
   */
  public static saveFormFieldsToSession(
      indexedDb: IindexedDb,
      dataEntryProject: AbstractProject,
      formFields: FormField[],
      session: Session,
      setBusinessObjectVisit = false,
  ): Promise<any> {

    if (setBusinessObjectVisit) {
      const uris = formFields.map( field => field.targetUri )
      dataEntryProject.dataEntry.visitedBusinessObjectsURI = [...dataEntryProject.dataEntry.visitedBusinessObjectsURI, ...uris]
    }

    const stateCodeMissing = dataEntryProject.getStateCodeMissing()
    if (!!stateCodeMissing) {
      const missingMeasures = this.extractMissingMeasures(dataEntryProject, formFields )
      missingMeasures.forEach( measure => {
        measure.state = stateCodeMissing
        measure.timestamp = new Date()
      } )
    }
    formFields.map( ( formField: FormField ) => {

      session.formFields = session.formFields.filter( formFieldIn => !formFieldIn.equals( formField ) )
      session.formFields.push( formField )

    })
    return indexedDb.put( IDB_SESSIONS, session )
  }

  /**
   * Get all measures relative to a variable about a given target object throughout the data entry project.
   *
   * @param dataEntryProject
   * @param parent
   * @param variable
   * @param target
   * @param index
   *
   * @return Measure
   */
  private static getVariableMeasureFromProject(
      dataEntryProject: AbstractProject,
      parent: HasVariables,
      variable: Variable,
      target: string,
      index: number,
  ): Measure[] {
    const resultMeasures: Measure[] = []
    // Create all measure needed as new measure.
    for (let repetition = 1; repetition <= variable.repetitions; repetition++) {
      resultMeasures.push( FormMeasureService.createNewMeasureForVariable( dataEntryProject, parent, variable, target, index, repetition ) )
    }
    return resultMeasures
  }

  private static validateIntField( measure: Measure ): boolean {
    return Number.isInteger( measure.value )
  }

  private static validateRealField( measure: Measure ) {
    return Number.isFinite( measure.value )
  }

  /**
   * Add form field group to array given an object uri.
   *
   * @param wipProject
   * @param uri
   * @param formFieldGroups
   * @param vm
   */
  public static addFormFieldGroupByUri(
      wipProject: AbstractProject,
      uri: string,
      formFieldGroups: FormFieldGroup[],
      vm: Vue,
  ): Promise<void> {
    const wipObject: Evaluable = wipProject.platform.uriMap.get( uri )
    const breadcrumbData: BreadcrumbData = PlatformService.getBreadcrumbsData(
        wipProject.platform,
        wipProject.dataEntry.currentPosition,
    )
    const objectTree: HasAnnotations[] = [
      breadcrumbData.objects.individual,
      breadcrumbData.objects.parcel,
      breadcrumbData.objects.subBlock,
      breadcrumbData.objects.block,
      breadcrumbData.objects.device,
    ].filter( ( v: HasAnnotations ) => !!v )
    return FormMeasureService.getFormFieldOfLevel( vm.$indexedDb, wipProject, wipProject, wipObject.uri )
        .then( fields => {
          const ffg = new FormFieldGroup(
              wipObject as BusinessObject,
              PlatformService.objectName( wipObject, vm ),
              wipObject.uri,
              breadcrumbData,
              objectTree,
              fields,
          )
          formFieldGroups.push( ffg )
        } )
  }

}
