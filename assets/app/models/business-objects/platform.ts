import OutExperimentationZone from '@adonis/app/models/business-objects/OutExperimentationZone'
import { Evaluable } from '../evaluation-variables/evaluation'
import Device, { ApiGetDevice } from './device'

export const PLATFORM_CLASS = 'platform'

/**
 * Interface IntPlatform.
 */
export interface ApiGetPlatform {
  id: number
  name: string
  devices: ApiGetDevice[]
  creationDate: Date
  site: string
  place: string
}

/**
 * Class Platform: Main entity to describe the structure concerned by a project.
 */
class Platform implements ApiGetPlatform {
  constructor(
      public id: number,
      public name: string,
      public devices: Device[],
      public uriMap: Map<string, Evaluable>,
      public positionMap: Map<string, Evaluable>,
      public identMap: Map<string, Evaluable>,
      public zheMap: Map<string, OutExperimentationZone>,
      public creationDate: Date,
      public site: string,
      public place: string,
  ) {
  }
}

export default Platform
