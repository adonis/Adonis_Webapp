export const TYPE_DEAD = 0
export const TYPE_BAD_TREATMENT = 1

export interface ApiGetAnomaly {
  description: string
  type: number
  constatedTreatmentName: string
}

/**
 * Class Device: Describe a device used in a platform.
 */
export default class Anomaly implements ApiGetAnomaly {
  constructor(
      public description: string,
      public type: number,
      public constatedTreatmentName: string,
  ) {
  }
}
