/**
 * Interface ApiGetModality.
 */
export interface ApiGetModality {

  value: string
}

/**
 * Class Modality.
 */
class Modality implements ApiGetModality {

  constructor(
      public value: string,
  ) {
  }
}

export default Modality
