import Anomaly from '@adonis/app/models/business-objects/anomaly'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import Device from '@adonis/app/models/business-objects/device'
import Platform from '@adonis/app/models/business-objects/platform'
import Note from '@adonis/app/models/entry-notes/note'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'

/**
 * Interface ApiGetOutExperimentationZone.
 */
export interface ApiGetOutExperimentationZone extends Evaluable, BusinessObject {
  uri: string
  x: number
  y: number
  num: string
  natureZHEUri: string
}

/**
 * Class OutExperimentationZone.
 */
class OutExperimentationZone implements ApiGetOutExperimentationZone {
  constructor(
      public parent: BusinessObject,
      public uri: string,
      public x: number,
      public y: number,
      public num: string,
      public natureZHEUri: string,
      public notes: Note[],
      public polygonContour: string[],
      public center: number[],
      public anomaly: Anomaly,
  ) {
  }

  get directChildren(): BusinessObject[] {
    return []
  }

  get filteredDirectChildren(): BusinessObject[] {
    return []
  }

  get labelizedName(): string {
    return `ZHE:${this.num}`
  }

  get name(): string {
    return this.num
  }

  childrenTree(): BusinessObject[] {
    return []
  }

  removeChild( item: Evaluable, platform: Platform ) {
  }

  parentDevice(): Device {
    return this.parent.parentDevice()
  }
}

export default OutExperimentationZone
