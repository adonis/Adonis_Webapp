import Modality, { ApiGetModality } from './modality'

/**
 * Interface ApiGetFactor.
 */
export interface ApiGetFactor {
  name: string
  modalities: ApiGetModality[]
}

/**
 * Class Factor.
 */
class Factor implements ApiGetFactor {
  constructor(
      public name: string,
      public modalities: Modality[],
  ) {
  }
}

export default Factor
