/**
 * Interface ApiGetNatureZHE.
 */
export interface ApiGetNatureZHE {
  name: string
  color: string
  texture: string
  uri: string
}

/**
 * Class NatureZHE.
 */
class NatureZHE implements ApiGetNatureZHE {
  constructor(
      public name: string,
      public color: string,
      public texture: string,
      public uri: string,
  ) {
  }
}

export default NatureZHE
