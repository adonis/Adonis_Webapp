import Anomaly from '@adonis/app/models/business-objects/anomaly'
import Device from '@adonis/app/models/business-objects/device'
import OutExperimentationZone, { ApiGetOutExperimentationZone } from '@adonis/app/models/business-objects/OutExperimentationZone'
import Platform from '@adonis/app/models/business-objects/platform'
import Note from '../entry-notes/note'
import Annotation, { HasAnnotations } from '../evaluation-variables/annotation'
import { Evaluable } from '../evaluation-variables/evaluation'
import Block from './block'
import { BusinessObject } from './business-object'
import UnitParcel, { ApiGetUnitParcel } from './unit-parcel'

export const SUBBLOCK_CLASS = 'SubBlock'

/**
 * Interface IntSubBlock.
 */
export interface ApiGetSubBlock extends HasAnnotations, BusinessObject, Evaluable {
  uri: string
  name: string
  unitParcels: ApiGetUnitParcel[]
  outExperimentationZones: ApiGetOutExperimentationZone[]
}

/**
 * Class SubBlock: Describe a sub-block, regrouping unit parcels under a block.
 */
export default class SubBlock implements ApiGetSubBlock {
  constructor(
      public parent: Block,
      public uri: string,
      public name: string,
      public unitParcels: UnitParcel[],
      public annotations: Annotation[],
      public notes: Note[],
      public polygonContour: string[],
      public center: number[],
      public outExperimentationZones: OutExperimentationZone[],
      public anomaly: Anomaly,
  ) {
  }

  get filteredDirectChildren(): BusinessObject[] {
    return this.unitParcels
  }

  get directChildren(): BusinessObject[] {
    return [...this.filteredDirectChildren, ...this.outExperimentationZones]
  }

  get labelizedName(): string {
    return `SB:${this.name}`
  }

  childrenTree(): BusinessObject[] {
    let children: BusinessObject[] = []
    this.unitParcels.forEach( ( child: BusinessObject ) => {
      children = [...children, child, ...child.childrenTree()]
    } )
    return children
  }

  removeChild( item: BusinessObject, platform: Platform ) {
    item.directChildren.forEach( child => item.removeChild( child, platform ) )
    if (item instanceof UnitParcel) {
      const index = this.unitParcels.indexOf( item )
      this.unitParcels.splice( index, 1 )
      if (!this.parentDevice().individualPU) {
        platform.positionMap.delete( `${item.x},${item.y}` )
      }
    } else if (item instanceof OutExperimentationZone) {
      const index = this.outExperimentationZones.indexOf( item )
      this.outExperimentationZones.splice( index, 1 )
      platform.zheMap.delete( `${item.x},${item.y}` )
    }
  }

  parentDevice(): Device {
    return this.parent.parentDevice()
  }
}
