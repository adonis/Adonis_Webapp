import Anomaly from '@adonis/app/models/business-objects/anomaly'
import OutExperimentationZone, { ApiGetOutExperimentationZone } from '@adonis/app/models/business-objects/OutExperimentationZone'
import Platform from '@adonis/app/models/business-objects/platform'
import Note from '../entry-notes/note'
import Annotation, { HasAnnotations } from '../evaluation-variables/annotation'
import { Evaluable } from '../evaluation-variables/evaluation'
import Block, { ApiGetBlock } from './block'
import { BusinessObject } from './business-object'
import Protocol, { ApiGetProtocol } from './protocol'

export const DEVICE_CLASS = 'Device'

export interface ApiGetDevice extends HasAnnotations, BusinessObject, Evaluable {
  uri: string
  name: string
  individualPU: boolean
  blocks: ApiGetBlock[]
  protocol: ApiGetProtocol
  creationDate: Date
  validationDate: Date
  outExperimentationZones: ApiGetOutExperimentationZone[]
}

/**
 * Class Device: Describe a device used in a platform.
 */
export default class Device implements ApiGetDevice {
  constructor(
      public uri: string,
      public name: string,
      public individualPU: boolean,
      public blocks: Block[],
      public annotations: Annotation[],
      public protocol: Protocol,
      public creationDate: Date,
      public validationDate: Date,
      public notes: Note[],
      public polygonContour: string[],
      public center: number[],
      public outExperimentationZones: OutExperimentationZone[],
      public anomaly: Anomaly,
  ) {
  }

  get filteredDirectChildren(): BusinessObject[] {
    return this.blocks
  }

  get directChildren(): BusinessObject[] {
    return [...this.filteredDirectChildren, ...this.outExperimentationZones]
  }

  get labelizedName(): string {
    return `D:${this.name}`
  }

  childrenTree(): BusinessObject[] {
    let children: BusinessObject[] = []
    this.blocks.forEach( ( child: BusinessObject ) => {
      children = [...children, child, ...child.childrenTree()]
    } )
    return children
  }

  removeChild( item: BusinessObject, platform: Platform ) {
    item.directChildren.forEach( child => item.removeChild( child, platform ) )
    if (item instanceof Block) {
      const index = this.blocks.indexOf( item )
      this.blocks.splice( index, 1 )
    } else if (item instanceof OutExperimentationZone) {
      const index = this.outExperimentationZones.indexOf( item )
      this.outExperimentationZones.splice( index, 1 )
      platform.zheMap.delete( `${item.x},${item.y}` )
    }
  }

  parentDevice(): Device {
    return this
  }
}
