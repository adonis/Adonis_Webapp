import Anomaly from '@adonis/app/models/business-objects/anomaly'
import OutExperimentationZone, { ApiGetOutExperimentationZone } from '@adonis/app/models/business-objects/OutExperimentationZone'
import Platform from '@adonis/app/models/business-objects/platform'
import Note from '../entry-notes/note'
import Annotation, { HasAnnotations } from '../evaluation-variables/annotation'
import { Evaluable } from '../evaluation-variables/evaluation'
import { BusinessObject } from './business-object'
import Device from './device'
import SubBlock, { ApiGetSubBlock } from './sub-block'
import UnitParcel, { ApiGetUnitParcel } from './unit-parcel'

export const BLOCK_CLASS = 'Block'

/**
 * Interface IntBlock.
 */
export interface ApiGetBlock extends HasAnnotations, BusinessObject, Evaluable {
  uri: string
  name: string
  subBlocks: ApiGetSubBlock[]
  unitParcels: ApiGetUnitParcel[]
  outExperimentationZones: ApiGetOutExperimentationZone[]
}

/**
 * Class Block: Describe a block, regrouping sub-blocks and unit parcels under a device.
 */
export default class Block implements ApiGetBlock {
  constructor(
      public parent: Device,
      public uri: string,
      public name: string,
      public subBlocks: SubBlock[],
      public unitParcels: UnitParcel[],
      public annotations: Annotation[],
      public notes: Note[],
      public polygonContour: string[],
      public center: number[],
      public outExperimentationZones: OutExperimentationZone[],
      public anomaly: Anomaly,
  ) {
  }

  get filteredDirectChildren(): BusinessObject[] {
    return !!this.subBlocks && 0 < this.subBlocks.length
        ? this.subBlocks
        : this.unitParcels
  }

  get directChildren(): BusinessObject[] {
    return [...this.filteredDirectChildren, ...this.outExperimentationZones]
  }

  get labelizedName(): string {
    return `B:${this.name}`
  }

  childrenTree(): BusinessObject[] {
    let children: BusinessObject[] = []
    this.directChildren
        .forEach( ( child: BusinessObject ) => {
          children = [...children, child, ...child.childrenTree()]
        } )
    return children
  }

  removeChild( item: BusinessObject, platform: Platform ) {
    item.directChildren.forEach( child => item.removeChild( child, platform ) )
    if (item instanceof SubBlock) {
      const index = this.subBlocks.indexOf( item )
      this.subBlocks.splice( index, 1 )
    } else if (item instanceof UnitParcel) {
      const index = this.unitParcels.indexOf( item )
      this.unitParcels.splice( index, 1 )
      if (!this.parentDevice().individualPU) {
        platform.positionMap.delete( `${item.x},${item.y}` )
      }
    } else if (item instanceof OutExperimentationZone) {
      const index = this.outExperimentationZones.indexOf( item )
      this.outExperimentationZones.splice( index, 1 )
      platform.zheMap.delete( `${item.x},${item.y}` )
    }
  }

  parentDevice(): Device {
    return this.parent.parentDevice()
  }
}
