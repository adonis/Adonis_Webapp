import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import Device from '@adonis/app/models/business-objects/device'
import Platform from '@adonis/app/models/business-objects/platform'
import { Evaluable } from '../evaluation-variables/evaluation'

export const TREATMENT_CLASS = 'Treatment'

/**
 * Interface IntTreatment.
 */
export interface ApiGetTreatment {
  uri: string
  name: string
  shortName: string
  repetitions: number
  modalitiesValue: string[]
}

/**
 * Class Treatment.
 */
class Treatment implements Evaluable, ApiGetTreatment {
  constructor(
      public uri: string,
      public name: string,
      public shortName: string,
      public repetitions: number,
      public modalitiesValue: string[],
  ) {
  }

  get filteredDirectChildren(): Evaluable[] {
    return []
  }

  get directChildren(): Evaluable[] {
    return []
  }

  get labelizedName(): string {
    return `Trt:${this.name}`
  }

  childrenTree(): BusinessObject[] {
    return []
  }

  equals( treatment: Treatment ): boolean {
    return !!treatment && this.uri === treatment.uri
  }

  removeChild( item: Evaluable, platform: Platform ) {
  }

  parentDevice(): Device {
    return null
  }
}

export default Treatment
