/**
 * Interface IntWorkpath.
 */
export interface ApiGetWorkpath {
  id: number
  username: string
  path: string
  startEnd: boolean
}

/**
 * Class Workpath: Describe the path to follow during project data entry workflow.
 */
class Workpath implements ApiGetWorkpath {
  constructor(
      public id: number,
      public username: string,
      public path: string,
      public startEnd: boolean,
  ) {
    this.startEnd = !!this.startEnd // Ensure 'undefined' and 'null' are set as 'false'.
  }
}

export default Workpath
