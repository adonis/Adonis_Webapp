import EnumerationService from '@adonis/app/services/enumeration-service'
import Factor, { ApiGetFactor } from './factor'
import Treatment, { ApiGetTreatment } from './treatment'

export const PROTO_UNKNOWN = 'unknown'

/**
 * Interface ApiGetInterface.
 */
export interface ApiGetProtocol {
  name: string
  aim: string
  algorithm: string
  factors: ApiGetFactor[]
  treatments: ApiGetTreatment[]
  creationdate: Date
  nbIndividualsPerParcel: number | null
  creatorLogin: string
}

/**
 * Class Protocol.
 */
export default class Protocol implements ApiGetProtocol {
  constructor(
      public name: string,
      public aim: string,
      public algorithm: string,
      public factors: Factor[],
      public treatments: Treatment[],
      public creationdate: Date,
      public nbIndividualsPerParcel: number | null,
      public creatorLogin: string,
  ) {
  }

  public getAlgorithm(): string {
    return EnumerationService.convertFromFrench( this.algorithm )
  }
}
