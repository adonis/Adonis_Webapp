import Anomaly from '@adonis/app/models/business-objects/anomaly'
import Device from '@adonis/app/models/business-objects/device'
import Platform from '@adonis/app/models/business-objects/platform'
import Note from '../entry-notes/note'
import Annotation, { HasAnnotations } from '../evaluation-variables/annotation'
import { Evaluable } from '../evaluation-variables/evaluation'
import { BusinessObject } from './business-object'
import { Identifiable } from './identifiable'
import UnitParcel from './unit-parcel'

export const INDIVIDUAL_CLASS = 'Individual'

/**
 * Interface IntIndividual.
 */
export interface ApiGetIndividual extends HasAnnotations, BusinessObject, Identifiable, Evaluable {
  uri: string
  name: string
  x: number
  y: number
  stateCode: number
  aparitionDate: Date
  demiseDate: Date,
}

/**
 * Class Individual: Describe an individual. Last element in a platform structure, grouped into unit parcels.
 */
export default class Individual implements ApiGetIndividual {
  constructor(
      public parent: UnitParcel,
      public uri: string,
      public name: string,
      public x: number,
      public y: number,
      public annotations: Annotation[],
      public stateCode: number,
      public aparitionDate: Date,
      public demiseDate: Date,
      public ident: string,
      public notes: Note[],
      public polygonContour: string[],
      public center: number[],
      public anomaly: Anomaly,
  ) {
  }

  get filteredDirectChildren(): BusinessObject[] {
    return []
  }

  get directChildren(): BusinessObject[] {
    return []
  }

  get labelizedName(): string {
    return `Ind:${this.name}`
  }

  childrenTree(): BusinessObject[] {
    return []
  }

  removeChild( item: BusinessObject, platform: Platform ) {
  }

  parentDevice(): Device {
    return this.parent.parentDevice()
  }
}
