import GraphicalStructure, {ApiGetGraphicalStructure} from '@adonis/app/models/app-functionalities/graphical-structure'
import NatureZHE, {ApiGetNatureZHE} from '@adonis/app/models/business-objects/NatureZHE'
import PlatformService from '../../services/platform-service'
import DesktopUser, {ApiGetDesktopUser} from '../app-functionalities/desktop-user'
import DataEntry, {ApiGetDataEntry, ApiPostDataEntry} from '../evaluation-variables/data-entry'
import GeneratorVariable, {ApiGetGeneratorVariable} from '../evaluation-variables/generator-variable'
import Material, {ApiGetMaterial} from '../evaluation-variables/material'
import StateCode, {ApiGetStateCode, CODE_NONE, CODE_TYPE_MISSING} from '../evaluation-variables/state-code'
import UniqueVariable, {ApiGetUniqueVariable} from '../evaluation-variables/unique-variable'
import Variable, {HasVariables} from '../evaluation-variables/variable'
import Platform, {ApiGetPlatform} from './platform'
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";

export const STATUS_NEW = 'new'
export const STATUS_WIP = 'wip'
export const STATUS_DONE = 'done'
export const STATUS_SENT = 'sent'

/**
 * Interface IntDataEntryProject.
 */
export interface ApiGetAbstractProject {
    id: number
    projectId: number | string
    ownerIri: string
    name: string
    platform: ApiGetPlatform
    creatorLogin: string
    desktopUsers: ApiGetDesktopUser[],
    uniqueVariables: ApiGetUniqueVariable[]
    generatorVariables: ApiGetGeneratorVariable[]
    dataEntry: ApiGetDataEntry
    stateCodes: ApiGetStateCode[]
    categories: string[]
    keywords: string[]
    status?: string
    creationDate: Date
    materials: ApiGetMaterial[]
    naturesZHE: ApiGetNatureZHE[]
    graphicalStructure: ApiGetGraphicalStructure
}

export interface ApiPostAbstractProject extends ApiGetAbstractProject {
    dataEntry: ApiPostDataEntry
}

/**
 * Class DataEntryProject: Contains all the information required to perform a data entry.
 */
abstract class AbstractProject implements HasVariables, ApiGetAbstractProject {
    protected constructor(
        public id: number,
        public projectId: number | string,
        public ownerIri: string,
        public name: string,
        public platform: Platform,
        public creatorLogin: string,
        public desktopUsers: DesktopUser[],
        public uniqueVariables: UniqueVariable[],
        public generatorVariables: GeneratorVariable[],
        public materials: Material[],
        public dataEntry: DataEntry,
        public stateCodes: StateCode[],
        public categories: string[],
        public keywords: string[],
        public status: string,
        public creationDate: Date,
        public variablesMap: Map<string, Variable>,
        public materialsMap: Map<string, Material>,
        public connectionsMap: Map<string, any>,
        public naturesZHE: NatureZHE[],
        public graphicalStructure: GraphicalStructure,
    ) {
    }

    get creator(): DesktopUser {
        return this.desktopUsers.filter((desktopUser: DesktopUser) => {
            return desktopUser.login === this.creatorLogin
        })[0]
    }

    get isCurrentSurfaceParcel(): boolean {
        return !this.platform?.devices[0]?.individualPU
    }

    get baseVariables(): Variable[] {
        return [...this.uniqueVariables, ...this.generatorVariables]
    }

    get maxOrder(): number {
        return this.baseVariables.map((variable: Variable) => variable.order)
            .reduce((previous: number, current: number) => current > previous
                ? current
                : previous, 0)
    }

    get stateCodeValues(): number[] {
        return this.stateCodes.map((state: StateCode) => state.code)
            .filter((code: number) => code !== CODE_NONE)
    }

    getCurrentLevel(): PathLevelEnum {
        return !!this.dataEntry.currentPosition
            ? PlatformService.getPathLevelFromUri(this.dataEntry.currentPosition)
            : PathLevelEnum.EXPERIMENT
    }

    getStateCodeMissing(): StateCode {
        return this.stateCodes.filter((stateCode: StateCode) => {
            return CODE_TYPE_MISSING === stateCode.type
        })[0]
    }

    getStateCodesColorMap(): Map<number, string> {
        const colorMap = new Map<number, string>()
        this.stateCodes.forEach((state: StateCode) => {
            colorMap.set(state.code, state.color)
        })
        return colorMap
    }

    getNatureZheColorMap(): Map<string, string> {
        const colorMap = new Map<string, string>()
        this.naturesZHE.forEach((nature: NatureZHE) => {
            colorMap.set(nature.uri, nature.color)
        })
        return colorMap
    }
}

export default AbstractProject
