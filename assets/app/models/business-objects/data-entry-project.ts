import GraphicalStructure from '@adonis/app/models/app-functionalities/graphical-structure'
import AbstractProject, { ApiGetAbstractProject } from '@adonis/app/models/business-objects/abstract-project'
import NatureZHE from '@adonis/app/models/business-objects/NatureZHE'
import DesktopUser from '../app-functionalities/desktop-user'
import DataEntry from '../evaluation-variables/data-entry'
import GeneratorVariable, { ApiGetGeneratorVariable } from '../evaluation-variables/generator-variable'
import Material from '../evaluation-variables/material'
import RequiredAnnotation, { ApiGetRequiredAnnotation } from '../evaluation-variables/required-annotation'
import StateCode from '../evaluation-variables/state-code'
import UniqueVariable, { ApiGetUniqueVariable } from '../evaluation-variables/unique-variable'
import Variable from '../evaluation-variables/variable'
import Platform from './platform'
import Workpath, { ApiGetWorkpath } from './workpath'

export const STATUS_NEW = 'new'
export const STATUS_WIP = 'wip'
export const STATUS_DONE = 'done'
export const STATUS_SENT = 'sent'

/**
 * Interface IntDataEntryProject.
 */
export interface ApiGetDataEntryProject extends ApiGetAbstractProject {
    workpaths: ApiGetWorkpath[]
    connectedUniqueVariables: ApiGetUniqueVariable[]
    connectedGeneratorVariables: ApiGetGeneratorVariable[]
    requiredAnnotations: ApiGetRequiredAnnotation[]
    improvised: boolean | null
}

/**
 * Class DataEntryProject: Contains all the information required to perform a data entry.
 */
class DataEntryProject extends AbstractProject {
    constructor(
        public id: number,
        public projectId: number | string,
        public ownerIri: string,
        public name: string,
        public platform: Platform,
        public creatorLogin: string,
        public desktopUsers: DesktopUser[],
        public workpaths: Workpath[],
        public uniqueVariables: UniqueVariable[],
        public generatorVariables: GeneratorVariable[],
        public connectedUniqueVariables: UniqueVariable[],
        public connectedGeneratorVariables: GeneratorVariable[],
        public materials: Material[],
        public dataEntry: DataEntry,
        public stateCodes: StateCode[],
        public categories: string[],
        public keywords: string[],
        public status: string,
        public creationDate: Date,
        public requiredAnnotations: RequiredAnnotation[],
        public variablesMap: Map<string, Variable>,
        public materialsMap: Map<string, Material>,
        public connectionsMap: Map<string, any>,
        public naturesZHE: NatureZHE[],
        public graphicalStructure: GraphicalStructure,
        public improvised: boolean,
    ) {
        super(
            id,
            projectId,
            ownerIri,
            name,
            platform,
            creatorLogin,
            desktopUsers,
            uniqueVariables,
            generatorVariables,
            materials,
            dataEntry,
            stateCodes,
            categories,
            keywords,
            status,
            creationDate,
            variablesMap,
            materialsMap,
            connectionsMap,
            naturesZHE,
            graphicalStructure,
        )
    }

    get hasWorkpaths(): boolean {
        return 0 < this.workpaths.length
    }

    get currentWorkpath(): Workpath | null {
        return this.workpaths.find(workpath => workpath.username === this.dataEntry.currentWorkpath)
    }

}

export default DataEntryProject
