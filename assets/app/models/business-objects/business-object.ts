import Anomaly from '@adonis/app/models/business-objects/anomaly'
import Device from '@adonis/app/models/business-objects/device'
import Platform from '@adonis/app/models/business-objects/platform'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import Note from '../entry-notes/note'

export interface BusinessObject extends Evaluable {
  uri: string
  name: string
  parent?: BusinessObject
  notes: Note[]
  polygonContour: string[]
  center: number[]
  anomaly: Anomaly
  directChildren: BusinessObject[]
  filteredDirectChildren: BusinessObject[]

  removeChild( item: BusinessObject, platform: Platform ): void

  parentDevice(): Device
}
