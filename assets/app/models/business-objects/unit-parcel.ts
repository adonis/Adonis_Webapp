import Anomaly from '@adonis/app/models/business-objects/anomaly'
import Device from '@adonis/app/models/business-objects/device'
import OutExperimentationZone, { ApiGetOutExperimentationZone } from '@adonis/app/models/business-objects/OutExperimentationZone'
import Platform from '@adonis/app/models/business-objects/platform'
import Note from '../entry-notes/note'
import Annotation, { HasAnnotations } from '../evaluation-variables/annotation'
import { Evaluable } from '../evaluation-variables/evaluation'
import Block from './block'
import { BusinessObject } from './business-object'
import { Identifiable } from './identifiable'
import Individual, { ApiGetIndividual } from './individual'
import SubBlock from './sub-block'

export const UNITPARCEL_CLASS = 'UnitParcel'

/**
 * Interface IntUnitParcel.
 */
export interface ApiGetUnitParcel extends HasAnnotations, BusinessObject, Identifiable, Evaluable {
  uri: string
  name: string
  x: number
  y: number
  individuals: ApiGetIndividual[]
  treatmentName: string
  stateCode: number
  aparitionDate: Date
  demiseDate: Date
  outExperimentationZones: ApiGetOutExperimentationZone[]
}

/**
 * Class UnitParcel: Describe a parcel, which can be used as main working object in a project,
 * or as a group of individuals under a block or a sub-block.
 */
export default class UnitParcel implements ApiGetUnitParcel {

  constructor(
      public parent: Block | SubBlock,
      public uri: string,
      public name: string,
      public x: number,
      public y: number,
      public individuals: Individual[],
      public annotations: Annotation[],
      public treatmentName: string,
      public stateCode: number,
      public aparitionDate: Date,
      public demiseDate: Date,
      public ident: string,
      public notes: Note[],
      public polygonContour: string[],
      public center: number[],
      public outExperimentationZones: OutExperimentationZone[],
      public anomaly: Anomaly,
  ) {
  }

  get filteredDirectChildren(): BusinessObject[] {
    return this.individuals
  }

  get directChildren(): BusinessObject[] {
    return [...this.filteredDirectChildren, ...this.outExperimentationZones]
  }

  get labelizedName(): string {
    return `PU:${this.name}`
  }

  childrenTree(): BusinessObject[] {
    let children: BusinessObject[] = []
    children = [...children, ...this.individuals]
    return children
  }

  removeChild( item: BusinessObject, platform: Platform ) {
    if (item instanceof Individual) {
      const index = this.individuals.indexOf( item )
      this.individuals.splice( index, 1 )
      platform.positionMap.delete( `${item.x},${item.y}` )
    } else if (item instanceof OutExperimentationZone) {
      const index = this.outExperimentationZones.indexOf( item )
      this.outExperimentationZones.splice( index, 1 )
      platform.zheMap.delete( `${item.x},${item.y}` )
    }
  }

  parentDevice(): Device {
    return this.parent.parentDevice()
  }
}
