export interface NoteInterface {
  text: string
  deleted: boolean
  creatorUri: string
  creatorName: string
  creationDate: Date
}

export default class Note implements NoteInterface {
  constructor(
      public text: string,
      public deleted: boolean,
      public creatorUri: string,
      public creatorName: string,
      public creationDate: Date,
  ) {
  }
}
