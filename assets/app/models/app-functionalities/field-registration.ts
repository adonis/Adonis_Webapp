import FormField from '@adonis/app/models/evaluation-variables/form-field'

/**
 * Interface permettant l'enregistrement et la récupération des mesures d'un champ de formulaire
 * pour un objet concernant une variable donnée.
 */
export interface FieldRegistrationInterface {

  fieldUuid: string
  formField: FormField
  sessionId: string
}

/**
 * Classe permettant l'enregistrement et la récupération des mesures d'un champ de formulaire
 * pour un objet concernant une variable donnée.
 */
export class FieldRegistration implements FieldRegistrationInterface {

  constructor(
      public fieldUuid: string,
      public formField: FormField,
      public sessionId: string,
  ) {
  }

}
