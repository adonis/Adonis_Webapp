export default interface MovementAlertInterface {

  currentUri: string
  currentPosition: number[]
  lastPosition: number[]
  deltaPosition: number[]
}
