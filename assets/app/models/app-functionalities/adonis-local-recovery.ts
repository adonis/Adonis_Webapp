export class AdonisLocalRecovery {

  tables: AdonisStoreRecovery[] = []

  constructor(
      public dbVersion: number,
      public dbName: string,
  ) {
  }
}

// tslint:disable-next-line:max-classes-per-file
export class AdonisStoreRecovery {

  rows: any[] = []

  constructor(
      public name: string,
  ) {
  }
}

// tslint:disable-next-line:max-classes-per-file
export class AdonisReportRecovery {

  objectStoreRestored: string[] = []
  objectStoreError: string[] = []
}
