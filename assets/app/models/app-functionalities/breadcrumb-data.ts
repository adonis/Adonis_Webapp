import BusinessBranch, { IntBusinessBranch } from './business-branch'

/**
 * Interface IntBreadcrumbData.
 */
export interface IntBreadcrumbData {
  deviceName: string
  blockName: string
  subBlockName: string
  unitParcelName: string
  treatmentName: string
  individualName: string
  identValue: string
  positionValue: string
  objects: IntBusinessBranch
}

/**
 * Class BreadcrumbData.
 */
export default class BreadcrumbData implements IntBreadcrumbData {
  constructor(
      public deviceName: string,
      public blockName: string,
      public subBlockName: string,
      public unitParcelName: string,
      public treatmentName: string,
      public individualName: string,
      public identValue: string,
      public positionValue: string,
      public objects: BusinessBranch,
  ) {
  }
}
