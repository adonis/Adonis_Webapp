import Report from '@adonis/app/models/app-functionalities/report'

export class MeasureReportData {
  constructor(
      public name: string,
      public reports: Report[],
  ) {
  }
}

export const REPORT_TYPE_MEASURE = 'measure'
export const REPORT_TYPE_ANNOTATION = 'annotation'
export const REPORT_TYPE_OBJECT = 'object'
