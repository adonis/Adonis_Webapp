export interface NavigationItem {
  route: string | any
  icon: string
  label: string
  children: NavigationItem[]
}

export interface NavigationEvent {
  eventName: string
  eventText: string
}

export default {}
