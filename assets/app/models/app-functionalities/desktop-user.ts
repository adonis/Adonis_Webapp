/**
 * Interface IntDesktopUser.
 */
export interface ApiGetDesktopUser {
  id: number
  name: string
  firstname: string
  login: string
  password: string
  email: string

}

/**
 * Class DesktopUser: Contains information about the user defined in Adonis Bureau.
 */
class DesktopUser implements ApiGetDesktopUser {
  constructor(
      public id: number,
      public name: string,
      public firstname: string,
      public login: string,
      public password: string,
      public email: string,
  ) {
  }

}

export default DesktopUser
