import Block, { ApiGetBlock } from '../business-objects/block'
import Device, { ApiGetDevice } from '../business-objects/device'
import Individual, { ApiGetIndividual } from '../business-objects/individual'
import SubBlock, { ApiGetSubBlock } from '../business-objects/sub-block'
import UnitParcel, { ApiGetUnitParcel } from '../business-objects/unit-parcel'

/**
 * Interface IntBusinessBranch.
 */
export interface IntBusinessBranch {
  individual: ApiGetIndividual
  parcel: ApiGetUnitParcel
  subBlock: ApiGetSubBlock
  block: ApiGetBlock
  device: ApiGetDevice
}

/**
 * Class BusinessBranch.
 */
class BusinessBranch implements IntBusinessBranch {
  constructor(
      public individual: Individual,
      public parcel: UnitParcel,
      public subBlock: SubBlock,
      public block: Block,
      public device: Device,
  ) {
  }
}

export default BusinessBranch
