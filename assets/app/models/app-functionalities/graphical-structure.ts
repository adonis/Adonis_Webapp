import EnumerationService from '@adonis/app/services/enumeration-service'

export const ORIGIN_TOP_LEFT = 'top-left'
export const ORIGIN_TOP_RIGHT = 'top-right'
export const ORIGIN_BOTTOM_LEFT = 'bottom-left'
export const ORIGIN_BOTTOM_RIGHT = 'bottom-right'

export interface ApiGetGraphicalStructure {
  platformColor: string
  deviceColor: string
  blockColor: string
  subBlockColor: string
  individualUnitParcelColor: string
  surfaceUnitParcelColor: string
  individualColor: string
  abnormalIndividualColor: string
  emptyColor: string
  origin: string
  baseHeight: number
  baseWidth: number
  abnormalUnitParcelColor: string
  tooltipActive: boolean
  deviceLabel: string
  blockLabel: string
  subBlockLabel: string
  individualUnitParcelLabel: string
  surfaceUnitParcelLabel: string
  individualLabel: string
}

export default class GraphicalStructure implements ApiGetGraphicalStructure {
  constructor(
      public platformColor: string,
      public deviceColor: string,
      public blockColor: string,
      public subBlockColor: string,
      public individualUnitParcelColor: string,
      public surfaceUnitParcelColor: string,
      public individualColor: string,
      public abnormalIndividualColor: string,
      public emptyColor: string,
      public origin: string,
      public baseHeight: number,
      public baseWidth: number,
      public abnormalUnitParcelColor: string,
      public tooltipActive: boolean,
      public deviceLabel: string,
      public blockLabel: string,
      public subBlockLabel: string,
      public individualUnitParcelLabel: string,
      public surfaceUnitParcelLabel: string,
      public individualLabel: string,
  ) {
  }

  getOrigin(): string {
    return EnumerationService.convertFromFrench( this.origin )
  }
}