export default interface TreeNode {
  id: number | string
  name: string
  children?: TreeNode[]
}
