export default class Report {
  constructor(
      public name: string,
      public type: string,
      public measureDone: number,
      public measureTodo: number,
  ) {
  }
}