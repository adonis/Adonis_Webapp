export const STATUS_NEW = 'new'
export const STATUS_SYNCHRONIZED = 'synchronized'
export const STATUS_DONE = 'done'

/**
 * Interface IntProject.
 */
export interface ApiGetProject {
    id: number
    status: string
    request: {
        '@id': string,
    }
    '@id': string
    webappProject?: {
        platform?: {
            site?: {
                '@id': string,
            },
        },
    }
}

/**
 * Class Project.
 */
export default class Project {

    public resourceId: number | string

    constructor(
        public id: number,
        public status: string,
        public resourceUri: string,
        public siteIri: string,
    ) {
    }
}
