import Device, { ApiGetDevice } from '@adonis/app/models/business-objects/device'
import NatureZHE, { ApiGetNatureZHE } from '@adonis/app/models/business-objects/NatureZHE'

export interface DeviceWrapperInterface {
  natureZhes: ApiGetNatureZHE[]
  device: ApiGetDevice
}

export default class DeviceWrapper implements DeviceWrapperInterface {
  constructor(
      public natureZhes: NatureZHE[],
      public device: Device,
  ) {
  }
}
