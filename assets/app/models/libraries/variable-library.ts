import { SimpleVariableDto } from '@adonis/shared/models/dto/simple-variable-dto'

export interface VariableLibraryInterface {
  id: string
  variables: SimpleVariableDto[]
}

export default class VariableLibrary implements VariableLibraryInterface {
  constructor(
      public id: string,
      public variables: SimpleVariableDto[],
  ) {
  }
}
