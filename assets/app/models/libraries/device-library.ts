import DeviceWrapper, { DeviceWrapperInterface } from '@adonis/app/models/libraries/device-wrapper'

export interface DeviceLibraryInterface {
  id: string | number
  deviceWrappers: DeviceWrapperInterface[]
}

export default class DeviceLibrary implements DeviceLibraryInterface {
  constructor(
      public id: string | number,
      public deviceWrappers: DeviceWrapper[],
  ) {
  }
}