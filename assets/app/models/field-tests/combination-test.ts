import EnumerationService from '../../services/enumeration-service'
import Test, { ApiGetTest } from './test'

export const COMBINATION_TEST = 'CombinationTest'

/**
 * Interface IntCombinationTest.
 */
export interface ApiGetCombinationTest extends ApiGetTest {
  firstOperandUid: string
  secondOperandUid: string
  operator: string
  highLimit: number
  lowLimit: number
}

/**
 * Class CombinationTest.
 */
export default class CombinationTest extends Test implements ApiGetCombinationTest {
  constructor(
      public name: string,
      public active: boolean,
      public firstOperandUid: string,
      public secondOperandUid: string,
      public operator: string,
      public highLimit: number,
      public lowLimit: number,
  ) {
    super(
        name,
        active,
    )
  }

  getOperator(): string {
    return !!this.operator
        ? EnumerationService.convertFromFrench( this.operator )
        : null
  }

  merge( test: CombinationTest ) {
    this.firstOperandUid = test.firstOperandUid
    this.secondOperandUid = test.secondOperandUid
    this.operator = test.operator
    this.lowLimit = test.lowLimit
    this.highLimit = test.highLimit
  }
}
