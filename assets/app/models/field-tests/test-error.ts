import Measure from '../evaluation-variables/measure'
import Variable from '../evaluation-variables/variable'
import Test from './test'

/**
 * Interface IntTestError.
 */
export interface TestErrorInterface {
  test: Test
  variable: Variable
  measure: Measure
  mandatory: boolean
  otherData?: any
}

/**
 * Class TestError: Contains information about an error thrown by a test.
 */
class TestError implements TestErrorInterface {
  constructor(
      public test: Test,
      public variable: Variable,
      public measure: Measure,
      public mandatory: boolean,
      public otherData?: any,
  ) {
  }
}

export default TestError
