import Test, { ApiGetTest } from './test'

/**
 * Interface IntRangeTest.
 */
export interface ApiGetRangeTest extends ApiGetTest {
  mandatoryMinValue: number
  mandatoryMaxValue: number
  optionalMinValue: number
  optionalMaxValue: number
}

/**
 * Class RangeTest.
 */
export default class RangeTest extends Test implements ApiGetRangeTest {
  constructor(
      public name: string,
      public active: boolean,
      public mandatoryMinValue: number,
      public mandatoryMaxValue: number,
      public optionalMinValue: number,
      public optionalMaxValue: number,
  ) {
    super(
        name,
        active,
    )
  }

  merge( test: RangeTest ) {
    this.mandatoryMinValue = test.mandatoryMinValue
    this.mandatoryMaxValue = test.mandatoryMaxValue
    this.optionalMinValue = test.optionalMinValue
    this.optionalMaxValue = test.optionalMaxValue
  }
}
