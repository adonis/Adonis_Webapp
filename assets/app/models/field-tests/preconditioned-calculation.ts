import EnumerationService from '../../services/enumeration-service'
import Test, { ApiGetTest } from './test'

/**
 * Interface IntPreconditionedCalculation.
 */
export interface ApiGetPreconditionedCalculation extends ApiGetTest {
  comparisonVariableUid: string
  condition: string
  comparedValue: number
  comparedVariableUid: string
  affectationValue: string
}

/**
 * Class PreconditionedCalculation.
 */
export default class PreconditionedCalculation extends Test implements ApiGetPreconditionedCalculation {
  constructor(
      public name: string,
      public active: boolean,
      public comparisonVariableUid: string,
      public condition: string,
      public comparedValue: number,
      public comparedVariableUid: string,
      public affectationValue: string,
  ) {
    super(
        name,
        active,
    )
  }

  getCondition(): string {
    return !!this.condition
        ? EnumerationService.convertFromFrench( this.condition )
        : null
  }

  merge( test: PreconditionedCalculation ) {
    this.comparedValue = test.comparedValue
    this.comparedVariableUid = test.comparedVariableUid
    this.affectationValue = test.affectationValue
    this.condition = test.condition
    this.comparisonVariableUid = test.comparisonVariableUid
  }
}
