/**
 * Interface IntTestErrorDialog.
 */
export interface IntTestErrorDialog {
  dialog: boolean
  title: string
  variable: string
  test: string
  value: string
  messages: string[]
  confirm: string
  continueBtnLabel: string
  cancelBtnLabel: string
  onConfirm: ( ...args: any ) => any
  onCancel: ( ...args: any ) => any
  priority: boolean
}

/**
 * Class TestErrorDialog.
 */
export default class TestErrorDialog {
  dialog: boolean
  title: string
  variable: string
  test: string
  value: string
  messages: string[]
  confirm: string
  continueBtnLabel: string
  cancelBtnLabel: string
  onConfirm: ( ...args: any ) => any
  onCancel: ( ...args: any ) => any
  priority: boolean

  constructor( props: IntTestErrorDialog ) {
    Object.assign( this, props )
  }
}
