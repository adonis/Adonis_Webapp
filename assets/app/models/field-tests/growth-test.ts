import Test, { ApiGetTest } from './test'

export const GROWTH_TEST = 'GrowthTest'

/**
 * Interface IntGrowthTest.
 */
export interface ApiGetGrowthTest extends ApiGetTest {
  comparisonVariableUid: string
  minDiff: number
  maxDiff: number
}

/**
 * Class GrowthTest.
 */
export default class GrowthTest extends Test implements ApiGetGrowthTest {
  constructor(
      public name: string,
      public active: boolean,
      public comparisonVariableUid: string,
      public minDiff: number,
      public maxDiff: number,
  ) {
    super(
        name,
        active,
    )
  }

  merge( test: GrowthTest ) {
    this.comparisonVariableUid = test.comparisonVariableUid
    this.minDiff = test.minDiff
    this.maxDiff = test.maxDiff
  }
}
