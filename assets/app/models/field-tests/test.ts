export const OPERATOR_ADDITION = '+'
export const OPERATOR_SUBTRACTION = '-'
export const OPERATOR_MULTIPLICATION = '*'
export const OPERATOR_DIVISION = '/'

export const COMPARISON_EQUALS = '='
export const COMPARISON_UPPER_EQUALS = '>='
export const COMPARISON_UPPER_STRICT = '>'
export const COMPARISON_LOWER_EQUALS = '<='
export const COMPARISON_LOWER_STRICT = '<'

/**
 * Interface IntTest.
 */
export interface ApiGetTest {
  name: string
  active: boolean
}

/**
 * Abstract Class RangeTest: Common information between Tests.
 */
abstract class Test implements ApiGetTest {
  protected constructor(
      public name: string,
      public active: boolean,
  ) {
  }

  abstract merge( test: Test ): void
}

export default Test
