import CombinationTest from '../field-tests/combination-test'
import GrowthTest from '../field-tests/growth-test'
import PreconditionedCalculation from '../field-tests/preconditioned-calculation'
import RangeTest from '../field-tests/range-test'
import GeneratorVariable from './generator-variable'
import MarkList from './mark-list'
import PreviousValue from './previous-value'
import ValueHintList, { ApiGetValueHintList } from './value-hint-list'
import Variable, { ApiGetVariable } from './variable'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum";
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";
import VariableFormatEnum from "@adonis/shared/constants/variable-format-enum";

/**
 * Interface IntUniqueVariable.
 */
export interface ApiGetUniqueVariable extends ApiGetVariable {
  valueHintList: ApiGetValueHintList
}

/**
 * Class UniqueVariable: Describe independent variable to be measured during project's data entry.
 */
class UniqueVariable extends Variable implements ApiGetUniqueVariable {
  constructor(
      public name: string,
      public shortName: string,
      public repetitions: number,
      public unit: string,
      public askTimestamp: boolean,
      public pathLevel: PathLevelEnum,
      public comment: string,
      public order: number,
      public active: boolean,
      public format: VariableFormatEnum|string,
      public type: VariableTypeEnum,
      public mandatory: boolean,
      public defaultValue: string | boolean | number,
      public growthTests: GrowthTest[],
      public rangeTests: RangeTest[],
      public combinationTests: CombinationTest[],
      public preconditionedCalculations: PreconditionedCalculation[],
      public scale: MarkList,
      public previousValues: PreviousValue[],
      public creationDate: Date,
      public modificationDate: Date,
      public uri: string,
      public parent: GeneratorVariable,
      public valueHintList: ValueHintList,
      public frameStartPosition: number,
      public frameEndPosition: number,
      public materialUid: string,
  ) {
    super(
        name,
        shortName,
        repetitions,
        unit,
        askTimestamp,
        pathLevel,
        comment,
        order,
        active,
        format,
        type,
        mandatory,
        defaultValue,
        growthTests,
        rangeTests,
        combinationTests,
        preconditionedCalculations,
        scale,
        previousValues,
        creationDate,
        modificationDate,
        uri,
        parent,
        frameStartPosition,
        frameEndPosition,
        materialUid,
    )
  }

  hasAssociatedList(): boolean {
    return !!this.valueHintList
  }
}

export default UniqueVariable
