export class StatisticDataRow {
  public deviceName?: string
  public blockName?: string
  public parcelName?: string
  public treatmentName?: string
  public total?: string
  public min?: number
  public max?: number
  public average?: string
  public deviation?: string
  public data?: number[]

  constructor(
      public variableName: string,
  ) {
  }

  get uniqId(): string {
    return `${this.deviceName}-${this.blockName}-${this.parcelName}-${this.treatmentName}`
  }
}
