import PathLevelEnum from "@adonis/shared/constants/path-level-enum";

export interface ApiGetRequiredAnnotation {
  pathLevel: PathLevelEnum
  type: number
  comment: string
  active: boolean
  askWhenEntering: boolean
  buisnessObjects: string
  category: string[]
  keywords: string[]
}


export default class RequiredAnnotation implements ApiGetRequiredAnnotation {
  constructor(
      public pathLevel: PathLevelEnum,
      public type: number,
      public comment: string,
      public active: boolean,
      public askWhenEntering: boolean,
      public buisnessObjects: string,
      public category: string[],
      public keywords: string[],
  ) {
  }
}
