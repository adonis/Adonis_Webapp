/**
 * Interface ApiGetDriver.
 */
export interface ApiGetDriver {
  type: string
  timeout: number
  frameLength: number
  frameStart: string
  frameEnd: string
  csvSeparator: string
  baudrate: number
  stopbit: number
  parity: string
  flowControl: string
  push: boolean
  request: string
  databitsFormat: number
  port: string
}

/**
 * Class Driver.
 */
export default class Driver implements ApiGetDriver {
  constructor(
      public type: string,
      public timeout: number,
      public frameLength: number,
      public frameStart: string,
      public frameEnd: string,
      public csvSeparator: string,
      public baudrate: number,
      public stopbit: number,
      public parity: string,
      public flowControl: string,
      public push: boolean,
      public request: string,
      public databitsFormat: number,
      public port: string,
      public confirmed = false,
  ) {
  }
}