import FormField, { ApiGetFormField } from './form-field'

/**
 * Interface IntSession.
 */
export interface ApiGetSession {
  id: string,
  startedAt: Date
  endedAt: Date
  formFields: ApiGetFormField[]
  projectId: number | string
}

/**
 * Class Session: Measure sessions in data entry project.
 */
class Session implements ApiGetSession {
  constructor(
      public id: string,
      public startedAt: Date,
      public endedAt: Date,
      public formFields: FormField[],
      public projectId: number | string,
  ) {
  }
}

export default Session
