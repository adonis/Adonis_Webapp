/**
 * Interface IntPreviousValue.
 */

export interface ApiGetPreviousValue {
  value: string
  objectUri: string
  date: Date
  stateCode: number | null
  originVariableUid: string
}

/**
 * Class PreviousValue
 */
export default class PreviousValue implements ApiGetPreviousValue {
  constructor(
      public value: string,
      public objectUri: string,
      public date: Date,
      public stateCode: number | null,
      public originVariableUid: string,
  ) {
  }
}
