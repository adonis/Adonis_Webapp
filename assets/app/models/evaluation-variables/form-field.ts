import GeneratedField, { ApiGetGeneratedField } from './generated-form-fields'
import Measure, { ApiGetMeasure } from './measure'
import { CODE_TYPE_DEAD } from './state-code'

/**
 * Interface ApiGetFormField.
 */
export interface ApiGetFormField {

  targetUri: string,
  variableUri: string,
  measures: ApiGetMeasure[],
  generatedFields: ApiGetGeneratedField[]
}

/**
 * Class FormField.
 */
export default class FormField implements ApiGetFormField {

  uuid: string

  constructor(
      public targetUri: string,
      public variableUri: string,
      public measures: Measure[],
      public generatedFields: GeneratedField[],
  ) {
    this.uuid = `${this.targetUri}-${this.variableUri}`
  }

  equals( formField: FormField ): boolean {
    return !!formField && this.uuid === formField.uuid
  }

  isDead(): boolean {
    return !!this.measures && CODE_TYPE_DEAD === this.measures[0].state.type
  }

  clone(): FormField {
    let generatedFields: GeneratedField[] = null
    if (!!this.generatedFields) {
      generatedFields = this.generatedFields.map( ( generatedField: GeneratedField ) => {
        return generatedField.clone()
      } )
    }
    return new FormField(
        this.targetUri,
        this.variableUri,
        this.measures.map( ( measure: Measure ) => {
          return measure.clone()
        } ),
        generatedFields,
    )
  }
}
