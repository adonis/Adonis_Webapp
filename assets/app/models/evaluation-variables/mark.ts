/**
 * Interface IntMark.
 */
export interface ApiGetMark {
  text: string
  value: number
  imageName: string
  imageData: string
}

/**
 * Class Mark.
 */
class Mark implements ApiGetMark {
  constructor(
      public text: string,
      public value: number,
      public imageName: string,
      public imageData: string,
  ) {
  }
}

export default Mark
