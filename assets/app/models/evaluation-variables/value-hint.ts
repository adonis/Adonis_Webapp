/**
 * Interface IntValueHint.
 */
export interface ApiGetValueHint {
  value: string
}

/**
 * Class ValueHint.
 */
class ValueHint implements ApiGetValueHint {
  constructor(
      public value: string,
  ) {
  }
}

export default ValueHint
