export const ANNOTATION_CLASS = 'Annotation'

export interface HasAnnotations {
  annotations: ApiGetAnnotation[]
}

export interface ApiGetAnnotation {
  name: string
  type: number
  value: string
  image: string
  timestamp: Date
  categories: string[]
  keywords: string[]
}

export const ANNOTATION_TYPE_TEXT = 0
export const ANNOTATION_TYPE_IMAG = 1

/**
 * Class Annotation: Contains annotation registered by the operator during data entry.
 */
export default class Annotation implements ApiGetAnnotation {
  constructor(
      public name: string,
      public type: number,
      public value: string,
      public image: string,
      public timestamp: Date = new Date(),
      public categories: string[],
      public keywords: string[],
  ) {
  }

  clone() {
    return new Annotation(
        this.name,
        this.type,
        this.value,
        this.image,
        this.timestamp,
        this.categories.map( value => value ),
        this.keywords.map( value => value ),
    )
  }
}
