import { DEVICE_CLASS } from '../business-objects/device'

export default class StatisticFilterField {
  constructor(
      public variable: string = null,
      public integrationLevel: string = DEVICE_CLASS,
      public device: string = null,
      public block: string = null,
      public parcel: string = null,
      public treatment: string = null,
      public histogramLevel = 4,
  ) {
  }
}
