import MovementAlertInterface from '@adonis/app/models/app-functionalities/movement-alert-interface'
import Workpath, { ApiGetWorkpath } from '@adonis/app/models/business-objects/workpath'
import { ApiGetSession } from '@adonis/app/models/evaluation-variables/session'
import { SoundEnum } from '@adonis/shared/sounds/audio'
import DesktopUser from '../app-functionalities/desktop-user'

/**
 * Interface IntDataEntry.
 */
export interface ApiGetDataEntry {
  status: string
  endedAt: Date
  startedAt: Date
  sessionIDs: string[]
  currentPosition: string
  lastPosition: string
  workpath: string[]
  currentWorkpath: string
  restrictedObject: string[]
  askIdent: boolean
  doPrint: boolean
  lastFieldDoMove: boolean
  disableNumeralKeyboard: boolean
  user?: DesktopUser
  visitedBusinessObjectsURI?: string[]
  speechRecognition: boolean
  currentSessionId: string
}

export interface ApiPostDataEntry extends ApiGetDataEntry {
  sessions?: ApiGetSession[]
}

/**
 * Class DataEntry.
 */
export default class DataEntry implements ApiGetDataEntry {
  constructor(
      public status: string,
      public startedAt: Date,
      public endedAt: Date,
      public currentPosition: string,
      public lastPosition: string,
      public workpath: string[],
      public currentWorkpath: string,
      public restrictedObject: string[],
      public sessionIDs: string[],
      public askIdent: boolean,
      public doPrint: boolean,
      public lastFieldDoMove: boolean,
      public disableNumeralKeyboard: boolean,
      public user: DesktopUser,
      public visitedBusinessObjectsURI: string[],
      public speechRecognition: boolean,
      public currentSessionId: string,
      public movementAlertMap = new Map<string, MovementAlertInterface>(),
      public movementSound: SoundEnum = null,
  ) {
  }
}
