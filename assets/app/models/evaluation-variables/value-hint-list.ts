import ValueHint, { ApiGetValueHint } from './value-hint'

/**
 * Interface IntValueHintList.
 */
export interface ApiGetValueHintList {
  name: string
  valueHints: ApiGetValueHint[]
}

/**
 * Class ValueHintList.
 */
class ValueHintList implements ApiGetValueHintList {
  constructor(
      public name: string,
      public valueHints: ValueHint[],
  ) {
  }

}

export default ValueHintList
