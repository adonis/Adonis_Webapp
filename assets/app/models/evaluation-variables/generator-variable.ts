import CombinationTest from '../field-tests/combination-test'
import GrowthTest from '../field-tests/growth-test'
import PreconditionedCalculation from '../field-tests/preconditioned-calculation'
import RangeTest from '../field-tests/range-test'
import MarkList from './mark-list'
import PreviousValue from './previous-value'
import UniqueVariable, { ApiGetUniqueVariable } from './unique-variable'
import Variable, { ApiGetVariable, HasVariables } from './variable'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum";
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";
import VariableFormatEnum from "@adonis/shared/constants/variable-format-enum";

/**
 * Interface IntGeneratorVariable.
 */
export interface ApiGetGeneratorVariable extends ApiGetVariable {
  generatedPrefix: string
  numeralIncrement: boolean
  uniqueVariables: ApiGetUniqueVariable[]
  generatorVariables: ApiGetGeneratorVariable[]
}

/**
 * Class GeneratorVariable: .
 */
class GeneratorVariable extends Variable implements HasVariables {
  constructor(
      public name: string,
      public shortName: string,
      public repetitions: number,
      public unit: string,
      public askTimestamp: boolean,
      public pathLevel: PathLevelEnum,
      public comment: string,
      public order: number,
      public active: boolean,
      public format: VariableFormatEnum|string,
      public type: VariableTypeEnum,
      public mandatory: boolean,
      public defaultValue: string | boolean | number,
      public growthTests: GrowthTest[],
      public rangeTests: RangeTest[],
      public combinationTests: CombinationTest[],
      public preconditionedCalculations: PreconditionedCalculation[],
      public scale: MarkList,
      public previousValues: PreviousValue[],
      public creationDate: Date,
      public modificationDate: Date,
      public uri: string,
      public parent: GeneratorVariable,
      public generatedPrefix: string,
      public numeralIncrement: boolean,
      public uniqueVariables: UniqueVariable[],
      public generatorVariables: GeneratorVariable[],
      public frameStartPosition: number,
      public frameEndPosition: number,
      public materialUid: string,
  ) {
    super(
        name,
        shortName,
        repetitions,
        unit,
        askTimestamp,
        pathLevel,
        comment,
        order,
        active,
        format,
        type,
        mandatory,
        defaultValue,
        growthTests,
        rangeTests,
        combinationTests,
        preconditionedCalculations,
        scale,
        previousValues,
        creationDate,
        modificationDate,
        uri,
        parent,
        frameStartPosition,
        frameEndPosition,
        materialUid,
    )
  }

  get generatedVariables(): Variable[] {
    return [...this.uniqueVariables, ...this.generatorVariables]
        .sort( ( variableA: Variable, variableB: Variable ) => {
          return variableA.order < variableB.order
              ? -1
              : 1
        } )
  }

}

export default GeneratorVariable
