import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import Measure from './measure'

export interface Evaluable {
  name: string
  uri: string
  labelizedName: string

  childrenTree(): BusinessObject[]
}

class Evaluation {
  constructor(
      public evaluated: Evaluable,
      public measure: Measure,
  ) {
  }
}

export default Evaluation
