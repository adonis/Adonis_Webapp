import EnumerationService from '../../services/enumeration-service'
import CombinationTest, {ApiGetCombinationTest} from '../field-tests/combination-test'
import GrowthTest, {ApiGetGrowthTest} from '../field-tests/growth-test'
import PreconditionedCalculation, {ApiGetPreconditionedCalculation} from '../field-tests/preconditioned-calculation'
import RangeTest, {ApiGetRangeTest} from '../field-tests/range-test'
import Test from '../field-tests/test'
import GeneratorVariable from './generator-variable'
import MarkList, {ApiGetMarkList} from './mark-list'
import PreviousValue, {ApiGetPreviousValue} from './previous-value'
import UniqueVariable from './unique-variable'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum";
import PathLevelEnum from "@adonis/shared/constants/path-level-enum";
import VariableFormatEnum from "@adonis/shared/constants/variable-format-enum";

export const VDATE_FORMAT_NUM_ORDER = 'numerical-order'

/**
 * Interface HasVariables.
 */
export interface HasVariables {
  name: string
  uniqueVariables: UniqueVariable[]
  generatorVariables: GeneratorVariable[]
}

/**
 * Interface IntVariable.
 */
export interface ApiGetVariable {
  id?: number
  name: string
  shortName: string
  repetitions: number
  unit: string
  askTimestamp: boolean
  pathLevel: PathLevelEnum
  comment: string
  order: number
  active: boolean
  format: VariableFormatEnum|string
  defaultValue: string | boolean | number
  type: VariableTypeEnum
  mandatory: boolean
  growthTests: ApiGetGrowthTest[]
  rangeTests: ApiGetRangeTest[]
  combinationTests: ApiGetCombinationTest[]
  preconditionedCalculations: ApiGetPreconditionedCalculation[]
  scale: ApiGetMarkList
  previousValues: ApiGetPreviousValue[]
  creationDate: Date
  modificationDate: Date
  uri: string
  frameStartPosition: number
  frameEndPosition: number
  materialUid: string
}

/**
 * Abstract Class Variable: Common information about all types of variable.
 */
abstract class Variable implements ApiGetVariable {

  public connectedVariables: Variable[]

  protected constructor(
      public name: string,
      public shortName: string,
      public repetitions: number,
      public unit: string,
      public askTimestamp: boolean,
      public pathLevel: PathLevelEnum,
      public comment: string,
      public order: number,
      public active: boolean,
      public format: VariableFormatEnum|string,
      public type: VariableTypeEnum,
      public mandatory: boolean,
      public defaultValue: string | boolean | number,
      public growthTests: GrowthTest[],
      public rangeTests: RangeTest[],
      public combinationTests: CombinationTest[],
      public preconditionedCalculations: PreconditionedCalculation[],
      public scale: MarkList,
      public previousValues: PreviousValue[],
      public creationDate: Date,
      public modificationDate: Date,
      public uri: string,
      public parent: GeneratorVariable,
      public frameStartPosition: number,
      public frameEndPosition: number,
      public materialUid: string,
  ) {
  }

  get tests(): Test[] {
    return [...this.growthTests, ...this.rangeTests, ...this.combinationTests, ...this.preconditionedCalculations]
  }

  hasAssociatedList(): boolean {
    return false
  }

  hasAssociatedScale(): boolean {
    return !!this.scale
  }

  addTest( test: Test ) {
    if (test instanceof CombinationTest) {
      this.combinationTests.push( test )
    } else if (test instanceof GrowthTest) {
      this.growthTests.push( test )
    } else if (test instanceof RangeTest) {
      this.rangeTests.push( test )
    } else if (test instanceof PreconditionedCalculation) {
      this.preconditionedCalculations.push( test )
    }
  }

  parse( value: string ): any {
    let parsed: any
    switch (this.type) {
      case VariableTypeEnum.REAL:
        parsed = Number.parseFloat( value )
        break
      case VariableTypeEnum.INTEGER:
        parsed = Number.parseInt( value, null )
        break
      default:
        parsed = value
        break
    }
    return parsed
  }
}

export default Variable
