import Mark, { ApiGetMark } from './mark'

/**
 * Interface ApiGetMarkList.
 */
export interface ApiGetMarkList {
  name: string
  minValue: number
  maxValue: number
  marks: ApiGetMark[]
  exclusive: boolean
}

/**
 * Class MarkList.
 */
class MarkList implements ApiGetMarkList {
  constructor(
      public name: string,
      public minValue: number,
      public maxValue: number,
      public exclusive: boolean,
      public marks: Mark[],
  ) {
  }
}

export default MarkList
