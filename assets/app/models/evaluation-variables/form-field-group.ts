import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import BreadcrumbData from '../app-functionalities/breadcrumb-data'
import { HasAnnotations } from './annotation'
import FormField from './form-field'

/**
 * Class FormFieldGroup.
 */
class FormFieldGroup {

  constructor(
      public object: BusinessObject,
      public objectName: string,
      public objectUri: string,
      public breadcrumbData: BreadcrumbData,
      public objectTree: HasAnnotations[],
      public objectFields: FormField[],
  ) {
  }
}

export default FormFieldGroup
