import FormField, { ApiGetFormField } from './form-field'

/**
 * Interface ApiGetGeneratedField.
 */
export interface ApiGetGeneratedField {
  index: number
  prefix: string
  numeralIncrement: boolean
  formFields: ApiGetFormField[]
}

/**
 * Class GeneratedField
 */
export default class GeneratedField implements ApiGetGeneratedField {
  constructor(
      public index: number,
      public prefix: string,
      public numeralIncrement: boolean,
      public formFields: FormField[],
  ) {
  }

  getName(): string {
    return `${this.prefix}_${this.numeralIncrement
        ? this.index
        : String.fromCharCode( 64 + this.index )}`
  }

  clone() {
    return new GeneratedField(
        this.index,
        this.prefix,
        this.numeralIncrement,
        this.formFields.map( ( formField: FormField ) => {
          return formField.clone()
        } ),
    )
  }
}
