import Annotation, { HasAnnotations } from './annotation'
import StateCode, { ApiGetStateCode, HasStateCode, STATE_NONE } from './state-code'

/**
 * Interface IntMeasure.
 */
export interface ApiGetMeasure extends HasAnnotations {
  value: string | boolean | number
  targetUri: string
  variableUid: string
  index: number
  repetition: number
  timestamp: Date
  state: ApiGetStateCode
}

/**
 * Class Measure: Contains values of date entries.
 */
class Measure implements ApiGetMeasure, HasStateCode {
  constructor(
      public value: string | boolean | number,
      public targetUri: string,
      public variableUid: string,
      public index: number,
      public repetition: number,
      public timestamp: Date,
      public state: StateCode,
      public annotations: Annotation[],
  ) {
  }

  equals( measure: Measure ): boolean {
    return !!measure && this.uuid === measure.uuid
  }

  get uuid(): string {
    return `${this.variableUid}-${this.targetUri}-${this.repetition}-${this.index}`
  }

  /**
   * Returns TRUE if the measure's state is "NONE".
   *
   * @return boolean
   */
  isEditable(): boolean {
    return STATE_NONE.equals( this.state )
  }

  clone() {
    return new Measure(
        this.value,
        this.targetUri,
        this.variableUid,
        this.index,
        this.repetition,
        this.timestamp,
        this.state,
        this.annotations.map( ( annotation: Annotation ) => {
          return annotation.clone()
        } ),
    )
  }
}

export default Measure
