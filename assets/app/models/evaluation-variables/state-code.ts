import PathLevelEnum from "@adonis/shared/constants/path-level-enum"

/**
 * Interface HasStateCode.
 */
export interface HasStateCode {
  state: StateCode
  targetUri: string
}

/**
 * Interface IntStateCode.
 */
export interface ApiGetStateCode {
  code: number
  label: string
  type: string
  description?: string
  propagation?: PathLevelEnum
  color?: string
}

/**
 * Class StateCode: Describe the status of a given measure, or structure object.
 */
class StateCode {
  constructor(
      public code: number,
      public label: string,
      public type: string,
      public description: string = null,
      public propagation: PathLevelEnum = null,
      public color: string = null,
  ) {
  }

  equals( stateCode: StateCode ): boolean {
    return !!stateCode && stateCode.code === this.code
  }

    getPropagation(isSurfaceParcel = false): PathLevelEnum {
    let propagation = this.propagation ?? null;
    if (isSurfaceParcel && PathLevelEnum.INDIVIDUAL === propagation) {
      propagation = PathLevelEnum.UNIT_PLOT
    }
    return propagation
  }

  mustPropagate( isSurfaceParcel = false ): boolean {
      return [PathLevelEnum.UNIT_PLOT, PathLevelEnum.INDIVIDUAL].includes(this.getPropagation(isSurfaceParcel))
  }
}

export const CODE_NONE = 0
const LABEL_NONE = 'Aucun'

export const CODE_TYPE_NONE = 'none'
export const CODE_TYPE_OTHER = 'non_permanent'
export const CODE_TYPE_MISSING = 'missing'
export const CODE_TYPE_DEAD = 'dead'
export const CODE_PROPAGATION_INDIVIDUAL = 'individu'

export const STATE_NONE: StateCode = new StateCode( CODE_NONE, LABEL_NONE, CODE_TYPE_NONE )

export default StateCode
