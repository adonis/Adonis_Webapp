import Driver, { ApiGetDriver } from './driver'
import Variable from './variable'

/**
 * Interface ApiGetMaterial
 */
export interface ApiGetMaterial {
  name: string
  physicalDevice: string
  manufacturer: string
  type: string
  driver: ApiGetDriver
  uid: string
}

/**
 * Class DataEntry.
 */
export default class Material implements ApiGetMaterial {
  constructor(
      public name: string,
      public physicalDevice: string,
      public manufacturer: string,
      public type: string,
      public driver: Driver,
      public uid: string,
      public variables: Variable[],
  ) {
  }
}