import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'

export abstract class DbStoreVersionner {

  db: IDBDatabase
  transaction: IDBTransaction
  abstract targetVersion: number

  init( request: IDBOpenDBRequest ): DbStoreVersionner {
    this.db = request.result
    this.transaction = request.transaction
    return this
  }

  abstract up(): void

  protected autoRunIdbUpdate( update: Map<string, DbStoreUpdateContent> ): void {
    for (const objectStoreName of Array.from( update.keys() )) {
      const store = this.transaction.objectStore( objectStoreName )
      update.get( objectStoreName ).deleteRows
          .forEach( r => store.delete( r ) )
      update.get( objectStoreName ).updateRows
          .forEach( r => store.put( r ) )
      update.get( objectStoreName ).addRows
          .forEach( r => store.add( r ) )
    }
  }

  abstract objectStoreUpdate( previous: Map<string, any[]> ): Map<string, DbStoreUpdateContent>
}
