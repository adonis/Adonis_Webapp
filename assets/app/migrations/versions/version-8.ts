import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY, IDB_GET_TABLE_PK, IDB_SESSIONS } from '@adonis/app/plugins/vue-indexedDb'
import { v4 as uuidv4 } from 'uuid'

export class Version8 extends DbStoreVersionner {

  targetVersion = 8

  up(): void {
    const previousData = new Map<string, any[]>()
    this.db.createObjectStore( IDB_SESSIONS, { keyPath: IDB_GET_TABLE_PK(IDB_SESSIONS) } )
    const store = this.transaction.objectStore( IDB_DATA_ENTRY )
    store.getAll().onsuccess = ( ev ) => {
      const getRequest = ev.target as IDBRequest<DataEntryProjectV7[]>
      previousData.set( IDB_DATA_ENTRY, getRequest.result )
      const update = this.objectStoreUpdate( previousData )
      this.autoRunIdbUpdate( update )
    }
  }

  objectStoreUpdate( previousData: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    const newData = new Map<string, DbStoreUpdateContent>()
    const dataEntryProjects = previousData.get( IDB_DATA_ENTRY )
    const sessionAdded: SessionV8[] = []
    const dataEntryProjectsUpdated: DataEntryProjectV8[] = []
    dataEntryProjects.forEach( ( dataEntryProject: DataEntryProjectV7 ) => {
      if (!!dataEntryProject.dataEntry) {
        const sessionIDs = dataEntryProject.dataEntry.sessions.map( session => {
          const sessionNew: SessionV8 = {
            ...session,
            id: uuidv4(),
            projectId: dataEntryProject.projectId,
          }
          sessionAdded.push( sessionNew )
          return sessionNew.id
        } )
        delete dataEntryProject.dataEntry.sessions
        const dataEntryProjectUpdated: DataEntryProjectV8 = {
          ...dataEntryProject,
          dataEntry: {
            ...dataEntryProject.dataEntry,
            sessionIDs,
          },
        }
        dataEntryProjectsUpdated.push( dataEntryProjectUpdated )
      }
    } )
    newData.set( IDB_DATA_ENTRY, new DbStoreUpdateContent( [], dataEntryProjectsUpdated, [] ) )
    newData.set( IDB_SESSIONS, new DbStoreUpdateContent( sessionAdded, [], [] ) )
    return newData
  }
}

interface DataEntryProjectV7 {
  id: number | string
  projectId: number | string
  dataEntry: DataEntryV7
}

interface DataEntryProjectV8 {
  id: number | string
  projectId: number | string
  dataEntry: DataEntryV8
}

interface DataEntryV7 {
  sessions: SessionV7[]
}

interface DataEntryV8 {
  sessionIDs: string[],
}

interface SessionV7 {
  startedAt: Date
  endedAt: Date
}

interface SessionV8 {
  id: string,
  startedAt: Date
  endedAt: Date
  projectId: number | string
}
