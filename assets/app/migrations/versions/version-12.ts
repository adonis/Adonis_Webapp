import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY } from '@adonis/app/plugins/vue-indexedDb'

export class Version12 extends DbStoreVersionner {

  targetVersion = 12

  up(): void {
    const dataEntryStore = this.transaction.objectStore( IDB_DATA_ENTRY )
    dataEntryStore.getAll().onsuccess = ( ev ) => {
      const getRequest = ev.target as IDBRequest<(ProjectV11)[]>
      const previousData = new Map<string, any[]>()
      previousData.set( IDB_DATA_ENTRY, getRequest.result )
      const update = this.objectStoreUpdate( previousData )
      this.autoRunIdbUpdate( update )
    }
  }

  objectStoreUpdate( previousData: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    const newData = new Map<string, DbStoreUpdateContent>()
    newData.set( IDB_DATA_ENTRY, new DbStoreUpdateContent(
        [],
        previousData.get( IDB_DATA_ENTRY )
            .map( ( dataEntryProject: ProjectV11 ) => ({
              ...dataEntryProject,
              requiredAnnotations: dataEntryProject.requiredMetadatas,
              platform: {
                ...dataEntryProject.platform,
                devices: dataEntryProject.platform.devices.map( device => ({
                  ...device,
                  annotations: device.metadata,
                  blocks: device.blocks.map( block => ({
                    ...block,
                    annotations: block.metadata,
                    subBlocks: block.subBlocks.map( subBlock => ({
                      ...subBlock,
                      annotations: subBlock.metadata,
                      unitParcels: subBlock.unitParcels.map( unitParcel => ({
                        ...unitParcel,
                        annotations: unitParcel.metadata,
                        individuals: unitParcel.individuals.map( individual => ({
                          ...individual,
                          annotations: individual.metadata,
                        }) ),
                      }) ),
                    }) ),
                    unitParcels: block.unitParcels.map( unitParcel => ({
                      ...unitParcel,
                      annotations: unitParcel.metadata,
                      individuals: unitParcel.individuals.map( individual => ({
                        ...individual,
                        annotations: individual.metadata,
                      }) ),
                    }) ),
                  }) ),
                }) ),
              },
            }) ),
        [],
    ) )
    return newData
  }
}

interface ProjectV11 {
  requiredMetadatas: any[],
  platform: {
    devices: {
      metadata: {}[],
      blocks: {
        metadata: {}[],
        subBlocks: {
          metadata: {}[],
          unitParcels: {
            metadata: {}[],
            individuals: {
              metadata: {}[],
            }[],
          }[],
        }[]
        unitParcels: {
          metadata: {}[],
          individuals: {
            metadata: {}[],
          }[],
        }[],
      }[],
    }[],
  }
}

interface ProjectV12 {
  requiredAnnotations: any[],
  platform: {
    devices: {
      annotations: {}[],
      blocks: {
        annotations: {}[],
        subBlocks: {
          annotations: {}[],
          unitParcels: {
            annotations: {}[],
            individuals: {
              annotations: {}[],
            }[],
          }[],
        }[]
        unitParcels: {
          annotation: {}[],
          individuals: {
            annotations: {}[],
          }[],
        }[],
      }[],
    }[],
  }
}
