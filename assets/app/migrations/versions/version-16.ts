import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY, IDB_LIBRARY_VARIABLES, IDB_PROJECT, IDB_SESSIONS } from '@adonis/app/plugins/vue-indexedDb'

export class Version16 extends DbStoreVersionner {

    targetVersion = 16

    up(): void {
        const variableLib = this.transaction.objectStore(IDB_LIBRARY_VARIABLES)
        variableLib.getAll().onsuccess = (evSessions) => {
            const getProjects = evSessions.target as IDBRequest<any[]>
            const previousData = new Map<string, any[]>()
            previousData.set(IDB_LIBRARY_VARIABLES, getProjects.result.map(variable => variable.id))
            const update = this.objectStoreUpdate(previousData)
            this.autoRunIdbUpdate(update)
        }
    }

    objectStoreUpdate(previousData: Map<string, any[]>): Map<string, DbStoreUpdateContent> {
        const newData = new Map<string, DbStoreUpdateContent>()
        newData.set(IDB_LIBRARY_VARIABLES, new DbStoreUpdateContent(
            [],
            [],
            previousData.get(IDB_LIBRARY_VARIABLES),
        ))
        return newData
    }
}
