import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY } from '@adonis/app/plugins/vue-indexedDb'

export class Version11 extends DbStoreVersionner {

  targetVersion = 11

  up(): void {
    const dataEntryStore = this.transaction.objectStore( IDB_DATA_ENTRY )
    dataEntryStore.getAll().onsuccess = ( ev ) => {
      const getRequest = ev.target as IDBRequest<(ProjectV10)[]>
      const previousData = new Map<string, any[]>()
      previousData.set( IDB_DATA_ENTRY, getRequest.result )
      const update = this.objectStoreUpdate( previousData )
      this.autoRunIdbUpdate( update )
    }
  }

  objectStoreUpdate( previousData: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    const newData = new Map<string, DbStoreUpdateContent>()
    newData.set( IDB_DATA_ENTRY, new DbStoreUpdateContent(
        [],
        previousData.get( IDB_DATA_ENTRY )
            .map( ( dataEntryProject: ProjectV10 ) => ({
              ...dataEntryProject,
              platform: {
                ...dataEntryProject.platform,
                devices: dataEntryProject.platform.devices.map( device => ({
                  ...device,
                  blocks: device.blocks.map( block => ({
                    ...block,
                    subBlocks: block.subBlocks.map( subBlock => ({
                      ...subBlock,
                      unitParcels: subBlock.unitParcels.map( unitParcel => ({
                        ...unitParcel,
                        aparitionDate: new Date(),
                        demiseDate: null,
                      }) ),
                    }) ),
                    unitParcels: block.unitParcels.map( unitParcel => ({
                      ...unitParcel,
                      aparitionDate: new Date(),
                      demiseDate: null,
                    }) ),
                  }) ),
                }) ),
              },
            }) ),
        [],
    ) )
    return newData
  }
}

interface ProjectV10 {
  platform: {
    devices: {
      blocks: {
        subBlocks: {
          unitParcels: {}[],
        }[]
        unitParcels: {}[],
      }[],
    }[],
  }
}

interface ProjectV11 {
  platform: {
    devices: {
      blocks: {
        subBlocks: {
          unitParcels: {
            aparitionDate: Date,
            demiseDate: Date,
          }[],
        }[]
        unitParcels: {
          aparitionDate: Date,
          demiseDate: Date,
        }[],
      }[],
    }[],
  }
}
