import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_GET_TABLE_PK, IDB_SPACIALIZATION } from '@adonis/app/plugins/vue-indexedDb'

export class Version6 extends DbStoreVersionner {

  targetVersion = 6

  up(): void {
    this.db.createObjectStore( IDB_SPACIALIZATION, { keyPath: IDB_GET_TABLE_PK(IDB_SPACIALIZATION) } )
  }

  objectStoreUpdate( previous: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    return undefined
  }
}
