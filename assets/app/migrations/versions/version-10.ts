import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY } from '@adonis/app/plugins/vue-indexedDb'

export class Version10 extends DbStoreVersionner {

  targetVersion = 10

  up(): void {
    const previousData = new Map<string, any[]>()
    const dataEntryStore = this.transaction.objectStore( IDB_DATA_ENTRY )
    dataEntryStore.getAll().onsuccess = ( ev ) => {
      const getRequest = ev.target as IDBRequest<(ProjectV9 | ProjectV10)[]>
      previousData.set( IDB_DATA_ENTRY, getRequest.result )
      const update = this.objectStoreUpdate( previousData )
      this.autoRunIdbUpdate( update )
    }
  }

  objectStoreUpdate( previousData: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    const newData = new Map<string, DbStoreUpdateContent>()
    const dataEntryProjects: (ProjectV9 | ProjectV10)[] = previousData.get( IDB_DATA_ENTRY )
    const dataEntryProjectsUpdated = dataEntryProjects.map( dataEntryProject => {
      const project = this.isProjectToUpdate( dataEntryProject )
      if (project) {
        return {
          ...project,
          dataEntry: {
            ...project.dataEntry,
            currentWorkpath: project.dataEntry.currentWorkpath.username,
          },
          workpaths: [...project.workpaths, project.dataEntry.currentWorkpath].filter( w => !!w ),
        }
      } else {
        return dataEntryProject as ProjectV10
      }
    } )
    newData.set( IDB_DATA_ENTRY, new DbStoreUpdateContent( [], dataEntryProjectsUpdated, [] ) )
    return newData
  }

  private isProjectToUpdate( project: ProjectV10 | ProjectV9 ): ProjectV9 {
    const currentWorkpath = project?.dataEntry?.currentWorkpath
    return currentWorkpath && !(typeof currentWorkpath === 'string' || currentWorkpath instanceof String)
        ? project as ProjectV9
        : null
  }
}

interface ApiGetDataEntryV10 {
  currentWorkpath: string
}

interface ApiGetDataEntryV9 {
  currentWorkpath: WorkpathV9andV10
  projectId: number | string
}

interface ProjectV9 {
  dataEntry: ApiGetDataEntryV9
  workpaths: WorkpathV9andV10[]
}

interface ProjectV10 {
  dataEntry: ApiGetDataEntryV10
  workpaths: WorkpathV9andV10[]
}

interface WorkpathV9andV10 {
  id: number
  username: string
  path: string
  startEnd: boolean
}
