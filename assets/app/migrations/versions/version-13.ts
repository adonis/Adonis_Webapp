import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_SESSIONS } from '@adonis/app/plugins/vue-indexedDb'

export class Version13 extends DbStoreVersionner {

  targetVersion = 13

  up(): void {
    const dataEntryStore = this.transaction.objectStore( IDB_SESSIONS )
    dataEntryStore.getAll().onsuccess = ( ev ) => {
      const getRequest = ev.target as IDBRequest<(SessionV12)[]>
      const previousData = new Map<string, any[]>()
      previousData.set( IDB_SESSIONS, getRequest.result )
      const update = this.objectStoreUpdate( previousData )
      this.autoRunIdbUpdate( update )
    }
  }

  objectStoreUpdate( previousData: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    const newData = new Map<string, DbStoreUpdateContent>()
    const recusriveFormField: ( formField: FormFieldV12 ) => FormFieldV13 = ( formField: FormFieldV12 ) => ({
      ...formField,
      measures: formField.measures.map( measure => ({
        ...measure,
        annotations: measure.metadata,
      }) ),
      generatedFields: formField.generatedFields?.map( generated => ({
        ...generated,
        formFields: generated.formFields.map( genFormField => recusriveFormField( genFormField ) ),
      }) ),
    })
    newData.set( IDB_SESSIONS, new DbStoreUpdateContent(
        [],
        previousData.get( IDB_SESSIONS )
            .map( ( session: SessionV12 ) => ({
              ...session,
              formFields: session.formFields.map( formField => recusriveFormField( formField ) ),
            }) ),
        [],
    ) )
    return newData
  }
}

interface SessionV12 {
  formFields: FormFieldV12[],
}

interface FormFieldV12 {
  measures: {
    metadata: {}[],
  }[],
  generatedFields: {
    formFields: FormFieldV12[],
  }[],
}

interface SessionV13 {
  formFields: FormFieldV13[],
}

interface FormFieldV13 {
  measures: {
    annotations: {}[],
  }[],
  generatedFields: {
    formFields: FormFieldV13[],
  }[],
}
