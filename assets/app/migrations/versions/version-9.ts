import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_FIELD_REGISTRY, IDB_GET_TABLE_PK } from '@adonis/app/plugins/vue-indexedDb'

export class Version9 extends DbStoreVersionner {

  targetVersion = 9

  up(): void {
    this.db.createObjectStore( IDB_FIELD_REGISTRY, { keyPath: IDB_GET_TABLE_PK(IDB_FIELD_REGISTRY) } )
  }

  objectStoreUpdate(): Map<string, DbStoreUpdateContent> {
    return undefined
  }
}
