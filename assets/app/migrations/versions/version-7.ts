import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_GET_TABLE_PK, IDB_LIBRARY_DEVICES, IDB_LIBRARY_VARIABLES } from '../../plugins/vue-indexedDb'

export class Version7 extends DbStoreVersionner {

  targetVersion = 7

  up(): void {
    this.db.createObjectStore( IDB_LIBRARY_VARIABLES, { keyPath: IDB_GET_TABLE_PK(IDB_LIBRARY_VARIABLES) } )
    this.db.createObjectStore( IDB_LIBRARY_DEVICES, { keyPath: IDB_GET_TABLE_PK(IDB_LIBRARY_DEVICES) } )
  }

  objectStoreUpdate( previous: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    return undefined
  }
}
