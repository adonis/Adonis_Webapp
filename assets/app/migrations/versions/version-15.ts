import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY, IDB_PROJECT, IDB_SESSIONS } from '@adonis/app/plugins/vue-indexedDb'

export class Version15 extends DbStoreVersionner {

    targetVersion = 15

    up(): void {
        const projectStore = this.transaction.objectStore(IDB_DATA_ENTRY)
        projectStore.getAll().onsuccess = (evSessions) => {
            const getProjects = evSessions.target as IDBRequest<(ProjectV14)[]>
            const previousData = new Map<string, any[]>()
            previousData.set(IDB_DATA_ENTRY, getProjects.result)
            const update = this.objectStoreUpdate(previousData)
            this.autoRunIdbUpdate(update)
        }
    }

    objectStoreUpdate(previousData: Map<string, any[]>): Map<string, DbStoreUpdateContent> {
        const newData = new Map<string, DbStoreUpdateContent>()
        newData.set(IDB_DATA_ENTRY, new DbStoreUpdateContent(
            [],
            previousData.get(IDB_DATA_ENTRY)
                .filter((project: ProjectV14) => !!project.ownerUri && project.ownerUri.startsWith('/User.'))
                .map((project: ProjectV14): ProjectV15 => ({
                    ...project,
                    ownerUri: undefined,
                    ownerIri: '/api/users/' + project.ownerUri.replace('/User.', ''),
                })),
            [],
        ))
        return newData
    }
}

interface ProjectV14 {
    ownerUri: string
}

interface ProjectV15 {
    ownerIri: string
    ownerUri: void
}
