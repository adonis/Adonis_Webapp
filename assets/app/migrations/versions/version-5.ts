import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY, IDB_GET_TABLE_PK, IDB_PROJECT } from '@adonis/app/plugins/vue-indexedDb'

export class Version5 extends DbStoreVersionner {

  targetVersion = 5

  up(): void {
    const tables: Table[] = [
      { name: IDB_PROJECT, params: { keyPath: IDB_GET_TABLE_PK(IDB_PROJECT) } },
      { name: IDB_DATA_ENTRY, params: { keyPath: IDB_GET_TABLE_PK(IDB_DATA_ENTRY) } },
    ]
    tables.forEach( ( table: Table ) => {
      if (this.db.objectStoreNames.contains( table.name )) {
        this.db.deleteObjectStore( table.name )
      }
      this.db.createObjectStore( table.name, table.params )
    } )
  }

  objectStoreUpdate( previous: Map<string, any[]> ): Map<string, DbStoreUpdateContent> {
    return undefined
  }

}

interface Table {
  name: string,
  params: {
    keyPath: string,
  }
}
