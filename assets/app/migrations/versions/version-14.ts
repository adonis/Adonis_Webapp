import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY, IDB_SESSIONS } from '@adonis/app/plugins/vue-indexedDb'

export class Version14 extends DbStoreVersionner {

    targetVersion = 14

    up(): void {
        const sessionStore = this.transaction.objectStore(IDB_SESSIONS)
        const dataEntryStore = this.transaction.objectStore(IDB_DATA_ENTRY)
        sessionStore.getAll().onsuccess = (evSessions) => {
            dataEntryStore.getAll().onsuccess = (evDataEntry) => {
                const getSessions = evSessions.target as IDBRequest<(SessionV13)[]>
                const getDataEntries = evDataEntry.target as IDBRequest<(DataEntryV13)[]>
                const previousData = new Map<string, any[]>()
                previousData.set(IDB_SESSIONS, getSessions.result)
                previousData.set(IDB_DATA_ENTRY, getDataEntries.result)
                const update = this.objectStoreUpdate(previousData)
                this.autoRunIdbUpdate(update)
            }
        }
    }

    objectStoreUpdate(previousData: Map<string, any[]>): Map<string, DbStoreUpdateContent> {
        const newData = new Map<string, DbStoreUpdateContent>()
        const projectIds = previousData.get(IDB_DATA_ENTRY).map(dataEntry => dataEntry.projectId)
        newData.set(IDB_SESSIONS, new DbStoreUpdateContent(
            [],
            previousData.get(IDB_SESSIONS)
                .filter((session: SessionV13) => projectIds.includes(session.projectId))
                .map((session: SessionV13) => {
                    return ({
                        ...session,
                        formFields: session.formFields.map((formField: FormFieldV13) => ({
                            ...formField,
                            variableUri: formField.variable.uri,
                            variable: undefined,
                            variableUid: undefined,
                        })),
                    })
                }),
            previousData.get(IDB_SESSIONS)
                .filter((session: SessionV13) => !projectIds.includes(session.projectId))
                .map((session: SessionV13) => session.id),
        ))
        return newData
    }
}

interface DataEntryV13 {
    projectId: number
}

interface SessionV13 {
    id: string,
    formFields: FormFieldV13[],
    projectId: number
}

interface FormFieldV13 {
    variable: { uri: string }
    variableUid: any
}

interface SessionV14 {
    formFields: FormFieldV14[],
}

interface FormFieldV14 {
    variableUri: string
    variable: any
    variableUid: any
}
