import { DbStoreUpdateContent } from '@adonis/app/migrations/db-store-update-content'
import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { IDB_DATA_ENTRY } from '@adonis/app/plugins/vue-indexedDb'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableTypeEnum from '@adonis/shared/constants/variable-type-enum'

export class Version17 extends DbStoreVersionner {

    targetVersion = 17

    up(): void {
        const projectStore = this.transaction.objectStore(IDB_DATA_ENTRY)
        projectStore.getAll().onsuccess = (evSessions) => {
            const getProjects = evSessions.target as IDBRequest<(ProjectV16)[]>
            const previousData = new Map<string, any[]>()
            previousData.set(IDB_DATA_ENTRY, getProjects.result)
            const update = this.objectStoreUpdate(previousData)
            this.autoRunIdbUpdate(update)
        }
    }

    objectStoreUpdate(previousData: Map<string, any[]>): Map<string, DbStoreUpdateContent> {
        const newData = new Map<string, DbStoreUpdateContent>()
        newData.set(IDB_DATA_ENTRY, new DbStoreUpdateContent(
            [],
            previousData.get(IDB_DATA_ENTRY)
                .map((project: ProjectV16): ProjectV17 => ({
                    ...project,
                    requiredAnnotations: project.requiredAnnotations?.map(item => ({
                        ...item,
                        pathLevel: transformPathLevel(item.pathLevel),
                    })) ?? [],
                    uniqueVariables: project.uniqueVariables?.map(item => ({
                        ...item,
                        pathLevel: transformPathLevel(item.pathLevel),
                        type: transformvariableType(item.type),
                    })) ?? [],
                    generatorVariables: project.generatorVariables?.map(item => ({
                        ...item,
                        pathLevel: transformPathLevel(item.pathLevel),
                        type: transformvariableType(item.type),
                    })) ?? [],
                    materials: project.materials.map(mat => ({
                        ...mat,
                        variables: mat.variables.map(item => ({
                            ...item,
                            pathLevel: transformPathLevel(item.pathLevel),
                            type: transformvariableType(item.type),
                        })),
                    })),
                    stateCodes: project.stateCodes?.map(item => ({
                        ...item,
                        propagation: item.propagation ? transformPathLevel(item.propagation) : null,
                    })) ?? [],
                    connectedUniqueVariables: project.connectedUniqueVariables?.map(item => ({
                        ...item,
                        pathLevel: transformPathLevel(item.pathLevel),
                        type: transformvariableType(item.type),
                    })) ?? [],
                    connectedGeneratorVariables: project.connectedGeneratorVariables?.map(item => ({
                        ...item,
                        pathLevel: transformPathLevel(item.pathLevel),
                        type: transformvariableType(item.type),
                    })) ?? [],
                })),
            [],
        ))
        return newData
    }
}

interface ProjectV16 {
    requiredAnnotations: {
        pathLevel: string,
    }[],
    uniqueVariables: {
        pathLevel: string,
        type: string,
    }[],
    generatorVariables: {
        pathLevel: string,
        type: string,
    }[],
    materials: {
        variables: {
            pathLevel: string,
            type: string,
        }[],
    }[],
    stateCodes: {
        propagation?: string,
    }[]
    connectedUniqueVariables: {
        pathLevel: string,
        type: string,
    }[],
    connectedGeneratorVariables: {
        pathLevel: string,
        type: string,
    }[],
}

interface ProjectV17 {
    requiredAnnotations: {
        pathLevel: PathLevelEnum,
    }[]
    uniqueVariables: {
        pathLevel: PathLevelEnum,
        type: VariableTypeEnum,
    }[],
    generatorVariables: {
        pathLevel: PathLevelEnum,
        type: VariableTypeEnum,
    }[],
    materials: {
        variables: {
            pathLevel: PathLevelEnum,
            type: VariableTypeEnum,
        }[],
    }[],
    stateCodes: {
        propagation: PathLevelEnum,
    }[],
    connectedUniqueVariables: {
        pathLevel: PathLevelEnum,
        type: VariableTypeEnum,
    }[],
    connectedGeneratorVariables: {
        pathLevel: PathLevelEnum,
        type: VariableTypeEnum,
    }[],
}

function transformPathLevel(pathLevel: string): PathLevelEnum {
    switch (pathLevel) {
        case 'Individu':
        case 'individu':
        case 'individual':
            return PathLevelEnum.INDIVIDUAL
        case 'Parcelle Unitaire':
        case 'parcelle':
        case 'unitPlot':
        case 'unit_plot':
            return PathLevelEnum.UNIT_PLOT
        case 'Sous Bloc':
        case 'sousBloc':
        case 'subBlock':
            return PathLevelEnum.SUB_BLOCK
        case 'Bloc':
        case 'bloc':
        case 'block':
            return PathLevelEnum.BLOCK
        case 'Dispositif':
        case 'dispositif':
        case 'experiment':
            return PathLevelEnum.EXPERIMENT
        case 'plateforme':
        case 'plateform':
            return PathLevelEnum.PLATFORM
        default:
            throw new Error()
    }
}

function transformvariableType(variableType: string): VariableTypeEnum {
    switch (variableType) {
        case 'reel':
        case 'real':
            return VariableTypeEnum.REAL
        case 'alphanumerique':
        case 'alphanumeric':
            return VariableTypeEnum.ALPHANUMERIC
        case 'entiere':
        case 'integer':
            return VariableTypeEnum.INTEGER
        case 'booleen':
        case 'boolean':
            return VariableTypeEnum.BOOLEAN
        case 'date':
            return VariableTypeEnum.DATE
        case 'heure':
        case 'time':
            return VariableTypeEnum.HOUR
        default:
            throw new Error()
    }
}
