import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { Version10 } from '@adonis/app/migrations/versions/version-10'
import { Version11 } from '@adonis/app/migrations/versions/version-11'
import { Version12 } from '@adonis/app/migrations/versions/version-12'
import { Version13 } from '@adonis/app/migrations/versions/version-13'
import { Version14 } from '@adonis/app/migrations/versions/version-14'
import { Version15 } from '@adonis/app/migrations/versions/version-15'
import { Version16 } from '@adonis/app/migrations/versions/version-16'
import { Version17 } from '@adonis/app/migrations/versions/version-17'
import { Version5 } from '@adonis/app/migrations/versions/version-5'
import { Version6 } from '@adonis/app/migrations/versions/version-6'
import { Version7 } from '@adonis/app/migrations/versions/version-7'
import { Version8 } from '@adonis/app/migrations/versions/version-8'
import { Version9 } from '@adonis/app/migrations/versions/version-9'

export class MigrationService {

  public static getOnupgradeneeded(): ( upgradeEvent: IDBVersionChangeEvent ) => void {
    return ( event: IDBVersionChangeEvent ) => {
      const request: IDBOpenDBRequest = event.target as IDBOpenDBRequest
      MigrationService.getVersions( event.oldVersion, event.newVersion )
          .forEach( version =>
              version.init( request )
                  .up(),
          )
      console.log( `updating database from version ${event.oldVersion}` )
    }
  }

  public static getVersions( oldVersion: number, newVersion: number ): DbStoreVersionner[] {
    const versions: DbStoreVersionner[] = []
    if (oldVersion < 5 && newVersion >= 5) {
      versions.push( new Version5() )
    }
    if (oldVersion < 6 && newVersion >= 6) {
      versions.push( new Version6() )
    }
    if (oldVersion < 7 && newVersion >= 7) {
      versions.push( new Version7() )
    }
    if (oldVersion < 8 && newVersion >= 8) {
      versions.push( new Version8() )
    }
    if (oldVersion < 9 && newVersion >= 9) {
      versions.push( new Version9() )
    }
    if (oldVersion < 10 && newVersion >= 10) {
      versions.push( new Version10() )
    }
    if (oldVersion < 11 && newVersion >= 11) {
      versions.push( new Version11() )
    }
    if (oldVersion < 12 && newVersion >= 12) {
      versions.push( new Version12() )
    }
    if (oldVersion < 13 && newVersion >= 13) {
      versions.push( new Version13() )
    }
    if (oldVersion < 14 && newVersion >= 14) {
      versions.push( new Version14() )
    }
    if (oldVersion < 15 && newVersion >= 15) {
      versions.push( new Version15() )
    }
    if (oldVersion < 16 && newVersion >= 16) {
      versions.push( new Version16() )
    }
      if (oldVersion < 17 && newVersion >= 17) {
          versions.push(new Version17())
      }
    return versions
  }
}
