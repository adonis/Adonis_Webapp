export class DbStoreUpdateContent {

  constructor(
      public addRows: any[],
      public updateRows: any[],
      public deleteRows: any[],
  ) {
  }
}
