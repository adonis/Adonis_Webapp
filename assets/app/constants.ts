export const FREE_MODE_LOCK = 0
export const FREE_MODE_OPEN = 1
export const FREE_MODE_CODE = 2
export const FREE_MODE_GRAPHIC = 3
export const FREE_MODE_MODULO = 4

export const FORM_MODE_BLOCK = 0
export const FORM_MODE_TABLE = 1
export const FORM_MODE_MODULO = 2
