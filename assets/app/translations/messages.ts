import shared from '../../shared/translations/shared'
import application from './application'
import createProject from './create-project'
import dataEntry from './data-entry'
import manageProject from './manage-project'
import selectProject from './select-project'
import synchronization from './synchronization'

export default {
  en: {
    shared: shared.en,
    application: application.en,
    createProject: createProject.fr,
    selectProject: selectProject.en,
    synchronization: synchronization.en,
    manageProject: manageProject.en,
    dataEntry: dataEntry.en,
  },
  fr: {
    shared: shared.fr,
    application: application.fr,
    createProject: createProject.fr,
    selectProject: selectProject.fr,
    synchronization: synchronization.fr,
    manageProject: manageProject.fr,
    dataEntry: dataEntry.fr,
  },
}
