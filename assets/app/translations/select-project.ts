export default {
  en: {
    newDataEntry: {
      title: 'New data entry',
    },
    continueDataEntry: {
      title: 'Continue data entry',
    },
    selectProject: {
      projectFieldLabel: 'Project',
      workflowFieldLabel: 'Workflow',
      selectionInfos: {
        title: 'Information concerning selected project',
        platformName: 'Platform name',
        creator: 'Creator',
        workpath: 'Workflow',
      },
      nextActionLabel: 'Next',
      errors: {
        fieldRequired: 'This field is required',
      },
      noAvailableWorkpaths: 'Unavailable',
    },
  },
  fr: {
    newDataEntry: {
      title: 'Nouvelle saisie',
    },
    continueDataEntry: {
      title: 'Reprendre saisie',
    },
    selectProject: {
      projectFieldLabel: 'Projet',
      workflowFieldLabel: 'Cheminement',
      selectionInfos: {
        title: 'Information concernant le projet sélectionné',
        platformName: 'Plateforme',
        creator: 'Créateur',
        workpath: 'Cheminement',
      },
      nextActionLabel: 'Suivant',
      errors: {
        fieldRequired: 'Ce champ est obligatoire',
      },
      noAvailableWorkpaths: 'Indisponible',
    },
  },
}
