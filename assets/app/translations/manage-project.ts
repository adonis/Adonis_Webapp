export default {
  en: {
    title: 'Manage projects',
    main: {
      cardTitle: 'Projects and data entries management',
      cardInfo: 'On this page, you can delete imported projects and/or closed and open data entry sessions',
    },
    confirm: {
      delete: {
        title: 'Confirmation',
        message: 'Delete « {name} » ?',
        okBtnLabel: 'Delete',
        cancelBtnLabel: 'Cancel',
      },
      cancel: {
        title: 'Confirmation',
        message: 'Move project « {name} » to « {state} » ?',
        okBtnLabel: 'Move',
        cancelBtnLabel: 'Cancel',
      },
    },
    dataEntryCard: {
      title: 'Data entry projects',
      noData: 'No project',
      sessions: {
        sentence: '{start} <br/> {end} <br/> {nb} measure(s)',
        delete: 'session started on {start}',
        noDate: 'Running',
      },
    },
    databaseRecoveryCard: {
      title: 'Data backup and recovery',
      confirmRestore: {
        title: 'Restore confirm',
        message: 'Caution! This action will delete all data currently stored on the device. Data in backup file will then be restored.',
        confirmAction: '@:commons.confirm',
        cancelAction: '@:commons.cancel',
      },
      saveAction: 'Backup',
      restoreAction: 'Restore',
      importField: 'Backup file to restore',
    },
  },
  fr: {
    title: 'Gérer les projets',
    main: {
      cardTitle: 'Gestion des projets et des saisies',
      cardInfo: 'Sur cette page vous pouvez supprimer les projets de saisie importés ou bien les saisies effectuées',
    },
    confirm: {
      delete: {
        title: 'Confirmation',
        message: 'Supprimer « {name} » ?',
        okBtnLabel: 'Continuer',
        cancelBtnLabel: 'Cancel',
      },
      cancel: {
        title: 'Confirmation',
        message: 'Déplacer « {name} » vers « {state} » ?',
        okBtnLabel: 'Continuer',
        cancelBtnLabel: 'Cancel',
      },
    },
    dataEntryCard: {
      title: 'Projets de saisie',
      noData: 'Aucun projet',
      sessions: {
        sentence: 'deb.: {start} <br/> fin: {end} <br/> {nb} mesure(s)',
        delete: 'session commencée le {start}',
        noDate: 'En cours',
      },
    },
    databaseRecoveryCard: {
      title: 'Sauvegarde et restauration',
      confirmRestore: {
        title: 'Confirmation de restauration',
        message: 'Attention ! En confirmant, les données actuellement présentes sur l\'appareil seront supprimées et remplacées par les données contenues dans le fichier de sauvegarde',
        confirmAction: 'Confirmer',
        cancelAction: 'Annuler',
      },
      saveAction: 'Sauvegarder',
      restoreAction: 'Restaurer',
      importField: 'Fichier de restauration',
    },
  },
}
