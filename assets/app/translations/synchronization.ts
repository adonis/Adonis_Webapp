export default {
  en: {
    exportDesktop: {
      title: 'Export to Desktop',
      cardTitle: 'Transfert to desktop',
      cardInfo: 'Select the closed data entries that will be exported to desktop application',
      noProject: 'No closed project found',
      sendActionLabel: 'Send',
      alerts: {
        done: '{n} project{plur} exported to the server',
        error: '{n} project{plur} in error',
      },
      connectionRequired: 'Internet connection is required to perform exportation.',
    },
    importDesktop: {
      title: 'Import to Desktop',
      cardTitle: 'Transfert from desktop',
      cardInfo: 'Search for new project and experiments from your personnal online account',
      receiveActionLabel: 'Synchronize',
      alerts: {
        done: 'All projects are already imported | {count} project added | {count} projects added',
      },
      connectionRequired: 'Internet connection is required to perform synchronization.',
    },
    serverUnreachable: 'Unable to connect with online server. Please check internet connection.',
  },
  fr: {
    exportDesktop: {
      title: 'Exporter',
      cardTitle: 'Transfert vers le bureau',
      cardInfo: 'Sélectionnez les saisies terminées qui vont être exportées vers l\'application bureau',
      noProject: 'Aucun projet clôturé trouvé',
      sendActionLabel: 'Envoyer',
      alerts: {
        done: '{n} projet{plur} envoyé{plur} vers le serveur',
        error: '{n} projet{plur} en erreur',
      },
      connectionRequired: 'Pour exporter vos projets, vous devez être connecté à internet.',
    },
    importDesktop: {
      title: 'Importer',
      cardTitle: 'Transfert depuis le bureau',
      cardInfo: 'Pour rechercher les projets de saisie et les dispositifs présents sur votre espace personnel en ligne, utiliser le bouton ci-dessous :',
      receiveActionLabel: 'Synchroniser',
      alerts: {
        done: 'Tous les projets ont déjà été importés | {n} projet ajouté | {n} projets ajoutés',
      },
      connectionRequired: 'Pour synchroniser vos projets, vous devez être connecté à internet.',
    },
    serverUnreachable: 'Accès au serveur online impossible. Vérifier votre connexion internet.',
  },
}
