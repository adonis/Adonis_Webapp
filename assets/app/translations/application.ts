export default {
  en: {
    appNavigation: {
      homepage: 'Homepage',
      dataEntryProject: 'Data Entry',
      newDataEntryProject: 'New project',
      improvisedDentryProject: 'Improvised project',
      continueDataEntryProject: 'Continue project',
      export: 'Export Projects',
      import: 'Import Projects',
      manageProject: 'Manage Projects',
      connection: 'Log out',
    },
  },
  fr: {
    appNavigation: {
      homepage: 'Accueil',
      dataEntryProject: 'Saisie',
      newDataEntryProject: 'Nouvelle saisie',
      improvisedDentryProject: 'Saisie improvisée',
      continueDataEntryProject: 'Reprendre saisie',
      export: 'Exporter',
      import: 'Importer',
      manageProject: 'Gérer projets',
      connection: 'Déconnexion',
    },
  },
}
