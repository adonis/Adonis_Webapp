export default {
  en: {
    title: 'Improvised data entry',
    selectDevice: {
      title: 'Select an experiment in an existing project',
      selected: 'Selected experiment(s)',
      help: 'You must select a experiment to continue',
      fromExistingProject: {
        projectFieldLabel: 'Data entry project',
        deviceFieldLabel: 'Add experiment',
        validateBtnLabel: 'Validate',
        errors: {
          fieldRequired: 'You must select a data entry project',
        },
      },
      fromImportedDevice: {
        title: 'From an imported experiment',
        deviceFieldLabel: 'Experiment',
        errors: {
          fieldRequired: 'You must select a experiment',
        },
      },
      validateLabel: 'Validate',
    },
    information: {
      title: 'Create data entry project',
      nameFieldLabel: 'Project name',
      siteNameFieldLabel: 'Platform\'s site name',
      placeNameFieldLabel: 'Platform\'s place name',
      deadCodeLabel: 'State code for "dead"',
      deviceFieldLabel: 'Selected experiment(s)',
      errors: {
        nameIsRequired: 'Name is required',
      },
    },
    continueBtnLabel: 'Validate',
    createInProgress: 'The project is currently being created. Please wait.',
  },
  fr: {
    title: 'Saisie improvisée',
    selectDevice: {
      title: 'Choisir un dispositif depuis un projet existant',
      selected: 'Dispositif(s) choisi(s)',
      help: 'Selectionnez un dispositif pour continuer',
      fromExistingProject: {
        projectFieldLabel: 'Projet de saisie',
        deviceFieldLabel: 'Ajouter un dispositif',
        validateBtnLabel: 'Valider',
        errors: {
          fieldRequired: 'Vous devez choisir un projet de saisie',
        },
      },
      fromImportedDevice: {
        title: 'Depuis un dispositif importé',
        deviceFieldLabel: 'Dispositif',
        errors: {
          fieldRequired: 'Vous devez choisir un dispositif',
        },
      },
      validateLabel: 'Valider',
    },
    information: {
      title: 'Créer un projet de saisie',
      nameFieldLabel: 'Nom du projet',
      siteNameFieldLabel: 'Nom du site',
      placeNameFieldLabel: 'Nom du lieu',
      deadCodeLabel: 'Code d\'état pour "mort"',
      deviceFieldLabel: 'Dispositif(s) choisi(s)',
      errors: {
        nameIsRequired: 'Le champ nom est requis',
      },
    },
    continueBtnLabel: 'Terminer',
    createInProgress: 'Le projet est en cours de création. Merci de patienter.',
  },
}
