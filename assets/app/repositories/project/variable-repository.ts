import AppHttpService from '@adonis/app/services/app-http-service'
import ObjectTypeEnum from '@adonis/shared/constants/object-type-enum'
import { SimpleVariableDto } from '@adonis/shared/models/dto/simple-variable-dto'

export default class VariableRepository {

    constructor(private httpService: AppHttpService) {
    }

    findAllSimpleVariables(params: any = {}) {
        return this.httpService.getAll<SimpleVariableDto>(this.httpService.getEndpointFromType(ObjectTypeEnum.SIMPLE_VARIABLE), {pagination: false, ...params})
            .then(variables => variables.data['hydra:member'])
    }
}
