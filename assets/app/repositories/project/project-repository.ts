import Project, { ApiGetProject, STATUS_SYNCHRONIZED, } from '@adonis/app/models/app-functionalities/project'
import AbstractProject, { ApiPostAbstractProject } from '@adonis/app/models/business-objects/abstract-project'
import DataEntryProject, { ApiGetDataEntryProject } from '@adonis/app/models/business-objects/data-entry-project'
import DataEntry from '@adonis/app/models/evaluation-variables/data-entry'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import Session from '@adonis/app/models/evaluation-variables/session'
import AppHttpService from '@adonis/app/services/app-http-service'
import StructureService from '@adonis/app/services/structure-service'
import { API_COMMUNICATION_FORMAT } from '@adonis/shared/constants'
import { BASE_URL } from '@adonis/shared/env'
import axios, { AxiosError, AxiosResponse } from 'axios'

export default class ProjectRepository {

    static axiosInst = axios.create({baseURL: BASE_URL})

    constructor(private httpService: AppHttpService) {
    }

    findProjects(userIri: string): Promise<Project[]> {
        return this.httpService.getAll<ApiGetProject>(
            'api/status_data_entries',
            {
                pagination: false,
                user: userIri,
                syncable: true,
                'request.benchmark': false,
                groups: ['status_project_mobile_view', 'id_read'],
            }, // Disable pagination client-side with api-platform.
        )
            .then((response: AxiosResponse) => {
                return (response.data['hydra:member']).map((apiProject: ApiGetProject) => {
                    return new Project(
                        apiProject.id,
                        apiProject.status,
                        apiProject.request['@id'],
                        apiProject?.webappProject?.platform?.site?.['@id'],
                    )
                })
            })
    }

    async findBenchmarkProject(token: string, userIri: string, statusIri: string): Promise<DataEntryProject> {
        const headers = {'Authorization': token}
        const axiosResponse = await this.httpService.get<any>(
            statusIri,
            {groups: ['status_project_mobile_view', 'id_read']}, // Disable pagination client-side with api-platform.
        )
        const requestIri = axiosResponse.data.request['@id']
        const projectResponse = await axios.get<ApiGetDataEntryProject>(requestIri, {headers})
        await this.httpService.delete(statusIri, {headers})
        return StructureService.buildDataEntryProject(projectResponse.data)
    }

    findDataEntryProjectResource(project: Project): Promise<DataEntryProject> {
        return this.httpService.get<ApiGetDataEntryProject>(project.resourceUri)
            .then((response: AxiosResponse) => {
                this.httpService.patch(
                    'api/status_data_entries/' + project.id,
                    {status: STATUS_SYNCHRONIZED},
                )
                return StructureService.buildDataEntryProject(response.data, project.id)
            })
    }

    static sendProject(token: string, abstractProject: AbstractProject, sessions: Session[]): Promise<void> {
        const headers = {
            'Authorization': token,
            'accept': API_COMMUNICATION_FORMAT,
            'Content-Type': API_COMMUNICATION_FORMAT,
        }
        const obj: ApiPostAbstractProject = abstractProject
        obj.dataEntry.sessions = sessions
        const data = JSON.stringify(obj, function (key: string, value: any) {
            if (key === 'parent' || key === 'uuid' || key === 'scale' || key === 'center' || key === 'polygonContour') {
                return
            }
            if (key === 'previousValues') {
                return []
            }
            if (this instanceof Measure) {
                if (key === 'targetUri' || key === 'variableUid') {
                    return
                }
                if (key === 'value' && value !== null) {
                    return value + ''
                }
                if (key === 'state') {
                    return {code: value.code}
                }
            }
            // never used on the server
            if (this instanceof DataEntry) {
                if (key === 'visitedBusinessObjectsURI' || key === 'workpath' || key === 'restrictedObject') {
                    return []
                }
            }
            return value
        })
        return new Promise<void>((resolve: (...args: any) => void, reject: (...args: any) => void) => {
            this.axiosInst.post('api/data_entry_projects',
                data,
                {headers},
            )
                .then(() => {
                    resolve()
                })

                .catch((error: AxiosError) => {
                    console.log(error)
                    if (!!error.response) {
                        reject(error.response.status)
                    } else {
                        reject(500)
                    }
                })
        })
    }
}
