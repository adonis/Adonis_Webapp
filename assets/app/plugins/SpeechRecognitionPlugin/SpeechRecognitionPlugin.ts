import _Vue from 'vue'
import { PluginFunction } from 'vue/types/plugin'

export class SpeechRecognitionHolder {
  private currentSpeechRecognizer: SpeechRecognition
  private pState = new _Vue( { data: { running: false } } )

  isAvailable(): boolean {
    return typeof window.SpeechRecognition !== 'undefined'
  }

  get state() {
    return this.pState.$data
  }

  startRecognition(
      lang: string,
      onResult: ( event: SpeechRecognitionEvent ) => void,
      onEnd: ( event: Event ) => void = null,
  ) {
    // Don't try to make it continuous before chrome for android allow it
    this.currentSpeechRecognizer = new SpeechRecognition()
    this.currentSpeechRecognizer.lang = lang
    this.currentSpeechRecognizer.onresult = onResult
    this.currentSpeechRecognizer.maxAlternatives = 1
    this.currentSpeechRecognizer.interimResults = true
    if (onEnd !== null) {
      this.currentSpeechRecognizer.onend = onEnd
    }
    this.currentSpeechRecognizer.onerror = ( event ) => {
      console.error( event )
    }
    this.currentSpeechRecognizer.start()
    this.pState.$data.running = true
  }

  stopRecognition() {
    if (!!this.currentSpeechRecognizer) {
      this.currentSpeechRecognizer.stop()
      this.pState.$data.running = false
    }
  }
}

declare type SpeechRecognitionOptions = {}

const SpeechRecognitionPlugin: PluginFunction<SpeechRecognitionOptions> = (
    Vue: typeof _Vue,
    options?: SpeechRecognitionOptions,
) => {
  // @ts-ignore
  window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
  // @ts-ignore
  window.SpeechGrammarList = window.SpeechGrammarList || window.webkitSpeechGrammarList
  Vue.prototype.$speechRecognition = new SpeechRecognitionHolder()
}

declare module 'vue/types/vue' {
  interface Vue {
    $speechRecognition: SpeechRecognitionHolder
  }
}

export default SpeechRecognitionPlugin
