import { ROUTE_CONNECT } from '@adonis/shared/router/routes-config'
import Vue from 'vue'
import VueRouter from 'vue-router'
import { RouteConfig } from 'vue-router/types/router'

Vue.use( VueRouter )

export const ROUTE_HOME = 'home'
export const ROUTE_BENCHMARK = 'benchmark'
export const ROUTE_NEW_DATA_ENTRY_PROJECT = 'newDataEntry'
export const ROUTE_CONTINUE_DATA_ENTRY_PROJECT = 'continueDataEntry'
export const ROUTE_IMPROVISED_DATA_ENTRY_PROJECT = 'newImprovisedDataEntry'
export const ROUTE_EXPORT = 'export'
export const ROUTE_IMPORT = 'import'
export const ROUTE_MANAGE_PROJECTS = 'manageProjects'
export const ROUTE_DATA_ENTRY = 'data-entry'
export const ROUTE_DATA_ENTRY__RUNNING = 'data-entry__progress'
export const ROUTE_DATA_ENTRY__VISUALISATION = 'data-entry__visualisation'
export const ROUTE_DATA_ENTRY__SITUATION = 'data-entry__situation'
export const ROUTE_DATA_ENTRY__STATISTIC = 'data-entry__statistics'
export const ROUTE_DATA_ENTRY__PARAMETERS = 'data-entry__parameters'
export const ROUTE_DATA_ENTRY__GRAPHIC = 'data-entry__plot'

const routes: RouteConfig[] = [
  {
    name: ROUTE_HOME,
    path: '/home/',
    component: () => import('@adonis/app/modules/application/AdoApplicationHomepage.vue'),
  },
  {
    name: ROUTE_BENCHMARK,
    path: '/benchmark/',
    component: () => import('@adonis/app/modules/benchmark/AdoBenchmark.vue'),
    props: route => ({
      statusIri: route.query.target,
    }),
  },
  {
    name: ROUTE_NEW_DATA_ENTRY_PROJECT,
    path: '/saisie/nouvelle/',
    component: () => import('@adonis/app/modules/select-project/AdoSelectProjectNewEntry.vue'),
  },
  {
    name: ROUTE_CONTINUE_DATA_ENTRY_PROJECT,
    path: '/saisie/reprendre/',
    component: () => import('@adonis/app/modules/select-project/AdoSelectProjectContinueEntry.vue'),
  },
  {
    name: ROUTE_IMPROVISED_DATA_ENTRY_PROJECT,
    path: '/saisie/improvisee/',
    component: () => import('@adonis/app/modules/create-project/AdoCreateProjectDataEntry.vue'),
  },
  {
    name: ROUTE_EXPORT,
    path: '/transfert/exporter/',
    component: () => import('@adonis/app/modules/synchronization/AdoExportDesktop.vue'),
  },
  {
    name: ROUTE_IMPORT,
    path: '/transfert/importer/',
    component: () => import('@adonis/app/modules/synchronization/AdoImportDesktop.vue'),
  },
  {
    name: ROUTE_MANAGE_PROJECTS,
    path: '/saisie/gerer-projets/',
    component: () => import('@adonis/app/modules/manage-project/AdoManageProject.vue'),
  },
  {
    name: ROUTE_DATA_ENTRY,
    path: '/projet/',
    component: () => import('@adonis/app/modules/data-entry/AdoDataEntry.vue'),
    redirect: to => {
      return {
        name: ROUTE_DATA_ENTRY__RUNNING,
        query: to.query,
      }
    },
    props: route => ({
      ...route.params,
      benchmark: route.query.benchmark,
    }),
    children: [
      {
        name: ROUTE_DATA_ENTRY__RUNNING,
        path: 'en-cours',
        component: () => import('@adonis/app/modules/data-entry/form/AdoForm.vue'),
        props: route => ({
          ...route.params,
          benchmark: route.query.benchmark,
        }),
      },
      {
        name: ROUTE_DATA_ENTRY__VISUALISATION,
        path: 'visualisation',
        component: () => import('@adonis/app/modules/data-entry/visualization/AdoDentryVisualization.vue'),
      },
      {
        name: ROUTE_DATA_ENTRY__SITUATION,
        path: 'situation',
        component: () => import('@adonis/app/modules/data-entry/situation/AdoDentrySituation.vue'),
      },
      {
        name: ROUTE_DATA_ENTRY__STATISTIC,
        path: 'statistics',
        component: () => import('@adonis/app/modules/data-entry/statistic/AdoDentryStatistic.vue'),
      },
      {
        name: ROUTE_DATA_ENTRY__PARAMETERS,
        path: 'parametres',
        component: () => import('@adonis/app/modules/data-entry/parameter/AdoParameter.vue'),
      },
      {
        name: ROUTE_DATA_ENTRY__GRAPHIC,
        path: 'graphique',
        component: () => import('@adonis/app/modules/data-entry/graphic/AdoGraphic.vue'),
      },
      {
        name: 'dataEntry_default',
        path: '*',
        redirect: 'en-cours',
      },
    ],
  },
  {
    name: ROUTE_CONNECT,
    path: '/connect/',
    component: () => import('../../shared/components/connection/AdoConnection.vue'),
    props: { application: 'Mobile' },
  },
  {
    name: 'default',
    path: '/*',
    redirect: { name: ROUTE_HOME },
  },
]

export default new VueRouter( {
  routes,
} )
