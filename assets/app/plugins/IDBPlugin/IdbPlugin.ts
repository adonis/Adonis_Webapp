import { AdonisLocalRecovery, AdonisReportRecovery } from '@adonis/app/models/app-functionalities/adonis-local-recovery'
import _Vue from 'vue'
import { PluginFunction } from 'vue/types/plugin'
import indexedDb from './database'

export declare interface IindexeddbTableParam {
  keyPath: string
}

export declare interface IindexedDbTable {
  name: string
  params: IindexeddbTableParam
}

export declare interface IindexedDb {
  databaseVersion: number
  dbName: string
  tables: IindexedDbTable[]
  db: IDBDatabase

  init(): Promise<any>

  count( tableName: string ): Promise<any>

  add( tableName: string, object: any ): Promise<any>

  addAll( tableName: string, objects: any[] ): Promise<any>

  delete( tableName: string, primaryKey: any ): Promise<any>

  get( tableName: string, primaryKey: any ): Promise<any>

  getAll( tableName: string ): Promise<any>

  clear( tableName: string ): Promise<any>

  put( tableName: string, object: any ): Promise<any>

  putAll( tableName: string, object: any[] ): Promise<any>

  backup(): Promise<AdonisLocalRecovery>

  restore( jsonData: AdonisLocalRecovery): Promise<AdonisReportRecovery>

}

declare type IdbPluginOptions = {
  databaseVersion?: number;
  dbName?: string;
}

const IdbPlugin: PluginFunction<IdbPluginOptions> = async ( Vue: typeof _Vue, options?: IdbPluginOptions ) => {
  indexedDb.databaseVersion = options.databaseVersion || 1
  indexedDb.dbName = options.dbName || 'defaultDbName'
  await indexedDb.init()
  Vue.prototype.$indexedDb = indexedDb
}

declare module 'vue/types/vue' {
  interface Vue {
    $indexedDb: IindexedDb
  }
}

export default IdbPlugin
