import { AdonisLocalRecovery } from '@adonis/app/models/app-functionalities/adonis-local-recovery'
import { IDB_GET_TABLE_PK, IDB_TABLES_NAMES } from '@adonis/app/plugins/vue-indexedDb'
import { IindexedDb } from './IdbPlugin'

if (!window.indexedDB) {
  window.alert( 'Votre navigateur ne supporte pas une version stable d\'IndexedDB. Quelques fonctionnalités ne seront pas disponibles.' )
}

const objectMap = new Map<string, any[]>()
/**
 * Functions will return promises. To get the value of a promise, you must add handlers on the promise instance (then,
 * catch, finally).
 */
export default {

  databaseVersion: null,

  dbName: null,

  db: null,

  init() {
    IDB_TABLES_NAMES.forEach(tableName => objectMap.set( tableName, [] ) )
    return Promise.resolve()
  },

  count( tableName: string ) {
    return Promise.resolve( objectMap.get( tableName ).length )
  },

  add( tableName: string, object: any ) {
    objectMap.get( tableName )
        .push( object )
    return Promise.resolve()
  },

  addAll( tableName: string, objects: any[] ) {
    objectMap.get( tableName )
        .push( ...objects )
    return Promise.resolve()
  },

  delete( tableName: string, primaryKey: any ) {
    objectMap.set( tableName, objectMap.get( tableName )
        .filter( object => object[IDB_GET_TABLE_PK( tableName )] !== primaryKey ) )
    return Promise.resolve()
  },

  get( tableName: string, primaryKey: any ) {
    return Promise.resolve( objectMap.get( tableName )
        .find( object => object[IDB_GET_TABLE_PK( tableName )] === primaryKey ) )
  },

  getAll( tableName: string ) {
    return Promise.resolve( objectMap.get( tableName ) )
  },

  clear( tableName: string ) {
    objectMap.set( tableName, [] )
    return Promise.resolve()
  },

  put( tableName: string, object: any ) {
    return this.delete(tableName, object[IDB_GET_TABLE_PK( tableName )])
        .then(this.add(tableName, object))
  },

  putAll( tableName: string, objects: any[] ) {
    return Promise.all(objects.map(object => this.put(tableName, object)))
  },

  backup() {},

  restore( jsonData: AdonisLocalRecovery ) {},
} as IindexedDb
