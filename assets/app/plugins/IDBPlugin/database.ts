import { DbStoreVersionner } from '@adonis/app/migrations/db-store-versionner'
import { AdonisLocalRecovery, AdonisStoreRecovery } from '@adonis/app/models/app-functionalities/adonis-local-recovery'
import { MigrationService } from '../../migrations/migration-service'
import { IindexedDb } from './IdbPlugin'

if (!window.indexedDB) {
  window.alert( 'Votre navigateur ne supporte pas une version stable d\'IndexedDB. Quelques fonctionnalités ne seront pas disponibles.' )
}

/**
 * Functions will return promises. To get the value of a promise, you must add handlers on the promise instance (then,
 * catch, finally).
 */
export default {

  databaseVersion: null,

  dbName: null,

  db: null,

  init() {
    const superThis = this
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        const request: IDBOpenDBRequest = indexedDB.open( superThis.dbName, superThis.databaseVersion )
        request.onerror = ( event: any ) => {
          console.log( event )
          reject( `error opening database ${event.target.errorCode}` )
        }

        request.onupgradeneeded = MigrationService.getOnupgradeneeded()

        request.onsuccess = ( event: any ) => {
          superThis.db = event.target.result
          resolve( event.target.result )
        }
      } else {
        resolve( this.db )
      }

    } )
  },

  count( tableName: string ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName] )
            .objectStore( tableName )
            .count()
        pr.onsuccess = ( event: any ) => resolve( event.target.result )
        pr.onerror = ( event: any ) => reject( `error counting data ${event.target.errorCode}` )
      }
    } )
  },

  add( tableName: string, object: any ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName], 'readwrite' )
            .objectStore( tableName )
            .add( object )
        pr.onsuccess = () => resolve()
        pr.onerror = ( event: any ) => reject( `error adding data : ${event.target.errorCode}; in object store ${tableName}` )
      }
    } )
  },

  addAll( tableName: string, objects: any[] ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const tr = this.db.transaction( [tableName], 'readwrite' )
        objects.forEach( ( object ) => tr.objectStore( tableName )
            .add( object ) )
        tr.oncomplete = () => resolve()
        tr.onerror = ( event: any ) => reject( `error adding data ${event.target.errorCode}` )
      }
    } )
  },

  delete( tableName: string, primaryKey: any ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName], 'readwrite' )
            .objectStore( tableName )
            .delete( primaryKey )
        pr.onsuccess = () => resolve()
        pr.onerror = ( event: any ) => reject( `error deleting data ${event.target.errorCode}` )
      }
    } )
  },

  get( tableName: string, primaryKey: any ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName] )
            .objectStore( tableName )
            .get( primaryKey )
        pr.onsuccess = ( event: any ) => resolve( event.target.result )
        pr.onerror = ( event: any ) => reject( `error getting data ${event.target.errorCode}` )
      }
    } )
  },

  getAll( tableName: string ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName] )
            .objectStore( tableName )
            .getAll()
        pr.onsuccess = ( event: any ) => resolve( event.target.result )
        pr.onerror = ( event: any ) => reject( `error getting cursor ${event.target.errorCode}` )
      }
    } )
  },

  clear( tableName: string ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName], 'readwrite' )
            .objectStore( tableName )
            .clear()
        pr.onsuccess = () => resolve()
        pr.onerror = ( event: any ) => reject( `error clearing data ${event.target.errorCode}` )
      }
    } )
  },

  put( tableName: string, object: any ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const pr = this.db.transaction( [tableName], 'readwrite' )
            .objectStore( tableName )
            .put( object )
        pr.onsuccess = () => resolve()
        pr.onerror = ( event: any ) => reject( `error putting data ${event.target.errorCode}` )
      }
    } )
  },

  putAll( tableName: string, objects: any[] ) {
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const tr = this.db.transaction( [tableName], 'readwrite' )
        objects.forEach( ( object ) => tr.objectStore( tableName )
            .put( object ) )
        tr.onsuccess = () => resolve()
        tr.onerror = ( event: any ) => reject( `error putting data ${event.target.errorCode}` )
      }
    } )
  },

  backup() {
    const superThis = this
    return new Promise( ( resolve, reject ) => {
      if (this.db === null) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else {
        const databaseRestore = new AdonisLocalRecovery( superThis.databaseVersion, superThis.dbName )
        Promise.all( [...this.db.objectStoreNames].map( ( tableName: string ) => {
          const objectStoreRestore = new AdonisStoreRecovery( tableName )
          databaseRestore.tables.push( objectStoreRestore )
          return superThis.getAll( tableName )
              .then( ( objects: any[] ) => objectStoreRestore.rows = [...objectStoreRestore.rows, ...objects] )
        } ) )
            .then( () => resolve( databaseRestore ) )
      }
    } )
  },

  restore( jsonData: AdonisLocalRecovery ) {
    const superThis = this
    return new Promise( ( resolve, reject ) => {
      if (null === this.db) {
        console.log( 'La base de données n\'est pas prête' )
        reject( 'La base de données n\'est pas prête' )
      } else if (!jsonData) {
        console.log( 'No readable data in file' )
        reject( 'No readable data in file' )
      } else if (jsonData.dbVersion < superThis.databaseVersion) {
        console.log( `updating file from version ${jsonData.dbVersion}` )
        try {
          const versions = MigrationService.getVersions( jsonData.dbVersion, superThis.databaseVersion )
          jsonData = versions.reduce( ( accJsonData: AdonisLocalRecovery, version: DbStoreVersionner ): AdonisLocalRecovery => {
            const previousData = new Map<string, any[]>()
            accJsonData.tables.forEach( t => {
              previousData.set( t.name, t.rows )
            } )
            const update = version.objectStoreUpdate( previousData )
            for (const objectStoreName of Array.from( update.keys() )) {
              const table = accJsonData.tables.find( t => objectStoreName === t.name )
              if (table) {
                update.get( objectStoreName ).deleteRows
                    .forEach( r => table.rows = table.rows.filter( rp => rp.id !== r.id ) )
                update.get( objectStoreName ).updateRows
                    .forEach( r => table.rows = [...table.rows.filter( rp => rp.id !== r.id ), r] )
                table.rows = [...table.rows, ...update.get( objectStoreName ).addRows]
              }
            }
            accJsonData.dbVersion = version.targetVersion
            return accJsonData
          }, jsonData )
          superThis.restore( jsonData )
              .then( () => resolve() )
        } catch (ex) {
          console.log( ex )
          reject( `Unable to update file from version ${jsonData.dbVersion}` )
        }

      } else if (jsonData.dbVersion === superThis.databaseVersion && jsonData.dbName === superThis.dbName) {
        const transaction = superThis.db.transaction( jsonData.tables.map( table => table.name ), 'readwrite' )
        jsonData.tables.forEach( objectStore => {
          transaction.objectStore( objectStore.name )
              .clear()
          objectStore.rows.forEach(
              row => transaction.objectStore( objectStore.name )
                  .add( row ),
          )
        } )
        transaction.oncomplete = () => resolve()
        transaction.onerror = ( event: any ) => {
          reject( `error restoring data ${event.target.errorCode}` )
          console.log( event )
        }
      } else {
        reject( `Unable to read data. Version or dbname mismatch: ${jsonData.dbVersion}:${superThis.databaseVersion} / ${jsonData.dbName}:${superThis.db.dbName}` )
      }
    } )
  },
} as IindexedDb
