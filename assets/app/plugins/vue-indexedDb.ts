import Vue from 'vue'
import IdbPlugin from './IDBPlugin/IdbPlugin'

export const IDB_PROJECT = 'project'
export const IDB_DATA_ENTRY = 'data-entry'
export const IDB_SPACIALIZATION = 'spacialization'
export const IDB_LIBRARY_VARIABLES = 'variables'
export const IDB_LIBRARY_DEVICES = 'devices'
export const IDB_SESSIONS = 'sessions'
export const IDB_FIELD_REGISTRY = 'field-registry'

export const IDB_TABLES_NAMES = [
  IDB_PROJECT,
  IDB_DATA_ENTRY,
  IDB_SPACIALIZATION,
  IDB_LIBRARY_VARIABLES,
  IDB_LIBRARY_DEVICES,
  IDB_SESSIONS,
  IDB_FIELD_REGISTRY,
]

export const IDB_GET_TABLE_PK = ( tableName: string ) => {
  switch (tableName) {
    case IDB_PROJECT:
      return 'id'
    case IDB_DATA_ENTRY:
      return 'id'
    case IDB_SPACIALIZATION:
      return 'id'
    case IDB_LIBRARY_VARIABLES:
      return 'id'
    case IDB_LIBRARY_DEVICES:
      return 'id'
    case IDB_SESSIONS:
      return 'id'
    case IDB_FIELD_REGISTRY:
      return 'fieldUuid'
  }
}

export const DATABASE_NAME = 'adonisMobile'
export const DATABASE_VERSION = 17

Vue.use( IdbPlugin, {
  databaseVersion: DATABASE_VERSION,
  dbName: DATABASE_NAME,
} )
