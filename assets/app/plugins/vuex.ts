import ANNOTATION_STORE_MODULE from '@adonis/app/stores/annotation/annotation-store-module'
import APP_STORE_MODULE from '@adonis/app/stores/app/app-store-module'
import DATA_ENTRY_STORE_MODULE from '@adonis/app/stores/data-entry/data-entry-store-module'
import CONFIRM_STORE_MODULE from '@adonis/shared/stores/confirm/confirm-store-module'
import CONNECTION_STORE_MODULE from '@adonis/shared/stores/connection/connection-store-module'
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use( Vuex )

export default new Vuex.Store( {
      modules: {
        app: APP_STORE_MODULE,
        dataEntry: DATA_ENTRY_STORE_MODULE,
        connection: CONNECTION_STORE_MODULE,
        confirm: CONFIRM_STORE_MODULE,
        annotation: ANNOTATION_STORE_MODULE,
      },
    },
)
