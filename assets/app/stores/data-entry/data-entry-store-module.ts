import { FORM_MODE_BLOCK, FREE_MODE_LOCK, FREE_MODE_OPEN } from '@adonis/app/constants'
import { MeasureReportData } from '@adonis/app/models/app-functionalities/measure-report-data'
import AbstractProject from '@adonis/app/models/business-objects/abstract-project'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import VisualizationFilterChoice from '@adonis/app/modules/data-entry/visualization/visualization-filter-choice'
import DataEntryService from '@adonis/app/services/data-entry-service'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import PrintService from '@adonis/app/services/print-service'
import DataEntryState from '@adonis/app/stores/data-entry/data-entry-state'
import DataEntryStore from '@adonis/app/stores/data-entry/data-entry-store'
import { DataEntryStoreHelper } from '@adonis/app/stores/data-entry/data-entry-store-helper'

/**
 * The data entry store module being added to vuex-store.
 */
export default {

  namespaced: true,

  /** The store state */
  state: DataEntryStoreHelper.getDefaultState(),

  getters: {

    /**
     * Get a boolean in order to know if a project is stored here.
     *
     * @param currentState
     *
     * @return boolean
     */
    isWorkInProgress( currentState: DataEntryState ): boolean {
      return !!currentState.wipProject
    },

    /**
     * Get the current working project.
     *
     * @param currentState
     *
     * @return
     */
    getWorkInProgress( currentState: DataEntryState ): AbstractProject {
      return currentState.wipProject
    },

    /**
     * Get the form field groups generated based on tab mode and current position.
     *
     * @param currentState
     *
     * @return FormFieldGroup[]
     */
    getFormFields( currentState: DataEntryState ): FormFieldGroup[] {
      return currentState.formFields
    },

    /**
     * Get the index of the current position object in form field group array.
     *
     * @param currentState
     *
     * @return number
     */
    getIndexOfCurrent( currentState: DataEntryState ): number {

      let currentIndex = null
      const currentUri = currentState.wipProject.dataEntry.currentPosition
      currentState.formFields.forEach( ( formGroup: FormFieldGroup, index: number ) => {
        if (currentUri === formGroup.objectUri) {
          currentIndex = index
        }
      } )
      return currentIndex
    },

    /**
     * Get the measure report.
     *
     * @param currentState
     *
     * @return MeasureReportData[]
     */
    getMeasureReports( currentState: DataEntryState ): MeasureReportData[] {
      return currentState.measureReports
    },

    /**
     * Returns TRUE if all measures are done on all object that need those measures.
     *
     * @param currentState
     *
     * @return boolean
     */
    isProjectComplete( currentState: DataEntryState ): boolean {

      const hasPath = !!currentState.wipProject?.dataEntry?.workpath &&
          0 < currentState.wipProject.dataEntry.workpath.length
      const visited = currentState.wipProject?.dataEntry?.visitedBusinessObjectsURI ?? []
      const toVisite = currentState.wipProject?.dataEntry?.workpath?.filter( uri => !visited.includes( uri ) ) ?? []

      return hasPath && 0 === toVisite.length
    },

    /**
     * Check if the current position is the last position listed in workpath.
     * Returns false when no workpath.
     *
     * @param currentState
     *
     * @return boolean
     */
    isEndOfWorkpath( currentState: DataEntryState ): boolean {

      let isEnd = false
      const workpath = currentState.wipProject.dataEntry.workpath
      const current = currentState.wipProject.dataEntry.currentPosition
      if (!!workpath && 0 < workpath.length) {
        const lastKey = workpath[workpath.length - 1]
        isEnd = !!current && lastKey === current
      }
      return isEnd
    },

    /**
     * Get the index of user's breadcrumb prefs.
     *
     * @param currentState
     *
     * @return number[]
     */
    getBreadcrumbPref( currentState: DataEntryState ): number[] {
      return currentState.breadcrumbPref
    },

    /**
     * Get the VisualizationFilterChoice of the user.
     *
     * @param currentState
     *
     * @return VisualizationFilterChoice
     */
    getVisualizationFilter( currentState: DataEntryState ): VisualizationFilterChoice {
      return currentState.visualizationFilter
    },

    /**
     * Access the state concerning form or table mode in data entry project.
     *
     * @param currentState
     *
     * @return number
     */
    getTableMode( currentState: DataEntryState ): number {
      return currentState.tableMode
    },

    /**
     * Get the navigation state of the project.
     *
     * @param currentState
     *
     * @return number
     */
    getFreeMode( currentState: DataEntryState ): number {
      // It's always true when current project has no workpaths to be selected.
      return currentState.freeMode
    },

    /**
     * Get auto increments values.
     *
     * @param currentState
     *
     * @return number[]
     */
    getIncrements( currentState: DataEntryState ): number[] {
      return currentState.increments
    },

    /**
     * Defines if the print action is required each time the operator validate a form.
     *
     * @param currentState
     *
     * @return boolean
     */
    getDoPrint( currentState: DataEntryState ): boolean {
      return currentState.doPrint
    },
  },

  actions: {

    /**
     * Action allowing user to set the currently working project or to update the state when it is already
     * loaded in the current store state.
     *
     * @param store
     * @param payload
     *
     * @return Promise<void>
     */
    setWorkInProgress( store: DataEntryStore, payload: { vm: Vue, wip: AbstractProject } ): Promise<void> {

      const vm = payload.vm
      const wipProject = payload.wip
      const openedProject = store.getters.getWorkInProgress
      const tableMode = store.getters.getTableMode
      const freeMode = store.getters.getFreeMode
      const doPrint = store.getters.getDoPrint

      return new Promise<void>( async resolve => {
        if (!store.getters.isWorkInProgress) {
          // When project is set for the first time, reinitialize table mode and free mode.
          store.commit( 'setTableMode', FORM_MODE_BLOCK )
          if (wipProject instanceof DataEntryProject) {
            store.commit( 'setFreeMode', !!wipProject && wipProject.hasWorkpaths
                ? FREE_MODE_LOCK
                : FREE_MODE_OPEN )
          }
          store.commit( 'setIncrements', [1, 1] )
        }
        // Handle fields of current project.
        const formFieldGroups: FormFieldGroup[] = []
        if (!!wipProject) {
          // Building form field groups.
          if (!!wipProject.dataEntry.currentPosition) {
            await DataEntryStoreHelper.buildFormFieldGroups( tableMode, freeMode, wipProject, formFieldGroups, vm )
          }
          // Building current project state reports.
          // Handle visualization filters in case project is not the same.
          if (!store.state.visualizationFilter || store.state.visualizationFilter.projectId !== wipProject.id) {
            store.commit( 'setVisualizationFilter', new VisualizationFilterChoice( wipProject.id ) )
          }

          const askedDoPrint = wipProject.dataEntry.doPrint
          if (wipProject instanceof DataEntryProject) {
            if (!doPrint && !!askedDoPrint) { // Activation of print mod ?
              PrintService.startPrintDataEntry( vm, wipProject )
            } else if (!!doPrint && !askedDoPrint) { // Inactivation of print mod ?
              PrintService.closePrintDataEntry( vm, wipProject )
            }
          }
        } else if (!!openedProject && doPrint) {
          PrintService.closePrintDataEntry( vm, openedProject )
        }

        store.commit( 'setWorkInProgress', wipProject )
        store.commit( 'setDoPrint', !!wipProject && wipProject.dataEntry.doPrint )
        store.commit( 'setFormFields', formFieldGroups )

        resolve()
      } )
    },

    /**
     * Allows to move the current position in form workflow to a specific number of object forward or backward.
     *
     * @param store
     * @param payload
     *
     * @return Promise<void>
     */
    move(
        store: DataEntryStore,
        payload: { vm: Vue, formFieldGroup: FormFieldGroup, movement: number },
    ): Promise<void> {

      return new Promise<void>( ( resolve: ( ...args: any ) => void ) => {
        const wipProject = store.getters.getWorkInProgress
        const movement: number = undefined !== payload.movement
            ? payload.movement
            : +1
        const formFields: FormField[] = payload.formFieldGroup.objectFields
        const vm: Vue = payload.vm
        const project = FormMeasureService.saveFormFieldsToProject( vm.$indexedDb, wipProject, formFields, true )

        if (store.getters.getDoPrint && !!payload.formFieldGroup) {
          PrintService.printFormFieldGroup( vm, project, payload.formFieldGroup )
        }

        const wip: AbstractProject = DataEntryService.moveTo(
            project,
            movement,
        )

        DataEntryService.saveProjectToDatabase( vm, wip )

            .then( () => {
              resolve()
            } )
      } )
    },

    /**
     * Allow to move to a specific target BusinessObject/Evaluable.
     *
     * @param store
     * @param payload
     *
     * @return Promise<void>
     */
    goto(
        store: DataEntryStore,
        payload: { vm: Vue, formFieldGroup: FormFieldGroup, target: Evaluable },
    ): Promise<void> {

      return new Promise<void>( ( resolve: ( ...args: any ) => void ) => {
        const wipProject = store.getters.getWorkInProgress
        const vm = payload.vm
        const formFields = !!payload.formFieldGroup
            ? payload.formFieldGroup.objectFields
            : null
        const target = payload.target
        const project = !!formFields
            ? FormMeasureService.saveFormFieldsToProject( vm.$indexedDb, wipProject, formFields, true )
            : wipProject

        if (store.getters.getDoPrint && !!payload.formFieldGroup) {
          PrintService.printFormFieldGroup( vm, project, payload.formFieldGroup )
        }

        DataEntryService.moveToObject(
            project,
            target )

        DataEntryService.saveProjectToDatabase( vm, wipProject )
            .then( () => {
              resolve()
            } )
      } )
    },

    /**
     * Save the breadcrumb preferences into store.
     *
     * @param store
     * @param payload
     */
    setBreadcrumbPref( store: DataEntryStore, payload: number[] ): void {
      store.commit( 'setBreadcrumbPref', payload )
    },

    /**
     * Save visualization preferences into store.
     *
     * @param store
     * @param payload
     */
    setVisualizationFilter( store: DataEntryStore, payload: VisualizationFilterChoice ): void {
      store.commit( 'setVisualizationFilter', payload )
    },

    /**
     * Change table/form data entry mode.
     *
     * @param store
     * @param payload
     */
    async setTableMode( store: DataEntryStore, payload: { vm: Vue, tableMode: number } ): Promise<void> {

      const vm = payload.vm
      const tableMode: number = payload.tableMode
      const freeMode = store.getters.getFreeMode
      const wipProject: DataEntryProject = store.getters.getWorkInProgress
      const formFieldGroups: FormFieldGroup[] = []

      if (!!wipProject.dataEntry.currentPosition) {
        await DataEntryStoreHelper.buildFormFieldGroups( tableMode, freeMode, wipProject, formFieldGroups, vm )
      }

      store.commit( 'setFormFields', formFieldGroups )
      return store.commit( 'setTableMode', tableMode )
    },

    /**
     * Change navigation date entry mode.
     * @param store
     * @param payload
     */
    async setFreeMode( store: DataEntryStore, payload: { vm: Vue, freeMode: number } ): Promise<void> {

      const vm = payload.vm
      const wipProject: AbstractProject = store.getters.getWorkInProgress
      let freeMode: number = payload.freeMode
      const tableMode: number = store.getters.getTableMode
      // May change the freeMode only if the current project has selectable workpaths, otherwise it's always true.
      if (
          wipProject instanceof DataEntryProject &&
          (!wipProject || !wipProject.hasWorkpaths) && FREE_MODE_LOCK === freeMode
      ) {
        freeMode = FREE_MODE_OPEN
      }
      const formFieldGroups: FormFieldGroup[] = []

      if (!!wipProject.dataEntry.currentPosition) {
        await DataEntryStoreHelper.buildFormFieldGroups( tableMode, freeMode, wipProject, formFieldGroups, vm )
      }

      store.commit( 'setFormFields', formFieldGroups ) // Get filled by buildFormFieldGroups() call.
      return store.commit( 'setFreeMode', freeMode )
    },

    /**
     * Change increments automatically used to select next objets in free position mode.
     *
     * @param store
     * @param payload
     */
    setIncrements( store: DataEntryStore, payload: { increments: number[] } ): void {

      if (2 === payload.increments.length) {
        store.commit( 'setIncrements', payload.increments )
      }
    },

    /**
     * Update reports informations regarding the running project.
     *
     * @param store
     * @param payload
     */
    updateReports( store: DataEntryStore, payload: { vm: Vue } ): Promise<void> {

      const project = store.getters.getWorkInProgress
      return FormMeasureService.buildMeasureReports( payload.vm, project )
          .then( reports => store.commit( 'setMeasureReports', reports ) )
    },
  },

  mutations: {

    /**
     * Mutate project in store.
     *
     * @param currentState
     * @param wipProject
     */
    setWorkInProgress( currentState: DataEntryState, wipProject: AbstractProject ): void {

      currentState.wipProject = null
      currentState.wipProject = wipProject
    },

    /**
     * Mutate form fields in store.
     *
     * @param currentState
     * @param formFields
     */
    setFormFields( currentState: DataEntryState, formFields: FormFieldGroup[] ): void {
      currentState.formFields = formFields
    },

    /**
     * Mutate measures reports in store.
     *
     * @param currentState
     * @param measureReports
     */
    setMeasureReports( currentState: DataEntryState, measureReports: MeasureReportData[] ): void {
      currentState.measureReports = measureReports
    },

    /**
     * Mutate breadcrumb preferences in store.
     *
     * @param currentState
     * @param breadcrumbPrefValue
     */
    setBreadcrumbPref( currentState: DataEntryState, breadcrumbPrefValue: number[] ): void {

      currentState.breadcrumbPref = breadcrumbPrefValue
      window.localStorage.setItem( DataEntryStoreHelper.BREADCRUMB_STORAGE_KEY, JSON.stringify( breadcrumbPrefValue ) )
    },

    /**
     * Mutate visualization filter choices in store.
     *
     * @param currentState
     * @param visualizationFilterValue
     */
    setVisualizationFilter( currentState: DataEntryState, visualizationFilterValue: VisualizationFilterChoice ): void {

      currentState.visualizationFilter = visualizationFilterValue
      window.localStorage.setItem( DataEntryStoreHelper.VISUALIZATION_STORAGE_KEY,
          JSON.stringify( DataEntryStoreHelper.buildVisualizationFilterToStorage( visualizationFilterValue ) ) )
    },

    /**
     * Mutate table mode in store.
     *
     * @param currentState
     * @param tableMode
     */
    setTableMode( currentState: DataEntryState, tableMode: number ): void {
      currentState.tableMode = tableMode
    },

    /**
     * Mutate free mode in store.
     *
     * @param currentState
     * @param freeMode
     */
    setFreeMode( currentState: DataEntryState, freeMode: number ): void {
      currentState.freeMode = freeMode
    },

    /**
     * Mutate increments in store.
     *
     * @param currentState
     * @param increments
     */
    setIncrements( currentState: DataEntryState, increments: number[] ): void {
      currentState.increments = increments
    },

    /**
     * Mutate do print flag in store.
     *
     * @param currentState
     * @param doPrint
     */
    setDoPrint( currentState: DataEntryState, doPrint: boolean ): void {
      currentState.doPrint = doPrint
    },
  },
}
