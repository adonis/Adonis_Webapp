import { MeasureReportData } from '@adonis/app/models/app-functionalities/measure-report-data'
import AbstractProject from '@adonis/app/models/business-objects/abstract-project'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import VisualizationFilterChoice from '@adonis/app/modules/data-entry/visualization/visualization-filter-choice'

/**
 * Class DataEntryState.
 */
export default class DataEntryState {

  constructor(
      public wipProject: AbstractProject,
      public formFields: FormFieldGroup[],
      public measureReports: MeasureReportData[],
      public breadcrumbPref: number[],
      public visualizationFilter: VisualizationFilterChoice,
      public tableMode: number,
      public freeMode: number,
      public increments: number[],
      public doPrint: boolean,
  ) {
  }
}
