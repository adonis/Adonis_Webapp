import { FORM_MODE_BLOCK, FORM_MODE_TABLE, FREE_MODE_LOCK } from '@adonis/app/constants'
import AbstractProject from '@adonis/app/models/business-objects/abstract-project'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import VisualizationCustomFilterModel from '@adonis/app/modules/data-entry/visualization/visualization-custom-filter-model'
import VisualizationFilterChoice from '@adonis/app/modules/data-entry/visualization/visualization-filter-choice'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import PlatformService from '@adonis/app/services/platform-service'
import DataEntryState from '@adonis/app/stores/data-entry/data-entry-state'
import VisualizationFilterStoreInterface from '@adonis/app/stores/data-entry/visualization-filter-store-interface'

/**
 * Service helper to group function that allow initializing data entry store module and manipulate complex objects.
 */
export class DataEntryStoreHelper {

  /** KEY used to store breadcrumb prefs of current user into local storage of navigator. */
  public static BREADCRUMB_STORAGE_KEY = 'Adonis::BreadcrumbPreferences'

  /** KEY used to store visualization prefs of current user into local storage of navigator. */
  public static VISUALIZATION_STORAGE_KEY = 'Adonis::VisualizationFilters'

  /**
   * Create the default state of the store based on local storage.
   *
   * @return DataEntryState
   */
  public static getDefaultState(): DataEntryState {
    const breadcrumbPrefStr = window.localStorage.getItem( this.BREADCRUMB_STORAGE_KEY )
    const breadcrumbPref = !!breadcrumbPrefStr
        ? JSON.parse( breadcrumbPrefStr )
        : [4]

    const visualizationFilterStr = window.localStorage.getItem( this.VISUALIZATION_STORAGE_KEY )

    const visualizationFilter = !!visualizationFilterStr
        ? DataEntryStoreHelper.buildVisualizationFilterFromStorage( JSON.parse( visualizationFilterStr ) )
        : new VisualizationFilterChoice( null )

    return new DataEntryState(
        null,
        [],
        [],
        breadcrumbPref,
        visualizationFilter,
        FORM_MODE_BLOCK,
        FREE_MODE_LOCK,
        [1, 1],
        false,
    )
  }

  /**
   * Build form field groups array given as params.
   *
   * @param tableMode
   * @param freePath
   * @param wipProject
   * @param formFieldGroups
   * @param vm
   */
  public static async buildFormFieldGroups(
      tableMode: number,
      freePath: number,
      wipProject: AbstractProject,
      formFieldGroups: FormFieldGroup[],
      vm: Vue,
  ): Promise<void> {
    if (FORM_MODE_TABLE === tableMode) {
      let uris: string[]
      if (FREE_MODE_LOCK !== freePath && 0 === wipProject.dataEntry.workpath.length) {
        // In this case, the path is free and there is no workflow actually selectable. All objects are available.
        uris = PlatformService.getObjectsOfLevel( wipProject.platform, wipProject.getCurrentLevel() )
            .map( ( obj: Evaluable ) => obj.uri )
      } else {
        // Based on workflow.
        uris = wipProject.dataEntry.workpath
            .filter( ( uri: string ) => uri.includes( wipProject.getCurrentLevel() ) )
      }
      await Promise.all( uris.map( async ( uri: string ) => {
        await FormMeasureService.addFormFieldGroupByUri( wipProject, uri, formFieldGroups, vm )
      } ) )
    } else {
      await FormMeasureService.addFormFieldGroupByUri( wipProject, wipProject.dataEntry.currentPosition, formFieldGroups, vm )
    }
  }

  /**
   * Instanciate the visualization filter props.
   *
   * @param parse
   *
   * @return VisualizationFilterChoice
   */
  public static buildVisualizationFilterFromStorage( parse: VisualizationFilterStoreInterface ): VisualizationFilterChoice {
    return new VisualizationFilterChoice(
        parse.projectId,
        new Map( parse.filterMap ),
        parse.columns,
        new VisualizationCustomFilterModel(
            parse.custom.column,
            parse.custom.operator,
            parse.custom.value,
        ),
    )
  }

  /**
   * Revert operation of buildVisualizationFilterFromStorage() to get interface to store in local storage.
   *
   * @param filter
   *
   * @return VisualizationFilterStoreInterface
   */
  public static buildVisualizationFilterToStorage( filter: VisualizationFilterChoice ): VisualizationFilterStoreInterface {
    return {
      projectId: filter.projectId,
      filterMap: Array.from( filter.filterMap.entries() ),
      custom: filter.custom,
      columns: filter.columns,
    }
  }

}
