import VisualizationCustomFilterModel from '@adonis/app/modules/data-entry/visualization/visualization-custom-filter-model'

/**
 * Interface to store the visualization filter.
 */
export default interface VisualizationFilterStoreInterface {
  projectId: number | string
  filterMap: any[]
  columns: string[]
  custom: VisualizationCustomFilterModel
}
