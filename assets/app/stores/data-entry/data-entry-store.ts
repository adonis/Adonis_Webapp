import DataEntryState from '@adonis/app/stores/data-entry/data-entry-state'

/**
 * Class DataEntryStore.
 */
export default class DataEntryStore {
  state: DataEntryState
  dispatch: ( ...args: any ) => any
  commit: ( ...args: any ) => any
  getters: any
}