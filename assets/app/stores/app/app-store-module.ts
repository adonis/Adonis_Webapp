import AppState from '@adonis/app/stores/app/app-state'
import AppStore from '@adonis/app/stores/app/app-store'

const state: AppState = new AppState()
state.menu = { isOpen: false }
state.context = { isOpen: false }
state.wip = false

export default {

  namespaced: true,
  state,

  getters: {

    isWorkInProgress( currentState: AppState ): boolean {
      return !!currentState.wip
    },
  },

  actions: {

    toggleMenu( store: AppStore ): void {
      if (!store.state.wip) {
        store.commit( 'setMenuOpen', !store.state.menu.isOpen )
        store.commit( 'setContextOpen', false )
      }
    },

    toggleContext( store: AppStore ): void {
      if (store.state.wip) {
        store.commit( 'setContextOpen', !store.state.context.isOpen )
        store.commit( 'setMenuOpen', false )
      }
    },

    setWorkInProgress( store: AppStore, wip: boolean ): void {
      store.commit( 'setWorkInProgress', wip )
    },
  },

  mutations: {

    setMenuOpen: ( currentState: AppState, open: boolean ) => {
      currentState.menu.isOpen = open
    },

    setContextOpen: ( currentState: AppState, open: boolean ) => {
      currentState.context.isOpen = open
    },

    setWorkInProgress( currentState: AppState, wip: boolean ): void {
      currentState.wip = wip
    },
  },
}
