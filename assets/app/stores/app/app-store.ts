import AppState from '@adonis/app/stores/app/app-state'

export default class AppStore {

  state: AppState
  commit: ( ...args: any ) => any
}