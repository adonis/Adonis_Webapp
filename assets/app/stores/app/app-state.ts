import MenuOpen from '@adonis/app/stores/app/menu-open'

export default class AppState {

  menu: MenuOpen
  context: MenuOpen
  wip: boolean
}