import AnnotationState from '@adonis/app/stores/annotation/annotation-state'
import AnnotationStore from '@adonis/app/stores/annotation/annotation-store'
import AsyncAnnotationPopup from '@adonis/app/stores/annotation/async-annotation-popup'

const state = new AnnotationState()
state.annotationPopup = null

export default {

  namespaced: true,

  state,

  getters: {

    getAnnotation( currentState: AnnotationState ): AsyncAnnotationPopup {
      return currentState.annotationPopup
    },
  },

  actions: {

    askAnnotation( store: AnnotationStore, payload: AsyncAnnotationPopup ) {
      if (!store.state.annotationPopup) {
        store.commit( 'setAnnotationRequest', payload )
      }
    },

    reinitialize( store: AnnotationStore ) {
      if (!!store.state.annotationPopup) {
        store.commit( 'setAnnotationRequest', null )
      }
    },
  },

  mutations: {

    setAnnotationRequest( currentState: AnnotationState, payload: AsyncAnnotationPopup ) {
      currentState.annotationPopup = payload
    },
  },
}
