import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import RequiredAnnotation from '@adonis/app/models/evaluation-variables/required-annotation'

export default interface AsyncAnnotationPopup {
  requiredAnnotation: RequiredAnnotation
  target: Evaluable
  onContinue: ( ...args: any ) => void
}
