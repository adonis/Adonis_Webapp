import AnnotationState from '@adonis/app/stores/annotation/annotation-state'

export default class AnnotationStore {
  commit: ( ...args: any ) => void
  state: AnnotationState
}
