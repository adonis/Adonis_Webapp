import AsyncAnnotationPopup from '@adonis/app/stores/annotation/async-annotation-popup'

export default class AnnotationState {
  annotationPopup: AsyncAnnotationPopup
}
