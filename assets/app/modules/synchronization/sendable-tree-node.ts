import TreeNode from '@adonis/app/models/app-functionalities/node-tree'
import AbstractProject from '@adonis/app/models/business-objects/abstract-project'

/**
 * TreeNode extension in order to add the project that can be sent to the server.
 */
export default interface SendableTreeNode extends TreeNode {
  project: AbstractProject;
}
