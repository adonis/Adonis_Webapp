import TreeNode from '@adonis/app/models/app-functionalities/node-tree'

/**
 * Project action wrapper.
 */
export default interface ManageProject extends TreeNode {
  id: number
  name: string
  deleteLabel: string
  deleteAction: ( ...args: any ) => any
  children?: ManageProject[]
}
