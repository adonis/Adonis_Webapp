import ManageProjectTreeNode from '@adonis/app/modules/manage-project/manage-project-tree-node'

/**
 * Manage project tree field.
 */
export default interface ManageProjectTree {
  group: string
  label: string
  nodes: ManageProjectTreeNode[]
}
