import TreeNode from '@adonis/app/models/app-functionalities/node-tree'

/**
 * Tree interface to handle manage project.
 */
export default interface ManageProjectTreeNode extends TreeNode {
  deleteLabel: string
  deleteAction: ( ...args: any ) => any
  cancelAction?: ( ...args: any ) => any
}
