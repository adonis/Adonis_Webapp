<template>
    <div
            :class="withPreviousValue ? 'has-previous-value' : 'no-previous-value'"
            class="my-1 valuer"
            ref="container"
    >
        <div
                v-for="previousValue of previousValues"
                :key="`prev-${previousValue.originVariableUid}`"
                class="previous-value"
        >
            <div class="previous-value__header">
                <div class="previous-value__header-left">
                  <b>{{ project.variablesMap.get(previousValue.originVariableUid).shortName }}&nbsp;:</b>
                </div>
                <div class="previous-value__header-right"><small>({{ previousValue.date | moment("YYYY-MM-DD") }})</small></div>
            </div>
            <div class="grow px-2">
                {{ previousValue.stateCode ? getStateCodeLabel(previousValue.stateCode) : formatValue(previousValue) }}
            </div>
        </div>
        <v-form
                :class="withPreviousValue ? 'form-field' : ''"
                class="px-1 mx-3 pt-2"
        >
            <!-- Using v-for directive here to handle variable repetition (array of measure). -->
            <v-sheet
                    v-for="(measure, position) in formField.measures"
                    :key="measure.uuid"
            >
                <!-- Measures must be editable. -->
                <field-type-adaptor
                        v-if="measure.isEditable()"
                        :ref="measure.uuid"
                        :active="active"
                        :form-field="formField"
                        :form-fields="formFields"
                        :label="fieldLabel(position)"
                        :measure="measure"
                        :project="project"
                        :index="index"
                        @click="onClick(position)"
                        @next="next(position)"
                        @change:state="propagateStateCode"
                        @semi-auto:measured="onSemiAutoMeasures"
                >
                    <template v-slot:variable-viewer>
                        <dialog-variable-viewer
                                :project="project"
                                :variable="variable"
                        ></dialog-variable-viewer>
                    </template>
                </field-type-adaptor>
                <!-- Show status code instead of field when not editable. -->
                <state-code-viewer
                        v-else
                        :ref="measure.uuid"
                        :active="active"
                        :label="fieldLabel(position)"
                        :measure="measure"
                        :project="project"
                        :variable="variable"
                        @keypress-enter="next(position)"
                        @state-propagate="propagateStateCode"
                        @state-removed="onStateRemove(position)"
                >
                    <template v-slot:variable-viewer>
                        <dialog-variable-viewer
                                :project="project"
                                :variable="variable"
                        ></dialog-variable-viewer>
                    </template>
                </state-code-viewer>
            </v-sheet>
        </v-form>
    </div>
</template>

<script lang="ts">
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import PreviousValue from '@adonis/app/models/evaluation-variables/previous-value'
import StateCode from '@adonis/app/models/evaluation-variables/state-code'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import VariableFormatEnum from "@adonis/shared/constants/variable-format-enum"
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum"
import moment from 'moment'
import { Component, Model, Prop, Vue } from 'vue-property-decorator'

/**
 * Component to show a specific form field as a block component (in form mod).
 */
@Component({
    components: {
        'dialog-variable-viewer': () => import('@adonis/app/modules/data-entry/form/dialogs/DialogVariableViewer.vue'),
        'state-code-viewer': () => import('@adonis/app/modules/data-entry/form/state-code/StateCodeViewer.vue'),
        'field-type-adaptor': () => import('@adonis/app/modules/data-entry/form/fields/FieldTypeAdaptor.vue'),
    },
})
export default class DentryFieldAsBlock extends Vue {

    /** Defines if the field is editable or not. */
    @Prop({default: false})
    active: boolean

    /** Define the place of this field in the form (don't take care about repetitions) */
    @Prop()
    index: number

    /** Concerned form field. */
    @Model()
    formField: FormField

    /** Other form fields rendered at the same level in the form.  */
    @Prop()
    formFields: FormField[]

    /** Access the whole project data. */
    @Prop()
    project: DataEntryProject

    /** Get the previous value of the form field if exists. */
    get previousValues(): PreviousValue[] {
        return this.variable.previousValues
            .filter(prevValue => prevValue.objectUri.replace('/', '') === this.formField.targetUri)
            .sort((prevValue1, prevValue2) => prevValue1.date < prevValue2.date ? -1 : 1)
    }

    get variable() {
        return this.project.variablesMap.get(this.formField.variableUri)
    }

    /** Defines if the form field has to handle previous value prompt or not. */
    get withPreviousValue(): boolean {
        return !!this.previousValues && 0 < this.previousValues.length
    }

    /**
     * When click occures on the field, start select process to handle hooks before focusing the field.
     *
     * @param position
     */
    onClick(position: number): void {
        this.select(position)
    }

    /**
     * If a state code matches given code, will return its label.
     *
     * @param code
     *
     * @return string
     */
    getStateCodeLabel(code: number): string {
        const stateInst = this.project.stateCodes.filter((state: StateCode) => state.code === code)[0]
        return !!stateInst
            ? `${stateInst.label} (${code})`
            : `Code ${code}`
    }

    /**
     * Gets field label for current variable at given position. Position correspond to the repetition instance.
     *
     * @param position
     *
     * @return string
     */
    fieldLabel(position: number): string {
        const variable = this.variable
        return FormMeasureService.fieldLabel(variable.name, position + 1, variable.repetitions, variable.mandatory)
    }

    /**
     * Selects the fields at given index in current variable repetition. Allow delete value when selected.
     *
     * @param position
     */
    select(position = 0): void {
        this.callChild(position)
    }

    doCallNext(position = 0): void {
        this.callChild(position, 'doCallNext')
    }

    callChild(position: number, fun = 'select'): void {
        // Using repetition structure to retry select action if the selected component is not yet mounted
        // when the function call occurred. Will repeat the action only 10 times, abort otherwise.
        let repetition = 0
        let selectAction = () => {
            this.$nextTick(() => {
                const measure: Measure = this.formField.measures[position]
                const selected: any = this.$refs[measure.uuid]
                if (!!selected && 0 < selected.length) {
                    selected[0][fun]()
                } else if (repetition < 10) {
                    setTimeout(() => {
                        ++repetition
                        selectAction()
                    }, 100)
                }
            })
        }
        selectAction()
    }

    /**
     * Called before do next to perform specific field hook.
     *
     * @param position
     */
    next(position: number): void {
        if (position + 1 < this.formField.measures.length) {
            this.select(position + 1)
        } else {
            const container = this.$refs['container'] as any
            if (!!container) {
                window.scrollBy(0, container.clientHeight)
            }
            this.$emit('pass')
        }
    }

    /**
     * Fires event "propagate" to inform about propagation of state code.
     *
     * @param targetUri
     * @param state
     * @param forcePropagationLevel
     */
    propagateStateCode(targetUri: string, state: StateCode, forcePropagationLevel: PathLevelEnum = null): void {
        this.$emit('change:state', targetUri, state, forcePropagationLevel)
    }

    /**
     * Handle automatic measures from specific materials.
     *
     * @param measures
     */
    onSemiAutoMeasures(measures: Map<string, any>): void {
        this.$emit('semi-auto:measured', measures)
    }

    /**
     * Handle state remove of field date.
     *
     * @param index
     */
    onStateRemove(index: number): void {
        this.select(index)
    }

    formatValue(previousValue: PreviousValue): string {
        let formatted = null
        if (!!previousValue && this.project.variablesMap.has(previousValue.originVariableUid)) {
            const connectedVariable = this.project.variablesMap.get(previousValue.originVariableUid)
            switch (connectedVariable.type) {
                case VariableTypeEnum.DATE:
                    const format = connectedVariable.format === VariableFormatEnum.QUANTIEM ? 'DDD' : 'YYYY-MM-DD';
                    formatted = moment(new Date(previousValue.value)).format(format)
                    break
                default:
                    formatted = previousValue.value
            }
        }
        return formatted
    }
}
</script>

<style lang="scss" scoped>

::v-deep .inputWithoutArrow input[type='number'] {
  -moz-appearance: textfield;
}

::v-deep .inputWithoutArrow input::-webkit-outer-spin-button,
::v-deep .inputWithoutArrow input::-webkit-inner-spin-button {
  -webkit-appearance: none;
}

.minWidth70 {
  min-width: 70px;
}

.minWidth160 {
  min-width: 160px;
}

.valuer {
  margin-bottom: 6px;
  border-top: thin solid #1b1b1b;
  border-bottom: thin solid #eee;

  &.has-previous-value {
    background-color: #eee;
    overflow-y: hidden;
    border-bottom: 6px solid #eee;

    .previous-value {
      display: flex;
      flex-direction: column;
      align-items: stretch;
      background-color: #f6f6f6;
      border: thin solid #bbb;
      color: #111;
      margin-left: 12px;
      margin-right: 12px;
      margin-top: -1px;
      padding-left: 3px;
      padding-right: 3px;

      &:first-of-type {
        margin-top: 8px;
      }

      &:last-of-type {
        margin-bottom: 8px;
      }

      .previous-value__header {
        max-width: 100%;
        display: flex;
        align-items: center;

        .previous-value__header-left {
          flex: 1
        }

        .previous-value__header-left, .previous-value__header-right {
          height: 16px;
          line-height: 1;
        }
      }
    }

    .form-field {
      background: #fff;
    }
  }
}

</style>
