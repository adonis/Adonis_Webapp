import FormField from '@adonis/app/models/evaluation-variables/form-field'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import PreviousValue from '@adonis/app/models/evaluation-variables/previous-value'
import Variable from '@adonis/app/models/evaluation-variables/variable'

/**
 * MeasureCell: regroup information required to show a form field as an inline component.
 */
export default class MeasureCell {

  constructor(
      public variableUri: string,
      public objectUri: string,
      public measure: Measure = null,
      public formField: FormField = null,
      public index = 0,
      public previousValue: PreviousValue = null,
  ) {
  }

  getKey(): string {
    if (this.measure) {
      return `${this.measure.uuid}-${this.index}`
    } else if (this.previousValue) {
      return`${this.objectUri}-${this.previousValue.originVariableUid}-${this.index}`
    } else {
      return `${this.variableUri}-${this.objectUri}-${this.index}`
    }
  }

}
