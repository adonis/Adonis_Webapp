<template>
  <v-dialog
      v-model="dialog"
      max-width="500px"
      scrollable
  >
    <template slot="activator" slot-scope="{ on }">
      <slot name="btn" v-bind:on="on">
        <v-btn
            v-on="on"
            :color="0 === filteredTests.length ? type : 'info'"
            :icon="icon"
            :small="icon"
            :x-small="!icon"
            fab
            light
        >
          <v-icon>{{ activatorIcon }}</v-icon>
        </v-btn>
      </slot>
    </template>
    <v-card v-if="dialog">
      <v-card-title>
        {{ $t('dataEntry.form.variableViewer.title') }}
      </v-card-title>
      <v-card-text>
        <div>
          {{ $t('dataEntry.form.variableViewer.variable', {name: variable.name}) }}
        </div>
        <div>
          <span>{{ $t('dataEntry.form.variableViewer.type', {type: vtypeString}) }}</span>
          <v-icon small>{{ activatorIcon }}</v-icon>
        </div>
        <v-alert
            v-if="!!variable.comment && '' !== variable.comment"
            color="info"
            class="mt-2 mb-0 no-mx"
            text
        >
          {{ variable.comment }}
        </v-alert>
        <div
            v-for="(test, testI) in filteredTests"
            :key="testI"
            class="mt-2"
        >
          <div
              v-for="(testDescription, descI) in testDescriptions(test)"
              :key="descI"
          >
            {{ testDescription }}
          </div>
        </div>
      </v-card-text>
      <v-card-actions>
        <v-btn
            class="mx-auto"
            color="secondary"
            dark
            rounded
            @click="dialog = false"
        >
          {{ $t('dataEntry.form.variableViewer.closeBtnLabel') }}
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
</template>

<script lang="ts">
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'

import Variable from '@adonis/app/models/evaluation-variables/variable'
import CombinationTest from '@adonis/app/models/field-tests/combination-test'
import GrowthTest from '@adonis/app/models/field-tests/growth-test'
import PreconditionedCalculation from '@adonis/app/models/field-tests/preconditioned-calculation'
import RangeTest from '@adonis/app/models/field-tests/range-test'
import Test from '@adonis/app/models/field-tests/test'
import UtilityService from '@adonis/app/services/utility-service'
import {Component, Prop, Vue} from 'vue-property-decorator'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum";

/**
 * Dialog component allowing the user to see information about a specific variable as dialog.
 */
@Component( {} )
export default class DialogVariableViewer extends Vue {

  /** The concerned project. */
  @Prop()
  project: DataEntryProject

  /** The variable to be shown. */
  @Prop()
  variable: Variable

  /** Defines the color of the dialog activator button. */
  @Prop( { default: 'tertiary' } )
  type: string

  /** Is icon button. */
  @Prop( { default: false } )
  icon: boolean

  /** Defines wether the dialog is open or not. */
  dialog: boolean = false

  /** Get the active tests of the variable. */
  get filteredTests(): Test[] {
    return this.variable.tests.filter( test => test.active )
  }

  /** Get the icon of the activator button according to variable type. */
  get activatorIcon(): string {
    let icon: string = 'help'
    if (!!this.variable) {
      switch (this.variable.type) {
        case VariableTypeEnum.REAL:
          icon = 'filter_9_plus'
          break
        case VariableTypeEnum.INTEGER:
          icon = 'filter_1'
          break
        case VariableTypeEnum.DATE:
          icon = 'event'
          break
        case VariableTypeEnum.HOUR:
          icon = 'timer'
          break
        case VariableTypeEnum.ALPHANUMERIC:
          icon = 'text_fields'
          break
        case VariableTypeEnum.BOOLEAN:
        default:
          icon = 'help_outline'
          break
      }
    }
    return icon
  }

  get vtypeString(): string {
    return UtilityService.variableTypeToString( this, this.variable )
  }

  testDescriptions( test: Test ): string[] {
    let testDescriptions: string[] = []
    if (test instanceof RangeTest) {
      testDescriptions.push( this.$t( 'dataEntry.form.tests.rangeTest' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.rangeError.mandatoryInterval', {
        min: (test as RangeTest).mandatoryMinValue,
        max: (test as RangeTest).mandatoryMaxValue,
      } ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.rangeError.optionalInterval', {
        min: (test as RangeTest).optionalMinValue,
        max: (test as RangeTest).optionalMaxValue,
      } ).toString() )
    } else if (test instanceof CombinationTest) {
      testDescriptions.push( this.$t( 'dataEntry.form.tests.combinationTest' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.combinationError.textualFormula', {
        first: this.project.variablesMap.get( test.firstOperandUid ).name,
        op: test.operator,
        second: this.project.variablesMap.get( test.secondOperandUid ).name,
      } ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.combinationError.condition' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.combinationError.conditionFormula', {
        min: test.lowLimit,
        max: test.highLimit,
      } ).toString() )
    } else if (test instanceof PreconditionedCalculation) {
      testDescriptions.push( this.$t( 'dataEntry.form.tests.preconditionedTest' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.preconditionedCalculation.calculation' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.preconditionedCalculation.calculationFormula', {
        var: this.variable.name,
        value: test.affectationValue,
      } ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.preconditionedCalculation.rule' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.preconditionedCalculation.ruleFormula', {
        var1: this.project.variablesMap.get( test.comparisonVariableUid ).name,
        var2: test.comparedVariableUid
            ? this.project.variablesMap.get( test.comparedVariableUid ).name
            : test.comparedValue
        ,
        condition: test.condition,
      } ).toString() )
    } else if (test instanceof GrowthTest) {
      const comparisonVariableName = this.project.variablesMap.get( test.comparisonVariableUid ).name
      testDescriptions.push( this.$t( 'dataEntry.form.tests.growthTest' ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.growthError.comparisonVariable', {
        variable: comparisonVariableName,
      } ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.growthError.differenceRule', {
        variable: comparisonVariableName,
      } ).toString() )
      testDescriptions.push( this.$t( 'dataEntry.form.tests.growthError.textualFormula', {
        var1: comparisonVariableName,
        var2: this.variable.name,
        min: test.minDiff,
        max: test.maxDiff,
      } ).toString() )
    }
    return testDescriptions
  }

}
</script>

<style lang="scss" scoped>

.no-mx {
  margin-left: -1rem;
  margin-right: -1rem;
}

</style>

