<template>
  <v-dialog
      v-model="active"
      fullscreen
      persistent
      scrollable
  >
    <template v-slot:activator="{ on }">
      <v-badge
          :content="countNotesItem"
          :value="countNotesItem"
          color="primary"
          offset-x="20"
          offset-y="15"
          overlap
      >
        <v-btn
            v-on="on"
            color="secondary"
            :class="totalNotes > 0 ? 'bordered' : ''"
            dark
            fab
            small
        >
          <v-icon>note_add</v-icon>
        </v-btn>
      </v-badge>
    </template>
    <v-card tile>
      <v-card-title class="pa-0">
        <notepad-selector
            :object.sync="selectedObject"
            :branch="objectBranch"
            :total="totalNotes"
        ></notepad-selector>
      </v-card-title>
      <v-divider></v-divider>
      <v-card-text
          class="pa-0 grey lighten-4 d-flex flex-column align-end"
          id="note-container"
          ref="note-container"
      >
        <notepad-viewer
            v-for="(note, index) in notes"
            :key="`note-${index}`"
            :class="0 === index ? 'mt-auto' : ''"
            :note="note"
            :expanded="index === notes.length - 1"
        >
        </notepad-viewer>
        <div
            class="px-2 py-1 mt-auto"
            v-if="0 === notes.length"
        >
          <v-alert
              color="tertiary"
              border="bottom"
          >
            {{ $t('dataEntry.form.notepad.emptyAlert') }}
          </v-alert>
        </div>
      </v-card-text>
      <v-card-actions>
        <v-textarea
            v-model="newTextNote"
            :label="$t('dataEntry.form.notepad.newNoteLabel')"
            ref="newNoteTextArea"
            rows="1"
            row-height="1rem"
            auto-grow
            hide-details
            @focus="onTextAreaFocus"
            @blur="stopSpeechRecognition"
        ></v-textarea>
      </v-card-actions>
      <v-card-actions>
        <v-btn
            :color="speechRecognition ? 'primary' : 'secondary'"
            v-if="$speechRecognition.isAvailable()"
            dark
            fab
            small
            class="mx-2"
            @click="toogleSpeechRecognition"
        >
          <v-icon>keyboard_voice</v-icon>
        </v-btn>
        <v-spacer></v-spacer>
        <v-btn
            color="primary"
            dark
            rounded
            @click="addNode"
        >{{ $t('dataEntry.form.notepad.addBtnLabel') }}
        </v-btn>
        <v-btn
            color="secondary"
            dark
            rounded
            @click="active = !active"
        >{{ $t('dataEntry.form.notepad.closeBtnLabel') }}
        </v-btn>
      </v-card-actions>
    </v-card>
  </v-dialog>
</template>

<script lang="ts">
import BusinessBranch from '@adonis/app/models/app-functionalities/business-branch'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import Note from '@adonis/app/models/entry-notes/note'
import PlatformService from '@adonis/app/services/platform-service'
import User from '@adonis/shared/models/user'
import { Component, Prop, Vue, Watch } from 'vue-property-decorator'

/**
 * Component allowing the operator to manage note on structural objects.
 */
@Component( {
  components: {
    'notepad-viewer': () => import('@adonis/app/modules/data-entry/form/notepad/NotepadViewer.vue'),
    'notepad-selector': () => import('@adonis/app/modules/data-entry/form/notepad/NotepadSelector.vue'),
  },
} )
export default class NotepadManager extends Vue {

  /** Defines if the manager is active or not. */
  active = false

  /** The object in current position. */
  @Prop()
  object: BusinessObject

  /** The object being annotate. */
  selectedObject: BusinessObject = null

  /** New note temporal container. */
  newTextNote = ''

  /** Define if the speech recognition is enabled */
  speechRecognition = false

  /** The current object branch hierarchy. */
  get objectBranch(): BusinessBranch {
    return PlatformService.getBranchObjects( this.object )
  }

  /** Get object's note. */
  get notes(): Note[] {
    let noteItems: Note[] = []
    if (!!this.selectedObject) {
      noteItems = this.selectedObject.notes
          .filter( ( note: Note ) => {
            return !!note && !note.deleted
          } )
          .sort( ( noteA: Note, noteB: Note ) => {
            // Ordering note so that the newest is the last one.
            return noteA.creationDate < noteB.creationDate
                ? -1
                : +1
          } )
    }
    return noteItems
  }

  /** Get count on note marked as active from any object in hierarchy branch. */
  get totalNotes(): number {
    let count = 0
    if (!!this.objectBranch) {
      const updateNoteCount = ( notesArray: Note[] ) => {
        count += notesArray.filter( ( note: Note ) => {
          return !!note && !note.deleted
        } ).length
      }
      if (!!this.objectBranch.individual) {
        updateNoteCount( this.objectBranch.individual.notes )
      }
      if (!!this.objectBranch.parcel) {
        updateNoteCount( this.objectBranch.parcel.notes )
      }
      if (!!this.objectBranch.subBlock) {
        updateNoteCount( this.objectBranch.subBlock.notes )
      }
      if (!!this.objectBranch.block) {
        updateNoteCount( this.objectBranch.block.notes )
      }
      if (!!this.objectBranch.device) {
        updateNoteCount( this.objectBranch.device.notes )
      }
    }
    return count
  }

  /** Get count on note marked as active from the current object. */
  get countNotesItem(): number {
    let count = 0
    count += this.object.notes.filter( ( note: Note ) => {
      return !!note && !note.deleted
    } ).length

    return count
  }

  /**
   * Initialize component.
   */
  mounted(): void {
    this.onCurrentObjectChange()
    this.scrollContainerToBottom()
  }

  /**
   * Handle parent object change.
   */
  @Watch( 'object' )
  onCurrentObjectChange(): void {
    this.selectedObject = this.object
  }

  /**
   * Scroll the note container to its bottom.
   */
  @Watch( 'active' )
  scrollContainerToBottom(): void {
    this.$nextTick( () => {
      if (this.active) {
        const refNoteContainer = this.$refs['note-container'] as any
        if (!!refNoteContainer) {
          setTimeout( () => {
            refNoteContainer.lastElementChild.scrollIntoView()
          }, 200 )
        }
      }
    } )
  }

  /**
   * Add a note to the object. Takes the value of field newNote and generate a note instance
   * before pushing it in object's note array.
   */
  addNode(): void {
    const noteValue = this.newTextNote.trim()
    if (0 < noteValue.length) {
      let user: User = this.$store.getters['connection/user']
      const newNote = new Note( noteValue, false, `/User.${user.id}`, user.username, new Date() )
      this.selectedObject.notes.push( newNote )
      this.newTextNote = ''
      this.$emit( 'save' )
      setTimeout(
          this.scrollContainerToBottom,
          300,
      )
    }
  }

  /**
   * Define the handler for the new note text area to activate the speech recognition
   */
  onTextAreaFocus(): void {
    if (this.speechRecognition) {
      // Define if the recognition had to be restarted after end
      let restart = false
      let newNote = this.newTextNote
      // Triggered each time the service recognize a part of the speech
      const onResult = ( event: SpeechRecognitionEvent ): void => {
        restart = true
        const recognitionResult = event.results.item( 0 )
        this.newTextNote = newNote + recognitionResult.item( 0 ).transcript + ' '
        // Define if the value will not be modified anymore by the service
        if (recognitionResult.isFinal) {
          if (recognitionResult.item( 0 ).transcript.toLowerCase().match( this.$t( 'dataEntry.form.dentryFields.speechRecognition.remove' ).toString() )) {
            this.newTextNote = ''
          }
          newNote = this.newTextNote
        }
      }
      const onEnd = ( event: Event ): void => {
        if (restart) {
          restart = false
          this.$speechRecognition.startRecognition( this.$root.$i18n.locale, onResult, onEnd )
        }
      }
      this.$speechRecognition.startRecognition( this.$root.$i18n.locale, onResult, onEnd )
    }
  }

  stopSpeechRecognition() {
    this.$speechRecognition.stopRecognition()
  }

  toogleSpeechRecognition() {
    this.speechRecognition = !this.speechRecognition
    if (this.speechRecognition) {
      // @ts-ignore
      this.$refs.newNoteTextArea.focus()
    } else {
      this.stopSpeechRecognition()
    }
  }

}
</script>
<style scoped>
.bordered {
  border: solid #678815 !important;
}
</style>

