import { FREE_MODE_LOCK, FREE_MODE_OPEN } from '@adonis/app/constants'
import BreadcrumbData from '@adonis/app/models/app-functionalities/breadcrumb-data'
import BusinessBranch from '@adonis/app/models/app-functionalities/business-branch'
import MovementAlertInterface from '@adonis/app/models/app-functionalities/movement-alert-interface'
import Block from '@adonis/app/models/business-objects/block'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import Device from '@adonis/app/models/business-objects/device'
import Individual from '@adonis/app/models/business-objects/individual'
import SubBlock from '@adonis/app/models/business-objects/sub-block'
import UnitParcel from '@adonis/app/models/business-objects/unit-parcel'
import { HasAnnotations } from '@adonis/app/models/evaluation-variables/annotation'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import FormFieldGroup from '@adonis/app/models/evaluation-variables/form-field-group'
import RequiredAnnotation from '@adonis/app/models/evaluation-variables/required-annotation'
import StateCode, { CODE_TYPE_DEAD, STATE_NONE } from '@adonis/app/models/evaluation-variables/state-code'
import BaseView from '@adonis/app/modules/base-view'
import DataEntryService from '@adonis/app/services/data-entry-service'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import PlatformService from '@adonis/app/services/platform-service'
import { TYPE_PRIMARY } from '@adonis/shared/constants'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import MainView from '@adonis/shared/MainView.vue'
import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { ConfirmAction } from '@adonis/shared/models/confirm-action'
import SignalSound from '@adonis/shared/sounds/audio-beep'
import { Component, Vue } from 'vue-property-decorator'

/**
 * FormHelper.
 */
@Component({})
export default class FormHelper extends BaseView {

  /** Flag to know if the navigation is opened. */
  navigationOpen = false

  /** Opened project. */
  project: DataEntryProject = null

  /** Opened form field group. */
  formFieldGroup: FormFieldGroup = null

  /** Get the instance of root component. */
  get mainView(): any {
    return this.$root as MainView
  }

  /** Get if movement locked or not. */
  get isFreeMode(): boolean {
    return FREE_MODE_LOCK !== this.$store.getters['dataEntry/getFreeMode']
  }

  /** Get if free mode is active or not. */
  get isFreeModeOpen(): boolean {
    return FREE_MODE_OPEN === this.$store.getters['dataEntry/getFreeMode']
  }

  /** Do print flag. */
  get doPrint(): boolean {
    return this.$store.getters['dataEntry/getDoPrint']
  }

  /** Breadcrumb data of field group. */
  get breadcrumbData(): BreadcrumbData {
    return !!this.formFieldGroup
        ? this.formFieldGroup.breadcrumbData
        : null
  }

  /** Get the object. */
  get object(): Evaluable {
    return !!this.formFieldGroup
        ? this.formFieldGroup.object
        : null
  }

  /** Get the object name. */
  get objectName(): string {
    return !!this.formFieldGroup
        ? this.formFieldGroup.objectName
        : null
  }

  /** Get the object fields. */
  get formFields(): FormField[] {
    return !!this.formFieldGroup
        ? this.formFieldGroup.objectFields.filter(ff => this.project.variablesMap.get(ff.variableUri).active)
        : []
  }

  /** Get the object tree. */
  get objectTree(): HasAnnotations[] {
    return !!this.formFieldGroup
        ? this.formFieldGroup.objectTree
        : []
  }

  /** User parameter to automatically pass to next object on last field validation. */
  get lastFieldDoMove(): boolean {
    return !!this.project && !!this.project.dataEntry && this.project.dataEntry.lastFieldDoMove
  }

  /** Getter from store for auto increment. */
  get increments(): number[] {
    return this.$store.getters['dataEntry/getIncrements']
  }

  /** Setter for auto increment, in case of update. */
  set increments(value: number[]) {
    this.$store.dispatch('dataEntry/setIncrements', {increments: value})
        .then()
  }

  /** Getter movement alert. */
  get movementAlert(): MovementAlertInterface {
    return !!this.project && !!this.project.dataEntry && !!this.project.dataEntry.movementAlertMap && !!this.object
        ? this.project.dataEntry.movementAlertMap.get(this.object.uri)
        : null
  }

  /**
   * Validate the current form.
   * Will check the required field to ask for confirmation before resolving the promise.
   *
   * @return Promise<boolean>
   */
  validate(): Promise<boolean> {

    return new Promise<boolean>((resolve: (...args: any) => void) => {

      const doResolve = (done: boolean) => {
        this.handleProjectCompletion()
        resolve(done)
      }

      const missingCount = FormMeasureService.extractMissingMeasures(this.project, this.formFields, true).length

      const reduceBooleanTab = (previous: boolean, current: boolean) => previous && current

      const verifyFormField = (formField: FormField): boolean => {
        const generatedFieldsValid: boolean = !formField.generatedFields ||
            formField.generatedFields.map((generated) => generated.formFields.map(verifyFormField)
                .reduce(reduceBooleanTab, true))
                .reduce(reduceBooleanTab, true)
        return generatedFieldsValid &&
            formField.measures.map(measure => FormMeasureService.isMissingMeasure(measure) ||
                measure.state !== STATE_NONE ||
                FormMeasureService.validateFormFieldContent(this.project.variablesMap.get(formField.variableUri), measure))
                .reduce(reduceBooleanTab, true)
      }

      if (
          !this.formFields.map(verifyFormField)
              .reduce(reduceBooleanTab, true)
      ) {
        const action: AsyncConfirmPopup = new AsyncConfirmPopup(
            this.$t('dataEntry.form.eDentryVariableForm.error.popupTitle')
                .toString(),
            [
              this.$t('dataEntry.form.eDentryVariableForm.error.popupMessage')
                  .toString(),
            ],
            [
              new ConfirmAction(
                  this.$t('dataEntry.form.eDentryVariableForm.error.confirmPopup')
                      .toString(),
                  TYPE_PRIMARY,
              ),
            ],
        )
        this.$store.dispatch('confirm/askConfirm', action)
            .then()
        doResolve(false)
      } else if (0 < missingCount) {
        const action: AsyncConfirmPopup = FormMeasureService.createConfirmValueMissing(this, doResolve)
        this.$store.dispatch('confirm/askConfirm', action)
            .then()
      } else {
        doResolve(true)
      }
    })
  }

  /**
   * After validate, dispatches the data entry update with the given movement.
   *
   * @param movement
   */
  move(movement = +1): void {
    this.mainView.addLoading()
    this.validate()
        .then((continueValidation: boolean) => {
          if (continueValidation) {
            this.$store.dispatch(
                'dataEntry/move',
                {
                  vm: this,
                  formFieldGroup: this.formFieldGroup,
                  movement,
                },
            )
                .then(() => {
                  this.onMoved()
                })
          }
        })
        .finally(() => {
          setTimeout(() => {
            this.mainView.removeLoading()
          }, 350)
        })
  }

  /**
   * When validation form is request.
   *
   * @param target
   */
  validForm(target: BusinessObject = null): void {
    this.mainView.addLoading()
    this.validate()
        .then((continueValidation: boolean) => {
          if (continueValidation) {
            if (!target) {
              // In cas of Individual, if an auto increment is defined, apply it and move to the new position when exists.
              if (
                  this.isFreeModeOpen &&
                  (this.formFieldGroup.object instanceof Individual || this.formFieldGroup.object instanceof UnitParcel) &&
                  (1 !== this.increments[0] || 1 !== this.increments[1])
              ) {
                const newX = this.formFieldGroup.object.x + this.increments[0] - 1
                const newY = this.formFieldGroup.object.y + this.increments[1] - 1
                target = this.project.platform.positionMap.get(`${newX},${newY}`) as BusinessObject
                if (!DataEntryService.evaluableInWorkpath(this.project, target)) {
                  this.mainView.showAlert(
                      'warning',
                      this.$t('dataEntry.form.position.autoIncrement.notValidPosition')
                          .toString(),
                  )
                  this.navigationOpen = true
                  this.increments = [1, 1]
                }
              } else {
                this.navigationOpen = true
              }
            }
            this.$store.dispatch(
                'dataEntry/goto',
                {
                  vm: this,
                  formFieldGroup: this.formFieldGroup,
                  target,
                },
            )
                .then(() => {
                  if (!!target) {
                    this.onMoved()
                  }
                })
          }
        })
        .finally(() => {
          setTimeout(() => {
            this.mainView.removeLoading()
          }, 350)
        })
  }

  /**
   * Callback when "move" action is done. Allows according to the specific situation to update the
   * vue component fields.
   */
  onMoved(): void | Promise<void> {
    throw new Error('Method not yet implemented.')
  }

  /**
   * Run audio beep.
   */
  playSound(): void {
    if (!!this.movementAlert && !!this.project.dataEntry.movementSound) {
      new Audio(SignalSound.use(this.project.dataEntry.movementSound)).play()
          .then()
    }
  }

  /**
   * In case of "locked navigation mode", shows a popup to inform the operator that the end of the workpath has
   * been reached. Handle the case when not all requested measures have been set.
   */
  async handleProjectCompletion(): Promise<void> {

    if (this.$store.getters['dataEntry/isEndOfWorkpath']) {
      await this.handleProjectCompletionAnnotations()

      let confirmPopup: AsyncConfirmPopup
      if (this.$store.getters['dataEntry/isProjectComplete']) {
        // When project is complete, informs the project can be closed.
        confirmPopup = new AsyncConfirmPopup(
            this.$t('dataEntry.endProject.title')
                .toString(),
            [
              this.$t('dataEntry.endProject.message1')
                  .toString(),
              this.$t('dataEntry.endProject.message2')
                  .toString(),
            ],
            [
              new ConfirmAction(
                  this.$t('dataEntry.endProject.okBtnLabel')
                      .toString(),
                  'primary',
              ),
            ],
        )
      } else {
        // When project is not complete, inform about the situation.
        confirmPopup = new AsyncConfirmPopup(
            this.$t('dataEntry.endWorkpath.title')
                .toString(),
            [
              this.$t('dataEntry.endWorkpath.message1')
                  .toString(),
              this.$t('dataEntry.endWorkpath.message2')
                  .toString(),
            ],
            [
              new ConfirmAction(
                  this.$t('dataEntry.endWorkpath.okBtnLabel')
                      .toString(),
                  'primary',
              ),
            ],
        )
      }
      this.$store.dispatch('confirm/askConfirm', confirmPopup)
          .then()
    }
  }

  async handleProjectCompletionAnnotations() {
    if (!this.project) {
      return
    }
    const currentEvaluable = PlatformService.getCurrentEvaluable(this.project)
    const currentBranch = PlatformService.getBranchObjects(currentEvaluable)
    await Promise.all(
    this.project.requiredAnnotations.map(async (requiredAnnotation: RequiredAnnotation) => new Promise((resolvePromise) =>  {
      const currentTarget = PlatformService.getObjectOfLevelFromBranch(currentBranch, requiredAnnotation.pathLevel) as Individual | UnitParcel | SubBlock | Block | Device
      if (
          !requiredAnnotation.buisnessObjects ||
          0 === requiredAnnotation.buisnessObjects.length ||
          requiredAnnotation.buisnessObjects.includes(currentTarget.uri)
      ) {
        this.askRequiredAnnotation(requiredAnnotation, currentTarget, resolvePromise)
      } else {
        resolvePromise()
      }
    })),
    )
  }

  /**
   * Switch from one dentry field to the next one.
   * When called by the last field, start "move" to the next object.
   *
   * @param i
   */
  switchFrom(i: number): void {
    const validate = () => {
      if (!!this.object && this.isFreeMode) {
        this.validForm(null)
      } else {
        this.move(+1)
      }
    }
    if (this.formFields[i].measures[0].state.type === CODE_TYPE_DEAD) {
      validate()
    } else if (!!this.formFields && i + 1 < this.formFields.length) {
      const inputs = this.$refs.input as Vue[]
      const input = inputs[i + 1] as Vue & { select: () => any } // Field component that can be selected.
      input.select()
    } else if (!!this.formFields && i + 1 === this.formFields.length && this.lastFieldDoMove) {
      validate()
    }
  }

  /**
   * Perform state code propagation over the target in the project.
   *
   * @param targetUri
   * @param state
   * @param forcePropagationLevel
   */
  propagateStateCode(targetUri: string, state: StateCode, forcePropagationLevel: PathLevelEnum = null): void {
    FormMeasureService.propagateStateCode(this.$indexedDb, this.project, targetUri, state, forcePropagationLevel)
        .then(() => DataEntryService.saveProjectToDatabase(this, this.project, true, true)
            .then(() => {
              this.onMoved()
            }))
  }

  /**
   * Handle semi-automatic variable measure push request.
   *
   * @param measures
   */
  onSemiAutoMeasures(measures: Map<string, any>): void {
    FormMeasureService.onSemiAutoMeasures(this.formFieldGroup, measures)
  }

  /**
   * Handle required annotation associated with current opened project.
   * One after the other, the required annotation will be handled by specific promise.
   */
  protected handleAllRequiredAnnotations(): void {
    if (!this.project) {
      return
    }
    const currentEvaluable = PlatformService.getCurrentEvaluable(this.project)
    const lastEvaluable = PlatformService.getLastEvaluable(this.project)
    const currentBranch = PlatformService.getBranchObjects(currentEvaluable)
    const lastBranch = PlatformService.getBranchObjects(lastEvaluable)
    if (!!currentEvaluable) {
      const handleAll = async () => {
        const size = this.project.requiredAnnotations.length
        for (let index = 0; index < size; index++) {
          const requiredAnnotation = this.project.requiredAnnotations[index]
          await this.handleRequiredAnnotation(requiredAnnotation, currentBranch, lastBranch)
        }
      }
      handleAll()
          .then()
    }
  }

  /**
   * annotation popup. Will open a popup if a required annotation matches the situation :
   * Last target when leaving the object, or current target when entering the object.
   * Call promise resolve callback when the operator answer to annotation popup or if the
   * annotation doesn't apply to the situation.
   *
   * @param requiredAnnotation
   * @param currentBranch
   * @param lastBranch
   *
   * @return Promise<void>
   */
  private handleRequiredAnnotation(
      requiredAnnotation: RequiredAnnotation,
      currentBranch: BusinessBranch,
      lastBranch: BusinessBranch,
  ): Promise<void> {
    return new Promise<void>((resolve: (...args: any) => void) => {
      if (!requiredAnnotation.active) {
        resolve()
        return
      }
      // Only Handle required annotation marked as active.
      // tslint:disable-next-line:max-line-length
      const lastTarget = PlatformService.getObjectOfLevelFromBranch(lastBranch, requiredAnnotation.pathLevel) as Individual | UnitParcel | SubBlock | Block | Device
      // tslint:disable-next-line:max-line-length
      const currentTarget = PlatformService.getObjectOfLevelFromBranch(currentBranch, requiredAnnotation.pathLevel) as Individual | UnitParcel | SubBlock | Block | Device
      const requiredAnnotationDoApply = (target1: BusinessObject, target2: BusinessObject): boolean => {
        return !!target1 &&
            (
                !requiredAnnotation.buisnessObjects ||
                0 === requiredAnnotation.buisnessObjects.length ||
                requiredAnnotation.buisnessObjects.includes(target1.uri)
            ) &&
            (!target2 || target1.uri !== target2.uri)
      }
      if (requiredAnnotation.askWhenEntering) {
        // Required annotation to be written on entering object.
        const doAction = requiredAnnotationDoApply(currentTarget, lastTarget)
        if (doAction && 0 === currentTarget.annotations.length) {
          this.askRequiredAnnotation(requiredAnnotation, currentTarget, resolve)
        } else {
          resolve()
        }
      } else {
        // Required annotation to be written on leaving object.
        const doAction = requiredAnnotationDoApply(lastTarget, currentTarget)
        if (doAction && 0 === lastTarget.annotations.length) {
          this.askRequiredAnnotation(requiredAnnotation, lastTarget, resolve)
        } else {
          resolve()
        }
      }
    })
  }

  /**
   * Dispatches the required annotation in the store. A listener is set to open the popup to ask the operator
   * about the required annotation.
   *
   * @param requiredAnnotation
   * @param target
   * @param onContinue
   */
  private askRequiredAnnotation(
      requiredAnnotation: RequiredAnnotation,
      target: Evaluable,
      onContinue: (...args: any) => void,
  ): void {
    this.$store.dispatch('annotation/askAnnotation', {requiredAnnotation, target, onContinue})
        .then()
  }

}
