import Variable from '@adonis/app/models/evaluation-variables/variable'

export class VariableHeader {
  constructor(
      public action: string,
      public label: string,
      public variable: Variable = null,
      public previous: boolean = false,
  ) {
  }
}
