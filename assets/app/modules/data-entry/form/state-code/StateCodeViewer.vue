<template>
  <div class="high-contrast-field">
    <v-text-field
        ref="input"
        :class="className"
        :label="label"
        :value="showStatus(measure)"
        :clearable="isClearable"
        color="error"
        readonly
        @keydown="onKeydown"
        @click:clear="deleteCurrentState"
    >
      <template v-slot:prepend>

        <slot name="variable-viewer"></slot>

      </template>
      <template v-slot:append-outer>

        <dentry-field-addon
            :active="active"
            :measure="measure"
            :project="project"
            @propagate="propagateStateCode"
            @remove-propagate="removePropagateStateCode"
        ></dentry-field-addon>

      </template>
    </v-text-field>
    <portal to="confirmReplantingPopupBody">
      <v-menu
          v-model="open"
          :close-on-content-click="false"
          :nudge-right="40"
          min-width="290px"
          offset-y
          transition="scale-transition"
      >
        <template slot="activator" slot-scope="{ on }">
          <template>
            <v-text-field
                v-on="on"
                :label="$t('dataEntry.form.state.replantingPopup.dateLabel')"
                :value="toDateString(plantationDate)"
                readonly
                @keypress.enter.prevent="open = false"
            >
            </v-text-field>
          </template>
        </template>

        <v-date-picker
            v-model="plantationDate"
            no-title
            @input="open = false"
        ></v-date-picker>
      </v-menu>
    </portal>
  </div>
</template>

<script lang="ts">
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import Individual from '@adonis/app/models/business-objects/individual'
import UnitParcel from '@adonis/app/models/business-objects/unit-parcel'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import StateCode, { CODE_TYPE_DEAD, CODE_TYPE_MISSING, STATE_NONE, } from '@adonis/app/models/evaluation-variables/state-code'
import Variable from '@adonis/app/models/evaluation-variables/variable'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import UtilityService from '@adonis/app/services/utility-service'
import PathLevelEnum from '@adonis/shared/constants/path-level-enum'
import { Component, Prop, Vue, Watch } from 'vue-property-decorator'

/**
 * Component to show to the operator the actual state of the measure if a state
 * is defined.
 */
@Component( {
  components: {
    'dentry-field-addon': () => import('../dentry-fields/DentryFieldAddon.vue'),
  },
} )
export default class StateCodeViewer extends Vue {

  /** Running project. */
  @Prop()
  project: DataEntryProject

  /** Variable concerned. */
  @Prop()
  variable: Variable

  /** The measure. */
  @Prop()
  measure: Measure

  /** Label of the field. */
  @Prop( { default: '' } )
  label: string

  /** Allows to add css classes. */
  @Prop( { default: '' } )
  className: string

  /** Is Field active. */
  @Prop( { default: true } )
  active: boolean

  /** Plantation date. */
  plantationDate: string = null

  /** Open state of replanting popup in cas of "dead" code remove. */
  open: boolean = false

  /** Defines whether the state can be removed in one click on clear icon. */
  get isClearable(): boolean {
    return !!this.measure.state && CODE_TYPE_MISSING === this.measure.state.type
  }

  /**
   * Gets the status corresponding to the given measure's state.
   *
   * @param measure
   *
   * @return string
   */
  showStatus( measure: Measure ): string {
    return !!measure.state
        ? measure.state.label
        : '?'
  }

  /**
   * Select the current field.
   */
  select(): void {
    let repetition = 0
    const selectAction = () => {
      this.$nextTick( () => {
        let inputField = this.$refs.input as any
        if (!!inputField) {
          setTimeout( () => {
            inputField.focus()
          }, 200 )
        } else if (10 > repetition) {
          setTimeout( () => {
            ++repetition
            selectAction()
          }, 100 )
        }
      } )
    }
    selectAction()
  }

  /**
   * Propagate a state code by emitting event to parent.
   *
   * @param target
   * @param state
   * @param forcePropagationLevel
   */
  propagateStateCode(target: string, state: StateCode, forcePropagationLevel: PathLevelEnum = null): void {
    this.$emit( 'state-propagate', target, state, forcePropagationLevel )
  }

  /**
   * Remove state code, allows to handle state code type "propagate".
   * Handle confirm replanting popup.
   *
   * @param target
   * @param state
   * @param forcePropagationLevel
   */
  removePropagateStateCode(target: string, state: StateCode, forcePropagationLevel: PathLevelEnum = null): void {
    if (this.measure.state.type === CODE_TYPE_DEAD) {
      let confirmAction = () => {
        let object = this.project.platform.uriMap.get( this.measure.targetUri )
        if (object instanceof Individual || object instanceof UnitParcel) {
          object.aparitionDate = new Date( this.plantationDate )
        }
        this.propagateStateCode( target, STATE_NONE, forcePropagationLevel )
        setTimeout( () => {
          this.measure.state = state
          this.$emit( 'state-removed' )
        }, 435 )
      }
      this.$store.dispatch( 'confirm/askConfirm', FormMeasureService.createConfirmReplantingPopup( this, confirmAction ) )
    } else {
      this.propagateStateCode( target, STATE_NONE, forcePropagationLevel )
      setTimeout( () => {
        this.measure.state = state
        this.$emit( 'state-removed' )
      }, 435 )
    }
  }

  /**
   * Handle keydown shortcuts.
   *
   * @param event
   */
  onKeydown( event: any ): void {
    event.preventDefault()
    event.stopImmediatePropagation()
    if (this.active) {
      if (['Backspace', 'Delete'].includes( event.key )) {
        this.deleteCurrentState()
      } else if (['Enter'].includes( event.key )) {
        this.$emit( 'keypress-enter' )
      }
    }

  }

  /**
   * Allow to remove the current state of the field.
   */
  private deleteCurrentState() {
    if (this.measure.state.type === CODE_TYPE_DEAD || !!this.measure.state.getPropagation()) {
      this.removePropagateStateCode( this.measure.targetUri, STATE_NONE, this.measure.state.getPropagation() )
    } else {
      this.measure.state = STATE_NONE
      this.$emit( 'state-removed' )
    }
  }

  /**
   * Transform date to string representation.
   *
   * @param date
   *
   * @return string|null
   */
  toDateString( date: string | null ): string | null {
    return UtilityService.formatDate( new Date( date ) )
  }

  /**
   * Handle date change on "remove dead code".
   */
  @Watch( 'plantationDate' )
  onDateChange() {
    this.$store.dispatch( 'confirm/setLockButton', { index: 0, disable: !this.plantationDate } )
  }

}
</script>
