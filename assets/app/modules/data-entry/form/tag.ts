export class Tag {
  label: string
  name: string
  value: string
}
