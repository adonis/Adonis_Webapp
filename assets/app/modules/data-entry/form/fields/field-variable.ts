import App from '@adonis/app/App.vue'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import Annotation, { ANNOTATION_TYPE_TEXT } from '@adonis/app/models/evaluation-variables/annotation'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import StateCode from '@adonis/app/models/evaluation-variables/state-code'
import Variable from '@adonis/app/models/evaluation-variables/variable'
import CombinationTest from '@adonis/app/models/field-tests/combination-test'
import GrowthTest from '@adonis/app/models/field-tests/growth-test'
import PreconditionedCalculation from '@adonis/app/models/field-tests/preconditioned-calculation'
import RangeTest from '@adonis/app/models/field-tests/range-test'
import TestError from '@adonis/app/models/field-tests/test-error'
import TestErrorDialog from '@adonis/app/models/field-tests/test-error-dialog'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import TestVariableService from '@adonis/app/services/test-variable-service'
import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { ConfirmAction } from '@adonis/shared/models/confirm-action'
import { Component, Prop, Vue } from 'vue-property-decorator'

/**
 * Component that handle field value.
 * Regroup action that are common between each type of variable Adonis can handle.
 */
@Component( {} )
export default class FieldVariable extends Vue {

  /** The currently running data entry project. */
  @Prop()
  project: DataEntryProject

  /** Required to perform tests which depends on all form fields. */
  @Prop()
  formFields: FormField[]

  /** The variable to render this field. */
  @Prop()
  variable: Variable

  /** The measure that will be edited by this field. */
  @Prop()
  measure: Measure

  /** Allow to decide the label to display or not. */
  @Prop()
  label: string

  /** Allow to define a specific class that will be applied to the main field. */
  @Prop()
  className: string

  /** Field active. */
  @Prop( { default: false } )
  active: boolean

  /** Define the place of this field in the form (don't take care about repetitions) */
  @Prop()
  index: number

  hint = ''

  materialActive = false

  get isMaterialReady(): boolean {
    let ready = false
    if (!!this.variable.materialUid) {
      ready = !!this.project.materialsMap.get( this.variable.materialUid )
    }
    return ready && !this.measure.value && 0 !== this.measure.value
  }

  get disableNumeralKeyboard(): boolean {
    return !!this.project && !!this.project.dataEntry && !!this.project.dataEntry.disableNumeralKeyboard
  }

  /**
   * Will call continueSelect(). It will active all listener about the value change event.
   */
  select( deleteValue = false, skipTests = false ): void {
    setTimeout( () => {
      if (deleteValue) {
        this.measure.value = null
      }
      if (skipTests) {
        this.continueSelect()
      } else {
        TestVariableService.handleVariableTests( this.project, this.formFields, this.variable, this.measure, true )

            .then( () => {
              this.continueSelect()
            } )

            .catch( ( error: TestError ) => {
              // Only PreconditionedTest is handled on Field opening.
              if (error.test instanceof PreconditionedCalculation) {
                const errorData: TestErrorDialog = this.getPreconditionedCalculationTestErrorDialog( error );
                // @ts-ignore App class has the showTestError() method implemented.
                (this.$root as App).showTestError( errorData )
              }
            } )
      }
    }, 200 )
  }

  /**
   * Perform selection.
   */
  continueSelect(): void {
    // Using repetition structure to retry select action if the selected component is not yet mounted
    // when the function call occurred. Will repeat the action only 10 times, abort otherwise.
    let repetition = 0
    const selectAction = () => {
      this.$nextTick( () => {
        if (this.isMaterialReady) {
          this.activeMaterialPopup()
        } else if (!!this.$refs.input) {
          const input: any = this.$refs.input as any
          if (!!input) {
            input.focus()
            this.selected()
          } else if (repetition < 10) {
            setTimeout( () => {
              ++repetition
              selectAction()
            }, 100 )
          }
        }
      } )
    }
    selectAction()
  }

  /**
   * Method called when the input field is really selected.
   * Can be overridden by subclass to trigger some actions
   */
  selected(): void {
  }

  /**
   * Method called after blur and other treatments.
   * Can be overridden by subclass to trigger some actions
   */
  blurred(): void {
  }

  /**
   * When called, notify by event the parent component.
   *
   * @param doNext
   */
  onValueChange( doNext = true ): void {
    this.blurred()
    // Define what to do after some checks
    const whenValid = () => {
      this.measure.timestamp = new Date()
      if (this.handleStateCode( doNext )) {
        return
      }
      this.handleTests( doNext )
    }
    const missingMeasure = FormMeasureService.isMissingMeasure( this.measure )
    if (!missingMeasure && !FormMeasureService.validateFormFieldContent( this.variable, this.measure )) {
      this.hint = this.$t( 'dataEntry.form.eDentryVariableForm.error.invalidContent' )
          .toString()
    } else {
      this.hint = ''
    }
    if (doNext &&
        this.variable.mandatory &&
        missingMeasure &&
        this.index + 1 !== this.formFields.length) {
      this.askForMandatory( whenValid )
    } else {
      whenValid()
    }
  }

  /**
   * handle state code trigger
   * @param doNext
   * @return must stop treatment
   */
  handleStateCode( doNext: boolean ): boolean {
    const newState: StateCode = FormMeasureService.handleMeasureStateCode( this.measure, this.project.stateCodes )
    if (0 > newState.code) {
      this.measure.state = newState
      this.measure.value = null
      if (newState.mustPropagate( this.project.isCurrentSurfaceParcel )) {
        this.propagateStateCode( this.measure.targetUri, newState )
      } else if (doNext) {
        this.next()
      }
      return true
    }
    return false
  }

  handleTests( doNext: boolean ) {
    TestVariableService.handleVariableTests( this.project, this.formFields, this.variable, this.measure )

        .then( () => {
          if (doNext) {
            this.next()
          }
        } )

        .catch( ( error: TestError ) => {
          if (error.test instanceof RangeTest) {
            const errorData: TestErrorDialog = this.getRangeTestErrorDialog( error, doNext );
            // @ts-ignore App class has the showTestError() method implemented.
            (this.$root as App).showTestError( errorData )
          } else if (error.test instanceof CombinationTest) {
            const errorData: TestErrorDialog = this.getCombinationTestErrorDialog( error, doNext );
            // @ts-ignore App class has the showTestError() method implemented.
            (this.$root as App).showTestError( errorData )
          } else if (error.test instanceof PreconditionedCalculation) {
            // This test does not throw error, it is handled on field opening.
          } else if (error.test instanceof GrowthTest) {
            const errorData: TestErrorDialog = this.getGrowthTestErrorDialog( error, doNext );
            // @ts-ignore App class has the showTestError() method implemented.
            (this.$root as App).showTestError( errorData )
          } else {
            console.log( error )
            alert( 'Test error.' )
          }
        } )
  }

  askForMandatory( whenValid: () => any ) {
    const confirmPopup = new AsyncConfirmPopup(
        this.$t( 'dataEntry.form.validate.title' )
            .toString(),
        [
          this.$t( 'dataEntry.form.validate.mandatoryVariable' )
              .toString(),
        ],
        [
          new ConfirmAction(
              this.$t( 'dataEntry.form.validate.confirm' )
                  .toString(),
              'primary',
              () => {
                whenValid()
              },
          ),
          new ConfirmAction(
              this.$t( 'dataEntry.form.validate.cancel' )
                  .toString(),
              'error',
              () => {
                this.select( false, true )
              },
          ),
        ],
    )
    this.$store.dispatch( 'confirm/askConfirm', confirmPopup )
  }

  /**
   * Go to the next field.
   */
  next(): void {
    this.continueNext()
  }

  continueNext(): void {
    this.$emit( 'next' )
  }

  doCallNext(): void {
    this.$emit( 'next' )
  }

  /**
   * Fires event "propagate" to inform about propagation of state code.
   *
   * @param targetUri
   * @param state
   */
  propagateStateCode( targetUri: string, state: StateCode ): void {
    this.$emit( 'change:state', targetUri, state )
  }

  activeMaterialPopup(): void {
    this.materialActive = true
  }

  onSemiAutoMeasures( measures: Map<string, any> ): void {
    this.$emit( 'semi-auto:measured', measures )
    this.$nextTick( () => {
      if (!!this.measure.value || 0 === this.measure.value) {
        this.onValueChange( true )
      }
    } )
  }

  /**
   * Builds the popup information about an error thrown by a range test.
   *
   * @param error
   * @param doNext
   *
   * @return TestErrorDialog
   */
  private getRangeTestErrorDialog( error: TestError, doNext: boolean ): TestErrorDialog {
    return new TestErrorDialog( {

      dialog: true,
      title: this.$t( 'dataEntry.form.tests.title' )
          .toString(),

      variable: this.$t( 'dataEntry.form.tests.variable', { name: error.variable.name } )
          .toString(),
      test: this.$t( 'dataEntry.form.tests.test', { name: error.test.name } )
          .toString(),
      value: this.$t( 'dataEntry.form.tests.value', { value: error.measure.value } )
          .toString(),

      messages: [
        error.mandatory
            ? this.$t( 'dataEntry.form.tests.rangeError.outMandatory' )
                .toString()
            : this.$t( 'dataEntry.form.tests.rangeError.outOptional' )
                .toString(),
        this.$t( 'dataEntry.form.tests.rangeError.mandatoryInterval', {
          min: (error.test as RangeTest).mandatoryMinValue,
          max: (error.test as RangeTest).mandatoryMaxValue,
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.rangeError.optionalInterval', {
          min: (error.test as RangeTest).optionalMinValue,
          max: (error.test as RangeTest).optionalMaxValue,
        } )
            .toString(),
      ],

      confirm: error.mandatory
          ? this.$t( 'dataEntry.form.tests.rangeError.rewriteMandatory' )
              .toString()
          : this.$t( 'dataEntry.form.tests.rangeError.confirmOptional' )
              .toString()
      ,
      continueBtnLabel: this.$t( 'dataEntry.form.tests.continueLabel' )
          .toString(),
      cancelBtnLabel: this.$t( 'dataEntry.form.tests.cancelLabel' )
          .toString(),

      onConfirm: () => {
        if (error.mandatory) {
          // User asks to modify the value, return to the field.
          this.select( true )
        } else {
          // User confirms the value out of optional interval but in mandatory interval.
          if (doNext) {
            this.next()
          }
        }
      },

      onCancel: () => {
        if (error.mandatory) {
          // User confirms an unauthorized value, mark the measure as "missing value" and generate annotation.
          const name: string = this.$t( 'dataEntry.form.tests.rangeError.annotationTitle', {
            test: error.test.name,
          } )
              .toString()
          const value: string = this.$t( 'dataEntry.form.tests.rangeError.annotationText', {
            value: error.measure.value,
          } )
              .toString()
          const annotation: Annotation = new Annotation(
              name,
              ANNOTATION_TYPE_TEXT,
              value,
              null,
              new Date(),
              ['Mesure', 'Test'],
              [],
          )
          error.measure.annotations.push( annotation )
          error.measure.state = this.project.getStateCodeMissing()
        } else {
          // User does not confirm the value out of optional interval, return to the field.
          this.select( true )
        }
      },

      priority: doNext,
    } )
  }

  /**
   * Builds the popup information about an error thrown by a combination test.
   *
   * @param error
   * @param doNext
   *
   * @return TestErrorDialog
   */
  private getCombinationTestErrorDialog( error: TestError, doNext: boolean ): TestErrorDialog {
    return new TestErrorDialog( {

      dialog: true,
      title: this.$t( 'dataEntry.form.tests.title' )
          .toString(),

      variable: this.$t( 'dataEntry.form.tests.variable', { name: error.variable.name } )
          .toString(),
      test: this.$t( 'dataEntry.form.tests.test', { name: error.test.name } )
          .toString(),
      value: null,

      messages: [
        this.$t( 'dataEntry.form.tests.combinationError.messageValue' )
            .toString(),
        this.$t( 'dataEntry.form.tests.combinationError.textualFormula', {
          first: this.project.variablesMap.get( (error.test as CombinationTest).firstOperandUid ).name,
          op: (error.test as CombinationTest).operator,
          second: this.project.variablesMap.get( (error.test as CombinationTest).secondOperandUid ).name,
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.combinationError.evaluationFormula', {
          first: error.otherData.firstValue,
          op: (error.test as CombinationTest).operator,
          second: error.otherData.secondValue,
          value: error.otherData.evaluation,
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.combinationError.condition' )
            .toString(),
        this.$t( 'dataEntry.form.tests.combinationError.conditionFormula', {
          min: (error.test as CombinationTest).lowLimit,
          max: (error.test as CombinationTest).highLimit,
        } )
            .toString(),
      ],

      confirm: this.$t( 'dataEntry.form.tests.combinationError.confirmQuestion', {
        min: (error.test as CombinationTest).lowLimit,
        value: error.otherData.evaluation,
        max: (error.test as CombinationTest).highLimit,
      } )
          .toString()
      ,
      continueBtnLabel: this.$t( 'dataEntry.form.tests.continueLabel' )
          .toString(),
      cancelBtnLabel: this.$t( 'dataEntry.form.tests.cancelLabel' )
          .toString(),

      onConfirm: () => {
        // User confirms the value, go to the next field and register an annotation about the test failure.
        const name: string = this.$t( 'dataEntry.form.tests.combinationError.annotationTitle', {
          test: error.test.name,
        } )
            .toString()
        const value: string = this.$t( 'dataEntry.form.tests.combinationError.annotationText', {
          min: (error.test as CombinationTest).lowLimit,
          value: error.otherData.evaluation,
          max: (error.test as CombinationTest).highLimit,
        } )
            .toString()
        error.measure.annotations.push( new Annotation(
            name,
            ANNOTATION_TYPE_TEXT,
            value,
            null,
            new Date(),
            ['Mesure', 'Test'],
            [],
        ) )
        if (doNext) {
          this.next()
        }
      },

      onCancel: () => {
        // User does not confirm the value, return to the field.
        this.select( true )
      },

      priority: doNext,
    } )
  }

  /**
   * Builds the popup information about a possibility offered by a preconditioned calculation.
   *
   * @param error
   *
   * @return TestErrorDialog
   */
  private getPreconditionedCalculationTestErrorDialog( error: TestError ): TestErrorDialog {
    return new TestErrorDialog( {

      dialog: true,
      title: this.$t( 'dataEntry.form.tests.preconditionedCalculation.title' )
          .toString(),

      variable: this.$t( 'dataEntry.form.tests.variable', { name: error.variable.name } )
          .toString(),
      test: this.$t( 'dataEntry.form.tests.test', { name: error.test.name } )
          .toString(),
      value: null,

      messages: [
        this.$t( 'dataEntry.form.tests.preconditionedCalculation.calculation' )
            .toString(),
        this.$t( 'dataEntry.form.tests.preconditionedCalculation.calculationFormula', {
          var: error.variable.name,
          value: error.otherData.value,
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.preconditionedCalculation.rule' )
            .toString(),
        this.$t( 'dataEntry.form.tests.preconditionedCalculation.ruleFormula', {
          var1: this.project.variablesMap.get( error.otherData.var1 ).name,
          var2: !!error.otherData.var2
              ? this.project.variablesMap.get( error.otherData.var2 ).name
              : error.otherData.var2bis
          ,
          condition: error.otherData.condition,
        } )
            .toString(),
      ],

      confirm: this.$t( 'dataEntry.form.tests.preconditionedCalculation.confirmQuestion' )
          .toString(),
      continueBtnLabel: this.$t( 'dataEntry.form.tests.continueLabel' )
          .toString(),
      cancelBtnLabel: this.$t( 'dataEntry.form.tests.cancelLabel' )
          .toString(),

      onConfirm: () => {
        error.measure.value = error.otherData.value
        this.select( false, true )
      },

      onCancel: () => {
        this.select( false, true )
      },

      priority: true,
    } )
  }

  /**
   * Builds the popup information about an error thrown by a combination test.
   *
   * @param error
   * @param doNext
   *
   * @return TestErrorDialog
   */
  private getGrowthTestErrorDialog( error: TestError, doNext: boolean ): TestErrorDialog {
    const comparisonVariableName = this.project.variablesMap.get( (error.test as GrowthTest).comparisonVariableUid ).name
    return new TestErrorDialog( {

      dialog: true,
      title: this.$t( 'dataEntry.form.tests.title' )
          .toString(),

      variable: this.$t( 'dataEntry.form.tests.variable', { name: error.variable.name } )
          .toString(),
      test: this.$t( 'dataEntry.form.tests.test', { name: error.test.name } )
          .toString(),
      value: this.$t( 'dataEntry.form.tests.value', { value: error.measure.value } )
          .toString(),

      messages: [
        this.$t( 'dataEntry.form.tests.growthError.comparisonVariable', {
          variable: '',
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.growthError.comparisonFormula', {
          variable: comparisonVariableName,
          value: error.otherData.comparisonValue,
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.growthError.differenceRule', {
          variable: comparisonVariableName,
        } )
            .toString(),
        this.$t( 'dataEntry.form.tests.growthError.textualFormula', {
          var1: comparisonVariableName,
          var2: error.variable.name,
          min: (error.test as GrowthTest).minDiff,
          max: (error.test as GrowthTest).maxDiff,
        } )
            .toString(),
      ],

      confirm: this.$t( 'dataEntry.form.tests.growthError.confirmQuestion' )
          .toString(),
      continueBtnLabel: this.$t( 'dataEntry.form.tests.continueLabel' )
          .toString(),
      cancelBtnLabel: this.$t( 'dataEntry.form.tests.cancelLabel' )
          .toString(),

      onConfirm: () => {
        // User confirms the value, go to the next field and register an annotation about the test failure.
        const name: string = this.$t( 'dataEntry.form.tests.growthError.annotationTitle', {
          test: error.test.name,
        } )
            .toString()
        const value: string = this.$t( 'dataEntry.form.tests.growthError.annotationText', {
          comp: comparisonVariableName,
          min: (error.test as GrowthTest).minDiff,
          value: error.measure.value,
          max: (error.test as GrowthTest).maxDiff,
        } )
            .toString()
        error.measure.annotations.push( new Annotation(
            name,
            ANNOTATION_TYPE_TEXT,
            value,
            null,
            new Date(),
            ['Mesure', 'Test'],
            [],
        ) )
        if (doNext) {
          this.next()
        }
      },

      onCancel: () => {
        // User does not confirm the value, return to the field.
        this.select( true )
      },

      priority: doNext,
    } )
  }
}
