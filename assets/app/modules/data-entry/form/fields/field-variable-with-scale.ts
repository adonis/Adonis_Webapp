import Mark from '@adonis/app/models/evaluation-variables/mark'
import MarkList from '@adonis/app/models/evaluation-variables/mark-list'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import AsyncConfirmPopup from '@adonis/shared/models/async-confirm-popup'
import { Component } from 'vue-property-decorator'
import FieldVariable from './field-variable'

/**
 * Component to regroup common actions of some variable type that handles value list association.
 */
@Component({})
export default class FieldVariableWithScale extends FieldVariable {

  doNextForce = false

  /**
   * Determine whether the variable has a scale association or not.
   */
  get withScale(): boolean {
    return this.variable.hasAssociatedScale()
  }

  /** Retrieve the scale. */
  get scale(): MarkList {
    return this.variable.scale
  }

  /** Retrieve the scale available values. */
  get markList(): Mark[] {
    return this.withScale
        ? this.scale.marks
        : []
  }

  /** Transform scale value into string value array. */
  get markListValue(): string[] {
    return this.withScale
        ? this.markList.map(mark => String(mark.value))
        : []
  }

  /**
   * Retrieve a name for a mark from its string value.
   *
   * @param value
   *
   * @return string
   */
  showMarkByValue(value: string): string {
    const mark: Mark = this.markList.filter(markIn => markIn.value === parseInt(value, null))[0]
    return !!mark
        ? `${mark.value} - ${mark.text}`
        : ''
  }

  /**
   * Get base64 image string of mark value if exists;
   *
   * @param value
   *
   * @return string
   */
  showImageMarkByValue(value: string): string {
    const mark: Mark = this.markList.filter(markIn => markIn.value === parseInt(value, null))[0]
    return !!mark
        ? mark.imageData
        : null
  }

  /**
   * Handle scale specific hook on field before handle "classic field" hooks.
   *
   * @param doNext
   */
  checkAddonChange(doNext = true): void {
    this.handleAddon()
        .then(() => {
          this.onValueChange(doNext || this.doNextForce)
          this.doNextForce = false
        })
  }

  selected() {
    this.doNextForce = false
  }

  doCallNext(): void {
    this.doNextForce = true
    if (!this.withScale) {
      this.$emit('next')
    }
  }

  /**
   * The hook callback before firing next field request.
   * Specific to each field.
   *
   * @return Promise<void>
   */
  protected handleAddon(): Promise<void> {
    return this.handleScaleValueValid()
  }

  /**
   * Handle scale new value with user dialog interaction.
   *
   * @return Promise<void>
   */
  protected handleScaleValueValid(): Promise<void> {
    return new Promise<void>((resolve: (...args: any) => void) => {
      if (this.withScale) {
        if (this.isValidScaleValue(this.measure.value as number)) {
          resolve()
        } else {
          const confirm: AsyncConfirmPopup = FormMeasureService.createConfirmOutOfScaleLimit(
              this, this.variable, String(this.measure.value),
          )
          this.$store.dispatch('confirm/askConfirm', confirm)
              .then()
        }
      } else {
        resolve()
      }
    })
  }

  protected isValidScaleValue(value: number) {
    const exclusiveOk = !this.scale.exclusive || this.markList.map(mark => mark.value)
        .includes(value)
    return (value === null) ||
        (this.scale.minValue <= value && this.scale.maxValue >= value && exclusiveOk) ||
        (this.project.stateCodeValues.includes(value))
  }
}
