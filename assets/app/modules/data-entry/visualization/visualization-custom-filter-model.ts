/**
 * Custom filter allows to filter a specific column if the operation between the filter value and each row value
 * is correct.
 */
export default class VisualizationCustomFilterModel {

  constructor(
      public column: string = null,
      public operator: string = null,
      public value: number = null,
  ) {
  }

  clone(): VisualizationCustomFilterModel {
    return new VisualizationCustomFilterModel(
        this.column,
        this.operator,
        this.value,
    )
  }
}