import VisualizationCustomFilterModel from '@adonis/app/modules/data-entry/visualization/visualization-custom-filter-model'

/**
 * Filter choice.
 */
export default class VisualizationFilterChoice {

  constructor(
      public projectId: number | string,
      public filterMap: Map<string, string> = new Map(),
      public columns: string[] = [],
      public custom: VisualizationCustomFilterModel = new VisualizationCustomFilterModel(),
  ) {
  }

  clone(): VisualizationFilterChoice {
    return new VisualizationFilterChoice(
        this.projectId,
        new Map( this.filterMap ),
        [...this.columns],
        this.custom.clone(),
    )
  }
}
