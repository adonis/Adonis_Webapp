<template>
  <v-row v-if="!!project">
    <portal to="toolbar-title">
      <v-toolbar-title>{{ $t('dataEntry.rightMenu.visualization') }}</v-toolbar-title>
    </portal>
    <v-col class="pt-0" cols="12">
      <visualization-filter
          v-model="filters"
          :available-columns="availableColumns"
          :select-data="filtersAvailableValues"
          @input="onFilterChange"
      ></visualization-filter>
    </v-col>

    <v-col class="pt-0 pr-0" cols="12">
      <v-card tile>
        <v-card-title>{{ $t('dataEntry.visualization.table.title') }}</v-card-title>
        <v-card-text>
          <visualization-table
              :columns="availableColumns"
              :filters="filters"
              :items="rows"
              :state-codes="stateCodes"
          ></visualization-table>
        </v-card-text>
      </v-card>
    </v-col>
  </v-row>
</template>

<script lang="ts">
import { BLOCK_CLASS } from '@adonis/app/models/business-objects/block'

import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import { DEVICE_CLASS } from '@adonis/app/models/business-objects/device'
import Individual, { INDIVIDUAL_CLASS } from '@adonis/app/models/business-objects/individual'
import { PLATFORM_CLASS } from '@adonis/app/models/business-objects/platform'
import { SUBBLOCK_CLASS } from '@adonis/app/models/business-objects/sub-block'
import Treatment, { TREATMENT_CLASS } from '@adonis/app/models/business-objects/treatment'
import UnitParcel, { UNITPARCEL_CLASS } from '@adonis/app/models/business-objects/unit-parcel'
import { ANNOTATION_CLASS } from '@adonis/app/models/evaluation-variables/annotation'
import { Evaluable } from '@adonis/app/models/evaluation-variables/evaluation'
import FormField from '@adonis/app/models/evaluation-variables/form-field'
import GeneratedField from '@adonis/app/models/evaluation-variables/generated-form-fields'
import Measure from '@adonis/app/models/evaluation-variables/measure'
import Session from '@adonis/app/models/evaluation-variables/session'
import { CODE_NONE, CODE_TYPE_DEAD } from '@adonis/app/models/evaluation-variables/state-code'
import Variable from '@adonis/app/models/evaluation-variables/variable'
import VisualizationFilterChoice from '@adonis/app/modules/data-entry/visualization/visualization-filter-choice'
import DataEntryService from '@adonis/app/services/data-entry-service'
import FormMeasureService from '@adonis/app/services/form-measure-service'
import PlatformService from '@adonis/app/services/platform-service'
import { Component, Vue } from 'vue-property-decorator'
import {
  COURSE_UNIT,
  DEAD,
  DEMISE_DATE,
  EMERGENCE_DATE,
  TREATMENT_SHORT,
  X_POSITION,
  Y_POSITION,
} from './visualization'

/**
 * Main component in charge of tab "visualization" in data entry form process.
 */
@Component( {
  components: {
    'visualization-table': () => import('./VisualizationTable.vue'),
    'visualization-filter': () => import('./VisualizationFilter.vue'),
  },
} )
export default class AdoDentryVisualization extends Vue {

  /** User filter choices container. */
  filters: VisualizationFilterChoice = null

  sessions: Session[] = []

  /** The opened project. */
  get project(): DataEntryProject {
    return this.$store.getters['dataEntry/getWorkInProgress']
  }

  /** The state codes label. */
  get stateCodes(): string[] {
    return !!this.project
        ? this.project.stateCodes.map( state => String( state.code ) )
        : []
  }

  /** Get columns associated with a variable. */
  get variableColumns(): string[] {
    let variableColumns: string[] = []
    if (!!this.project) {
      variableColumns = FormMeasureService.getVariableNames( this.project )
    }
    return variableColumns
  }

  /** Get all columns available. */
  get availableColumns(): string[] {
    let availableColumns: string[] = []
    if (!!this.project) {
      availableColumns = [
        PLATFORM_CLASS,
        DEVICE_CLASS,
        BLOCK_CLASS,
        SUBBLOCK_CLASS,
        UNITPARCEL_CLASS,
        DEAD,
        EMERGENCE_DATE,
        DEMISE_DATE,
        TREATMENT_CLASS,
        TREATMENT_SHORT,
        X_POSITION,
        Y_POSITION,
        INDIVIDUAL_CLASS,
        ...this.variableColumns,
        ANNOTATION_CLASS,
      ]
    }
    return availableColumns
  }

  /** Get all rows with all data. */
  get rows(): Map<string, string>[] {
    let rows: Map<string, string>[] = []
    let formFieldByTargetUri: Map<string, FormField[]> = new Map()
    if (this.sessions.length > 0) {
      this.sessions
          .sort( ( sessionA: Session, sessionB: Session ) => {
            return sessionA.startedAt < sessionB.startedAt
                ? -1
                : 1
          } )
          .forEach( ( session: Session ) => {
            session.formFields.forEach( ( formField: FormField ) => {
              if (!formFieldByTargetUri.get( formField.targetUri )) {
                formFieldByTargetUri.set( formField.targetUri, [] )
              }
              let formFields: FormField[] = formFieldByTargetUri.get( formField.targetUri )
              formFields = formFields.filter( ( inFormField: FormField ) => {
                return !inFormField.equals( formField )
              } )
              formFields.push( formField )
              formFieldByTargetUri.set( formField.targetUri, formFields )
            } )
          } )
    }
    Array.from( formFieldByTargetUri.keys() ).forEach( ( targetUri: string ) => {
      let rowData: Map<string, string> = new Map()
      let object: Evaluable = this.project.platform.uriMap.get( targetUri )
      let { individual, parcel, subBlock, block, device } = PlatformService.getBranchObjects( object )
      let positionable: Individual | UnitParcel = !!individual
          ? individual
          : parcel
      let treatment: Treatment = !!parcel
          ? PlatformService.getTreatmentByName( this.project.platform, parcel.treatmentName )
          : null
      rowData.set( PLATFORM_CLASS, this.project.platform.name )
      rowData.set( DEVICE_CLASS, !!device
          ? device.name
          : '' )
      rowData.set( BLOCK_CLASS, !!block
          ? block.name
          : '' )
      rowData.set( SUBBLOCK_CLASS, !!subBlock
          ? subBlock.name
          : '' )
      rowData.set( UNITPARCEL_CLASS, !!parcel
          ? parcel.name
          : '' )
      rowData.set( EMERGENCE_DATE, !!individual && !!individual.aparitionDate
          ? new Date( individual.aparitionDate ).toLocaleString()
          : '' )
      rowData.set( DEMISE_DATE, !!individual && !!individual.demiseDate
          ? new Date( individual.demiseDate ).toLocaleString()
          : '' )
      rowData.set( TREATMENT_CLASS, !!treatment
          ? treatment.name
          : '' )
      rowData.set( TREATMENT_SHORT, !!treatment
          ? treatment.shortName
          : '' )
      rowData.set( X_POSITION, !!positionable
          ? String( positionable.x )
          : '' )
      rowData.set( Y_POSITION, !!positionable
          ? String( positionable.y )
          : '' )
      rowData.set( INDIVIDUAL_CLASS, !!individual
          ? individual.name
          : '' )
      let dead: boolean = false
      rowData.set( DEAD, String( dead ) )
      rowData.set( ANNOTATION_CLASS, '' )
      rowData.set( COURSE_UNIT, object.constructor.name )

      let rowRepetitions: GeneratedField[] = []

      let handleFormField = ( formField: FormField, insertFirst: boolean = false ): boolean => {
        let value: string = ''
        formField.measures.forEach( ( measure: Measure ) => {
          if ((!!measure.value || 0 === measure.value) && (isNaN( Number( measure.value ) ) || 0 <= measure.value)) {
            value = String( measure.value )
          }
          if (CODE_NONE !== measure.state.code) {
            value = String( measure.state.code )
          }
          dead = dead || CODE_TYPE_DEAD === measure.state.type
          const variable: Variable = this.project.variablesMap.get( measure.variableUid )
          rowData.set( FormMeasureService.fieldLabel( variable.name, measure.repetition, variable.repetitions ), value )
        } )
        if (!!formField.generatedFields && 0 < formField.generatedFields.length) {
          rowRepetitions = insertFirst
              ? [...formField.generatedFields, ...rowRepetitions]
              : [...rowRepetitions, ...formField.generatedFields]
          return false
        } else {
          return true
        }
      }

      let resetFormField = ( formField: FormField ): void => {
        formField.measures.forEach( ( measure: Measure ) => {
          const variable: Variable = this.project.variablesMap.get( measure.variableUid )
          rowData.delete( FormMeasureService.fieldLabel( variable.name, measure.repetition, variable.repetitions ) )
        } )
      }

      formFieldByTargetUri.get( targetUri ).forEach(
          ( formField: FormField ) => {
            handleFormField( formField, false )
          },
      )

      if (0 === rowRepetitions.length) {
        rows.push( rowData )
      } else {
        while (0 < rowRepetitions.length) {
          let generatedField = rowRepetitions.shift()
          rowData.set( generatedField.prefix, generatedField.getName() )
          generatedField.formFields.forEach( ( formField: FormField ) => {
            if (handleFormField( formField, true )) {
              rows.push( new Map( rowData ) )
              resetFormField( formField )
              rowData.delete( generatedField.prefix )
            }
          } )
        }
      }
    } )

    return rows
  }

  /** Get values that all filter field can take. */
  get filtersAvailableValues(): Map<string, string[]> {
    let valuesMap: Map<string, string[]> = new Map()
    this.availableColumns.forEach( ( column: string ) => {
      let values: string[] = []
      this.rows.forEach( ( rowMap: Map<string, string> ) => {
        let value = rowMap.get( column )
        if (!values.includes( value )) {
          values.push( value )
        }
      } )
      valuesMap.set( column, values )
    } )
    valuesMap.set( COURSE_UNIT, [
      INDIVIDUAL_CLASS,
      UNITPARCEL_CLASS,
      SUBBLOCK_CLASS,
      BLOCK_CLASS,
      DEVICE_CLASS,
    ] )
    return valuesMap
  }

  /**
   * Initialize component by retrieving from store the user filter choice.
   */
  mounted(): void {
    this.filters = this.$store.getters['dataEntry/getVisualizationFilter']
    this.updateSessions()
  }

  async updateSessions(): Promise<void> {
    this.sessions = await DataEntryService.retrieveSessionForProject( this.$indexedDb, this.project )
  }

  /**
   * Handle filter choice changes.
   */
  onFilterChange(): void {
    this.$store.dispatch( 'dataEntry/setVisualizationFilter', this.filters )
  }
}
</script>

<style lang="scss" scoped>
.h-600 {
  max-height: 600px;
}
</style>
