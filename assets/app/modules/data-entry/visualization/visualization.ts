import { BLOCK_CLASS } from '@adonis/app/models/business-objects/block'
import { DEVICE_CLASS } from '@adonis/app/models/business-objects/device'
import { INDIVIDUAL_CLASS } from '@adonis/app/models/business-objects/individual'
import { PLATFORM_CLASS } from '@adonis/app/models/business-objects/platform'
import { SUBBLOCK_CLASS } from '@adonis/app/models/business-objects/sub-block'
import { TREATMENT_CLASS } from '@adonis/app/models/business-objects/treatment'
import { UNITPARCEL_CLASS } from '@adonis/app/models/business-objects/unit-parcel'
import { ANNOTATION_CLASS } from '@adonis/app/models/evaluation-variables/annotation'

/**
 * Translation utility helper in visualization context.
 */
export class VisualizationTranslator {

  /**
   * Translate a column code into a label.
   *
   * @param vm
   * @param code
   *
   * @return string
   */
  static translateColumn( vm: Vue, code: string ): string {
    let translation: string = code
    switch (code) {
      case PLATFORM_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.platform' )
            .toString()
        break
      case DEVICE_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.device' )
            .toString()
        break
      case BLOCK_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.block' )
            .toString()
        break
      case SUBBLOCK_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.subBlock' )
            .toString()
        break
      case UNITPARCEL_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.parcel' )
            .toString()
        break
      case TREATMENT_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.treatment' )
            .toString()
        break
      case TREATMENT_SHORT:
        translation = vm.$t( 'dataEntry.visualization.table.columns.treatmentShort' )
            .toString()
        break
      case INDIVIDUAL_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.individual' )
            .toString()
        break
      case COURSE_UNIT:
        translation = vm.$t( 'dataEntry.visualization.filter.fieldsLabel.courseUnit' )
            .toString()
        break
      case DEMISE_DATE:
        translation = vm.$t( 'dataEntry.visualization.table.columns.demiseDate' )
            .toString()
        break
      case EMERGENCE_DATE:
        translation = vm.$t( 'dataEntry.visualization.table.columns.emergenceDate' )
            .toString()
        break
      case DEAD:
        translation = vm.$t( 'dataEntry.visualization.table.columns.dead' )
            .toString()
        break
      case ANNOTATION_CLASS:
        translation = vm.$t( 'dataEntry.visualization.table.columns.annotation' )
            .toString()
        break
      case X_POSITION:
        translation = vm.$t( 'dataEntry.visualization.table.columns.x' )
            .toString()
        break
      case Y_POSITION:
        translation = vm.$t( 'dataEntry.visualization.table.columns.y' )
            .toString()
        break
      default:
        break
    }
    return translation
  }

  /**
   * Translate operator code.
   *
   * @param vm
   * @param code
   *
   * @return string
   */
  public static translateOperator( vm: Vue, code: string ): string {
    let translation: string = code
    switch (code) {
      case OPERATOR_BIGGER:
        translation = vm.$t( 'dataEntry.visualization.filter.custom.bigger' )
            .toString()
        break
      case OPERATOR_BIGGER_EQUAL:
        translation = vm.$t( 'dataEntry.visualization.filter.custom.biggerEqual' )
            .toString()
        break
      case OPERATOR_EQUAL:
        translation = vm.$t( 'dataEntry.visualization.filter.custom.equal' )
            .toString()
        break
      case OPERATOR_SMALLER_EQUAL:
        translation = vm.$t( 'dataEntry.visualization.filter.custom.smallerEqual' )
            .toString()
        break
      case OPERATOR_SMALLER:
        translation = vm.$t( 'dataEntry.visualization.filter.custom.smaller' )
            .toString()
        break
      default:
        break
    }
    return translation
  }
}

export const X_POSITION = 'X'
export const Y_POSITION = 'Y'
export const COURSE_UNIT = 'CourseUnit'
export const TREATMENT_SHORT = 'TreatmentShort'
export const DEMISE_DATE = 'DemiseDate'
export const EMERGENCE_DATE = 'EmergenceDate'
export const DEAD = 'Dead'

export const OPERATOR_SMALLER = '<'
export const OPERATOR_SMALLER_EQUAL = '<='
export const OPERATOR_EQUAL = '='
export const OPERATOR_BIGGER_EQUAL = '>='
export const OPERATOR_BIGGER = '>'
