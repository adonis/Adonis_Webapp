<template>
  <v-dialog
      v-model="dialog"
      max-width="500px"
      persistent
      scrollable
      @input="onOpening"
      @close=""
  >
    <template slot="activator" slot-scope="{ on }">
      <slot name="btn" v-bind:on="on">
        <v-btn
            v-on="on"
            color="primary"
            dark
            rounded
        >{{ $t('dataEntry.parameter.addBtnLabel') }}
        </v-btn>
      </slot>
    </template>
    <v-form
        ref="form"
    >
      <v-card>
        <v-card-title>
          <slot name="title">
            {{ $t('dataEntry.parameter.cardTest.testDialog.title') }}
          </slot>
        </v-card-title>
        <v-divider></v-divider>
        <v-card-text>
          <v-select
              v-model="type"
              :disabled="!creation"
              :items="availableTestTypes"
              :label="$t('dataEntry.parameter.cardTest.testDialog.testTypeField')"
              :rules="testTypesRules"
              clearable
              required
          ></v-select>
          <test-form-range
              v-if="isTypeRange"
              v-model="valid"
              :creation="creation"
              :initValue="test"
              @update:test="updateLocalTest"
          ></test-form-range>
          <test-form-growth
              v-if="isTypeGrowth"
              v-model="valid"
              :connected-variables="connectedVariables"
              :creation="creation"
              :initValue="test"
              :variables="variables"
              @update:test="updateLocalTest"
          ></test-form-growth>
          <test-form-combination
              v-if="isTypeCombination"
              v-model="valid"
              :connected-variables="connectedVariables"
              :creation="creation"
              :initValue="test"
              :variables="variables"
              @update:test="updateLocalTest"
          ></test-form-combination>
          <test-form-preconditioned
              v-if="isTypePreconditioned"
              v-model="valid"
              :connected-variables="connectedVariables"
              :creation="creation"
              :initValue="test"
              :variables="variables"
              @update:test="updateLocalTest"
          ></test-form-preconditioned>
        </v-card-text>
        <v-divider></v-divider>
        <v-card-actions>
          <v-btn
              class="ml-auto"
              color="primary"
              :disabled="!this.valid"
              :dark="this.valid"
              rounded
              @click="validate"
          >{{ $t('dataEntry.parameter.validBtnLabel') }}
          </v-btn>
          <v-btn
              color="secondary"
              dark
              rounded
              @click="dialog = false"
          >{{ $t('dataEntry.parameter.cancelBtnLabel') }}
          </v-btn>
        </v-card-actions>
      </v-card>
    </v-form>
  </v-dialog>
</template>

<script lang="ts">
import Variable from '@adonis/app/models/evaluation-variables/variable'
import CombinationTest from '@adonis/app/models/field-tests/combination-test'
import GrowthTest from '@adonis/app/models/field-tests/growth-test'
import PreconditionedCalculation from '@adonis/app/models/field-tests/preconditioned-calculation'
import RangeTest from '@adonis/app/models/field-tests/range-test'
import Test from '@adonis/app/models/field-tests/test'
import VariableTypeEnum from "@adonis/shared/constants/variable-type-enum"
import { Component, Prop, Vue, Watch } from 'vue-property-decorator'

const TYPE_RANGE = 'Range'
const TYPE_GROWTH = 'Growth'
const TYPE_COMBINATION = 'Combination'
const TYPE_PRECONDITIONED = 'Preconditioned'

/**
 * Dialog component allowing the operator to edit or add tests in a variable.
 */
@Component( {
  components: {
    'test-form-preconditioned': () => import('@adonis/app/modules/data-entry/parameter/test-forms/TestFormPreconditioned.vue'),
    'test-form-combination': () => import('@adonis/app/modules/data-entry/parameter/test-forms/TestFormCombination.vue'),
    'test-form-growth': () => import('@adonis/app/modules/data-entry/parameter/test-forms/TestFormGrowth.vue'),
    'test-form-range': () => import('@adonis/app/modules/data-entry/parameter/test-forms/TestFormRange.vue'),
  },
} )
export default class ParameterTestFormDialog extends Vue {

  /** Defines whether the dialog is open. */
  dialog = false

  /** The test being edit. */
  @Prop( { default: (): Test => null } )
  test: Test

  /** The concerned variable. */
  @Prop()
  variable: Variable

  /** The available variables. */
  @Prop()
  variables: Variable[]

  /** The available connected variables. */
  @Prop()
  connectedVariables: Variable[]

  /** Defines whether the form is valid. */
  valid = this.test !== null

  /** The type of the test. */
  type: string = null

  /** The test copy to modify/create. */
  workingTest: Test = null

  /** Defines whether the form is in creation mode or note. */
  creation: boolean = this.test === null

  /** Defines whether the variable is numeric. */
  get numericVariable(): boolean {
    return !!this.variable && [VariableTypeEnum.REAL.toString(), VariableTypeEnum.INTEGER.toString()].includes( this.variable.type.toString() )
  }

  /** Get the test available according to the situation. */
  get availableTestTypes() {
    let res = []
    if (this.numericVariable) {
      if (this.variable.rangeTests.length === 0 || !this.creation) {
        res.push( {
          text: this.$t( 'dataEntry.parameter.cardTest.types.interval' ).toString(),
          value: TYPE_RANGE,
        } )
      }
      if (this.variable.growthTests.length === 0 || !this.creation) {
        res.push( {
          text: this.$t( 'dataEntry.parameter.cardTest.types.increase' ).toString(),
          value: TYPE_GROWTH,
        } )
      }
      if (this.variable.combinationTests.length === 0 || !this.creation) {
        res.push( {
          text: this.$t( 'dataEntry.parameter.cardTest.types.combine' ).toString(),
          value: TYPE_COMBINATION,
        } )
      }
    }
    if (this.variable.preconditionedCalculations.length === 0 || !this.creation) {
      res.push( {
        text: this.$t( 'dataEntry.parameter.cardTest.types.placeholder' ).toString(),
        value: TYPE_PRECONDITIONED,
      } )
    }
    return res
  }

  /** Check type is selected. */
  get testTypesRules(): Function[] {
    return [
      ( v: string ) => !!v || this.$t( 'dataEntry.parameter.cardTest.testDialog.errors.typeTestIsRequired' ).toString(),
    ]
  }

  /** TYPE_GROWTH. */
  get isTypeGrowth(): boolean {
    return this.type === TYPE_GROWTH
  }

  /** TYPE_COMBINATION. */
  get isTypeCombination(): boolean {
    return this.type === TYPE_COMBINATION
  }

  /** TYPE_RANGE. */
  get isTypeRange(): boolean {
    return this.type === TYPE_RANGE
  }

  /** TYPE_PRECONDITIONED. */
  get isTypePreconditioned(): boolean {
    return this.type === TYPE_PRECONDITIONED
  }

  /**
   * Handle dialog opening event.
   */
  onOpening(): void {
    if (this.dialog) {
      this.type = null
      if (this.test instanceof RangeTest) {
        this.type = TYPE_RANGE
      } else if (this.test instanceof GrowthTest) {
        this.type = TYPE_GROWTH
      } else if (this.test instanceof CombinationTest) {
        this.type = TYPE_COMBINATION
      } else if (this.test instanceof PreconditionedCalculation) {
        this.type = TYPE_PRECONDITIONED
      }
    }
  }

  /**
   * Validate the form.
   */
  validate(): void {
    if ((this.$refs.form as VForm).validate() && this.valid) {
      if (this.creation) {
        this.$emit( 'update:test', this.workingTest )
      } else {
        this.test.merge( this.workingTest )
        this.$emit( 'update:test' )
      }
      this.dialog = false
    }
  }

  /**
   * Sync local test copy with prop.
   *
   * @param test
   */
  updateLocalTest( test: Test ) {
    this.workingTest = test
  }

  /**
   * Reset form.
   */
  @Watch( 'type' )
  reset(): void {
    this.workingTest = null
  }
}
</script>
