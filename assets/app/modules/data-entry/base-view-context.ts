import { Component, Vue } from 'vue-property-decorator'

/**
 * Component extension to allow opening/closing contextual menu when exists.
 */
@Component( {} )
export default class BaseViewContext extends Vue {

  protected toggleMenu(): void {
    Promise.resolve( this.$store.dispatch( 'app/toggleContext' ) )
  }
}
