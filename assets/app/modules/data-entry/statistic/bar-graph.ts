import { Bar } from 'vue-chartjs'

export interface BarChartData {
  label: string
  data: number
}

export default {
  extends: Bar,
  props: ['label', 'chartData'],
  mounted() {
    // Overwriting base render method with actual data.
    this.renderChart(
        {
          labels: this.chartData.map( ( bar: BarChartData ) => bar.label ),
          datasets: [
            {
              label: this.label,
              backgroundColor: '#678815',
              data: this.chartData.map( ( bar: BarChartData ) => bar.data ),
            },
          ],
        },
        {
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true,
                  stepSize: 1,
                },
                gridLines: {
                  display: true,
                },
              },
            ],
            xAxes: [
              {
                ticks: {
                  beginAtZero: true,
                },
                gridLines: {
                  display: false,
                },
              },
            ],
          },
          responsive: true,
        },
    )
  },
}
