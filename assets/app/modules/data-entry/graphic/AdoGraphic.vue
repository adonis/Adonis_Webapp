<template>

  <v-card
      v-if="active"
      ref="draw-viewer"
      class="full-height overflow-scroll-a"
      tile
  >
    <portal to="toolbar-title">
      <v-toolbar-title>{{ $t('dataEntry.rightMenu.graphic') }}</v-toolbar-title>
    </portal>

    <v-overlay
        v-if="'100%' !== loadingPercent"
    >
      <div class="mx-auto my-auto">
        <b>{{ loadingPercent }}</b>
      </div>
    </v-overlay>

    <div
        id="draw-container"
        ref="draw-container"
    >
    </div>

    <v-footer
        app
        color="tertiary"
        elevation="2"
    >
      <v-container class="pa-0">
        <v-btn
            v-if="!!targetName && isFreeModeGraphic"
            :class="!!targetSubName && '' !== targetSubName ? 'mb-0' : 'mb-3'"
            block
            color="secondary"
            dark
            rounded
            @click="moveToTarget"
        >
          <v-icon>arrow_right</v-icon>
          {{ targetName }}
        </v-btn>
        <v-row
            v-else-if="!!targetName"
            class="align-center"
        >
          <v-col
              :class="!!targetSubName && '' !== targetSubName ? 'pb-0' : ''"
              class="font-weight-bold"
          >
            <v-icon>arrow_right</v-icon>
            {{ targetName }}
          </v-col>
        </v-row>
        <v-row
            v-if="!!targetSubName && '' !== targetSubName"
            class="align-center"
        >
          <v-col class="font-weight-bold">
            <small>{{ targetSubName }}</small>
          </v-col>
        </v-row>
        <v-row class="align-center">
          <v-btn
              class="mx-2"
              color="secondary"
              dark
              fab
              small
              @click="centerOnCurrentObject"
          >
            <v-icon>star</v-icon>
          </v-btn>
          <v-spacer></v-spacer>
          <v-btn
              class="mx-2"
              color="primary"
              dark
              fab
              small
              @click="zoomOut"
          >
            <v-icon>zoom_out</v-icon>
          </v-btn>
          <v-btn
              class="mr-2"
              color="primary"
              dark
              fab
              small
              @click="zoomIn"
          >
            <v-icon>zoom_in</v-icon>
          </v-btn>
        </v-row>
      </v-container>
    </v-footer>
  </v-card>

</template>

<!--suppress TypeScriptCheckImport -->
<script lang="ts">
import { FREE_MODE_GRAPHIC } from '@adonis/app/constants'
import { BusinessObject } from '@adonis/app/models/business-objects/business-object'
import DataEntryProject from '@adonis/app/models/business-objects/data-entry-project'
import Device from '@adonis/app/models/business-objects/device'
import OutExperimentationZone from '@adonis/app/models/business-objects/OutExperimentationZone'
import GraphicRequestWrapper from '@adonis/app/modules/data-entry/graphic/graphic-request-wrapper'
import { ROUTE_DATA_ENTRY__RUNNING } from '@adonis/app/plugins/vue-router'
import DataEntryService from '@adonis/app/services/data-entry-service'
import GraphicService from '@adonis/app/services/graphic-service'
import PlatformService from '@adonis/app/services/platform-service'
import MainView from '@adonis/shared/MainView.vue'
import { SVG } from '@svgdotjs/svg.js'
import { max, min } from 'mathjs'
import { Component, Vue, Watch } from 'vue-property-decorator'

/**
 * Graphical component to show visual drawing of the project platform.
 */
@Component( {} )
export default class AdoGraphic extends Vue {

  /** The main draw SVG. */
  mainSVG: any

  /** The x row SVG. */
  xRowSVG: any

  /** The y column SVG. */
  yColSVG: any

  /** Allows to scale the drawing. */
  scale = 0.8

  /** Draw elements that need to always be front. */
  frontElements: any[] = []

  /** Two points to define the size of the SVG. */
  size: number[][] = null

  /** All draw wrapper to be shown on SVG. */
  drawers: GraphicRequestWrapper[] = []

  /** Selected contour when user click on some SVG polygon. */
  selectedContour: any[]

  /** Selected evaluable based on selected contour. */
  selectedTarget: BusinessObject = null

  /** Count the polygon being added to the SVG. */
  polygonDoneCount = 0

  /** Get the root component. */
  get mainView(): MainView {
    return this.$root as MainView
  }

  /** Get from store the current opened project. */
  get project(): DataEntryProject {
    return this.$store.getters['dataEntry/getWorkInProgress']
  }

  /** If the graphical structure doesn't exist, inactivate the graphic component. */
  get active(): boolean {
    return !!this.project && !!this.project.graphicalStructure
  }

  /** Get the extremum of the drawing grid. */
  get maxSize(): number[] {
    let size = [0, 0]
    if (!!this.size) {
      size = [
        this.size[0][0] + Math.abs( this.size[1][0] - this.size[0][0] + 2 ),
        this.size[0][1] + Math.abs( this.size[1][1] - this.size[0][1] + 2 ),
      ]
    }
    return size
  }

  /** Get the extremum of the drawing image in pixel. */
  get drawSize(): number[] {
    let size = [0, 0]
    if (!!this.active) {
      let sizing = GraphicService.extractPointWithMargin( this.maxSize, this.project.graphicalStructure )

      /*
       Size is calculated by the difference between the top left rectangle center and the bottom right one.
       We need to add to this distance 2 * 1/2 base width/height corresponding
       to the smallest rectangle at each border of the platform.
       We also need to add 2 * 25 units corresponding to the largest marge at each border
       */
      size = [
        this.project.graphicalStructure.baseWidth + 50 + sizing.xCenter,
        this.project.graphicalStructure.baseHeight + 50 + sizing.yCenter,
      ]
    }
    return size
  }

  /** Get all devices in project. */
  get devices(): Device[] {
    return this.active
        ? this.project.platform.devices
        : []
  }

  /** Get the name of the target when a contour is selected. */
  get targetName(): string {
    return !!this.selectedTarget
        ? PlatformService.objectName( this.selectedTarget, this )
        : null
  }

  /** Get the sub name of the target when a contour is selected. */
  get targetSubName(): string {
    return !!this.selectedTarget
        ? GraphicService.objectSubTitle( this.selectedTarget )
        : null
  }

  /** Based on the drawers to be shown and the polygon count, get the loading purcent. */
  get loadingPercent(): string {
    return !!this.drawers.length
        ? Math.trunc( this.polygonDoneCount / this.drawers.length * 100 ) + '%'
        : '0%'
  }

  /** Get from store if the graphic allows navigation or not. */
  get isFreeModeGraphic(): boolean {
    return FREE_MODE_GRAPHIC === this.$store.getters['dataEntry/getFreeMode']
  }

  /** Flag to know if a current position exists or not. */
  get hasCurrentPosition(): boolean {
    return !!this.project && !!this.project.dataEntry && !!this.project.dataEntry.currentPosition
  }

  /**
   * Gets called when component is mounted. Initialize component.
   */
  mounted(): void {

    if (this.active) {
      this.$nextTick( () => {

        const mainContainer: any = this.$refs['draw-viewer']
        const container: any = this.$refs['draw-container']
        container.innerHTML = ''
        this.xRowSVG = SVG().addTo( '#draw-container' )
        this.yColSVG = SVG().addTo( '#draw-container' )
        this.mainSVG = SVG().addTo( '#draw-container' )
        this.handleViewport( mainContainer, container )
        this.initialize()

      } )
    }

  }

  /**
   * Handle viewport of graph tab by adding left and top sticky X,Y position shower.
   *
   * @param mainContainer
   * @param container
   */
  handleViewport( mainContainer: any, container: any ): void {
    mainContainer.$el.addEventListener( 'scroll', () => {
      container.children[1].style['left'] = mainContainer.$el.scrollLeft
    } )
    container.style['marginTop'] = `-${GraphicService.FONT_SIZE * 1.5}px`
    container.children[0].style['position'] = 'sticky'
    container.children[0].style['top'] = 0
    container.children[0].style['left'] = 0
    container.children[0].style['backgroundColor'] = 'white'
    container.children[0].style['borderBottom'] = '1px solid grey'
    container.children[0].style['zIndex'] = 2
    container.children[1].style['position'] = 'absolute'
    container.children[1].style['top'] = 0
    container.children[1].style['left'] = 0
    container.children[1].style['backgroundColor'] = 'white'
    container.children[1].style['borderRight'] = '1px solid grey'
    container.children[1].style['zIndex'] = 1
    container.children[2].style['zIndex'] = 0
  }

  /**
   * Check if the graphic is fully loaded and runs callbacks when true.
   */
  @Watch( 'loadingPercent' )
  onLoaded(): void {

    if ('100%' === this.loadingPercent) {

      this.frontElements.forEach( // Handle elements that need to stay on front.
          ( el: any ) => el.front(),
      )

      DataEntryService.saveProjectToDatabase( this, this.project, true, false )

      this.resizeDrawContainer()
      this.setZoom( this.scale )
    }

  }

  /**
   * Handles click on some contour to select the target or unselect it in case click occured on already selected contour.
   *
   * @param drawer
   */
  onClickTarget( drawer: GraphicRequestWrapper ): void {

    if (!!this.selectedContour) {
      this.selectedContour.forEach(
          ( contour: any ) => {
            contour.remove()
          },
      )
    }

    if (this.selectedTarget !== drawer.target) {

      this.selectedContour = []
      drawer.polygonContour.forEach(
          ( polygonStr: string ) => {
            this.selectedContour.push(
                this.mainSVG.polyline( polygonStr + polygonStr.split( ' ' )[0] )
                    .fill( 'none' )
                    .stroke( { width: 3, color: GraphicService.STROKE_COLOR } ),
            )
          },
      )
      this.selectedTarget = drawer.target
    } else {

      this.selectedTarget = null
    }

  }

  /**
   * Allows to change the scale in order to zoom in.
   */
  zoomIn(): void {

    this.scale = this.scale + 0.1 <= 1.9
        ? this.scale + 0.1
        : this.scale
    this.scale = Math.round( (this.scale + Number.EPSILON) * 10 ) / 10
    this.setZoom( this.scale )

  }

  /**
   * Allows to change the scale in order to zoom out.
   */
  zoomOut(): void {

    this.scale = this.scale - 0.1 >= 0.1
        ? this.scale - 0.1
        : this.scale
    this.scale = Math.round( (this.scale + Number.EPSILON) * 10 ) / 10
    this.setZoom( this.scale )

  }

  /**
   * Allows to scroll the parent container to the position of the current object.
   */
  centerOnCurrentObject(): void {

    if (this.hasCurrentPosition) {
      let object = PlatformService.getObjectFromUri(
          this.project.platform, this.project.dataEntry.currentPosition,
      ) as BusinessObject
      if (!!object && !!object.center) {
        this.setPosition(
            (object.center[0] * this.scale) - window.outerWidth / 2,
            (object.center[1] * this.scale) + 56 - window.outerHeight / 2,
        )
      }
    }

  }

  /**
   * Allows to call store action in order to move the current position to the selected target.
   */
  moveToTarget(): void {

    if (this.isFreeModeGraphic) {
      this.$store.dispatch(
          'dataEntry/goto',
          {
            vm: this,
            formFieldGroup: null,
            target: this.selectedTarget,
          },
      )
          .then( () => {
            this.$router.push( { name: ROUTE_DATA_ENTRY__RUNNING } )
          } )
    }

  }

  /**
   * Initialize the graphic.
   */
  private initialize(): void {

    this.mainView.addLoading()
    this.selectedContour = null

    this.$nextTick( () => {

      this.drawers = []

      if (this.active) {
        this.mainSVG.clear()

        this.handleGraphicSize()
        this.$nextTick( this.initializeDrawers )
      }

      this.mainView.removeLoading()
    } )

  }

  /**
   * Updates the graphic size according to the project platform.
   */
  private handleGraphicSize(): void {

    let points = [
      ...Array.from( this.project.platform.positionMap.keys() ),
      ...Array.from( this.project.platform.zheMap.keys() ),
    ]

    let pointsByX = GraphicService.orderByX( points )
    let pointsByY = GraphicService.orderByY( points )

    let minPointXY = [
      !!pointsByX[0]
          ? Number( pointsByX[0].split( ',' )[0] )
          : 0,
      !!pointsByY[0]
          ? Number( pointsByY[0].split( ',' )[1] )
          : 0,
    ]
    let maxPointXY = [
      !!pointsByX[pointsByX.length - 1]
          ? Number( pointsByX[pointsByX.length - 1].split( ',' )[0] )
          : 0,
      !!pointsByY[pointsByY.length - 1]
          ? Number( pointsByY[pointsByY.length - 1].split( ',' )[1] )
          : 0,
    ]

    this.size = [[Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER], [0, 0]]
    this.size[0][0] = min(minPointXY[0], this.size[0][0])
    this.size[0][1] = min(minPointXY[1], this.size[0][1])
    this.size[1][0] = max(maxPointXY[0], this.size[1][0])
    this.size[1][1] = max(maxPointXY[1], this.size[1][1])

  }

  /**
   * Initialize the drawers to create the drawers based on the platform.
   */
  private initializeDrawers(): void {
    this.drawers = []

    const stateColorMap = this.project.getStateCodesColorMap()
    const zheColorMap = this.project.getNatureZheColorMap()

    this.devices.forEach(
        ( device: Device ) => {
          const deviceDrawers = GraphicService.buildDrawerOfEvaluable(
              device,
              device.individualPU,
              this.project.graphicalStructure,
              this.maxSize,
              stateColorMap,
              zheColorMap,
              this.project.dataEntry.restrictedObject,
          )
          this.drawers = [...this.drawers, ...deviceDrawers]
        },
    )

    this.handleGridLines()

    setTimeout(() => {
      const nbPass = Math.ceil(this.drawers.length / GraphicService.GRAPH_BUILD_SIZE)
      const promises: Promise<any>[] = []
      for (let pass = 0; pass < nbPass; pass++) {
        promises.push(...this.drawers
            .slice(pass * GraphicService.GRAPH_BUILD_SIZE, (pass + 1) * GraphicService.GRAPH_BUILD_SIZE)
            .map(drawer => this.main(drawer)))
      }
      Promise.all(promises).then(() => this.handleLastMovementArrow())
    }, 100)

  }

  /**
   * Build an arrow to draw the last movement if exists.
   */
  private handleLastMovementArrow(): void {
    if (!!this.project.dataEntry.lastPosition && !!this.project.dataEntry.currentPosition) {
      const current = (this.project.platform.uriMap.get( this.project.dataEntry.currentPosition ) as BusinessObject)
      const lastOne = (this.project.platform.uriMap.get( this.project.dataEntry.lastPosition ) as BusinessObject)
      if (!!current.center && !!lastOne.center) {
        const arrow = this.mainSVG.line( lastOne.center[0], lastOne.center[1], current.center[0], current.center[1] )
            .stroke( { width: 5, color: GraphicService.CURRENT_COLOR, dasharray: '15, 15' } )
        this.frontElements.push( arrow )
      }
    }
  }

  /**
   * Build the lines that together show the platform grid.
   */
  private handleGridLines(): void {

    const baseMovementWithMargin = GraphicService.BASE_MOVEMENT_SPACING * 11
    for (let x = 1; x <= this.maxSize[0]; x++) {
      const pA: any = GraphicService.extractPointWithMargin( [x, 0], this.project.graphicalStructure )
      const pB: any = GraphicService.extractPointWithMargin( [x, this.maxSize[1]], this.project.graphicalStructure )
      pA.xCenter = pA.xCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseWidth / 2
      pA.yCenter = pA.yCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseHeight / 2
      pB.xCenter = pB.xCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseWidth / 2
      pB.yCenter = pB.yCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseHeight / 2

      const line = this.mainSVG.line(
          pA.xCenter + baseMovementWithMargin, pA.yCenter,
          pB.xCenter + baseMovementWithMargin, pB.yCenter + Math.round( baseMovementWithMargin / 2 ),
      )
          .stroke( { width: 1, color: 'lightgrey' } )
      this.frontElements.push( line )
      const header = this.xRowSVG.text( ( add: any ) => {
        const position = this.project.graphicalStructure.getOrigin().includes( 'right' )
            ? this.maxSize[0] - x
            : x - 1
        if (position !== 0) {
          add.tspan(position)
              .dx(pA.xCenter - (this.project.graphicalStructure.baseWidth / 2))
              .dy(GraphicService.FONT_SIZE)
              .font({size: GraphicService.FONT_SIZE, family: 'Roboto'})
        }
      } )
      this.frontElements.push( header )
    }

    for (let y = 1; y <= this.maxSize[1]; y++) {
      const pA: any = GraphicService.extractPointWithMargin( [0, y], this.project.graphicalStructure )
      const pB: any = GraphicService.extractPointWithMargin( [this.maxSize[0], y], this.project.graphicalStructure )
      pA.xCenter = pA.xCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseWidth / 2
      pA.yCenter = pA.yCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseHeight / 2
      pB.xCenter = pB.xCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseWidth / 2
      pB.yCenter = pB.yCenter - GraphicService.BASE_MOVEMENT_SPACING * 5 + this.project.graphicalStructure.baseHeight / 2

      const line = this.mainSVG.line(
          pA.xCenter, pA.yCenter + baseMovementWithMargin,
          pB.xCenter + Math.round( baseMovementWithMargin / 2 ), pB.yCenter + baseMovementWithMargin,
      )
          .stroke( { width: 1, color: 'lightgrey' } )
      this.frontElements.push( line )
      const header = this.yColSVG.text( ( add: any ) => {
        const position = this.project.graphicalStructure.getOrigin().includes( 'bottom' )
            ? this.maxSize[1] - y
            : y - 1
        if (position !== 0) {
          add.tspan(position)
              .dx(5)
              .dy(pA.yCenter + (this.project.graphicalStructure.baseHeight / 2))
              .font({size: GraphicService.FONT_SIZE, family: 'Roboto'})
        }
      } )
      this.frontElements.push( header )
    }

  }

  /**
   * Resize the draw container based on the calculated draw size.
   * If exists, the container will be moved to the current position, move to top left otherwise.
   */
  private resizeDrawContainer(): void {

    if (!!this.mainSVG) {
      this.mainSVG.size( this.drawSize[0], this.drawSize[1] )
      this.xRowSVG.size( this.drawSize[0], GraphicService.FONT_SIZE * 1.5 )
      this.yColSVG.size( GraphicService.FONT_SIZE * 2, this.drawSize[1] )
      if (this.hasCurrentPosition) {
        this.centerOnCurrentObject()
      } else {
        let movePoint = GraphicService.extractPointWithMargin( this.size[0], this.project.graphicalStructure )
        this.setPosition( movePoint.xCenter, movePoint.yCenter )
      }
    }

  }

  /**
   * Scale the draw container according to the zoom param.
   *
   * @param zoom
   */
  private setZoom( zoom: number ): void {

    let prefix = ['webkit', 'moz', 'ms', 'o']
    let scale = 'scale(' + zoom + ')'
    let origin = 'top left'

    let container: any = this.$refs['draw-container']

    container.children.forEach( ( element: any ) => {
      for (let i = 0; i < prefix.length; i++) {
        element.style[prefix[i] + 'Transform'] = scale
        element.style[prefix[i] + 'TransformOrigin'] = origin
      }

      element.style['transform'] = scale
      element.style['transformOrigin'] = origin

    } )

  }

  /**
   * Scroll the draw container parent to the given X, Y position.
   *
   * @param x
   * @param y
   */
  private setPosition( x: number, y: number ): void {

    let viewer: any = this.$refs['draw-viewer']
    viewer.$el.scrollLeft = x
    viewer.$el.scrollTop = y

  }

  /**
   * Given a draw wrapper, add the polygon to the SVG. If the evaluable has no polygon contour, run the explore algorithm,
   * to build it. After adding the polygon, instantiate all callbacks and event relative to the user actions (click).
   *
   * @param drawer
   */
  private main(drawer: GraphicRequestWrapper): Promise<any> {

    // An initial point is required to draw the polygon contour.
    // Without it, the object might not be complete (spacialization concern).
    if (!drawer.initialPoint) {
      ++this.polygonDoneCount
      return
    }

    // When object polygon is not yet known, explore the grid to build the polygon and save it.
    if (!drawer.polygonContour || 1 === drawer.polygonContour.length && '' === drawer.polygonContour[0]) {
      drawer.polygonContour = ['']
      let count = 0
      do {
        // Build the polygon for the current object.
        GraphicService.explore( drawer.initialPoint, drawer.initialDirection, drawer, this.project.graphicalStructure )

        drawer.polygonContour = drawer.polygonContour
            .map(
                ( contour: string ) => {
                  return GraphicService.optimizeContour( contour )
                },
            )

        GraphicService.updateDrawer( drawer, this.project.graphicalStructure )
        ++count
      } while (0 < drawer.pointsInForm.length && GraphicService.MAX_GRAPH_LOOP > count)

      drawer.target.polygonContour = drawer.polygonContour
      drawer.target.center = GraphicService.getPolygonMeanCenter( drawer.polygonContour[0] )
    }

    // Create promise to parallelize drawing treatment and perform some action when polygon drawing is complete
    return new Promise<any>((resolve: (...args: any) => void) => {
      let polygons = drawer.polygonContour.map(
          ( contour: string ) => {
            return this.mainSVG.polygon( contour ) // Draw the polygon.
                .fill( drawer.color )
                .stroke( { width: 1, color: GraphicService.STROKE_COLOR } )
          },
      )
      resolve( polygons )
    } )
        .then( ( polygons: any[] ) => {

          const { xCenter, yCenter } = GraphicService.extractPointWithMargin(
              drawer.initialPoint.split( ',' )
                  .map( v => Number( v ) ),
              this.project.graphicalStructure,
          )
          let textName = null
          // 2 cases where the level is the last one : SURFACE PU => PU & INDIVIDUAL PU => INDIVIDUAL
          if (
              !(drawer.target instanceof OutExperimentationZone) && (
                  GraphicService.INDIVIDUAL_LEVEL === drawer.level ||
                  !drawer.individualPU && GraphicService.PARCEL_LEVEL === drawer.level
              )
          ) {
            textName = this.mainSVG.text( ( add: any ) => {
              add.tspan(
                  this.project.dataEntry.visitedBusinessObjectsURI.includes( drawer.target.uri )
                      ? 'X'
                      : drawer.target.name,
              )
                  .dx( xCenter - ((this.project.graphicalStructure.baseWidth / 2) + 5 * (drawer.level - 1)) )
                  .dy( yCenter + (GraphicService.FONT_SIZE / 3) )
                  .font( { size: GraphicService.FONT_SIZE, family: 'Roboto' } )
            } )

          }

          if (this.project.dataEntry.currentPosition === drawer.target.uri) {
            let drawPolyline = async () => {
              drawer.polygonContour.forEach(
                  ( contour: string ) => {
                    this.mainSVG
                        .polyline( drawer.polygonContour + contour.split( ' ' )[0] )
                        .fill( 'none' )
                        .stroke( { width: 5, color: GraphicService.CURRENT_COLOR, dasharray: '15, 15' } )
                  },
              )
            }
            drawPolyline()
          }

          if (
              !(drawer.target instanceof OutExperimentationZone) && (
                  !this.project.dataEntry.restrictedObject
                  || 0 === this.project.dataEntry.restrictedObject.length
                  || this.project.dataEntry.restrictedObject.includes( drawer.target.uri )
              )
          ) {
            if (!!textName) {
              textName.on( 'click', () => this.onClickTarget( drawer ) )
            }
            polygons.forEach(
                ( polygon: any ) => {
                  polygon.on( 'click', () => this.onClickTarget( drawer ) )
                },
            )
          }

          ++this.polygonDoneCount

        } )

  }

}

</script>

<style lang="scss" scoped>

.full-height {
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;
}

.overflow-scroll-a {
  overflow: scroll;
}

</style>
