import { BusinessObject } from '@adonis/app/models/business-objects/business-object'

/**
 * Interface of graphic data wrapper.
 */
export interface GraphicRequestWrapperInterface {
  level: number
  pointsInForm: string[]
  polygonContour: string[]
  initialPoint: string
  initialDirection: number
  target: BusinessObject
  individualPU: boolean
  color: string
}

/**
 * Wrapper to contain information about graphic element that need to be drown on graphic tab.
 */
export default class GraphicRequestWrapper implements GraphicRequestWrapperInterface {
  constructor(
      public level: number,
      public pointsInForm: string[],
      public polygonContour: string[],
      public initialPoint: string,
      public initialDirection: number,
      public target: BusinessObject,
      public individualPU: boolean,
      public color: string,
  ) {
  }
}