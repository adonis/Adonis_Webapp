2.6.8 (2024-12-19)
------------------

* Bug fix : additionals corrections to generate xml from mobile data entries with annotations and state codes.

2.6.7 (2024-12-19)
------------------

* Bug fix : additionals corrections to generate xml from mobile data entries with variable values list.

2.6.6 (2024-12-18)
------------------

* Bug fix : additionals corrections to generate xml from mobile data entries with variable values list.

2.6.5 (2024-12-12)
------------------

* Bug fix : additionals corrections to generate xml from mobile data entries with out of experimentation zone

2.6.4 (2024-12-09)
------------------

* Bug fix : additional correction to generate xml from mobile data entries with generator variable

2.6.3 (2024-12-05)
------------------

* Bug fix : additional correction to generate xml from mobile data entries with generator variable

2.6.2 (2024-12-03)
------------------

* Bug fix : correction to generate xml from mobile data entries with generator variable

2.6.1 (2024-11-18)
------------------

* Symfony update for multiple CVE fixes

2.6.0 (2024-10-31)
------------------

* Bug fix : logo for preprod environment
* Bug fix : transfert data entry projects to mobile if benchmark not deleted
* Bug fix : set data entry project owner from platform if missing
* Bug fix : various bug fixes
* Export variables
* Edit path base and user path

2.5.3 (2024-10-16)
------------------

* Bug fix : import of XML variables collection
* Bug fix : data entries to webapp

2.5.2 (2024-10-14)
------------------

* Bug fix : import of XML platform with data entries

2.5.1 (2024-10-09)
------------------

* Bug fix : import of XML platform with tests

2.5.0 (2024-10-04)
------------------

* XML serialisation / deserialisation rewrite

2.4.3 (2024-09-06)
------------------

* Bug fix : displaying entry date of previous data entries when connecting data
* Bug fix : export of graphic edition as image

2.4.2 (2024-07-11)
------------------

* Bug fix : import of CSV experiment.
* Bug fix : export project file download.

2.4.1 (2024-07-08)
------------------

* Bug fix : always generate zip if response is allready transfered.
* Bug fix : validate in csv that two traitments cannot have same modalities.

2.4.0 (2024-06-18)
------------------

* Bug fix : adding "missing data" row in data fusion
* Bug fix : Site duplication
* Bug fix : Cannot add advanced right on experimenter within a platform
* Bug fix : Allow naming result of data entry fusion
* Bug fix : Administration password reset
* Add geometry to Business Object
* Add experiment to Sixtine
* Add "uri" to Business Object
* Import germplasm as factors and modalities from Sixtine
* Export data entries to Sixtine

2.3.6 (2024-03-06)
------------------

* Minor/Major/Blocking bug fixes

2.3.5 (2024-02-08)
------------------

* Minor/Major bug fixes
* project data view optimization

2.3.4 (2023-11-28)
------------------

* Allow big data entries to be viewed

2.3.3 (2023-10-19)
------------------

* Blocking/Minor/major bug fixes

2.3.2 (2023-09-08)
------------------

* Import path
* Improve improvised data entry project
* Minor/major bug fixes

2.2.10 (2023-05-03)
------------------

* Minor/major bug fixes

2.2.9 (2023-05-02)
------------------

* Blocking bug fix : connected variables
* Minor/major bug fixes

2.2.8 (2023-03-10)
------------------

* Minor/major bug fixes

2.2.7 (2023-01-25)
------------------

* Minor/major bug fixes

2.2.6 (2022-12-16)
------------------

* Add experiment replication
* Improve design left menu
* Minor/major bug fixes

2.2.5 (2022-11-29)
------------------

* Minor/major bug fixes

2.2.4 (2022-11-15)
------------------

* Minor/major bug fixes

2.2.3 (2022-10-20)
------------------

* Add benchmark functionality that was missing
* Minor/major bug fixes

2.2.2 (2022-09-19)
------------------

* Add a command to hard delete soft deleted files
* Minor/major bug fixes

2.2.1 (2022-08-30)
------------------

* Add missing PDF translations
* Minor/major/blocking bug fixes

2.2.0 (2022-07-20)
------------------

* Allow to store files on an AWS S3 server
* Minor/major bug fixes

2.1.0 (2022-07-04)
------------------

* Allow a user to clone a user path for another user
* Add the possibility to import variables at XML format (or ZIP if it needs attachment)
* Minor bug fixes

2.0.6 to 2.0.9 (from 2022-06-13 to 2022-07-01)
------------------

* Minor/Major Bug fixes

2.0.5 (on 2022-06-09)
------------------

* Fix blocking bugs
* Code cleanup

2.0.4 (on 2022-06-08)
------------------

* Permits to add or delete attachments from property view
* Detect conflicts when merging 2 data entries
* Selection by object property on graphical editor
* Bug fixes

2.0.3 (on 2022-06-01)
------------------

* Allow object filtering in graphical editor
* Documentation update

2.0.2 (on 2022-05-27)
------------------

* Manage north indicator in graphical editor
* Allow to add text zone in the graphical editor

2.0.1 (on 2022-05-25)
------------------

* MVP version for Adonis Webapp
* Allow creating and following experiments lifecycle
* Synced with local Adonis mobile instance

1.1.11 (on 2022-02-02)
------------------

* Bugfix image display on value hint helper when used over an alphanumeric field
* When hitting enter key on a "dead" individual, the form will be automatically submitted without hitting enter on every
  following variable
* When starting typing, the value list helper on an alphanumeric field will automatically open
* Generated variable popup will now wait for value list hook when the alphanumeric field is the last field before
  validation

1.1.10-beta (on 2021-11-03)
------------------

* XML export with value list fixed
* Auto increment move mode in data entry process fixed
* Insert all non visited business object as missing value when closing data entry process

1.1.9-beta (on 2021-07-08)
------------------

* Hotfix display of X,Y coordinate in individual unit plot breadcrumb

1.1.8-beta (on 2021-07-08)
------------------

* Hotfix state code and variable modification date in local project cause export to crash

1.1.7-beta (on 2021-07-07)
------------------

* Bugfix sort on user results in admin vue on Online interface when "all" is selected as page size
* Data have been lost due to Google Chrome miss manipulation and data manual deletion. To prevent future inconvenient,
  data can now be saved and restore as file on user demand
* Bugfix breadcrumb data X,Y position in individual PU to show the smallest individual X,Y couple instead of pu X,Y
  position
* Bugfix X,Y coordinate shown as 0,0 in CSV export of note pad
* Bugfix state code missing data given to any unactivated variable
* Review previous data visualisation to allow display of multiple previous data AND short name visual
* Review rewrite response file command to use the same date as the previous file

1.1.6-beta (on 2021-05-07)
------------------

* Add colored button to access path direction break information (red i button)
* Bugfix state code propagation to update vue rendering after change being applied to objects

1.1.5-beta (on 2021-04-21)
------------------

* Adding sound effect when using forced workflow on path direction change
* Update translation

1.1.4-beta (on 2021-03-29)
------------------

* Update background when login onto ONLINE page
* Bugfix scale when using "disable numeral keyboard option"

1.1.3-beta (on 2021-03-08)
------------------

* Add permission to remove state code "missing data" in one click on the clear icon no matter the field type
* Update date format in the whole app to use YYYY-MM-DD
* Bugfix list component

1.1.2-beta (on 2021-03-05)
------------------

* Bugfix generated variable added into variable map

1.1.1-beta (on 2021-03-01)
------------------

* Bugfix information about current navigation mode different from the one currently in use.
* Bugfix end of workflow alert message was displayed when entering last form rather than when validating last form.
  Fixed.
* Bugfix when multiple confirm popups were to show in a chain, only the first one was displayed. Fixed.

1.1.0-beta (on 2021-02-22)
------------------
Major change on measure process :

* Bugfix switch between individuals on large project can be slow. Whole measure system has been reworked in order to
  perform asynchronously

Minor changes included :

* Bugfix on number format when writing the xml. Adonis Bureau only accepts xxxxx.xx format where values were formatted
  xx,xxx.xx
* Bugfix switch between two use on same device may produce unwanted access to project not owned by the one actually
  connected
* Bugfix unable the user to use comma or dot decimal separation on integer variable
* Bugfix show up dialog when the user validates the last member of a workflow no matter if the whole workflow has been
  measured or not
* Add a button on semi-automatic variable dialog to allow the user to disconnect the selected serial port and renew
  connection process
* Bugfix to allow clear state code by clic on date and time fields

------------------

1.0.6-beta
------------------

* Hotfix on X,Y coordinate navigation system.

1.0.5-beta
------------------

* Bugfix in snackbar to wrap instead of x-overflow
* Add a scroll request when validating a measure in block form
* Bugfix click on "validate" in generator variable popup
* Bugfix allowing work path using individual ident to move to parent if no variable fount concerning individuals
* Bugfix data return when adding photos
* Bugfix mandatory variable alert on each required field instead of only on form validation

1.0.4-beta
------------------

* Bugfix on import single variable and library
* Add delete previous variable library when new one get imported and add single variable to current library.

1.0.3-beta
------------------

* Bugfix position shown in breadcrumb on individual parcels
* Bugfix on statistic tab in data entry to handle histogram update
* Update visualisation filter design for a better understanding
* Update export process to handle boolean variable default value
* Update field label of sub block creation in spacialization

1.0.2-beta (on 2020-11-05)
------------------

* Bugfix on data entry project synchronization process
* Bugfix to handle test on connected variable that is connected to another variable
* Bugfix on import and export line deletion process
* Bugfix project import failure when treatment doesn't have repetition attribute in XML file
* Bugfix first variable selection
* Bugfix scroll to top when automatically select first variable
* Bugfix select column in visualization tab doesn't work when click occurs on checkbox

1.0.1-beta (on 2020-11-04)
------------------

* Fix bug in spacialization reader, user:create command and update doc
* Add surface parcel support and required metadata support with some bugfix
* Hotfix password change fields and disable the Android keyboard on numeric fields when requested

1.0.0-beta (on 2020-10-16)
------------------

* Adonis Mobile "beta" delivery with in code dev documentation
* Install prod & dev server documentation
