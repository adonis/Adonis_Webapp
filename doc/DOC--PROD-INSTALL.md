# Install Adonis Webapp for production

## Installation procedure

### Requirements

  * Server apache with HTTPS enabled and postgresql 11 (and contrib) installed and running
  * on the local machine where code will be compiled, php7.4 and [its extensions](../composer.json)
  * Git command line to clone repository
  * composer to manage backend dependencies
  * nodejs and npm to manage frontend dependencies
  * vim or any other text editor for configuration

### Procedure

To proceed, open two different console and connect one to the remote server. In this procedure,
"on the local machine" references the local console on the machine where the code is compiled and
"on the remote machine" references the remote console connected to the server.

#### Prepare database

(on the remote machine)

Connect on the remote server where the postgresql service is running and as <code>postgres</code> user
enter psql console <i>(Non root user can start the command bellow with sudo)</i>:
```
/usr/pgsql-11/bin/postgresql-11-setup initdb
systemctl start postgresql-11.service
systemctl enable postgresql-11.service
systemctl restart postgresql-11.service
```
```
su postgres
```

On bash console as postgres, enter psql console:
```
psql
```

On psql console, create database and role that are going to be used by the application. You may
change <adonisdb>, <username> and <user-password> with your own configuration, just remember them
in order to connect the application with this database:
```
postgres=# CREATE DATABASE <adonisdb>;
postgres=# CREATE ROLE <usernane> WITH PASSWORD '<user-password>';
postgres=# GRANT ALL PRIVILEGES ON DATABASE <adonisdb> TO <usernane>;
postgres=# ALTER ROLE <usernane> WITH LOGIN;
postgres=# CREATE EXTENSION unaccent;
```

Leave the psql prompt and postgresql account and return to root

To allow Adonis Mobile app to connect to local database, edit <code>/var/lib/pgsql/11/data/pg_hba.conf</code>
and add the lines for adonis <username> role on <adonisdb>:
```
# IPV4
host    adonisweb       adonis          127.0.0.1/32            md5
# IPV6
host    adonisweb       adonis          ::1/128                 md5
```

Now restart postgresql service:
```
systemctl restart postgresql-11.service
```

#### Get the code

(on the local machine)

First clone the code from the git repository in a local folder on your computer using git command:
```
git clone ssh://git-repository-url-here
```

#### Configure

(on the local machine)

enter the cloned directory and create a <code>.env.local</code> file:
```
cd adonis-terrain
touch .env.local
```

Open <code>.env</code> file to see the available configurations that need to be updated with
the system where to install the application. Override the configuration in freshly created
<code>.env.local</code> file :
```
### Modify environment of .env file.

### Development environment.
APP_ENV=prod

### Personal url to connect to postgresql 11 on server.
DATABASE_URL=postgresql://<usernane>:<user-password>@localhost:5432/<adonisdb>?serverVersion=11&charset=utf8

### Base url used in client and app for API calls.
### This env variables are given to the Vue.js app using webpack encore.
BASE_URL=<adonis-base-url>
API_URL=https:<adonis-base-url>/api/
APP_URL=https:<adonis-base-url>/app/
WEBAPP_URL=https:<adonis-base-url>/webapp/

### Symfony Mailer configuration.
MAILER_DSN=smtp://<smtp-url-for-adonis-account>
MAILER_SENDER='<adonis-mail>'

### Configuration: Upload directory to be used on the server (NO "/" AT THE END).
UPLOAD_DIR='<full-path-upload-directory'

ALGORITHM_DIR='<relative-path-algorithm-directory'

### Define authentication mod.
xxxxxxxxx
```

At this point, authentication mod has to be set between "self managed" or "LDAP". Depending
on this choice complete .env.local file with the appropriate configuration (xxxxxxxxx):
  * Self managed:
```
LDAP_ON=false
```
  * LDAP:
```
LDAP_ON=true
LDAP_HOST=<ldap-host>
LDAP_PORT=<ldap-port>
LDAP_ENCRYPT=<ldap-encrypt>
LDAP_MAIL=<ldap-mail>
LDAP_BASE_DN=<ldap-base-dn>
LDAP_SEARCH_DN=<ldap-search-dn>
LDAP_SEARCH_PASSWORD=<ldap-search-password>
```

#### Compile

(on the local machine)

Now, install node dependencies and composer dependencies using:
```
composer install --no-dev --optimize-autoloader
npm install
npm run build
```

#### Upload to server

(on the remote machine)

Create a directory where the compiled code will be synchronized in connected user's home directory:
```
mkdir ~/adonis-rsync
```

(on the local machine)

Then synchronize the compiled code to this directory. The '.' dot in following command reprents
the compiled code directory, here the local console is open in this directory:
```
rsync -av -e ssh --exclude='var' --exclude='node_modules' --exclude='assets' --exclude='.git' . <remote-user>@<remote-host>:~/adonis-rsync
```

#### Install on server

(on the remote machine)

Create the application structure in remote www/html directory:
```
/var/www/html
    /adonisweb
        /current (symlink to the current running version)
        /release-<version_number> (the version being release during install)
        /shared (shared ressources (like upload / algorithms)
```

Copy adonisweb file to release location:
```
mkdir /var/adonisweb/release-<version_number>
cp -rv ~/adonis-rsync /var/www/html/adonisweb/release-<version_number>
chown -R apache:apache /var/www/html/adonisweb
```

Build the cache:
```
cd /var/www/html/adonisweb/release
sudo -u apache -s /bin/bash -c 'php bin/console cache:clear'
sudo -u apache -s /bin/bash -c 'php bin/console cache:warmup'
```

#### Install

(on the remote machine)

Installation script is available under <code>database</code> directory. Find
the SQL script containing the "initial" value and execute it on the database.
Replace in following command the terms adonisdb and adonisrole with your specific environment:
```
psql adonisdb adonisrole < adoniswebapp-db--v1.0.0--initial.sql
```

#### Configure apache server

Edit apache configuration by adding or modifying adonis conf file located in
<code>/etc/httpd/conf.d/adonisweb.conf</code> and modify virtualhost configuration like follow:
```
<VirtualHost *:80>
    #ServerName www.example.com
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/adonisweb/current/public

    <Directory />
        Options FollowSymLinks
        AllowOverride All
    </Directory>
    <Directory /var/www/html/adonisweb/current/public/>
        Options Indexes FollowSymLinks Multiviews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/adonisweb-error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

Then restart apache server:
```
systemctl restart httpd.service
```

### Install JWT Token authentication system

(on the remote machine)

This step requires openssl to be installed. It follows API Platform instructions regarding the use
of JWT token authentication system.

> https://api-platform.com/docs/core/jwt/

In <code>/var/www/html/adonisweb/release</code> directory:
```
set -e
mkdir -p config/jwt
jwt_passphrase=${JWT_PASSPHRASE:-$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')}
echo "$jwt_passphrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
echo "$jwt_passphrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
```

The above command generate <code>config/jwt</code> directory with pemkey to encrypt tokens.
To allow apache user to access this data, change owner:
```
chown -R apache:apache config/jwt
```

Copy the content of JWT config in the shared folder in order to keep the same keys for each release

#### create symlink to current

As the virtualhost points on <code>current</code> directory, rename the <code>release</code>
directory to <code>current</code>:
```
rm /var/www/html/adonisweb/current
ln -s /var/www/html/adonisweb/release-<version_number> /var/www/html/adonisweb/current
```

#### First install in self managed mode:

(on the remote machine)

To create the first admin user, use following command,
in <code>/var/www/html/adonisweb/current</code> directory:
```
sudo -u apache -s /bin/bash -c 'php bin/console adonis:user:create [-A] \'<username>\' \'<usermail>\' \'<userpass>\''
```
This command has 4 parameters each required and order must be respected:
  * <code>\<username></code>: The username of the operator between simple quotes '
  * <code>\<userpass></code>: The password of the operator between simple quotes '
  * <code>\<usermail></code>: The mail address of the operator between simple quotes '
  * <code>\<admin></code>: <code>true</code> or <code>false</code> set the user admin role

Finally, add cron task for apache to run asynchronous tasks in charge of file reading and writing on
the server.
```
*/2 * * * * /path_to_app_location/bin/console dtc:queue:run -d 120
* */2 * * * /path_to_app_location/bin/console adonis:graphic:clear -y
```

### Enable HTTPS protocol to allow PWA installation

(on the remote machine)

Enabling HTTPS on the apache server requires a certificate. Using a private key, a <code>.csr</code> file can
be generated to retrieve a valid certificate from a Certification Authority.

When done, upload the certificate <code>.pem</code> file and the private key to the remote server.
Store the private key in <code>/etc/pki/tls/private</code> directory, and the certificate in
<code>/etc/pki/tls/certs</code> directory. When done, edit httpd conf file to add SSL support.

First, ensure that <code>openssl</code> and <code>mod_ssl</code> are both installed on remote server. Then
modify the virtualhost file to handle SSL:
```
vim +/SSLCertificateFile /etc/httpd/conf.d/ssl.conf
```
```
SSLCertificateFile /etc/pki/tls/certs/<certifate.pem>
SSLCertificateKeyFile /etc/pki/tls/private/<private.key>
```
```
vim /etc/httpd/conf.d/adonisweb.conf
```
```
<VirtualHost *:80>
    Redirect permanent / https://www.example.com/
</VirtualHost>
<VirtualHost *:443>
    ServerName www.example.com
    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/adonisweb/current/public

    SSLEngine on
    SSLCertificateFile /etc/pki/tls/certs/<certifate.pem>
    SSLCertificateKeyFile /etc/pki/tls/private/<private.key>

    <Directory />
        Options FollowSymLinks
        AllowOverride All
    </Directory>
    <Directory /var/www/html/adonisweb/current/public/>
        Options Indexes FollowSymLinks Multiviews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/adonisweb-error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

The server is now configure to work under HTTPS protocol and needs to be restarted to apply the changes.
```
systemctl restart httpd.service
```

### Install algorithms

Adonis Webapp can use validated R scripts in order to generate randomized experiments. Algorithms are executed by R as
soon as PHP ask for an execution. This section tells how to install R and add new scripts

```
yum install R
```

Once installed, edit .env.local by adding a value for ALGORITHM_DIR. The value must be the relative path to the
directory used to store and execute R scripts.

Add the script file inside the configured directory and execute the following command.

```
sudo -u apache -s /bin/bash -c 'php bin/console adonis:algorithm:add algoName.R'
```

Generic information about the script arguments will be asked, like parameters. After that, the script will be executed
as if it will be by the app.
