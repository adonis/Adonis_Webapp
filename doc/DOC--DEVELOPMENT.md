# Adonis Mobile

## Development ressources

### Frontend

#### Vue Single Application

Adonis Mobile consist into 2 single page applications using Vue.js framework.
> https://vuejs.org/v2/guide/

Application sources are located in "assets" directory and devided into:
  * "app" directory where the Adonis Mobile App Vue.js application is located
  * "client" directory where the Adonis Mobile Online Vue.js application is located (deprecated by Adonis Webapp)
  * "webapp" directory where the Adonis Webapp is located
  * "shared" directory where the common component and ressources are stored

#### Vuetify

To looks like an Android application, Adonis Mobile uses the Vuetify component library.
> https://vuetifyjs.com/en/getting-started/quick-start/

#### Icon library

Icons used in Adonis Mobile are provided by "material-design-icons-iconfont"
following vuetify material icon recommandation.
> https://jossef.github.io/material-design-icons-iconfont/

To use an icon, find its name and simply add the icon using Vuetify icon component:

    <v-icon>icon_name</v-icon> 

### Backend

#### Symfony & Doctrine

Backend operation are performed using Symfony framework.
> https://symfony.com/doc/4.4/setup.html

Symfony source code are located in "src" directory and all its configuration in "config" directory.

#### API Platform

To expose entities, Adonis Mobile use API Platform built on symfony framework

> https://api-platform.com/docs/distribution/

Custom GET and POST requests are built using API Plateform recommandation. For more details:

> https://api-platform.com/docs/core/data-providers/ <br/>
> https://api-platform.com/docs/core/data-persisters/

## Development structure

### Vue.js apps compilation

Applications are managed and served by symfony projet. All assets are compiled using Encore webpack.

A dual export is used to export App and Client in two differents configurations
as App needs PWA and Webmanifest support in order to be installable and usable offline.

The file webpack.config.js details this process.

Applications are then served to the browser by "classic" symfony using twig and a base controller.

    src/Adonis/Application/Controller/FrontController.php

Two routes exists allowing to acces /app (App) or /online (Client)

Twig template are located in "templates" directory and contains Emails shapes and a simple html
template with the main div where the Vue.js app will be mounted.

### Database in development context

#### Create the development specific database

    sudo -u postgres psql <br/>
    postgres=# create database mydb; <br/>
    postgres=# create user myuser with encrypted password 'mypass'; <br/>
    postgres=# grant all privileges on database mydb to myuser; <br/>

> https://medium.com/coding-blocks/creating-user-database-and-adding-access-on-postgresql-8bfcd2f4a91e

In order to access the remote database from development computer
(Replace occurences of 'user' and 'ip-address' with your own configuration):

    ssh 'user'@adonis.trydev
    su - postgres
    nano /var/lib/pgsql/11/data/pg_hba.conf
    host all all 'ip-address' trust

    nano /var/lib/pgsql/11/postgresql.conf
    Look for: listen_addresses='localhost'
    And replace with: listen_adresses='*' (or add your own ip-address)

And restart postgresql service to apply modifications:

    systemctl restart posgtresql-11

> https://www.cyberciti.biz/tips/postgres-allow-remote-access-tcp-connection.html

#### Configure symfony project to use the dev database

In .env.local file, add the configuration reliable to your system and replace
"db_user", "db_password" and "db_name" with your own information:

    DATABASE_URL=postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8

### Email sender configuration

User management allows the administrator to define the email associated to each user.
This email is used by Adonis Mobile to send user password allowing the user to connect to its personnal space.

Add in .env.local file your configuration for MAILER:

    MAILER_URL=smtp://xxxx
    MAILER_SENDER=email@address.domain

As emails and frontend scripts are compile in order to communicate with the server, information about
the server URL is required and stored in the .env.local file

    APP_URL=
    WEBAPP_URL=

Using Twig global variables, those link are then added into emails body.

### Automatic tasks

Adonis Mobile uses DTC.QUEUE bundle in order to perform asynchronous jobs on server side.
Therefore, a command exists that can be manually called:
``` bash
php bin/console dtc:queue:run -d 120
```

The above command perform queue job for 120 seconds. That's why the command can be rerunned using
cron tab programmation:
``` bash
*/2 * * * * /path_to_app_location/bin/console dtc:queue:run -d 120 > /path_to_app_location/var/cronJob.log
```
