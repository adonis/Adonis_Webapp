# Adonis development platform

Adonis web app has been developed using a combination of an API built with Symfony + API-platform
and 2 single apps consuming the API build with vue.js and axios as HTTP client.

This documentation explains how to install development platform in order to run this project locally for development
purpose. Instructions are provided in order to run the application on a VM or either run the project with composer.

<i> It is possible to install other platforms and/or using local webserver provided by Symfony by configuring the
database connection and other url associations based on file env.local content.</i>

## Configuration

* Developments aim Centos 7 x64 as target OS but works with any server OS
* Apache2 server with PHP 7.4 and modules: apache_mod_php
* PostgreSQL 11 database (works with newer versions since Symfony/doctrine support them)
* VHost serving 3 roots: Main API entry, and 2 others entries for /app and /webapp the two vue.js apps

## Installing development virtual machine

### Configuring Centos server

#### Add EPEL and REMI Repository

    rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

#### Install HTTPD, PHP, POSTGRESQL and required modules

    yum --enablerepo=remi,remi-php74 install httpd php php-common php-zip

It might be possible that the firewall blocks http request.
Therefore, add both http and https to permanent allowed services :

    firewall-cmd --add-service=http --permanent
    firewall-cmd --add-service=https --permanent
    firewall-cmd --reload

Then start httpd service using :

    systemctl start httpd.service

Next, install both postgresql-server (if you run the database locally, postgresql instead) an php modules :

    yum --enablerepo=remi,remi-php74 install postgresql-server postgresql-contrib php-pgsql php-xml php-xml php-ldap

For more details, please refere to:
> https://linuxize.com/post/how-to-install-postgresql-on-centos-7/

#### Initialize postgresql local server and prepare database

    /usr/pgsql-11/bin/postgresql-11-setup initdb
    systemctl start postgresql-11.service
    systemctl enable postgresql-11.service

Open file <code> pg_hba.conf </code> located at <code> /var/lib/pgsql/data/ </code> and edit line

    host    all             all             127.0.0.1/32            ident
    host    all             all             ::1/128                 ident

to :

    host    all             all             127.0.0.1/32            trust
    host    all             all             ::1/128                 trust

Finally, connect to postgresql-server :

    su postgres
    psql

Now connected, create a role and a database for Adonis application :

    CREATE DATABASE <adonisdb>;
    CREATE ROLE <usernane> WITH PASSWORD '<user-password>';
    GRANT ALL PRIVILEGES ON DATABASE <adonisdb> TO <usernane>;
    ALTER ROLE <usernane> WITH LOGIN;

Add the unaccent extension to postgres in order to be allow the application to use the Unaccent function :

    CREATE EXTENSION unaccent;

#### Deploy code

Copy, sync or clone source code inside the apache vHost directory (/var/www/html by default)

## Frontend and Backend Development

### Dependencies

After synchronizing the development repo with the git remote repository, get and install dependencies
Make sure to have composer and node.js installed on development computer to enable dependencies management

Backend dependencies are handled using composer. Use composer command:

    composer install

Backend development dependencies (vendor dir) must be synchronized on remote server.

Front end dependencies are handled using node.js npm:

    npm install

Npm script are declared in order to start local dev-server. The application uses the app urls defined in
.env.local file, so make sure the urls point to dev server before running:

    npm run dev-ser

When dev-server is running, it will generate manifest file for assets in "public" directory.
Synchronize this directory with remote in order to enable assets and load the app by accesing
the remote (or local) host url.

### Configure symfony project to use the dev database (local)

In .env.local file, add the configuration reliable to your system and replace
"db_user", "db_password" and "db_name" with your own information:

    DATABASE_URL=postgresql://db_user:db_password@127.0.0.1:5432/db_name?serverVersion=11&charset=utf8

Then use doctrine bin/console command to check database state:

    php bin/console doctrine:migrations:status

Apply migrations on database to update it according to current entities structure:

    php bin/console doctrine:migrations:migrate

Read more on doctrine migration on:

> https://www.doctrine-project.org/projects/doctrine-migrations/en/2.2/reference/introduction.html

### Install JWT Token authentication system

Install openssl as root on remote server where the app is running.

    yum install openssl

> https://api-platform.com/docs/core/jwt/

    set -e
    mkdir -p config/jwt
    jwt_passphrase=${JWT_PASSPHRASE:-$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')}
    echo "$jwt_passphrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
    echo "$jwt_passphrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout

### Create a user using custom adonis command

Command below use 4 arguments where the last one defines whether the created user is or is not
an admin user:

    php bin/console adonis:user:create 'username' 'password' 'email@address.com' true

### Install or update the database

(on remote machine)

Check migrations availability using doctrine-migrations command:

```
sudo -u apache -s /bin/bash -c 'php bin/console doctrine:migrations:status'
```

When needed, run migrations using command:

```
sudo -u apache -s /bin/bash -c 'php bin/console doctrine:migrations:migrate'
```

### Install algorithms

Adonis Webapp can use validated R scripts in order to generate randomized experiments. Algorithms are executed by R as
soon as PHP ask for an execution. This section tells how to install R and add new scripts

```
yum install R
```

Once installed, edit .env.local by adding a value for ALGORITHM_DIR. The value must be the relative path to the
directory used to store and execute R scripts.

Add the script file inside the configured directory and execute the following command.

```
sudo -u apache -s /bin/bash -c 'php bin/console adonis:algorithm:add algoName.R'
```

Generic information about the script arguments will be asked, like parameters. After that, the script will be executed
as if it will be by the app.

### Run unit tests

In order to ensure that the application basis are not changed by development (especially on right management) a few unit
tests are available.

To run them, execute

```
php ./vendor/bin/phpunit 
```
