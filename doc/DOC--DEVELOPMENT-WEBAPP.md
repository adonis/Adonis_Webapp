# Adonis WebApp

## Frontend File structure overview

```
└── assets
    ├── app
    |   └── ...
    ├── client
    |   └── ...
    ├── shared
    |   └── ...
    └── webapp
        ├── constants
        ├── form-interfaces
        ├── models
        ├── components
        |   ├── commons & app level component ...
        |   └── modules
        |       ├── design
        |       ├── project
        |       ├── data
        |       ├── transfer
        |       └── admin
        ├── services
        |   ├── providers
        |   ├── transformers
        |   ├── data-manipulation
        |   ├── http
        |   |   └── entities
        |   └── other utility services
        ├── router
        ├── vuetify
        ├── stores
        └── translations
            ├── modules
            |   ├── admin.ts 
            |   ├── conception.ts
            |   ├── data.ts
            |   └── project.ts
            ├── messages.ts
            ├── modules.ts
            └── navigation.ts
```

### entities

#### models

All Objects are defined in this directory. Classes that represent objects only implements getters and setters. No other
data manipulation is allowed.

Calculated properties are possible since they are implemented as getters.

#### form-interfaces

Object used in forms, does not contain any work logic.

### components

All vue components will be located in this folder and sub-folders

#### app level components

Those folders hold all component related to the navigation. It includes nav bars, user menu and all other component that
aren't directly linked with modules or can be used anywhere.

#### modules

This folder will contain all main components. Vue components will be sorted in their related module's directory.

Most of the components have to be independent of each other in order to be reused in different situations.

Component will only deals about showing the information. They will only manipulate their related data and no other. In
order to do complex treatment, it has to call a service.

### plugins

In this directory, each Vue plugin have to be declared and configured.

### services

Each advanced treatment have to be implemented in a file under this folder. A service have to be declared as injectable
in the top level component and injected in each component that needs it.

A service can be related to a module or a specific part of the module.

#### providers

Each Entity need to be fetched to be used. services will implement this fetch action. A provider implements an interface
and is linked to a specific data source.

Services are also injectable services and can be used in eny component.

### stores

Stores are local mutation based storage. They can be used to store the state of the application, or an in-construction
object.

### translations

This folder contains the strings used in the application. They are defined in typescript files as JSON objects.

To add a translation, both en and fr tree must have the same nodes. They differ only on the leafs.

## Backend File structure overview

Each module will have its own file tree. Every module will follow the same scheme. Each sub directory will be explained
under and is not mandatory if empty.

├── WebApp
│ ├── Admin
│ │ ├── Entities
| │ │ ├── Persistence
| │ │ ├── Resource
| │ │ └── DTO
│ │ ├── Controllers
│ │ ├── DataProvider
│ │ ├── DataPersisters
│ │ ├── Voters
│ │ | ├── -EntityName-Voter.php
│ │ | ├── CustomsVoters
│ │ | └── ...
│ │ ├── Repositories
│ │ ├── Service
│ │ | ├── Transformers
│ │ | | ├── PersistenceResource
│ │ | | └── ResourceDTO
│ │ | ├── Actions
│ │ | | └── Helpers
│ │ | └── Utils
│ │ ├── EventListeners
│ │ └── Commands
│ ├── Conception
│ | └── ...
│ ├── Projects
│ | └── ...
│ ├── DataEntry
│ | └── ...
│ └── Shared
| └── ...
├── Mobile
└── Migrations

### Entity

All data class will be located in this directory. Entities only implements basic getters setters and have private
attributes.

#### Persistence

This directory will contain entities managed by the ORM in a database. Entities are not directly exposed to the API as
ApiResources.

#### Resource

Each entity in this directory will be exposed through the API. They will be the base of the exchange between client and
server.

#### DTO

When a resource entity need different input and output content, they will be created here.

### Controller

All custom routes will be defined in this module. Each class in this directory will handle multiple custom routes but
all routes in one class must share a same parent URL.

A controller have to manually call voters to grand or deny access to the resource. A controller must use services to
access the data.

### DataProvider

Data providers will be the link between the ORM and data exposed by the API. They will call Repositories and
transformers services.

Each entity class that need to be exposed have to be managed by a Data provider and a Persistence entity.

There will be 2 types of providers :

- Collection providers used to fetch en iterable object
- Item providers used to fetch single objects

If something have to be applied on multiple data classes, a doctrine extension have to be created. (e.g. add 'where
obj.creator == current user') [APIPlatform extensions](https://api-platform.com/docs/core/extensions/)

### DataPersisters

Data persisters will be called by APIPlatform for each modification, creation and deletion action. They will persist the
data given by call doctrine ORM or other persistence service. It's possible to chain multiple persisters for a single
entity in order to manage multiple storages.

Data persisters have to use Repository and transformers services. Data persisters don't have to care about security.

### Voters

According to API Platform documentation [APIPlatform Security](https://api-platform.com/docs/core/security/), voters are
the best way to run security hooks on both classic and custom routes.

Voters will only handle and block/allow general methods on objets. They won't look at the object relationship.

Each resource entity will have its own Voter class. There can be a unique or multiples attributes in a voter (one for
each HTTP method).

The voter directory will contain also voters for customs controllers if needed in a dedicated directory.

Voters will never filter a collection or query a related object for security check, this is the role of the
DataProviders.

### Repository

Repositories will act as collection. They will virtually contain all data for a specific class. Implementation have to
map the collection query to a database (or any datasource).

A repository can implement specific queries in order to handle many business use cases.

### Service

This directory will contain all injectable services that will help the application. Each service must be Stateless.

#### Transformers

Transformers are services that transform resources entities to DTO entities when DTO have to be set.
see [APIPlatform DTO](https://api-platform.com/docs/core/dto/)

It's also possible to define transformers for the persistence layer.

#### Actions

This directory will contain all services that define data manipulations. Controllers have to use those classes to avoid
any code duplication.

Action services can use Utils services to organize and factorize functionalities.

#### Utils

This directory will contain atomic functionalities and treatments. There can only be used in action services

### EventSubscribers

Event subscribers are triggered functions that listen to certain events.
see [APIPlatform events](https://api-platform.com/docs/core/events/)
and [Symfony event listeners](https://symfony.com/doc/current/event_dispatcher.html)

The symfony autoconf will detect them and register them automatically. If the event subscriber is involved in circular
dependencies, it can be replaced by an event listener.

### Command

This folder will contain all custom command that an admin can call via the bin/console command line executable.

## Classic development process

This part will explain the integration of a new functionality on both server and client side. In this example, we will
explain how to add a function that clone a Protocole and ask a new name for the Protocole

### Front-end

The first step is to create a menu action in the protocol representation that will trigger the corresponding popup.

This action will call the main view popup function to show the correct form by passing him the protocol as a payload.

Then, create the Vue component that ask a new name in the right directory :
assets/webapp/components/modules/conception/forms/CloneProtocoleForm.ts

This component will react to the validation of the new name by calling the service that handle Protocol data
manipulation providing the new name and the existing protocole : ProtocolService.

In this service, we must create a new method to clone the protocol. This service will call a new action in the
ConceptionActionService.

This action have to return a new protocol with the right name in it. This action will also call the existing action that
clone a factor for each of the protocol's factor.

Back to the service, it also has to call the PlatformAPIDAO in order to push the copy to the server side.

### Back-end

On the server, the pushed data will be binded on a DTO object or directly on a Ressource object.

The first class to implement is a Voter. This voter class have to verify that the user have the rights to create a
protocol ony with its Role. In that case, a push operation on Protocol is granted for the Platform_Manager and higher.

Then, the data persister will recieve the entity to persist. First, it must verify the integrity of the data, customs
rights then persist the protocol in database.

A data provider must also be able to retrieve the just created protocol in order to respond to the request with infos
like its ID.
