# Update a currently running version of Adonis Webapp

## Update procedure

### Requirements

This procedure requires that Adonis Webapp has been installed on the remote server
following the steps in [Installation procedure](./DOC--PROD-INSTALL.md).

As all requirements has been fulfilled during first installation, only new dependencies
has to be installed.

### Compiling and preparing new version

(on the local machine)

Open the directory containing the source code at the new version.
Ensure that the <code>.env.local</code> is in it and contains the information relative
to the current installation:
```
### Modify environment of .env file.

### Development environment.
APP_ENV=prod

### Personal url to connect to postgresql 11 on server.
DATABASE_URL=postgresql://<usernane>:<user-password>@localhost:5432/<adonisdb>?serverVersion=11&charset=utf8

### Base url used in client and app for API calls.
### This env variables are given to the Vue.js app using webpack encore.
BASE_URL=<adonis-base-url>
API_URL=https:<adonis-base-url>/api/
APP_URL=https:<adonis-base-url>/app/
WEBAPP_URL=https:<adonis-base-url>/webapp/

### Symfony Mailer configuration.
MAILER_DSN=smtp://<smtp-url-for-adonis-account>
MAILER_SENDER='<adonis-mail>'

### Configuration: Upload directory to be used on the server (NO "/" AT THE END).
UPLOAD_DIR='<full-path-upload-directory'

### Define the Application version.
VERSION=1.0.0-beta

### Define authentication mod.
LDAP_ON=false
```

Now, install node dependencies and composer dependencies using:
```
composer install --no-dev --optimize-autoloader
npm install
npm run build
```

### Upload to server

(on the remote machine)

Create a directory where the compiled code will be synchronized in connected user's home directory.
If the directory already exists, be sure the directory is empty:
```
mkdir ~/adonis-rsync
```

(on the local machine)

Then synchronize the compiled code to this directory. The '.' dot in following command reprents
the compiled code directory, here the local console is open in this directory:
```
rsync -av -e ssh --exclude='var' --exclude='node_modules' --exclude='assets' --exclude='.git' . <remote-user>@<remote-host>:~/adonis-rsync
```

### Install new release version

(on the remote machine)

After connecting on the remote machine using the same user as the one used in synchronization procedure,
move the source code to <code>release</code> directory under adonisweb html source directory.
```
mv ~/adonis-rsync /var/www/html/adonisweb/release-<version_number>
cd /var/www/html/adonisweb
```

From <code>/var/www/html/adonisweb</code> directory, copy the jwt configuration from <code>current</code>
directory to <code>release</code> directory.
to keep user's authentication tokens working.
```
cp -rv ./shared/config/jwt ./release-<version_number>/config/
```

To change the effective version change the symlink : 

```
rm ./current 
ln -s ./release-<version_number> ./current
```

The code served by the server is now the new version. The database can still be inconsistent with the new
version of Adonis Webapp. Follow next "database update" procedure to update the database structure.

### Update the database structure

(on the remote machine)

Update SQL scripts are available under <code>database</code> directory. Find the SQL script matching
the new version.
Replace in following command the terms adonisdb and adonisrole with your specific environment:
```
psql adonisdb adonisrole < adoniswebapp-db--previous--new.sql
```

### Update the Adonis Bureau version

When the Adonis bureau's version change, The return files have to reference the new version number in order to be correctly imported on the desktop application. 
In order to follow the Version number of the desktop application, change the value of the  ADONIS_DESKTOP_VERSION env variable to match the new version.

Because these changes does not impact the front end, there is no need to redeploy the application when the changes occur (a hotfix will work).
This will work only if the XML generator don't have to be updated.

### Add a randomizer algorithm (used in protocols)

In order to let the admin add new randomizer algorithms, parameters and applciation conditions are needed. 
The R script must be placed inside the algorithm directory defined in the env file.

Then use the command bellow to insert algorithm inside available : 
```
php7 bin/console adonis:algorithm:add nameOfTheFile.R
```

The command will ask name, parameters (with the possibility to add non existing ones that will be asked to the user when he will create an experiment, conditions.
After that, the algorithm is tested in order to confirm the correct order in parameters. After that the algorithm is added.
