# Business objects description

## Correspondence between webapp and mobile entities

| French entity name            | WebApp                 | Mobile                 |
|-------------------------------|------------------------|------------------------|
| Utilisateur                   | DesktopUser            | DesktopUser            |
| Plateforme                    | Platform               | Platform               |
| Dispositif expérimental       | Experiment             | Device                 |
| Protocole                     | Protocol               | Protocol               |
| Facteur                       | Factor                 | Factor                 |
| Modalité                      | Modality               | Modality               |
| Traitement                    | Treatment              | Treatment              |
| Bloc                          | Block                  | Block                  |
| Sous-bloc                     | SubBlock               | SubBlock               |
| Parcelle unitaire             | UnitPlot               | UnitParcel             |
| Parcelle surfacique           | SurfacicUnitPlot       |                        |
| Individu                      | Individual             | Individual             |
| Code d'état                   | StateCode              | StateCode              |
|                               | VariableScale          | Scale                  |
|                               | VariableScaleItem      | Mark                   |                   
|                               | ValueList              | ValueHintList          |
|                               | Test                   |                        |
| Projet de saisie              | Project                | DataEntryProject       |
| Session d'un projet de saisie |                        | Session                |
| Données de saisie             | ProjectData            | DataEntry              |
| Mesure                        |                        | Measure                |
| Cheminement                   | PathBase               | Workpath               |
| Note                          | Note                   | EntryNote              |
|                               | OutExperimentationZone | OutExperimentationZone |
| Variable                      | AbstractVariable       | Variable               |
|                               | SimpleVariable         | UniqueVariable         |
|                               | GeneratorVariable      | GeneratorVariable      |
|                               | SemiAutomaticVariable  |                        |
| Matériel                      | Device                 | Material               |
| Pilote matériel               | -                      | Driver                 |
| Nature ZHE                    | OezNature              | NatureZHE              |
| Annotation                    | Annotation             | Annotation             |
|                               | RequiredAnnotation     |                        |
